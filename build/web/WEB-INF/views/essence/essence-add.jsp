<%-- 
    Document   : essence-add
    Created on : 17 déc. 2018, 15:06:30
    Author     : champlain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

            <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Formulaire</h3>
              <h4 class="status label label-success"></h4>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group group-reference">
                  <label for="reference" class="col-sm-2 control-label has-error">Reférence</label>

                  <div class="col-sm-10">
                      <input type="text" required="" class="form-control" id="ref" name="reference" placeholder="reference de l'essence">
                      <span class="help-block col-sm-10"></span>
                  </div>
                </div>
                <div class="form-group group-libelle">
                  <label for="libelle" class="col-sm-2 control-label has-error">Libelle</label>

                  <div class="col-sm-10">
                      <input type="text" required="" class="form-control" id="libelle" name="libelle" placeholder="libelle de l'essence">
                      <span class="help-block col-sm-10"></span>
                  </div>
                </div>
                <div class="form-group group-densite">
                  <label for="densite" class="col-sm-2 control-label">Densité</label>

                  <div class="col-sm-10">
                      <input type="number" min="0"  required="" class="form-control" id="densite" name="densité" placeholder="densité de l'essence">
                      <span class="help-block col-sm-10"></span>
                  </div>
                </div>
              </div>
                
              <div class="box-footer">
                  <button type="button"  id="btn-send" class="btn btn-microsoft pull-right"><i class="glyphicon glyphicon-link"></i> Ajouter</button>
              </div>
            </form>
          </div>
            <div id="error-zone"></div>
            <script src="/resources/js/essence/add.js"></script>