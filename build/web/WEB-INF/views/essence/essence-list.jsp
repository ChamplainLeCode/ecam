<%-- 
    Document   : essence-list
    Created on : 17 déc. 2018, 15:06:37
    Author     : champlain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div class="modal fade"  id="zoneEditEssence" tabindex="-1" style="margin-top: 60px;" role="dialog" aria-labelledby="zoneEditEssence" aria-hidden="true">
    <div class="body col-sm-offset-3 col-sm-6"></div>
</div>
<div id="zoneAddEssence"></div>
    <div class="box">
            <div class="box-header">
                <button style="margin-right: 40px; margin-bottom: 20px; background-color: transparent;" type="button" onclick="history.back()" class="pull-left btn btn-flat" data-toggle="modal" data-target="#modal-default">
                    <i class="fa fa-arrow-left "></i> Retour
                </button>
                <button class="btn btn-bitbucket pull-lef"><i class="glyphicon glyphicon-print"></i> Imprimer</button>
              <button id="btnAddEssence" class="btn btn-flat pull-right"><i class="glyphicon glyphicon-plus"></i> Ajouter une Essence</button>
              <button id="closeAddEssence" class="btn btn-danger pull-right hide" ><i class="glyphicon glyphicon-remove"></i> Fermer Ajouter une Essence</button>
              <br><br><h3 class="box-title">Liste des essences</h3>
            </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="list-fournisseurs" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 179.75px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Reférence</th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 227.3px;" aria-label="Browser: activate to sort column ascending">Libelle</th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 206.917px;" aria-label="Platform(s): activate to sort column ascending">Densité</th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 110.1px;" aria-label="CSS grade: activate to sort column ascending">Opération</th>
                                </tr>
                            </thead>
                            <tbody id="table_data"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="/resources/js/essence/list.js"></script>