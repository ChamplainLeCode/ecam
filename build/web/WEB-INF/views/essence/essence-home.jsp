<%-- 
    Document   : totoPage
    Created on : 8 déc. 2018, 14:42:16
    Author     : champlain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ecam Essence</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

            <%@include file="/WEB-INF/views/layouts/style.jsp" %>
      <link rel="stylesheet" href="/resources/css/dataTables.bootstrap.min.css">    
    
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <%@include file="/WEB-INF/views/layouts/topBar.jsp" %>
      <!-- Left side column. contains the logo and sidebar -->
      <%@include file="/WEB-INF/views/layouts/sideBar.jsp" %>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Essences
            <small>Liste des essences</small>
            <label class="label label-danger hide" id="status-fail"></label>
            <label class="label label-success hide" id="status-success"></label>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="">ecam</li>
            <li class="">essences</li>
            <li class="active">list</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content container-fluid " id="essence-container">
            <div class="modal fade"  id="zoneEditEssence" tabindex="-1" style="margin-top: 60px;" role="dialog" aria-labelledby="zoneEditEssence" aria-hidden="true">
    <div class="body col-sm-offset-3 col-sm-6"></div>
</div>
<div id="zoneAddEssence"></div>
    <div class="box">
            <div class="box-header">
                <button style="margin-right: 40px; margin-bottom: 20px; background-color: transparent;" type="button" onclick="history.back()" class="pull-left btn btn-flat" data-toggle="modal" data-target="#modal-default">
                    <i class="fa fa-arrow-left "></i> Retour
                </button>
                <button class="btn btn-bitbucket pull-left" id="btn-imprimer"><i class="glyphicon glyphicon-print"></i> Imprimer</button>
              <button id="btnAddEssence" class="btn btn-flat pull-right"><i class="glyphicon glyphicon-plus"></i> Ajouter une Essence</button>
              <button id="closeAddEssence" class="btn btn-danger pull-right hide" ><i class="glyphicon glyphicon-remove"></i> Fermer Ajouter une Essence</button>
              <br><br><h3 class="box-title">Liste des essences</h3>
            </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="list-essence" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 179.75px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Reférence</th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 227.3px;" aria-label="Browser: activate to sort column ascending">Libelle</th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 206.917px;" aria-label="Platform(s): activate to sort column ascending">Densité</th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 110.1px;" aria-label="CSS grade: activate to sort column ascending">Opération</th>
                                </tr>
                            </thead>
                            <tbody id="table_data"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <!-- Main Footer -->
      <%@include file="/WEB-INF/views/layouts/footer.jsp" %>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <%@include file="/WEB-INF/views/layouts/script.jsp" %>    
    <script src="/resources/js/jquery.dataTables.min.js"></script>
    <script src="/resources/js/dataTables.bootstrap.min.js"></script>
    <script src="/resources/js/essence/list.js"></script>

    <script type="text/javascript">
        $(function(){
            loadData();
            document.getElementById("btn-imprimer").onclick = function(){
                tableToExcel('list-essence', 'W3C Example Table');
            };
        });
    </script>
    </body>
</html>
