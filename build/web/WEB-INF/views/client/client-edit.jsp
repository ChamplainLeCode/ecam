<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="/resources/css/select2/select2.min.css">

<div id="form-add-client">
    <p class="status-add-client" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Reférence</label>

          <div class="col-sm-10">
              <input type="text" required="" readonly="" value="<%=request.getParameter("ref")%>" class="form-control" id="zone-ref" placeholder="Reférence du Client">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-nom" class="col-sm-2 control-label">Nom</label>

          <div class="col-sm-10">
              <input type="text" required="" value="<%=request.getParameter("nom")%>" class="form-control" id="zone-nom" placeholder="Nom du client">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-pays" class="col-sm-2 control-label">Pays</label>

          <div class="col-sm-10">
              <select required="" class="form-control select2" style="width:100%;" id="zone-pays"></select>
          </div>
        </div>
        <div class="form-group">
          <label for="zone-nationalite" class="col-sm-2 control-label">Nationalité</label>
          <div class="col-sm-10">
              <select class="form-control select2" id="zone-nationalite" style="width:100%;"></select>
          </div>
        </div>
        <div class="form-group">
          <label for="zone-ville" class="col-sm-2 control-label">Ville</label>
          <div class="col-sm-10">
              <select class="form-control select2" id="zone-ville" style="width:100%;"></select>
          </div>
        </div>
        <div class="form-group">
          <label for="zone-telephone" class="col-sm-2 control-label">Téléphone</label>
          <div class="col-sm-10">
              <input type="tel" value="<%=request.getParameter("telephone")%>"  class="form-control select2" id="zone-telephone" placeholder="Téléphone du client">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-email" class="col-sm-2 control-label">Email</label>
          <div class="col-sm-10">
              <input type="email" value="<%=request.getParameter("email")%>"  class="form-control select2" id="zone-email" placeholder="Email du client">
          </div>
        </div>
      </div>
        
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-client">Enregistrer</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        var uniqueList = [];
        function groupByPays(element){
            var i; 
            for(i=0; i<uniqueList.length && uniqueList[i] !== element; i++); 
            if(i>=uniqueList.length){
                uniqueList.push(element);
                return true;
            }
            return false; 
        }
        $.ajax({
            url: '/resources/json/ville.json',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var data = JSON.parse(jqXHR.responseText);
                var select = document.getElementById('zone-pays');
                var selected=-1;
                
                data = data.filter((d)=>groupByPays(d.pays));
                
                for(var i=0; i<data.length; i++){
                    var opt = document.createElement("option");
                    opt.value = data[i].pays;
                    opt.innerHTML = data[i].pays;
                    select.appendChild(opt);
                    if(data[i].pays == '<%=request.getAttribute("pays")%>'){
                        selected = i
                    }
                }
                document.getElementById('zone-pays').selectedIndex = selected;
                $('select').select2();
                console.log(<%= request.getParameter("pays")%>);
            }
        });
        $('#zone-pays').change(function(e){
            $.ajax({
                url: '/resources/json/ville.json', 
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    var data = JSON.parse(jqXHR.responseText);
                    var select = document.getElementById('zone-ville');
                    var selected=-1;

                    var data = data.filter((d)=>$('#zone-pays').val() === d.pays);
                    select.innerHTML = '';
                    for(var i=0; i<data.length; i++){
                        var opt = document.createElement("option");
                        opt.value = data[i].ville;
                        opt.innerHTML = data[i].ville;
                        select.appendChild(opt);
                    }
                    document.getElementById('zone-ville').selectedIndex = selected;
                    $('select').select2();
                }
            });
        });
        
        $.ajax({
            url: '/resources/json/nationalite.json',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var data = JSON.parse(jqXHR.responseText);
                var select = document.getElementById('zone-nationalite');
                var selected=-1;
                for(var i=0; i<data.length; i++){
                    var opt = document.createElement("option");
                    opt.value = data[i];
                    opt.innerHTML = data[i];
                    if(data[i]=='<%=request.getAttribute("nationalite")%>'){
                        selected = i;
                    }
                    select.appendChild(opt);
                }
                document.getElementById('zone-nationalite').selectedIndex = selected;
                //$('select').select2();
            }
        });
        $('#zone-ville').val('<%=request.getAttribute("ville")%>');
        $("#btn-edit-client").click(function(){
            editData($('#zone-ref').val(), $('#zone-nom').val(), $('#zone-pays').val(), $("#zone-nationalite").val(), $('#zone-ville').val(), $("#zone-telephone").val(), $('#zone-email').val());
            $('#form-add-client .overlay').removeClass('hide');
            $('#form-add-client #btn-edit-client').addClass("disabled");
        });

    </script>
</div>
