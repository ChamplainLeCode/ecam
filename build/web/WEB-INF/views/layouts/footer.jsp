<%-- 
    Document   : footer
    Created on : 10 déc. 2018, 15:13:42
    Author     : champlain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
      <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            <a href="http://bixterprise.com">Bixterprise Coorp</a>
        </div>
        <!-- Default to the left -->
        <strong>GESTION DE PRODUCTION ECAM PLACAGES &copy; 2019 <a href="http://bixterprise.com">Bixterprise</a>.</strong> All rights reserved.
      </footer>
