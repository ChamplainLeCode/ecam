<%-- 
    Document   : totoPage
    Created on : 8 déc. 2018, 14:42:16
    Author     : champlain
--%>

<%@page import="app.models.Commande"%>
<%@page import="java.util.List"%>
<%@page import="app.models.Fournisseur"%>
<%
    /**
        Fournisseur fournisseur = (Fournisseur) request.getAttribute("fournisseur");
        List<Commande> liste = (List<Commande>) request.getAttribute("listeCommandes");
    */
%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ecam fournisseur détails</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <%@include file="/WEB-INF/views/layouts/style.jsp" %>
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <%@include file="/WEB-INF/views/layouts/topBar.jsp" %>
      <!-- Left side column. contains the logo and sidebar -->
      <%@include file="/WEB-INF/views/layouts/sideBar.jsp" %>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
              Bakop M. Champlain (<% out.print(request.getParameter("ref")); %>)
            <small>
                <% 
                    /**out.print(fournisseur.getNomFournisseur() + " ["+ fournisseur.getRefFournisseur()+"]"); */
                %>
            </small>
            <label class="label label-danger hide" id="status-fail"></label>
            <label class="label label-success hide" id="status-success"></label>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="">ecam</li>
            <li class="">fournisseurs</li>
            <li class="active">details</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">

            
 <!--           <div class="modal fade" id="zoneReceptionList" tabindex="-1" role="dialog" aria-labelledby="zoneReceptionList" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form>
                        <div class="form-group">
                          <label for="recipient-name" class="col-form-label">Recipient:</label>
                          <input type="text" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                          <label for="message-text" class="col-form-label">Message:</label>
                          <textarea class="form-control" id="message-text"></textarea>
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Send message</button>
                    </div>
                  </div>
                </div>
              </div>
-->
            
            <div id="commande-list"></div>

        </section>

      </div>
      <!-- /.content-wrapper -->

      <!-- Main Footer -->
      <%@include file="/WEB-INF/views/layouts/footer.jsp" %>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->
    <%@include file="/WEB-INF/views/layouts/script.jsp" %>
        <script type="text/javascript">
            var fournisseur = '<% out.print(request.getParameter("ref").toString()); %>';
        </script>
        <script src="/resources/js/fournisseur/detail.js"></script>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. -->
    </body>
</html>
