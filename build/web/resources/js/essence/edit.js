


    $("#btn-update").click(function(){
       $.ajax({
            url: "/essence/edit",
            method: 'POST',
            data: {
                ref: $("#reference").val(),
                libelle: $('#libelle').val(),
                densite: $('#densite').val()
            },
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                $('#zoneEditEssence>.body').html(jqXHR.responseText).html(jqXHR.responseText);
            },
            success: function (data, textStatus, jqXHR) {
                $('#status-modification>.body').html(data);
                location.href = location.href;
            }
            
       }) ; 
        zoneAddFormVisible = false;
        $('#closeAddEssence').addClass('hide');
        $('#btnAddEssence').removeClass('hide');
    
    });
