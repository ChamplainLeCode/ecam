/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function(){
    hideStatusSuccess();
    hideStatusFail();
    $('.status').html('');

    $('#libelle, #densite').val("");
    
    $('#btn-send').click(function(e){sendRegistration(e);});

    $('#btn-retour').click(function(){
       window.history.back(); 
    });
        
    $("#libelle").keyup(function(e){
       if(e.key !== ' '){
           $('.group-libelle').removeClass('has-error');
            $('.group-libelle>div>span').html("");
            hideStatusFail();
            hideStatusSuccess();
       }; 
       $(this).val() === '' ? setLibelleError() : null;
    });

    function setLibelleError(){
        $('.group-libelle').addClass("has-error");
        $('.group-libelle>div>span').html("Veuillez entrer le libelle");
        $('#libelle').focus();
    }



    function setDensiteError(){
        $('.group-densite').addClass("has-error");
        $('.group-densite>div>span').html("Veuillez entrer la densite valide, uniquement 1 2 3 4 5 6 7 8 9 . ,");
        $('#densite').focus().val('');
    }
    
    function showStatusSuccess(){
        $('#status-success').html('Essence ajoutée').removeClass('hide');
        setTimeout(function(){hideStatusSuccess();}, 3000);
    }
    
    function showStatusFail(){
        $('#status-fail').html("Essence non ajoutée").removeClass('hide');
        setTimeout(function(){hideStatusFail();}, 3000);
    }
        
    function hideStatusSuccess(){
        $('#status-success').addClass('hide');
    }
    
    function hideStatusFail(){
        $('#status-fail').addClass('hide');
    }
    
    function sendRegistration(e){
        var libelle = $('#libelle').val();
        var densite = $('#densite').val();
        var reference = $('#ref').val();
        var i= 0;

        if(libelle === '' || densite === ''){
            if(libelle === ''){
                setLibelleError();
                return false;
            }
            if(densite === ''){
                setDensiteError();
                return false;
            }
        }

        for(i = 0; i<libelle.length && libelle[i] === ' '; i++);
        if(i >= libelle.length){
            setLibelleError();
            return false;
        }

        for(i = 0; i<densite.length && densite[i] === ' '; i++);
        if(i >= densite.length){
            setDensiteError();
            return false;
        }

        $.ajax({
            dataType: 'json',
            url: "/essence/add",
            method: 'POST',
            data: {
                libelle: libelle,
                densite: densite,
                ref: reference
            },
            success: function (data, textStatus, jqXHR) {
                if((data.resultat === false) === true){hideStatusSuccess(); showStatusFail(); return;}
                $('#libelle').val("");
                $('#densite').val("");
                $('.status').html('Enregistré');
                showStatusSuccess();
                hideStatusFail(); 
                $('.group-densite').removeClass("has-error");
                $('.group-libelle').removeClass("has-error");
                /**$('#table_data').html(
                    '<tr role="row" class="odd" id="row_'+data.ref+'">'+
                        '<td class="sorting_1">'+data.ref+'</td>'+
                        '<td>'+libelle+'</td>'+
                        '<td>'+densite+'</td>'+
                        '<td>'+
                            '<button class="btn btn-danger btn-supprimer" data="'+data.ref+'" title="Supprimer l\'essence '+libelle+'"><i class="glyphicon glyphicon-trash"></i></button>'+
                            '<button class="btn btn-success btn-modifier" data="'+data.ref+'" title="Modifier l\'essence '+libelle+'"><i class="glyphicon glyphicon-pencil"></i></button>'+
                        '</td>'+
                    '</tr>'+$(this).html()           
                ); */
                //location.href = location.href;
                $.ajax({
                    url: '/essence/list',
                    method: 'GET',
                    data: {mark: "all"},
                    dataType: 'json',
                    success: function (data, textStatus, jqXHR) {
                        $('#essence-container').html(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#essence-container').html(jqXHR.responseText);
                    },
                    beforeSend: function (xhr) {
                        $('#essence-container').html("<h2 style=\"color:gray\">Téléchargement des données en cours</h2>");
                    }
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR.responseText);
                hideStatusSuccess();
                showStatusFail();
                $('.status').html('non enregistré');
                    $('#status-fail').html('erreur: Essence non modifié').removeClass('hide');
                    $('#status-success').html('').addClass('hide');
            }
        });
    }
});
