/* global fournisseur */

$(function(){

    
    $.ajax({
        url: '/commande/fournisseur',
        dataType: 'json',
        data: {
            ref: fournisseur
        },
        method: "GET",
        success: function (data, textStatus, jqXHR) {
            $('#commande-list').html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#commande-list').html(jqXHR.responseText);
        }
    });

    $('.btn-reception').click(function(e){
       $.ajax({
            url: '/commande/list/reception',
            dataType: 'json',
            data: {
                data: $(this).attr('data')
            },
            method: "GET",
            success: function (data, textStatus, jqXHR) {
                $("#zoneReceptionList").html(data);
                $('#openReceptionZone').click();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $("#zoneReceptionList").html(jqXHR.responseText);
                console.log(jqXHR.responseText);
                //$('#openReceptionZone').click();
            }
       })
    });
    
});
