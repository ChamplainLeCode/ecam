/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.routes;

import app.controllers.BillonController;
import app.controllers.BillonnageController;
import app.controllers.CertificatController;
import app.controllers.ChauffeurController;
import app.controllers.ClassificationController;
import app.controllers.ClassificationEssController;
import app.controllers.ClientController;
import app.controllers.ColisController;
import app.controllers.CommandeController;
import app.controllers.CompagnieController;
import app.controllers.ContenaireController;
import app.controllers.ConteneurController;
import app.controllers.CoupeController;
import app.controllers.CuveController;
import app.controllers.CuvePlotController;

import app.controllers.DetailCommandeController;
import app.controllers.DetailEquipeController;
import app.controllers.DetailReceptionController;
import app.controllers.EmpotageController; 
import app.controllers.EquipeController;
import app.controllers.EssenceController;
import app.controllers.FeuilleController;
import app.controllers.FournisseurController;
import app.controllers.IndexController;
import app.controllers.LettreCamionController;
import app.controllers.MassicotController;
import app.controllers.MassicotPaquetController;
import app.controllers.PaquetController;
import app.controllers.ParcChargementController;
import app.controllers.ParcController;
import app.controllers.PersonnelController;
import app.controllers.PlotController;
import app.controllers.PrivilegeController;
import app.controllers.RebutController;
import app.controllers.ReceptionController;
import app.controllers.RouteController;
import app.controllers.SechoirController;
import app.controllers.StockController;
import app.controllers.TrancheController;
import app.controllers.TrancheMachineController;
import app.controllers.TransporteurController;
import app.controllers.TypeContenaireController;
import app.controllers.TypeCoupeController;
import app.controllers.TypeParcController;
import app.controllers.AccesController;
import app.controllers.ArmateurController;
import app.controllers.BateauController;
import app.controllers.JointageController;
import app.controllers.TitreController;


import kodvel.core.route.Router;

/**
 *
 * @author champlain
 */
final public class Api{
    
    protected static String urlPattern = "/api";
    public static int maxResult = 20;

    protected void Api(){
        //Api.this.registerRouter();
        
    }

    public static void registerRouter() {

        Router.get(urlPattern+"/comptabilite/billon", new StockController() , "billon");
        Router.get(urlPattern+"/comptabilite/billonnage", new StockController() , "billonnage");
        Router.get(urlPattern+"/comptabilite/client", new StockController() , "client");
        Router.get(urlPattern+"/comptabilite/colis", new StockController() , "colis");
        Router.get(urlPattern+"/comptabilite/cuve", new StockController() , "cuve");
        Router.get(urlPattern+"/comptabilite/embarquement", new StockController() , "embarquement");
        Router.get(urlPattern+"/comptabilite/empotage", new StockController() , "empotage");
        Router.get(urlPattern+"/comptabilite/fournisseur", new StockController() , "fournisseur");
        Router.get(urlPattern+"/comptabilite/lettre", new StockController() , "lettre");
        Router.get(urlPattern+"/comptabilite/paquet", new StockController() , "paquet");
        Router.get(urlPattern+"/comptabilite/plot", new StockController() , "plot");
        Router.get(urlPattern+"/comptabilite/parc", new StockController() , "parc");
        
        Router.post(urlPattern+"/auth/verify", new IndexController(), "verifyAuth");

        Router.get(urlPattern+"/certification/travail/colis", new IndexController(), "getCertificationByColis");
        Router.get(urlPattern+"/certification/travail", new IndexController(), "getCertificationBy");
        Router.get(urlPattern+"/check/travail", new IndexController(), "checkTravail");
        
        Router.get(urlPattern+"/billon", new  BillonController(), "data");
        Router.get(urlPattern+"/billon/list", new  BillonController(), "dataList");
        Router.get(urlPattern+"/billon/list/page", new  BillonController(), "dataListPage");
        Router.get(urlPattern+"/billon_from_bille/list", new BillonController(), "billonFromBille");
        Router.get(urlPattern+"/billon/billon_for_massicot", new BillonController(), "billonForMassicot");
        Router.get(urlPattern+"/bille/travail_for_massicot", new BillonController(), "billeForMassicot");

        Router.get(urlPattern+"/billonnage", new  BillonnageController(), "data");
        Router.get(urlPattern+"/billonnage/list", new  BillonnageController(), "dataList");
        Router.get(urlPattern+"/billonnage/list/page", new  BillonnageController(), "dataListPage");
        Router.get(urlPattern+"/billonnage/list/reference", new  BillonnageController(), "referenceList");

        Router.get(urlPattern+"/certificat", new  CertificatController(), "data");

        Router.get(urlPattern+"/chauffeur/list", new ChauffeurController(), "dataList");

        Router.get(urlPattern+"/compagnie/list", new CompagnieController(), "dataList");

        Router.get(urlPattern+"/conteneur/list", new ConteneurController(), "dataList");

        Router.get(urlPattern+"/classification", new  ClassificationController(), "data");
        Router.get(urlPattern+"/classification/list", new  ClassificationController(), "dataList");
        Router.get(urlPattern+"/classification/list/page", new  ClassificationController(), "dataListPage");

        Router.get(urlPattern+"/classification_ess", new  ClassificationEssController(), "data");
        Router.get(urlPattern+"/classification_ess/list", new  ClassificationEssController(), "dataList");
        Router.get(urlPattern+"/classification_ess/list/page", new  ClassificationEssController(), "dataListPage");

        Router.get(urlPattern+"/client", new  ClientController(), "data");
        Router.get(urlPattern+"/client/list", new  ClientController(), "dataList");
        Router.get(urlPattern+"/client/list/page", new  ClientController(), "dataListPage");

        Router.get(urlPattern+"/colis/certification", new  ColisController(), "getCertificationColis");
        Router.get(urlPattern+"/colis", new  ColisController(), "data");
        Router.get(urlPattern+"/colis/opened", new  ColisController(), "getColisOuverts");
        Router.get(urlPattern+"/colis/opened/atelier", new  ColisController(), "getColisOuvertsByAtelier");
        Router.get(urlPattern+"/colis/group_by/paquet_number", new  ColisController(), "groupPaquetByNumber");
        Router.get(urlPattern+"/colis/list", new  ColisController(), "dataList");
        Router.get(urlPattern+"/colis/for_empotage/list", new  ColisController(), "colisForEmpotage");
        Router.get(urlPattern+"/colis/for_contenaire/list", new  ColisController(), "colisForContenaire");
        Router.get(urlPattern+"/colis/list/page", new  ColisController(), "dataListPage");

        Router.get(urlPattern+"/commande", new  CommandeController(), "data");
        Router.get(urlPattern+"/commande/list/ref", new  CommandeController(), "refList");
        Router.get(urlPattern+"/commande/list", new  CommandeController(), "dataList");
        Router.get(urlPattern+"/commande/list/page", new  CommandeController(), "dataListPage");
        Router.get(urlPattern+"/commande_fournisseur/list", new CommandeController(), "findCommandeByFounisseur");
        
        Router.get(urlPattern+"/contenaire/armateur", new  ArmateurController(), "data");
        Router.get(urlPattern+"/contenaire/armateur/list", new  ArmateurController(), "dataList");
        Router.get(urlPattern+"/contenaire/armateur/list/page", new  ArmateurController(), "dataListPage");
        
        
        Router.get(urlPattern+"/contenaire/navire", new  BateauController(), "data");
        Router.get(urlPattern+"/contenaire/navire/list", new  BateauController(), "dataList");
        Router.get(urlPattern+"/contenaire/navire/list/page", new  BateauController(), "dataListPage");
        
        Router.get(urlPattern+"/contenaire", new  ContenaireController(), "data");
        Router.get(urlPattern+"/contenaire/list", new  ContenaireController(), "dataList");
        Router.get(urlPattern+"/contenaire/list/page", new  ContenaireController(), "dataListPage");
        Router.get(urlPattern+"/contenaire/qualite_n_epaisseur", new  ContenaireController(), "getQualityNEpaisseur");

        Router.get(urlPattern+"/coupe", new  CoupeController(), "data");
        Router.get(urlPattern+"/coupe/list", new  CoupeController(), "dataList");
        Router.get(urlPattern+"/coupe/list/page", new  CoupeController(), "dataListPage");
        Router.get(urlPattern+"/coupe/bille_for_coupe/list", new CoupeController(), "billeForCoupe");
        Router.get(urlPattern+"/coupe/billon_from_bille", new BillonController(), "billonFromBilleForCoupe");
        
        
        Router.get(urlPattern+"/cuve", new  CuveController(), "data");
        Router.get(urlPattern+"/cuve/list", new  CuveController(), "dataList");
        Router.get(urlPattern+"/cuve/list/page", new  CuveController(), "dataListPage");
        Router.get(urlPattern+"/cuve/vide/list", new  CuveController(), "cuveVideList");
        Router.get(urlPattern+"/cuve/pleine/list", new CuveController(), "cuvePleineList");
        
        Router.get(urlPattern+"/cuve_plot", new  CuvePlotController(), "data");
        Router.get(urlPattern+"/cuve_plot/list", new  CuvePlotController(), "dataList");
        Router.get(urlPattern+"/cuve_plot/list/page", new  CuvePlotController(), "dataListPage");
        Router.get(urlPattern+"/cuve_plot/in_cuve", new CuvePlotController(), "getPlotInCuve");
        
        Router.get(urlPattern+"/detail_commande", new  DetailCommandeController(), "data");
        Router.get(urlPattern+"/detail_commande/list", new  DetailCommandeController(), "dataList");
        Router.get(urlPattern+"/detail_commande/list/page", new  DetailCommandeController(), "dataListPage");
        Router.get(urlPattern+"/detail_commande/by_commande", new DetailCommandeController(), "findDetailByCommande");
        Router.get(urlPattern+"/detail_commande/by_bille", new DetailCommandeController(), "findDetailByBille");
        
        Router.get(urlPattern+"/detail_reception", new  DetailReceptionController(), "data");
        Router.get(urlPattern+"/detail_reception/list", new  DetailReceptionController(), "dataList");
        Router.get(urlPattern+"/detail_reception/list/page", new  DetailReceptionController(), "dataListPage");
        Router.get(urlPattern+"/detail_reception/by_bille", new  DetailReceptionController(), "getByBille");
        Router.get(urlPattern+"/detail_reception/non_recu/by_commande", new  DetailReceptionController(), "getBilleNonReceptionnees");
        Router.get(urlPattern+"/detail_reception/by_commande", new  DetailReceptionController(), "getByCommande");
        Router.get(urlPattern+"/detail_reception/not_received", new DetailReceptionController(), "getNotReceived");
        Router.get(urlPattern+"/detail_reception/essence/by_bille", new DetailReceptionController(), "essenceByNumBille");

        
        Router.get(urlPattern+"/detail_equipe", new  DetailEquipeController(), "data");
        Router.get(urlPattern+"/detail_equipe/list", new  DetailEquipeController(), "dataList");
        Router.get(urlPattern+"/detail_equipe/list/page", new  DetailEquipeController(), "dataListPage");

        Router.get(urlPattern+"/empotage", new  EmpotageController(), "data");
        Router.get(urlPattern+"/empotage/list", new  EmpotageController(), "dataList");
        Router.get(urlPattern+"/empotage/list/page", new  EmpotageController(), "dataListPage");

        Router.get(urlPattern+"/equipe", new  EquipeController(), "data");
        Router.get(urlPattern+"/equipe_full", new EquipeController(), "dataFull");
        Router.get(urlPattern+"/equipe/list_full", new EquipeController(), "dataListFull");
        Router.get(urlPattern+"/equipe/list", new  EquipeController(), "dataList");
        Router.get(urlPattern+"/equipe/list/page", new  EquipeController(), "dataListPage");

        Router.get(urlPattern+"/essence/check", new  EssenceController(), "essenceCheck");
        Router.get(urlPattern+"/essence", new  EssenceController(), "data");
        Router.get(urlPattern+"/essence/list", new  EssenceController(), "dataList");
        Router.get(urlPattern+"/essence/base/list", new  EssenceController(), "dataBaseList");
        Router.get(urlPattern+"/essence/list/page", new  EssenceController(), "dataListPage");

        Router.get(urlPattern+"/feuille", new  FeuilleController(), "data");
        Router.get(urlPattern+"/feuille/list", new  FeuilleController(), "dataList");
        Router.get(urlPattern+"/feuille/list/page", new  FeuilleController(), "dataListPage");

        Router.get(urlPattern+"/fournisseur", new  FournisseurController(), "data");
        Router.get(urlPattern+"/fournisseur/list", new  FournisseurController(), "dataList");
        Router.get(urlPattern+"/fournisseur/list/page", new  FournisseurController(), "dataListPage");
        
        Router.get(urlPattern+"/jointage/list/fiche", new JointageController(), "fiche");
        Router.get(urlPattern+"/jointage/list", new JointageController(), "dataList");
        Router.get(urlPattern+"/jointage/list/count", new JointageController(), "dataListCount");
        Router.get(urlPattern+"/jointage/colis/for_jointage", new JointageController(), "listOfColis");
        
        Router.get(urlPattern+"/lettre_camion", new  LettreCamionController(), "data");
        Router.get(urlPattern+"/lettre_camion/list", new  LettreCamionController(), "dataList");
        Router.get(urlPattern+"/lettre_camion/list/page", new  LettreCamionController(), "dataListPage");

        Router.get(urlPattern+"/massicot", new  MassicotController(), "data");
        Router.get(urlPattern+"/massicot/list", new  MassicotController(), "dataList");
        Router.get(urlPattern+"/massicot/list/page", new  MassicotController(), "dataListPage");

        Router.get(urlPattern+"/massicot_paquet", new  MassicotPaquetController(), "data");
        Router.get(urlPattern+"/massicot_paquet/list", new  MassicotPaquetController(), "dataList");
        Router.get(urlPattern+"/massicot_paquet/list/page", new  MassicotPaquetController(), "dataListPage");
        Router.get(urlPattern+"/massicot_paquet/plot/numero", new  MassicotPaquetController(), "getNumPlotFromMassicot");
        Router.get(urlPattern+"/massicot_paquet/paquet", new  MassicotPaquetController(), "getPaquetForValidation");
        Router.post(urlPattern+"/massicotage/fiche", new  MassicotPaquetController(), "getDataforMassicotFiche");

        Router.get(urlPattern+"/paquet", new  PaquetController(), "data");
        Router.get(urlPattern+"/paquet/list", new  PaquetController(), "dataList");
        Router.get(urlPattern+"/paquet/list/page", new  PaquetController(), "dataListPage");

        Router.get(urlPattern+"/parc", new  ParcController(), "data");
        Router.get(urlPattern+"/parc/list", new  ParcController(), "dataList");
        Router.get(urlPattern+"/parc/list/page", new  ParcController(), "dataListPage");
        Router.get(urlPattern+"/parc/by_bille", new ParcController(), "getParcByBille");
        Router.get(urlPattern+"/parc/list/by_origine", new ParcController(), "getParcByOrigine");
        Router.get(urlPattern+"/parc/recently/out", new ParcController(), "getRecentlyOut");
        Router.get(urlPattern+"/parc_chargement/by_commande", new ParcChargementController(), "byCommande");
        Router.get(urlPattern+"/parc_chargement", new  ParcChargementController(), "data");
        Router.get(urlPattern+"/parc_chargement/by_fournisseur", new  ParcChargementController(), "byFournisseur");
        Router.get(urlPattern+"/parc_chargement/list", new  ParcChargementController(), "dataList");
        Router.get(urlPattern+"/parc_chargement/list/page", new  ParcChargementController(), "dataListPage");

        Router.get(urlPattern+"/agent/list", new app.controllers.AgentController(), "dataList");
        Router.get(urlPattern+"/douanier/list", new app.controllers.DouanierController(), "dataList");
        Router.get(urlPattern+"/port/list", new app.controllers.PortController(), "dataList");
        Router.get(urlPattern+"/pays/list", new app.controllers.PaysController(), "paysList");
        Router.get(urlPattern+"/ville/list", new app.controllers.VilleController(), "dataList");
        
        Router.get(urlPattern+"/acces", new  AccesController(), "data");
        Router.get(urlPattern+"/acces/list", new  AccesController(), "dataList");
        Router.get(urlPattern+"/acces/list/page", new  AccesController(), "dataListPage");
        
        
        
        Router.get(urlPattern+"/route", new  RouteController(), "data");
        Router.get(urlPattern+"/route/list", new  RouteController(), "dataList");
        Router.get(urlPattern+"/route/list/page", new  RouteController(), "dataListPage");
        
        
        Router.get(urlPattern+"/privilege", new  PrivilegeController(), "data");
        Router.get(urlPattern+"/privilege/list", new  PrivilegeController(), "dataList");
        Router.get(urlPattern+"/privilege/list/page", new  PrivilegeController(), "dataListPage");
        
        Router.get(urlPattern+"/personnel", new  PersonnelController(), "data");
        Router.get(urlPattern+"/personnel/list", new  PersonnelController(), "dataList");
        Router.get(urlPattern+"/personnel/list/page", new  PersonnelController(), "dataListPage");

        Router.get(urlPattern+"/plot", new  PlotController(), "data");
        Router.get(urlPattern+"/plot/list/for_cuve", new  PlotController(), "plotforCuve");
        Router.get(urlPattern+"/plot/list", new  PlotController(), "dataList");
        Router.get(urlPattern+"/plot/list/page", new  PlotController(), "dataListPage");
        Router.get(urlPattern+"/plot/get_last_face", new PlotController(), "getLastFace");
        Router.get(urlPattern+"/plot/plot_for_massicot", new PlotController(), "getPlotForMassicot");
        
        Router.get(urlPattern+"/privilege", new  PrivilegeController(), "data");
        Router.get(urlPattern+"/privilege/list", new  PrivilegeController(), "dataList");
        Router.get(urlPattern+"/privilege/list/page", new  PrivilegeController(), "dataListPage");

        Router.get(urlPattern+"/rebut", new  RebutController(), "data");
        Router.get(urlPattern+"/rebut/list", new  RebutController(), "dataList");
        Router.get(urlPattern+"/rebut/list/page", new  RebutController(), "dataListPage");

        Router.get(urlPattern+"/reception/list/numero_bille", new  ReceptionController(), "billeList");
        Router.get(urlPattern+"/reception/list/numero_bille/no_numTravail", new  ReceptionController(), "billeListWithNumTravail");
        Router.get(urlPattern+"/reception", new  ReceptionController(), "data");
        Router.get(urlPattern+"/reception/list", new  ReceptionController(), "dataList");
        Router.get(urlPattern+"/reception/list/page", new  ReceptionController(), "dataListPage");
        Router.get(urlPattern+"/reception/min", new ReceptionController(), "dataMinList");
        
        
        Router.get(urlPattern+"/sechoir", new  SechoirController(), "data");
        Router.get(urlPattern+"/sechoir/list", new  SechoirController(), "dataList");
        Router.get(urlPattern+"/sechoir/list/page", new  SechoirController(), "dataListPage");

        Router.get(urlPattern+"/tranche-machine", new  TrancheMachineController(), "data");
        Router.get(urlPattern+"/tranche-machine/list", new  TrancheMachineController(), "dataList");
        Router.get(urlPattern+"/tranche-machine/list/page", new  TrancheMachineController(), "dataListPage");

        Router.get(urlPattern+"/tranche", new  TrancheController(), "data");
        Router.get(urlPattern+"/tranche/list", new  TrancheController(), "dataList");
        Router.get(urlPattern+"/tranche/list/page", new  TrancheController(), "dataListPage");
        Router.get(urlPattern+"/tranche/epaisseur_for_plot", new TrancheController(), "getEpaisseurForPlot");
        Router.get(urlPattern+"/tranche/plot_for_tranche/list", new TrancheController(), "getPlotForTranche");
        

        Router.get(urlPattern+"/transporteur/list", new TransporteurController(), "dataList");

        Router.get(urlPattern+"/titre/list", new TitreController(), "dataList");
        Router.get(urlPattern+"/titre/for_parc", new TitreController(), "byParc");
        
        Router.get(urlPattern+"/type_coupe", new  TypeCoupeController(), "data");
        Router.get(urlPattern+"/type_coupe/list", new  TypeCoupeController(), "dataList");
        Router.get(urlPattern+"/type_coupe/list/page", new  TypeCoupeController(), "dataListPage");

        Router.get(urlPattern+"/type_parc", new  TypeParcController(), "data");
        Router.get(urlPattern+"/type_parc/list", new  TypeParcController(), "dataList");
        Router.get(urlPattern+"/type_parc/list/page", new  TypeParcController(), "dataListPage");

        Router.get(urlPattern+"/type_contenaire", new  TypeContenaireController(), "data");
        Router.get(urlPattern+"/type_contenaire/list", new  TypeContenaireController(), "dataList");
        Router.get(urlPattern+"/type_contenaire/list/page", new  TypeContenaireController(), "dataListPage");
        
        Router.get(urlPattern+"/type_mouvement_parc", new ParcController(), "getMouvementParc");
    }
}

