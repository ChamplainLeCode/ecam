/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.routes;

import app.controllers.AccesController;
import app.controllers.AgentController;
import app.controllers.ArmateurController;
import app.controllers.BateauController;
import app.controllers.BillonController;
import app.controllers.BillonnageController;
import app.controllers.CertificatController;
import app.controllers.ChauffeurController;
import app.controllers.ClassificationController;
import app.controllers.ClassificationEssController;
import app.controllers.ClientController;
import app.controllers.ColisController;
import app.controllers.CommandeController;
import app.controllers.CompagnieController;

import app.controllers.ContenaireController;
import app.controllers.ConteneurController;
import app.controllers.CoupeController;
import app.controllers.CuveController;
import app.controllers.CuvePlotController;

import app.controllers.DetailEquipeController;
import app.controllers.DetailReceptionController;
import app.controllers.DouanierController;
import app.controllers.EmpotageController;
import app.controllers.EquipeController;
import app.controllers.EssenceController;
import app.controllers.FeuilleController;
import app.controllers.FournisseurController;
import app.controllers.IndexController;
import app.controllers.LettreCamionController;
import app.controllers.ParcController;
import app.controllers.PaquetController;
import app.controllers.PersonnelController;
import app.controllers.MassicotController;
import app.controllers.MassicotPaquetController;
import app.controllers.ParcChargementController;
import app.controllers.PaysController;
import app.controllers.PlotController;
import app.controllers.PortController;
import app.controllers.PrivilegeController;
import app.controllers.RebutController;
import app.controllers.ReceptionController;
import app.controllers.RouteController;
import app.controllers.SechoirController;
import app.controllers.TrancheController;
import app.controllers.TrancheMachineController;
import app.controllers.TransporteurController;
import app.controllers.TypeContenaireController;
import app.controllers.TypeCoupeController;


import app.controllers.TypeParcController;
import app.controllers.VilleController;
import kodvel.interfaces.Route.BaseRouter;
import kodvel.core.route.Router;
/**
 * Register routes
 * 
 * @author Md. Rezve Hasan
 * @since 0.0.1
 */
public class Web implements BaseRouter{
    /**
     * Register your routes here.
     * 
     * Your controller and method will be invoked when user request for the specified URL.
     * Currently supported HTTP methods are GET,POST,PUT,PATCH,DELETE
     */
    FournisseurController fournisseurController = new FournisseurController();

    EssenceController essenceController = new EssenceController();
    ClientController clientController = new ClientController();
    EquipeController equipeController = new EquipeController();
    PersonnelController personnelController = new PersonnelController();
    BillonnageController billonnageController = new BillonnageController();
    TypeCoupeController typecoupeController = new TypeCoupeController();
    TypeContenaireController typecontenaireController = new TypeContenaireController();
    TrancheController trancheController = new TrancheController();
    TrancheMachineController trancheMachineController = new TrancheMachineController();
    RebutController rebutController = new RebutController();
    EmpotageController empotageController = new EmpotageController();
    CuveController cuveController = new CuveController();
    BillonController billonController = new BillonController();
    ParcChargementController parcchargementController = new ParcChargementController();
    PlotController plotController = new PlotController();
    PaysController paysController = new PaysController();
    VilleController villeController = new VilleController();
    PortController portController = new PortController();
    DouanierController douanierController = new DouanierController();
    AgentController agentController = new AgentController();
    ParcController parcController = new ParcController();

    PaquetController paquetController = new PaquetController();
    CuvePlotController cuveplotController = new CuvePlotController();
    CoupeController coupeController = new CoupeController();
    ColisController colisController = new ColisController();
    SechoirController sechoirController = new SechoirController();
    ContenaireController contenaireController = new ContenaireController();
    FeuilleController feuilleController = new FeuilleController();
    DetailEquipeController detailequipeController = new DetailEquipeController();
    TypeParcController typeparcController = new TypeParcController();

    CommandeController commandeFournisseurController = new CommandeController();
    ReceptionController receptionController = new ReceptionController();

    
    MassicotController massicotController = new MassicotController();
    
    LettreCamionController lettrecamionController = new LettreCamionController();
    MassicotPaquetController massicotpaquetController = new MassicotPaquetController();
    PrivilegeController privilegeController = new PrivilegeController();
    RouteController routeController = new RouteController();
    AccesController accessController = new AccesController();
    ArmateurController armateurController = new ArmateurController();
    
    
    @Override
    public void registerRouter() {

// ------------------------------------- API -----------------------------------------
        Router.get("/", new IndexController(), "welcome");
        Router.get("/login", new IndexController(), "index");
        Router.get("/register", new IndexController(), "registrationForm");
        Router.get("/lockscreen", new IndexController(), "lockscreen");
        Router.post("/login", new IndexController(), "connexion");
        Router.post("/register/new", new PersonnelController(), "create");
        Router.post("/user/disconnected", new PersonnelController(), "disconnected");
        Router.post("/user/logout", new PersonnelController(), "logout");
        Router.post("/user/block", new PersonnelController(), "block");
        Router.post("/user/block_me", new PersonnelController(), "blockMe");
        
        Router.get("/classification", new ClassificationController(), "index");
        Router.get("/classification/add", new ClassificationController(), "add");
        Router.get("/classification/edit", new ClassificationController(), "edit");
        Router.post("/classification/add", new ClassificationController(), "save");
        Router.post("/classification/delete", new ClassificationController(), "delete");
        Router.post("/classification/edit", new ClassificationController(), "update");
        
        Router.get("/classification-ess", new ClassificationEssController(), "index");
        Router.get("/classification-ess/add", new ClassificationEssController(), "add");
        Router.get("/classification-ess/edit", new ClassificationEssController(), "edit");
        Router.post("/classification-ess/add", new ClassificationEssController(), "save");
        Router.post("/classification-ess/delete", new ClassificationEssController(), "delete");
        Router.post("/classification-ess/edit", new ClassificationEssController(), "update");
        
        
        Router.get("/client", clientController, "index");
        Router.get("/client/add", clientController, "add");
        Router.get("/client/edit", clientController, "edit");
        Router.post("/client/add", clientController, "create");
        Router.post("/client/edit", clientController, "update");
        Router.post("/client/delete", clientController, "delete");
        
        
         Router.get("/equipe", equipeController, "index");
        Router.get("/equipe/add", equipeController, "add");
        Router.get("/equipe/edit", equipeController, "edit");
        Router.post("/equipe/add", equipeController, "create");
        Router.post("/equipe/edit", equipeController, "update");
        Router.post("/equipe/delete", equipeController, "delete");
        
        
         Router.get("/contenaire/ticket-pesee", contenaireController, "ticketPesee");
         Router.get("/contenaire/bordereau", contenaireController, "fichebordereau");
         Router.get("/contenaire/specification", contenaireController, "specification");
         Router.get("/contenaire/essence", contenaireController, "essence");
         Router.post("/contenaire/ticket-pesee", contenaireController, "ticketPesee");
         Router.get("/contenaire", contenaireController, "index");
        Router.get("/contenaire/add", contenaireController, "add");
        Router.get("/contenaire/edit", contenaireController, "edit");
        Router.post("/contenaire/add", contenaireController, "create");
        Router.post("/contenaire/edit", contenaireController, "update");
        Router.post("/contenaire/delete", contenaireController, "delete");
        
        Router.get("/jointage", new app.controllers.JointageController(), "index");
        Router.get("/jointage/add", new app.controllers.JointageController(), "add");
        Router.post("/jointage/add", new app.controllers.JointageController(), "create");
        
        Router.get("/personnel", personnelController, "index");
        Router.get("/personnel/management", personnelController, "userManagement");
        Router.get("/personnel/add", personnelController, "add");
        Router.get("/personnel/edit", personnelController, "edit");
        Router.post("/personnel/add", personnelController, "create");
        Router.post("/personnel/delete", personnelController, "delete");
        Router.post("/personnel/edit", personnelController, "update");
        
        
        Router.get("/privilege", privilegeController, "index");
        Router.get("/privilege/add", privilegeController, "add");
        Router.get("/privilege/edit", privilegeController, "edit");
        Router.post("/privilege/add", privilegeController, "create");
        Router.post("/privilege/delete", privilegeController, "delete");
        Router.post("/privilege/edit", privilegeController, "update");
        
        Router.get("/access", accessController, "index");
        Router.get("/access/add", accessController, "add");
        Router.get("/access/edit", accessController, "edit");
        Router.post("/access/add", accessController, "create");
        Router.post("/access/delete", accessController, "delete");
        Router.post("/access/edit", accessController, "update");
        
        Router.get("/armateur", armateurController, "index");
        Router.get("/armateur/add", armateurController, "add");
        Router.get("/armateur/edit", armateurController, "edit");
        Router.post("/armateur/add", armateurController, "create");
        Router.post("/armateur/delete", armateurController, "delete");
        Router.post("/armateur/edit", armateurController, "update");
        
        
        Router.get("/pays", paysController, "index");
        Router.get("/pays/add", paysController, "add");
        Router.get("/pays/edit", paysController, "edit");
        Router.post("/pays/add", paysController, "create");
        Router.post("/pays/delete", paysController, "delete");
        Router.post("/pays/edit", paysController, "update");
        
        
        Router.get("/agent", agentController, "index");
        Router.get("/agent/add", agentController, "add");
        Router.get("/agent/edit", agentController, "edit");
        Router.post("/agent/edit/unSuspendre", agentController, "unSuspend");
        Router.post("/agent/edit/suspendre", agentController, "suspend");
        Router.post("/agent/add", agentController, "create");
        Router.post("/agent/delete", agentController, "delete");
        Router.post("/agent/edit", agentController, "update");
        
        
        
        Router.get("/douanier", douanierController, "index");
        Router.get("/douanier/add", douanierController, "add");
        Router.get("/douanier/edit", douanierController, "edit");
        Router.post("/douanier/edit/unSuspendre", douanierController, "unSuspend");
        Router.post("/douanier/edit/suspendre", douanierController, "suspend");
        Router.post("/douanier/add", douanierController, "create");
        Router.post("/douanier/delete", douanierController, "delete");
        Router.post("/douanier/edit", douanierController, "update");
        
        
        Router.get("/port", portController, "index");
        Router.get("/port/add", portController, "add");
        Router.get("/port/edit", portController, "edit");
        Router.post("/port/add", portController, "create");
        Router.post("/port/delete", portController, "delete");
        Router.post("/port/edit", portController, "update");
        
        Router.get("/ville", villeController, "index");
        Router.get("/ville/add", villeController, "add");
        Router.get("/ville/edit", villeController, "edit");
        Router.post("/ville/add", villeController, "create");
        Router.post("/ville/delete", villeController, "delete");
        Router.post("/ville/edit", villeController, "update");
        
        
        Router.get("/route", routeController, "index");
        Router.get("/route/add", routeController, "add");
        Router.get("/route/edit", routeController, "edit");
        Router.post("/route/add", routeController, "create");
        Router.post("/route/delete", routeController, "delete");
        Router.post("/route/edit", routeController, "update");
        
        
        Router.get("/plot", plotController, "index");
        Router.get("/plot/add", plotController, "add");
        Router.get("/plot/edit", plotController, "edit");
        Router.post("/plot/add", plotController, "create");
        Router.post("/plot/delete", plotController, "delete");
        Router.post("/plot/edit", plotController, "update");
        
        
         Router.get("/cuve", cuveController, "index");
        Router.get("/cuve/add", cuveController, "add");
        Router.get("/cuve/edit", cuveController, "edit");
        Router.post("/cuve/add", cuveController, "create");
        Router.post("/cuve/edit", cuveController, "update");
        Router.post("/cuve/delete", cuveController, "delete");
        
        
        
        Router.get("/cuveplot", cuveplotController, "index");
        Router.get("/cuveplot/add", cuveplotController, "add");
        Router.get("/cuveplot/edit", cuveplotController, "edit");
        Router.post("/cuveplot/add", cuveplotController, "create");
        Router.post("/cuveplot/edit", cuveplotController, "update");
        Router.post("/cuveplot/delete", cuveplotController, "delete");
        Router.post("/cuve_plot/fin_cuisson", cuveplotController, "setFinCuisson");
        
         Router.get("/empotage", empotageController, "index");
        Router.get("/empotage/add", empotageController, "add");
        Router.get("/empotage/edit", empotageController, "edit");
        Router.post("/empotage/add", empotageController, "create");
        Router.post("/empotage/delete", empotageController, "delete");
        Router.post("/empotage/edit", empotageController, "update");
        
        
        // --------- Api ---------------
                
        Router.get("/fournisseur/add", fournisseurController, "add");
        Router.get("/fournisseur", fournisseurController, "index");
        Router.get("/fournisseur/edit", fournisseurController, "edit");
        Router.get("/fournisseur/detail", fournisseurController, "detail");
        Router.get("/fournisseur/card", fournisseurController, "card");
        Router.get("/fournisseur/search", fournisseurController, "search");
        Router.post("/fournisseur/add", fournisseurController, "create");
        Router.post("/fournisseur/edit", fournisseurController, "update");
        Router.post("/fournisseur/delete", fournisseurController, "delete");
       
        
        Router.post("/parc_chargement/delete", new ParcChargementController(), "delete");
        Router.post("/certificat/delete", new CertificatController(), "delete");
        
        
        
        
         
        Router.get("/parc", parcController, "index");
        Router.get("/parc/add", parcController, "add");
        Router.get("/parc/list", parcController, "list");
        Router.get("/parc/edit", parcController, "edit");
        Router.get("/parc/detail", parcController, "detail");
       Router.post("/parc/add", parcController, "create");
        Router.post("/parc/delete", parcController, "delete");
        Router.post("/parc/edit", parcController, "update");
      
         Router.get("/typeparc", typeparcController, "index");
        Router.get("/typeparc/add", typeparcController, "add");
        Router.get("/typeparc/edit", typeparcController, "edit");
        Router.post("/typeparc/add", typeparcController, "create");
        Router.post("/typeparc/delete", typeparcController, "delete");
        Router.post("/typeparc/edit", typeparcController, "update");
        
        
         Router.get("/colis", colisController, "index");
        Router.get("/colis/add", colisController, "add");
        Router.get("/colis/edit", colisController, "edit");
        Router.post("/colis/add", colisController, "create");
        Router.post("/colis/edit", colisController, "update");
        Router.post("/colis/delete", colisController, "delete");
        
        
        Router.get("/essences", essenceController, "index");
        Router.get("/essence/list", essenceController, "list");
        Router.get("/essence/add", essenceController, "add");
        Router.get("/essence/edit", essenceController, "edit");
        Router.post("/essence/add", essenceController, "create");
        Router.post("/essence/delete", essenceController, "delete");
        Router.post("/essence/edit", essenceController, "update");  
        
// -------------------------------------------     POST -------------------------------------------------------------
        
        Router.post("/essence/add", essenceController, "create");
        Router.post("/essence/delete", essenceController, "delete");
        Router.post("/essence/edit", essenceController, "update");   
        
         
        
        Router.get("/billonnage", billonnageController, "index");
        Router.get("/billonnage/fiche", billonnageController, "fiche");
        Router.get("/billonnage/add", billonnageController, "add");
        Router.get("/billonnage/edit", billonnageController, "edit");        
        Router.post("/billonnage/add", billonnageController, "create");
        Router.post("/billonnage/delete", billonnageController, "delete");
        Router.post("/billonnage/edit", billonnageController, "update");   
        
        
        Router.get("/billon", billonController, "index");
        Router.get("/billon/add", billonController, "add");
        Router.get("/billon/edit", billonController, "edit");        
        Router.post("/billon/add", billonController, "create");
        Router.post("/billon/delete", billonController, "delete");
        Router.post("/billon/edit", billonController, "update");   
        
        Router.get("/chauffeur/add", new ChauffeurController(), "add");
        Router.post("/chauffeur/add", new ChauffeurController(), "create");
        
        Router.get("/compagnie/add", new CompagnieController(), "add");
        Router.post("/compagnie/add", new CompagnieController(), "create");
        
        Router.get("/navire", new BateauController(), "index");
        Router.get("/navire/add", new BateauController(), "add");
        Router.get("/navire/edit", new BateauController(), "edit");
        Router.post("/navire/add", new BateauController(), "create");
        Router.post("/navire/edit", new BateauController(), "update");
        Router.post("/navire/delete", new BateauController(), "delete");
        
        Router.get("/conteneur", new ConteneurController(), "index");
        Router.get("/conteneur/add", new ConteneurController(), "add");
        Router.get("/conteneur/edit", new ConteneurController(), "edit");
        Router.post("/conteneur/add", new ConteneurController(), "create");
        Router.post("/conteneur/edit", new ConteneurController(), "update");
        Router.post("/conteneur/delete", new ConteneurController(), "delete");
        
        Router.get("/coupe", coupeController, "index");
        Router.get("/coupe/add", coupeController, "add");
        Router.get("/coupe/edit", coupeController, "edit");        
        Router.post("/coupe/add", coupeController, "create");
        Router.post("/coupe/delete", coupeController, "delete");
        Router.post("/coupe/edit", coupeController, "update");  
      
        Router.get("/typecoupe", typecoupeController, "index");
        Router.get("/typecoupe/add", typecoupeController, "add");
        Router.get("/typecoupe/edit", typecoupeController, "edit");        
        Router.post("/typecoupe/add", typecoupeController, "create");
        Router.post("/typecoupe/delete", typecoupeController, "delete");
        Router.post("/typecoupe/edit", typecoupeController, "update"); 
        
        
      Router.get("/typecontenaire", typecontenaireController, "index");
        Router.get("/typecontenaire/add", typecontenaireController, "add");
        Router.get("/typecontenaire/edit", typecontenaireController, "edit");        
        Router.post("/typecontenaire/add", typecontenaireController, "create");
        Router.post("/typecontenaire/delete", typecontenaireController, "delete");
        Router.post("/typecontenaire/edit", typecontenaireController, "update"); 
            
        
         Router.get("/tranche", trancheController, "index");
         Router.get("/tranche/fiche", trancheController, "fiche");
        Router.get("/tranche/add", trancheController, "add");
        Router.get("/tranche/edit", trancheController, "edit");        
        Router.post("/tranche/add", trancheController, "create");
        Router.post("/tranche/delete", trancheController, "delete");
        Router.post("/tranche/edit", trancheController, "update");  
        
        
        
         Router.get("/tranche-machine", trancheMachineController, "index");
        Router.get("/tranche-machine/add", trancheMachineController, "add");
        Router.get("/tranche-machine/edit", trancheMachineController, "edit");        
        Router.post("/tranche-machine/add", trancheMachineController, "create");
        Router.post("/tranche-machine/delete", trancheMachineController, "delete");
        Router.post("/tranche-machine/edit", trancheMachineController, "update");  
        
        
        Router.get("/transporteur", new TransporteurController(), "index");
        Router.get("/transporteur/add", new TransporteurController(), "add");
        Router.get("/transporteur/edit", new TransporteurController(), "edit");
        Router.post("/transporteur/add", new TransporteurController(), "create");
        Router.post("/transporteur/edit", new TransporteurController(), "update");
        Router.post("/transporteur/delete", new TransporteurController(), "delete");
        
        
        Router.get("/rebut", rebutController, "index");
        Router.get("/rebut/add", rebutController, "add");
        Router.get("/rebut/edit", rebutController, "edit");
        Router.post("/rebut/add", rebutController, "create");
        Router.post("/rebut/edit", rebutController, "update");
        Router.post("/rebut/delete", rebutController, "delete");
        Router.post("/rebut/set_rebut", rebutController, "setRebut");
        Router.post("/rebut/un_rebut", rebutController, "unRebut");
        
        Router.get("/paquet", paquetController, "index");
        Router.get("/paquet/add", paquetController, "add");
        Router.get("/paquet/edit", paquetController, "edit");
        Router.post("/paquet/add", paquetController, "create");
        Router.post("/paquet/edit", paquetController, "update");
        Router.post("/paquet/delete", paquetController, "delete");
        
        Router.get("/sechoir", sechoirController, "index");
        Router.get("/sechoir/add", sechoirController, "add");
        Router.get("/sechoir/list", sechoirController, "list");
        Router.get("/sechoir/edit", sechoirController, "edit");
        Router.get("/sechoir/detail", sechoirController, "detail");
       Router.post("/sechoir/add", sechoirController, "create");
        Router.post("/sechoir/delete", sechoirController, "delete");
        Router.post("/sechoir/edit", sechoirController, "update");
        
        
         Router.get("/feuille", feuilleController, "index");
        Router.get("/feuille/add", feuilleController, "add");
        Router.get("/feuille/edit", feuilleController, "edit");
        Router.post("/feuille/add", feuilleController, "create");
        Router.post("/feuille/edit", feuilleController, "update");
        Router.post("/feuille/delete", feuilleController, "delete");
        
        
        
        Router.get("/commandes", commandeFournisseurController, "index");
        Router.get("/commande", commandeFournisseurController, "index");
        Router.get("/commande/add", commandeFournisseurController, "add");
        Router.get("/commande/edit", commandeFournisseurController, "edit");        
        Router.post("/commande/add", commandeFournisseurController, "create");
        Router.post("/commande/delete", commandeFournisseurController, "delete");
        Router.post("/commande/edit", commandeFournisseurController, "update");   
        
        Router.get("/commande/fournisseur", commandeFournisseurController, "listForFournisseuur");
        
        
         Router.get("/massicot", massicotController, "index");
        Router.get("/massicot/add", massicotController, "add");
        Router.get("/massicot/edit", massicotController, "edit");
        Router.post("/massicot/add", massicotController, "create");
        Router.post("/massicot/delete", massicotController, "delete");
        Router.post("/massicot/edit", massicotController, "update");
      
        Router.get("/feuille", feuilleController, "index");
        Router.get("/feuille/add", feuilleController, "add");
        Router.get("/feuille/edit", feuilleController, "edit");
        Router.post("/feuille/add", feuilleController, "create");
        Router.post("/feuille/delete", feuilleController, "delete");
        Router.post("/feuille/edit", feuilleController, "update");
        
        
        Router.get("/lettrecamion", lettrecamionController, "index");
        Router.get("/lettrecamion/add", lettrecamionController, "add");
        Router.get("/lettrecamion/edit", lettrecamionController, "edit");
        Router.post("/lettrecamion/add", lettrecamionController, "create");
        Router.post("/lettrecamion/delete", lettrecamionController, "delete");
        Router.post("/lettrecamion/edit", lettrecamionController, "update");
        
        
        Router.get("/parcchargement", parcchargementController, "index");
        Router.get("/parcchargement/add", parcchargementController, "add");
        Router.get("/parcchargement/edit", parcchargementController, "edit");
        Router.post("/parcchargement/add", parcchargementController, "create");
        Router.post("/parcchargement/delete", parcchargementController, "delete");
        Router.post("/parcchargement/edit", parcchargementController, "update");
        
        
        Router.get("/plot", plotController, "index");
        Router.get("/plot/add", plotController, "add");
        Router.get("/plot/edit", plotController, "edit");
        Router.post("/plot/add", plotController, "create");
        Router.post("/plot/delete", plotController, "delete");
        Router.post("/plot/edit", plotController, "update");
        
        
        //Router.get("/massicotpaquet", massicotpaquetController, "index");
        Router.get("/massicotpaquet", massicotpaquetController, "massicotage");
        Router.get("/massicotpaquet/massicotage", massicotpaquetController, "massicotage");
        Router.get("/massicotage", massicotpaquetController, "massicotage");
        Router.get("/massicotage/fiche", massicotpaquetController, "ficheProduction");
        Router.get("/massicotpaquet/add", massicotpaquetController, "add");
        Router.get("/massicotpaquet/edit", massicotpaquetController, "edit");
        Router.post("/massicotpaquet/add", massicotpaquetController, "create");
        Router.post("/massicotpaquet/delete", massicotpaquetController, "delete");
        Router.post("/massicotpaquet/edit", massicotpaquetController, "update");
        Router.post("/massicot_paquet/update_lock", massicotpaquetController, "updateLockPaquet");
        Router.post("/massicot_paquet/add/massicotage", massicotpaquetController, "buildColis");
        
        
        
         Router.get("/detailequipe", detailequipeController, "index");
        Router.get("/detailequipe/add", detailequipeController, "add");
        Router.get("/detailequipe/edit", detailequipeController, "edit");
        Router.post("/detailequipe/add", detailequipeController, "create");
        Router.post("/detailequipe/delete", detailequipeController, "delete");
        Router.post("/detailequipe/edit", detailequipeController, "update");
        
        Router.get("/reception/bon", receptionController, "bonReception");
        Router.get("/reception/livree", receptionController, "receptionLivree");
        Router.post("/reception/bon", receptionController, "buildBonReception");
        Router.get("/reception", receptionController, "index");
        Router.post("/reception/add", receptionController, "create");
        
        Router.post("/detail_reception/init_travail", new DetailReceptionController(), "setNumTravailForBille");
        Router.post("/detail_reception/go_back_fournisseur", new DetailReceptionController(), "setRetourFournisseurForBille");
        
        Router.post("/upload/lettre-camion", receptionController, "uploadFile");

        Router.get("/comptabilite", new app.controllers.StockController(), "index");
        Router.get("/comptabilite/fiche_rendement", new app.controllers.StockController(), "ficheRendement");
        
        
        Api.registerRouter();
        
        UserService.registerService();
    }
}
