/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.routes.services;

import app.modelController.PersonnelJpaController;
import app.models.Database;
import app.models.Personnel;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.FutureTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.websocket.Session;
import org.json.JSONException;
import org.json.JSONObject;


/**
 *
 * @author champlain
 */
@ApplicationScoped
class UserSessionHandler {
    
    protected static Set<Session> sessions;
    
    protected static HashMap<String, JSONObject> connections;

    public UserSessionHandler() {
        
        sessions = new HashSet<>();
        
        connections = new HashMap();
        
    }
    
    public void addSession(Session s){
        //sessions.add(s);
        try {
            s.getBasicRemote().sendText("{\"resultat\": \"Ok\", \"message\": \"création réussie\"}");
        } catch (IOException ex) {
            Logger.getLogger(UserSessionHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("\nSession "+s.getId()+" créée");
        System.out.println(connections);
    }
    
    public void removeSession(Session s){
        //sessions.remove(s);
        
        connections.forEach((String key, JSONObject userMap)->{
            try {
                if(s == userMap.get("session")){
                    userMap.put("session", (Object) null);
                }
            } catch (JSONException ex) {
                Logger.getLogger(UserSessionHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    void setSession(String userMatricule, String jsession, String ip, Session session) {
        JSONObject userMap = connections.get(jsession);
        if(userMap == null)
            userMap = new JSONObject();
        try{
            userMap.put("user", userMatricule);
            userMap.put("session", session);
            userMap.put("ip", ip);
            connections.put(jsession, userMap);
        }catch(JSONException e){
            System.out.println("Set Session error = "+e.getMessage());
        }
    }

    String getConnections() {
        return connections.toString();
    }

    boolean logout(String matricule) {
        System.err.println("deconnexion de "+matricule);
        Set<String> jsessions = connections.keySet();
        
        for(String jsession : jsessions){
            System.out.println(jsession);
            try {
                JSONObject userMap = connections.get(jsession);
                if(userMap.getString("user").equals(matricule)){
                    Session s = (Session) userMap.get("session");
                    if(s != null){
                        System.out.println("Deconnexion de "+matricule);
                        PersonnelJpaController contPersonnel = new PersonnelJpaController(Database.getEntityManager());
                        Personnel p = contPersonnel.findByCNI(userMap.getString("user"));
                        p.setStatut(Personnel.OFFLINE);
                        contPersonnel.edit(p);
                        s.getBasicRemote().sendText("{\"command\": \"off\"}");
                        System.err.println("Session destroying command sent");
                        return true;
                    }
                }
            } catch (JSONException ex) {
                System.err.printf("Impossible de faire passer l'utilisateur %s déconnecté 1\n",matricule);
                return false;
            } catch (IOException ex) {
                System.err.printf("Impossible de faire passer l'utilisateur %s déconnecté 2\n",matricule);
                return false;
            } catch (Exception ex) {
                System.err.printf("Impossible de faire passer l'utilisateur %s déconnecté 3\n",matricule);
                return false;
            }
        }
        return false;
    }

    HashMap<String, Object> blockUser(String matricule) {
        Set<String> jsessions = connections.keySet();
        String msg = "";
        try{
            PersonnelJpaController contPersonnel = new PersonnelJpaController(Database.getEntityManager());
            Personnel p = contPersonnel.findByCNI(matricule);
            p.setStatut(Objects.equals(p.getStatut(), Personnel.BLOCKLINE) ? Personnel.OFFLINE : Personnel.BLOCKLINE);
            contPersonnel.edit(p);
            msg = "Utilisateur bloqué";
        }catch(Exception e){
            System.err.println("Impossible de trouver l'utilisateur à bloquer/débloquer");
        }
        for(String jsession : jsessions){
            try {
                JSONObject userMap = connections.get(jsession);
                if(userMap.getString("user").equals(matricule)){
                    Session s = (Session) userMap.get("session");
                    if(s != null){
                        s.getBasicRemote().sendText("{\"command\": \"block\"}");
                        HashMap<String, Object> l = new HashMap<>();
                        l.put("status", Boolean.TRUE);
                        l.put("message", "Utilisateur bloque");
                        return l;
                    }
                }
            } catch (Exception ex) {
                System.err.printf("Impossible d'envoyer la commande de blocage à l'utilisateur %s\n",matricule);
            }
        }
        HashMap<String, Object> l = new HashMap();
        l.put("status", Boolean.TRUE);
        l.put("message", msg.length() == 0 ? "Utilisateur non bloque" : msg);
        return l;
    }
    

    boolean blockUserMe(String matricule) {
        System.err.println("deconnexion de "+matricule);
        Set<String> jsessions = connections.keySet();
        
        for(String jsession : jsessions){
            System.out.println(jsession);
            try {
                JSONObject userMap = connections.get(jsession);
                if(userMap.getString("user").equals(matricule)){
                    Session s = (Session) userMap.get("session");
                    if(s != null){
                        PersonnelJpaController contPersonnel = new PersonnelJpaController(Database.getEntityManager());
                        Personnel p = contPersonnel.findByCNI(userMap.getString("user"));
                        p.setStatut(Personnel.BLOCKLINE);
                        contPersonnel.edit(p);
                        System.err.println("Session destroying command sent");
                    }
                    return true;
                }
            } catch (Exception ex) {
                System.err.printf("Impossible bloquer / débloquer l'utilisateur %s  \n",matricule);
            }
        }
        return false;
    }
    

}
