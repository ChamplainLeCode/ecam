/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.routes.services;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
/**
 *
 * @author champlain
 */
import org.json.JSONException;
import org.json.JSONObject;

@ApplicationScoped
@ServerEndpoint("/user")
public class UserManagement {
    
    public UserManagement(){}
    
    
    //@Inject
    private static UserSessionHandler sessionHandler;
    
    
    public void registerService(){
        
        sessionHandler = new UserSessionHandler();
        System.out.println("-------- UserManager start ---------");
    }
    
    public static boolean logout(String matricule){
        return sessionHandler.logout(matricule);
    }
    
    public static HashMap<String, Object> block(String matricule){
        return sessionHandler.blockUser(matricule);
    }
    
    public static boolean blockMe(String matricule){
        return sessionHandler.blockUserMe(matricule);
    }
    
    @OnOpen
    public void open(Session session) {
        sessionHandler.addSession(session);
    }

    @OnClose
    public void close(Session session) {        
        try{
            sessionHandler.removeSession(session);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @OnError
    public void onError(Throwable error) {
        System.out.println("\nError: "+error.getMessage());
    }

    @OnMessage
    public void handleMessage(String message, Session session) {
        try{
            JSONObject msg = new JSONObject(message);
            if(msg.getString("message").equals("init")){
                new Thread(){
                    public void run(){
                        try {
                            sessionHandler.setSession(msg.getString("user"), msg.getString("key"), msg.getString("ip"), session);
                        } catch (JSONException ex) {
        
                        }
                    }
                }.start();
            }
            try{
                session.getBasicRemote().sendText("{\"status\": \"ok\"}");
            }catch(Exception e){
                System.out.println("Exception "+e.getMessage());
            }
        }catch(JSONException ex){

        }
    }
    
}
