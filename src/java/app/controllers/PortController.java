/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.PortJpaController;
import app.modelController.VilleJpaController;
import app.models.Port;
import app.models.Database;
import app.models.Ville;
import app.routes.Api;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class PortController extends Controller implements IController{
    
    public void paysList(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try {
            PortJpaController cont = new PortJpaController(Database.getEntityManager());
            res.getOutputStream().print(cont.findPortEntities().toString());
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
        
       //resources("json/pays.json", req, res);
    }
    
    public void villeList(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        resources("json/ville.json", req, res);
    }

    @Override
    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res) {
        view("contenaire/port/port-home", req, res);
    }
    public void add(HttpServletRequest req, HttpServletResponse res) {
        view("contenaire/port/port-add", req, res);
    }
    public void edit(HttpServletRequest req, HttpServletResponse res) {
        view("contenaire/port/port-edit", req, res);
    }
    
    
    public void create(HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json");
        try {
            PortJpaController cont = new PortJpaController(Database.getEntityManager());
            Port var = new Port(System.currentTimeMillis());
            var.setRefPort(System.currentTimeMillis());
            var.setNomPort(req.getParameter("nom"));
            var.setNomVille(req.getParameter("ville"));
            var.setNomPays(req.getParameter("pays"));
            new Thread(){public void run(){try {
                cont.create(var);
                } catch (Exception ex) {
                    Logger.getLogger(PortController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }}.start();
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    @Override
    public void data(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json");
        try{
            PortJpaController control = new  PortJpaController(Database.getEntityManager());
            Port b = control.findPort(Long.parseLong(req.getParameter("ref")));
            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            PortJpaController control = new  PortJpaController(Database.getEntityManager());
            List list = control.findPortEntities();
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            PortJpaController control = new  PortJpaController(Database.getEntityManager());
            List list = control.findPortEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    @Override
    public void update(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json");
        try {
            PortJpaController cont = new PortJpaController(Database.getEntityManager());
            Port var = new Port();
            var.setRefPort(Long.parseLong(req.getParameter("ref")));
            var.setNomPort(req.getParameter("nom"));
            var.setNomVille(req.getParameter("ville"));
            var.setNomPays(req.getParameter("pays"));
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    @Override
    public void delete(HttpServletRequest req, HttpServletResponse res) throws IOException {
            res.setHeader("Content-Type", "Application/json");
        try {
            PortJpaController cont = new PortJpaController(Database.getEntityManager());
            cont.destroy(Long.parseLong(req.getParameter("ref")));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    
}
