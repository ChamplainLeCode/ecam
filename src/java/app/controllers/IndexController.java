/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.ColisJpaController;
import app.modelController.DetailReceptionJpaController;
import app.modelController.PaquetJpaController;
import app.modelController.PersonnelHistoryJpaController;
import app.modelController.PersonnelJpaController;
import app.models.Database;
import app.models.Paquet;
import app.models.Personnel;
import app.models.PersonnelHistory;
import java.io.IOException;
import java.util.Date;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;

/**
 *
 * @author champlain
 */
public class IndexController extends Controller {
    
    public final static String LOGIN_PAGE = "/login";
    public final static String REGISTER_PAGE = "/register";
    
    public void index(HttpServletRequest req, HttpServletResponse res){
        //super.index(req, res);
        start(req, res);
    }
    
    public void start(HttpServletRequest req, HttpServletResponse res) {
        view("authentification/login", req, res);
    } 
    
    public void print(HttpServletRequest req, HttpServletResponse res) {
            view("index", req, res);
    } 
    
    public void welcome(HttpServletRequest req, HttpServletResponse res) {
        req.setAttribute("continue", "true");
        super.index(req, res);
        view("home", req, res);
    }

    public void registrationForm(HttpServletRequest req, HttpServletResponse res) {
        req.setAttribute("continue", "true");
        super.index(req, res);
        view("authentification/registration", req, res);
    }

    public void lockscreen(HttpServletRequest req, HttpServletResponse res) {
        view("authentification/lockscreen", req, res);
    }

    public void verifyAuth(HttpServletRequest req, HttpServletResponse res) {
        verifyAuthentification(req, res);
    }
    
    public static boolean verifyAuthentification(HttpServletRequest req, HttpServletResponse res){
        
        if(req.getParameter("user") == null) return false;
        
        try{
            Personnel p = new PersonnelJpaController(Database.getEntityManager()).findPersonnel(req.getParameter("user"));
            Boolean b = Personnel.accessAllow(p, req.getPathInfo());
            if(b == false){
                res.sendRedirect(LOGIN_PAGE);
                return false;
            }
            System.out.println(p);
            req.setAttribute("privilege", p.getFonction());
            
            //res.getOutputStream().print("{\"state\": true}");
            
        }catch(Exception e){
            try {
                System.out.println("Login exception "+e.getMessage());
                res.sendRedirect(LOGIN_PAGE);
            } catch (IOException ex) {

            }
            return false;
        }
        return true;
    }
    
    public void connexion(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            PersonnelJpaController cont = new PersonnelJpaController(Database.getEntityManager());
            PersonnelHistoryJpaController contPH = new PersonnelHistoryJpaController(Database.getEntityManager());
            Personnel p = cont.connexion(req.getParameter("matricule"), req.getParameter("password"));
            String ip = req.getParameter("ip");
            
            
            
            if(p == null){
                res.getOutputStream().print("{"
                        + "\"resultat\": false,"
                        + "\"reason\": \"Matricule ou mot de passe incorrect\""
                        + "}");
            }else{
                if(Objects.equals(p.getStatut(), Personnel.BLOCKLINE)){
                    res.getOutputStream().print("{"
                        + "\"resultat\": false,"
                        + "\"reason\": \"Utilisateur bloqué, Contactez l'administrateur\""
                        + "}");
                    return;
                }
                
                if(p.getFonction() != null)
                    p.getFonction().loadRoute();
                res.getOutputStream().print("{"
                        + "\"resultat\": true, "
                        + "\"user\": "+p+"}");
                p.setStatut(Personnel.ONLINE);
                cont.edit(p);
                PersonnelHistory ph = new PersonnelHistory(new Date());
                ph.setAction("Connexion");
                ph.setIp(ip);
                ph.setMatricule(p.getCniPersonnel());
                contPH.create(ph);
                
            }
        }catch(java.sql.SQLRecoverableException e){
            try {
                res.getOutputStream().print("{"
                        + "\"resultat\": false, "
                        + "\"reason\": \"Problème de connexion réseau\"}");
                //e.printStackTrace(new PrintStream(res.getOutputStream()));
            } catch (IOException ex) {

            }
        }catch(Exception e){
            try {
                res.getOutputStream().print("{"
                        + "\"resultat\": false, "
                        + "\"reason\": \"Problème de connexion\"}");
                //e.printStackTrace(new PrintStream(res.getOutputStream()));
            } catch (IOException ex) {

            }
        }
    }
    
    public void getCertificationByColis(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        
        try {
            DetailReceptionJpaController control = new  DetailReceptionJpaController(Database.getEntityManager());
            Paquet pt = new PaquetJpaController(Database.getEntityManager()).getPaquetInColis(new ColisJpaController(Database.getEntityManager()).findColis(req.getParameter("ref")));
            String certification = null;
            if(pt != null)
                certification = control.getCertificationForNumTravail(pt.getNumTravail());
            if(certification != null)
                res.getOutputStream().print("{\"certification\": \""+certification+"\"}");
            else
                res.getOutputStream().print(null);
        }catch(Exception ex){try {
            ex.printStackTrace();
            res.getOutputStream().print(null);
            } catch (IOException ex1) {}
        }
    }
    
    public static String getColisCertification(String refColis){
        try {
            DetailReceptionJpaController control = new  DetailReceptionJpaController(Database.getEntityManager());
            Paquet pt = new PaquetJpaController(Database.getEntityManager()).getPaquetInColis(new ColisJpaController(Database.getEntityManager()).findColis(refColis));
            String certification = null;
            if(pt != null)
                certification = control.getCertificationForNumTravail(pt.getNumTravail());
            return certification != null ? certification : "";
        }catch(Exception ex){
            return "";
        }
    }
    
    
    public void getCertificationBy(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        
        try {
            DetailReceptionJpaController control = new  DetailReceptionJpaController(Database.getEntityManager());
            String certification = control.getCertificationForNumTravail(req.getParameter("ref"));
            if(certification != null)
                res.getOutputStream().print("{\"certification\": \""+certification+"\"}");
            else
                res.getOutputStream().print(null);
        }catch(Exception ex){try {
            ex.printStackTrace();
            res.getOutputStream().print(null);
            } catch (IOException ex1) {}
        }
    }
    
    
    public void checkTravail(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try {
            DetailReceptionJpaController control = new  DetailReceptionJpaController(Database.getEntityManager());
            Object dr = control.checkTravail(req.getParameter("travail"));
            System.out.println("obje = "+dr);
            if(dr != null)
                res.getOutputStream().print("{\"resultat\": true}");
            else
                res.getOutputStream().print("{\"resultat\": false}");
        }catch(Exception ex){try {
            res.getOutputStream().print(null);
            } catch (IOException ex1) {}
        }
    }
    
}
