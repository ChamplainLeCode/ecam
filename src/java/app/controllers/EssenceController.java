/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.ContennaireJpaController;
import app.modelController.DetailLvJpaController;
import app.modelController.EssenceJpaController;
import app.modelController.PaquetJpaController;
import app.models.Database;
import app.models.DetailLv;
import app.models.Embarquement;
import app.models.Essence;
import app.routes.Api;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 *
 * @author champlain
 */
public class EssenceController extends Controller  implements IController{
    
    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
            view("essence/essence-home", req, res);
    }
    
    public void essenceStock(HttpServletRequest req, HttpServletResponse res){
        view("contenaire/specification", req, res);
    }
    
    
    public void add(HttpServletRequest req, HttpServletResponse res){
        
        view("essence/essence-add", req, res);
    }
    
    public void edit(HttpServletRequest req, HttpServletResponse res){
        String ref = req.getParameter("ref");
        req.setAttribute("essence", null);
        Essence e = new EssenceJpaController(Database.getEntityManager()).findEssence(ref);
        if(e != null){
            req.setAttribute("essence", e);
        }
        view("essence/essence-edit", req, res);
    }
    
    public void list(HttpServletRequest req, HttpServletResponse res) throws IOException{
        List<Essence> liste = new LinkedList<>();
        if(req.getAttribute("liste") == null){
            try{
                liste = new EssenceJpaController(Database.getEntityManager()).findEssenceEntities();
                //System.out.println("Data "+liste);
            }finally{
                req.setAttribute("liste", liste);
                view("essence/essence-list", req, res);
            }
            return;
        }

        view("essence/essence-list", req, res);
    }
    
    
    
// ------------------------------------- POST -----------------------------------------
    public void delete(HttpServletRequest req, HttpServletResponse res){
        String resultat = "";
        try{
            new EssenceJpaController(Database.getEntityManager()).destroy(req.getParameter("ref"));
            resultat = "{\"resultat\": \"true\"}";
        } catch (Exception ex) {
            resultat = "{\"resultat\": \"false\"}";
        }finally{
            try {
                res.getOutputStream().println(resultat);
            } catch (IOException ex) {}
        }
    }
    
    public void create(HttpServletRequest req, HttpServletResponse res) {
        String resultat = "";
        try{
            Essence essence = new Essence();
            essence.setLibelle(req.getParameter("libelle"));
            essence.setDensite(Double.parseDouble(req.getParameter("densite")));
            essence.setRefEssence(req.getParameter("ref"));

            new EssenceJpaController(Database.getEntityManager()).create(essence);
            resultat = "{\"resultat\": \"true\", \"ref\": \""+essence.getRefEssence()+"\"}";
        } catch (Exception ex) {
            System.out.println(ex);
            resultat = "{\"resultat\": \"false\"}";
        }finally{
            try{
                res.getOutputStream().write(resultat.getBytes());
            } catch (IOException ex) {

            }finally{}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res) {
        String resultat = "";
        try{
            Essence essence = new EssenceJpaController(Database.getEntityManager()).findEssence(req.getParameter("ref"));
            essence.setLibelle(req.getParameter("libelle"));
            essence.setDensite(Double.parseDouble(req.getParameter("densite")));

            new EssenceJpaController(Database.getEntityManager()).edit(essence);
            resultat = "{\"resultat\": \"true\", \"ref\": \""+essence.getRefEssence()+"\"}";
        } catch (Exception ex) {
            System.out.println(ex);
            resultat = "{\"resultat\": \"false\"}";
        }finally{
            try{
                res.getOutputStream().write(resultat.getBytes());
            } catch (IOException ex) {

            }finally{}
        }
    }

    
    public void essenceCheck (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            EssenceJpaController control = new  EssenceJpaController(Database.getEntityManager());
            DetailLvJpaController controlLV = new DetailLvJpaController(Database.getEntityManager());
            
            /**
             * Retourne un tableau d'objets JSON
             */
            Collection<Object> lo = control.getEssenceByEpoach(req.getParameter("dd"), req.getParameter("df"));

            lo.forEach((o) -> {
                JSONObject jo = (JSONObject) o;
                try {
                    JSONArray li = jo.getJSONArray("numBille");
                    double volume = 0;
                    for(int i=0; i<li.length(); i++){
                        DetailLv lv = controlLV.findDetailLvByNumBille(li.getString(i));
                        double vol;
                        volume += vol = ((lv.getGdDiam()+lv.getPttDiam())/2)*lv.getLongueur();
                        lv.setVolume(vol);
                        controlLV.edit(lv);
                    }
                    jo.put("volume", volume/1000000); // on divise par 1'000'000 parce que c'est stocké en cm donc le volume devient en cm3 
                    jo.put("parc", PaquetJpaController.getVolumeFromPaquetByEssenceAndDate(jo.getString("libelle"), req.getParameter("dd"), req.getParameter("df")));
//                    jo.put("parc", ContennaireJpaController.getVolumeFromContennaireByEssenceAndDate(jo.getString("libelle"), req.getParameter("dd"), req.getParameter("df")));
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            });
            res.getOutputStream().print(lo.toString());
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            ex.printStackTrace();
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            EssenceJpaController control = new  EssenceJpaController(Database.getEntityManager());
            List list = control.findEssenceEntities();
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[d]");
            ex.printStackTrace(new PrintStream(res.getOutputStream()));
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }

    }
    
    public void dataBaseList (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            EssenceJpaController control = new  EssenceJpaController(Database.getEntityManager());
            List<Essence> list = control.findEssenceEntities();
    
            if(list != null && list.size() >0){
                List<String> baseList = new LinkedList<>();
                list.forEach((e)->baseList.add(e.toBaseModel()));
                res.getOutputStream().print(baseList.toString());
            }
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            System.err.println(ex);
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }

    }
    
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            EssenceJpaController control = new  EssenceJpaController(Database.getEntityManager());
            List list = control.findEssenceEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }


    
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            EssenceJpaController control = new  EssenceJpaController(Database.getEntityManager());
            Essence b = control.findEssence(req.getParameter("ref"));

            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
}
