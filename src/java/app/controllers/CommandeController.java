/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.CommandeJpaController;
import app.modelController.DetailCommandeJpaController;
import app.modelController.EssenceJpaController;
import app.modelController.FournisseurJpaController;
import app.modelController.ParcChargementJpaController;
import app.modelController.ReceptionJpaController;
import app.modelController.exceptions.NonexistentEntityException;
import app.models.Commande;
import app.models.Database;
import app.models.DetailCommande;
import app.models.Fournisseur;
import app.models.Reception;
import app.routes.Api;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class CommandeController extends Controller implements IController{
    
    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        view("commande-fournisseur/commande-home", req, res);
    }
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("commande-fournisseur/commande-add", req, res);
    }
    public void edit(HttpServletRequest req, HttpServletResponse res){
        view("commande-fournisseur/commande-edit", req, res);
    }
    public void list(HttpServletRequest req, HttpServletResponse res){
        view("commande-fournisseur/commande-list", req, res);
    }
    
    public void listForFournisseuur(HttpServletRequest req, HttpServletResponse res){
        List<Commande> liste = new LinkedList<>();
        try{
            String ref = req.getAttribute("ref").toString();
            Fournisseur fournisseur = new FournisseurJpaController(Database.getEntityManager()).findFournisseur(ref);
            if(fournisseur != null)
                liste = fournisseur.getCommandeList();
            
        }finally{
            req.setAttribute("liste", liste);
            list(req, res);
        }
    }

    
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json");
        EntityManagerFactory em = Database.getEntityManager();
        try {
            CommandeJpaController cont = new CommandeJpaController(em);
            ReceptionJpaController rcont = new ReceptionJpaController(em);
            
            /**
             * Création de la commande
             */
            Commande var = new Commande(req.getParameter("ref"));
            var.setDateCommande(new Date());//new Date(Long.parseLong(req.getParameter("date"))));
            var.setRefFournisseur(new FournisseurJpaController(Database.getEntityManager()).findFournisseur(req.getParameter("fournisseur")));
            var.setRefParc(new ParcChargementJpaController(em).findParcChargement(req.getParameter("parc")));
            var.setTypePaie(req.getParameter("typePaiement"), req.getParameter("prixTransport"));
            cont.create(var);
            
            // Récupération du nombre de détail de la commande.
            int tailleDetail = Integer.parseInt(req.getParameter("taille"));
            List<DetailCommande> listeDetail = new LinkedList<>();
            DetailCommandeJpaController contDCommande = new DetailCommandeJpaController(Database.getEntityManager());
            
            // Initialisation de la liste de détails
            for(int i=0; i<tailleDetail; i++){
                /**
                 * Creation du détail de la commande
                 */
                DetailCommande dcommande = new DetailCommande("DCOM_"+var.getRefCommande()+"/"+i); 
                    dcommande.setVolume(Float.parseFloat(req.getParameter("detail["+i+"][volume]")));
                    dcommande.setGdDiametre(Float.parseFloat(req.getParameter("detail["+i+"][gdDiam]")));
                    dcommande.setPtDiametre(Float.parseFloat(req.getParameter("detail["+i+"][pttDiam]")));
                    dcommande.setMyDiamtre(Float.parseFloat(req.getParameter("detail["+i+"][moyDiam]")));
                    dcommande.setNumBille(req.getParameter("detail["+i+"][bille]"));
                    dcommande.setObservation(req.getParameter("detail["+i+"][observation]"));
                    dcommande.setLongueur(Float.parseFloat(req.getParameter("detail["+i+"][longueur]")));
                    dcommande.setRefEssence(new EssenceJpaController(Database.getEntityManager()).findEssence(req.getParameter("detail["+i+"][essence]")));
                    dcommande.setRefCommande(var);
                    dcommande.setPrixUnitaire(Float.parseFloat(req.getParameter("detail["+i+"][prix]")));
                // Sauvegarde du détail
                contDCommande.create(dcommande);
                
                    /**
                     * Creation des listes de détails de reception et commande
                     */
                    listeDetail.add(dcommande);
            }
            // Ajoute de la liste à la commande
            var.setDetailCommandeList(listeDetail);
            
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
                ex.printStackTrace();
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    /**
     *  Api permettant d'appliquer les modifications apportées sur une commande;
     * La modification d'une commande implique la modification exclusive des détails de la commande et de la reception
     * <ul>
     *      <li> Suppression des anciens DetailCommande</li>
     *      <li> Suppression des anciens DetailReception</li>
     *      <li> Creation de nouveaux DetailCommande</li>
     *      <li> Creation de nouveaux DetailReception</li>
     * </ul>
     * 
     * <b>Il est à noter que les reférences de DetailCommande sont au format DCOM_NUMCommande/IndiceDePassage</b>
     * <b>Il est à noter que les reférences sont des suites de chiffres</b>
     * 
     * <b>Il est à noter que les reférences de DetailReception sont au format DREC_NUMCommande/IndiceReception</b>
     * <b>Il est à noter que les reférences de Reception sont au format REC_NUMCommande</b>
     * @param req instance of server request
     * @param res instance of server response
     */
    @Override
    public void update(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try {
            CommandeJpaController cont = new CommandeJpaController(Database.getEntityManager());

            DetailCommandeJpaController contDCommande = new DetailCommandeJpaController(Database.getEntityManager());

            Commande var = cont.findCommande(req.getParameter("ref"));
            Reception reception = new ReceptionJpaController(Database.getEntityManager()).findReception(var.getRefCommande());

            //cont.edit(var);
            
            /**
             * Suppression des anciens détails de la commande et mise à jour des nouvelles
             */
                List<DetailCommande> ldetail = DetailCommandeJpaController.findDetailCommandeByCommande(var.getRefCommande());
                if(ldetail != null && ldetail.size()>0){
                    ldetail.forEach(e->{
                        try {contDCommande.destroy(e.getRefDetail());} catch (NonexistentEntityException ex) {
                            ex.printStackTrace();
                        }
                    });
                }
            
            // Récupération du nombre de détail de la commande.
            int tailleDetail = Integer.parseInt(req.getParameter("taille"));
            List<DetailCommande> listeDetail = new LinkedList<>();
            // On procède à la création de la liste de détails de reception et commande
            for(int i=0; i<tailleDetail; i++){
                /**
                 * Ici on tente de récupérer le detail de la commande dans la base de données en filtrant par le N° de la bille(valeur unique)
                 * Si la bille n'est pas trouvée (Valeur de retour null)  on crée une autre detail de commande
                 */
                DetailCommande dcommande = DetailCommandeJpaController.findDetailCommandeByBille(req.getParameter("detail["+i+"][bille]"));
                if(dcommande == null)
                    dcommande = new DetailCommande("DCOM_"+var.getRefCommande()+"/"+i); 
                dcommande.setVolume(Float.parseFloat(req.getParameter("detail["+i+"][volume]")));
                dcommande.setGdDiametre(Float.parseFloat(req.getParameter("detail["+i+"][gdDiam]")));
                dcommande.setPtDiametre(Float.parseFloat(req.getParameter("detail["+i+"][pttDiam]")));
                dcommande.setMyDiamtre(Float.parseFloat(req.getParameter("detail["+i+"][moyDiam]")));
                dcommande.setNumBille(req.getParameter("detail["+i+"][bille]"));
                dcommande.setObservation(req.getParameter("detail["+i+"][observation]"));
                dcommande.setLongueur(Float.parseFloat(req.getParameter("detail["+i+"][longueur]")));
                dcommande.setRefEssence(new EssenceJpaController(Database.getEntityManager()).findEssence(req.getParameter("detail["+i+"][essence]")));
                dcommande.setRefCommande(var);
                // Sauvegarde du détail
                contDCommande.create(dcommande);
                
                listeDetail.add(dcommande);
            }
            // Ajoute de la liste à la commande
            var.setDetailCommandeList(listeDetail);
            
            
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try {
            CommandeJpaController cont = new CommandeJpaController(Database.getEntityManager());
            DetailCommandeJpaController contDCommande = new DetailCommandeJpaController(Database.getEntityManager());
            Commande var = cont.findCommande(req.getParameter("ref"));
            
            List<DetailCommande> ldetail = DetailCommandeJpaController.findDetailCommandeByCommande(var.getRefCommande());
            ldetail.forEach(e->{
                try {contDCommande.destroy(e.getRefDetail());} catch (NonexistentEntityException ex) {}
            });

            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }


    
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            CommandeJpaController control = new  CommandeJpaController(Database.getEntityManager());
            Commande b = control.findCommande(req.getParameter("ref"));

            if(b != null){
                b.setDetailCommandeList(DetailCommandeJpaController.findDetailCommandeByCommande(b.getRefCommande()));
                res.getOutputStream().print(b.toString());
            }else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    public void refList(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        List<Commande> liste = CommandeJpaController.findCommandeRefByFounisseur(req.getParameter("ref"));
        
        try{
            if(liste != null & liste.size() > 0)
                res.getOutputStream().print(new JSONArray(liste).toString());
            else
                res.getOutputStream().print("[]");
        } catch (IOException ex) {
            try {
                res.getOutputStream().print("[]");
            } catch (IOException ex1) {}
        }
    }
    
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        
/**        List<Commande> list = new LinkedList<>();
        Commande cmd;
        List<DetailCommande> liste;
        
        for(int i=0; i<20; i++){
            liste = new LinkedList<>();
            cmd = new Commande("00000"+i+(i*i)+' '+i);
            cmd.setDateCommande(new Date());
            cmd.setDetailCommandeList(liste);
            Fournisseur f;
            cmd.setRefFournisseur(f = new Fournisseur("REF_Four_"+i+' '+i+' '+i));
            f.setCertificat("CERTif_0000"+i);
            f.setContact("698-1281-55");
            f.setContribuable("Contri_"+i);
            f.setNomFournisseur("Bakop N. Champlain");
            for(int j=0; j<i+1; j++){
                DetailCommande dcmd = new DetailCommande("REF_"+j+i, j*i*2f, "00000"+i+j, 50f*j, 40f*j, (50f*j+40f*j)/2, "B+"+j);
                dcmd.setRefCommande(cmd);
                
                    Essence e = new Essence("REF_"+i+"0"+i); 
                    e.setDensite(Double.parseDouble(i+"00"+i));
                    e.setLibelle("Bibiga_"+i);
                    e.setPrixUnitaireAchat(0x0030L*i);
                    e.setPrixUnitaireVente(0x0400L*i);
            
                dcmd.setRefEssence(e);
                liste.add(dcmd);
            }
            list.add(cmd);
        }
        try{
            res.getOutputStream().print(list.toString());
        }catch(Exception ee){
            try {
                ee.printStackTrace(new PrintStream(res.getOutputStream()));
            } catch (IOException ex) {
                
            }
        }*/
        
        
            try{
                CommandeJpaController control = new  CommandeJpaController(Database.getEntityManager());
                List<Commande> list = control.findCommandeEntities();
                if(list != null && list.size() >0){
                list.forEach((c) -> {
                c.setDetailCommandeList(DetailCommandeJpaController.findDetailCommandeByCommande(c.getRefCommande()));
                });
                res.getOutputStream().print(list.toString());
                }else
                res.getOutputStream().print("[]");
            }catch(Exception ex){try {
                    ex.printStackTrace(new PrintStream(res.getOutputStream()));
                    res.getOutputStream().print("[]");
                } catch (IOException ex1) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
                }
            }
    }
    
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            CommandeJpaController control = new  CommandeJpaController(Database.getEntityManager());
            List<Commande> list = control.findCommandeEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);

            if(list != null && list.size() >0){
                list.forEach((c) -> {
                    c.setDetailCommandeList(DetailCommandeJpaController.findDetailCommandeByCommande(c.getRefCommande()));
                });
;
                res.getOutputStream().print(list.toString());
            }else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    public void findCommandeByFounisseur(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try {
            res.getOutputStream().print(CommandeJpaController.findCommandeByFounisseur(req.getParameter("ref")).toString());
        } catch (IOException ex) {
            try {
                res.getOutputStream().print("[]");
            } catch (IOException ex1) {}
        }
    }
    
}
