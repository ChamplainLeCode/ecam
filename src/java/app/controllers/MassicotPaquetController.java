/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.ColisJpaController;
import app.modelController.EssenceJpaController;
import app.modelController.MassicotJpaController;
import app.modelController.MassicotpaquetJpaController;
import app.modelController.PaquetJpaController;
import app.modelController.PlotJpaController;
import app.modelController.exceptions.NonexistentEntityException;
import app.models.Colis;
import app.models.Database;
import app.models.Massicotpaquet;
import app.models.Paquet;
import app.routes.Api;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import javax.servlet.ServletOutputStream;
/**
 *
 * @author champlain
 */

public class MassicotPaquetController extends Controller implements IController{
    
     
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        view("massicotpaquet/massicotage-new", req, res);
//        view("massicotpaquet/massicotpaquet-home", req, res);
    }
    
    public void edit(HttpServletRequest req, HttpServletResponse res){
        view("massicotpaquet/massicotpaquet-edit", req, res);
    }
    
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("massicotpaquet/massicotpaquet-add", req, res);
    }

    public void massicotage(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
        view("massicotpaquet/massicotage", req, res);
    }

    public void ficheProduction(HttpServletRequest req, HttpServletResponse res){
        view("massicotpaquet/fiche_production", req, res);
    }
    
    public void getDataforMassicotFiche(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            res.getOutputStream().print(new MassicotpaquetJpaController(Database.getEntityManager()).getDataforMassicotFiche(new Date(Long.parseLong(req.getParameter("date")))));
        }catch(IOException | NumberFormatException e){
            try {
                e.printStackTrace();
                res.getOutputStream().print("[]");
            } catch (IOException ex) {}
        }
    }

    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json");            
    }
    
    public void buildColis(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json");
         
                try {
                    int NumPaquet = 1;
                    ColisJpaController contColis = new ColisJpaController(Database.getEntityManager());
                    PaquetJpaController contPaquet = new PaquetJpaController(Database.getEntityManager());
                    Map<String, String[]> map = req.getParameterMap();
                    int n = Integer.parseInt(map.get("taille")[0]);
                    int year = Calendar.getInstance().get(Calendar.YEAR);
                    String atelier = map.get("atelier")[0];
                    int nbrColisForThisYear = contColis.getcolisCountForThisYearByAtelier(year, atelier);


                    // nombre de colis
                    for(int compteur = 0; compteur<n; compteur++){
                        
                        Colis colis;
                        double volume = 0;
                        Colis b = new ColisJpaController(Database.getEntityManager()).findColis(year+"-"+atelier+""+(nbrColisForThisYear+1+compteur));
                        System.out.println("b = >'"+b+"'");
                        if(b == null && map.get("data["+compteur+"][colis][ref]")[0].equals("ref")){
                            colis = new Colis();
                            colis.setDateDebut(new Date());
                            colis.setQualite(map.get("data["+compteur+"][colis][qualite]")[0]);
                            colis.setLargeurColis(Double.parseDouble(map.get("data["+compteur+"][colis][largeur]")[0]));
                            colis.setLongueurColis(Double.parseDouble(map.get("data["+compteur+"][colis][longueur]")[0]));
                            colis.setNbrePaquet(Integer.parseInt(map.get("data["+compteur+"][paquets][taille]")[0]));
                            colis.setSurfaceColis(Double.parseDouble(map.get("data["+compteur+"][colis][surface]")[0]));
//                            colis.setRefEquipe(new EquipeJpaController(Database.getEntityManager()).findEquipe(map.get("data["+compteur+"][colis][equipe]")[0]));
                            colis.setRefColis(year+"-"+atelier+""+(nbrColisForThisYear+1+compteur));
                            colis.setEssence(new EssenceJpaController(Database.getEntityManager()).findEssence(map.get("data["+compteur+"][colis][essence]")[0]).getLibelle());
                            colis.setCodeBarColis(colis.getRefColis());
                            colis.setEpaisseur(Paquet.convertEpaisseur(map.get("data["+compteur+"][colis][epaisseur]")[0]));
                            contColis.create(colis);
                        }else{
                            colis = contColis.findColis(map.get("data["+compteur+"][colis][ref]")[0]);
                            if(colis == null){
                                //compteur--;
                                continue;
                            }
                            colis.setEssence(new EssenceJpaController(Database.getEntityManager()).findEssence(map.get("data["+compteur+"][colis][essence]")[0]).getLibelle());
                            colis.setNbrePaquet(colis.getNbrePaquet()+Integer.parseInt(map.get("data["+compteur+"][paquets][taille]")[0]));
                            colis.setSurfaceColis(Double.parseDouble(map.get("data["+compteur+"][colis][surface]")[0]));
//                            colis.setRefEquipe(new EquipeJpaController(Database.getEntityManager()).findEquipe(map.get("data["+compteur+"][colis][equipe]")[0]));
                            colis.setQualite(map.get("data["+compteur+"][colis][qualite]")[0]);
                            colis.setEpaisseur(Paquet.convertEpaisseur(map.get("data["+compteur+"][colis][epaisseur]")[0]));
//                            colis.setEpaisseur(Paquet.convertEpaisseur(map.get("data["+compteur+"][colis][epaisseur]")[0]));
                            contColis.edit(colis);
                            volume = colis.getVoulumeColis();
                            NumPaquet = colis.getNbrePaquet();
                        }
                        int nbrPaquets = Integer.parseInt(req.getParameter("data["+compteur+"][paquets][taille]"));
                        List<Paquet> listePaquets = new LinkedList<>();
                        for(int j=0; j<nbrPaquets; j++){
                            Paquet p;
                            if((p = contPaquet.findPaquet(
                                //map.get("data["+compteur+"][colis][travail]")[0]+"-"+
                                map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[0]+"-"+
                                map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[1]+"-"+
                                map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[2]+"-"+
                                (Integer.parseInt(map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[3])+NumPaquet)+"")) == null
                            ){
                                p = new Paquet();
                                System.out.println("0 = "+map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[0]);
                                System.out.println("1 = "+map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[1]);
                                System.out.println("2 = "+map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[2]);
                                p.setRefPaquet(//p.getNumTravail()+"-"+
                                    map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[0]+"-"+
                                    map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[1]+"-"+
                                    map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[2]+"-"+
                                    (Integer.parseInt(map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[3])+NumPaquet)+"");
                                p.setNbreFeuille(Integer.parseInt(map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[4]));
                                p.setNumTravail(map.get("data["+compteur+"][colis][travail]")[0]);
                                p.setQualite(map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[7]);
                                p.setRefCodebar(colis.getCodeBarColis()+"-"+j);

                                p.setRefColis(contColis.findColis(colis.getRefColis()));
                                p.setRefPlot(new PlotJpaController(Database.getEntityManager()).findPlot(
                                    p.getNumTravail()+"-"+
                                        map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[0]+"-"+
                                    map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[0]
                                ));
                                p.setLongueur(Double.parseDouble(map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[5]));
                                p.setLargeur(Double.parseDouble(map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[6]));
                                p.setEpaisseur(map.get("data["+compteur+"][colis][epaisseur]")[0]);
                                contPaquet.create(p);
                                volume += p.getEpaisseur()*Float.parseFloat(map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[8]);
                            }else{
                                    p.setNbreFeuille(Integer.parseInt(map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[4]));
                                p.setNumTravail(map.get("data["+compteur+"][colis][travail]")[0]);
                                p.setQualite(map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[7]);
                                p.setRefCodebar(colis.getCodeBarColis()+"-"+j);
                                p.setRefColis(contColis.findColis(colis.getRefColis()));
                                p.setRefPlot(new PlotJpaController(Database.getEntityManager()).findPlot(
                                    p.getNumTravail()+"-"+
                                        map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[0]+"-"+
                                    map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[0]
                                ));
                                p.setLongueur(Double.parseDouble(map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[5]));
                                p.setLargeur(Double.parseDouble(map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[6]));
                                p.setEpaisseur(map.get("data["+compteur+"][colis][epaisseur]")[0]);
                                contPaquet.edit(p);
                                volume += p.getEpaisseur()*Float.parseFloat(map.get("data["+compteur+"][paquets][list]["+j+"][tab][]")[8]);
                            }
                            listePaquets.add(p);
                        }
                        
                        colis.setVoulumeColis(volume);
                        colis.setPaquetList(listePaquets);
                        contColis.edit(colis);
                    }
                    res.getOutputStream().print("{\"result\":true}");
                } catch (Exception ex) {
                    System.out.println("Massicotage error => "+ex.getMessage());
                    ex.printStackTrace();
                    try {
                        JSONObject obj = new JSONObject();
                        obj.put("resultat", "false");
                        obj.put("reason", ex.getMessage());
                        res.getOutputStream().print(obj.toString());
                        ex.printStackTrace();
                    } catch (JSONException ex1) {
                    
                    }
                }
            
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try {
            MassicotpaquetJpaController cont = new MassicotpaquetJpaController(Database.getEntityManager());
            Massicotpaquet var = cont.findMassicotpaquet(req.getParameter("ref"));
            var.setNumTravail(req.getParameter("numTravail"));
            var.setRefMassicot(new MassicotJpaController(Database.getEntityManager()).findMassicot(req.getParameter("massicot")));
            var.setRefPaquet(new PaquetJpaController(Database.getEntityManager()).findPaquet(req.getParameter("paquet")));
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    /**
     *  Cette fonction permet de supprimer les paquets fabriqués au massicot à partir d'un
     * certain plot
     * @param req instance of server request
     * @param res instance of server response
     */
    @Override
    public void delete(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try {
            MassicotpaquetJpaController cont = new MassicotpaquetJpaController(Database.getEntityManager());
            PaquetJpaController contP = new PaquetJpaController(Database.getEntityManager());
            List<String> list = cont.getMassicotPaquetByPlot(req.getParameter("ref"));
            list.forEach((refMassicotPaquet)-> {try {cont.destroy(refMassicotPaquet); cont.destroyByMassicotPaquet(refMassicotPaquet); } catch (NonexistentEntityException ex) {}});
            //cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }


    @Override
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            MassicotpaquetJpaController control = new  MassicotpaquetJpaController(Database.getEntityManager());
            Massicotpaquet b = control.findMassicotpaquet(req.getParameter("ref"));

            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    @Override
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            MassicotpaquetJpaController control = new  MassicotpaquetJpaController(Database.getEntityManager());
            List list = control.findMassicotpaquetEntities();

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    @Override
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            MassicotpaquetJpaController control = new  MassicotpaquetJpaController(Database.getEntityManager());
            List list = control.findMassicotpaquetEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult+1000);

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void getNumPlotFromMassicot (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            MassicotpaquetJpaController control = new  MassicotpaquetJpaController(Database.getEntityManager());
            String list = control.getNumPlotFromMassicot();
            res.getOutputStream().print(list);
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void getPaquetForValidation (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{

            String plot = req.getParameter("plot"),
            face = req.getParameter("face");
            
            MassicotpaquetJpaController control = new  MassicotpaquetJpaController(Database.getEntityManager());
            String list = control.getPaquetForValidation(plot, face);
            res.getOutputStream().print(list);
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    
    public void updateLockPaquet (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            
            String feuille = req.getParameter("feuille"),
            face = req.getParameter("face"),
            qualite = req.getParameter("qualite"),
            ref = req.getParameter("ref");
            
            MassicotpaquetJpaController control = new  MassicotpaquetJpaController(Database.getEntityManager());
            String list = control.updateLockPaquet(ref, face, feuille, qualite);
            res.getOutputStream().print(list);
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }


}
