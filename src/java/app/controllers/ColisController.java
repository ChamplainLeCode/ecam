/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.ColisJpaController;
import app.models.Colis;
import app.models.Database;
import app.routes.Api;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class ColisController extends Controller implements IController{
    
     
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        view("colis/colis-home", req, res);
    }
    
    public void edit(HttpServletRequest req, HttpServletResponse res){
        view("colis/colis-edit", req, res);
    }
    
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("colis/colis-add", req, res);
    }
    
  
 
    public void create (HttpServletRequest req, HttpServletResponse res) throws IOException {
        try {
            ColisJpaController cont = new ColisJpaController(Database.getEntityManager());
            Colis var = new Colis(req.getParameter("ref"));
            var.setDateDebut(new Date(Long.parseLong(req.getParameter("dateDebut"))));
            var.setDateFermeture(new Date(Long.parseLong(req.getParameter("dateFermeture"))));
            var.setDateFin(new Date(Long.parseLong(req.getParameter("dateFin"))));
            var.setPoidEmballage(Double.parseDouble(req.getParameter("poidsEmballage")));
            var.setPoidNet(Double.parseDouble(req.getParameter("poidsNet")));
            var.setNbrePaquet(Integer.parseInt(req.getParameter("nbrePaquet")));
            var.setLongueurColis(Double.parseDouble(req.getParameter("longueurColis")));
             var.setLargeurColis(Double.parseDouble(req.getParameter("largeurColis")));
             var.setCodeBarColis(req.getParameter("codeBarColis"));
            var.setPoidsColis(Double.parseDouble(req.getParameter("poidsColis")));
            //var.setRefEquipe(new EquipeJpaController(Database.getEntityManager()).findEquipe(req.getParameter("equipeRef")));
            //var.setRefPaquet(new PaquetJpaController(Database.getEntityManager()).findPaquet(req.getParameter("paquetRef")));
            var.setVoulumeColis(Double.parseDouble(req.getParameter("volume")));
            
            cont.create(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        try {
            ColisJpaController cont = new ColisJpaController(Database.getEntityManager());
            Colis var = cont.findColis(req.getParameter("ref"));
            var.setDateDebut(new Date(Long.parseLong(req.getParameter("dateDebut"))));
            var.setDateFermeture(new Date(Long.parseLong(req.getParameter("dateFermeture"))));
            var.setDateFin(new Date(Long.parseLong(req.getParameter("dateFin"))));
            var.setPoidEmballage(Double.parseDouble(req.getParameter("poidsEmballage")));
            var.setPoidNet(Double.parseDouble(req.getParameter("poidsNet")));
            var.setPoidsColis(Double.parseDouble(req.getParameter("poidsColis")));
            //var.setRefEquipe(new EquipeJpaController(Database.getEntityManager()).findEquipe(req.getParameter("equipeRef")));
//            var.setRefPaquet(new PaquetJpaController(Database.getEntityManager()).findPaquet(req.getParameter("paquetRef")));
            var.setVoulumeColis(Double.parseDouble(req.getParameter("volume")));
            
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        try {
            ColisJpaController cont = new ColisJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("refColis"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ColisJpaController control = new  ColisJpaController(Database.getEntityManager());
            Colis b = control.findColis(req.getParameter("ref"));

            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    
    public void groupPaquetByNumber(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        JSONObject obj = new ColisJpaController(Database.getEntityManager()).groupPaquetByNumber(req.getParameter("colis"));
        try {
            res.getOutputStream().print(obj.toString());
        } catch (IOException ex) {
            try {
                res.getOutputStream().print(null);
                System.err.println("Impossible de grouper les paquets par nombre");
            } catch (IOException ex1) {
                System.err.println("Impossible de grouper les paquets par nombre");
            }
        }
    }
    
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            String type = req.getParameter("categorie");
            String dateDebutS = req.getParameter("dateDebut");
            String dateFinS = req.getParameter("dateFin");
            ColisJpaController control = new  ColisJpaController(Database.getEntityManager());
            List<Colis> list;
            long dateDebut, dateFin;
            
            if(dateDebutS == null) dateDebut = 0;
            else dateDebut = Long.parseLong(dateDebutS);
            
            if(dateFinS == null) dateFin = 0;
            else dateFin = Long.parseLong(dateFinS);

            if(type == null || type.equalsIgnoreCase("stock"))
                list = control.findColisEnStock(dateDebut, dateFin);
            else if(type.equalsIgnoreCase("expedie"))
                list = control.findColisExpedies(dateDebut, dateFin);
            else if(type.equalsIgnoreCase("jointage"))
                list = control.findColisJointage(dateDebut, dateFin);
            else if(type.equalsIgnoreCase("ouvert"))
                list = control.findColisOuverts(dateDebut, dateFin);
            else{
                list = control.findColisByOrigin(type, dateDebut, dateFin);
            }
            
            
            
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ColisJpaController control = new  ColisJpaController(Database.getEntityManager());
            List list = control.findColisEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    
    public void getColisOuverts (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ColisJpaController control = new  ColisJpaController(Database.getEntityManager());
            List list = control.getColisOuverts();

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void getColisOuvertsByAtelier (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ColisJpaController control = new  ColisJpaController(Database.getEntityManager());
            List list = control.getColisOuvertsByAtelier(req.getParameter("atelier"));

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void colisForEmpotage (HttpServletRequest req, HttpServletResponse res){
        getColisOuverts(req, res);
    }
    
    
    public void colisForContenaire (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");

        try {
            ColisJpaController control = new  ColisJpaController(Database.getEntityManager());
            List list = control.colisForContenaire();

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void getCertificationColis (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        
        try {
            ColisJpaController control = new  ColisJpaController(Database.getEntityManager());
            System.out.println("ref = "+req.getParameter("ref"));
            String certification = control.getCertificationColis(req.getParameter("ref"));
            
            if(certification != null)
                res.getOutputStream().print("{\"certification\": \""+certification+"\"}");
            else
                res.getOutputStream().print(null);
        }catch(Exception ex){try {
            res.getOutputStream().print(null);
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
}
