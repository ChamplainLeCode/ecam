/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.PaysJpaController;
import app.modelController.VilleJpaController;
import app.models.Pays;
import app.models.Database;
import app.models.Ville;
import app.routes.Api;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class PaysController extends Controller implements IController{
    
    public void paysList(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try {
            PaysJpaController cont = new PaysJpaController(Database.getEntityManager());
            res.getOutputStream().print(cont.findPaysEntities().toString());
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
        
       //resources("json/pays.json", req, res);
    }
    
    public void villeList(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        resources("json/ville.json", req, res);
    }

    @Override
    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res) {
        view("contenaire/pays/pays-home", req, res);
    }
    public void add(HttpServletRequest req, HttpServletResponse res) {
        view("contenaire/pays/pays-add", req, res);
    }
    public void edit(HttpServletRequest req, HttpServletResponse res) {
        view("contenaire/pays/pays-edit", req, res);
    }
    
    
    public void create(HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json");
        try {
            PaysJpaController cont = new PaysJpaController(Database.getEntityManager());
            Pays var = new Pays(System.currentTimeMillis()+"");
            var.setRef(req.getParameter("code"));
            var.setNom(req.getParameter("nom"));
            new Thread(){public void run(){try {
                cont.create(var);
                } catch (Exception ex) {
                    Logger.getLogger(PaysController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }}.start();
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    @Override
    public void data(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json");
        try{
            PaysJpaController control = new  PaysJpaController(Database.getEntityManager());
            Pays b = control.findPays(req.getParameter("ref"));
            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            PaysJpaController control = new  PaysJpaController(Database.getEntityManager());
            List list = control.findPaysEntities();
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            PaysJpaController control = new  PaysJpaController(Database.getEntityManager());
            List list = control.findPaysEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    @Override
    public void update(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json");
        try {
            PaysJpaController cont = new PaysJpaController(Database.getEntityManager());
            Pays var = new Pays(req.getParameter("ref"));
            var.setNom(req.getParameter("libelle"));
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    @Override
    public void delete(HttpServletRequest req, HttpServletResponse res) throws IOException {
            res.setHeader("Content-Type", "Application/json");
        try {
            PaysJpaController cont = new PaysJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("code"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    
}
