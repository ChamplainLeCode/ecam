/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.BillonJpaController;
import app.modelController.CoupeJpaController;
import app.modelController.PlotJpaController;

import app.models.Coupe;
import app.models.Plot;
import app.models.Database;
import app.routes.Api;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class PlotController extends Controller implements IController{
    
    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        view("plot/plot-home", req, res);
    }
    
    public void edit(HttpServletRequest req, HttpServletResponse res){
        view("plot/plot-edit", req, res);
    }
    
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("plot/plot-add", req, res);
    }
    
    

    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json");
        try {
            int taille = Integer.parseInt(req.getParameter("taille"));
            PlotJpaController cont = new PlotJpaController(Database.getEntityManager());
            List<Plot> liste = new LinkedList<>();
            Coupe coupe = new CoupeJpaController(Database.getEntityManager()).findCoupeByBillon(req.getParameter("billon"));
            if(coupe == null){
                coupe = new  Coupe(req.getParameter("billon")+"|"+req.getParameter("taille"));
                coupe.setRefBillon(new BillonJpaController(Database.getEntityManager()).findBillon(req.getParameter("billon")));
                coupe.setNbrePlot(Integer.parseInt(req.getParameter("taille")));
                new CoupeJpaController(Database.getEntityManager()).create(coupe);
            }else{
                coupe.setNbrePlot(coupe.getNbrePlot()+Integer.parseInt(req.getParameter("taille")));
                new CoupeJpaController(Database.getEntityManager()).edit(coupe);
            }
            for(int i=0; i<taille; i++){
                Plot var = new Plot(req.getParameter("data["+i+"][ref]"));
                var.setLongueur(Integer.parseInt(req.getParameter("data["+i+"][longueur]")));
                var.setFace1(0);
                var.setFace2(0);
                var.setFace3(0);
                var.setFaceMN((var.getLongueur() > 5 ? "M" : (var.getLongueur() == 5 ? "N" : "-")));
                var.setRefCoupe(coupe);
                var.setNumTravail(coupe.getRefBillon().getNumTravail());
                var.setRefBillon(coupe.getRefBillon());
                cont.create(var);
            }
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        try {
            PlotJpaController cont = new PlotJpaController(Database.getEntityManager());
            Plot var = cont.findPlot(req.getParameter("ref"));
                var.setFace1(Integer.parseInt(req.getParameter("face1")));
                var.setFace2(Integer.parseInt(req.getParameter("face2")));
                var.setFace3(Integer.parseInt(req.getParameter("face3")));
                var.setFaceMN(req.getParameter("faceMN"));
            var.setRefCoupe(new CoupeJpaController(Database.getEntityManager()).findCoupe(req.getParameter("coupeRef")));
            var.setLongueur(Integer.parseInt(req.getParameter("longueur")));
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        try {
            PlotJpaController cont = new PlotJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    @Override
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            PlotJpaController control = new  PlotJpaController(Database.getEntityManager());
            Plot b = control.findPlot(req.getParameter("ref"));

            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    /**
     *  retourne les plots qui ne sont pas encore passés par la cuve
     * @param req server request instance
     * @param res server response instance
     */
    public void plotforCuve (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            PlotJpaController control = new  PlotJpaController(Database.getEntityManager());
            String list = control.plotforCuve(req.getParameter("nature"));

                res.getOutputStream().print(list);
        }catch(Exception ex){try {
            res.getOutputStream().print("[s]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
        @Override
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            PlotJpaController control = new  PlotJpaController(Database.getEntityManager());
            List list = control.findPlotEntities();

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
        @Override
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            PlotJpaController control = new  PlotJpaController(Database.getEntityManager());
            List list = control.findPlotEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    
    public void getLastFace (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            PlotJpaController control = new  PlotJpaController(Database.getEntityManager());
            String face = control.getLastFaceForBillon(req.getParameter("billon"));

            if(face != null )
                res.getOutputStream().print(face);
            else
                res.getOutputStream().print("{\"face\": null}");
        }catch(Exception ex){try {
            res.getOutputStream().print(ex.getMessage());
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    
    
    public void getPlotForMassicot(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            String type = req.getParameter("type"   );
            String value = req.getParameter("value");
            System.out.println("value = "+value+" type = "+type);
            String face = "";
            if(type.equals("travail")){
                PlotJpaController control = new  PlotJpaController(Database.getEntityManager());
                face = control.getPlotForMassicot(value);
            }else if(type.equals("billon")){
                PlotJpaController control = new  PlotJpaController(Database.getEntityManager());
                face = control.getPlotForMassicotByBillon(value);
            }

            System.out.println("type = travail"+" res = "+face);
            if(face != null )
                res.getOutputStream().print(face);
            else
                res.getOutputStream().print("{\"face\": null}");
        }catch(Exception ex){try {
            res.getOutputStream().print(ex.getMessage());
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    

}
