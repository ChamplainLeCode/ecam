/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.ChauffeurJpaController;
import app.models.Chauffeur;
import app.models.Database;
import app.routes.Api;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class ChauffeurController extends Controller implements IController{

    public void add(HttpServletRequest req, HttpServletResponse res){
        view("chauffeur/chauffeur-add", req, res);
    }
    public void create(HttpServletRequest req, HttpServletResponse res){
                res.setHeader("Content-Type", "Application/json");
        try {
            ChauffeurJpaController cont = new ChauffeurJpaController(Database.getEntityManager());
            Chauffeur var = new Chauffeur(req.getParameter("ref"));
            var.setNom(req.getParameter("nom"));
            var.setPrenom(req.getParameter("prenom"));
            cont.create(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    
    @Override
    public void data(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json");
        try{
            ChauffeurJpaController control = new  ChauffeurJpaController(Database.getEntityManager());
            Chauffeur b = control.findChauffeur(req.getParameter("ref"));
            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            ChauffeurJpaController control = new  ChauffeurJpaController(Database.getEntityManager());
            List list = control.findChauffeurEntities();
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ChauffeurJpaController control = new  ChauffeurJpaController(Database.getEntityManager());
            List list = control.findChauffeurEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }   
    }

    @Override
    public void update(HttpServletRequest req, HttpServletResponse res) throws IOException {

    }

    @Override
    public void delete(HttpServletRequest req, HttpServletResponse res) throws IOException {

    }

    @Override
    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res) {

    }
    
}
