/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.SechoirJpaController;
import app.modelController.SechoirJpaController;
import app.modelController.SechoirJpaController;
import app.modelController.SechoirJpaController;
import app.modelController.PlotJpaController;
import app.models.Database;
import app.models.Sechoir;
import app.models.Sechoir;
import app.models.Sechoir;
import app.routes.Api;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author KOUNOU BESSALA ERIC
 */
public class SechoirController  extends Controller implements IController {
    
     
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        view("sechoir/sechoir-home", req, res);
    }
    
    public void edit(HttpServletRequest req, HttpServletResponse res){
        view("sechoir/sechoir-edit", req, res);
    }
    
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("sechoir/sechoir-add", req, res);
    }
    

  
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        
       try {
            SechoirJpaController cont = new SechoirJpaController(Database.getEntityManager());
            Sechoir var = new Sechoir("REF_SEC_"+System.currentTimeMillis());
            var.setLibelleSechoir(req.getParameter("libelle"));
             cont.create(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    @Override
    public void update(HttpServletRequest req, HttpServletResponse res) throws IOException {
try {
            SechoirJpaController cont = new SechoirJpaController(Database.getEntityManager());
            Sechoir var = cont.findSechoir(req.getParameter("ref"));
            var.setLibelleSechoir(req.getParameter("libelle"));
            
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    @Override
    public void delete(HttpServletRequest req, HttpServletResponse res) throws IOException {
try {
            SechoirJpaController cont = new SechoirJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
     @Override
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            SechoirJpaController control = new  SechoirJpaController(Database.getEntityManager());
            Sechoir b = control.findSechoir(req.getParameter("ref"));

            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    @Override
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            SechoirJpaController control = new  SechoirJpaController(Database.getEntityManager());
            List list = control.findSechoirEntities();

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    @Override
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            SechoirJpaController control = new  SechoirJpaController(Database.getEntityManager());
            List list = control.findSechoirEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

}
