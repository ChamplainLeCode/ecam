/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author champlain
 */
public interface IController {
    
    
    public void data (HttpServletRequest req, HttpServletResponse res) throws IOException;

    public void dataList (HttpServletRequest req, HttpServletResponse res) throws IOException;

    public void dataListPage (HttpServletRequest req, HttpServletResponse res) throws IOException;

    public void create (HttpServletRequest req, HttpServletResponse res) throws IOException;

    public void update (HttpServletRequest req, HttpServletResponse res) throws IOException;

    public void delete (HttpServletRequest req, HttpServletResponse res) throws IOException;

    public void start(HttpServletRequest req, HttpServletResponse res);


}
