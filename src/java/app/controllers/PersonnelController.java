/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.PersonnelHistoryJpaController;
import app.modelController.PersonnelJpaController;
import app.modelController.PrivilegeJpaController;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.Database;
import app.models.Personnel;
import app.models.PersonnelHistory;
import app.routes.Api;
import app.routes.services.UserManagement;
import java.io.IOException;
import java.sql.SQLRecoverableException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class PersonnelController extends Controller  implements IController{
    
    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        view("personnel/personnel-home", req, res);
        System.out.println(req.getSession().getId());
    }
    
    public void edit(HttpServletRequest req, HttpServletResponse res){
        view("personnel/personnel-edit", req, res);
    }
    
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("personnel/personnel-add", req, res);
    }
    
    public void userManagement(HttpServletRequest req, HttpServletResponse res){
        List<PersonnelHistory> liste = new PersonnelHistoryJpaController(Database.getEntityManager()).findPersonnelHistoryByPersonnel(req.getParameter("matricule"));
        req.setAttribute("liste", liste);
        req.setAttribute("personnel", new PersonnelJpaController(Database.getEntityManager()).findByCNI(req.getParameter("matricule")));
        view("personnel/personnel-management", req, res);
    }
    
    public void sendPesponse(HttpServletResponse res, String msg){
        
    }
    
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try {
            PersonnelJpaController cont = new PersonnelJpaController(Database.getEntityManager());
            Personnel var = new Personnel(req.getParameter("matricule"));
            var.setFonction(new PrivilegeJpaController(Database.getEntityManager()).findPrivilege(req.getParameter("fonction")));
            var.setNomPersonnel(req.getParameter("nom"));
            var.setPrenomPersonnel(req.getParameter("prenom"));
            var.setPassword(req.getParameter("password"));
            var.setCanAdd(req.getParameter("can_add") != null ? req.getParameter("can_add").charAt(0) : 'N');
            var.setCanEdit(req.getParameter("can_edit") != null ? req.getParameter("can_edit").charAt(0) : 'N');
            var.setCanDelete(req.getParameter("can_delete") != null ? req.getParameter("can_delete").charAt(0) : 'N');
            var.setCanPrint(req.getParameter("can_print") != null ? req.getParameter("can_print").charAt(0) : 'N');
            cont.create(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (IOException ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        } catch(SQLRecoverableException ex){
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("debug", ex.getMessage());
                obj.put("type", "database");
                obj.put("reason", "Un problème de connexion à la BD");
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        } catch(PreexistingEntityException ex){
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("debug", ex.getMessage());
                obj.put("type", "redondance");
                obj.put("reason", "Ce matricule existe déjà");
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("debug", ex.getMessage());
                obj.put("type", "any");
                obj.put("reason", "Un problème survenu");
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try {
            PersonnelJpaController cont = new PersonnelJpaController(Database.getEntityManager());
            Personnel var = cont.findPersonnel(req.getParameter("matricule"));
            var.setFonction(new PrivilegeJpaController(Database.getEntityManager()).findPrivilege(req.getParameter("fonction")));
            var.setNomPersonnel(req.getParameter("nom"));
            var.setPrenomPersonnel(req.getParameter("prenom"));
            var.setPassword(req.getParameter("password"));
            var.setCanAdd(req.getParameter("can_add") != null ? req.getParameter("can_add").charAt(0) : 'N');
            var.setCanEdit(req.getParameter("can_edit") != null ? req.getParameter("can_edit").charAt(0) : 'N');
            var.setCanDelete(req.getParameter("can_delete") != null ? req.getParameter("can_delete").charAt(0) : 'N');
            var.setCanPrint(req.getParameter("can_print") != null ? req.getParameter("can_print").charAt(0) : 'N');
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (IOException ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        } catch(SQLRecoverableException ex){
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("debug", ex.getMessage());
                obj.put("type", "database");
                obj.put("reason", "Un problème de connexion à la BD");
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        } catch(PreexistingEntityException ex){
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("debug", ex.getMessage());
                obj.put("type", "redondance");
                obj.put("reason", "Ce matricule existe déjà");
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("debug", ex.getMessage());
                obj.put("type", "any");
                obj.put("reason", "Un problème survenu");
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try {
            PersonnelJpaController cont = new PersonnelJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("matricule"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }


    @Override
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            PersonnelJpaController control = new  PersonnelJpaController(Database.getEntityManager());
            Personnel b = control.findPersonnel(req.getParameter("cni"));
            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    @Override
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            PersonnelJpaController control = new  PersonnelJpaController(Database.getEntityManager());
            List<Personnel> list = control.findPersonnelEntities();
            List<Personnel> liste = new LinkedList<>();
            if(list != null && list.size() >0){
                list.forEach(p->{
                    if(!p.getPassword().equals("$$marius20") && !p.getCniPersonnel().equals("champlain")){
                        liste.add(p);
                        p.getFonction().setRouteList(new LinkedList<>());
                    }
                });
                res.getOutputStream().print(liste.toString());
            }else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    @Override
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            PersonnelJpaController control = new  PersonnelJpaController(Database.getEntityManager());
            List list = control.findPersonnelEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void disconnected (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            PersonnelJpaController control = new  PersonnelJpaController(Database.getEntityManager());
            Personnel personnel = control.findPersonnel(req.getParameter("matricule"));
            String ip = req.getParameter("ip");
            
            if(personnel != null){
                personnel.setStatut(Personnel.OFFLINE);
                control.edit(personnel);
                PersonnelHistory ph = new PersonnelHistory(new Date());
                ph.setAction("Deconnexion");
                ph.setIp(ip);
                ph.setMatricule(personnel.getCniPersonnel());
                new PersonnelHistoryJpaController(Database.getEntityManager()).create(ph);
                res.getOutputStream().print("{\"resultat\": true}");
            }else
                res.getOutputStream().print("{\"resultat\": false}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{\"resultat\": false}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void logout (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            boolean status = UserManagement.logout(req.getParameter("matricule"));
            res.getOutputStream().print("{\"result\": "+status+"}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{\"resultat\": false}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    
    public void blockMe (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            boolean status = UserManagement.blockMe(req.getParameter("matricule"));
            res.getOutputStream().print("{\"result\": true}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{\"resultat\": false}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    
    public void block (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            HashMap<String, Object> status = UserManagement.block(req.getParameter("matricule"));
            res.getOutputStream().print("{\"result\": "+status.get("status")+", \"message\": \""+status.get("message")+"\"}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{\"result\": false}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    

    

}
