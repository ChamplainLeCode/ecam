/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.EquipeJpaController;
import app.modelController.PersonnelJpaController;
import app.models.Database;
import app.models.Equipe;
import app.routes.Api;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class EquipeController extends Controller implements IController{
    
    
     
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        
            view("equipe/equipe-home", req, res);
    }
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("equipe/equipe-add", req, res);
    }
    public void edit(HttpServletRequest req, HttpServletResponse res){
        // L'objet json essence est récupéré sous forme de tableau indexé essence[ref], essence[densite] essence[libelle]
            view("equipe/equipe-edit", req, res);
    }
        
    
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        try {
            EquipeJpaController cont = new EquipeJpaController(Database.getEntityManager());
            Equipe var = new Equipe(req.getParameter("ref"));
            var.setDateEquipe(new Date(Long.parseLong(req.getParameter("dateEquipe"))));
            var.setDateExecution(new Date(Long.parseLong(req.getParameter("dateExec"))));
            var.setLibelle(req.getParameter("libelle"));

            cont.create(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                ex.printStackTrace(new PrintStream(res.getOutputStream()));//.print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        try {
            EquipeJpaController cont = new EquipeJpaController(Database.getEntityManager());
            Equipe var = cont.findEquipe(req.getParameter("ref"));
            var.setDateEquipe(new Date(Long.parseLong(req.getParameter("dateEquipe"))));
            var.setDateExecution(new Date(Long.parseLong(req.getParameter("dateExec"))));
            var.setLibelle(req.getParameter("libelle"));
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        try {
            EquipeJpaController cont = new EquipeJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }


    @Override
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            EquipeJpaController control = new  EquipeJpaController(Database.getEntityManager());
            Equipe b = control.findEquipe(req.getParameter("ref"));

            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    public void dataFull (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            EquipeJpaController control = new  EquipeJpaController(Database.getEntityManager());
            Equipe b = control.findEquipe(req.getParameter("ref"));

            if(b != null)
                res.getOutputStream().print(b.fullString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    @Override
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
  
        try{
            EquipeJpaController control = new  EquipeJpaController(Database.getEntityManager());
            List list = control.findEquipeEntities();

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            ex.printStackTrace(new PrintStream(res.getOutputStream()));
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    
    public void dataListFull (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            EquipeJpaController control = new  EquipeJpaController(Database.getEntityManager());
            List<Equipe> list = control.findEquipeEntities();
            if(list.size() >0){
                res.getOutputStream().print("[");
               
                for(int i=0; i<list.size()-1; i++)
                    res.getOutputStream().print(list.get(i).fullString()+",");
                res.getOutputStream().print(list.get(list.size()-1).fullString());
            
                res.getOutputStream().print("]");
            }else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    @Override
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            EquipeJpaController control = new  EquipeJpaController(Database.getEntityManager());
            List list = control.findEquipeEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    

}
