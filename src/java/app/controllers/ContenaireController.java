/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.ChauffeurJpaController;
import app.modelController.ClientJpaController;
import app.modelController.ColisJpaController;
import app.modelController.CompagnieJpaController;
import app.modelController.ConteneurJpaController;
import app.modelController.ContennaireJpaController;
import app.modelController.TransporteurJpaController;
import app.modelController.exceptions.NonexistentEntityException;
import app.models.Colis;
import app.models.Database;
import app.models.Embarquement;
import app.routes.Api;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class ContenaireController extends Controller implements IController{
    
     
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        
            view("contenaire/contenaire-home", req, res);
    }
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("contenaire/contenaire-add", req, res);
    }
    public void edit(HttpServletRequest req, HttpServletResponse res){
        // L'objet json essence est récupéré sous forme de tableau indexé essence[ref], essence[densite] essence[libelle]
            view("contenaire/contenaire-edit", req, res);
    } 

    public void specification(HttpServletRequest req, HttpServletResponse res){
        Embarquement e = new ContennaireJpaController(Database.getEntityManager()).findContennaire(req.getParameter("plomb"));
        if(e != null){
            e.toString();
            List<HashMap> list = new ContennaireJpaController(Database.getEntityManager()).getTracabilite(e.getColisList());
            req.setAttribute("tracabilite", list);
        }
        req.setAttribute("embarquement", e);
        view("contenaire/specification", req, res);
    }

    public void essence(HttpServletRequest req, HttpServletResponse res){
        /*Embarquement e = new ContennaireJpaController(Database.getEntityManager()).findContennaire(req.getParameter("plomb"));
        if(e != null){
            e.toString();
            List<HashMap> list = new ContennaireJpaController(Database.getEntityManager()).getTracabilite(e.getColisList());
            req.setAttribute("tracabilite", list);
        }
        req.setAttribute("embarquement", e);*/
        view("contenaire/rendu-essence", req, res);
    }

    public void ticketPesee(HttpServletRequest req, HttpServletResponse res){
        Embarquement e = new ContennaireJpaController(Database.getEntityManager()).findContennaire(req.getParameter("plomb"));
        if(e != null){
            e.toString();
            List<HashMap> list = new ContennaireJpaController(Database.getEntityManager()).getTracabilite(e.getColisList());
            req.setAttribute("tracabilite", list);
        }
        req.setAttribute("embarquement", e);
        view("contenaire/ticket-pesee", req, res);
    }

    public void fichebordereau(HttpServletRequest req, HttpServletResponse res){
        Embarquement e = new ContennaireJpaController(Database.getEntityManager()).findContennaire(req.getParameter("plomb"));
        e.toString();
        req.setAttribute("embarquement", e);
        view("contenaire/bordereau", req, res);
    }


    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json");
        try {
            
            ContennaireJpaController cont = new ContennaireJpaController(Database.getEntityManager());
            EntityManager em = cont.getEntityManager();
            EntityTransaction trans = em.getTransaction();
            
            trans.begin();
            Embarquement var = new Embarquement();
            var.setNumConteneur(new ConteneurJpaController(em.getEntityManagerFactory()).findConteneur(req.getParameter("conteneur")));
            var.setDateChargement(new Date(Long.parseLong(req.getParameter("date"))));
            var.setVolumeContennaire(Double.parseDouble(req.getParameter("volume")));
            var.setVolumeBrut(Double.parseDouble(req.getParameter("volumeBrut")));
            var.setNumPlomb(req.getParameter("plomb"));
            var.setSurface(Double.parseDouble(req.getParameter("surface")));
            var.setRefCompagnie(new CompagnieJpaController(em.getEntityManagerFactory()).findCompagnie(req.getParameter("compagnie")));
            var.setRefTransporteur(new TransporteurJpaController(em.getEntityManagerFactory()).findTransporteur(req.getParameter("transporteur")));
            var.setRefChauffeur(new ChauffeurJpaController(em.getEntityManagerFactory()).findChauffeur(req.getParameter("chauffeur")));
            var.setRefClient(new ClientJpaController(em.getEntityManagerFactory()).findClient(req.getParameter("client")));
            var.setImmatriculation(req.getParameter("immatriculation"));
            var.setPaysDestination(req.getParameter("pays"));
            var.setVilleDestination(req.getParameter("ville"));
            var.setLettreCamion(req.getParameter("lettre"));
            var.setAdresse(req.getParameter("adresse"));
            var.setPortDepart(req.getParameter("portDepart"));
            var.setPortDestination(req.getParameter("portDestination"));
            var.setTelephone(req.getParameter("telephone"));
            var.setNomBateau(req.getParameter("bateau"));
            cont.create(var);
            
            List<Colis> liste = new LinkedList<>();
            int nbrPaquets = 0;
            ColisJpaController contColis = new ColisJpaController(em.getEntityManagerFactory());
            for (String parameterValue : req.getParameterValues("colis[]")) {
            //System.out.println("lookup colis = "+parameterValue);
                Colis c = contColis.findColis(parameterValue);
            //System.out.println("colis find = "+c);
                c.setNumPlomb(var);
                liste.add(c);
                nbrPaquets += c.getNbrePaquet();
            //System.out.println("colis after setting contenaire = "+c.getRefContennaire());
                contColis.edit(c);
            //System.out.println("colis after editing = "+c);
            }
            
            var.setColisList(liste);
            
            var.setNbrColis(liste.size());
            var.setNbrPaquets(nbrPaquets);
            cont.edit(var);
            trans.commit();
            req.setAttribute("contenaire", var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
            System.out.println("Embarquement terminé status => [0K]");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
                ex.printStackTrace(new PrintStream(res.getOutputStream()));
            } catch (IOException | JSONException ex1) {}
            System.out.println("Embarquement terminé status => [Failed]");
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try {
            ContennaireJpaController cont = new ContennaireJpaController(Database.getEntityManager());
            Embarquement var = cont.findContennaire(req.getParameter("ref"));
            var.setDateChargement(new Date(req.getParameter("dateChargement")));

            var.setVolumeContennaire(Double.parseDouble(req.getParameter("volume")));
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try {
            ContennaireJpaController cont = new ContennaireJpaController(Database.getEntityManager());
            ColisJpaController contColis = new ColisJpaController(Database.getEntityManager());
            
            EntityTransaction trans = cont.getEntityManager().getTransaction();
            
            trans.begin();
                /*List<Colis> colis = contColis.colisForContenaire(req.getParameter("plomb"));
                colis.forEach((Colis c)->{
                    c.setEmbarquement(null);
                    try {
                        contColis.edit(c);
                    } catch (NonexistentEntityException ex) {

                    } catch (Exception ex) {

                    }
                });*/
                cont.destroy(req.getParameter("plomb"));
            trans.commit();
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ContennaireJpaController control = new  ContennaireJpaController(Database.getEntityManager());
            Embarquement b = control.findContennaire(req.getParameter("ref"));
            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json");
        try{
            ContennaireJpaController control = new  ContennaireJpaController(Database.getEntityManager());
            List list = control.findContennaireEntities();
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            ex.printStackTrace();
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ContennaireJpaController control = new  ContennaireJpaController(Database.getEntityManager());
            List list = control.findContennaireEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void getQualityNEpaisseur (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ColisJpaController control = new  ColisJpaController(Database.getEntityManager());
            String list = control.getQualityNEpaisseurForContenaire(req.getParameter("plomb"));
            res.getOutputStream().print(list);
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            ex.printStackTrace(new PrintStream(res.getOutputStream()));
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    
    
}
