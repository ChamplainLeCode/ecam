/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.PrivilegeJpaController;
import app.modelController.RouteJpaController;
import app.models.Database;
import app.models.Privilege;
import app.models.Route;
import app.routes.Api;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class AccesController extends Controller implements IController{
    

    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        try {
            PrivilegeJpaController cont = new PrivilegeJpaController(Database.getEntityManager());
            Privilege var = cont.findPrivilege(req.getParameter("privilege"));
            String[] routes = req.getParameterValues("route[]");
            LinkedList<Route> l = new LinkedList<>();
            for(String s: routes){
                l.add(new RouteJpaController(Database.getEntityManager()).findRoute(s));
            }
            var.setRouteList(l);
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        try {
            PrivilegeJpaController cont = new PrivilegeJpaController(Database.getEntityManager());
            Privilege var = cont.findPrivilege(req.getParameter("ref"));
            var.setRouteList(new LinkedList<>());
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        try {
            PrivilegeJpaController cont = new PrivilegeJpaController(Database.getEntityManager());
            Privilege var = cont.findPrivilege(req.getParameter("ref"));
            var.setRouteList(new LinkedList<>());
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        @Override
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            PrivilegeJpaController control = new  PrivilegeJpaController(Database.getEntityManager());
            Privilege b = control.findPrivilege(req.getParameter("nom"));

            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
        @Override
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            PrivilegeJpaController control = new  PrivilegeJpaController(Database.getEntityManager());
            List<Privilege> list = control.findPrivilegeEntities();
            if(list != null && list.size() >0){
                list.forEach((pri) -> {
                    pri.loadRoute();
                });
                res.getOutputStream().print(list.toString());
            }else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
        @Override
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            PrivilegeJpaController control = new  PrivilegeJpaController(Database.getEntityManager());
            List list = control.findPrivilegeEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res) {
        view("access/access-home", req, res);
    }

    public void add(HttpServletRequest req, HttpServletResponse res) {
        view("access/access-add", req, res);
    }

    public void edit(HttpServletRequest req, HttpServletResponse res) {
        view("access/access-edit", req, res);
    }

    

}
