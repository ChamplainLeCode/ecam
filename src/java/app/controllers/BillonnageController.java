/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.BillonJpaController;
import app.modelController.BillonnageJpaController;
import app.modelController.DetailReceptionJpaController;
import app.modelController.ParcJpaController;
import app.modelController.exceptions.NonexistentEntityException;
import app.models.Billon;
import app.models.Billonnage;
import app.models.Database;
import app.models.DetailReception;
import app.routes.Api;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.Date;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kodvel.core.controller.Controller;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class BillonnageController extends Controller implements IController{

    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        view("Billonnage/billonnage-home", req, res);
    }
    
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("Billonnage/billonnage-add", req, res);
    }
    
    public void edit(HttpServletRequest req, HttpServletResponse res){
        view("Billonnage/billonnage-edit", req, res);
    }
     
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json");
        try {
            BillonnageJpaController cont = new BillonnageJpaController(Database.getEntityManager());
            Billonnage var = new Billonnage(req.getParameter("ref"));
            DetailReceptionJpaController contDR = new DetailReceptionJpaController(Database.getEntityManager());
            
            DetailReception dr = contDR.findDetailReceptionByNumBille(req.getParameter("bille"));
            dr.setNumtravail((Calendar.getInstance().get(Calendar.YEAR))+"-"+req.getParameter("numTravail"));
            contDR.edit(dr);
            
            var.setNbreBillon(Integer.parseInt(req.getParameter("nombre")));
            var.setNumBille(req.getParameter("bille"));
            var.setNumTravail(dr.getNumtravail());
            var.setRefEquippe(req.getParameter("equipeRef"));
            var.setRefParc(new ParcJpaController(Database.getEntityManager()).findParc(req.getParameter("parcRef")));
            cont.create(var);
            
            for(int i=0; i<var.getNbreBillon(); i++){
                Billon b = new Billon(var.getNumTravail()+"-"+req.getParameter("data["+i+"][ref]").split("-")[1]);
                //double d1 = Double.parseDouble(req.getParameter("data["+i+"][diam1]")),
                //       d2 = Double.parseDouble(req.getParameter("data["+i+"][diam2]"));
                b.setNumTravail(var.getNumTravail());
                b.setRefBillonnage(var);
                
//                b.setGdDiam(d1 > d2 ? d1 : d2);
                b.setLongueur(Integer.parseInt(req.getParameter("data["+i+"][longueur]")));
//                b.setPtitDiam(d1 > d2 ? d2 : d1);
//                b.setVolume(Math.pow((b.getGdDiam()+b.getPtitDiam())/4,2));
                new BillonJpaController(Database.getEntityManager()).create(b);
            }
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        System.out.println(new JSONObject(req.getParameterMap()).toString());
        try {
            BillonnageJpaController cont = new BillonnageJpaController(Database.getEntityManager());
            Billonnage var = cont.findBillonnage(req.getParameter("ref"));
            var.setNbreBillon(Integer.parseInt(req.getParameter("nbrBillon")));
            var.setNumBille(req.getParameter("bille"));
            var.setNumTravail(Calendar.getInstance().get(Calendar.YEAR)+"-"+req.getParameter("numTravail"));
            var.setRefEquippe(req.getParameter("equipeRef"));
            var.setRefParc(new ParcJpaController(Database.getEntityManager()).findParc(req.getParameter("parcRef")));
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex);
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try {
            BillonnageJpaController cont = new BillonnageJpaController(Database.getEntityManager());
            List<Billon> list = BillonJpaController.findByBillonnage(req.getParameter("ref"));
            BillonJpaController contBillon = new BillonJpaController(Database.getEntityManager());
            list.forEach((b)->{
                try{contBillon.destroy(b.getRefBillon());}catch(NonexistentEntityException e){}
            });
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
                ex.printStackTrace(new PrintStream(res.getOutputStream()));
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            BillonnageJpaController control = new  BillonnageJpaController(Database.getEntityManager());
            Billonnage b = control.findBillonnage(req.getParameter("ref"));
            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            BillonnageJpaController control = new  BillonnageJpaController(Database.getEntityManager());
            List list = control.findBillonnageEntities();
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    public void referenceList (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            BillonnageJpaController control = new  BillonnageJpaController(Database.getEntityManager());
            List<Billonnage> list = control.findBillonnageEntities();
            
            if(list != null && list.size() >0){
                List<String> liste = new LinkedList<>();
                for(Billonnage b: list){
                    liste.add(b.getRefBillonnage());
                }
                res.getOutputStream().print(liste.toString());
            }else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]"); 
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            BillonnageJpaController control = new  BillonnageJpaController(Database.getEntityManager());
            List list = control.findBillonnageEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    
    public void fiche (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        view("parc/billonnage-fiche", req, res);
    }

    
}
