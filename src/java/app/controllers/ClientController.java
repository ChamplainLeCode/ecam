/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.ClientJpaController;
import app.models.Database;
import app.models.Client;
import app.routes.Api;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class ClientController extends Controller  implements IController{
    
    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        
            view("client/client-home", req, res);
    }
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("client/client-add", req, res);
    }
    public void edit(HttpServletRequest req, HttpServletResponse res){
        // L'objet json essence est récupéré sous forme de tableau indexé essence[ref], essence[densite] essence[libelle]
            view("client/client-edit", req, res);
    }
        
// ------------------------------------- POST -----------------------------------------

 
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-type", "Application/json");
        try {
            ClientJpaController cont = new ClientJpaController(Database.getEntityManager());
            Client client = new Client(req.getParameter("ref"));
            client.setNomClient(req.getParameter("nom"));
            client.setPays(req.getParameter("pays"));
            client.setNationalite(req.getParameter("nationalite"));
            client.setVille(req.getParameter("ville"));
            client.setTelephones(req.getParameter("telephone"));
            client.setEmail(req.getParameter("email"));

            cont.create(client);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-type", "Application/json");
        try {
            ClientJpaController cont = new ClientJpaController(Database.getEntityManager());
            Client client = cont.findClient(req.getParameter("ref"));
            client.setNomClient(req.getParameter("nom"));
            client.setEmail(req.getParameter("email"));
            client.setNationalite(req.getParameter("nationalite"));
            client.setNomClient(req.getParameter("nom"));
            client.setTelephones(req.getParameter("telephone"));
            client.setVille(req.getParameter("ville"));

            cont.edit(client);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-type", "Application/json");
        try {
            ClientJpaController cont = new ClientJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }


    
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ClientJpaController control = new  ClientJpaController(Database.getEntityManager());
            Client b = control.findClient(req.getParameter("ref"));

            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            ClientJpaController control = new  ClientJpaController(Database.getEntityManager());
            List list = control.findClientEntities();
    
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ClientJpaController control = new  ClientJpaController(Database.getEntityManager());
            List list = control.findClientEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    
}
