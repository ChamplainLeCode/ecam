/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.ColisJpaController;
import app.modelController.JointageJpaController;
import app.modelController.PaquetJpaController;
import app.models.Colis;
import app.models.Database;
import app.models.Jointage;
import app.models.Paquet;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONArray;

/**
 *
 * @author champlain
 */
public class JointageController extends Controller implements IController{

    @Override
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("jointage/jointage-add", req, res);
    }
    
    @Override
    public void start(HttpServletRequest req, HttpServletResponse res) {
        view("jointage/jointage-home", req, res);
    }

    @Override
    public void data(HttpServletRequest req, HttpServletResponse res) throws IOException {

    }

    @Override
    public void dataList(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json;charset=utf-8");
        try{
            JointageJpaController cont = new JointageJpaController(Database.getEntityManager());
            List<Jointage> liste = cont.findJointageEntities();
            if(liste.isEmpty()){
                res.getOutputStream().print("[]");
            }else{
                res.getOutputStream().print(liste.toString());
            }
        }catch(Exception e){
            res.getOutputStream().println("[]");
            e.printStackTrace(new PrintStream(res.getOutputStream()));
        }
    }

    public void dataListCount(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json;charset=utf-8");
        try{
            ColisJpaController cont = new ColisJpaController(Database.getEntityManager());
            int count = cont.getNumberColisJoint();
                res.getOutputStream().print("{\"number\": "+count+"}");
        }catch(Exception e){
            res.getOutputStream().println("[]");
            e.printStackTrace(new PrintStream(res.getOutputStream()));
        }
    }

    
    public void fiche(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json;charset=utf-8");
        try{
            JointageJpaController cont = new JointageJpaController(Database.getEntityManager());
            JSONArray liste = cont.findJointageByDate(new Date(Long.parseLong(req.getParameter("date"))));
            System.out.println(liste);
            if(liste.length()<=0){
                res.getOutputStream().print("[]");
            }else{
                res.getOutputStream().print(liste.toString());
            }
        }catch(Exception e){
            res.getOutputStream().println("[]");
            e.printStackTrace(new PrintStream(res.getOutputStream()));
        }
    }

    public void listOfColis(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json; charset=UTF-8");
        try{
            ColisJpaController cont = new ColisJpaController(Database.getEntityManager());
            List<Colis> liste = cont.getColisFermesNonJoints();
            if(liste.isEmpty()){
                res.getOutputStream().print("[]");
            }else{
                res.getOutputStream().print(liste.toString());
            }
        }catch(Exception e){
            res.getOutputStream().println("[]");
            e.printStackTrace(new PrintStream(res.getOutputStream()));
        }
    }

    @Override
    public void dataListPage(HttpServletRequest req, HttpServletResponse res) throws IOException {

    }

    @Override
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json; charset=UTF-8");
            JointageJpaController jointCont = new JointageJpaController(Database.getEntityManager());
            ColisJpaController cont = new ColisJpaController(Database.getEntityManager());
            PaquetJpaController paquetCont = new PaquetJpaController(Database.getEntityManager());
        try {
            Jointage jointage = new Jointage("J_"+new Date().getTime());
            double epaisseur = Paquet.convertEpaisseur(req.getParameter("epaisseur"));
            Colis c = new Colis();
            
            jointage.setColisFinal(c);
            jointage.setDateJointage(new Date());
            jointage.setEpaisseur((float)epaisseur);
            jointage.setLongueur(Integer.parseInt(req.getParameter("longueurFeuille")));
            jointage.setLargeur(Integer.parseInt(req.getParameter("largeurFeuille")));
            jointage.setNbrFeuille(Integer.parseInt(req.getParameter("feuilleParPaquet")));
            jointage.setNbrPaquetRestant(Integer.parseInt(req.getParameter("nbrPaquetRestant")));
            jointage.setNbrFeuilleDefectueuse(Integer.parseInt(req.getParameter("nbrFeuilleDefectueuse")));
            jointage.setNbrFeuilleDernierPaquet(Integer.parseInt(req.getParameter("nbrFeuilleDernierPaquet")));
            
            c.setNbrePaquet(Integer.parseInt(req.getParameter("paquet")));
            c.setLongueurColis(Double.parseDouble(req.getParameter("longueurColis")));
            c.setLargeurColis(Double.parseDouble(req.getParameter("largeurColis")));
            c.setEpaisseur(epaisseur);
            c.setSurfaceColis(jointage.getLargeur()*jointage.getLongueur()*epaisseur*c.getNbrePaquet());
            c.setPaquetList(new LinkedList());
            c.setVoulumeColis(jointage.getLargeur()*jointage.getLongueur()*epaisseur*Integer.parseInt(req.getParameter("feuilleParPaquet")));
            c.setRefColis(Calendar.getInstance().get(Calendar.YEAR)+"-J-"+cont.getNumberColisJoint());
            c.setCodeBarColis(c.getRefColis());
            c.setDateDebut(new Date());
            //c.setEssence(req.getParameter("essence"));
            c.setColisJoin();
            String[] values = req.getParameterValues("colis[]");
            
            
            //Colis oldColis = cont.findColis(req.getParameter("colis"));
            
            List<Colis> liste = new LinkedList();
            //List<String> listeEssence = new LinkedList();
            //c.setEssence(null);
            
            if(values == null ||  values.length == 0){
            //if(oldColis == null){
                System.out.println("Aucun colis à joindre trouver trouvé");
                res.getOutputStream().print("{\"error\": \"Probleme avec les colis, Aucun colis trouve pour le jointage <br>Veuillez en selectionner\"}");
                return;
            }
            Colis oldColis = cont.findColis(values[0]);
//            oldColis.setJointage(jointage);
            c.setEssence(oldColis.getEssence());
            c.setQualite(oldColis.getQualite());
            
            
            for(String colRef: values){
                Colis colis = cont.findColis(colRef);
                //c.setRefColis(c.getRefColis()+colis.getRefColis()+"_");
                liste.add(colis); 
                colis.setJointeur(jointage.getRefJointage());
                //c.setQualite(colis.getQualite());
            }
            
            
            
            cont.create(c);
            jointage.setNbrColisJoints(liste.size());
            jointage.setColisList(liste);
            jointCont.create(jointage);
            
            for(Colis col: liste)
                cont.edit(col);
            

            
            Paquet pt = paquetCont.getPaquetInColis(oldColis);
            for(int i=0; i<c.getNbrePaquet(); i++){
                Paquet paquet = new Paquet(c.getRefColis()+'-'+i);
                paquet.setEpaisseur(req.getParameter("epaisseur"));
                paquet.setLargeur(Double.parseDouble(jointage.getLargeur()+""));
                paquet.setLongueur(Double.parseDouble(jointage.getLongueur()+""));
                paquet.setNbreFeuille(jointage.getNbrFeuille());
                paquet.setRefCodebar(c.getRefColis());
                paquet.setNumTravail(pt == null ? "" : pt.getNumTravail());
                paquet.setQualite(c.getQualite());
                paquet.setRefColis(c);
                paquetCont.create(paquet);
            }
            
            new Thread(()->{
                cont.updateColis(req);
            }).start();
            
            res.getOutputStream().print("{\"resultat\":true}");
        } catch (Exception ex) {
            ex.printStackTrace(new PrintStream(res.getOutputStream()));
        }
       
        
    }

    @Override
    public void update(HttpServletRequest req, HttpServletResponse res) throws IOException {

    }

    @Override
    public void delete(HttpServletRequest req, HttpServletResponse res) throws IOException {

    }
    
}
