/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.ColisJpaController;
import app.modelController.EmpotageJpaController;
import app.models.Colis;
import app.models.Database;
import app.models.Empotage;
import app.routes.Api;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class EmpotageController extends Controller implements IController{
    
     
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        view("empotage/empotage-home", req, res);
    }
    
    public void edit(HttpServletRequest req, HttpServletResponse res){
        view("empotage/empotage-edit", req, res);
    }
    
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("empotage/empotage-add", req, res);
    }
    
    
     @Override
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        try {
            EmpotageJpaController cont = new EmpotageJpaController(Database.getEntityManager());
            Empotage var = new Empotage();
            var.setRefColis(new ColisJpaController(Database.getEntityManager()).findColis(req.getParameter("colis")));
            Colis colis = var.getRefColis();
            var.setFermetureCaisse(req.getParameter("fermetureCaisse"));
            var.setHauteur(Double.parseDouble(req.getParameter("hauteur")));
            var.setLongueur(Double.parseDouble(req.getParameter("longueur")));
            var.setLargeur(Double.parseDouble(req.getParameter("largeur")));
            var.setRefEmpotage(colis.getRefColis());
            colis.setDateFermeture(new Date());
            colis.setFermetureColis(var.getFermetureCaisse());
            colis.setDateFin(new Date());
            colis.setPoidEmballage(Double.parseDouble(req.getParameter("emballage-poids")));
            colis.setPoidsColis(Double.parseDouble(req.getParameter("total-poids")));
            colis.setPoidNet(Double.parseDouble(req.getParameter("total-poids")));
            new ColisJpaController(Database.getEntityManager()).edit(colis);
            cont.create(var);
            res.sendRedirect("/empotage?user="+req.getParameter("user")+"&page="+req.getParameter("page"));
//            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                ex.printStackTrace();
//                res.getOutputStream().print(obj.toString());
            } catch ( JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        try {
            EmpotageJpaController cont = new EmpotageJpaController(Database.getEntityManager());
            Empotage var = cont.findEmpotage(req.getParameter("ref"));
            var.setFermetureCaisse(req.getParameter("fermetureCaisse"));
            var.setHauteur(Double.parseDouble(req.getParameter("hauteur")));
            var.setLargeur(Double.parseDouble(req.getParameter("largeur")));
            var.setRefColis(new ColisJpaController(Database.getEntityManager()).findColis(req.getParameter("colis")));
            
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        try {
            EmpotageJpaController cont = new EmpotageJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }


    @Override
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            EmpotageJpaController control = new  EmpotageJpaController(Database.getEntityManager());
            Empotage b = control.findEmpotage(req.getParameter("ref"));

            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
        @Override

    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            EmpotageJpaController control = new  EmpotageJpaController(Database.getEntityManager());
            List list = control.findEmpotageEntities();

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
        @Override
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            EmpotageJpaController control = new  EmpotageJpaController(Database.getEntityManager());
            List list = control.findEmpotageEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    

}
