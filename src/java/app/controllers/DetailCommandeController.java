/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.CommandeJpaController;
import app.modelController.DetailCommandeJpaController;
import app.modelController.EssenceJpaController;
import app.models.Database;
import app.models.DetailCommande;
import app.routes.Api;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class DetailCommandeController extends Controller implements IController{
    
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        try {
            DetailCommandeJpaController cont = new DetailCommandeJpaController(Database.getEntityManager());
            DetailCommande var = new DetailCommande(req.getParameter("ref"));
            var.setVolume(Float.parseFloat(req.getParameter("longueur")));
            var.setRefCommande(new CommandeJpaController(Database.getEntityManager()).findCommande(req.getParameter("commandeRef")));
            var.setRefEssence(new EssenceJpaController(Database.getEntityManager()).findEssence(req.getParameter("essenceRef")));
            
            cont.create(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        try {
            DetailCommandeJpaController cont = new DetailCommandeJpaController(Database.getEntityManager());
            DetailCommande var = cont.findDetailCommande(req.getParameter("ref"));
            var.setVolume(Float.parseFloat(req.getParameter("longueur")));
            var.setRefCommande(new CommandeJpaController(Database.getEntityManager()).findCommande(req.getParameter("commande")));
            var.setRefEssence(new EssenceJpaController(Database.getEntityManager()).findEssence(req.getParameter("essence")));
            
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        try {
            DetailCommandeJpaController cont = new DetailCommandeJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    @Override
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            DetailCommandeJpaController control = new  DetailCommandeJpaController(Database.getEntityManager());
            DetailCommande b = control.findDetailCommande(req.getParameter("ref"));

            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print(null);
        }catch(Exception ex){try {
            res.getOutputStream().print(null);
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    @Override
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            DetailCommandeJpaController control = new  DetailCommandeJpaController(Database.getEntityManager());
            List list = control.findDetailCommandeEntities();

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    @Override
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            DetailCommandeJpaController control = new  DetailCommandeJpaController(Database.getEntityManager());
            List list = control.findDetailCommandeEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    public void findDetailByBille(HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        
        try {
            DetailCommande liste = DetailCommandeJpaController.findDetailCommandeByBille(req.getParameter("bille"));
            if(liste != null)
                res.getOutputStream().print(liste.toString());
            else
                res.getOutputStream().print(null);
        } catch (Exception ex) {
            try {
                res.getOutputStream().print(null);
            } catch (IOException ex1) {}
        }
    }

    

    public void findDetailByCommande(HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        
        try {
            List<DetailCommande> liste = DetailCommandeJpaController.findDetailCommandeByCommande(req.getParameter("commande"));
            if(liste != null && liste.size()>0)
                res.getOutputStream().print(liste.toString());
            else
                res.getOutputStream().print("[]");
        } catch (Exception ex) {
            try {
                res.getOutputStream().print("[]");
            } catch (IOException ex1) {}
        }
    }

    @Override
    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res) {

    }

    

}
