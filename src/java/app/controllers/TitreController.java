/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.TitreJpaController;
import app.modelController.exceptions.NonexistentEntityException;
import app.models.Database;
import app.models.Titre;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class TitreController  extends Controller implements IController{

    @Override
    public void data(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json");
        try{
            TitreJpaController control = new  TitreJpaController(Database.getEntityManager());
            Titre b = control.findTitre(req.getParameter("ref"));

            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            ex.printStackTrace(new PrintStream(res.getOutputStream()));
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    @Override
    public void dataList(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json");
        try{
            TitreJpaController control = new  TitreJpaController(Database.getEntityManager());
            List<Titre> b = control.findTitreEntities();
            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            ex.printStackTrace(new PrintStream(res.getOutputStream()));
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    public void byParc(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json");
        try{
            TitreJpaController control = new  TitreJpaController(Database.getEntityManager());
            Titre b = control.findLastTitreByRefParc(req.getParameter("refParc"));
            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("null");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            ex.printStackTrace(new PrintStream(res.getOutputStream()));
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    @Override
    public void dataListPage(HttpServletRequest req, HttpServletResponse res) throws IOException {
    
    }

    @Override
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {

    }

    @Override
    public void update(HttpServletRequest req, HttpServletResponse res) throws IOException {

    }

    @Override
    public void delete(HttpServletRequest req, HttpServletResponse res) throws IOException {
        try {
            TitreJpaController cont = new TitreJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (Exception ex1) {
            }
        }
    }

    @Override
    public void index(HttpServletRequest req, HttpServletResponse res) {
        super.index(req, res);
    }

    @Override
    public void start(HttpServletRequest req, HttpServletResponse res) {
    }
    
}
