/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.ClassificationEssJpaController;
import app.modelController.ClassificationJpaController;
import app.modelController.EssenceJpaController;
import app.models.ClassificationEss;
import app.models.Database;
import app.routes.Api;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class ClassificationEssController extends Controller implements IController{
    
    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
            view("classification-ess/classification-home", req, res);
    }
    public void add(HttpServletRequest req, HttpServletResponse res){
            view("classification-ess/classification-add", req, res);
    }
    public void edit(HttpServletRequest req, HttpServletResponse res){
        // L'objet json essence est récupéré sous forme de tableau indexé essence[ref], essence[densite] essence[libelle]
            view("classification-ess/classification-edit", req, res);
    }
    

    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ClassificationEssJpaController control = new  ClassificationEssJpaController(Database.getEntityManager());
            ClassificationEss b = control.findClassificationEss(req.getParameter("ref"));

            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            ClassificationEssJpaController control = new  ClassificationEssJpaController(Database.getEntityManager());
            List list = control.findClassificationEssEntities();
            
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ClassificationEssJpaController control = new  ClassificationEssJpaController(Database.getEntityManager());
            List list = control.findClassificationEssEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    public void save(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try {
            ClassificationEssJpaController cont = new ClassificationEssJpaController(Database.getEntityManager());
            ClassificationEss classif = new ClassificationEss(req.getParameter("ref"));
            classif.setEpaisseur(Double.parseDouble(req.getParameter("epaisseur")));
            classif.setRefEssence( new EssenceJpaController(Database.getEntityManager()).findEssence(req.getParameter("essence")));
            cont.create(classif);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try {
            ClassificationEssJpaController cont = new ClassificationEssJpaController(Database.getEntityManager());
            ClassificationEss classif = cont.findClassificationEss(req.getParameter("ref"));
            classif.setEpaisseur(Double.parseDouble(req.getParameter("epaisseur")));
            classif.setRefEssence(new EssenceJpaController(Database.getEntityManager()).findEssence(req.getParameter("essenceRef")));
            cont.edit(classif);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try {
            ClassificationEssJpaController cont = new ClassificationEssJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    @Override
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        save(req, res);
    }
}
