/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.CuveJpaController;
import app.models.Cuve;
import app.models.Database;
import app.routes.Api;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class CuveController extends Controller implements IController{
    
    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        
            view("cuve/cuve-home", req, res);
    }
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("cuve/cuve-add", req, res);
    }
    public void edit(HttpServletRequest req, HttpServletResponse res){
        // L'objet json essence est récupéré sous forme de tableau indexé essence[ref], essence[densite] essence[libelle]
            view("cuve/cuve-edit", req, res);
    }
        
    

        public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        try {
            CuveJpaController cont = new CuveJpaController(Database.getEntityManager());
            Cuve var = new Cuve(req.getParameter("ref"));
            var.setLibelle(req.getParameter("libelle"));
            var.setCuve(req.getParameter("cuve"));
            
            cont.create(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        try {
            CuveJpaController cont = new CuveJpaController(Database.getEntityManager());
            Cuve var = cont.findCuve(req.getParameter("ref"));
            var.setCuve(req.getParameter("cuve"));
            var.setLibelle(req.getParameter("libelle"));
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        try {
            CuveJpaController cont = new CuveJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("refCuve"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    


    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            CuveJpaController control = new  CuveJpaController(Database.getEntityManager());
            Cuve b = control.findCuve(req.getParameter("ref"));
            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            CuveJpaController control = new  CuveJpaController(Database.getEntityManager());
            List list = control.findCuveEntities();
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            CuveJpaController control = new  CuveJpaController(Database.getEntityManager());
            List list = control.findCuveEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    
    
    public void cuveVideList (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            CuveJpaController control = new  CuveJpaController(Database.getEntityManager());
            List list = control.getCuveVide();
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void cuvePleineList (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            CuveJpaController control = new  CuveJpaController(Database.getEntityManager());
            List list = control.getCuvePleine();
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    
    
}
