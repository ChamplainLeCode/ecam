/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.ArmateurJpaController;
import app.models.Database;
import app.models.Armateur;
import app.routes.Api;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class ArmateurController extends Controller implements IController{
    

    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try {
            ArmateurJpaController cont = new ArmateurJpaController(Database.getEntityManager());
            Armateur var = new Armateur(System.currentTimeMillis()+"", req.getParameter("libelle"));
            cont.create(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try {
            ArmateurJpaController cont = new ArmateurJpaController(Database.getEntityManager());
            Armateur var = cont.findArmateur(req.getParameter("ref"));
            var.setLibelle(req.getParameter("libelle"));
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try {
            ArmateurJpaController cont = new ArmateurJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        @Override
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ArmateurJpaController control = new  ArmateurJpaController(Database.getEntityManager());
            Armateur b = control.findArmateur(req.getParameter("ref"));

            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
        @Override
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            ArmateurJpaController control = new  ArmateurJpaController(Database.getEntityManager());
            List<Armateur> list = control.findArmateurEntities();
            if(list != null && list.size() >0){
                res.getOutputStream().print(list.toString());
            }else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            ex.printStackTrace();
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
        @Override
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ArmateurJpaController control = new  ArmateurJpaController(Database.getEntityManager());
            List list = control.findArmateurEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res) {
        view("contenaire/armateur/armateur-home", req, res);
    }

    public void add(HttpServletRequest req, HttpServletResponse res) {
        view("contenaire/armateur/armateur-add", req, res);
    }

    public void edit(HttpServletRequest req, HttpServletResponse res) {
        view("contenaire/armateur/armateur-edit", req, res);
    }

    

}
