/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.CertificatJpaController;
import app.modelController.exceptions.NonexistentEntityException;
import app.models.Certificat;
import app.models.Database;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;

/**
 *
 * @author champlain
 */
public class CertificatController extends Controller implements IController{

    @Override
    public void data(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-type", "Application/json");
        try{
            CertificatJpaController cont = new CertificatJpaController(Database.getEntityManager());
            List<Certificat> liste = cont.findCertificatEntities();
            List list = new LinkedList();
            liste.forEach((Certificat e)->{
                if(e.getRefFournisseur().equals(req.getParameter("ref")))
                    list.add(e);
            });
            res.getOutputStream().print(list.toString());
        }catch(Exception e){
            try {res.getOutputStream().print("[]");} catch (IOException ex) {}
        }
    }

    
    public static List<Certificat> getCertificatFor(String fournisseur){
        CertificatJpaController cont = new CertificatJpaController(Database.getEntityManager());
        List<Certificat> liste = cont.findCertificatEntities();
        List<Certificat> list = new LinkedList();
        System.out.println(list);
        liste.forEach((Certificat e)->{
            if(e.getRefFournisseur().equals(fournisseur))
                list.add(e);
        });
        return list;
    }

    @Override
    public void dataList(HttpServletRequest req, HttpServletResponse res) throws IOException {
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        res.getOutputStream().print("[]");
    }

    @Override
    public void dataListPage(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-type", "Application/json");
        res.getOutputStream().print("[]");
    }

    @Override
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {

    }

    @Override
    public void update(HttpServletRequest req, HttpServletResponse res) throws IOException {

    }

    @Override
    public void delete(HttpServletRequest req, HttpServletResponse res) throws IOException {
        
        try {
            new CertificatJpaController(Database.getEntityManager()).destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\": \"true\"}");
        } catch (NonexistentEntityException ex) {
        
        }
        
    }

    @Override
    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res) {

    }
    
}
