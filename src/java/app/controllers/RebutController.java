/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.RebutJpaController;
import app.models.Billon;
import app.models.Database;
import app.models.Plot;
import app.models.Rebut;
import app.routes.Api;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class RebutController extends Controller implements IController{

    public static void setRebut(Class type, String ref) {
    
        RebutJpaController.setRebut(type, ref);
    }

    public static void unSetRebut(Class type, String objectRef, String rebutRef) {
        RebutJpaController.unSetRebut(type, objectRef, rebutRef);
    }

    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        view("rebut/rebut-home", req, res);
    }
     public void edit(HttpServletRequest req, HttpServletResponse res){
        view("rebut/rebut-edit", req, res);
    }
    
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("rebut/rebut-add", req, res);
    }
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        try {
            RebutJpaController cont = new RebutJpaController(Database.getEntityManager());
            Rebut var = new Rebut(req.getParameter("ref"));
            var.setDateRebut(new Date(Long.parseLong(req.getParameter("date"))));
            var.setTypeRebut(req.getParameter("typeRebut"));
            cont.create(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        try {
            RebutJpaController cont = new RebutJpaController(Database.getEntityManager());
            Rebut var = cont.findRebut(req.getParameter("ref"));
            var.setDateRebut(new Date(req.getParameter("date")));
            var.setTypeRebut(req.getParameter("typeRebut"));
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        try {
            RebutJpaController cont = new RebutJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    
    @Override
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            RebutJpaController control = new  RebutJpaController(Database.getEntityManager());
            Rebut b = control.findRebut(req.getParameter("ref"));

            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    @Override
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            RebutJpaController control = new  RebutJpaController(Database.getEntityManager());
            List list = control.findRebutEntities();

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    @Override
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            RebutJpaController control = new  RebutJpaController(Database.getEntityManager());
            List list = control.findRebutEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void setRebut(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            switch(req.getParameter("class")){
                case "Billon":
                    setRebut(Billon.class, req.getParameter("ref"));
                    res.getOutputStream().print("{\"result\": true}");
                    break;
                case "Plot":
                    setRebut(Plot.class, req.getParameter("ref"));
                    res.getOutputStream().print("{\"result\": true}");
                    break;
                default:
                    res.getOutputStream().print("{\"result\": false}");
            }
        }catch(IOException e){try{
            res.getOutputStream().print("{\"result\": false}");
        }catch(IOException ex){}}
    }
    
    public void unRebut(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            switch(req.getParameter("typeRebut")){
                case "Billon":
                    unSetRebut(Billon.class, req.getParameter("objectRef"), req.getParameter("ref"));
                    res.getOutputStream().print("{\"result\": true}");
                    break;
                case "Plot":
                    unSetRebut(Plot.class, req.getParameter("objectRef"), req.getParameter("ref"));
                    res.getOutputStream().print("{\"result\": true}");
                    break;
                default:
                    res.getOutputStream().print("{\"result\": false}");
            }
        }catch(IOException e){try{
            res.getOutputStream().print("{\"result\": false}");
        }catch(IOException ex){}}
    }

    

}
