/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.CommandeJpaController;
import app.modelController.DetailCommandeJpaController;
import app.modelController.DetailLvJpaController;
import app.modelController.DetailReceptionJpaController;
import app.modelController.EssenceJpaController;
import app.modelController.LettreCamionJpaController;
import app.modelController.ParcJpaController;
import app.modelController.ReceptionJpaController;
import app.modelController.TypeParcJpaController;
import app.models.Commande;
import app.models.Database;
import app.models.DetailCommande;
import app.models.DetailLv;
import app.models.DetailReception;
import app.models.Essence;
import app.models.LettreCamion;
import app.models.Parc;
import app.models.Reception;
import app.routes.Api;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class ReceptionController extends Controller implements IController{
    
    
    
    @Override
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    @Override
    public void start(HttpServletRequest req, HttpServletResponse res){
        view("reception/reception-home", req, res);
    }
    
    public void edit(HttpServletRequest req, HttpServletResponse res){
        view("reception/reception-edit", req, res);
    }
    
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("reception/reception-add", req, res);
    }
    
        public String s = "<pre><code>[";
    
    public void bonReception(HttpServletRequest req, HttpServletResponse res) throws IOException{
        
        view("reception/bon-reception", req, res);
    }
    public void receptionLivree(HttpServletRequest req, HttpServletResponse res) throws IOException{
        
        view("reception/reception-livree", req, res);
    }
    
    /**
         * {
	*	key: data[0][bille],
	*	value:256325
	*}
	*{
	*	key: data[0][prix],
	*	value:1089.0
	*}
	*{
	*	key: data[0][essence],
	*	value:1102
	*}
	*{
	*	key: prixTransport,
	*	value:31998
	*}
	*{
	*	key: typePaiement,
	*	value:Exclus
	*}
	*{
	*	key: data[0][gdDiam],
	*	value:2.0
	*}
	*{
	*	key: commande,
	*	value:147852147
	*}
	*{
	*	key: data[0][longueur],
	*	value:2
	*}
	*{
	*	key: data[0][volume],
	*	value:0
	*}
	*{
	*	key: lettreReference,
	*	value:QSDFEFDSFD2D55S6D
	*}
	*{
	*	key: taille,
	*	value:1
	*}
	*{
	*	key: lettreFichier,
	*	value:C:\fakepath\Hydrangeas.jpg
	*}
	*{
	*	key: data[0][pttDiam],
	*	value:2.0
	*}
	*{
	*	key: data[0][moyDiam],
	*	value:2
	*}
	*{
	*	key: data[0][statut],
	*	value:Commande
	*}
	*{
	*	key: data[0][parc],
	*	value:PBL
	*}
	*{
	*	key: data[0][observation],
	*	value:C
	*}
        *
     * @param req HttpServletRequest instance d'une requete
     * @param res HttpServletResponse instance d'une réponse
     * @throws java.io.IOException cette exception est levée en cas de rupture du pipe de connexion*/
        
    public void buildBonReception(HttpServletRequest req, HttpServletResponse res) throws IOException{
        List<HashMap<String, String>> li = new LinkedList<>();
        int taille = Integer.parseInt(req.getParameter("taille"));
        Commande cmd = new CommandeJpaController(Database.getEntityManager()).findCommande(req.getParameter("commande"));
        
        for(int i=0; i<taille; i++){
            Essence ess = new EssenceJpaController(Database.getEntityManager()).findEssence(req.getParameter("data[parc]["+i+"][essence]"));     
            DetailCommande detailCommande = new DetailCommandeJpaController(Database.getEntityManager()).getDetailCommandeByNumBille(req.getParameter("data[parc]["+i+"][bille]"));
            HashMap<String, String> m = new HashMap<>();
            
            m.put("certificat", req.getParameter("data[parc]["+i+"][certificat]"));
            m.put("bille", req.getParameter("data[parc]["+i+"][bille]"));
            m.put("essence", ess.getRefEssence()+" "+ess.getLibelle());
            m.put("prix", req.getParameter("data[parc]["+i+"][prix]"));
            m.put("gdDiam", req.getParameter("data[lv]["+i+"][gdDiam]"));
            m.put("longueur", req.getParameter("data[lv]["+i+"][longueur]"));
            m.put("volume", req.getParameter("data[lv]["+i+"][volume]"));
            m.put("oldVolume", req.getParameter("data[parc]["+i+"][volume]")); //detailCommande.getVolume()+"");
            m.put("pttDiam", req.getParameter("data[lv]["+i+"][pttDiam]"));
            m.put("moyDiam", req.getParameter("data[lv]["+i+"][moyDiam]"));
            m.put("observation", req.getParameter("data[parc]["+i+"][observation]"));
            
            li.add(m);
        }
        req.setAttribute("commande", cmd);
        req.setAttribute("data", li);
        req.setAttribute("num", req.getParameter("lettreReference"));
        view("reception/bon-reception", req, res);
        res.getOutputStream().print("");
    }
    
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {

        res.setHeader("Content-Type", "Application/json");
        try {
            ReceptionJpaController cont = new ReceptionJpaController(Database.getEntityManager());
            Reception var = new Reception(req.getParameter("commande")+"/"+new Date().hashCode());
            var.setDateReception(new Date());
            var.setRefCommande(new CommandeJpaController(Database.getEntityManager()).findCommande(req.getParameter("commande")));
            
            
            LettreCamion lettre = new LettreCamion();
            lettre.setFichier(req.getParameter("lettreFichier"));
            lettre.setRefLettre(req.getParameter("lettreReference"));
            new LettreCamionJpaController(Database.getEntityManager()).create(lettre);
            
            
            var.setRefLettre(lettre);
            cont.create(var);
            
            int taille = Integer.parseInt(req.getParameter("taille"));
            LinkedList<DetailReception> listeDetail = new LinkedList<>();
            DetailReceptionJpaController cDRecep = new DetailReceptionJpaController(Database.getEntityManager());
            DetailLvJpaController cDLettreVoiture = new DetailLvJpaController(Database.getEntityManager());
            TypeParcJpaController contTypePar = new TypeParcJpaController(Database.getEntityManager());
            
            for(int i=0; i<taille; i++){

                DetailReception drecep = new DetailReception();
                DetailLv dLettrevoiture = new DetailLv();
                
                drecep.setRefDetail(req.getParameter("commande")+new Date().hashCode()+"/"+i);
                drecep.setRefEssence(new EssenceJpaController(Database.getEntityManager()).findEssence(req.getParameter("data[parc]["+i+"][essence]")));
                drecep.setDiaMoyen(Float.parseFloat(req.getParameter("data[parc]["+i+"][moyDiam]")));
                drecep.setGdDiam(Float.parseFloat(req.getParameter("data[parc]["+i+"][gdDiam]")));
                drecep.setPtitDiam(Float.parseFloat(req.getParameter("data[parc]["+i+"][pttDiam]")));
                drecep.setLongueur(Double.valueOf(Long.parseLong(req.getParameter("data[parc]["+i+"][longueur]"))));
                drecep.setNumBille(req.getParameter("data[parc]["+i+"][bille]"));
                drecep.setRefReception(var);
                drecep.setDateReception(new Date());
                drecep.setNbre(0D);
                drecep.setObservation(req.getParameter("data[parc]["+i+"][observation]"));
                drecep.setPrix(Double.parseDouble(req.getParameter("data[parc]["+i+"][prix]")));
                drecep.setStatut(req.getParameter("data[parc]["+i+"][statut]"));
                drecep.setCertificat(req.getParameter("data[parc]["+i+"][certificat]"));
                drecep.setNumtravail("");
                drecep.setVolume(Double.parseDouble(req.getParameter("data[parc]["+i+"][volume]")));
                cDRecep.create(drecep);
                
                dLettrevoiture.setRefDlv("LV_"+req.getParameter("commande")+new Date().hashCode()+"/"+i);
                dLettrevoiture.setEssence(Integer.parseInt(req.getParameter("data[lv]["+i+"][essence]")));
                dLettrevoiture.setGdDiam(Float.parseFloat(req.getParameter("data[lv]["+i+"][gdDiam]")));
                dLettrevoiture.setPttDiam(Float.parseFloat(req.getParameter("data[lv]["+i+"][pttDiam]")));
                dLettrevoiture.setLongueur(Double.valueOf(Long.parseLong(req.getParameter("data[parc]["+i+"][longueur]"))));
                dLettrevoiture.setBille(req.getParameter("data[parc]["+i+"][bille]"));
                dLettrevoiture.setCertificat(req.getParameter("data[parc]["+i+"][certificat]"));
                cDLettreVoiture.create(dLettrevoiture);
                /**
                 * Rangement dans le parc
                 */
                Parc p = new Parc(drecep.getNumBille()+new Date().hashCode()+"/"+i);
                p.setLibelle("Entree");
                p.show();
                p.setNumBille(drecep.getNumBille());
                System.out.println("Certificat "+req.getParameter("data[parc]["+i+"][certificat]"));
                p.setRefTypeparc(contTypePar.findTypeParc(req.getParameter("data[parc]["+i+"][certificat]")));// var.getRefCommande().getRefParc().getRefTypeparc());// new TypeParcJpaController(Database.getEntityManager()).findTypeParc(req.getParameter("data["+i+"][parc]")));
                p.setRefTypeparcDepart(p.getRefTypeparc());
                p.setTypeMvt(null);
                new ParcJpaController(Database.getEntityManager()).create(p);
                
                
                listeDetail.add(drecep);
            }
            
            
            var.setDetailReceptionList(listeDetail);
            
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":true, \"lettre\": \""+lettre.getRefLettre()+"\", \"id\": \""+var.getRefReception()+"\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                ex.printStackTrace(new PrintStream(res.getOutputStream()));
//                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        try {
            ReceptionJpaController cont = new ReceptionJpaController(Database.getEntityManager());
            Reception var = cont.findReception(req.getParameter("ref"));
            var.setDateReception(new Date(req.getParameter("date")));
            var.setRefCommande(new CommandeJpaController(Database.getEntityManager()).findCommande(req.getParameter("commande")));
            var.setRefLettre(new LettreCamionJpaController(Database.getEntityManager()).findLettreCamion(req.getParameter("lettreCamion")));
            
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        try {
            ReceptionJpaController cont = new ReceptionJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    @Override
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ReceptionJpaController control = new  ReceptionJpaController(Database.getEntityManager());
            Reception b = control.findReception(req.getParameter("ref"));
            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    public void dataMinList(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            EntityManagerFactory emf = Database.getEntityManager();
            ReceptionJpaController control = new ReceptionJpaController(emf);
            String type = req.getParameter("typeParc");
            if(type.equalsIgnoreCase("all")){
                String liste = control.getMinimalData();            
                res.getOutputStream().print(liste);
                return;
            }
            res.getOutputStream().print(control.getMinimalDataForCertificat(type));
            
        }catch(Exception e){
            try {
                res.getOutputStream().print("[]");
            } catch (IOException ex) {}
        }
        
    }
    
    @Override
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            ReceptionJpaController control = new  ReceptionJpaController(Database.getEntityManager());
            List list = control.findReceptionEntities();
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    @Override
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ReceptionJpaController control = new  ReceptionJpaController(Database.getEntityManager());
            List list = control.findReceptionEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    /**
     * Cette fonction permet de retourner la liste de Numéros de billes receptionnées
     * @param req  an instance of request
     * @param res  an instance of response
     */
    public void billeListWithNumTravail (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            DetailReceptionJpaController control = new  DetailReceptionJpaController(Database.getEntityManager());
            String list = control.getAllBilleWithNumTravail();// findDetailReceptionEntities();
            if(list != null){
                res.getOutputStream().print(list);
            }else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    /**
     * Cette fonction permet de retourner la liste de Numéros de billes receptionnées
     * @param req  an instance of request
     * @param res  an instance of response
     */
    public void billeList (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            DetailReceptionJpaController control = new  DetailReceptionJpaController(Database.getEntityManager());
            String list = control.getAllBilleNumber();// findDetailReceptionEntities();
            if(list != null){
                System.out.println(list);
                res.getOutputStream().print(list);
            }else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    /**
     * Cette fonction permet de retourner la liste de Numéros de billes receptionnées
     * @param req  an instance of request
     * @param res  an instance of response
     */
    public void billeListInParc (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            DetailReceptionJpaController control = new  DetailReceptionJpaController(Database.getEntityManager());
            String list;// findDetailReceptionEntities();
            list = control.getAllBilleNumberByMVT(Parc.MVT_CHANGEMENT);
            if(list != null){
                System.out.println(list);
                res.getOutputStream().print(list);
            }else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void billeListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ReceptionJpaController control = new  ReceptionJpaController(Database.getEntityManager());
            List list = control.findReceptionEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    public void uploadFile(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        
        boolean isMultipart = ServletFileUpload.isMultipartContent(req);
			
	        if (isMultipart) {
	        	// Create a factory for disk-based file items
	        	FileItemFactory factory = new DiskFileItemFactory();

	        	// Create a new file upload handler
	        	ServletFileUpload upload = new ServletFileUpload(factory);
	 
	            try {
	            	// Parse the request
					List /* FileItem */ items = upload.parseRequest(req);
					Iterator iterator = items.iterator();
                            LettreCamion lettre = null;
                            File path = null;
                            LettreCamionJpaController cont = new LettreCamionJpaController(Database.getEntityManager());
                            
                        String chemin = "/uploads/Lettres-camion/"+Calendar.getInstance().get(Calendar.YEAR)+"/"+Calendar.getInstance().get(Calendar.MONTH)+"/"+Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

                        while (iterator.hasNext()) {
	                    FileItem item = (FileItem) iterator.next();
	                    if (!item.isFormField()) {
	                        String fileName = item.getName();	 
	                        String root = req.getServletContext().getRealPath("/");
	                        path = new File(root+chemin);
	                        if (!path.exists()) {
                                    boolean status = path.mkdirs();
	                        }
	
	                        
                                try{
                                    File uploadedFile = new File(path + "/" + fileName);
                                    chemin += '/'+fileName;
                                    System.out.println("Lettre chemin = "+chemin);
                                    item.write(uploadedFile);
                                }catch(org.apache.commons.io.FileExistsException e){
                                    File uploadedFile = new File(path + "/2/" + fileName);
                                    chemin += "/2/"+fileName;
                                    item.write(uploadedFile);
                                }
	                    }else{
                                lettre = cont.findLettreCamion(item.getString());
                            }
	                }
                        if(lettre != null && path != null){
                            lettre.setFichier(chemin);
                            cont.edit(lettre);
                            res.getOutputStream().print("{\"resultat\": true}");
                            return;
                        }
                        System.out.println("Lettre = "+lettre);
                        
	            } catch (FileUploadException e) {
	                e.printStackTrace();
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	        }
                try {
                    res.getOutputStream().print("{\"resultat\": false}");
                } catch (IOException ex) {

                }
    }

}
