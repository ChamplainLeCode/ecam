/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.EssenceJpaController;
import app.modelController.FournisseurJpaController;
import app.modelController.ParcChargementJpaController;
import app.modelController.TitreJpaController;
import app.modelController.TypeParcJpaController;
import app.models.Database;
import app.models.Essence;
import app.models.ParcChargement;
import app.models.Titre;
import app.routes.Api;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class ParcChargementController extends Controller implements IController{
    
    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        view("parcchargement/parcchargement-home", req, res);
    }
    
    public void edit(HttpServletRequest req, HttpServletResponse res){
        view("parcchargement/parcchargement-edit", req, res);
    }
    
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("parcchargement/parcchargement-add", req, res);
    }
    
     
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json");

        try {
            ParcChargementJpaController cont = new ParcChargementJpaController(Database.getEntityManager());
            ParcChargement var = new ParcChargement(req.getParameter("ref"));
            var.setLibelle(req.getParameter("libelle"));
            var.setRefFournisseur(new FournisseurJpaController(Database.getEntityManager()).findFournisseur(req.getParameter("fournisseur")));
            var.setCertificat(req.getParameter("certificat"));
            var.setRefTypeparc(new TypeParcJpaController(Database.getEntityManager()).findTypeParc(req.getParameter("origine")));
            cont.create(var);
            
            Titre titre  = new Titre();
            titre.setCreateAt(new Date());
            titre.setConcession(req.getParameter("concession"));
            titre.setDateDebut(new Date(Long.parseLong(req.getParameter("dateDebut"))));
            titre.setDateFin(new Date(Long.parseLong(req.getParameter("dateFin"))));
            titre.setNumTitre(req.getParameter("titreRef"));
            titre.setRefParc(var);
            
            
            List<Essence> listeEssence = new LinkedList<>();
            String[] essenceAuth = req.getParameterValues("essence[]");
            EssenceJpaController contEssence = new EssenceJpaController(Database.getEntityManager());
            
            if(essenceAuth != null && essenceAuth.length >0)
                for(String ess : essenceAuth){
                    listeEssence.add(contEssence.findEssence(ess));
                }
            titre.setEssenceList(listeEssence);
            
            new TitreJpaController(Database.getEntityManager()).create(titre);
            
            LinkedList<Titre> listeTitre = new LinkedList<>();
            listeTitre.add(titre);
            var.setTitreList(listeTitre);
            cont.edit(var);
            
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try {
            ParcChargementJpaController cont = new ParcChargementJpaController(Database.getEntityManager());
            ParcChargement var = cont.findParcChargement(req.getParameter("ref"));
            var.setLibelle(req.getParameter("libelle"));
            var.setRefFournisseur(new FournisseurJpaController(Database.getEntityManager()).findFournisseur(req.getParameter("fournisseur")));
//            var.setCertificat(req.getParameter("certificat"));
            var.setRefTypeparc(new TypeParcJpaController(Database.getEntityManager()).findTypeParc(req.getParameter("origine")));
            cont.edit(var);
            
            TitreJpaController titreCont = new TitreJpaController(Database.getEntityManager());
            Titre t = titreCont.findTitre(req.getParameter("titre"));
            
            /**
             * On vide la liste des anciennes essences pour les remettre à jour
             */
            t.getEssenceList().clear();
            String[] essRef = req.getParameterValues("essences[]");
            EssenceJpaController essCont = new EssenceJpaController(Database.getEntityManager());
            for(String essence : essRef){
                t.getEssenceList().add(essCont.findEssence(essence));
            }
            titreCont.edit(t);
            
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try {
            ParcChargementJpaController cont = new ParcChargementJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    public void byCommande(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ParcChargement parcC = ParcChargementJpaController.byCommande(req.getParameter("commande"));
            res.getOutputStream().print(parcC == null ? null : parcC.toString());
        }catch(Exception e){
            try {
                res.getOutputStream().print(null);
            } catch (IOException ex) {}
        }
    }

    public void byFournisseur(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Appliation/json");
        try{
            List<ParcChargement> parc = ParcChargementJpaController.byFournisseur(req.getParameter("fournisseur"));
            parc.forEach(p->{
                //p.initTitre();
            });
            System.out.println("=>>> "+parc);
            res.getOutputStream().print(parc.toString());
        }catch(Exception e){
            try {
                res.getOutputStream().print(null);
            } catch (IOException ex) {}
        }
    }
    @Override
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ParcChargementJpaController control = new  ParcChargementJpaController(Database.getEntityManager());
            ParcChargement b = control.findParcChargement(req.getParameter("ref"));

            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("null");
        }catch(Exception ex){try {
            //res.getOutputStream().print("{d}");
            ex.printStackTrace(new PrintStream(res.getOutputStream()));
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    @Override
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            ParcChargementJpaController control = new  ParcChargementJpaController(Database.getEntityManager());
            List<ParcChargement> list = control.findParcChargementEntities();
            list.forEach(l->l.setTitreList(new LinkedList<>()));
            res.getOutputStream().print(list.toString());
        }catch(Exception ex){try {
            //res.getOutputStream().print("[]");
            ex.printStackTrace(new PrintStream(res.getOutputStream()));
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    @Override
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ParcChargementJpaController control = new  ParcChargementJpaController(Database.getEntityManager());
            List list = control.findParcChargementEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    

}
