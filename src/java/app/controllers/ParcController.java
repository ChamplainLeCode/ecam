/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.ParcJpaController;
import app.modelController.TypeParcJpaController;
import app.models.Database;
import app.models.Parc;
import app.routes.Api;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class ParcController extends Controller implements IController{
   
       
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        view("parc/parc-home", req, res);
    }
    
    public void edit(HttpServletRequest req, HttpServletResponse res){
        view("parc/parc-edit", req, res);
    }
    
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("parc/parc-add", req, res);
    }
    
        public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        try {
            ParcJpaController cont = new ParcJpaController(Database.getEntityManager());
            Parc var = new Parc(req.getParameter("refparc"));
            var.setDateMvt(new Date(Long.parseLong(req.getParameter("dateMvt"))));
            var.setLibelle(req.getParameter("libelle"));
            var.setNumBille(req.getParameter("numBille"));
            var.setRefTypeparc(new TypeParcJpaController(Database.getEntityManager()).findTypeParc(req.getParameter("refTypeparc")));
            var.setTypeMvt(req.getParameter("typeMvt"));
            cont.create(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        try {
            ParcJpaController cont = new ParcJpaController(Database.getEntityManager());
            Parc var = cont.findParc(req.getParameter("ref"));

            var.setDateMvt(new Date(req.getParameter("date")));
            var.setLibelle(req.getParameter("libelle"));
            var.setNumBille(req.getParameter("numBille"));
            var.setRefTypeparc(new TypeParcJpaController(Database.getEntityManager()).findTypeParc(req.getParameter("typeParc")));;
            var.setTypeMvt(req.getParameter("type"));
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        try {
            ParcJpaController cont = new ParcJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }


    
    @Override
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ParcJpaController control = new  ParcJpaController(Database.getEntityManager());
            Parc b = control.findParc(req.getParameter("ref"));

            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    @Override
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            ParcJpaController control = new  ParcJpaController(Database.getEntityManager());
            List<Parc> list =  control.allBilleInParc(); //findParcEntities();
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            ex.printStackTrace(new PrintStream(res.getOutputStream()));
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    @Override
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            ParcJpaController control = new  ParcJpaController(Database.getEntityManager());
            List list = control.findParcEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    public void getMouvementParc(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
           try {
               res.getOutputStream().print(Parc.getMouvementListToString());
           } catch (Exception ex) {
               try {
                   res.getOutputStream().print("[]");
               } catch (IOException ex1) {
               
               }
           }
    }
    
    public void getParcByBille(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        
        try{
            res.getOutputStream().print(new ParcJpaController(Database.getEntityManager()).getParcByBille(req.getParameter("bille")).toString());
        }catch (Exception e){
            try {
                res.getOutputStream().print("[]");
            } catch (IOException ex) {
                
            }
        }
    }

    public void getParcByOrigine(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            res.getOutputStream().print(new ParcJpaController(Database.getEntityManager()).getParcByOrigine(req.getParameterMap()).toString());
        }catch (Exception e){
            try {
                res.getOutputStream().print("[]");
            } catch (IOException ex) {
                
            }
        }
    }

    public void getRecentlyOut(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            res.getOutputStream().print(new ParcJpaController(Database.getEntityManager()).getRecentlyOut());
        }catch (Exception e){
            try {
                res.getOutputStream().print("[]");
            } catch (IOException ex) {
                
            }
        }
    }

}
