/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.VilleJpaController;
import app.models.Database;
import app.models.Ville;
import app.routes.Api;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class VilleController extends Controller implements IController{
    
        
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res) {
        view("contenaire/ville/ville-home", req, res);
    }
    public void add(HttpServletRequest req, HttpServletResponse res) {
        view("contenaire/ville/ville-add", req, res);
    }
    public void edit(HttpServletRequest req, HttpServletResponse res) {
        view("contenaire/ville/ville-edit", req, res);
    }
    
    
    public void create(HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json");
        try {
            VilleJpaController cont = new VilleJpaController(Database.getEntityManager());
            Ville var = new Ville();
            var.setNom(req.getParameter("nom"));
            var.setPays(req.getParameter("pays"));
            var.setRef(System.currentTimeMillis());
            cont.create(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void data(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json");
        try{
            VilleJpaController control = new  VilleJpaController(Database.getEntityManager());
            Ville b = control.findVille(req.getParameter("ref"));
            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json");
        try{
            VilleJpaController control = new  VilleJpaController(Database.getEntityManager());
            List<Ville> list = control.findVilleEntities();
            if(list != null && list.size() >0){
                res.getOutputStream().print(list.toString());
            }else
                res.getOutputStream().print("[l]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[k]");
            ex.printStackTrace();
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            VilleJpaController control = new  VilleJpaController(Database.getEntityManager());
            List list = control.findVilleEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    public void update(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json");
        try {
            VilleJpaController cont = new VilleJpaController(Database.getEntityManager());
            Ville var = new Ville();
            var.setRef(Long.parseLong(req.getParameter("ref")));
            var.setNom(req.getParameter("nom"));
            var.setPays(req.getParameter("pays"));
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    public void delete(HttpServletRequest req, HttpServletResponse res) throws IOException {
            res.setHeader("Content-Type", "Application/json");
        try {
            VilleJpaController cont = new VilleJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    
    
    

}
