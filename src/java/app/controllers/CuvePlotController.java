/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.CuveJpaController;
import app.modelController.CuvePlotJpaController;
import app.modelController.PlotJpaController;
import app.models.Cuve;
import app.models.Database;
import app.models.CuvePlot;
import app.models.Plot;
import app.routes.Api;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class CuvePlotController extends Controller implements IController{
     
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        view("cuveplot/cuveplot-home", req, res);
    }
     public void edit(HttpServletRequest req, HttpServletResponse res){
        view("cuveplot/cuveplot-edit", req, res);
    }
    
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("cuveplot/cuveplot-add", req, res);
    }

    
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        try {

            int taille = Integer.parseInt(req.getParameter("taille"));
            Date dDebut = new Date(Long.parseLong(req.getParameter("dateDebut")));
//            Date fDebut = new Date(Long.parseLong(req.getParameter("dateFin")));
            CuvePlotJpaController cont = new CuvePlotJpaController(Database.getEntityManager());
            CuveJpaController cuveController = new CuveJpaController(Database.getEntityManager());
            Cuve cuve = cuveController.findCuve(req.getParameter("cuve"));

            cuve.setPlein();
            cuveController.edit(cuve);

            
            for(int i=0; i<taille; i++){
                List<Plot> plots = new LinkedList<>();
                // liste de plots definit par N° de travail
                if(req.getParameter("data["+i+"][type]").equals("travail")){
                    String travail = req.getParameter("data["+i+"][value]");
                    plots = new PlotJpaController((Database.getEntityManager())).finPlotByTravail(travail);
                }else if(req.getParameter("data["+i+"][type]").equals("billon")){
                    for(String ref: req.getParameterValues("data["+i+"][value][]"))
                        plots = new PlotJpaController(Database.getEntityManager()).finPlotByBillon(ref);                    
                }else if(req.getParameter("data["+i+"][type]").equals("plot")){
                    for(String ref: req.getParameterValues("data["+i+"][value][]"))
                        plots.add(new PlotJpaController(Database.getEntityManager()).findPlot(ref));
                }

                for(int j=0; j<plots.size(); j++){
                    CuvePlot var = new CuvePlot("CV_"+plots.get(j).getRefPlot()+"_"+i+"-"+j+"_");
                    var.setDateDebut(dDebut);
                    var.setDateFin(null);//fDebut);
                    var.setRefCuve(new CuveJpaController(Database.getEntityManager()).findCuve(req.getParameter("cuve")));
                    var.setRefPlot(plots.get(j));
                        try{cont.create(var);}catch(Exception em){}
                };
                
            }
            
            
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
                ex.printStackTrace(new PrintStream(res.getOutputStream()));
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        try {
            CuvePlotJpaController cont = new CuvePlotJpaController(Database.getEntityManager());
            CuvePlot var = cont.findCuvePlot(req.getParameter("ref"));
            var.setDateDebut(new Date(req.getParameter("dateDebut")));
            var.setDateFin(new Date(req.getParameter("dateFin")));
            var.setRefCuve(new CuveJpaController(Database.getEntityManager()).findCuve(req.getParameter("cuveRef")));
            var.setRefPlot(new PlotJpaController(Database.getEntityManager()).findPlot(req.getParameter("plotRef")));
            
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        try {
            CuvePlotJpaController cont = new CuvePlotJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            CuvePlotJpaController control = new  CuvePlotJpaController(Database.getEntityManager());
            CuvePlot b = control.findCuvePlot(req.getParameter("ref"));

            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            CuvePlotJpaController control = new  CuvePlotJpaController(Database.getEntityManager());
            List list = control.findCuvePlotEntities();

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            ex.printStackTrace(new PrintStream(res.getOutputStream()));
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            CuvePlotJpaController control = new  CuvePlotJpaController(Database.getEntityManager());
            List list = control.findCuvePlotEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    public void getPlotInCuve(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            String list = CuvePlotJpaController.getPlotInCuve(Database.getEntityManager(), req.getParameter("cuve"));
            res.getOutputStream().print(list);
        } catch (IOException ex) {
            try {
                res.getOutputStream().print("[]");
            } catch (IOException ex1) {}
        }
    }

    public void setFinCuisson(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            CuvePlotJpaController cont = new CuvePlotJpaController(Database.getEntityManager());
            String cuveRef = req.getParameter("cuve");
            String travail = req.getParameter("travail");
            
            Date dateFin = new Date();
            
                List<CuvePlot> pl = cont.findByCuve(cuveRef);

                if(travail.equalsIgnoreCase("all"))
                    for(int i=0; i<pl.size(); i++){
                        pl.get(i).setDateFin(dateFin);
                        try{ cont.edit(pl.get(i)); }catch(Exception e){}
                    }
                else
                    pl.forEach((CuvePlot epl)->{
                        if(epl.getRefPlot().getNumTravail().equalsIgnoreCase(travail))
                                            epl.setDateFin(dateFin); 
                        try{ cont.edit(epl); }catch(Exception e){}
                    });
            
                /**
                 * Ici on va controller la cuve pour savoir si elle est vide ou pas malgré le retrait;
                 */
 
                CuveJpaController cuveController = new CuveJpaController(Database.getEntityManager());
                Cuve cuve = cuveController.findCuve(cuveRef);
                int nbrPlotInCuve = cont.getCuvePlotCountInCuve(cuve);
                System.out.println("nbr = "+nbrPlotInCuve);
                if(nbrPlotInCuve == 0){
                    cuve.setVide();
                    cuveController.edit(cuve);
                }
                res.getOutputStream().print("{\"result\": true}");
                
        }catch(Exception e){
            try {
                res.getOutputStream().print("{\"result\": false}");
                e.printStackTrace(new PrintStream(res.getOutputStream()));
            } catch (IOException ex) {}
        }
    }

    /**
     * Ici on doit renvoyer les plot qui sont déjà cuits, donc sortis de la cuve
     * @param req instance of server request
     * @param res instance of server response
     */
    
    public static void getPlotForTranche(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            CuvePlotJpaController cont = new CuvePlotJpaController(Database.getEntityManager());
            
            String list = cont.getPlotForTranche();
            
            res.getOutputStream().print(list);
                
        }catch(Exception e){
            try {
                res.getOutputStream().print("{\"result\": false}");
                e.printStackTrace(new PrintStream(res.getOutputStream()));
            } catch (IOException ex) {}
        }
    }
}
