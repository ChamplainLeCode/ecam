/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.DetailReceptionJpaController;
import app.modelController.ParcJpaController;
import app.modelController.ReceptionJpaController;
import app.modelController.TypeParcJpaController;
import app.models.Database;
import app.models.DetailReception;
import app.models.Parc;
import app.routes.Api;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kodvel.core.controller.Controller;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class DetailReceptionController extends Controller implements IController{

    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        
    }
    
    public static String getVolumeForBille(String numBille) {
        return new DetailReceptionJpaController(Database.getEntityManager()).getVolumeBille(numBille);
    }
    
    @Override
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        try {
            DetailReceptionJpaController cont = new DetailReceptionJpaController(Database.getEntityManager());
            DetailReception var = new DetailReception(req.getParameter("ref"));
            var.setDiaMoyen(Float.parseFloat(req.getParameter("moyDiam")));
            var.setGdDiam(Float.parseFloat(req.getParameter("gdDiam")));
            var.setLongueur(Double.valueOf(Long.parseLong(req.getParameter("longueur"))));
            var.setNbre(Double.valueOf(Long.parseLong(req.getParameter("nombre"))));
            var.setPtitDiam(Float.parseFloat(req.getParameter("pttDiam")));
            var.setRefReception(new ReceptionJpaController(Database.getEntityManager()).findReception(req.getParameter("receptionRef")));
            var.setVolume(Double.parseDouble(req.getParameter("volume")));
            
            cont.create(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        try {
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        try {
            DetailReceptionJpaController cont = new DetailReceptionJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    public void getNotReceived(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        
        EntityManager em = Database.getEntityManager().createEntityManager();
        try{
            Query q = em.createQuery("select drecept from DetailReception drecept, Reception recept "
                    + " where recept.refCommande.refCommande = :refCommande "
                    + " and recept.refReception = drecept.refReception.refReception "
                    + " and drecept.numBille not in (select dcmd.numBille from  DetailCommande dcmd where dcmd.refCommande.refCommande = :refCmd)"
                    + "", DetailReception.class);
            q.setParameter("refCommande", req.getParameter("commande"));
            q.setParameter("refCmd", req.getParameter("commande"));
            List<DetailReception> list = q.getResultList();
            
            int somme = 0;
            if(list != null && list.size() > 0){
                res.getOutputStream().print(list.toString());
            }
        } catch (IOException ex) {
            try {
                res.getOutputStream().print("[]");
            } catch (IOException ex1) {}
        }finally{
            em.close();
        }
    }
    public void getByBille(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        
        try{
            DetailReception bille = DetailReceptionJpaController.getByBille(req.getParameter("bille"));
            if(bille == null){
                res.getOutputStream().print(null);
            }else
                res.getOutputStream().print(bille.toString());
        }catch(Exception e){
            try {
                res.getOutputStream().print(null);
            } catch (IOException ex) {}
        }
    }
    
    public void essenceByNumBille(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        
        try{
            app.models.Essence essence = DetailReceptionJpaController.essenceByNumBille(req.getParameter("bille"));
            System.out.println("Essence = "+essence);
            res.getOutputStream().print(essence.toString());
        }catch(Exception e){
            try {
                res.getOutputStream().print(null);
            } catch (IOException ex) {}
        }
    }

    public void getByCommande(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type","Application/json");
        EntityManager em = Database.getEntityManager().createEntityManager();
        try{
            /**
             * Ici on récupère toutes les billes receptionnées d'une reception liée à une commande spécifique
             */
            Query q = em.createQuery("SELECT drecept FROM DetailReception drecept WHERE drecept.refReception.refReception LIKE :commande "
                    + " order by drecept.dateReception desc", DetailReception.class);
            q.setParameter("commande", req.getParameter("commande")+"%");
            List<DetailReception> list = q.getResultList();
            
            int somme = 0;
            if(list != null && list.size() > 0){
                res.getOutputStream().print(list.toString());
            }else{
                res.getOutputStream().print(new LinkedList<>().toString());
            }
        } catch (IOException ex) {
            try {
                res.getOutputStream().print("[]");
            } catch (IOException ex1) {}
        }finally{
            em.close();
        }
        
    }
    
    /**
     * Cette fonction permet de retourner les billes faisant partir de la commande <b>req.getParameter("commande")</b>
     *  mais qui n'ont pas été receptionnées
     * 
     * @param req an instance of server request, we will use the param <b>commande</b> in this set
     * @param res an instance of server response
     * 
     * 
     */
    public void getBilleNonReceptionnees(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type","Application/json");
        EntityManager em = Database.getEntityManager().createEntityManager();
        try{
            Query qExclus = em.createQuery("select cmd from DetailCommande cmd where cmd.refCommande.refCommande = :refCommande "
                    + "and cmd.numBille not in (SELECT drecept.numBille FROM DetailReception drecept WHERE drecept.refReception.refReception LIKE :commande)");
            qExclus.setParameter("refCommande", req.getParameter("commande"));
            qExclus.setParameter("commande", req.getParameter("commande")+"%");
            res.getOutputStream().print(qExclus.getResultList().toString());
        }catch(Exception e){
            try {
                res.getOutputStream().print("[]");
            } catch (IOException ex) {}
        } finally{
            if(em != null)
                em.close();
        }
    }

    @Override
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            DetailReceptionJpaController control = new  DetailReceptionJpaController(Database.getEntityManager());
            DetailReception b = control.findDetailReception(req.getParameter("ref"));
            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print(null);
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    @Override
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            DetailReceptionJpaController control = new  DetailReceptionJpaController(Database.getEntityManager());
            List list = control.findDetailReceptionEntities();
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    @Override
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            DetailReceptionJpaController control = new  DetailReceptionJpaController(Database.getEntityManager());
            List list = control.findDetailReceptionEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    public void setNumTravailForBille(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        
        try{
            EntityManagerFactory emf = Database.getEntityManager();
            ParcJpaController cont = new ParcJpaController(emf);
            
            Date d = new Date();
            
            
            /**
             * On désactive les précédentes entrées de cette bille dans le parc
             */
            List<Parc> parcs = cont.getParcsByBille(req.getParameter("refBille[numBille]"));
            parcs.forEach(p->{
                try {
                    p.hide();
                    cont.edit(p);
                } catch (Exception ex) {
                    ex.printStackTrace(System.err);
                }
            });
            
            Parc p = new Parc(req.getParameter("refBille[ref]")+d.hashCode());
            p.show();
            p.setDateMvt(d);
            p.setLibelle(Parc.MVT_MISE_PRODUCTION);
            p.setNumBille(req.getParameter("refBille[numBille]"));
            p.setTypeMvt(Parc.MVT_MISE_PRODUCTION);
            p.setRefTypeparc(new TypeParcJpaController(emf).findTypeParc(req.getParameter("refBille[refTypeparc][ref]")));
            p.setRefTypeparcDepart(new TypeParcJpaController(emf).findTypeParc(req.getParameter("refBille[refTypeparcDepart][ref]")));
            cont.create(p);
            
            DetailReception bille = DetailReceptionJpaController.getByBille(req.getParameter("refBille[numBille]"));
            bille.setNumtravail(req.getParameter("travail"));
            
            new DetailReceptionJpaController(emf).edit(bille);
            res.getOutputStream().print("{\"result\": \"true\"}");
        }catch(Exception e){
            try {
                res.getOutputStream().print("{\"result\": \"false\", \"reason\": \""+e.getMessage()+"\"}");
            } catch (IOException ex) {}
        }
    }

    public void setRetourFournisseurForBille(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        
        try{
            EntityManagerFactory emf = Database.getEntityManager();
            ParcJpaController cont = new ParcJpaController(emf);

            Date d = new Date();

            /**
             * On désactive les précédentes entrées de cette bille dans le parc
             */
            List<Parc> parcs = cont.getParcsByBille(req.getParameter("refBille[numBille]"));
            parcs.forEach(p->{
                try {
                    p.hide();
                    cont.edit(p);
                } catch (Exception ex) {
                    ex.printStackTrace(System.err);
                }
            });
            
            Parc p = new Parc(req.getParameter("refBille[ref]")+d.hashCode());
            p.setDateMvt(d);
            p.setLibelle(Parc.MVT_RETOUR_FOUNISSEUR);
            p.setNumBille(req.getParameter("refBille[numBille]"));
            p.setTypeMvt(Parc.MVT_RETOUR_FOUNISSEUR);
            p.setRefTypeparc(new TypeParcJpaController(emf).findTypeParc(req.getParameter("refBille[refTypeparc][ref]")));
            p.setRefTypeparcDepart(new TypeParcJpaController(emf).findTypeParc(req.getParameter("refBille[refTypeparcDepart][ref]")));
            cont.create(p);
            /*
                DetailReception bille = DetailReceptionJpaController.getByBille(req.getParameter("refBille[numBille]"));
                bille.setNumtravail(req.getParameter("travail"));

                new DetailReceptionJpaController(emf).edit(bille);
            */
            res.getOutputStream().print("{\"result\": \"true\"}");
        }catch(Exception e){
            try {
                res.getOutputStream().print("{\"result\": \"false\", \"reason\": \""+e.getMessage()+"\"}");
            } catch (IOException ex) {}
        }
    }
    

}
