/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.EquipeJpaController;
import app.modelController.PlotJpaController;
import app.modelController.TrancheJpaController;
import app.modelController.TrancheMachineJpaController;
import app.models.Database;
import app.models.Plot;
import app.models.Tranche;
import app.routes.Api;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class TrancheController extends Controller implements IController{
      
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        view("tranche/tranche-home", req, res);
    }
    
    public void fiche(HttpServletRequest req, HttpServletResponse res){
        view("tranche/tranche-fiche", req, res);
    }
    
    public void edit(HttpServletRequest req, HttpServletResponse res){
        view("tranche/tranche-edit", req, res);
    }
    
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("tranche/tranche-add", req, res);
    }
    

    
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        try {
            TrancheJpaController cont = new TrancheJpaController(Database.getEntityManager());
            Tranche var = new Tranche(req.getParameter("ref"));
            Date dd;
            var.setDateDebut(dd = new Date(Long.parseLong(req.getParameter("dateDebut"))));
            Date df;
            var.setMachine(new TrancheMachineJpaController(Database.getEntityManager()).findTrancheMachine(req.getParameter("machine")));
            var.setEpaisseur(req.getParameter("epaisseur"));
            var.setNumTravail(req.getParameter("travail"));
            var.setRefEquipe(new EquipeJpaController(Database.getEntityManager()).findEquipe(req.getParameter("equipe")));
            var.setRefPlot(new PlotJpaController(Database.getEntityManager()).findPlot(req.getParameter("plot")));

            Plot plot = var.getRefPlot();
            plot.setFace1(Integer.parseInt(req.getParameter("face1")));
            plot.setFace2(Integer.parseInt(req.getParameter("face2")));
            plot.setFace3(Integer.parseInt(req.getParameter("face3")));
            
            new PlotJpaController(Database.getEntityManager()).edit(plot);
            cont.create(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        try {
            TrancheJpaController cont = new TrancheJpaController(Database.getEntityManager());
            Tranche var = cont.findTranche(req.getParameter("ref"));
            Date dd;
            var.setDateDebut(dd = new Date(req.getParameter("dateDebut")));
            var.setEpaisseur(req.getParameter("epaisseur"));
            var.setNumTravail(req.getParameter("numTravail"));
            var.setRefEquipe(new EquipeJpaController(Database.getEntityManager()).findEquipe(req.getParameter("equipe")));
            var.setRefPlot(new PlotJpaController(Database.getEntityManager()).findPlot(req.getParameter("plot")));
            new PlotJpaController(Database.getEntityManager()).edit(var.getRefPlot());
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        try {
            TrancheJpaController cont = new TrancheJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    @Override
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            TrancheJpaController control = new  TrancheJpaController(Database.getEntityManager());
            Tranche b = control.findTranche(req.getParameter("ref"));
            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    @Override
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            TrancheJpaController control = new  TrancheJpaController(Database.getEntityManager());
            List list = control.findTrancheEntities();
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){
            try {
                res.getOutputStream().print("[]");
            } catch (IOException ex1) {}
        }
    }
    
    @Override
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            TrancheJpaController control = new  TrancheJpaController(Database.getEntityManager());
            List list = control.findTrancheEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){
            try {
                res.getOutputStream().print("[]");
            } catch (IOException ex1) {}
        }
    }

    public void getPlotForTranche(HttpServletRequest req, HttpServletResponse res){
        CuvePlotController.getPlotForTranche(req, res);
    }

    public void getEpaisseurForPlot(HttpServletRequest req, HttpServletResponse res){
        TrancheJpaController.getEpaisseurForPlot(req, res);
    }

}
