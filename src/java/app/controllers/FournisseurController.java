/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.CertificatJpaController;
import app.modelController.FournisseurJpaController;
import app.modelController.ParcChargementJpaController;
import app.modelController.exceptions.NonexistentEntityException;
import app.models.Certificat;
import app.models.Database;
import app.models.Fournisseur;
import app.models.ParcChargement;
import app.routes.Api;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class FournisseurController extends Controller  implements IController{
    
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json");
        
        try {
            FournisseurJpaController cont = new FournisseurJpaController(Database.getEntityManager());
            Fournisseur var = new Fournisseur(req.getParameter("ref"));
            var.setContact(req.getParameter("contact"));
            var.setContribuable(req.getParameter("contribuable"));
            var.setNomFournisseur(req.getParameter("nom"));

            cont.create(var);
            
            
            /* 
                for(int i=0; i<Integer.parseInt(req.getParameter("tailleCertificat")); i++){
                    Certificat p = new Certificat("REF_CERT_"+new Date().hashCode()+'/'+i, req.getParameterValues("certificats[]")[i], var.getRefFournisseur());
                    new CertificatJpaController(Database.getEntityManager()).create(p);
                    System.out.println(p);
                }


                List<ParcChargement> l = new LinkedList<>();

                for(int i=0; i<Integer.parseInt(req.getParameter("taille")); i++){
                    ParcChargement p = new ParcChargement();
                    p.setRefParc("REF_PARC_"+new Date().hashCode()+'/'+i);
                    p.setLibelle(req.getParameterValues("parcs[]")[i]);
                    p.setRefFournisseur(var);
                    System.out.println("Parc "+i+" -> "+req.getParameterValues("parcs[]")[i]);
                    l.add(p);
                }
                l.forEach((ParcChargement p)->{
                    try {
                        new ParcChargementJpaController(Database.getEntityManager()).create(p);
                    } catch (Exception ex) {}
                });
                var.setParcChargementList(l);
                cont.edit(var);
            */
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        req.getParameterMap().forEach((String key, String[] v)->{
            System.out.println("Key = "+key+" val = "+v[0]);
        });
        try {
            FournisseurJpaController cont = new FournisseurJpaController(Database.getEntityManager());
            Fournisseur var = cont.findFournisseur(req.getParameter("ref"));
            var.setContact(req.getParameter("contact"));
            var.setContribuable(req.getParameter("contribuable"));
            var.setNomFournisseur(req.getParameter("nom"));

            CertificatJpaController contCertif = new CertificatJpaController(Database.getEntityManager());
            List<Certificat> li = Certificat.getCertificatFor(var.getRefFournisseur());
            if(li != null){
                li.forEach((Certificat e)->{try {contCertif.destroy(e.getIdentifiant());} catch (NonexistentEntityException ex) {}});
            }
            for(int i=0; i<Integer.parseInt(req.getParameter("tailleCertificat")); i++){
                Certificat p = new Certificat("REF_CERT_"+new Date().hashCode()+'/'+i, req.getParameterValues("certificats[]")[i], var.getRefFournisseur());
                new CertificatJpaController(Database.getEntityManager()).create(p);
            }

            ParcChargementJpaController contParcs = new ParcChargementJpaController(Database.getEntityManager());
            
            List<ParcChargement> list = contParcs.findParcChargementByFournisseur(var.getRefFournisseur());
            
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try {
            FournisseurJpaController cont = new FournisseurJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("refFournisseur"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    public void add(HttpServletRequest req, HttpServletResponse res){
        view("fournisseurs/fournisseur-add", req, res);
    }
    
    public void edit(HttpServletRequest req, HttpServletResponse res){
        view("fournisseurs/fournisseur-edit", req, res);
        
    }
    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        view("fournisseurs/fournisseur-list", req, res);       
    }
    
    public void detail(HttpServletRequest req, HttpServletResponse res){
        //Fournisseur fournisseur = new FournisseurJpaController((Database.getEntityManager())).findFournisseur(req.getParameter("ref").toString());
        //List<Commande> list = fournisseur.getCommandeList();
        //req.setAttribute("fournisseur", fournisseur);
        //req.setAttribute("listeCommandes", list);
        
        view("fournisseurs/fournisseur-detail", req, res);
    }
    
    public void card(HttpServletRequest req, HttpServletResponse res) {
        try{
            String commande = req.getParameter("refFournisseur");
            Fournisseur cmd = new FournisseurJpaController(Database.getEntityManager()).findFournisseur(commande);
            cmd.setParcChargementList(new ParcChargementJpaController(Database.getEntityManager()).findParcChargementByFournisseur(cmd.getRefFournisseur()));
            if(cmd != null) {
                req.setAttribute("fournisseur", cmd);
            }
            view("fournisseurs/card", req, res);
        }catch(Exception ex){}finally{
            view("fournisseurs/card", req, res);
            
        }
    }
    
    public void search(HttpServletRequest req, HttpServletResponse res){
        detail(req, res);
    }
    
    
    
    @Override
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            FournisseurJpaController control = new  FournisseurJpaController(Database.getEntityManager());
            Fournisseur b = control.findFournisseur(req.getParameter("ref"));

            if(b != null)
                res.getOutputStream().print(b.baseString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    @Override
    public void dataList (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        String origin = req.getParameter("origine") == null ? "all": req.getParameter("origine");
        String dateD = req.getParameter("dateDebut");
        String dateF = req.getParameter("dateFin");
        long dateDebut, dateFin;
        
        dateDebut = dateD == null ? 0 : Long.parseLong(dateD);
        dateFin = dateD == null ? 0 : Long.parseLong(dateF);
        try{
            EntityManagerFactory db = Database.getEntityManager();
            FournisseurJpaController control = new  FournisseurJpaController(db);
            List<Fournisseur> list = control.findFournisseurEntities(origin, dateDebut, dateFin);
            list.forEach((Fournisseur e)->{
                e.setParcChargementList(new ParcChargementJpaController(db).findParcChargementByFournisseur(e.getRefFournisseur()));
            });
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    @Override
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            FournisseurJpaController control = new  FournisseurJpaController(Database.getEntityManager());
            List list = control.findFournisseurEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    

    
}
