/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.ColisJpaController;
import app.modelController.ContennaireJpaController;
import app.modelController.DetailCommandeJpaController;
import app.modelController.DetailReceptionJpaController;
import app.modelController.PaquetJpaController;
import app.modelController.ParcChargementJpaController;
import app.models.Colis;
import app.models.Database;
import app.models.DetailCommande;
import app.models.DetailReception;
import app.models.Embarquement;
import app.models.Fournisseur;
import app.models.Paquet;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;

/**
 *
 * @author champlain
 */
public class StockController extends Controller implements IController{

    @Override
    public void start(HttpServletRequest req, HttpServletResponse res) {
        view("stock/stock-home", req, res);
    }
    

    public void ficheRendement(HttpServletRequest req, HttpServletResponse res) {
        view("stock/fiche-rendement", req, res);
    }
    
    public static HashMap getBilleIntoEmbarquement(String numPlomb){
        Embarquement embarquement = new ContennaireJpaController(Database.getEntityManager()).findContennaire(numPlomb);
        if(embarquement == null)
            return null;
        
        List<DetailReception> liste = new LinkedList<>();
        List<DetailCommande> listeDC = new LinkedList<>();
        
        List<String> listNumTravail = new LinkedList<>();
        
        embarquement.setColisList(new ColisJpaController(Database.getEntityManager()).colisForContenaire(numPlomb));
        
        embarquement.getColisList().forEach((c) -> {
            listNumTravail.addAll(new ColisJpaController(Database.getEntityManager()).numTravailIn(c));
        });
        
        DetailReceptionJpaController cont = new DetailReceptionJpaController(Database.getEntityManager());
        DetailCommandeJpaController contDC = new DetailCommandeJpaController(Database.getEntityManager());

        listNumTravail.forEach(e ->{
            DetailReception b;
            liste.add(b = cont.findDetailReceptionByNumTravail(e)); 
            listeDC.add(contDC.getDetailCommandeByNumBille(b.getNumBille()));
        });

        HashMap map = new HashMap();
        
        map.put("dc", listeDC);
        map.put("dr", liste);
        return map;
    }
    
    @Override
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void home(HttpServletRequest req, HttpServletResponse res) {
        view("contenaire/contenaire-home", req, res);
    }

    public void billon(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        Date debut = new Date(Long.parseLong(req.getParameter("debut")));
        Date fin = new Date(Long.parseLong(req.getParameter("fin")));
        res.getOutputStream().print(this.findData(debut, fin, "Billon").toString());
        
    }

    public void billonnage(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        Date debut = new Date(Long.parseLong(req.getParameter("debut")));
        Date fin = new Date(Long.parseLong(req.getParameter("fin")));
        res.getOutputStream().print(this.findData(debut, fin, "Billonnage").toString());
    }
    
    public void client(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        Date debut = new Date(Long.parseLong(req.getParameter("debut")));
        Date fin = new Date(Long.parseLong(req.getParameter("fin")));
        res.getOutputStream().print(this.findData(debut, fin, "Client").toString());
    }

    public void colis(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        Date debut = new Date(Long.parseLong(req.getParameter("debut")));
        Date fin = new Date(Long.parseLong(req.getParameter("fin")));
        res.getOutputStream().print(this.findData(debut, fin, "Colis").toString());
    }

    public void cuve(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        Date debut = new Date(Long.parseLong(req.getParameter("debut")));
        Date fin = new Date(Long.parseLong(req.getParameter("fin")));
        res.getOutputStream().print(this.findData(debut, fin, "Cuve").toString());
    }

    public void embarquement(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        Date debut = new Date(Long.parseLong(req.getParameter("debut")));
        Date fin = new Date(Long.parseLong(req.getParameter("fin")));
        res.getOutputStream().print(this.findData(debut, fin, "Embarquement").toString());
    }

    public void empotage(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        Date debut = new Date(Long.parseLong(req.getParameter("debut")));
        Date fin = new Date(Long.parseLong(req.getParameter("fin")));
        res.getOutputStream().print(this.findData(debut, fin, "Empotage").toString());
    }

    public void fournisseur(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        Date debut = new Date(Long.parseLong(req.getParameter("debut")));
        Date fin = new Date(Long.parseLong(req.getParameter("fin")));
        List<Fournisseur> l = this.findData(debut, fin, "Fournisseur");
        l.forEach((Fournisseur e) -> {
            e.setParcChargementList(new ParcChargementJpaController(Database.getEntityManager()).findParcChargementByFournisseur(e.getRefFournisseur()));
        });
        res.getOutputStream().print(l.toString());
    }

    public void lettre(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        Date debut = new Date(Long.parseLong(req.getParameter("debut")));
        Date fin = new Date(Long.parseLong(req.getParameter("fin")));
        res.getOutputStream().print(this.findData(debut, fin, "LettreCamion").toString());
    }

    public void paquet(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        Date debut = new Date(Long.parseLong(req.getParameter("debut")));
        Date fin = new Date(Long.parseLong(req.getParameter("fin")));
        res.getOutputStream().print(this.findData(debut, fin, "Paquet").toString());
    }

    public void plot(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        Date debut = new Date(Long.parseLong(req.getParameter("debut")));
        Date fin = new Date(Long.parseLong(req.getParameter("fin")));
        res.getOutputStream().print(this.findData(debut, fin, "Plot").toString());
    }

    public void parc(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        Date debut = new Date(Long.parseLong(req.getParameter("debut")));
        Date fin = new Date(Long.parseLong(req.getParameter("fin")));
        res.getOutputStream().print(this.findData(debut, fin, "Parc").toString());
    }
    
    @Override
    public void data(HttpServletRequest req, HttpServletResponse res) throws IOException {

    }

    @Override
    public void dataList(HttpServletRequest req, HttpServletResponse res) throws IOException {

    }

    @Override
    public void dataListPage(HttpServletRequest req, HttpServletResponse res) throws IOException {

    }

    @Override
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {

    }

    @Override
    public void update(HttpServletRequest req, HttpServletResponse res) throws IOException {

    }

    @Override
    public void delete(HttpServletRequest req, HttpServletResponse res) throws IOException {

    }
    
    protected List findData(Date debut, Date fin, String type){
        String request ="";
        try{
            EntityManager em = Database.getEntityManager().createEntityManager();
            try{
                Query q = em.createQuery("from "+type+" b where b.create_at BETWEEN :dateDebut and :dateFin", Class.forName("app.models."+type));
                q.setParameter("dateDebut", debut);
                q.setParameter("dateFin", fin);
                return q.getResultList();
            }catch(java.lang.IllegalArgumentException e){
                Query q = em.createQuery("from "+type+" b where b.createAt BETWEEN :dateDebut and :dateFin", Class.forName("app.models."+type));
                q.setParameter("dateDebut", debut);
                q.setParameter("dateFin", fin);
                return q.getResultList();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
        
    }
    
    private static Timestamp localToTimeStamp(LocalDate date){
        return Timestamp.from(date.atStartOfDay().toInstant(ZoneOffset.UTC));
    }
    
    
}
