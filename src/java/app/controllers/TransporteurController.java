/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.TransporteurJpaController;
import app.models.Transporteur;
import app.models.Database;
import app.routes.Api;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class TransporteurController  extends Controller implements IController{
    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    
    public void start(HttpServletRequest req, HttpServletResponse res){
        view("transporteur/transporteur-home", req, res);
    }
    
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("transporteur/transporteur-add", req, res);
    }
    
    public void edit(HttpServletRequest req, HttpServletResponse res){
        view("transporteur/transporteur-edit", req, res);
    }
    
    public void create(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try {
            TransporteurJpaController cont = new TransporteurJpaController(Database.getEntityManager());
            Transporteur var = new Transporteur(req.getParameter("ref"));
            var.setNumContribuable(req.getParameter("contribuable"));
            var.setNom(req.getParameter("nom"));
            cont.create(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    @Override
    public void data(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setHeader("Content-Type", "Application/json");
        try{
            TransporteurJpaController control = new  TransporteurJpaController(Database.getEntityManager());
            Transporteur b = control.findTransporteur(req.getParameter("ref"));
            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            TransporteurJpaController control = new  TransporteurJpaController(Database.getEntityManager());
            List list = control.findTransporteurEntities();
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            TransporteurJpaController control = new  TransporteurJpaController(Database.getEntityManager());
            List list = control.findTransporteurEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    @Override
    public void update(HttpServletRequest req, HttpServletResponse res) throws IOException {
        
        res.setHeader("Content-Type", "Application/json");
        try {
            TransporteurJpaController cont = new TransporteurJpaController(Database.getEntityManager());
            Transporteur var = new Transporteur(req.getParameter("ref"));
            var.setNumContribuable(req.getParameter("contribuable"));
            var.setNom(req.getParameter("nom"));
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    @Override
    public void delete(HttpServletRequest req, HttpServletResponse res) throws IOException {
        
        res.setHeader("Content-Type", "Application/json");
        try {
            TransporteurJpaController cont = new TransporteurJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    

}
