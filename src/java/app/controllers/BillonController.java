/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.BillonJpaController;
import app.modelController.BillonnageJpaController;
import app.models.Database;
import app.models.Billon;
import app.routes.Api;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class BillonController extends Controller implements IController{
    
    
    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        view("Billon/billon-home", req, res); 
    }
    
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("Billon/billon-add", req, res);
    }
    
    public void edit(HttpServletRequest req, HttpServletResponse res){
        view("Billon/billon-edit", req, res);
    }
     
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        try {
            BillonJpaController cont = new BillonJpaController(Database.getEntityManager());
            Billon var = new Billon(req.getParameter("ref"));
            //var.setGdDiam(Double.parseDouble(req.getParameter("gdDiam")));
            var.setLongueur(Integer.parseInt(req.getParameter("longueur")));
            var.setNumTravail(req.getParameter("travail"));
            //var.setPtitDiam(Double.parseDouble(req.getParameter("pttDiam")));
            var.setRefBillonnage(new BillonnageJpaController(Database.getEntityManager()).findBillonnage(req.getParameter("billonnage")));
            var.setVolume(Double.parseDouble(req.getParameter("volume")));
            
            cont.create(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        try {
            BillonJpaController cont = new BillonJpaController(Database.getEntityManager());
            Billon var = cont.findBillon(req.getParameter("ref"));
            //var.setGdDiam(Double.parseDouble(req.getParameter("gdDiam")));
            var.setLongueur(Integer.parseInt(req.getParameter("longueur")));
            var.setNumTravail(req.getParameter("travail"));
            //var.setPtitDiam(Double.parseDouble(req.getParameter("pttDiam")));
            var.setRefBillonnage(new BillonnageJpaController(Database.getEntityManager()).findBillonnage(req.getParameter("billonnage")));
            var.setVolume(Double.parseDouble(req.getParameter("volume")));
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        try {
            BillonJpaController cont = new BillonJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    

    
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            BillonJpaController control = new  BillonJpaController(Database.getEntityManager());
            Billon b = control.findBillon(req.getParameter("ref"));
            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            BillonJpaController control = new  BillonJpaController(Database.getEntityManager());
            List list = control.findBillonEntities();
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json");
        try{
            BillonJpaController control = new  BillonJpaController(Database.getEntityManager());
            List list = control.findBillonEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);
            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void billonFromBille(HttpServletRequest req, HttpServletResponse res){
        
        res.setHeader("Content-Type", "Application/json");
        
        try{
            List liste = BillonJpaController.findByBilleNum(Database.getEntityManager().createEntityManager(), req.getParameter("bille"));
            res.getOutputStream().print(new JSONArray(liste).toString());
        }catch(IOException e){
            try{
                res.getOutputStream().print("[]");
            }catch(IOException ex){}
        }
    }
    
    
    public void billonFromBilleForCoupe(HttpServletRequest req, HttpServletResponse res){
        
        res.setHeader("Content-Type", "Application/json");
        
        try{
            List liste = BillonJpaController.findByBilleNumForCoupe(Database.getEntityManager().createEntityManager(), req.getParameter("bille"));
            res.getOutputStream().print(new JSONArray(liste).toString());
        }catch(IOException e){
            try{
                res.getOutputStream().print("[]");
            }catch(IOException ex){}
        }
    }
    
    public void billonForMassicot(HttpServletRequest req, HttpServletResponse res){
        
        res.setHeader("Content-Type", "Application/json");
        
        try{
            List liste = BillonJpaController.findBillonForMassicot(Database.getEntityManager().createEntityManager());
            res.getOutputStream().print(new JSONArray(liste).toString());
        }catch(Exception e){
            try{
                res.getOutputStream().print("[]");
            }catch(IOException ex){}
        }
    }

    public void billeForMassicot(HttpServletRequest req, HttpServletResponse res){
        
        res.setHeader("Content-Type", "Application/json");
        
        try{
            List liste = BillonJpaController.findBilleForMassicot(Database.getEntityManager().createEntityManager());
            res.getOutputStream().print(new JSONArray(liste).toString());
            System.out.println("<<<<<<<<= ici =>>>>>>>>>");
        }catch(Exception e){
            try{
                e.printStackTrace();
                res.getOutputStream().print("[]");
            }catch(IOException ex){
                ex.printStackTrace();
            }
        }
    }
}
