/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.PaquetJpaController;
import app.modelController.PlotJpaController;
import app.models.Database;
import app.models.Paquet;
import app.routes.Api;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class PaquetController extends Controller implements IController{
    
    
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        view("paquet/paquet-home", req, res);
    }
    
    public void edit(HttpServletRequest req, HttpServletResponse res){
        view("paquet/paquet-edit", req, res);
    }
    
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("paquet/paquet-add", req, res);
    }
    
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        try {
            PaquetJpaController cont = new PaquetJpaController(Database.getEntityManager());
            Paquet var = new Paquet(req.getParameter("ref"));
            var.setNbreFeuille(Integer.parseInt(req.getParameter("nbrFeuilles")));
            var.setNumTravail(req.getParameter("numTravail"));
            var.setRefPlot(new PlotJpaController(Database.getEntityManager()).findPlot(req.getParameter("plot")));
            cont.create(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        try {
            PaquetJpaController cont = new PaquetJpaController(Database.getEntityManager());
            Paquet var = cont.findPaquet(req.getParameter("ref"));
            var.setNbreFeuille(Integer.parseInt(req.getParameter("nbrFeuilles")));
            var.setNumTravail(req.getParameter("numTravail"));
            var.setRefPlot(new PlotJpaController(Database.getEntityManager()).findPlot(req.getParameter("plot")));
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        try {
            PaquetJpaController cont = new PaquetJpaController(Database.getEntityManager());
            cont.destroy(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }


    @Override
    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            PaquetJpaController control = new  PaquetJpaController(Database.getEntityManager());
            Paquet b = control.findPaquet(req.getParameter("ref"));

            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    @Override
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            PaquetJpaController control = new  PaquetJpaController(Database.getEntityManager());
            List list = control.findPaquetEntities();

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            ex.printStackTrace(new PrintStream(res.getOutputStream()));
            
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    @Override
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            PaquetJpaController control = new  PaquetJpaController(Database.getEntityManager());
            List list = control.findPaquetEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    

}
