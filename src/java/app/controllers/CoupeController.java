/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.modelController.BillonJpaController;
import app.modelController.CoupeJpaController;
import app.modelController.PlotJpaController;
import app.modelController.TypeCoupeJpaController;
import app.models.Coupe;
import app.models.Database;
import app.models.Plot;
import app.routes.Api;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kodvel.core.controller.Controller;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class CoupeController extends Controller implements IController{
    
     
    public void index(HttpServletRequest req, HttpServletResponse res){
        super.index(req, res);
    }
    public void start(HttpServletRequest req, HttpServletResponse res){
        view("coupe/coupe-home", req, res);
    }
    
    public void edit(HttpServletRequest req, HttpServletResponse res){
        view("coupe/coupe-edit", req, res);
    }
    
    public void add(HttpServletRequest req, HttpServletResponse res){
        view("coupe/coupe-add", req, res);
    }
    
    
    public void create(HttpServletRequest req, HttpServletResponse res) throws IOException {
        try {
            CoupeJpaController cont = new CoupeJpaController(Database.getEntityManager());
            Coupe var = new Coupe(req.getParameter("ref"));
            var.setNbrePlot(Integer.parseInt(req.getParameter("nombrePlot")));
            var.setRefBillon(new BillonJpaController(Database.getEntityManager()).findBillon(req.getParameter("billon")));
            var.setRefCoupe(var.getRefBillon().getRefBillon()+'|'+var.getNbrePlot());
            var.setRefTypecoupe(new TypeCoupeJpaController(Database.getEntityManager()).findTypeCoupe(req.getParameter("typeCoupe")));
            cont.create(var);
            
            PlotJpaController contPlot = new PlotJpaController(Database.getEntityManager());
            List liste = new LinkedList();
            
            for(int i=0; i<var.getNbrePlot(); i++){
                Plot p = new Plot();
                p.setLongueur(Integer.parseInt(req.getParameter("data["+i+"][longueur]")));
                p.setFace1(0);
                p.setFace2(0);
                p.setFace3(0);
                p.setFaceMN(p.getLongueur() > 450 ? "M" : "N" );
                p.setRefCoupe(var);
                p.setRefBillon(var.getRefBillon());
                p.setNumTravail(p.getRefBillon().getNumTravail());
                p.setRefPlot(var.getRefBillon().getNumTravail()+var.getRefBillon().getRefBillon().substring(var.getRefBillon().getRefBillon().lastIndexOf("-"))+'-'+(i+1));//req.getParameter("data["+i+"][ref]"));
                contPlot.create(p);
                System.out.println(p);
                liste.add(p);
            }
            
            var.setPlotList(liste);
            cont.edit(var);
            
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
    public void update(HttpServletRequest req, HttpServletResponse res){
        try {
            CoupeJpaController cont = new CoupeJpaController(Database.getEntityManager());
            Coupe var = cont.findCoupe(req.getParameter("ref"));
            var.setNbrePlot(Integer.parseInt(req.getParameter("nombrePlot")));
            var.setRefBillon(new BillonJpaController(Database.getEntityManager()).findBillon(req.getParameter("billon")));
            
            cont.edit(var);
            res.getOutputStream().print("{\"resultat\":\"true\"}");
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }
    
        
    public void delete(HttpServletRequest req, HttpServletResponse res){
        try {
            CoupeJpaController cont = new CoupeJpaController(Database.getEntityManager());
            PlotJpaController plotController = new PlotJpaController(Database.getEntityManager());
            plotController.erasePlotByCoupe(req.getParameter("ref"));
            res.getOutputStream().print("{\"resultat\":\"true\"}");
            cont.destroy(req.getParameter("ref"));
        } catch (Exception ex) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("resultat", "false");
                obj.put("reason", ex.getMessage());
                res.getOutputStream().print(obj.toString());
            } catch (IOException | JSONException ex1) {}
        }
    }

    

    public void data (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            CoupeJpaController control = new  CoupeJpaController(Database.getEntityManager());
            Coupe b = control.findCoupe(req.getParameter("ref"));

            if(b != null)
                res.getOutputStream().print(b.toString());
            else
                res.getOutputStream().print("{}");
        }catch(Exception ex){try {
            res.getOutputStream().print("{}");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }    
    
    public void dataList (HttpServletRequest req, HttpServletResponse res){
            res.setHeader("Content-Type", "Application/json;charset=ISO-8859-1");
        try{
            CoupeJpaController control = new  CoupeJpaController(Database.getEntityManager());
            List list = control.findCoupeEntities();

                res.getOutputStream().print(list.toString());
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void dataListPage (HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        try{
            CoupeJpaController control = new  CoupeJpaController(Database.getEntityManager());
            List list = control.findCoupeEntities(Api.maxResult, Integer.parseInt(req.getParameter("page"))*Api.maxResult);

            if(list != null && list.size() >0)
                res.getOutputStream().print(list.toString());
            else
                res.getOutputStream().print("[]");
        }catch(Exception ex){try {
            res.getOutputStream().print("[]");
            } catch (IOException ex1) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    /**
     *  Ici on a besoin des billes ayant subit une opération de billonnage, mais qui ne sont pas
     * encore passées par la coupe
     * @param req instance of Server Request
     * @param res instance of Server Response
     * 
     */
    
    public void billeForCoupe(HttpServletRequest req, HttpServletResponse res){
        res.setHeader("Content-Type", "Application/json");
        
        try{
            List list = CoupeJpaController.billeForCoupe(Database.getEntityManager().createEntityManager());
            res.getOutputStream().print(new JSONArray(list).toString());
        }catch(Exception e){try{
                res.getOutputStream().print("[]");
            }catch(Exception ex){}
        }
    }

    
}
