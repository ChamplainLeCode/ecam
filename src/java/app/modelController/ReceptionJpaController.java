/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Commande;
import app.models.LettreCamion;
import app.models.DetailReception;
import app.models.Reception;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.Database;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class ReceptionJpaController implements Serializable {

    public ReceptionJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Reception reception) throws PreexistingEntityException, Exception {
        if (reception.getDetailReceptionList() == null) {
            reception.setDetailReceptionList(new ArrayList<DetailReception>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Commande refCommande = reception.getRefCommande();
            if (refCommande != null) {
                refCommande = em.getReference(refCommande.getClass(), refCommande.getRefCommande());
                reception.setRefCommande(refCommande);
            }
            LettreCamion refLettre = reception.getRefLettre();
            if (refLettre != null) {
                refLettre = em.getReference(refLettre.getClass(), refLettre.getRefLettre());
                reception.setRefLettre(refLettre);
            }
            List<DetailReception> attachedDetailReceptionList = new ArrayList<DetailReception>();
            for (DetailReception detailReceptionListDetailReceptionToAttach : reception.getDetailReceptionList()) {
                detailReceptionListDetailReceptionToAttach = em.getReference(detailReceptionListDetailReceptionToAttach.getClass(), detailReceptionListDetailReceptionToAttach.getRefDetail());
                attachedDetailReceptionList.add(detailReceptionListDetailReceptionToAttach);
            }
            reception.setDetailReceptionList(attachedDetailReceptionList);
            em.persist(reception);

            if (refLettre != null) {
                refLettre.getReceptionList().add(reception);
                refLettre = em.merge(refLettre);
            }
            for (DetailReception detailReceptionListDetailReception : reception.getDetailReceptionList()) {
                Reception oldRefReceptionOfDetailReceptionListDetailReception = detailReceptionListDetailReception.getRefReception();
                detailReceptionListDetailReception.setRefReception(reception);
                detailReceptionListDetailReception = em.merge(detailReceptionListDetailReception);
                if (oldRefReceptionOfDetailReceptionListDetailReception != null) {
                    oldRefReceptionOfDetailReceptionListDetailReception.getDetailReceptionList().remove(detailReceptionListDetailReception);
                    oldRefReceptionOfDetailReceptionListDetailReception = em.merge(oldRefReceptionOfDetailReceptionListDetailReception);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findReception(reception.getRefReception()) != null) {
                throw new PreexistingEntityException("Reception " + reception + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Reception reception) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Reception persistentReception = em.find(Reception.class, reception.getRefReception());
            Commande refCommandeOld = persistentReception.getRefCommande();
            Commande refCommandeNew = reception.getRefCommande();
            LettreCamion refLettreOld = persistentReception.getRefLettre();
            LettreCamion refLettreNew = reception.getRefLettre();
            List<DetailReception> detailReceptionListOld = persistentReception.getDetailReceptionList();
            List<DetailReception> detailReceptionListNew = reception.getDetailReceptionList();
            if (refCommandeNew != null) {
                refCommandeNew = em.getReference(refCommandeNew.getClass(), refCommandeNew.getRefCommande());
                reception.setRefCommande(refCommandeNew);
            }
            if (refLettreNew != null) {
                refLettreNew = em.getReference(refLettreNew.getClass(), refLettreNew.getRefLettre());
                reception.setRefLettre(refLettreNew);
            }
            List<DetailReception> attachedDetailReceptionListNew = new ArrayList<DetailReception>();
            for (DetailReception detailReceptionListNewDetailReceptionToAttach : detailReceptionListNew) {
                detailReceptionListNewDetailReceptionToAttach = em.getReference(detailReceptionListNewDetailReceptionToAttach.getClass(), detailReceptionListNewDetailReceptionToAttach.getRefDetail());
                attachedDetailReceptionListNew.add(detailReceptionListNewDetailReceptionToAttach);
            }
            detailReceptionListNew = attachedDetailReceptionListNew;
            reception.setDetailReceptionList(detailReceptionListNew);
            reception = em.merge(reception);


            if (refLettreOld != null && !refLettreOld.equals(refLettreNew)) {
                refLettreOld.getReceptionList().remove(reception);
                refLettreOld = em.merge(refLettreOld);
            }
            if (refLettreNew != null && !refLettreNew.equals(refLettreOld)) {
                refLettreNew.getReceptionList().add(reception);
                refLettreNew = em.merge(refLettreNew);
            }
            for (DetailReception detailReceptionListOldDetailReception : detailReceptionListOld) {
                if (!detailReceptionListNew.contains(detailReceptionListOldDetailReception)) {
                    detailReceptionListOldDetailReception.setRefReception(null);
                    detailReceptionListOldDetailReception = em.merge(detailReceptionListOldDetailReception);
                }
            }
            for (DetailReception detailReceptionListNewDetailReception : detailReceptionListNew) {
                if (!detailReceptionListOld.contains(detailReceptionListNewDetailReception)) {
                    Reception oldRefReceptionOfDetailReceptionListNewDetailReception = detailReceptionListNewDetailReception.getRefReception();
                    detailReceptionListNewDetailReception.setRefReception(reception);
                    detailReceptionListNewDetailReception = em.merge(detailReceptionListNewDetailReception);
                    if (oldRefReceptionOfDetailReceptionListNewDetailReception != null && !oldRefReceptionOfDetailReceptionListNewDetailReception.equals(reception)) {
                        oldRefReceptionOfDetailReceptionListNewDetailReception.getDetailReceptionList().remove(detailReceptionListNewDetailReception);
                        oldRefReceptionOfDetailReceptionListNewDetailReception = em.merge(oldRefReceptionOfDetailReceptionListNewDetailReception);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = reception.getRefReception();
                if (findReception(id) == null) {
                    throw new NonexistentEntityException("The reception with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Reception reception;
            try {
                reception = em.getReference(Reception.class, id);
                reception.getRefReception();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The reception with id " + id + " no longer exists.", enfe);
            }

            LettreCamion refLettre = reception.getRefLettre();
            if (refLettre != null) {
                refLettre.getReceptionList().remove(reception);
                refLettre = em.merge(refLettre);
            }
            List<DetailReception> detailReceptionList = reception.getDetailReceptionList();
            for (DetailReception detailReceptionListDetailReception : detailReceptionList) {
                detailReceptionListDetailReception.setRefReception(null);
                detailReceptionListDetailReception = em.merge(detailReceptionListDetailReception);
            }
            em.remove(reception);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Reception> findReceptionEntities() {
        return findReceptionEntities(true, -1, -1);
    }

    public List<Reception> findReceptionEntities(int maxResults, int firstResult) {
        return findReceptionEntities(false, maxResults, firstResult);
    }

    private List<Reception> findReceptionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Reception.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Reception findReception(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Reception.class, id);
        } finally {
            em.close();
        }
    }

    public int getReceptionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Reception> rt = cq.from(Reception.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public float getVolumeForCommande(String refCommande) {
        EntityManager em = getEntityManager();
        float volume = 1.0f;
        try{
            Query q = em.createQuery(""
                    + " select sum(drecept.volume) from DetailReception drecept, Reception recept"
                    + " where recept.refCommande.refCommande = :refCommande "
                    + " and   recept.refReception = drecept.refReception.refReception "
                    + ""
            + "");
            q.setParameter("refCommande", refCommande);
            List result = q.getResultList();
            if(result != null && result.size() > 0){
                volume = Float.parseFloat(result.get(0).toString());
            }
        } finally{
            em.close();
        }
        return volume;
    }

    public int getReceptionCountForFournisseur(String refFournisseur) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("SELECT COUNT(1) from Reception r WHERE r.refCommande.refFournisseur.refFournisseur = :fournisseur");
            q.setParameter("fournisseur", refFournisseur);
            return Integer.parseInt(q.getSingleResult().toString());
        }finally{
            em.close();
        }
    }

    public String getMinimalData() {
        EntityManagerFactory emf = Database.getEntityManager();
        List<Reception> liste = new ReceptionJpaController(emf).findReceptionEntities();
        if(liste == null || liste.size() <= 0){
            return "{}";
        }
        System.out.println("Taill = "+liste.size()+" content = "+liste);
        List<String> list = new LinkedList<>();
        JSONObject var  = new JSONObject();
        liste.forEach((e)->{
            try {
                JSONObject obj = new JSONObject();
                obj.put("date", e.getDateReception().getTime());
                obj.put("commande", e.getRefCommande().getRefCommande());
                obj.put("fournisseur", e.getRefCommande().getRefFournisseur().getNomFournisseur()+'('+e.getRefCommande().getRefFournisseur().getContact()+')');
                obj.put("lettre", e.getRefLettre().getRefLettre());
                obj.put("volumeRecu", e.getVolumeRecu());
                obj.put("volumeTotal", ""+e.getRefCommande().getVolumeTotal());
                obj.put("complete", DetailCommandeJpaController.findDetailCommandeByCommande(e.getRefCommande().getRefCommande()).size() == DetailReceptionJpaController.findDetailReceptionNumberByCommande(e.getRefCommande().getRefCommande()));
                // old
                //obj.put("certification", new JSONObject(e.getRefCommande().toString()).getJSONObject("parc").getJSONObject("origine").get("libelle"));
                obj.put("certification", e.getRefCommande().getDetailCommandeList().size());//toString()).getJSONObject("parc").getJSONObject("origine").get("libelle"));
                var.put(e.getRefCommande().getRefCommande(), obj);
                list.add(obj.toString());
            } catch (JSONException ex) {
                System.out.println(ex.getMessage());
            }
        });
        return var.toString();
    }

    public String getMinimalDataForCertificat(String type) {
        EntityManagerFactory emf = Database.getEntityManager();
        List<Reception> liste = new ReceptionJpaController(emf).findReceptionEntities();
        if(liste == null || liste.size() <= 0){
            return "{}";
        }
        List<String> list = new LinkedList<>();
        JSONObject var  = new JSONObject();
        liste.forEach((e)->{
            try {
                JSONObject certif = new JSONObject(e.getRefCommande().toString()).getJSONObject("parc").getJSONObject("origine");
                System.out.println(certif);
                if(certif.getString("ref").equalsIgnoreCase(type)){
                    JSONObject obj = new JSONObject();
                    obj.put("date", e.getDateReception().getTime());
                    obj.put("commande", e.getRefCommande().getRefCommande());
                    obj.put("fournisseur", e.getRefCommande().getRefFournisseur().getNomFournisseur()+'('+e.getRefCommande().getRefFournisseur().getContact()+')');
                    obj.put("lettre", e.getRefLettre().getRefLettre());
                    obj.put("volumeRecu", e.getVolumeRecu());
                    obj.put("volumeTotal", ""+e.getRefCommande().getVolumeTotal());
                    obj.put("complete", DetailCommandeJpaController.findDetailCommandeByCommande(e.getRefCommande().getRefCommande()).size() == DetailReceptionJpaController.findDetailReceptionNumberByCommande(e.getRefCommande().getRefCommande()));
                    obj.put("certification", certif.get("libelle"));
                    var.put(e.getRefCommande().getRefCommande(), obj);
                    list.add(obj.toString());
                }
            } catch (JSONException ex) {
                System.out.println(ex.getMessage());
            }
        });
        return var.toString();

    }
    
}
