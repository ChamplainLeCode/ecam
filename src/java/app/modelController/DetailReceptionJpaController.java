/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.models.DetailReception;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Reception;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.Database;
import app.models.Essence;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class DetailReceptionJpaController implements Serializable {

    public static List<DetailReception> findDetailReceptionByCommande(String refCommande) {
        EntityManager em = Database.getEntityManager().createEntityManager();
        try {
            Query q = em.createQuery("from DetailReception cmd where cmd.refDetail = :ref");
            q.setParameter("ref", refCommande);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public static int findDetailReceptionNumberByCommande(String refCommande) {
        EntityManager em = Database.getEntityManager().createEntityManager();
        try {
            Query q = em.createQuery("select count(1) from Reception recp, DetailReception drecept "
                    + " where recp.refCommande.refCommande = :refCommande and drecept.refReception.refReception = recp.refReception "
                    + " group by recp.refCommande.refCommande");
            q.setParameter("refCommande", refCommande);
            List list = q.getResultList();
            
            int somme = 0;
            if(list != null && list.size() > 0){
                somme = Integer.parseInt(list.get(0)+"");
            }
            return somme;
        } finally {
            em.close();
        }
    }

    public static DetailReception getByBille(String billeNum) {
        EntityManager em = Database.getEntityManager().createEntityManager();
        try{
            Query q = em.createQuery("from DetailReception recept where recept.numBille = :number");
            q.setParameter("number", billeNum);
            return (DetailReception) q.getSingleResult();
        }finally{
            em.close();
        }
    }

    private static int countDetailReceptionByReception(String refReception) {
        EntityManager em = Database.getEntityManager().createEntityManager();
        
        try{
            Query q = em.createQuery("select count(1) as somme from DetailReception cmd where cmd.refReception.refReception = :ref");
            return Integer.parseInt(q.getResultList().get(0)+"");
        }finally{
            em.close();
        }
    }

    public static Essence essenceByNumBille(String numBille) {
        EntityManager em = Database.getEntityManager().createEntityManager();
        
        try{
            TypedQuery<Essence> q = em.createQuery("Select dr.refEssence From DetailReception dr where dr.numBille = :bille", Essence.class);
            q.setParameter("bille", numBille);
            return q.getSingleResult();
        }catch(Exception ex){
            System.out.println("ex= "+ex.getMessage());
            return null;
        }finally{
            em.close();
        }
    }

    public float getVolumeCommande(String refCommande) {
        float volume = 19;

        EntityManager em = Database.getEntityManager().createEntityManager();
        try{
            Query q = em.createQuery(""
                    + " select sum(cmd.volume) as taille from DetailCommande cmd where cmd.refCommande.refCommande = :refCommande"
                    + ""
            + "");
            q.setParameter("refCommande", refCommande);
            List result = q.getResultList();
            
            if(result != null && result.size() > 0){
                //System.out.println("##################################### "+result);
                volume = Float.parseFloat(result.get(0) == null ? "0" : result.get(0).toString());
            }
        } finally{
            em.close();
        }        
        return volume;
    }

    public DetailReceptionJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DetailReception detailReception) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Reception refReception = detailReception.getRefReception();
            if (refReception != null) {
                refReception = em.getReference(refReception.getClass(), refReception.getRefReception());
                detailReception.setRefReception(refReception);
            }
            em.persist(detailReception);
            if (refReception != null) {
                refReception.getDetailReceptionList().add(detailReception);
                refReception = em.merge(refReception);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDetailReception(detailReception.getRefDetail()) != null) {
                throw new PreexistingEntityException("DetailReception " + detailReception + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DetailReception detailReception) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailReception persistentDetailReception = em.find(DetailReception.class, detailReception.getRefDetail());
            Reception refReceptionOld = persistentDetailReception.getRefReception();
            Reception refReceptionNew = detailReception.getRefReception();
            if (refReceptionNew != null) {
                refReceptionNew = em.getReference(refReceptionNew.getClass(), refReceptionNew.getRefReception());
                detailReception.setRefReception(refReceptionNew);
            }
            detailReception = em.merge(detailReception);
            if (refReceptionOld != null && !refReceptionOld.equals(refReceptionNew)) {
                refReceptionOld.getDetailReceptionList().remove(detailReception);
                refReceptionOld = em.merge(refReceptionOld);
            }
            if (refReceptionNew != null && !refReceptionNew.equals(refReceptionOld)) {
                refReceptionNew.getDetailReceptionList().add(detailReception);
                refReceptionNew = em.merge(refReceptionNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = detailReception.getRefDetail();
                if (findDetailReception(id) == null) {
                    throw new NonexistentEntityException("The detailReception with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailReception detailReception;
            try {
                detailReception = em.getReference(DetailReception.class, id);
                detailReception.getRefDetail();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The detailReception with id " + id + " no longer exists.", enfe);
            }
            Reception refReception = detailReception.getRefReception();
            if (refReception != null) {
                refReception.getDetailReceptionList().remove(detailReception);
                refReception = em.merge(refReception);
            }
            em.remove(detailReception);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DetailReception> findDetailReceptionEntities() {
        return findDetailReceptionEntities(true, -1, -1);
    }

    public List<DetailReception> findDetailReceptionEntities(int maxResults, int firstResult) {
        return findDetailReceptionEntities(false, maxResults, firstResult);
    }

    private List<DetailReception> findDetailReceptionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DetailReception.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DetailReception findDetailReception(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DetailReception.class, id);
        } finally {
            em.close();
        }
    }

    public int getDetailReceptionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DetailReception> rt = cq.from(DetailReception.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }


    /**
     * Cette fonction permet de retourner la liste des Numéros de billes receptionnées
     * @return <b>List of String</b>
     */
    public String getAllBilleNumber() {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select cmd.numBille from DetailReception cmd");
//            Root<DetailReception> detailReception = q.setfrom(DetailReception.class);
            //q.select(detailReception.get("NUM_BILLE"));
            //Query query = em.createQuery(q);
            JSONArray list = new JSONArray(q.getResultList());
            return list.toString();
        } finally {
            em.close();
        }
    
    }

    public String getAllBilleNumberByMVT(String MVT_CHANGEMENT) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select cmd.numBille from Parc parc where not(parc.numBille = null)");
//            Root<DetailReception> detailReception = q.setfrom(DetailReception.class);
            //q.select(detailReception.get("NUM_BILLE"));
            //Query query = em.createQuery(q);
            JSONArray list = new JSONArray(q.getResultList());
            return list.toString();
        } finally {
            em.close();
        }
    }

    /**
     *  Ici on retourne la liste de couple (numBille, numTravail) de
     * billes reçues et n'ayant pas encore subit d'opération de billonnage
     * @return List of [String, String]
     */
    public String getAllBilleWithNumTravail() {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("Select distinct cmd.numBille, cmd.numtravail from DetailReception cmd where not (cmd.numtravail = null) "
                    + " and cmd.numBille not in (Select bllnage.numBille from Billonnage bllnage)");
            return new JSONArray(q.getResultList()).toString();
        }finally {
            em.close();
        }
    }

    public String getVolumeBille(String numBille) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("Select cmd.volume from DetailReception cmd where cmd.numBille = :bille");
            q.setParameter("bille", numBille);
            List l = q.getResultList();
            return l.size() > 0 ? l.get(0).toString() : "0"; 
        }finally {
            em.close();
        }
    }
    
    public List<DetailReception> findDetailReceptionByReception(String recept){
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("from DetailReception dr where dr.refReception.refReception = :ref", DetailReception.class);
            q.setParameter("ref", recept);
            return q.getResultList();
        }finally{
            em.close();
        }
    }
    
    public DetailReception findDetailReceptionByNumBille(String numBille){
        EntityManager em = getEntityManager();
        try{
            Query q = em.createNamedQuery("DetailReception.findByNumBille", DetailReception.class);
            q.setParameter("numBille", numBille);
            List<DetailReception> r = q.getResultList();
            System.out.println("Liste de bille = "+r);
            return (DetailReception) q.getSingleResult();
        }finally{
            em.close();
        }
    }
    
    public DetailReception findDetailReceptionByNumTravail(String travail){
        EntityManager em = getEntityManager();
        try{
//            Query q = em.createNamedQuery("DetailReception.findByNumtravail", DetailReception.class);
            Query q = em.createQuery("from DetailReception dr where dr.numtravail = :numTravail ");
            q.setParameter("numTravail", travail);
            return (DetailReception) q.getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }

    public String getCertificationForNumTravail(String numTravail) {
        
        EntityManager em = getEntityManager();
        try{
            
            Query q = em.createQuery(
                "Select pr.refTypeparc.libelle from DetailReception dr, Parc pr where (:numTravail = dr.numtravail or :numTravail2 = dr.numtravail) and dr.numBille = pr.numBille");
            q.setParameter("numTravail", numTravail);
            q.setParameter("numTravail2", Calendar.getInstance().get(Calendar.YEAR)+"-"+numTravail);
            List<Object> list = q.getResultList();
            return list.size() > 0 ? list.get(0).toString() : null;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }

    public DetailReception checkTravail(String numTravail) {
        
        EntityManager em = getEntityManager();
        try{
            
            Query q = em.createQuery(
                "Select dr from DetailReception dr where :numTravail = dr.numtravail");
            q.setParameter("numTravail", Calendar.getInstance().get(Calendar.YEAR)+"-"+numTravail);
            System.out.println("travail = "+ Calendar.getInstance().get(Calendar.YEAR)+'-'+numTravail);
            List<DetailReception> list = q.getResultList();
            System.out.println("List = "+list);
            return list.size() > 0 ? list.get(0) : null;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }
}
