/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Cuve;
import app.models.CuvePlot;
import app.models.Plot;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import org.json.JSONArray;

/**
 *
 * @author champlain
 */
public class CuvePlotJpaController implements Serializable {


    public CuvePlotJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CuvePlot cuvePlot) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cuve refCuve = cuvePlot.getRefCuve();
            if (refCuve != null) {
                refCuve = em.getReference(refCuve.getClass(), refCuve.getRefCuve());
                cuvePlot.setRefCuve(refCuve);
            }
            Plot refPlot = cuvePlot.getRefPlot();
            if (refPlot != null) {
                refPlot = em.getReference(refPlot.getClass(), refPlot.getRefPlot());
                cuvePlot.setRefPlot(refPlot);
            }
            em.persist(cuvePlot);
            if (refCuve != null) {
                refCuve.getCuvePlotList().add(cuvePlot);
                refCuve = em.merge(refCuve);
            }
            if (refPlot != null) {
                refPlot.getCuvePlotList().add(cuvePlot);
                refPlot = em.merge(refPlot);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCuvePlot(cuvePlot.getRefCuveplot()) != null) {
                throw new PreexistingEntityException("CuvePlot " + cuvePlot + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CuvePlot cuvePlot) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CuvePlot persistentCuvePlot = em.find(CuvePlot.class, cuvePlot.getRefCuveplot());
            Cuve refCuveOld = persistentCuvePlot.getRefCuve();
            Cuve refCuveNew = cuvePlot.getRefCuve();
            Plot refPlotOld = persistentCuvePlot.getRefPlot();
            Plot refPlotNew = cuvePlot.getRefPlot();
            if (refCuveNew != null) {
                refCuveNew = em.getReference(refCuveNew.getClass(), refCuveNew.getRefCuve());
                cuvePlot.setRefCuve(refCuveNew);
            }
            if (refPlotNew != null) {
                refPlotNew = em.getReference(refPlotNew.getClass(), refPlotNew.getRefPlot());
                cuvePlot.setRefPlot(refPlotNew);
            }
            cuvePlot = em.merge(cuvePlot);
            if (refCuveOld != null && !refCuveOld.equals(refCuveNew)) {
                refCuveOld.getCuvePlotList().remove(cuvePlot);
                refCuveOld = em.merge(refCuveOld);
            }
            if (refCuveNew != null && !refCuveNew.equals(refCuveOld)) {
                refCuveNew.getCuvePlotList().add(cuvePlot);
                refCuveNew = em.merge(refCuveNew);
            }
            if (refPlotOld != null && !refPlotOld.equals(refPlotNew)) {
                refPlotOld.getCuvePlotList().remove(cuvePlot);
                refPlotOld = em.merge(refPlotOld);
            }
            if (refPlotNew != null && !refPlotNew.equals(refPlotOld)) {
                refPlotNew.getCuvePlotList().add(cuvePlot);
                refPlotNew = em.merge(refPlotNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = cuvePlot.getRefCuveplot();
                if (findCuvePlot(id) == null) {
                    throw new NonexistentEntityException("The cuvePlot with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CuvePlot cuvePlot;
            try {
                cuvePlot = em.getReference(CuvePlot.class, id);
                cuvePlot.getRefCuveplot();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cuvePlot with id " + id + " no longer exists.", enfe);
            }
            Cuve refCuve = cuvePlot.getRefCuve();
            if (refCuve != null) {
                refCuve.getCuvePlotList().remove(cuvePlot);
                refCuve = em.merge(refCuve);
            }
            Plot refPlot = cuvePlot.getRefPlot();
            if (refPlot != null) {
                refPlot.getCuvePlotList().remove(cuvePlot);
                refPlot = em.merge(refPlot);
            }
            em.remove(cuvePlot);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CuvePlot> findCuvePlotEntities() {
        return findCuvePlotEntities(true, -1, -1);
    }

    public List<CuvePlot> findCuvePlotEntities(int maxResults, int firstResult) {
        return findCuvePlotEntities(false, maxResults, firstResult);
    }

    private List<CuvePlot> findCuvePlotEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CuvePlot.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CuvePlot findCuvePlot(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CuvePlot.class, id);
        } finally {
            em.close();
        }
    }

    public int getCuvePlotCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CuvePlot> rt = cq.from(CuvePlot.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List findByCuve(String cuve) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select cp from CuvePlot cp where cp.refCuve.refCuve = :refCuve", CuvePlot.class);
            q.setParameter("refCuve", cuve);
            return q.getResultList();
        }finally{
            em.close();
        }
    }

    
    public static String getPlotInCuve(EntityManagerFactory entityManager, String cuveRef) {
        EntityManager em = entityManager.createEntityManager();
        try{
            Query q = em.createQuery("select cp.refPlot.refPlot from CuvePlot cp where cp.refCuve.refCuve = :refCuve and cp.dateFin is null");
            q.setParameter("refCuve", cuveRef);
            return new JSONArray(q.getResultList()).toString();
        }catch(Exception e){
            return "[]";
        }finally{
            em.close();
        }
    }
    
    public String getPlotForTranche() {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select pl.refPlot, pl.numTravail from Plot pl "
                    + "where "
                    + "     pl.refPlot in (select cp.refPlot.refPlot from CuvePlot cp where cp.dateFin is not null) "
                    + "AND "
                    + "     pl.refPlot not in (select tr.refPlot.refPlot from Tranche tr)");

            return new JSONArray(q.getResultList()).toString();
        }catch(Exception e){
            return new LinkedList<>().toString();
        }finally{
            em.close();
        }
    }

    public int getCuvePlotCountInCuve(Cuve cuve) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("SELECT count(1) From CuvePlot cp WHERE cp.dateFin is NULL and cp.refCuve.refCuve = :refcuv group by cp.refCuve.refCuve");
            q.setParameter("refcuv", cuve.getRefCuve());
            return Integer.getInteger(q.getSingleResult().toString());
        }catch(NoResultException e){
            return 0;
        }catch(Exception e){
            return -1;
        }finally{
            em.close();
        }
    }
    
}

//"select pl.refPlot, pl.numTravail "
//                    + "from "
//                    + "     Plot pl "
//                    + "where "
//                    + "     pl.refPlot not in (select cp.refPlot.refPlot from CuvePlot cp where cp.dateFin is not null) ");
//                    + "     (select tr.refPlot.refPlot from Tranche tr) trPlot "
//                    + "where"
//                    + "     pl.refPlot not in cpCuite and pl.refPlot not in trPlot"
//                    + ""