/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Coupe;
import app.models.Billon;
import app.models.TypeCoupe;
import app.models.Tranche;
import java.util.ArrayList;
import java.util.List;
import app.models.Feuille;
import app.models.CuvePlot;
import app.models.Database;
import app.models.Paquet;
import app.models.Plot;
import java.io.PrintStream;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import org.json.JSONArray;

/**
 *
 * @author champlain
 */
public class PlotJpaController implements Serializable {

    public PlotJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Plot plot) throws PreexistingEntityException, Exception {
        if (plot.getTrancheList() == null) {
            plot.setTrancheList(new ArrayList<Tranche>());
        }
        if (plot.getFeuilleList() == null) {
            plot.setFeuilleList(new ArrayList<Feuille>());
        }
        if (plot.getCuvePlotList() == null) {
            plot.setCuvePlotList(new ArrayList<CuvePlot>());
        }
        if (plot.getPaquetList() == null) {
            plot.setPaquetList(new ArrayList<Paquet>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Coupe refCoupe = plot.getRefCoupe();
            if (refCoupe != null) {
                refCoupe = em.getReference(refCoupe.getClass(), refCoupe.getRefCoupe());
                plot.setRefCoupe(refCoupe);
            }
            Billon refBillon = plot.getRefBillon();
            if (refBillon != null) {
                refBillon = em.getReference(refBillon.getClass(), refBillon.getRefBillon());
                plot.setRefBillon(refBillon);
            }
            List<Tranche> attachedTrancheList = new ArrayList<Tranche>();
            for (Tranche trancheListTrancheToAttach : plot.getTrancheList()) {
                trancheListTrancheToAttach = em.getReference(trancheListTrancheToAttach.getClass(), trancheListTrancheToAttach.getRefTranche());
                attachedTrancheList.add(trancheListTrancheToAttach);
            }
            plot.setTrancheList(attachedTrancheList);
            List<Feuille> attachedFeuilleList = new ArrayList<Feuille>();
            for (Feuille feuilleListFeuilleToAttach : plot.getFeuilleList()) {
                feuilleListFeuilleToAttach = em.getReference(feuilleListFeuilleToAttach.getClass(), feuilleListFeuilleToAttach.getRefFeuille());
                attachedFeuilleList.add(feuilleListFeuilleToAttach);
            }
            plot.setFeuilleList(attachedFeuilleList);
            List<CuvePlot> attachedCuvePlotList = new ArrayList<CuvePlot>();
            for (CuvePlot cuvePlotListCuvePlotToAttach : plot.getCuvePlotList()) {
                cuvePlotListCuvePlotToAttach = em.getReference(cuvePlotListCuvePlotToAttach.getClass(), cuvePlotListCuvePlotToAttach.getRefCuveplot());
                attachedCuvePlotList.add(cuvePlotListCuvePlotToAttach);
            }
            plot.setCuvePlotList(attachedCuvePlotList);
            List<Paquet> attachedPaquetList = new ArrayList<Paquet>();
            for (Paquet paquetListPaquetToAttach : plot.getPaquetList()) {
                paquetListPaquetToAttach = em.getReference(paquetListPaquetToAttach.getClass(), paquetListPaquetToAttach.getRefPaquet());
                attachedPaquetList.add(paquetListPaquetToAttach);
            }
            plot.setPaquetList(attachedPaquetList);
            em.persist(plot);
            if (refCoupe != null) {
                refCoupe.getPlotList().add(plot);
                refCoupe = em.merge(refCoupe);
            }
            if (refBillon != null) {
                refBillon.getPlotList().add(plot);
                refBillon = em.merge(refBillon);
            }
            for (Tranche trancheListTranche : plot.getTrancheList()) {
                Plot oldRefPlotOfTrancheListTranche = trancheListTranche.getRefPlot();
                trancheListTranche.setRefPlot(plot);
                trancheListTranche = em.merge(trancheListTranche);
                if (oldRefPlotOfTrancheListTranche != null) {
                    oldRefPlotOfTrancheListTranche.getTrancheList().remove(trancheListTranche);
                    oldRefPlotOfTrancheListTranche = em.merge(oldRefPlotOfTrancheListTranche);
                }
            }
            for (Feuille feuilleListFeuille : plot.getFeuilleList()) {
                Plot oldRefPlotOfFeuilleListFeuille = feuilleListFeuille.getRefPlot();
                feuilleListFeuille.setRefPlot(plot);
                feuilleListFeuille = em.merge(feuilleListFeuille);
                if (oldRefPlotOfFeuilleListFeuille != null) {
                    oldRefPlotOfFeuilleListFeuille.getFeuilleList().remove(feuilleListFeuille);
                    oldRefPlotOfFeuilleListFeuille = em.merge(oldRefPlotOfFeuilleListFeuille);
                }
            }
            for (CuvePlot cuvePlotListCuvePlot : plot.getCuvePlotList()) {
                Plot oldRefPlotOfCuvePlotListCuvePlot = cuvePlotListCuvePlot.getRefPlot();
                cuvePlotListCuvePlot.setRefPlot(plot);
                cuvePlotListCuvePlot = em.merge(cuvePlotListCuvePlot);
                if (oldRefPlotOfCuvePlotListCuvePlot != null) {
                    oldRefPlotOfCuvePlotListCuvePlot.getCuvePlotList().remove(cuvePlotListCuvePlot);
                    oldRefPlotOfCuvePlotListCuvePlot = em.merge(oldRefPlotOfCuvePlotListCuvePlot);
                }
            }
            for (Paquet paquetListPaquet : plot.getPaquetList()) {
                Plot oldRefPlotOfPaquetListPaquet = paquetListPaquet.getRefPlot();
                paquetListPaquet.setRefPlot(plot);
                paquetListPaquet = em.merge(paquetListPaquet);
                if (oldRefPlotOfPaquetListPaquet != null) {
                    oldRefPlotOfPaquetListPaquet.getPaquetList().remove(paquetListPaquet);
                    oldRefPlotOfPaquetListPaquet = em.merge(oldRefPlotOfPaquetListPaquet);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPlot(plot.getRefPlot()) != null) {
                throw new PreexistingEntityException("Plot " + plot + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Plot plot) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Plot persistentPlot = em.find(Plot.class, plot.getRefPlot());
            Coupe refCoupeOld = persistentPlot.getRefCoupe();
            Coupe refCoupeNew = plot.getRefCoupe();
            Billon refBillonOld = persistentPlot.getRefBillon();
            Billon refBillonNew = plot.getRefBillon();
            List<Tranche> trancheListOld = persistentPlot.getTrancheList();
            List<Tranche> trancheListNew = plot.getTrancheList();
            List<Feuille> feuilleListOld = persistentPlot.getFeuilleList();
            List<Feuille> feuilleListNew = plot.getFeuilleList();
            List<CuvePlot> cuvePlotListOld = persistentPlot.getCuvePlotList();
            List<CuvePlot> cuvePlotListNew = plot.getCuvePlotList();
            List<Paquet> paquetListOld = persistentPlot.getPaquetList();
            List<Paquet> paquetListNew = plot.getPaquetList();
            if (refCoupeNew != null) {
                refCoupeNew = em.getReference(refCoupeNew.getClass(), refCoupeNew.getRefCoupe());
                plot.setRefCoupe(refCoupeNew);
            }
            if (refBillonNew != null) {
                refBillonNew = em.getReference(refBillonNew.getClass(), refBillonNew.getRefBillon());
                plot.setRefBillon(refBillonNew);
            }
            List<Tranche> attachedTrancheListNew = new ArrayList<Tranche>();
            for (Tranche trancheListNewTrancheToAttach : trancheListNew) {
                trancheListNewTrancheToAttach = em.getReference(trancheListNewTrancheToAttach.getClass(), trancheListNewTrancheToAttach.getRefTranche());
                attachedTrancheListNew.add(trancheListNewTrancheToAttach);
            }
            trancheListNew = attachedTrancheListNew;
            plot.setTrancheList(trancheListNew);
            List<Feuille> attachedFeuilleListNew = new ArrayList<Feuille>();
            for (Feuille feuilleListNewFeuilleToAttach : feuilleListNew) {
                feuilleListNewFeuilleToAttach = em.getReference(feuilleListNewFeuilleToAttach.getClass(), feuilleListNewFeuilleToAttach.getRefFeuille());
                attachedFeuilleListNew.add(feuilleListNewFeuilleToAttach);
            }
            feuilleListNew = attachedFeuilleListNew;
            plot.setFeuilleList(feuilleListNew);
            List<CuvePlot> attachedCuvePlotListNew = new ArrayList<CuvePlot>();
            for (CuvePlot cuvePlotListNewCuvePlotToAttach : cuvePlotListNew) {
                cuvePlotListNewCuvePlotToAttach = em.getReference(cuvePlotListNewCuvePlotToAttach.getClass(), cuvePlotListNewCuvePlotToAttach.getRefCuveplot());
                attachedCuvePlotListNew.add(cuvePlotListNewCuvePlotToAttach);
            }
            cuvePlotListNew = attachedCuvePlotListNew;
            plot.setCuvePlotList(cuvePlotListNew);
            List<Paquet> attachedPaquetListNew = new ArrayList<Paquet>();
            for (Paquet paquetListNewPaquetToAttach : paquetListNew) {
                paquetListNewPaquetToAttach = em.getReference(paquetListNewPaquetToAttach.getClass(), paquetListNewPaquetToAttach.getRefPaquet());
                attachedPaquetListNew.add(paquetListNewPaquetToAttach);
            }
            paquetListNew = attachedPaquetListNew;
            plot.setPaquetList(paquetListNew);
            plot = em.merge(plot);
            if (refCoupeOld != null && !refCoupeOld.equals(refCoupeNew)) {
                refCoupeOld.getPlotList().remove(plot);
                refCoupeOld = em.merge(refCoupeOld);
            }
            if (refCoupeNew != null && !refCoupeNew.equals(refCoupeOld)) {
                refCoupeNew.getPlotList().add(plot);
                refCoupeNew = em.merge(refCoupeNew);
            }
            if (refBillonOld != null && !refBillonOld.equals(refBillonNew)) {
                refBillonOld.getPlotList().remove(plot);
                refBillonOld = em.merge(refBillonOld);
            }
            if (refBillonNew != null && !refBillonNew.equals(refBillonOld)) {
                refBillonNew.getPlotList().add(plot);
                refBillonNew = em.merge(refBillonNew);
            }
            for (Tranche trancheListOldTranche : trancheListOld) {
                if (!trancheListNew.contains(trancheListOldTranche)) {
                    trancheListOldTranche.setRefPlot(null);
                    trancheListOldTranche = em.merge(trancheListOldTranche);
                }
            }
            for (Tranche trancheListNewTranche : trancheListNew) {
                if (!trancheListOld.contains(trancheListNewTranche)) {
                    Plot oldRefPlotOfTrancheListNewTranche = trancheListNewTranche.getRefPlot();
                    trancheListNewTranche.setRefPlot(plot);
                    trancheListNewTranche = em.merge(trancheListNewTranche);
                    if (oldRefPlotOfTrancheListNewTranche != null && !oldRefPlotOfTrancheListNewTranche.equals(plot)) {
                        oldRefPlotOfTrancheListNewTranche.getTrancheList().remove(trancheListNewTranche);
                        oldRefPlotOfTrancheListNewTranche = em.merge(oldRefPlotOfTrancheListNewTranche);
                    }
                }
            }
            for (Feuille feuilleListOldFeuille : feuilleListOld) {
                if (!feuilleListNew.contains(feuilleListOldFeuille)) {
                    feuilleListOldFeuille.setRefPlot(null);
                    feuilleListOldFeuille = em.merge(feuilleListOldFeuille);
                }
            }
            for (Feuille feuilleListNewFeuille : feuilleListNew) {
                if (!feuilleListOld.contains(feuilleListNewFeuille)) {
                    Plot oldRefPlotOfFeuilleListNewFeuille = feuilleListNewFeuille.getRefPlot();
                    feuilleListNewFeuille.setRefPlot(plot);
                    feuilleListNewFeuille = em.merge(feuilleListNewFeuille);
                    if (oldRefPlotOfFeuilleListNewFeuille != null && !oldRefPlotOfFeuilleListNewFeuille.equals(plot)) {
                        oldRefPlotOfFeuilleListNewFeuille.getFeuilleList().remove(feuilleListNewFeuille);
                        oldRefPlotOfFeuilleListNewFeuille = em.merge(oldRefPlotOfFeuilleListNewFeuille);
                    }
                }
            }
            for (CuvePlot cuvePlotListOldCuvePlot : cuvePlotListOld) {
                if (!cuvePlotListNew.contains(cuvePlotListOldCuvePlot)) {
                    cuvePlotListOldCuvePlot.setRefPlot(null);
                    cuvePlotListOldCuvePlot = em.merge(cuvePlotListOldCuvePlot);
                }
            }
            for (CuvePlot cuvePlotListNewCuvePlot : cuvePlotListNew) {
                if (!cuvePlotListOld.contains(cuvePlotListNewCuvePlot)) {
                    Plot oldRefPlotOfCuvePlotListNewCuvePlot = cuvePlotListNewCuvePlot.getRefPlot();
                    cuvePlotListNewCuvePlot.setRefPlot(plot);
                    cuvePlotListNewCuvePlot = em.merge(cuvePlotListNewCuvePlot);
                    if (oldRefPlotOfCuvePlotListNewCuvePlot != null && !oldRefPlotOfCuvePlotListNewCuvePlot.equals(plot)) {
                        oldRefPlotOfCuvePlotListNewCuvePlot.getCuvePlotList().remove(cuvePlotListNewCuvePlot);
                        oldRefPlotOfCuvePlotListNewCuvePlot = em.merge(oldRefPlotOfCuvePlotListNewCuvePlot);
                    }
                }
            }
            for (Paquet paquetListOldPaquet : paquetListOld) {
                if (!paquetListNew.contains(paquetListOldPaquet)) {
                    paquetListOldPaquet.setRefPlot(null);
                    paquetListOldPaquet = em.merge(paquetListOldPaquet);
                }
            }
            for (Paquet paquetListNewPaquet : paquetListNew) {
                if (!paquetListOld.contains(paquetListNewPaquet)) {
                    Plot oldRefPlotOfPaquetListNewPaquet = paquetListNewPaquet.getRefPlot();
                    paquetListNewPaquet.setRefPlot(plot);
                    paquetListNewPaquet = em.merge(paquetListNewPaquet);
                    if (oldRefPlotOfPaquetListNewPaquet != null && !oldRefPlotOfPaquetListNewPaquet.equals(plot)) {
                        oldRefPlotOfPaquetListNewPaquet.getPaquetList().remove(paquetListNewPaquet);
                        oldRefPlotOfPaquetListNewPaquet = em.merge(oldRefPlotOfPaquetListNewPaquet);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = plot.getRefPlot();
                if (findPlot(id) == null) {
                    throw new NonexistentEntityException("The plot with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Plot plot;
            try {
                plot = em.getReference(Plot.class, id);
                plot.getRefPlot();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The plot with id " + id + " no longer exists.", enfe);
            }
            Coupe refCoupe = plot.getRefCoupe();
            if (refCoupe != null) {
                refCoupe.getPlotList().remove(plot);
                refCoupe = em.merge(refCoupe);
            }
            Billon refBillon = plot.getRefBillon();
            if (refBillon != null) {
                refBillon.getPlotList().remove(plot);
                refBillon = em.merge(refBillon);
            }
            List<Tranche> trancheList = plot.getTrancheList();
            for (Tranche trancheListTranche : trancheList) {
                trancheListTranche.setRefPlot(null);
                trancheListTranche = em.merge(trancheListTranche);
            }
            List<Feuille> feuilleList = plot.getFeuilleList();
            for (Feuille feuilleListFeuille : feuilleList) {
                feuilleListFeuille.setRefPlot(null);
                feuilleListFeuille = em.merge(feuilleListFeuille);
            }
            List<CuvePlot> cuvePlotList = plot.getCuvePlotList();
            for (CuvePlot cuvePlotListCuvePlot : cuvePlotList) {
                cuvePlotListCuvePlot.setRefPlot(null);
                cuvePlotListCuvePlot = em.merge(cuvePlotListCuvePlot);
            }
            List<Paquet> paquetList = plot.getPaquetList();
            for (Paquet paquetListPaquet : paquetList) {
                paquetListPaquet.setRefPlot(null);
                paquetListPaquet = em.merge(paquetListPaquet);
            }
            em.remove(plot);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Plot> findPlotEntities() {
        return findPlotEntities(true, -1, -1);
    }

    public List<Plot> findPlotEntities(int maxResults, int firstResult) {
        return findPlotEntities(false, maxResults, firstResult);
    }

    private List<Plot> findPlotEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Plot.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Plot findPlot(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Plot.class, id);
        } finally {
            em.close();
        }
    }

    public int getPlotCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Plot> rt = cq.from(Plot.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }    

    public String plotforCuve(String nature) {
        EntityManager em = getEntityManager();
        try{
            Query q = null;
            if(nature.equals("billon"))
                q = em.createQuery(""
                    + "select distinct pl.refBillon.refBillon from Plot pl where pl.refPlot not in (select cp.refPlot.refPlot from CuvePlot cp)");
            else if(nature.equals("plot"))
                q = em.createQuery(""
                    + "select distinct pl.refPlot from Plot pl where pl.refPlot not in (select cp.refPlot.refPlot from CuvePlot cp)");
            
            return new JSONArray(q.getResultList()).toString();
        }finally{
            em.close();
        }
    }

    public String getLastFaceForBillon(String refBillon) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("Select pl.refPlot from Plot pl WHERE pl.refBillon.refBillon = :ref ORDER BY pl.refPlot DESC ");
            q.setParameter("ref", refBillon);
            List l = q.getResultList();
            if(l != null && l.size() >0)
                return "{\"face\": \""+l.get(0).toString()+"\"}";
            return "{\"face\": null}";
        }finally{
            em.close();
        }
    }

    public void erasePlotByCoupe(String refCoupe) {
        EntityManager em = getEntityManager();
        try{
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
                PlotJpaController cont = new PlotJpaController(em.getEntityManagerFactory());
                Query q = em.createQuery("FROM Plot pl where pl.refCoupe.refCoupe = :refCoupe", Plot.class);
                q.setParameter("refCoupe", refCoupe);
                List<Plot> list = q.getResultList();
                for(int i=0; i<list.size(); i++){
                    Plot p = list.get(i);
                        try{cont.destroy(p.getRefPlot());}catch(NonexistentEntityException ex){}
                    };
            transaction.commit();
        }finally{
            em.close();
        }
    }

    public List<Plot> finPlotByBillon(String ref) {
        EntityManager em = getEntityManager();
        
        try{
            Query q = em.createQuery("from Plot pl where pl.refBillon.refBillon = :ref", Plot.class);
            q.setParameter("ref", ref);
            System.out.println(q.getResultList().size());
            return q.getResultList();
        }finally{
            em.close();
        }
    }

    public List<Plot> finPlotByTravail(String travail) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("from Plot pl where pl.numTravail = :travail", Plot.class);
            q.setParameter("travail", travail);
            return q.getResultList();
        }finally{
            em.close();
        }
    }

    public static long countFeuilleByBillon(String refBillon) {
        try{
            EntityManager em = Database.getEntityManager().createEntityManager();
            Query q = em.createQuery("select sum(pl.face1 + pl.face2 + pl.face3)  FROM Plot pl where pl.refBillon.refBillon = :refBillon");
            q.setParameter("refBillon", refBillon);
            long value = Long.parseLong((String)q.getSingleResult());
            em.close();
            return value;
        }catch(Exception e){
            e.printStackTrace(System.out);
            return 0L;
        }
    }

    public static long countFeuilleByTravail(String numTravail) {
        try{
            EntityManager em = Database.getEntityManager().createEntityManager();
            Query q = em.createQuery("select sum(pl.face1 + pl.face2 + pl.face3)  FROM Plot pl where pl.numTravail = :travail");
            q.setParameter("travail", numTravail);
            System.out.println(q.getSingleResult().toString());
            long value = Long.parseLong(q.getSingleResult().toString());
            em.close();
            return value;
        }catch(Exception e){
            e.printStackTrace(System.out);
            return 0L;
        }
    }

    public String getPlotForMassicot(String numTravail) {
        try{
            EntityManager em = Database.getEntityManager().createEntityManager();
            Query q = em.createQuery("select tr.refPlot.refPlot "
                    + "FROM Tranche tr "
                    + "where tr.refPlot.refPlot LIKE :travail "
                    + "AND tr.refPlot.refPlot not in (SELECT pqt.refPlot.refPlot from Paquet pqt)");
            q.setParameter("travail", numTravail+"%");
            String value = new JSONArray(q.getResultList().toString()).toString();
            em.close();
            return value;
        }catch(Exception e){
            e.printStackTrace(System.out);
            return "[]";
        }
    }

    public String getPlotForMassicotByBillon(String refBillon) {
        try{
            EntityManager em = Database.getEntityManager().createEntityManager();
            Query q = em.createQuery("select tr.refPlot.refPlot "
                    + "FROM Tranche tr "
                    + "where tr.refPlot.refBillon.refBillon = :refBillon "
                    + "AND tr.refPlot.refPlot not in (SELECT pqt.refPlot.refPlot from Paquet pqt)");
            q.setParameter("refBillon", refBillon);
            String value = new JSONArray(q.getResultList().toString()).toString();
            em.close();
            return value;
        }catch(Exception e){
            //e.printStackTrace(System.out);
            return "[]";
        }
    }



}
