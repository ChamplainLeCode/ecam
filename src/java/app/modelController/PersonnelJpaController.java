/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Equipe;
import java.util.ArrayList;
import java.util.List;
import app.models.Detailequipe;
import app.models.Personnel;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class PersonnelJpaController implements Serializable {

    public PersonnelJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Personnel personnel) throws PreexistingEntityException, Exception {
        if (personnel.getDetailequipeList() == null) {
            personnel.setDetailequipeList(new ArrayList<>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Detailequipe> attachedDetailequipeList = new ArrayList<>();
            for (Detailequipe detailequipeListDetailequipeToAttach : personnel.getDetailequipeList()) {
                detailequipeListDetailequipeToAttach = em.getReference(detailequipeListDetailequipeToAttach.getClass(), detailequipeListDetailequipeToAttach.getRefDetail());
                attachedDetailequipeList.add(detailequipeListDetailequipeToAttach);
            }
            personnel.setDetailequipeList(attachedDetailequipeList);
            em.persist(personnel);
            for (Detailequipe detailequipeListDetailequipe : personnel.getDetailequipeList()) {
                Personnel oldRefPersonnelOfDetailequipeListDetailequipe = detailequipeListDetailequipe.getRefPersonnel();
                detailequipeListDetailequipe.setRefPersonnel(personnel);
                detailequipeListDetailequipe = em.merge(detailequipeListDetailequipe);
                if (oldRefPersonnelOfDetailequipeListDetailequipe != null) {
                    oldRefPersonnelOfDetailequipeListDetailequipe.getDetailequipeList().remove(detailequipeListDetailequipe);
                    oldRefPersonnelOfDetailequipeListDetailequipe = em.merge(oldRefPersonnelOfDetailequipeListDetailequipe);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPersonnel(personnel.getCniPersonnel()) != null) {
                throw new PreexistingEntityException("Personnel " + personnel + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Personnel personnel) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Personnel persistentPersonnel = em.find(Personnel.class, personnel.getCniPersonnel());
            List<Detailequipe> detailequipeListOld = persistentPersonnel.getDetailequipeList();
            List<Detailequipe> detailequipeListNew = personnel.getDetailequipeList();
            List<Detailequipe> attachedDetailequipeListNew = new ArrayList<Detailequipe>();

            for (Detailequipe detailequipeListNewDetailequipeToAttach : detailequipeListNew) {
                detailequipeListNewDetailequipeToAttach = em.getReference(detailequipeListNewDetailequipeToAttach.getClass(), detailequipeListNewDetailequipeToAttach.getRefDetail());
                attachedDetailequipeListNew.add(detailequipeListNewDetailequipeToAttach);
            }
            detailequipeListNew = attachedDetailequipeListNew;
            personnel.setDetailequipeList(detailequipeListNew);
            personnel = em.merge(personnel);
            for (Detailequipe detailequipeListOldDetailequipe : detailequipeListOld) {
                if (!detailequipeListNew.contains(detailequipeListOldDetailequipe)) {
                    detailequipeListOldDetailequipe.setRefPersonnel(null);
                    detailequipeListOldDetailequipe = em.merge(detailequipeListOldDetailequipe);
                }
            }
            for (Detailequipe detailequipeListNewDetailequipe : detailequipeListNew) {
                if (!detailequipeListOld.contains(detailequipeListNewDetailequipe)) {
                    Personnel oldRefPersonnelOfDetailequipeListNewDetailequipe = detailequipeListNewDetailequipe.getRefPersonnel();
                    detailequipeListNewDetailequipe.setRefPersonnel(personnel);
                    detailequipeListNewDetailequipe = em.merge(detailequipeListNewDetailequipe);
                    if (oldRefPersonnelOfDetailequipeListNewDetailequipe != null && !oldRefPersonnelOfDetailequipeListNewDetailequipe.equals(personnel)) {
                        oldRefPersonnelOfDetailequipeListNewDetailequipe.getDetailequipeList().remove(detailequipeListNewDetailequipe);
                        oldRefPersonnelOfDetailequipeListNewDetailequipe = em.merge(oldRefPersonnelOfDetailequipeListNewDetailequipe);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = personnel.getCniPersonnel();
                if (findPersonnel(id) == null) {
                    throw new NonexistentEntityException("The personnel with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Personnel personnel;
            try {
                personnel = em.getReference(Personnel.class, id);
                personnel.getCniPersonnel();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The personnel with id " + id + " no longer exists.", enfe);
            }
            List<Detailequipe> detailequipeList = personnel.getDetailequipeList();
            for (Detailequipe detailequipeListDetailequipe : detailequipeList) {
                detailequipeListDetailequipe.setRefPersonnel(null);
                detailequipeListDetailequipe = em.merge(detailequipeListDetailequipe);
            }
            em.remove(personnel);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Personnel> findPersonnelEntities() {
        return findPersonnelEntities(true, -1, -1);
    }

    public List<Personnel> findPersonnelEntities(int maxResults, int firstResult) {
        return findPersonnelEntities(false, maxResults, firstResult);
    }

    private List<Personnel> findPersonnelEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Personnel.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Personnel findPersonnel(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Personnel.class, id);
        } finally {
            em.close();
        }
    }
    
    public Personnel findByCNI(String cni){
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select p from Personnel p  where p.cniPersonnel = :cni");
            q.setParameter("cni", cni);
            System.out.println("cni = "+cni);
            System.out.println(q.getResultList());
            return (Personnel) q.getSingleResult();
        }finally{
            em.close();
        }
    }

    public int getPersonnelCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Personnel> rt = cq.from(Personnel.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public Personnel connexion(String matricule, String password) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("from Personnel p where p.cniPersonnel = :matricule and p.password = :pass");
            q.setParameter("matricule", matricule);
            q.setParameter("pass", password);
            List l = q.getResultList();
            if(l.size()==1)
                return (Personnel) l.get(0);
        }finally{
            em.close();
        }
        return null;
    }
    
}
