/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Tranche;
import java.util.ArrayList;
import java.util.List;
import app.models.Colis;
import app.models.Detailequipe;
import app.models.Equipe;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class EquipeJpaController implements Serializable {

    public EquipeJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Equipe equipe) throws PreexistingEntityException, Exception {
        if (equipe.getTrancheList() == null) {
            equipe.setTrancheList(new ArrayList<Tranche>());
        }

        if (equipe.getDetailequipeList() == null) {
            equipe.setDetailequipeList(new ArrayList<Detailequipe>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Tranche> attachedTrancheList = new ArrayList<Tranche>();
            for (Tranche trancheListTrancheToAttach : equipe.getTrancheList()) {
                trancheListTrancheToAttach = em.getReference(trancheListTrancheToAttach.getClass(), trancheListTrancheToAttach.getRefTranche());
                attachedTrancheList.add(trancheListTrancheToAttach);
            }
            equipe.setTrancheList(attachedTrancheList);

            List<Detailequipe> attachedDetailequipeList = new ArrayList<Detailequipe>();
            for (Detailequipe detailequipeListDetailequipeToAttach : equipe.getDetailequipeList()) {
                detailequipeListDetailequipeToAttach = em.getReference(detailequipeListDetailequipeToAttach.getClass(), detailequipeListDetailequipeToAttach.getRefDetail());
                attachedDetailequipeList.add(detailequipeListDetailequipeToAttach);
            }
            equipe.setDetailequipeList(attachedDetailequipeList);
            em.persist(equipe);
            for (Tranche trancheListTranche : equipe.getTrancheList()) {
                Equipe oldRefEquipeOfTrancheListTranche = trancheListTranche.getRefEquipe();
                trancheListTranche.setRefEquipe(equipe);
                trancheListTranche = em.merge(trancheListTranche);
                if (oldRefEquipeOfTrancheListTranche != null) {
                    oldRefEquipeOfTrancheListTranche.getTrancheList().remove(trancheListTranche);
                    oldRefEquipeOfTrancheListTranche = em.merge(oldRefEquipeOfTrancheListTranche);
                }
            }

            for (Detailequipe detailequipeListDetailequipe : equipe.getDetailequipeList()) {
                Equipe oldRefEquipeOfDetailequipeListDetailequipe = detailequipeListDetailequipe.getRefEquipe();
                detailequipeListDetailequipe.setRefEquipe(equipe);
                detailequipeListDetailequipe = em.merge(detailequipeListDetailequipe);
                if (oldRefEquipeOfDetailequipeListDetailequipe != null) {
                    oldRefEquipeOfDetailequipeListDetailequipe.getDetailequipeList().remove(detailequipeListDetailequipe);
                    oldRefEquipeOfDetailequipeListDetailequipe = em.merge(oldRefEquipeOfDetailequipeListDetailequipe);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findEquipe(equipe.getRefEquipe()) != null) {
                throw new PreexistingEntityException("Equipe " + equipe + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Equipe equipe) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Equipe persistentEquipe = em.find(Equipe.class, equipe.getRefEquipe());
            List<Tranche> trancheListOld = persistentEquipe.getTrancheList();
            List<Tranche> trancheListNew = equipe.getTrancheList();

            List<Detailequipe> detailequipeListOld = persistentEquipe.getDetailequipeList();
            List<Detailequipe> detailequipeListNew = equipe.getDetailequipeList();
            List<Tranche> attachedTrancheListNew = new ArrayList<Tranche>();
            for (Tranche trancheListNewTrancheToAttach : trancheListNew) {
                trancheListNewTrancheToAttach = em.getReference(trancheListNewTrancheToAttach.getClass(), trancheListNewTrancheToAttach.getRefTranche());
                attachedTrancheListNew.add(trancheListNewTrancheToAttach);
            }
            trancheListNew = attachedTrancheListNew;
            equipe.setTrancheList(trancheListNew);

            List<Detailequipe> attachedDetailequipeListNew = new ArrayList<Detailequipe>();
            for (Detailequipe detailequipeListNewDetailequipeToAttach : detailequipeListNew) {
                detailequipeListNewDetailequipeToAttach = em.getReference(detailequipeListNewDetailequipeToAttach.getClass(), detailequipeListNewDetailequipeToAttach.getRefDetail());
                attachedDetailequipeListNew.add(detailequipeListNewDetailequipeToAttach);
            }
            detailequipeListNew = attachedDetailequipeListNew;
            equipe.setDetailequipeList(detailequipeListNew);
            equipe = em.merge(equipe);
            for (Tranche trancheListOldTranche : trancheListOld) {
                if (!trancheListNew.contains(trancheListOldTranche)) {
                    trancheListOldTranche.setRefEquipe(null);
                    trancheListOldTranche = em.merge(trancheListOldTranche);
                }
            }
            for (Tranche trancheListNewTranche : trancheListNew) {
                if (!trancheListOld.contains(trancheListNewTranche)) {
                    Equipe oldRefEquipeOfTrancheListNewTranche = trancheListNewTranche.getRefEquipe();
                    trancheListNewTranche.setRefEquipe(equipe);
                    trancheListNewTranche = em.merge(trancheListNewTranche);
                    if (oldRefEquipeOfTrancheListNewTranche != null && !oldRefEquipeOfTrancheListNewTranche.equals(equipe)) {
                        oldRefEquipeOfTrancheListNewTranche.getTrancheList().remove(trancheListNewTranche);
                        oldRefEquipeOfTrancheListNewTranche = em.merge(oldRefEquipeOfTrancheListNewTranche);
                    }
                }
            }


            for (Detailequipe detailequipeListOldDetailequipe : detailequipeListOld) {
                if (!detailequipeListNew.contains(detailequipeListOldDetailequipe)) {
                    detailequipeListOldDetailequipe.setRefEquipe(null);
                    detailequipeListOldDetailequipe = em.merge(detailequipeListOldDetailequipe);
                }
            }
            for (Detailequipe detailequipeListNewDetailequipe : detailequipeListNew) {
                if (!detailequipeListOld.contains(detailequipeListNewDetailequipe)) {
                    Equipe oldRefEquipeOfDetailequipeListNewDetailequipe = detailequipeListNewDetailequipe.getRefEquipe();
                    detailequipeListNewDetailequipe.setRefEquipe(equipe);
                    detailequipeListNewDetailequipe = em.merge(detailequipeListNewDetailequipe);
                    if (oldRefEquipeOfDetailequipeListNewDetailequipe != null && !oldRefEquipeOfDetailequipeListNewDetailequipe.equals(equipe)) {
                        oldRefEquipeOfDetailequipeListNewDetailequipe.getDetailequipeList().remove(detailequipeListNewDetailequipe);
                        oldRefEquipeOfDetailequipeListNewDetailequipe = em.merge(oldRefEquipeOfDetailequipeListNewDetailequipe);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = equipe.getRefEquipe();
                if (findEquipe(id) == null) {
                    throw new NonexistentEntityException("The equipe with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Equipe equipe;
            try {
                equipe = em.getReference(Equipe.class, id);
                equipe.getRefEquipe();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The equipe with id " + id + " no longer exists.", enfe);
            }
            List<Tranche> trancheList = equipe.getTrancheList();
            for (Tranche trancheListTranche : trancheList) {
                trancheListTranche.setRefEquipe(null);
                trancheListTranche = em.merge(trancheListTranche);
            }

            List<Detailequipe> detailequipeList = equipe.getDetailequipeList();
            for (Detailequipe detailequipeListDetailequipe : detailequipeList) {
                detailequipeListDetailequipe.setRefEquipe(null);
                detailequipeListDetailequipe = em.merge(detailequipeListDetailequipe);
            }
            em.remove(equipe);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Equipe> findEquipeEntities() {
        return findEquipeEntities(true, -1, -1);
    }

    public List<Equipe> findEquipeEntities(int maxResults, int firstResult) {
        return findEquipeEntities(false, maxResults, firstResult);
    }

    private List<Equipe> findEquipeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Equipe.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Equipe findEquipe(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Equipe.class, id);
        } finally {
            em.close();
        }
    }

    public int getEquipeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Equipe> rt = cq.from(Equipe.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
