/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.Chauffeur;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author champlain
 */
public class ChauffeurJpaController implements Serializable {

    public ChauffeurJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Chauffeur chauffeur) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(chauffeur);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findChauffeur(chauffeur.getRefChauffeur()) != null) {
                throw new PreexistingEntityException("Chauffeur " + chauffeur + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Chauffeur chauffeur) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            chauffeur = em.merge(chauffeur);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = chauffeur.getRefChauffeur();
                if (findChauffeur(id) == null) {
                    throw new NonexistentEntityException("The chauffeur with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Chauffeur chauffeur;
            try {
                chauffeur = em.getReference(Chauffeur.class, id);
                chauffeur.getRefChauffeur();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The chauffeur with id " + id + " no longer exists.", enfe);
            }
            em.remove(chauffeur);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Chauffeur> findChauffeurEntities() {
        return findChauffeurEntities(true, -1, -1);
    }

    public List<Chauffeur> findChauffeurEntities(int maxResults, int firstResult) {
        return findChauffeurEntities(false, maxResults, firstResult);
    }

    private List<Chauffeur> findChauffeurEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Chauffeur.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Chauffeur findChauffeur(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Chauffeur.class, id);
        } finally {
            em.close();
        }
    }

    public int getChauffeurCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Chauffeur> rt = cq.from(Chauffeur.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
