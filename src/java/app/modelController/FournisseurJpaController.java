/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Commande;
import app.models.Fournisseur;
import java.util.ArrayList;
import java.util.List;
import app.models.ParcChargement;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.util.Date;
import java.util.LinkedList;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class FournisseurJpaController implements Serializable {

    public FournisseurJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Fournisseur fournisseur) throws PreexistingEntityException, Exception {
        if (fournisseur.getCommandeList() == null) {
            fournisseur.setCommandeList(new ArrayList<Commande>());
        }
        if (fournisseur.getParcChargementList() == null) {
            fournisseur.setParcChargementList(new ArrayList<ParcChargement>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Commande> attachedCommandeList = new ArrayList<Commande>();
            for (Commande commandeListCommandeToAttach : fournisseur.getCommandeList()) {
                commandeListCommandeToAttach = em.getReference(commandeListCommandeToAttach.getClass(), commandeListCommandeToAttach.getRefCommande());
                attachedCommandeList.add(commandeListCommandeToAttach);
            }
            fournisseur.setCommandeList(attachedCommandeList);
            List<ParcChargement> attachedParcChargementList = new ArrayList<ParcChargement>();
            for (ParcChargement parcChargementListParcChargementToAttach : fournisseur.getParcChargementList()) {
                parcChargementListParcChargementToAttach = em.getReference(parcChargementListParcChargementToAttach.getClass(), parcChargementListParcChargementToAttach.getRefParc());
                attachedParcChargementList.add(parcChargementListParcChargementToAttach);
            }
            fournisseur.setParcChargementList(attachedParcChargementList);
            em.persist(fournisseur);
            for (Commande commandeListCommande : fournisseur.getCommandeList()) {
                Fournisseur oldRefFournisseurOfCommandeListCommande = commandeListCommande.getRefFournisseur();
                commandeListCommande.setRefFournisseur(fournisseur);
                commandeListCommande = em.merge(commandeListCommande);
                if (oldRefFournisseurOfCommandeListCommande != null) {
                    oldRefFournisseurOfCommandeListCommande.getCommandeList().remove(commandeListCommande);
                    oldRefFournisseurOfCommandeListCommande = em.merge(oldRefFournisseurOfCommandeListCommande);
                }
            }
            for (ParcChargement parcChargementListParcChargement : fournisseur.getParcChargementList()) {
                Fournisseur oldRefFournisseurOfParcChargementListParcChargement = parcChargementListParcChargement.getRefFournisseur();
                parcChargementListParcChargement.setRefFournisseur(fournisseur);
                parcChargementListParcChargement = em.merge(parcChargementListParcChargement);
                if (oldRefFournisseurOfParcChargementListParcChargement != null) {
                    oldRefFournisseurOfParcChargementListParcChargement.getParcChargementList().remove(parcChargementListParcChargement);
                    oldRefFournisseurOfParcChargementListParcChargement = em.merge(oldRefFournisseurOfParcChargementListParcChargement);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findFournisseur(fournisseur.getRefFournisseur()) != null) {
                throw new PreexistingEntityException("Fournisseur " + fournisseur + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Fournisseur fournisseur) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Fournisseur persistentFournisseur = em.find(Fournisseur.class, fournisseur.getRefFournisseur());
            List<Commande> commandeListOld = persistentFournisseur.getCommandeList();
            List<Commande> commandeListNew = fournisseur.getCommandeList();
            List<ParcChargement> parcChargementListOld = persistentFournisseur.getParcChargementList();
            List<ParcChargement> parcChargementListNew = fournisseur.getParcChargementList();
            List<Commande> attachedCommandeListNew = new ArrayList<Commande>();
            for (Commande commandeListNewCommandeToAttach : commandeListNew) {
                commandeListNewCommandeToAttach = em.getReference(commandeListNewCommandeToAttach.getClass(), commandeListNewCommandeToAttach.getRefCommande());
                attachedCommandeListNew.add(commandeListNewCommandeToAttach);
            }
            commandeListNew = attachedCommandeListNew;
            fournisseur.setCommandeList(commandeListNew);
            List<ParcChargement> attachedParcChargementListNew = new ArrayList<ParcChargement>();
            for (ParcChargement parcChargementListNewParcChargementToAttach : parcChargementListNew) {
                parcChargementListNewParcChargementToAttach = em.getReference(parcChargementListNewParcChargementToAttach.getClass(), parcChargementListNewParcChargementToAttach.getRefParc());
                attachedParcChargementListNew.add(parcChargementListNewParcChargementToAttach);
            }
            parcChargementListNew = attachedParcChargementListNew;
            fournisseur.setParcChargementList(parcChargementListNew);
            fournisseur = em.merge(fournisseur);
            for (Commande commandeListOldCommande : commandeListOld) {
                if (!commandeListNew.contains(commandeListOldCommande)) {
                    commandeListOldCommande.setRefFournisseur(null);
                    commandeListOldCommande = em.merge(commandeListOldCommande);
                }
            }
            for (Commande commandeListNewCommande : commandeListNew) {
                if (!commandeListOld.contains(commandeListNewCommande)) {
                    Fournisseur oldRefFournisseurOfCommandeListNewCommande = commandeListNewCommande.getRefFournisseur();
                    commandeListNewCommande.setRefFournisseur(fournisseur);
                    commandeListNewCommande = em.merge(commandeListNewCommande);
                    if (oldRefFournisseurOfCommandeListNewCommande != null && !oldRefFournisseurOfCommandeListNewCommande.equals(fournisseur)) {
                        oldRefFournisseurOfCommandeListNewCommande.getCommandeList().remove(commandeListNewCommande);
                        oldRefFournisseurOfCommandeListNewCommande = em.merge(oldRefFournisseurOfCommandeListNewCommande);
                    }
                }
            }
            for (ParcChargement parcChargementListOldParcChargement : parcChargementListOld) {
                if (!parcChargementListNew.contains(parcChargementListOldParcChargement)) {
                    parcChargementListOldParcChargement.setRefFournisseur(null);
                    parcChargementListOldParcChargement = em.merge(parcChargementListOldParcChargement);
                }
            }
            for (ParcChargement parcChargementListNewParcChargement : parcChargementListNew) {
                if (!parcChargementListOld.contains(parcChargementListNewParcChargement)) {
                    Fournisseur oldRefFournisseurOfParcChargementListNewParcChargement = parcChargementListNewParcChargement.getRefFournisseur();
                    parcChargementListNewParcChargement.setRefFournisseur(fournisseur);
                    parcChargementListNewParcChargement = em.merge(parcChargementListNewParcChargement);
                    if (oldRefFournisseurOfParcChargementListNewParcChargement != null && !oldRefFournisseurOfParcChargementListNewParcChargement.equals(fournisseur)) {
                        oldRefFournisseurOfParcChargementListNewParcChargement.getParcChargementList().remove(parcChargementListNewParcChargement);
                        oldRefFournisseurOfParcChargementListNewParcChargement = em.merge(oldRefFournisseurOfParcChargementListNewParcChargement);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = fournisseur.getRefFournisseur();
                if (findFournisseur(id) == null) {
                    throw new NonexistentEntityException("The fournisseur with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Fournisseur fournisseur;
            try {
                fournisseur = em.getReference(Fournisseur.class, id);
                fournisseur.getRefFournisseur();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The fournisseur with id " + id + " no longer exists.", enfe);
            }
            List<Commande> commandeList = fournisseur.getCommandeList();
            for (Commande commandeListCommande : commandeList) {
                commandeListCommande.setRefFournisseur(null);
                commandeListCommande = em.merge(commandeListCommande);
            }
            List<ParcChargement> parcChargementList = fournisseur.getParcChargementList();
            for (ParcChargement parcChargementListParcChargement : parcChargementList) {
                parcChargementListParcChargement.setRefFournisseur(null);
                parcChargementListParcChargement = em.merge(parcChargementListParcChargement);
            }
            em.remove(fournisseur);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Fournisseur> findFournisseurEntities() {
        return findFournisseurEntities(true, -1, -1);
    }

    public List<Fournisseur> findFournisseurEntities(int maxResults, int firstResult) {
        return findFournisseurEntities(false, maxResults, firstResult);
    }

    private List<Fournisseur> findFournisseurEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Fournisseur.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Fournisseur findFournisseur(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Fournisseur.class, id);
        } finally {
            em.close();
        }
    }

    public int getFournisseurCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Fournisseur> rt = cq.from(Fournisseur.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<Fournisseur> findFournisseurEntities(String origin, long...intervalTemps) {
        if(origin.equalsIgnoreCase("all"))
            return findFournisseurEntities();
        
        EntityManager em = getEntityManager();
        try{
            Query q;
            String s = ""
                    + "select distinct f "
                    + "from Fournisseur f, Reception r, Parc p, DetailReception dr "
                    + "where f.refFournisseur = r.refCommande.refFournisseur.refFournisseur "
                    + "and r.refReception = dr.refReception.refReception "
                    + "and dr.numBille = p.numBille "
                    + "and p.refTypeparc.refTypeparc = :type";
            if(intervalTemps[0] == 0 && intervalTemps[1] == 0)
                q = em.createQuery(s);
            else if(intervalTemps[0] > 0 && intervalTemps[1]==0){ // NUMTODSINTERVAL(MMSTAMP, SECOND)
                q = em.createQuery(s+" AND r.create_at >= :date");
                q.setParameter("date", new Date(intervalTemps[0]));
            }else if(intervalTemps[0] == 0 && intervalTemps[1]>0){
                q = em.createQuery(s+" AND r.create_at <= :date");
                q.setParameter("date", new Date(intervalTemps[1]));
            }else if(intervalTemps[0]<intervalTemps[1]){
                q = em.createQuery(s+" AND r.create_at BETWEEN :dateD AND :dateF");
                q.setParameter("dateD", new Date(intervalTemps[0]));
                q.setParameter("dateF", new Date(intervalTemps[1]));
            }else
                q = em.createQuery(s);

            q.setParameter("type", origin);
            System.err.println(q.toString());
        return q.getResultList();
        }catch(Exception e){
            System.out.println(e.getMessage());
            return new LinkedList();
        }finally{
            em.close();
        }
    }
    
}
