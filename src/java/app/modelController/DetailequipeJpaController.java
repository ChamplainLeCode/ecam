/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.models.Detailequipe;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Equipe;
import app.models.Personnel;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class DetailequipeJpaController implements Serializable {

    public DetailequipeJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Detailequipe detailequipe) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Equipe refEquipe = detailequipe.getRefEquipe();
            if (refEquipe != null) {
                refEquipe = em.getReference(refEquipe.getClass(), refEquipe.getRefEquipe());
                detailequipe.setRefEquipe(refEquipe);
            }
            Personnel refPersonnel = detailequipe.getRefPersonnel();
            if (refPersonnel != null) {
                refPersonnel = em.getReference(refPersonnel.getClass(), refPersonnel.getCniPersonnel());
                detailequipe.setRefPersonnel(refPersonnel);
            }
            em.persist(detailequipe);
            if (refEquipe != null) {
                refEquipe.getDetailequipeList().add(detailequipe);
                refEquipe = em.merge(refEquipe);
            }
            if (refPersonnel != null) {
                refPersonnel.getDetailequipeList().add(detailequipe);
                refPersonnel = em.merge(refPersonnel);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDetailequipe(detailequipe.getRefDetail()) != null) {
                throw new PreexistingEntityException("Detailequipe " + detailequipe + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Detailequipe detailequipe) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Detailequipe persistentDetailequipe = em.find(Detailequipe.class, detailequipe.getRefDetail());
            Equipe refEquipeOld = persistentDetailequipe.getRefEquipe();
            Equipe refEquipeNew = detailequipe.getRefEquipe();
            Personnel refPersonnelOld = persistentDetailequipe.getRefPersonnel();
            Personnel refPersonnelNew = detailequipe.getRefPersonnel();
            if (refEquipeNew != null) {
                refEquipeNew = em.getReference(refEquipeNew.getClass(), refEquipeNew.getRefEquipe());
                detailequipe.setRefEquipe(refEquipeNew);
            }
            if (refPersonnelNew != null) {
                refPersonnelNew = em.getReference(refPersonnelNew.getClass(), refPersonnelNew.getCniPersonnel());
                detailequipe.setRefPersonnel(refPersonnelNew);
            }
            detailequipe = em.merge(detailequipe);
            if (refEquipeOld != null && !refEquipeOld.equals(refEquipeNew)) {
                refEquipeOld.getDetailequipeList().remove(detailequipe);
                refEquipeOld = em.merge(refEquipeOld);
            }
            if (refEquipeNew != null && !refEquipeNew.equals(refEquipeOld)) {
                refEquipeNew.getDetailequipeList().add(detailequipe);
                refEquipeNew = em.merge(refEquipeNew);
            }
            if (refPersonnelOld != null && !refPersonnelOld.equals(refPersonnelNew)) {
                refPersonnelOld.getDetailequipeList().remove(detailequipe);
                refPersonnelOld = em.merge(refPersonnelOld);
            }
            if (refPersonnelNew != null && !refPersonnelNew.equals(refPersonnelOld)) {
                refPersonnelNew.getDetailequipeList().add(detailequipe);
                refPersonnelNew = em.merge(refPersonnelNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = detailequipe.getRefDetail();
                if (findDetailequipe(id) == null) {
                    throw new NonexistentEntityException("The detailequipe with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Detailequipe detailequipe;
            try {
                detailequipe = em.getReference(Detailequipe.class, id);
                detailequipe.getRefDetail();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The detailequipe with id " + id + " no longer exists.", enfe);
            }
            Equipe refEquipe = detailequipe.getRefEquipe();
            if (refEquipe != null) {
                refEquipe.getDetailequipeList().remove(detailequipe);
                refEquipe = em.merge(refEquipe);
            }
            Personnel refPersonnel = detailequipe.getRefPersonnel();
            if (refPersonnel != null) {
                refPersonnel.getDetailequipeList().remove(detailequipe);
                refPersonnel = em.merge(refPersonnel);
            }
            em.remove(detailequipe);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Detailequipe> findDetailequipeEntities() {
        return findDetailequipeEntities(true, -1, -1);
    }

    public List<Detailequipe> findDetailequipeEntities(int maxResults, int firstResult) {
        return findDetailequipeEntities(false, maxResults, firstResult);
    }

    private List<Detailequipe> findDetailequipeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Detailequipe.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Detailequipe findDetailequipe(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Detailequipe.class, id);
        } finally {
            em.close();
        }
    }

    public int getDetailequipeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Detailequipe> rt = cq.from(Detailequipe.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
