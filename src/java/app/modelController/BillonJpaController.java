/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.models.Billon;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Billonnage;
import app.models.Coupe;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.Database;
import app.models.Plot;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;


/**
 *
 * @author champlain
 */
public class BillonJpaController implements Serializable {

    

    public BillonJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Billon billon) throws PreexistingEntityException, Exception {
        if (billon.getCoupeList() == null) {
            billon.setCoupeList(new ArrayList<Coupe>());
        }
        if (billon.getPlotList() == null) {
            billon.setPlotList(new ArrayList<Plot>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Billonnage refBillonnage = billon.getRefBillonnage();
            if (refBillonnage != null) {
                refBillonnage = em.getReference(refBillonnage.getClass(), refBillonnage.getRefBillonnage());
                billon.setRefBillonnage(refBillonnage);
            }
            Coupe coupe = billon.getCoupe();
            if (coupe != null) {
                coupe = em.getReference(coupe.getClass(), coupe.getRefCoupe());
                billon.setCoupe(coupe);
            }
            List<Coupe> attachedCoupeList = new ArrayList<Coupe>();
            for (Coupe coupeListCoupeToAttach : billon.getCoupeList()) {
                coupeListCoupeToAttach = em.getReference(coupeListCoupeToAttach.getClass(), coupeListCoupeToAttach.getRefCoupe());
                attachedCoupeList.add(coupeListCoupeToAttach);
            }
            billon.setCoupeList(attachedCoupeList);
            List<Plot> attachedPlotList = new ArrayList<Plot>();
            for (Plot plotListPlotToAttach : billon.getPlotList()) {
                plotListPlotToAttach = em.getReference(plotListPlotToAttach.getClass(), plotListPlotToAttach.getRefPlot());
                attachedPlotList.add(plotListPlotToAttach);
            }
            billon.setPlotList(attachedPlotList);
            em.persist(billon);
            if (refBillonnage != null) {
                refBillonnage.getBillonList().add(billon);
                refBillonnage = em.merge(refBillonnage);
            }
            if (coupe != null) {
                Billon oldRefBillonOfCoupe = coupe.getRefBillon();
                if (oldRefBillonOfCoupe != null) {
                    oldRefBillonOfCoupe.setCoupe(null);
                    oldRefBillonOfCoupe = em.merge(oldRefBillonOfCoupe);
                }
                coupe.setRefBillon(billon);
                coupe = em.merge(coupe);
            }
            for (Coupe coupeListCoupe : billon.getCoupeList()) {
                Billon oldRefBillonOfCoupeListCoupe = coupeListCoupe.getRefBillon();
                coupeListCoupe.setRefBillon(billon);
                coupeListCoupe = em.merge(coupeListCoupe);
                if (oldRefBillonOfCoupeListCoupe != null) {
                    oldRefBillonOfCoupeListCoupe.getCoupeList().remove(coupeListCoupe);
                    oldRefBillonOfCoupeListCoupe = em.merge(oldRefBillonOfCoupeListCoupe);
                }
            }
            for (Plot plotListPlot : billon.getPlotList()) {
                Billon oldRefBillonOfPlotListPlot = plotListPlot.getRefBillon();
                plotListPlot.setRefBillon(billon);
                plotListPlot = em.merge(plotListPlot);
                if (oldRefBillonOfPlotListPlot != null) {
                    oldRefBillonOfPlotListPlot.getPlotList().remove(plotListPlot);
                    oldRefBillonOfPlotListPlot = em.merge(oldRefBillonOfPlotListPlot);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findBillon(billon.getRefBillon()) != null) {
                throw new PreexistingEntityException("Billon " + billon + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Billon billon) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Billon persistentBillon = em.find(Billon.class, billon.getRefBillon());
            Billonnage refBillonnageOld = persistentBillon.getRefBillonnage();
            Billonnage refBillonnageNew = billon.getRefBillonnage();
            Coupe coupeOld = persistentBillon.getCoupe();
            Coupe coupeNew = billon.getCoupe();
            List<Coupe> coupeListOld = persistentBillon.getCoupeList();
            List<Coupe> coupeListNew = billon.getCoupeList();
            List<Plot> plotListOld = persistentBillon.getPlotList();
            List<Plot> plotListNew = billon.getPlotList();
            if (refBillonnageNew != null) {
                refBillonnageNew = em.getReference(refBillonnageNew.getClass(), refBillonnageNew.getRefBillonnage());
                billon.setRefBillonnage(refBillonnageNew);
            }
            if (coupeNew != null) {
                coupeNew = em.getReference(coupeNew.getClass(), coupeNew.getRefCoupe());
                billon.setCoupe(coupeNew);
            }
            List<Coupe> attachedCoupeListNew = new ArrayList<Coupe>();
            for (Coupe coupeListNewCoupeToAttach : coupeListNew) {
                coupeListNewCoupeToAttach = em.getReference(coupeListNewCoupeToAttach.getClass(), coupeListNewCoupeToAttach.getRefCoupe());
                attachedCoupeListNew.add(coupeListNewCoupeToAttach);
            }
            coupeListNew = attachedCoupeListNew;
            billon.setCoupeList(coupeListNew);
            List<Plot> attachedPlotListNew = new ArrayList<Plot>();
            for (Plot plotListNewPlotToAttach : plotListNew) {
                plotListNewPlotToAttach = em.getReference(plotListNewPlotToAttach.getClass(), plotListNewPlotToAttach.getRefPlot());
                attachedPlotListNew.add(plotListNewPlotToAttach);
            }
            plotListNew = attachedPlotListNew;
            billon.setPlotList(plotListNew);
            billon = em.merge(billon);
            if (refBillonnageOld != null && !refBillonnageOld.equals(refBillonnageNew)) {
                refBillonnageOld.getBillonList().remove(billon);
                refBillonnageOld = em.merge(refBillonnageOld);
            }
            if (refBillonnageNew != null && !refBillonnageNew.equals(refBillonnageOld)) {
                refBillonnageNew.getBillonList().add(billon);
                refBillonnageNew = em.merge(refBillonnageNew);
            }
            if (coupeOld != null && !coupeOld.equals(coupeNew)) {
                coupeOld.setRefBillon(null);
                coupeOld = em.merge(coupeOld);
            }
            if (coupeNew != null && !coupeNew.equals(coupeOld)) {
                Billon oldRefBillonOfCoupe = coupeNew.getRefBillon();
                if (oldRefBillonOfCoupe != null) {
                    oldRefBillonOfCoupe.setCoupe(null);
                    oldRefBillonOfCoupe = em.merge(oldRefBillonOfCoupe);
                }
                coupeNew.setRefBillon(billon);
                coupeNew = em.merge(coupeNew);
            }
            for (Coupe coupeListOldCoupe : coupeListOld) {
                if (!coupeListNew.contains(coupeListOldCoupe)) {
                    coupeListOldCoupe.setRefBillon(null);
                    coupeListOldCoupe = em.merge(coupeListOldCoupe);
                }
            }
            for (Coupe coupeListNewCoupe : coupeListNew) {
                if (!coupeListOld.contains(coupeListNewCoupe)) {
                    Billon oldRefBillonOfCoupeListNewCoupe = coupeListNewCoupe.getRefBillon();
                    coupeListNewCoupe.setRefBillon(billon);
                    coupeListNewCoupe = em.merge(coupeListNewCoupe);
                    if (oldRefBillonOfCoupeListNewCoupe != null && !oldRefBillonOfCoupeListNewCoupe.equals(billon)) {
                        oldRefBillonOfCoupeListNewCoupe.getCoupeList().remove(coupeListNewCoupe);
                        oldRefBillonOfCoupeListNewCoupe = em.merge(oldRefBillonOfCoupeListNewCoupe);
                    }
                }
            }
            for (Plot plotListOldPlot : plotListOld) {
                if (!plotListNew.contains(plotListOldPlot)) {
                    plotListOldPlot.setRefBillon(null);
                    plotListOldPlot = em.merge(plotListOldPlot);
                }
            }
            for (Plot plotListNewPlot : plotListNew) {
                if (!plotListOld.contains(plotListNewPlot)) {
                    Billon oldRefBillonOfPlotListNewPlot = plotListNewPlot.getRefBillon();
                    plotListNewPlot.setRefBillon(billon);
                    plotListNewPlot = em.merge(plotListNewPlot);
                    if (oldRefBillonOfPlotListNewPlot != null && !oldRefBillonOfPlotListNewPlot.equals(billon)) {
                        oldRefBillonOfPlotListNewPlot.getPlotList().remove(plotListNewPlot);
                        oldRefBillonOfPlotListNewPlot = em.merge(oldRefBillonOfPlotListNewPlot);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = billon.getRefBillon();
                if (findBillon(id) == null) {
                    throw new NonexistentEntityException("The billon with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Billon billon;
            try {
                billon = em.getReference(Billon.class, id);
                billon.getRefBillon();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The billon with id " + id + " no longer exists.", enfe);
            }
            Billonnage refBillonnage = billon.getRefBillonnage();
            if (refBillonnage != null) {
                refBillonnage.getBillonList().remove(billon);
                refBillonnage = em.merge(refBillonnage);
            }
            Coupe coupe = billon.getCoupe();
            if (coupe != null) {
                coupe.setRefBillon(null);
                coupe = em.merge(coupe);
            }
            List<Coupe> coupeList = billon.getCoupeList();
            for (Coupe coupeListCoupe : coupeList) {
                coupeListCoupe.setRefBillon(null);
                coupeListCoupe = em.merge(coupeListCoupe);
            }
            List<Plot> plotList = billon.getPlotList();
            for (Plot plotListPlot : plotList) {
                plotListPlot.setRefBillon(null);
                plotListPlot = em.merge(plotListPlot);
            }
            em.remove(billon);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Billon> findBillonEntities() {
        return findBillonEntities(true, -1, -1);
    }

    public List<Billon> findBillonEntities(int maxResults, int firstResult) {
        return findBillonEntities(false, maxResults, firstResult);
    }

    private List<Billon> findBillonEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Billon.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Billon findBillon(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Billon.class, id);
        } finally {
            em.close();
        }
    }

    public int getBillonCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Billon> rt = cq.from(Billon.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public static List<Billon> findByBillonnage(String refBillonnage) {
        EntityManager em = Database.getEntityManager().createEntityManager();
        try{
            Query q = em.createQuery("select b from Billon b where b.refBillonnage.refBillonnage = :refBillonnage", Billon.class);
            q.setParameter("refBillonnage", refBillonnage);
            return q.getResultList();
        }finally{
            em.close();
        }
    }

    /**
     *  Cette fonction permet de retourner les billons associés à une bille, 
     * le filtre ici est par rapport au numBille
     * @param em instance de connexion à la base de donnée EntityManager
     * @param numBille Numéro de la bille
     * @return List of Billon (RefBillon, NumTravail, Longueur)
     */
    public static List findByBilleNum(EntityManager em, String numBille) {
        try{
            Query q = em.createQuery("select bil.refBillon, bil.numTravail, bil.longueur as travail from Billon bil where bil.refBillon LIKE :numBille order by bil.refBillon asc");
            q.setParameter("numBille", numBille+"%");
            return q.getResultList();
        }finally{
            em.close();
        }
    }

    /**
     *  Cette fonction permet de retourner les billons associés à une bille, 
     * le filtre ici est par rapport au numBille
     * @param em instance de connexion à la base de donnée EntityManager
     * @param numBille Numéro de la bille
     * @return List of Billon (RefBillon, NumTravail, Longueur)
     */
    public static List findByBilleNumForCoupe(EntityManager em, String numBille) {
        try{
            Query q = em.createQuery(""
                    + "select bil.refBillon, bil.numTravail, bil.longueur as travail "
                    + "from Billon bil "
                    + "where bil.refBillon LIKE :numBille AND bil.refBillon not in (SELECT coup.refBillon.refBillon FROM Coupe coup) "
                    + "ORDER BY bil.refBillon ASC");
            q.setParameter("numBille", numBille+"%");
            return q.getResultList();
        }finally{
            em.close();
        }
    }

    public static List findBillonForMassicot(EntityManager em) {
        
        try{
            Query q = em.createQuery(""
                    + "select bil.refBillon "
                    + "from Billon bil ");
//                    + "where bil.refBillon not in (select distinct paq.refPlot.refBillon.refBillon from Paquet paq)");
            return q.getResultList();
        }finally{
            em.close();
        }
    }

    public static List findBilleForMassicot(EntityManager em) {
        try{
            
            Query q = em.createQuery(""
                    + "select distinct tr.numTravail, tr.refPlot.refPlot, dr.refEssence.refEssence, tr.epaisseur "
                    + "from Tranche tr, DetailReception dr "
                    //+ "where tr.refPlot.refPlot not in (select mp.refPaquet.refPlot.refPlot from Massicotpaquet mp) and tr.numTravail = dr.numtravail");
                    + "where tr.refPlot.refPlot not in (select pt.refPlot.refPlot from Paquet pt) and tr.numTravail = dr.numtravail");
//                    + "where bil.refBillon not in (select distinct paq.refPlot.refBillon.refBillon from Paquet paq)");
            return q.getResultList();
        }catch(Exception e){
            System.out.println("----- JPA -----------");
            return new LinkedList();
        }finally{
            em.close();
        }
    }

    
    
}
