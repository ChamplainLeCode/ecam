/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.Transporteur;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author champlain
 */
public class TransporteurJpaController implements Serializable {

    public TransporteurJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Transporteur transporteur) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(transporteur);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTransporteur(transporteur.getRefTransporteur()) != null) {
                throw new PreexistingEntityException("Transporteur " + transporteur + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Transporteur transporteur) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            transporteur = em.merge(transporteur);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = transporteur.getRefTransporteur();
                if (findTransporteur(id) == null) {
                    throw new NonexistentEntityException("The transporteur with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Transporteur transporteur;
            try {
                transporteur = em.getReference(Transporteur.class, id);
                transporteur.getRefTransporteur();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The transporteur with id " + id + " no longer exists.", enfe);
            }
            em.remove(transporteur);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Transporteur> findTransporteurEntities() {
        return findTransporteurEntities(true, -1, -1);
    }

    public List<Transporteur> findTransporteurEntities(int maxResults, int firstResult) {
        return findTransporteurEntities(false, maxResults, firstResult);
    }

    private List<Transporteur> findTransporteurEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Transporteur.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Transporteur findTransporteur(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Transporteur.class, id);
        } finally {
            em.close();
        }
    }

    public int getTransporteurCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Transporteur> rt = cq.from(Transporteur.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
