/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Colis;
import app.models.Database;
import app.models.Embarquement;
import app.models.ParcChargement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class ContennaireJpaController implements Serializable {

    public static double getVolumeFromContennaireByEssenceAndDate(String essence, String dateDebut, String dateFin) {
        Date dd = new Date(Long.parseLong(dateDebut));
        Date df = new Date(Long.parseLong(dateFin));
        
        EntityManager em = Database.getEntityManager().createEntityManager();
        
        try{
            Query q = em.createQuery("FROM Embarquement e where e.createAt between :d1 and :d2", Embarquement.class);
            q.setParameter("d1", dd);
            q.setParameter("d2", df);
            double volume = 0;
            List<Embarquement> le = q.getResultList();
            for (Embarquement e : le) {
                e.toString();
                volume = e.getColisList().stream().filter((c) -> (c.getEssence().equalsIgnoreCase(essence))).map((c) -> c.getVoulumeColis()).reduce(volume, (accumulator, _item) -> accumulator + _item);
            }
            return volume;
        }catch(Exception e){
            return 0D;
        }finally{
            em.close();
        }
    }

    public ContennaireJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Embarquement contennaire) throws PreexistingEntityException, Exception {
        if (contennaire.getColisList() == null) {
            contennaire.setColisList(new ArrayList<Colis>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            List<Colis> attachedColisList = new ArrayList<Colis>();
            for (Colis colisListColisToAttach : contennaire.getColisList()) {
                colisListColisToAttach = em.getReference(colisListColisToAttach.getClass(), colisListColisToAttach.getRefColis());
                attachedColisList.add(colisListColisToAttach);
            }
            contennaire.setColisList(attachedColisList);
            em.persist(contennaire);

            for (Colis colisListColis : contennaire.getColisList()) {
                Embarquement oldRefContennaireOfColisListColis = colisListColis.getNumPlomb();
                colisListColis.setNumPlomb(contennaire);
                colisListColis = em.merge(colisListColis);
                if (oldRefContennaireOfColisListColis != null) {
                    oldRefContennaireOfColisListColis.getColisList().remove(colisListColis);
                    oldRefContennaireOfColisListColis = em.merge(oldRefContennaireOfColisListColis);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findContennaire(contennaire.getNumPlomb()) != null) {
                throw new PreexistingEntityException("Contennaire " + contennaire + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Embarquement contennaire) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Embarquement persistentContennaire = em.find(Embarquement.class, contennaire.getNumPlomb());

            List<Colis> colisListOld = persistentContennaire.getColisList();
            List<Colis> colisListNew = contennaire.getColisList();

            List<Colis> attachedColisListNew = new ArrayList<Colis>();
            for (Colis colisListNewColisToAttach : colisListNew) {
                colisListNewColisToAttach = em.getReference(colisListNewColisToAttach.getClass(), colisListNewColisToAttach.getRefColis());
                attachedColisListNew.add(colisListNewColisToAttach);
            }
            colisListNew = attachedColisListNew;
            contennaire.setColisList(colisListNew);
            contennaire = em.merge(contennaire);

            for (Colis colisListOldColis : colisListOld) {
                if (!colisListNew.contains(colisListOldColis)) {
                    colisListOldColis.setNumPlomb(null);
                    colisListOldColis = em.merge(colisListOldColis);
                }
            }
            for (Colis colisListNewColis : colisListNew) {
                if (!colisListOld.contains(colisListNewColis)) {
                    Embarquement oldRefContennaireOfColisListNewColis = colisListNewColis.getNumPlomb();
                    colisListNewColis.setNumPlomb(contennaire);
                    colisListNewColis = em.merge(colisListNewColis);
                    if (oldRefContennaireOfColisListNewColis != null && !oldRefContennaireOfColisListNewColis.equals(contennaire)) {
                        oldRefContennaireOfColisListNewColis.getColisList().remove(colisListNewColis);
                        oldRefContennaireOfColisListNewColis = em.merge(oldRefContennaireOfColisListNewColis);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = contennaire.getNumPlomb();
                if (findContennaire(id) == null) {
                    throw new NonexistentEntityException("The contennaire with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Embarquement contennaire;
            try {
                contennaire = em.getReference(Embarquement.class, id);
                contennaire.getNumPlomb();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The contennaire with id " + id + " no longer exists.", enfe);
            }

            List<Colis> colisList = contennaire.getColisList();
            for (Colis colisListColis : colisList) {
                colisListColis.setNumPlomb(null);
                colisListColis = em.merge(colisListColis);
            }
            em.remove(contennaire);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Embarquement> findContennaireEntities() {
        return findContennaireEntities(true, -1, -1);
    }

    public List<Embarquement> findContennaireEntities(int maxResults, int firstResult) {
        return findContennaireEntities(false, maxResults, firstResult);
    }

    private List<Embarquement> findContennaireEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Embarquement.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Embarquement findContennaire(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Embarquement.class, id);
        } finally {
            em.close();
        }
    }

    public int getContennaireCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Embarquement> rt = cq.from(Embarquement.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<HashMap> getTracabilite(List<Colis> colisList) {
        List<HashMap> liste = new LinkedList<>();
        List<Colis> listColisJoints = new LinkedList<>();
        
        colisList.stream().forEach(new Consumer<Colis>() {
            @Override
            public void accept(Colis c) {
        
                HashMap<String, Object> o = new HashMap<>();
                o.put("essence", c.getEssence());
                o.put("volume", c.getVoulumeColis()+"");
                o.put("colis", c.getRefColis());
                ParcChargement p = new ParcChargementJpaController(Database.getEntityManager()).findParcChargementForColis(c.getRefColis(), c.getEssence());
                Collection<? extends Colis> tmpList;
                if(p == null)
                    if((tmpList = new ColisJpaController(Database.getEntityManager()).getColisJoints(c)).isEmpty())
                        return;
                    else{
                        listColisJoints.addAll(tmpList);
                        return;
                    }
                o.put("parc", p);
                liste.add(o);
            }
        });
        
        
        listColisJoints.stream().forEach(c ->{
                HashMap<String, Object> o = new HashMap<>();
                o.put("essence", c.getEssence());
                o.put("volume", c.getVoulumeColis()+"");
                ParcChargement p = new ParcChargementJpaController(Database.getEntityManager()).findParcChargementForColis(c.getRefColis(), c.getEssence());
                System.out.println(c.getRefColis()+" => "+p);
                if(p == null)
                    return;
                o.put("parc", p);
                liste.add(o);
        });
        
        return liste;
    }
    
    
}
