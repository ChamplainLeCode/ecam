/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Equipe;
import app.models.Plot;
import app.models.Tranche;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.Database;
import java.io.IOException;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author champlain
 */
public class TrancheJpaController implements Serializable {


    public TrancheJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Tranche tranche) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Equipe refEquipe = tranche.getRefEquipe();
            if (refEquipe != null) {
                refEquipe = em.getReference(refEquipe.getClass(), refEquipe.getRefEquipe());
                tranche.setRefEquipe(refEquipe);
            }
            Plot refPlot = tranche.getRefPlot();
            if (refPlot != null) {
                refPlot = em.getReference(refPlot.getClass(), refPlot.getRefPlot());
                tranche.setRefPlot(refPlot);
            }
            em.persist(tranche);
            if (refEquipe != null) {
                refEquipe.getTrancheList().add(tranche);
                refEquipe = em.merge(refEquipe);
            }
            if (refPlot != null) {
                refPlot.getTrancheList().add(tranche);
                refPlot = em.merge(refPlot);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTranche(tranche.getRefTranche()) != null) {
                throw new PreexistingEntityException("Tranche " + tranche + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Tranche tranche) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Tranche persistentTranche = em.find(Tranche.class, tranche.getRefTranche());
            Equipe refEquipeOld = persistentTranche.getRefEquipe();
            Equipe refEquipeNew = tranche.getRefEquipe();
            Plot refPlotOld = persistentTranche.getRefPlot();
            Plot refPlotNew = tranche.getRefPlot();
            if (refEquipeNew != null) {
                refEquipeNew = em.getReference(refEquipeNew.getClass(), refEquipeNew.getRefEquipe());
                tranche.setRefEquipe(refEquipeNew);
            }
            if (refPlotNew != null) {
                refPlotNew = em.getReference(refPlotNew.getClass(), refPlotNew.getRefPlot());
                tranche.setRefPlot(refPlotNew);
            }
            tranche = em.merge(tranche);
            if (refEquipeOld != null && !refEquipeOld.equals(refEquipeNew)) {
                refEquipeOld.getTrancheList().remove(tranche);
                refEquipeOld = em.merge(refEquipeOld);
            }
            if (refEquipeNew != null && !refEquipeNew.equals(refEquipeOld)) {
                refEquipeNew.getTrancheList().add(tranche);
                refEquipeNew = em.merge(refEquipeNew);
            }
            if (refPlotOld != null && !refPlotOld.equals(refPlotNew)) {
                refPlotOld.getTrancheList().remove(tranche);
                refPlotOld = em.merge(refPlotOld);
            }
            if (refPlotNew != null && !refPlotNew.equals(refPlotOld)) {
                refPlotNew.getTrancheList().add(tranche);
                refPlotNew = em.merge(refPlotNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = tranche.getRefTranche();
                if (findTranche(id) == null) {
                    throw new NonexistentEntityException("The tranche with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Tranche tranche;
            try {
                tranche = em.getReference(Tranche.class, id);
                tranche.getRefTranche();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tranche with id " + id + " no longer exists.", enfe);
            }
            Equipe refEquipe = tranche.getRefEquipe();
            if (refEquipe != null) {
                refEquipe.getTrancheList().remove(tranche);
                refEquipe = em.merge(refEquipe);
            }
            Plot refPlot = tranche.getRefPlot();
            if (refPlot != null) {
                refPlot.getTrancheList().remove(tranche);
                refPlot = em.merge(refPlot);
            }
            em.remove(tranche);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Tranche> findTrancheEntities() {
        return findTrancheEntities(true, -1, -1);
    }

    public List<Tranche> findTrancheEntities(int maxResults, int firstResult) {
        return findTrancheEntities(false, maxResults, firstResult);
    }

    private List<Tranche> findTrancheEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Tranche.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Tranche findTranche(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Tranche.class, id);
        } finally {
            em.close();
        }
    }

    public int getTrancheCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Tranche> rt = cq.from(Tranche.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public static void getEpaisseurForPlot(HttpServletRequest req, HttpServletResponse res) {
        res.setHeader("Content-Type", "Application/json");
        try{
            EntityManager em = Database.getEntityManager().createEntityManager();
            Query q = em.createQuery("Select tranch.epaisseur from Tranche tranch where tranch.refPlot.refPlot = :refPlot");
            q.setParameter("refPlot", req.getParameter("plot"));
            Object result = q.getSingleResult();
            if(result == null)
                res.getOutputStream().print("[\"0/10\"]");
            else
                res.getOutputStream().print("[\""+result.toString()+"\"]");
    
            em.close();
        }catch(Exception e){
            try {
                e.printStackTrace(new PrintStream(res.getOutputStream()));
            } catch (IOException ex) {
            }
        }
    }
    
    public static float getEpaisseurForNumTravail(String travail) {

        EntityManager em = Database.getEntityManager().createEntityManager();

        try{
            Query q = em.createQuery("Select distinct pt.numTravail, c.epaisseur from Colis c, Paquet pt where c.refColis = pt.refColis.refColis and pt.numTravail = :travail");
            q.setParameter("travail", travail);
            Object[] result = (Object[])q.getSingleResult();
            return Float.parseFloat(result[1].toString());
        }catch(Exception e){
            return 0;
        }finally{
            em.close();
        }
    }
    
    
    /**
     * return numTravail => {(qualite, Somme(surface(qualite, numTravail)}
     * @param travail
     * @return tableau de taille deux(2) contenant la qualite et la surface(proportion) pour ce numéro de travail
     * result[0] => qualite 
     * result[1] => surface 
     */
    public static List<Object[]> getQualiteProportionForNumTravail(String travail) {

        EntityManager em = Database.getEntityManager().createEntityManager();

        try{
            Query q = em.createQuery(""
                    + "Select distinct pt.qualite as qualite, SUM(pt.longueur * pt.largeur) as surface, pt.numTravail "
                    + "from Paquet pt "
                    + "where pt.numTravail = :travail "
                    + "group by pt.qualite, pt.numTravail");
            q.setParameter("travail", travail);
            List<Object[]> result = q.getResultList();
            return result;
        }catch(Exception e){
            e.printStackTrace();
            return new LinkedList();
        }finally{
            em.close();
        }
    }
}
