/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.models.Cuve;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.CuvePlot;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class CuveJpaController implements Serializable {

    public CuveJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Cuve cuve) throws PreexistingEntityException, Exception {
        if (cuve.getCuvePlotList() == null) {
            cuve.setCuvePlotList(new ArrayList<CuvePlot>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<CuvePlot> attachedCuvePlotList = new ArrayList<CuvePlot>();
            for (CuvePlot cuvePlotListCuvePlotToAttach : cuve.getCuvePlotList()) {
                cuvePlotListCuvePlotToAttach = em.getReference(cuvePlotListCuvePlotToAttach.getClass(), cuvePlotListCuvePlotToAttach.getRefCuveplot());
                attachedCuvePlotList.add(cuvePlotListCuvePlotToAttach);
            }
            cuve.setCuvePlotList(attachedCuvePlotList);
            em.persist(cuve);
            for (CuvePlot cuvePlotListCuvePlot : cuve.getCuvePlotList()) {
                Cuve oldRefCuveOfCuvePlotListCuvePlot = cuvePlotListCuvePlot.getRefCuve();
                cuvePlotListCuvePlot.setRefCuve(cuve);
                cuvePlotListCuvePlot = em.merge(cuvePlotListCuvePlot);
                if (oldRefCuveOfCuvePlotListCuvePlot != null) {
                    oldRefCuveOfCuvePlotListCuvePlot.getCuvePlotList().remove(cuvePlotListCuvePlot);
                    oldRefCuveOfCuvePlotListCuvePlot = em.merge(oldRefCuveOfCuvePlotListCuvePlot);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCuve(cuve.getRefCuve()) != null) {
                throw new PreexistingEntityException("Cuve " + cuve + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Cuve cuve) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cuve persistentCuve = em.find(Cuve.class, cuve.getRefCuve());
            List<CuvePlot> cuvePlotListOld = persistentCuve.getCuvePlotList();
            List<CuvePlot> cuvePlotListNew = cuve.getCuvePlotList();
            List<CuvePlot> attachedCuvePlotListNew = new ArrayList<CuvePlot>();
            for (CuvePlot cuvePlotListNewCuvePlotToAttach : cuvePlotListNew) {
                cuvePlotListNewCuvePlotToAttach = em.getReference(cuvePlotListNewCuvePlotToAttach.getClass(), cuvePlotListNewCuvePlotToAttach.getRefCuveplot());
                attachedCuvePlotListNew.add(cuvePlotListNewCuvePlotToAttach);
            }
            cuvePlotListNew = attachedCuvePlotListNew;
            cuve.setCuvePlotList(cuvePlotListNew);
            cuve = em.merge(cuve);
            for (CuvePlot cuvePlotListOldCuvePlot : cuvePlotListOld) {
                if (!cuvePlotListNew.contains(cuvePlotListOldCuvePlot)) {
                    cuvePlotListOldCuvePlot.setRefCuve(null);
                    cuvePlotListOldCuvePlot = em.merge(cuvePlotListOldCuvePlot);
                }
            }
            for (CuvePlot cuvePlotListNewCuvePlot : cuvePlotListNew) {
                if (!cuvePlotListOld.contains(cuvePlotListNewCuvePlot)) {
                    Cuve oldRefCuveOfCuvePlotListNewCuvePlot = cuvePlotListNewCuvePlot.getRefCuve();
                    cuvePlotListNewCuvePlot.setRefCuve(cuve);
                    cuvePlotListNewCuvePlot = em.merge(cuvePlotListNewCuvePlot);
                    if (oldRefCuveOfCuvePlotListNewCuvePlot != null && !oldRefCuveOfCuvePlotListNewCuvePlot.equals(cuve)) {
                        oldRefCuveOfCuvePlotListNewCuvePlot.getCuvePlotList().remove(cuvePlotListNewCuvePlot);
                        oldRefCuveOfCuvePlotListNewCuvePlot = em.merge(oldRefCuveOfCuvePlotListNewCuvePlot);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = cuve.getRefCuve();
                if (findCuve(id) == null) {
                    throw new NonexistentEntityException("The cuve with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cuve cuve;
            try {
                cuve = em.getReference(Cuve.class, id);
                cuve.getRefCuve();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cuve with id " + id + " no longer exists.", enfe);
            }
            List<CuvePlot> cuvePlotList = cuve.getCuvePlotList();
            for (CuvePlot cuvePlotListCuvePlot : cuvePlotList) {
                cuvePlotListCuvePlot.setRefCuve(null);
                cuvePlotListCuvePlot = em.merge(cuvePlotListCuvePlot);
            }
            em.remove(cuve);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Cuve> findCuveEntities() {
        return findCuveEntities(true, -1, -1);
    }

    public List<Cuve> findCuveEntities(int maxResults, int firstResult) {
        return findCuveEntities(false, maxResults, firstResult);
    }

    private List<Cuve> findCuveEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Cuve.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Cuve findCuve(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Cuve.class, id);
        } finally {
            em.close();
        }
    }

    public int getCuveCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Cuve> rt = cq.from(Cuve.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List getCuveVide() {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("FROM Cuve cuv WHERE cuv.etat = :etat");
            q.setParameter("etat", "VIDE");
            return q.getResultList();
        }finally{
            em.close();
        }
    }
    
    public List getCuvePleine() {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("FROM Cuve cuv WHERE cuv.etat = :etat");
            q.setParameter("etat", "PLEINE");
            return q.getResultList();
        }finally{
            em.close();
        }
    }

    
}
