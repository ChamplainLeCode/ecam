/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.models.LettreCamion;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Reception;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class LettreCamionJpaController implements Serializable {

    public LettreCamionJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(LettreCamion lettreCamion) throws PreexistingEntityException, Exception {
        if (lettreCamion.getReceptionList() == null) {
            lettreCamion.setReceptionList(new ArrayList<Reception>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Reception> attachedReceptionList = new ArrayList<Reception>();
            for (Reception receptionListReceptionToAttach : lettreCamion.getReceptionList()) {
                receptionListReceptionToAttach = em.getReference(receptionListReceptionToAttach.getClass(), receptionListReceptionToAttach.getRefReception());
                attachedReceptionList.add(receptionListReceptionToAttach);
            }
            lettreCamion.setReceptionList(attachedReceptionList);
            em.persist(lettreCamion);
            for (Reception receptionListReception : lettreCamion.getReceptionList()) {
                LettreCamion oldRefLettreOfReceptionListReception = receptionListReception.getRefLettre();
                receptionListReception.setRefLettre(lettreCamion);
                receptionListReception = em.merge(receptionListReception);
                if (oldRefLettreOfReceptionListReception != null) {
                    oldRefLettreOfReceptionListReception.getReceptionList().remove(receptionListReception);
                    oldRefLettreOfReceptionListReception = em.merge(oldRefLettreOfReceptionListReception);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findLettreCamion(lettreCamion.getRefLettre()) != null) {
                throw new PreexistingEntityException("LettreCamion " + lettreCamion + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(LettreCamion lettreCamion) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            LettreCamion persistentLettreCamion = em.find(LettreCamion.class, lettreCamion.getRefLettre());
            List<Reception> receptionListOld = persistentLettreCamion.getReceptionList();
            List<Reception> receptionListNew = lettreCamion.getReceptionList();
            List<Reception> attachedReceptionListNew = new ArrayList<Reception>();
            for (Reception receptionListNewReceptionToAttach : receptionListNew) {
                receptionListNewReceptionToAttach = em.getReference(receptionListNewReceptionToAttach.getClass(), receptionListNewReceptionToAttach.getRefReception());
                attachedReceptionListNew.add(receptionListNewReceptionToAttach);
            }
            receptionListNew = attachedReceptionListNew;
            lettreCamion.setReceptionList(receptionListNew);
            lettreCamion = em.merge(lettreCamion);
            for (Reception receptionListOldReception : receptionListOld) {
                if (!receptionListNew.contains(receptionListOldReception)) {
                    receptionListOldReception.setRefLettre(null);
                    receptionListOldReception = em.merge(receptionListOldReception);
                }
            }
            for (Reception receptionListNewReception : receptionListNew) {
                if (!receptionListOld.contains(receptionListNewReception)) {
                    LettreCamion oldRefLettreOfReceptionListNewReception = receptionListNewReception.getRefLettre();
                    receptionListNewReception.setRefLettre(lettreCamion);
                    receptionListNewReception = em.merge(receptionListNewReception);
                    if (oldRefLettreOfReceptionListNewReception != null && !oldRefLettreOfReceptionListNewReception.equals(lettreCamion)) {
                        oldRefLettreOfReceptionListNewReception.getReceptionList().remove(receptionListNewReception);
                        oldRefLettreOfReceptionListNewReception = em.merge(oldRefLettreOfReceptionListNewReception);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = lettreCamion.getRefLettre();
                if (findLettreCamion(id) == null) {
                    throw new NonexistentEntityException("The lettreCamion with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            LettreCamion lettreCamion;
            try {
                lettreCamion = em.getReference(LettreCamion.class, id);
                lettreCamion.getRefLettre();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The lettreCamion with id " + id + " no longer exists.", enfe);
            }
            List<Reception> receptionList = lettreCamion.getReceptionList();
            for (Reception receptionListReception : receptionList) {
                receptionListReception.setRefLettre(null);
                receptionListReception = em.merge(receptionListReception);
            }
            em.remove(lettreCamion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<LettreCamion> findLettreCamionEntities() {
        return findLettreCamionEntities(true, -1, -1);
    }

    public List<LettreCamion> findLettreCamionEntities(int maxResults, int firstResult) {
        return findLettreCamionEntities(false, maxResults, firstResult);
    }

    private List<LettreCamion> findLettreCamionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(LettreCamion.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public LettreCamion findLettreCamion(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(LettreCamion.class, id);
        } finally {
            em.close();
        }
    }

    public int getLettreCamionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<LettreCamion> rt = cq.from(LettreCamion.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
