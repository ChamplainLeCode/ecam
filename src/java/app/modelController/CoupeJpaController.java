/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Billon;
import app.models.Coupe;
import app.models.TypeCoupe;
import app.models.Plot;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class CoupeJpaController implements Serializable {

    public static List billeForCoupe(EntityManager em) {
        try{
            Query q = em.createQuery("select bil.numTravail from Billonnage bil");
            return q.getResultList();
        }finally{
            em.close();
        }
    }

    public CoupeJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Coupe coupe) throws PreexistingEntityException, Exception {
        if (coupe.getPlotList() == null) {
            coupe.setPlotList(new ArrayList<Plot>());
        }
        if (coupe.getBillonList() == null) {
            coupe.setBillonList(new ArrayList<Billon>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Billon refBillon = coupe.getRefBillon();
            if (refBillon != null) {
                refBillon = em.getReference(refBillon.getClass(), refBillon.getRefBillon());
                coupe.setRefBillon(refBillon);
            }
            TypeCoupe refTypecoupe = coupe.getRefTypecoupe();
            if (refTypecoupe != null) {
                refTypecoupe = em.getReference(refTypecoupe.getClass(), refTypecoupe.getRefTypecoupe());
                coupe.setRefTypecoupe(refTypecoupe);
            }
            List<Plot> attachedPlotList = new ArrayList<Plot>();
            for (Plot plotListPlotToAttach : coupe.getPlotList()) {
                plotListPlotToAttach = em.getReference(plotListPlotToAttach.getClass(), plotListPlotToAttach.getRefPlot());
                attachedPlotList.add(plotListPlotToAttach);
            }
            coupe.setPlotList(attachedPlotList);
            List<Billon> attachedBillonList = new ArrayList<Billon>();
            for (Billon billonListBillonToAttach : coupe.getBillonList()) {
                billonListBillonToAttach = em.getReference(billonListBillonToAttach.getClass(), billonListBillonToAttach.getRefBillon());
                attachedBillonList.add(billonListBillonToAttach);
            }
            coupe.setBillonList(attachedBillonList);
            em.persist(coupe);
            if (refBillon != null) {
                refBillon.getCoupeList().add(coupe);
                refBillon = em.merge(refBillon);
            }
            if (refTypecoupe != null) {
                refTypecoupe.getCoupeList().add(coupe);
                refTypecoupe = em.merge(refTypecoupe);
            }
            for (Plot plotListPlot : coupe.getPlotList()) {
                Coupe oldRefCoupeOfPlotListPlot = plotListPlot.getRefCoupe();
                plotListPlot.setRefCoupe(coupe);
                plotListPlot = em.merge(plotListPlot);
                if (oldRefCoupeOfPlotListPlot != null) {
                    oldRefCoupeOfPlotListPlot.getPlotList().remove(plotListPlot);
                    oldRefCoupeOfPlotListPlot = em.merge(oldRefCoupeOfPlotListPlot);
                }
            }
            for (Billon billonListBillon : coupe.getBillonList()) {
                Coupe oldCoupeOfBillonListBillon = billonListBillon.getCoupe();
                billonListBillon.setCoupe(coupe);
                billonListBillon = em.merge(billonListBillon);
                if (oldCoupeOfBillonListBillon != null) {
                    oldCoupeOfBillonListBillon.getBillonList().remove(billonListBillon);
                    oldCoupeOfBillonListBillon = em.merge(oldCoupeOfBillonListBillon);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCoupe(coupe.getRefCoupe()) != null) {
                throw new PreexistingEntityException("Coupe " + coupe + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Coupe coupe) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Coupe persistentCoupe = em.find(Coupe.class, coupe.getRefCoupe());
            Billon refBillonOld = persistentCoupe.getRefBillon();
            Billon refBillonNew = coupe.getRefBillon();
            TypeCoupe refTypecoupeOld = persistentCoupe.getRefTypecoupe();
            TypeCoupe refTypecoupeNew = coupe.getRefTypecoupe();
            List<Plot> plotListOld = persistentCoupe.getPlotList();
            List<Plot> plotListNew = coupe.getPlotList();
            List<Billon> billonListOld = persistentCoupe.getBillonList();
            List<Billon> billonListNew = coupe.getBillonList();
            if (refBillonNew != null) {
                refBillonNew = em.getReference(refBillonNew.getClass(), refBillonNew.getRefBillon());
                coupe.setRefBillon(refBillonNew);
            }
            if (refTypecoupeNew != null) {
                refTypecoupeNew = em.getReference(refTypecoupeNew.getClass(), refTypecoupeNew.getRefTypecoupe());
                coupe.setRefTypecoupe(refTypecoupeNew);
            }
            List<Plot> attachedPlotListNew = new ArrayList<Plot>();
            for (Plot plotListNewPlotToAttach : plotListNew) {
                plotListNewPlotToAttach = em.getReference(plotListNewPlotToAttach.getClass(), plotListNewPlotToAttach.getRefPlot());
                attachedPlotListNew.add(plotListNewPlotToAttach);
            }
            plotListNew = attachedPlotListNew;
            coupe.setPlotList(plotListNew);
            List<Billon> attachedBillonListNew = new ArrayList<Billon>();
            for (Billon billonListNewBillonToAttach : billonListNew) {
                billonListNewBillonToAttach = em.getReference(billonListNewBillonToAttach.getClass(), billonListNewBillonToAttach.getRefBillon());
                attachedBillonListNew.add(billonListNewBillonToAttach);
            }
            billonListNew = attachedBillonListNew;
            coupe.setBillonList(billonListNew);
            coupe = em.merge(coupe);
            if (refBillonOld != null && !refBillonOld.equals(refBillonNew)) {
                refBillonOld.getCoupeList().remove(coupe);
                refBillonOld = em.merge(refBillonOld);
            }
            if (refBillonNew != null && !refBillonNew.equals(refBillonOld)) {
                refBillonNew.getCoupeList().add(coupe);
                refBillonNew = em.merge(refBillonNew);
            }
            if (refTypecoupeOld != null && !refTypecoupeOld.equals(refTypecoupeNew)) {
                refTypecoupeOld.getCoupeList().remove(coupe);
                refTypecoupeOld = em.merge(refTypecoupeOld);
            }
            if (refTypecoupeNew != null && !refTypecoupeNew.equals(refTypecoupeOld)) {
                refTypecoupeNew.getCoupeList().add(coupe);
                refTypecoupeNew = em.merge(refTypecoupeNew);
            }
            for (Plot plotListOldPlot : plotListOld) {
                if (!plotListNew.contains(plotListOldPlot)) {
                    plotListOldPlot.setRefCoupe(null);
                    plotListOldPlot = em.merge(plotListOldPlot);
                }
            }
            for (Plot plotListNewPlot : plotListNew) {
                if (!plotListOld.contains(plotListNewPlot)) {
                    Coupe oldRefCoupeOfPlotListNewPlot = plotListNewPlot.getRefCoupe();
                    plotListNewPlot.setRefCoupe(coupe);
                    plotListNewPlot = em.merge(plotListNewPlot);
                    if (oldRefCoupeOfPlotListNewPlot != null && !oldRefCoupeOfPlotListNewPlot.equals(coupe)) {
                        oldRefCoupeOfPlotListNewPlot.getPlotList().remove(plotListNewPlot);
                        oldRefCoupeOfPlotListNewPlot = em.merge(oldRefCoupeOfPlotListNewPlot);
                    }
                }
            }
            for (Billon billonListOldBillon : billonListOld) {
                if (!billonListNew.contains(billonListOldBillon)) {
                    billonListOldBillon.setCoupe(null);
                    billonListOldBillon = em.merge(billonListOldBillon);
                }
            }
            for (Billon billonListNewBillon : billonListNew) {
                if (!billonListOld.contains(billonListNewBillon)) {
                    Coupe oldCoupeOfBillonListNewBillon = billonListNewBillon.getCoupe();
                    billonListNewBillon.setCoupe(coupe);
                    billonListNewBillon = em.merge(billonListNewBillon);
                    if (oldCoupeOfBillonListNewBillon != null && !oldCoupeOfBillonListNewBillon.equals(coupe)) {
                        oldCoupeOfBillonListNewBillon.getBillonList().remove(billonListNewBillon);
                        oldCoupeOfBillonListNewBillon = em.merge(oldCoupeOfBillonListNewBillon);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = coupe.getRefCoupe();
                if (findCoupe(id) == null) {
                    throw new NonexistentEntityException("The coupe with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Coupe coupe;
            try {
                coupe = em.getReference(Coupe.class, id);
                coupe.getRefCoupe();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The coupe with id " + id + " no longer exists.", enfe);
            }
            Billon refBillon = coupe.getRefBillon();
            if (refBillon != null) {
                refBillon.getCoupeList().remove(coupe);
                refBillon = em.merge(refBillon);
            }
            TypeCoupe refTypecoupe = coupe.getRefTypecoupe();
            if (refTypecoupe != null) {
                refTypecoupe.getCoupeList().remove(coupe);
                refTypecoupe = em.merge(refTypecoupe);
            }
            List<Plot> plotList = coupe.getPlotList();
            for (Plot plotListPlot : plotList) {
                plotListPlot.setRefCoupe(null);
                plotListPlot = em.merge(plotListPlot);
            }
            List<Billon> billonList = coupe.getBillonList();
            for (Billon billonListBillon : billonList) {
                billonListBillon.setCoupe(null);
                billonListBillon = em.merge(billonListBillon);
            }
            em.remove(coupe);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Coupe> findCoupeEntities() {
        return findCoupeEntities(true, -1, -1);
    }

    public List<Coupe> findCoupeEntities(int maxResults, int firstResult) {
        return findCoupeEntities(false, maxResults, firstResult);
    }

    private List<Coupe> findCoupeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Coupe.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Coupe findCoupe(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Coupe.class, id);
        } finally {
            em.close();
        }
    }

    public int getCoupeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Coupe> rt = cq.from(Coupe.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public Coupe findCoupeByBillon(String refBillon) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("Select coup from Coupe coup where coup.refBillon.refBillon = :refBillon", Coupe.class);
            q.setParameter("refBillon", refBillon);
            List l = q.getResultList();
            return l != null && l.size() > 0 ? (Coupe)l.get(0) : null;
        }finally{
            em.close();
        }
    }
    
}
