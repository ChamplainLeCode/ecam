/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.models.Classification;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author champlain
 */
public class ClassificationJpaController implements Serializable {

    public ClassificationJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Classification classification) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(classification);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findClassification(classification.getRefClassification()) != null) {
                throw new PreexistingEntityException("Classification " + classification + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Classification classification) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            classification = em.merge(classification);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = classification.getRefClassification();
                if (findClassification(id) == null) {
                    throw new NonexistentEntityException("The classification with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Classification classification;
            try {
                classification = em.getReference(Classification.class, id);
                classification.getRefClassification();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The classification with id " + id + " no longer exists.", enfe);
            }
            em.remove(classification);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Classification> findClassificationEntities() {
        return findClassificationEntities(true, -1, -1);
    }

    public List<Classification> findClassificationEntities(int maxResults, int firstResult) {
        return findClassificationEntities(false, maxResults, firstResult);
    }

    private List<Classification> findClassificationEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Classification.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Classification findClassification(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Classification.class, id);
        } finally {
            em.close();
        }
    }

    public int getClassificationCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Classification> rt = cq.from(Classification.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
