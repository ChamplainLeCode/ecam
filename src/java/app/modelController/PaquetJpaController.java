/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.modelController.exceptions.IllegalOrphanException;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Plot;
import app.models.Colis;
import app.models.Database;
import app.models.Massicotpaquet;
import app.models.Paquet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class PaquetJpaController implements Serializable {

    public static HashMap<String, Double> getVolumeFromPaquetByEssenceAndDate(String libelleEssence, String dateDebut, String dateFin) {
        Date dd = new Date(Long.parseLong(dateDebut));
        Date df = new Date(Long.parseLong(dateFin));
        
        EntityManager em = Database.getEntityManager().createEntityManager();
        
        try{
            Query q = em.createQuery("FROM Paquet p where p.create_at between :d1 and :d2 and p.refColis.essence = :ess", Paquet.class);
            q.setParameter("d1", dd);
            q.setParameter("d2", df);
            q.setParameter("ess", libelleEssence);
            double volume = 0;
            double surface = 0;
            List<Paquet> le = q.getResultList();
            for (Paquet e : le) {
                volume += Double.parseDouble(new JSONObject(e.toString()).getString("volume")); // c'est en cm³ donc à la fin il convertir en m³
                surface += Double.parseDouble(new JSONObject(e.toString()).getString("surface")); // c'est en cm³ donc à la fin il convertir en m³
                //e.toString();
                //volume = e..stream().filter((c) -> (c.getEssence().equalsIgnoreCase(essence))).map((c) -> c.getVoulumeColis()).reduce(volume, (accumulator, _item) -> accumulator + _item);
            }
            HashMap<String, Double> m = new HashMap();
            m.put("volume", volume);
            m.put("surface", surface);
            return m;
        }catch(Exception e){
            HashMap<String, Double> m = new HashMap();
            m.put("volume", 0D);
            m.put("surface", 0D);
            return m;
        }finally{
            em.close();
        }
    }

    public PaquetJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Paquet paquet) throws PreexistingEntityException, Exception {
        if (paquet.getMassicotpaquetList() == null) {
            paquet.setMassicotpaquetList(new ArrayList<Massicotpaquet>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Plot refPlot = paquet.getRefPlot();
            if (refPlot != null) {
                refPlot = em.getReference(refPlot.getClass(), refPlot.getRefPlot());
                paquet.setRefPlot(refPlot);
            }
            Colis refColis = paquet.getRefColis();
            if (refColis != null) {
                refColis = em.getReference(refColis.getClass(), refColis.getRefColis());
                paquet.setRefColis(refColis);
            }
            em.persist(paquet);
            List<Massicotpaquet> attachedMassicotpaquetList = new ArrayList<Massicotpaquet>();
            for (Massicotpaquet massicotpaquetListMassicotpaquetToAttach : paquet.getMassicotpaquetList()) {
                massicotpaquetListMassicotpaquetToAttach = em.getReference(massicotpaquetListMassicotpaquetToAttach.getClass(), massicotpaquetListMassicotpaquetToAttach.getRefMassicotpaquet());
                attachedMassicotpaquetList.add(massicotpaquetListMassicotpaquetToAttach);
            }
            paquet.setMassicotpaquetList(attachedMassicotpaquetList);
            em.persist(paquet);
            if (refPlot != null) {
                refPlot.getPaquetList().add(paquet);
                refPlot = em.merge(refPlot);
            }
            if (refColis != null) {
                refColis.getPaquetList().add(paquet);
                refColis = em.merge(refColis);
            }
            for (Massicotpaquet massicotpaquetListMassicotpaquet : paquet.getMassicotpaquetList()) {
                Paquet oldRefPaquetOfMassicotpaquetListMassicotpaquet = massicotpaquetListMassicotpaquet.getRefPaquet();
                massicotpaquetListMassicotpaquet.setRefPaquet(paquet);
                massicotpaquetListMassicotpaquet = em.merge(massicotpaquetListMassicotpaquet);
                if (oldRefPaquetOfMassicotpaquetListMassicotpaquet != null) {
                    oldRefPaquetOfMassicotpaquetListMassicotpaquet.getMassicotpaquetList().remove(massicotpaquetListMassicotpaquet);
                    oldRefPaquetOfMassicotpaquetListMassicotpaquet = em.merge(oldRefPaquetOfMassicotpaquetListMassicotpaquet);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPaquet(paquet.getRefPaquet()) != null) {
                throw new PreexistingEntityException("Paquet " + paquet + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Paquet paquet) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Paquet persistentPaquet = em.find(Paquet.class, paquet.getRefPaquet());
            Plot refPlotOld = persistentPaquet.getRefPlot();
            Plot refPlotNew = paquet.getRefPlot();
            Colis refColisOld = persistentPaquet.getRefColis();
            Colis refColisNew = paquet.getRefColis();
            List<Massicotpaquet> massicotpaquetListOld = persistentPaquet.getMassicotpaquetList();
            List<Massicotpaquet> massicotpaquetListNew = paquet.getMassicotpaquetList();
            List<String> illegalOrphanMessages = null;
            for (Massicotpaquet massicotpaquetListOldMassicotpaquet : massicotpaquetListOld) {
                if (!massicotpaquetListNew.contains(massicotpaquetListOldMassicotpaquet)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Massicotpaquet " + massicotpaquetListOldMassicotpaquet + " since its refPaquet field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (refPlotNew != null) {
                refPlotNew = em.getReference(refPlotNew.getClass(), refPlotNew.getRefPlot());
                paquet.setRefPlot(refPlotNew);
            }
            if (refColisNew != null) {
                refColisNew = em.getReference(refColisNew.getClass(), refColisNew.getRefColis());
                paquet.setRefColis(refColisNew);
            }
            List<Massicotpaquet> attachedMassicotpaquetListNew = new ArrayList<Massicotpaquet>();
            for (Massicotpaquet massicotpaquetListNewMassicotpaquetToAttach : massicotpaquetListNew) {
                massicotpaquetListNewMassicotpaquetToAttach = em.getReference(massicotpaquetListNewMassicotpaquetToAttach.getClass(), massicotpaquetListNewMassicotpaquetToAttach.getRefMassicotpaquet());
                attachedMassicotpaquetListNew.add(massicotpaquetListNewMassicotpaquetToAttach);
            }
            massicotpaquetListNew = attachedMassicotpaquetListNew;
            paquet.setMassicotpaquetList(massicotpaquetListNew);
            paquet = em.merge(paquet);
            if (refPlotOld != null && !refPlotOld.equals(refPlotNew)) {
                refPlotOld.getPaquetList().remove(paquet);
                refPlotOld = em.merge(refPlotOld);
            }
            if (refPlotNew != null && !refPlotNew.equals(refPlotOld)) {
                refPlotNew.getPaquetList().add(paquet);
                refPlotNew = em.merge(refPlotNew);
            }
            if (refColisOld != null && !refColisOld.equals(refColisNew)) {
                refColisOld.getPaquetList().remove(paquet);
                refColisOld = em.merge(refColisOld);
            }
            if (refColisNew != null && !refColisNew.equals(refColisOld)) {
                refColisNew.getPaquetList().add(paquet);
                refColisNew = em.merge(refColisNew);
            }
            for (Massicotpaquet massicotpaquetListNewMassicotpaquet : massicotpaquetListNew) {
                if (!massicotpaquetListOld.contains(massicotpaquetListNewMassicotpaquet)) {
                    Paquet oldRefPaquetOfMassicotpaquetListNewMassicotpaquet = massicotpaquetListNewMassicotpaquet.getRefPaquet();
                    massicotpaquetListNewMassicotpaquet.setRefPaquet(paquet);
                    massicotpaquetListNewMassicotpaquet = em.merge(massicotpaquetListNewMassicotpaquet);
                    if (oldRefPaquetOfMassicotpaquetListNewMassicotpaquet != null && !oldRefPaquetOfMassicotpaquetListNewMassicotpaquet.equals(paquet)) {
                        oldRefPaquetOfMassicotpaquetListNewMassicotpaquet.getMassicotpaquetList().remove(massicotpaquetListNewMassicotpaquet);
                        oldRefPaquetOfMassicotpaquetListNewMassicotpaquet = em.merge(oldRefPaquetOfMassicotpaquetListNewMassicotpaquet);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = paquet.getRefPaquet();
                if (findPaquet(id) == null) {
                    throw new NonexistentEntityException("The paquet with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Paquet paquet;
            try {
                paquet = em.getReference(Paquet.class, id);
                paquet.getRefPaquet();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The paquet with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Massicotpaquet> massicotpaquetListOrphanCheck = paquet.getMassicotpaquetList();
            for (Massicotpaquet massicotpaquetListOrphanCheckMassicotpaquet : massicotpaquetListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Paquet (" + paquet + ") cannot be destroyed since the Massicotpaquet " + massicotpaquetListOrphanCheckMassicotpaquet + " in its massicotpaquetList field has a non-nullable refPaquet field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Plot refPlot = paquet.getRefPlot();
            if (refPlot != null) {
                refPlot.getPaquetList().remove(paquet);
                refPlot = em.merge(refPlot);
            }
            Colis refColis = paquet.getRefColis();
            if (refColis != null) {
                refColis.getPaquetList().remove(paquet);
                refColis = em.merge(refColis);
            }
            em.remove(paquet);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Paquet> findPaquetEntities() {
        return findPaquetEntities(true, -1, -1);
    }

    public List<Paquet> findPaquetEntities(int maxResults, int firstResult) {
        return findPaquetEntities(false, maxResults, firstResult);
    }

    private List<Paquet> findPaquetEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Paquet.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Paquet findPaquet(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Paquet.class, id);
        } finally {
            em.close();
        }
    }

    public int getPaquetCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Paquet> rt = cq.from(Paquet.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    String findNumTravailFromPaquetforColis(String refColis) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("SELECT distinct pt.numTravail FROM Paquet pt where pt.refColis.refColis = :refColis OR pt.refCodebar like :code");
            q.setParameter("refColis", refColis);
            q.setParameter("code", refColis+"%");
            List l = q.getResultList();
            return l == null || l.isEmpty() ? null : l.get(0).toString();
        }finally{
            em.close();
        }
    }

    public Double countFeuilleByColis(String refColis) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("SELECT count(pt.nbreFeuille) FROM Paquet pt where pt.refColis.refColis = :refColis OR pt.refCodebar like :code");
            q.setParameter("refColis", refColis);
            q.setParameter("code", refColis+"%");
            List l = q.getResultList();
            return l == null || l.isEmpty() ? null : Double.parseDouble(l.get(0).toString());
        }finally{
            em.close();
        }
    }
    

    public static String CalculVolumeForBilleByNumTravail(String numTravail) {
        EntityManager em = Database.getEntityManager().createEntityManager();
        try{
            Query q = em.createQuery("SELECT sum(pt.largeur*pt.longueur) AS surface FROM Paquet pt where pt.numTravail = :travail");
            q.setParameter("travail", numTravail);
//            List<Object[]> l = q.getResultList();
//            l.forEach((e)->{System.out.println("suf = "+e[0]+" lar = "+e[1]+" long = "+e[2]);});

            return q.getSingleResult().toString();//ResultList().toString();
        }finally{
            em.close();
        }
    }
    

    public static String getQualiteForBilleByNumTravail(String numTravail) {
        EntityManager em = Database.getEntityManager().createEntityManager();
        try{
            Query q = em.createQuery("SELECT sum(pt.longueur*pt.largeur) AS surface, pt.qualite FROM Paquet pt where pt.numTravail = :travail");
            q.setParameter("travail", numTravail);
//            List<Object[]> l = q.getResultList();
//            l.forEach((e)->{System.out.println("suf = "+e[0]+" lar = "+e[1]+" long = "+e[2]);});

            return q.getSingleResult().toString();//ResultList().toString();
        }finally{
            em.close();
        }
    }

    public Paquet getPaquetInColis(Colis colis) {
        EntityManager em = Database.getEntityManager().createEntityManager();
        try{
            Query q = em.createQuery("SELECT pt FROM Paquet pt where pt.refColis.refColis = :colis");
            q.setParameter("colis", colis.getRefColis());
//            List<Object[]> l = q.getResultList();
//            l.forEach((e)->{System.out.println("suf = "+e[0]+" lar = "+e[1]+" long = "+e[2]);});
            List<Paquet> l = q.getResultList();
            return l.size() >0 ? l.get(0) : null;
        }finally{
            em.close();
        }
    }

    List<Paquet> getPaquetInColisByNbrFeuille(String refColis, int nbrFeuille) {
        EntityManager em = Database.getEntityManager().createEntityManager();
        System.out.println("recherche paquets colis "+refColis+" nbrFeuille = "+nbrFeuille);
        try{
            Query q = em.createQuery("SELECT pt FROM Paquet pt where pt.refColis.refColis = :colis and pt.nbreFeuille = :nbr");
            q.setParameter("colis", refColis);
            q.setParameter("nbr", nbrFeuille);
            List liste = q.getResultList();
            System.out.println("--------> liste taille = "+liste.size());
            return liste;
        }finally{
            em.close();
        }
    }
    
}
