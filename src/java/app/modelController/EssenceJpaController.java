/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.ClassificationEss;
import java.util.ArrayList;
import java.util.List;
import app.models.DetailCommande;
import app.models.Essence;
import app.modelController.exceptions.IllegalOrphanException;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.DetailReception;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class EssenceJpaController implements Serializable {

    public EssenceJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Essence essence) throws PreexistingEntityException, Exception {
        if (essence.getClassificationEssList() == null) {
            essence.setClassificationEssList(new ArrayList<ClassificationEss>());
        }
        if (essence.getDetailCommandeList() == null) {
            essence.setDetailCommandeList(new ArrayList<DetailCommande>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<ClassificationEss> attachedClassificationEssList = new ArrayList<ClassificationEss>();
            for (ClassificationEss classificationEssListClassificationEssToAttach : essence.getClassificationEssList()) {
                classificationEssListClassificationEssToAttach = em.getReference(classificationEssListClassificationEssToAttach.getClass(), classificationEssListClassificationEssToAttach.getRefClassification());
                attachedClassificationEssList.add(classificationEssListClassificationEssToAttach);
            }
            essence.setClassificationEssList(attachedClassificationEssList);
            List<DetailCommande> attachedDetailCommandeList = new ArrayList<DetailCommande>();
            for (DetailCommande detailCommandeListDetailCommandeToAttach : essence.getDetailCommandeList()) {
                detailCommandeListDetailCommandeToAttach = em.getReference(detailCommandeListDetailCommandeToAttach.getClass(), detailCommandeListDetailCommandeToAttach.getRefDetail());
                attachedDetailCommandeList.add(detailCommandeListDetailCommandeToAttach);
            }
            essence.setDetailCommandeList(attachedDetailCommandeList);
            em.persist(essence);
            for (ClassificationEss classificationEssListClassificationEss : essence.getClassificationEssList()) {
                Essence oldRefEssenceOfClassificationEssListClassificationEss = classificationEssListClassificationEss.getRefEssence();
                classificationEssListClassificationEss.setRefEssence(essence);
                classificationEssListClassificationEss = em.merge(classificationEssListClassificationEss);
                if (oldRefEssenceOfClassificationEssListClassificationEss != null) {
                    oldRefEssenceOfClassificationEssListClassificationEss.getClassificationEssList().remove(classificationEssListClassificationEss);
                    oldRefEssenceOfClassificationEssListClassificationEss = em.merge(oldRefEssenceOfClassificationEssListClassificationEss);
                }
            }
            for (DetailCommande detailCommandeListDetailCommande : essence.getDetailCommandeList()) {
                Essence oldRefEssenceOfDetailCommandeListDetailCommande = detailCommandeListDetailCommande.getRefEssence();
                detailCommandeListDetailCommande.setRefEssence(essence);
                detailCommandeListDetailCommande = em.merge(detailCommandeListDetailCommande);
                if (oldRefEssenceOfDetailCommandeListDetailCommande != null) {
                    oldRefEssenceOfDetailCommandeListDetailCommande.getDetailCommandeList().remove(detailCommandeListDetailCommande);
                    oldRefEssenceOfDetailCommandeListDetailCommande = em.merge(oldRefEssenceOfDetailCommandeListDetailCommande);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findEssence(essence.getRefEssence()) != null) {
                throw new PreexistingEntityException("Essence " + essence + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Essence essence) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Essence persistentEssence = em.find(Essence.class, essence.getRefEssence());
            List<ClassificationEss> classificationEssListOld = persistentEssence.getClassificationEssList();
            List<ClassificationEss> classificationEssListNew = essence.getClassificationEssList();
            List<DetailCommande> detailCommandeListOld = persistentEssence.getDetailCommandeList();
            List<DetailCommande> detailCommandeListNew = essence.getDetailCommandeList();
            List<String> illegalOrphanMessages = null;
            for (DetailCommande detailCommandeListOldDetailCommande : detailCommandeListOld) {
                if (!detailCommandeListNew.contains(detailCommandeListOldDetailCommande)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain DetailCommande " + detailCommandeListOldDetailCommande + " since its refEssence field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<ClassificationEss> attachedClassificationEssListNew = new ArrayList<ClassificationEss>();
            for (ClassificationEss classificationEssListNewClassificationEssToAttach : classificationEssListNew) {
                classificationEssListNewClassificationEssToAttach = em.getReference(classificationEssListNewClassificationEssToAttach.getClass(), classificationEssListNewClassificationEssToAttach.getRefClassification());
                attachedClassificationEssListNew.add(classificationEssListNewClassificationEssToAttach);
            }
            classificationEssListNew = attachedClassificationEssListNew;
            essence.setClassificationEssList(classificationEssListNew);
            List<DetailCommande> attachedDetailCommandeListNew = new ArrayList<DetailCommande>();
            for (DetailCommande detailCommandeListNewDetailCommandeToAttach : detailCommandeListNew) {
                detailCommandeListNewDetailCommandeToAttach = em.getReference(detailCommandeListNewDetailCommandeToAttach.getClass(), detailCommandeListNewDetailCommandeToAttach.getRefDetail());
                attachedDetailCommandeListNew.add(detailCommandeListNewDetailCommandeToAttach);
            }
            detailCommandeListNew = attachedDetailCommandeListNew;
            essence.setDetailCommandeList(detailCommandeListNew);
            essence = em.merge(essence);
            for (ClassificationEss classificationEssListOldClassificationEss : classificationEssListOld) {
                if (!classificationEssListNew.contains(classificationEssListOldClassificationEss)) {
                    classificationEssListOldClassificationEss.setRefEssence(null);
                    classificationEssListOldClassificationEss = em.merge(classificationEssListOldClassificationEss);
                }
            }
            for (ClassificationEss classificationEssListNewClassificationEss : classificationEssListNew) {
                if (!classificationEssListOld.contains(classificationEssListNewClassificationEss)) {
                    Essence oldRefEssenceOfClassificationEssListNewClassificationEss = classificationEssListNewClassificationEss.getRefEssence();
                    classificationEssListNewClassificationEss.setRefEssence(essence);
                    classificationEssListNewClassificationEss = em.merge(classificationEssListNewClassificationEss);
                    if (oldRefEssenceOfClassificationEssListNewClassificationEss != null && !oldRefEssenceOfClassificationEssListNewClassificationEss.equals(essence)) {
                        oldRefEssenceOfClassificationEssListNewClassificationEss.getClassificationEssList().remove(classificationEssListNewClassificationEss);
                        oldRefEssenceOfClassificationEssListNewClassificationEss = em.merge(oldRefEssenceOfClassificationEssListNewClassificationEss);
                    }
                }
            }
            for (DetailCommande detailCommandeListNewDetailCommande : detailCommandeListNew) {
                if (!detailCommandeListOld.contains(detailCommandeListNewDetailCommande)) {
                    Essence oldRefEssenceOfDetailCommandeListNewDetailCommande = detailCommandeListNewDetailCommande.getRefEssence();
                    detailCommandeListNewDetailCommande.setRefEssence(essence);
                    detailCommandeListNewDetailCommande = em.merge(detailCommandeListNewDetailCommande);
                    if (oldRefEssenceOfDetailCommandeListNewDetailCommande != null && !oldRefEssenceOfDetailCommandeListNewDetailCommande.equals(essence)) {
                        oldRefEssenceOfDetailCommandeListNewDetailCommande.getDetailCommandeList().remove(detailCommandeListNewDetailCommande);
                        oldRefEssenceOfDetailCommandeListNewDetailCommande = em.merge(oldRefEssenceOfDetailCommandeListNewDetailCommande);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = essence.getRefEssence();
                if (findEssence(id) == null) {
                    throw new NonexistentEntityException("The essence with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Essence essence;
            try {
                essence = em.getReference(Essence.class, id);
                essence.getRefEssence();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The essence with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<DetailCommande> detailCommandeListOrphanCheck = essence.getDetailCommandeList();
            for (DetailCommande detailCommandeListOrphanCheckDetailCommande : detailCommandeListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Essence (" + essence + ") cannot be destroyed since the DetailCommande " + detailCommandeListOrphanCheckDetailCommande + " in its detailCommandeList field has a non-nullable refEssence field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<ClassificationEss> classificationEssList = essence.getClassificationEssList();
            for (ClassificationEss classificationEssListClassificationEss : classificationEssList) {
                classificationEssListClassificationEss.setRefEssence(null);
                classificationEssListClassificationEss = em.merge(classificationEssListClassificationEss);
            }
            em.remove(essence);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Essence> findEssenceEntities() {
        return findEssenceEntities(true, -1, -1);
    }

    public List<Essence> findEssenceEntities(int maxResults, int firstResult) {
        return findEssenceEntities(false, maxResults, firstResult);
    }

    private List<Essence> findEssenceEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Essence.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Essence findEssence(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Essence.class, id);
        } finally {
            em.close();
        }
    }

    public int getEssenceCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Essence> rt = cq.from(Essence.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    public String getEssenceRefByName(String libelle){
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select e FROM Essence e where e.libelle = :libelle");
            q.setParameter("libelle", libelle);
            List<Essence> l = q.getResultList();
            return l.isEmpty() ? " " : l.get(0).getRefEssence();
        }finally{
            em.close();
        }
    }

    public Collection<Object> getEssenceByEpoach(String dateDebut, String dateFin) {
        Date dateD = new Date(Long.parseLong(dateDebut)), dateF = new Date(Long.parseLong(dateFin));
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("FROM DetailReception dr where dr.create_at between :d1 and :d2", DetailReceptionJpaController.class);
            q.setParameter("d1", dateD);
            q.setParameter("d2", dateF);
            List<DetailReception> ldr = q.getResultList();
            System.err.println(q.toString());
            HashMap<String, Object> map = new HashMap();
            ldr.forEach((DetailReception dr)->{
                try{
                    if(map.get(dr.getRefEssence().getRefEssence()) == null){
                        JSONObject o = new JSONObject();
                        JSONArray li = new JSONArray();
                            o.put("libelle", dr.getRefEssence().getLibelle());
                            o.put("code", dr.getRefEssence().getRefEssence());

                            li.put(dr.getNumBille());
                            o.put("numBille", li);                                


                            map.put(dr.getRefEssence().getRefEssence(), o);

                    }else{
                        JSONObject o = (JSONObject) map.get((dr.getRefEssence().getRefEssence()));
                        o.getJSONArray("numBille").put(dr.getNumBille());
                    }
                }catch(Exception e){

                }
            });
                return map.values();
        }catch(Exception e){
            e.printStackTrace();
            return new LinkedList();
        }finally{
            em.close();
        }
    }
}
