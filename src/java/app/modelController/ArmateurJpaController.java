/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.Armateur;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author champlain
 */
public class ArmateurJpaController implements Serializable {

    public ArmateurJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Armateur armateur) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(armateur);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findArmateur(armateur.getRefArmateur()) != null) {
                throw new PreexistingEntityException("Armateur " + armateur + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Armateur armateur) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            armateur = em.merge(armateur);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = armateur.getRefArmateur();
                if (findArmateur(id) == null) {
                    throw new NonexistentEntityException("The armateur with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Armateur armateur;
            try {
                armateur = em.getReference(Armateur.class, id);
                armateur.getRefArmateur();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The armateur with id " + id + " no longer exists.", enfe);
            }
            em.remove(armateur);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Armateur> findArmateurEntities() {
        return findArmateurEntities(true, -1, -1);
    }

    public List<Armateur> findArmateurEntities(int maxResults, int firstResult) {
        return findArmateurEntities(false, maxResults, firstResult);
    }

    private List<Armateur> findArmateurEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Armateur.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Armateur findArmateur(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Armateur.class, id);
        } finally {
            em.close();
        }
    }

    public int getArmateurCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Armateur> rt = cq.from(Armateur.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
