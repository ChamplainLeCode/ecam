/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Massicot;
import app.models.Massicotpaquet;
import app.models.Paquet;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.Colis;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class MassicotpaquetJpaController implements Serializable {

    public MassicotpaquetJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Massicotpaquet massicotpaquet) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Massicot refMassicot = massicotpaquet.getRefMassicot();
            if (refMassicot != null) {
                refMassicot = em.getReference(refMassicot.getClass(), refMassicot.getRefMassicot());
                massicotpaquet.setRefMassicot(refMassicot);
            }
            Paquet refPaquet = massicotpaquet.getRefPaquet();
            if (refPaquet != null) {
                refPaquet = em.getReference(refPaquet.getClass(), refPaquet.getRefPaquet());
                massicotpaquet.setRefPaquet(refPaquet);
            }
            em.persist(massicotpaquet);
            if (refMassicot != null) {
                refMassicot.getMassicotpaquetList().add(massicotpaquet);
                refMassicot = em.merge(refMassicot);
            }
            if (refPaquet != null) {
                refPaquet.getMassicotpaquetList().add(massicotpaquet);
                refPaquet = em.merge(refPaquet);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMassicotpaquet(massicotpaquet.getRefMassicotpaquet()) != null) {
                throw new PreexistingEntityException("Massicotpaquet " + massicotpaquet + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Massicotpaquet massicotpaquet) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Massicotpaquet persistentMassicotpaquet = em.find(Massicotpaquet.class, massicotpaquet.getRefMassicotpaquet());
            Massicot refMassicotOld = persistentMassicotpaquet.getRefMassicot();
            Massicot refMassicotNew = massicotpaquet.getRefMassicot();
            Paquet refPaquetOld = persistentMassicotpaquet.getRefPaquet();
            Paquet refPaquetNew = massicotpaquet.getRefPaquet();
            if (refMassicotNew != null) {
                refMassicotNew = em.getReference(refMassicotNew.getClass(), refMassicotNew.getRefMassicot());
                massicotpaquet.setRefMassicot(refMassicotNew);
            }
            if (refPaquetNew != null) {
                refPaquetNew = em.getReference(refPaquetNew.getClass(), refPaquetNew.getRefPaquet());
                massicotpaquet.setRefPaquet(refPaquetNew);
            }
            massicotpaquet = em.merge(massicotpaquet);
            if (refMassicotOld != null && !refMassicotOld.equals(refMassicotNew)) {
                refMassicotOld.getMassicotpaquetList().remove(massicotpaquet);
                refMassicotOld = em.merge(refMassicotOld);
            }
            if (refMassicotNew != null && !refMassicotNew.equals(refMassicotOld)) {
                refMassicotNew.getMassicotpaquetList().add(massicotpaquet);
                refMassicotNew = em.merge(refMassicotNew);
            }
            if (refPaquetOld != null && !refPaquetOld.equals(refPaquetNew)) {
                refPaquetOld.getMassicotpaquetList().remove(massicotpaquet);
                refPaquetOld = em.merge(refPaquetOld);
            }
            if (refPaquetNew != null && !refPaquetNew.equals(refPaquetOld)) {
                refPaquetNew.getMassicotpaquetList().add(massicotpaquet);
                refPaquetNew = em.merge(refPaquetNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = massicotpaquet.getRefMassicotpaquet();
                if (findMassicotpaquet(id) == null) {
                    throw new NonexistentEntityException("The massicotpaquet with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Massicotpaquet massicotpaquet;
            try {
                massicotpaquet = em.getReference(Massicotpaquet.class, id);
                massicotpaquet.getRefMassicotpaquet();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The massicotpaquet with id " + id + " no longer exists.", enfe);
            }
            Massicot refMassicot = massicotpaquet.getRefMassicot();
            if (refMassicot != null) {
                refMassicot.getMassicotpaquetList().remove(massicotpaquet);
                refMassicot = em.merge(refMassicot);
            }
            Paquet refPaquet = massicotpaquet.getRefPaquet();
            if (refPaquet != null) {
                refPaquet.getMassicotpaquetList().remove(massicotpaquet);
                refPaquet = em.merge(refPaquet);
            }
            em.remove(massicotpaquet);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Massicotpaquet> findMassicotpaquetEntities() {
        return findMassicotpaquetEntities(true, -1, -1);
    }

    public List<Massicotpaquet> findMassicotpaquetEntities(int maxResults, int firstResult) {
        return findMassicotpaquetEntities(false, maxResults, firstResult);
    }

    private List<Massicotpaquet> findMassicotpaquetEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Massicotpaquet.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Massicotpaquet findMassicotpaquet(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Massicotpaquet.class, id);
        } finally {
            em.close();
        }
    }

    public int getMassicotpaquetCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Massicotpaquet> rt = cq.from(Massicotpaquet.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<String> getMassicotPaquetByPlot(String refPlot) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select mp.refMassicotpaquet from Massicotpaquet mp where mp.refPaquet.refPlot.refPlot = :refPlot");
            q.setParameter("refPlot", refPlot);
            return q.getResultList();
        }finally{
            
        }
    }

    public void destroyByMassicotPaquet(String refMassicotPaquet) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select pk.refPaquet from Paquet pk, Massicotpaquet mp where mp.refPaquet.refPaquet = pk.refPaquet and mp.refMassicotpaquet = :ref");
            q.setParameter("ref", refMassicotPaquet);
            List result = q.getResultList();
            if(result !=null && result.size()>0)
                try {
                    destroy(result.toString());
                } catch (NonexistentEntityException ex) {
                }
        }finally{
            em.close();
        }
    }

    public String getNumPlotFromMassicot() {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select distinct mp.refPaquet.refPlot.refPlot from Massicotpaquet mp");
            return new JSONArray(q.getResultList()).toString();
        }finally{
            em.close();
        }
    }


    public String getPaquetForValidation(String plot, String face) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery(""
                    + "select mp.refMassicotpaquet, mp.refPaquet.nbreFeuille, mp.qualite "
                    + "from Massicotpaquet mp "
                    + "where mp.refMassicotpaquet like :ref "
                    + "AND mp.locked = 'FALSE'");
            q.setParameter("ref", plot+"-"+face+"%");
            return new JSONArray(q.getResultList()).toString();
        }finally{
            em.close();
        }
    }


    public String updateLockPaquet(String ref, String face, String feuille, String qualite) {
        Massicotpaquet mp = findMassicotpaquet(ref);
        PaquetJpaController contPaq = new PaquetJpaController(getEntityManager().getEntityManagerFactory());
        
        if(mp == null)
            return "{\"result\": false}";
        mp.setQualite(qualite);
        
            Paquet paq = mp.getRefPaquet();
                paq.setNbreFeuille(Integer.parseInt(feuille));
        mp.setRefPaquet(paq);
        mp.setLocked("TRUE");
        try{
            edit(mp);
            contPaq.edit(paq);
            return "{\"result\": false}";
        }catch(Exception e){return "{\"result\": false}";}
        
    }

    public String getDataforMassicotFiche(Date date) {
        EntityManager em = getEntityManager();
        
        try{
            TypedQuery q = em.createQuery(
                    "SELECT  " +
                    "    DISTINCT pt.numTravail as BILLE, " +
                    "    c.refColis,  " +
                    "    concat('[', concat(e.refEssence, concat('] - ', concat(c.essence, concat(' [', concat(c.qualite, concat(' ', concat(c.epaisseur, ']')))))))) AS ESSENCE,  " +
                    "    c.surfaceColis, " +
                    "    c.createAt, " +
                    "    ptt.longueur, " +
                    "    ptt.largeur, " +
                    "    ptt.refColis " +
                    "FROM Colis c, Essence e, Paquet pt, " + 
                    "(Select max(p.longueur) as longueur, min(p.largeur) as largeur, p.refColis.refColis  from Paquet p group by p.refColis.refColis ) ptt " +
                    "WHERE  " +
                    "    ptt.refColis = c.refColis " +
                    "    and c.essence = e.libelle " +
                    "    and c.refColis = pt.refColis.refColis "+
                    "    and c.createAt > :d1 and c.createAt < :d2 " +
//                    "    and c.createAt < :dateFin " + 
                    "order by c.refColis asc" +
                    "", List.class);
            
            Date d1 = new Date(date.getTime()), d2 = new Date(date.getTime());
            d1.setDate(d1.getDate()-1);
            d2.setDate(d2.getDate()+1); 
            q.setParameter("d1", d1);// "'"+dateDebut.getDate()+"/"+dateDebut.getMonth()+"/"+(dateDebut.getYear()+1900)+"'");
            q.setParameter("d2", d2);// "'"+dateDebut.getDate()+"/"+dateDebut.getMonth()+"/"+(dateDebut.getYear()+1900)+"'");
        
            List<Object[]> l = q.getResultList();

            List<String> result = new LinkedList();
            HashMap<String, JSONObject> map = new HashMap<>();
            l.forEach((Object[] e)->{
                JSONObject o = new JSONObject();
                JSONArray tab;
                System.out.println("colis = "+e[7]+" longueur = "+e[5]+" largeur "+e[6]);
                try {
                    /**
                    * e[1] contient le N° de Colis, on agrège les données ici par N° Colis
                    */
                   if(map.get("\""+e[1].getClass().cast(e[1])+"\"") == null){
                       tab = new JSONArray();
                       JSONObject obj = new JSONObject();
                       obj.put("data", tab);
                       obj.put("colis", "\""+e[1].getClass().cast(e[1])+"\"");
                       map.put("\""+e[1].getClass().cast(e[1])+"\"", obj);
                   }else
                       tab = (JSONArray) map.get("\""+e[1].getClass().cast(e[1])+"\"").get("data");
                
                    o.put("bille", e[0].getClass().cast(e[0]));
                    o.put("colis", e[1].getClass().cast(e[1]));
                    o.put("essence", e[2].getClass().cast(e[2]));
                    o.put("surface", e[3].getClass().cast(e[3]));
                    o.put("date", e[4].getClass().cast(e[4]));
                    o.put("longueur", e[5].getClass().cast(e[5]));
                    o.put("largeur", e[6].getClass().cast(e[6]));
                    tab.put(o);
                } catch (JSONException ex) {}
            });

            return map.toString().replaceAll("=", " : ");
        }catch(Exception e){
            e.printStackTrace();
            return "[]";
        }finally{
            em.close();
        }
    }

    
}
