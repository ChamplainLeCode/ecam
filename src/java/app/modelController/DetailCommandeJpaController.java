/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Commande;
import app.models.DetailCommande;
import app.models.Essence;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.Database;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class DetailCommandeJpaController implements Serializable {

    public static List<DetailCommande> findDetailCommandeByCommande(String refCommande) {
        EntityManager em = Database.getEntityManager().createEntityManager();
        try {
            Query q = em.createQuery("from DetailCommande cmd where cmd.refCommande.refCommande = :commande");
            q.setParameter("commande", refCommande);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public static DetailCommande findDetailCommandeByBille(String bille) {
        EntityManager em = Database.getEntityManager().createEntityManager();
        try {
            Query q = em.createQuery("from DetailCommande cmd where cmd.numBille = :bille", DetailCommande.class);
            q.setParameter("bille", bille);
            Object result = q.getSingleResult();
            if(result == null)
                return null;
            return (DetailCommande) result;
        } finally {
            em.close();
        }
    }


    public DetailCommandeJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DetailCommande detailCommande) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Commande refCommande = detailCommande.getRefCommande();
            if (refCommande != null) {
                refCommande = em.getReference(refCommande.getClass(), refCommande.getRefCommande());
                detailCommande.setRefCommande(refCommande);
            }
            Essence refEssence = detailCommande.getRefEssence();
            if (refEssence != null) {
                refEssence = em.getReference(refEssence.getClass(), refEssence.getRefEssence());
                detailCommande.setRefEssence(refEssence);
            }
            em.persist(detailCommande);
            if (refCommande != null) {
                refCommande.getDetailCommandeList().add(detailCommande);
                refCommande = em.merge(refCommande);
            }
            if (refEssence != null) {
                refEssence.getDetailCommandeList().add(detailCommande);
                refEssence = em.merge(refEssence);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDetailCommande(detailCommande.getRefDetail()) != null) {
                throw new PreexistingEntityException("DetailCommande " + detailCommande + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DetailCommande detailCommande) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailCommande persistentDetailCommande = em.find(DetailCommande.class, detailCommande.getRefDetail());
            Commande refCommandeOld = persistentDetailCommande.getRefCommande();
            Commande refCommandeNew = detailCommande.getRefCommande();
            Essence refEssenceOld = persistentDetailCommande.getRefEssence();
            Essence refEssenceNew = detailCommande.getRefEssence();
            if (refCommandeNew != null) {
                refCommandeNew = em.getReference(refCommandeNew.getClass(), refCommandeNew.getRefCommande());
                detailCommande.setRefCommande(refCommandeNew);
            }
            if (refEssenceNew != null) {
                refEssenceNew = em.getReference(refEssenceNew.getClass(), refEssenceNew.getRefEssence());
                detailCommande.setRefEssence(refEssenceNew);
            }
            detailCommande = em.merge(detailCommande);
            if (refCommandeOld != null && !refCommandeOld.equals(refCommandeNew)) {
                refCommandeOld.getDetailCommandeList().remove(detailCommande);
                refCommandeOld = em.merge(refCommandeOld);
            }
            if (refCommandeNew != null && !refCommandeNew.equals(refCommandeOld)) {
                refCommandeNew.getDetailCommandeList().add(detailCommande);
                refCommandeNew = em.merge(refCommandeNew);
            }
            if (refEssenceOld != null && !refEssenceOld.equals(refEssenceNew)) {
                refEssenceOld.getDetailCommandeList().remove(detailCommande);
                refEssenceOld = em.merge(refEssenceOld);
            }
            if (refEssenceNew != null && !refEssenceNew.equals(refEssenceOld)) {
                refEssenceNew.getDetailCommandeList().add(detailCommande);
                refEssenceNew = em.merge(refEssenceNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = detailCommande.getRefDetail();
                if (findDetailCommande(id) == null) {
                    throw new NonexistentEntityException("The detailCommande with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailCommande detailCommande;
            try {
                detailCommande = em.getReference(DetailCommande.class, id);
                detailCommande.getRefDetail();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The detailCommande with id " + id + " no longer exists.", enfe);
            }
            Commande refCommande = detailCommande.getRefCommande();
            if (refCommande != null) {
                refCommande.getDetailCommandeList().remove(detailCommande);
                refCommande = em.merge(refCommande);
            }
            Essence refEssence = detailCommande.getRefEssence();
            if (refEssence != null) {
                refEssence.getDetailCommandeList().remove(detailCommande);
                refEssence = em.merge(refEssence);
            }
            em.remove(detailCommande);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DetailCommande> findDetailCommandeEntities() {
        return findDetailCommandeEntities(true, -1, -1);
    }

    public List<DetailCommande> findDetailCommandeEntities(int maxResults, int firstResult) {
        return findDetailCommandeEntities(false, maxResults, firstResult);
    }

    private List<DetailCommande> findDetailCommandeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DetailCommande.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DetailCommande findDetailCommande(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DetailCommande.class, id);
        } finally {
            em.close();
        }
    }

    public int getDetailCommandeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DetailCommande> rt = cq.from(DetailCommande.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public DetailCommande getDetailCommandeByNumBille(String billeNum) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createNamedQuery("DetailCommande.findByNumBille", DetailCommande.class);
            q.setParameter("numBille", billeNum);
            return (DetailCommande) q.getResultList().get(0);
        }finally{
            em.close();
        }
    }
    
}
