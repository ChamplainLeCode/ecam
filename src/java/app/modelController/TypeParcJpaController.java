/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Parc;
import app.models.TypeParc;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.ParcChargement;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class TypeParcJpaController implements Serializable {

    public TypeParcJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TypeParc typeParc) throws PreexistingEntityException, Exception {
        if (typeParc.getParcList() == null) {
            typeParc.setParcList(new ArrayList<Parc>());
        }
        if (typeParc.getParcChargementList() == null) {
            typeParc.setParcChargementList(new ArrayList<ParcChargement>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Parc> attachedParcList = new ArrayList<Parc>();
            for (Parc parcListParcToAttach : typeParc.getParcList()) {
                parcListParcToAttach = em.getReference(parcListParcToAttach.getClass(), parcListParcToAttach.getRefParc());
                attachedParcList.add(parcListParcToAttach);
            }
            typeParc.setParcList(attachedParcList);
            List<ParcChargement> attachedParcChargementList = new ArrayList<ParcChargement>();
            for (ParcChargement parcChargementListParcChargementToAttach : typeParc.getParcChargementList()) {
                parcChargementListParcChargementToAttach = em.getReference(parcChargementListParcChargementToAttach.getClass(), parcChargementListParcChargementToAttach.getRefParc());
                attachedParcChargementList.add(parcChargementListParcChargementToAttach);
            }
            typeParc.setParcChargementList(attachedParcChargementList);
            em.persist(typeParc);
            for (Parc parcListParc : typeParc.getParcList()) {
                TypeParc oldRefTypeparcOfParcListParc = parcListParc.getRefTypeparc();
                parcListParc.setRefTypeparc(typeParc);
                parcListParc = em.merge(parcListParc);
                if (oldRefTypeparcOfParcListParc != null) {
                    oldRefTypeparcOfParcListParc.getParcList().remove(parcListParc);
                    oldRefTypeparcOfParcListParc = em.merge(oldRefTypeparcOfParcListParc);
                }
            }
            for (ParcChargement parcChargementListParcChargement : typeParc.getParcChargementList()) {
                TypeParc oldRefTypeparcOfParcChargementListParcChargement = parcChargementListParcChargement.getRefTypeparc();
                parcChargementListParcChargement.setRefTypeparc(typeParc);
                parcChargementListParcChargement = em.merge(parcChargementListParcChargement);
                if (oldRefTypeparcOfParcChargementListParcChargement != null) {
                    oldRefTypeparcOfParcChargementListParcChargement.getParcChargementList().remove(parcChargementListParcChargement);
                    oldRefTypeparcOfParcChargementListParcChargement = em.merge(oldRefTypeparcOfParcChargementListParcChargement);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTypeParc(typeParc.getRefTypeparc()) != null) {
                throw new PreexistingEntityException("TypeParc " + typeParc + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TypeParc typeParc) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TypeParc persistentTypeParc = em.find(TypeParc.class, typeParc.getRefTypeparc());
            List<Parc> parcListOld = persistentTypeParc.getParcList();
            List<Parc> parcListNew = typeParc.getParcList();
            List<ParcChargement> parcChargementListOld = persistentTypeParc.getParcChargementList();
            List<ParcChargement> parcChargementListNew = typeParc.getParcChargementList();
            List<Parc> attachedParcListNew = new ArrayList<Parc>();
            for (Parc parcListNewParcToAttach : parcListNew) {
                parcListNewParcToAttach = em.getReference(parcListNewParcToAttach.getClass(), parcListNewParcToAttach.getRefParc());
                attachedParcListNew.add(parcListNewParcToAttach);
            }
            parcListNew = attachedParcListNew;
            typeParc.setParcList(parcListNew);
            List<ParcChargement> attachedParcChargementListNew = new ArrayList<ParcChargement>();
            for (ParcChargement parcChargementListNewParcChargementToAttach : parcChargementListNew) {
                parcChargementListNewParcChargementToAttach = em.getReference(parcChargementListNewParcChargementToAttach.getClass(), parcChargementListNewParcChargementToAttach.getRefParc());
                attachedParcChargementListNew.add(parcChargementListNewParcChargementToAttach);
            }
            parcChargementListNew = attachedParcChargementListNew;
            typeParc.setParcChargementList(parcChargementListNew);
            typeParc = em.merge(typeParc);
            for (Parc parcListOldParc : parcListOld) {
                if (!parcListNew.contains(parcListOldParc)) {
                    parcListOldParc.setRefTypeparc(null);
                    parcListOldParc = em.merge(parcListOldParc);
                }
            }
            for (Parc parcListNewParc : parcListNew) {
                if (!parcListOld.contains(parcListNewParc)) {
                    TypeParc oldRefTypeparcOfParcListNewParc = parcListNewParc.getRefTypeparc();
                    parcListNewParc.setRefTypeparc(typeParc);
                    parcListNewParc = em.merge(parcListNewParc);
                    if (oldRefTypeparcOfParcListNewParc != null && !oldRefTypeparcOfParcListNewParc.equals(typeParc)) {
                        oldRefTypeparcOfParcListNewParc.getParcList().remove(parcListNewParc);
                        oldRefTypeparcOfParcListNewParc = em.merge(oldRefTypeparcOfParcListNewParc);
                    }
                }
            }
            for (ParcChargement parcChargementListOldParcChargement : parcChargementListOld) {
                if (!parcChargementListNew.contains(parcChargementListOldParcChargement)) {
                    parcChargementListOldParcChargement.setRefTypeparc(null);
                    parcChargementListOldParcChargement = em.merge(parcChargementListOldParcChargement);
                }
            }
            for (ParcChargement parcChargementListNewParcChargement : parcChargementListNew) {
                if (!parcChargementListOld.contains(parcChargementListNewParcChargement)) {
                    TypeParc oldRefTypeparcOfParcChargementListNewParcChargement = parcChargementListNewParcChargement.getRefTypeparc();
                    parcChargementListNewParcChargement.setRefTypeparc(typeParc);
                    parcChargementListNewParcChargement = em.merge(parcChargementListNewParcChargement);
                    if (oldRefTypeparcOfParcChargementListNewParcChargement != null && !oldRefTypeparcOfParcChargementListNewParcChargement.equals(typeParc)) {
                        oldRefTypeparcOfParcChargementListNewParcChargement.getParcChargementList().remove(parcChargementListNewParcChargement);
                        oldRefTypeparcOfParcChargementListNewParcChargement = em.merge(oldRefTypeparcOfParcChargementListNewParcChargement);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = typeParc.getRefTypeparc();
                if (findTypeParc(id) == null) {
                    throw new NonexistentEntityException("The typeParc with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TypeParc typeParc;
            try {
                typeParc = em.getReference(TypeParc.class, id);
                typeParc.getRefTypeparc();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The typeParc with id " + id + " no longer exists.", enfe);
            }
            List<Parc> parcList = typeParc.getParcList();
            for (Parc parcListParc : parcList) {
                parcListParc.setRefTypeparc(null);
                parcListParc = em.merge(parcListParc);
            }
            List<ParcChargement> parcChargementList = typeParc.getParcChargementList();
            for (ParcChargement parcChargementListParcChargement : parcChargementList) {
                parcChargementListParcChargement.setRefTypeparc(null);
                parcChargementListParcChargement = em.merge(parcChargementListParcChargement);
            }
            em.remove(typeParc);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TypeParc> findTypeParcEntities() {
        return findTypeParcEntities(true, -1, -1);
    }

    public List<TypeParc> findTypeParcEntities(int maxResults, int firstResult) {
        return findTypeParcEntities(false, maxResults, firstResult);
    }

    private List<TypeParc> findTypeParcEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TypeParc.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TypeParc findTypeParc(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TypeParc.class, id);
        } finally {
            em.close();
        }
    }

    public int getTypeParcCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TypeParc> rt = cq.from(TypeParc.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    
}
