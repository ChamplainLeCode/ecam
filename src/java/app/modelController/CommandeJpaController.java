/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.models.Commande;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Fournisseur;
import app.models.Reception;
import java.util.ArrayList;
import java.util.List;
import app.models.DetailCommande;
import app.modelController.exceptions.IllegalOrphanException;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.Database;
import app.models.DetailReception;
import app.models.ParcChargement;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class CommandeJpaController implements Serializable {

    
    public static List<Commande> findCommandeByFounisseur(String refFournisseur) {
        EntityManager em = Database.getEntityManager().createEntityManager();
        try {
            Query q = em.createQuery("from Commande cmd where cmd.refFournisseur.refFournisseur = :refFournisseur");
            q.setParameter("refFournisseur", refFournisseur);
            List<Commande> commande =  q.getResultList();
            return commande;
        } finally {
            em.close();
        }
    }

    public static List<Commande> findCommandeRefByFounisseur(String refFournisseur) {
        EntityManager em = Database.getEntityManager().createEntityManager();
        try{
            Query q = em.createQuery("Select cmd.refCommande from Commande cmd where cmd.refFournisseur.refFournisseur = :refFournisseur");
            q.setParameter("refFournisseur", refFournisseur);
            List liste = q.getResultList();
            return liste;
        }finally{
            em.close();
        }
    }

    public CommandeJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Commande commande) throws PreexistingEntityException, Exception {
        if (commande.getDetailCommandeList() == null) {
            commande.setDetailCommandeList(new ArrayList<DetailCommande>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Fournisseur refFournisseur = commande.getRefFournisseur();
            if (refFournisseur != null) {
                refFournisseur = em.getReference(refFournisseur.getClass(), refFournisseur.getRefFournisseur());
                commande.setRefFournisseur(refFournisseur);
            }

            List<DetailCommande> attachedDetailCommandeList = new ArrayList<DetailCommande>();
            for (DetailCommande detailCommandeListDetailCommandeToAttach : commande.getDetailCommandeList()) {
                detailCommandeListDetailCommandeToAttach = em.getReference(detailCommandeListDetailCommandeToAttach.getClass(), detailCommandeListDetailCommandeToAttach.getRefDetail());
                attachedDetailCommandeList.add(detailCommandeListDetailCommandeToAttach);
            }
            commande.setDetailCommandeList(attachedDetailCommandeList);
            em.persist(commande);
            if (refFournisseur != null) {
                refFournisseur.getCommandeList().add(commande);
                refFournisseur = em.merge(refFournisseur);
            }

            for (DetailCommande detailCommandeListDetailCommande : commande.getDetailCommandeList()) {
                Commande oldRefCommandeOfDetailCommandeListDetailCommande = detailCommandeListDetailCommande.getRefCommande();
                detailCommandeListDetailCommande.setRefCommande(commande);
                detailCommandeListDetailCommande = em.merge(detailCommandeListDetailCommande);
                if (oldRefCommandeOfDetailCommandeListDetailCommande != null) {
                    oldRefCommandeOfDetailCommandeListDetailCommande.getDetailCommandeList().remove(detailCommandeListDetailCommande);
                    oldRefCommandeOfDetailCommandeListDetailCommande = em.merge(oldRefCommandeOfDetailCommandeListDetailCommande);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCommande(commande.getRefCommande()) != null) {
                throw new PreexistingEntityException("Commande " + commande + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Commande commande) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Commande persistentCommande = em.find(Commande.class, commande.getRefCommande());
            Fournisseur refFournisseurOld = persistentCommande.getRefFournisseur();
            Fournisseur refFournisseurNew = commande.getRefFournisseur();

            List<DetailCommande> detailCommandeListOld = persistentCommande.getDetailCommandeList();
            List<DetailCommande> detailCommandeListNew = commande.getDetailCommandeList();
            List<String> illegalOrphanMessages = null;
            for (DetailCommande detailCommandeListOldDetailCommande : detailCommandeListOld) {
                if (!detailCommandeListNew.contains(detailCommandeListOldDetailCommande)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain DetailCommande " + detailCommandeListOldDetailCommande + " since its refCommande field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (refFournisseurNew != null) {
                refFournisseurNew = em.getReference(refFournisseurNew.getClass(), refFournisseurNew.getRefFournisseur());
                commande.setRefFournisseur(refFournisseurNew);
            }

            List<DetailCommande> attachedDetailCommandeListNew = new ArrayList<DetailCommande>();
            for (DetailCommande detailCommandeListNewDetailCommandeToAttach : detailCommandeListNew) {
                detailCommandeListNewDetailCommandeToAttach = em.getReference(detailCommandeListNewDetailCommandeToAttach.getClass(), detailCommandeListNewDetailCommandeToAttach.getRefDetail());
                attachedDetailCommandeListNew.add(detailCommandeListNewDetailCommandeToAttach);
            }
            detailCommandeListNew = attachedDetailCommandeListNew;
            commande.setDetailCommandeList(detailCommandeListNew);
            commande = em.merge(commande);
            if (refFournisseurOld != null && !refFournisseurOld.equals(refFournisseurNew)) {
                refFournisseurOld.getCommandeList().remove(commande);
                refFournisseurOld = em.merge(refFournisseurOld);
            }
            if (refFournisseurNew != null && !refFournisseurNew.equals(refFournisseurOld)) {
                refFournisseurNew.getCommandeList().add(commande);
                refFournisseurNew = em.merge(refFournisseurNew);
            }

            for (DetailCommande detailCommandeListNewDetailCommande : detailCommandeListNew) {
                if (!detailCommandeListOld.contains(detailCommandeListNewDetailCommande)) {
                    Commande oldRefCommandeOfDetailCommandeListNewDetailCommande = detailCommandeListNewDetailCommande.getRefCommande();
                    detailCommandeListNewDetailCommande.setRefCommande(commande);
                    detailCommandeListNewDetailCommande = em.merge(detailCommandeListNewDetailCommande);
                    if (oldRefCommandeOfDetailCommandeListNewDetailCommande != null && !oldRefCommandeOfDetailCommandeListNewDetailCommande.equals(commande)) {
                        oldRefCommandeOfDetailCommandeListNewDetailCommande.getDetailCommandeList().remove(detailCommandeListNewDetailCommande);
                        oldRefCommandeOfDetailCommandeListNewDetailCommande = em.merge(oldRefCommandeOfDetailCommandeListNewDetailCommande);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = commande.getRefCommande();
                if (findCommande(id) == null) {
                    throw new NonexistentEntityException("The commande with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Commande commande;
            try {
                commande = em.getReference(Commande.class, id);
                commande.getRefCommande();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The commande with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<DetailCommande> detailCommandeListOrphanCheck = commande.getDetailCommandeList();
            for (DetailCommande detailCommandeListOrphanCheckDetailCommande : detailCommandeListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Commande (" + commande + ") cannot be destroyed since the DetailCommande " + detailCommandeListOrphanCheckDetailCommande + " in its detailCommandeList field has a non-nullable refCommande field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Fournisseur refFournisseur = commande.getRefFournisseur();
            if (refFournisseur != null) {
                refFournisseur.getCommandeList().remove(commande);
                refFournisseur = em.merge(refFournisseur);
            }

            em.remove(commande);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Commande> findCommandeEntities() {
        return findCommandeEntities(true, -1, -1);
    }

    public List<Commande> findCommandeEntities(int maxResults, int firstResult) {
        return findCommandeEntities(false, maxResults, firstResult);
    }

    private List<Commande> findCommandeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Commande.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Commande findCommande(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Commande.class, id);
        } finally {
            em.close();
        }
    }

    public int getCommandeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Commande> rt = cq.from(Commande.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    ParcChargement findCommandeForNumTravail(String numTravail) {
        EntityManager em = getEntityManager();
        
        try{
            Query q = em.createQuery(""
                    + "SELECT drecept FROM DetailReception drecept "
                    + "WHERE drecept.numtravail = :numTravail "
            );
            q.setParameter("numTravail", numTravail);
            List<DetailReception> l = q.getResultList();
            System.out.println("Parc = "+l+ " travail = "+numTravail);
            return l == null || l.isEmpty() ? null : l.get(0).getRefReception().getRefCommande().getRefParc();
        }finally{
            em.close();
        }
    }

    public int getCommandeCountForFournisseur(String refFournisseur) {
    
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("SELECT COUNT(1) from Commande c WHERE c.refFournisseur.refFournisseur = :fournisseur");
            q.setParameter("fournisseur", refFournisseur);
            return Integer.parseInt(q.getSingleResult().toString());
        }finally{
            em.close();
        }
    }
    
}
