/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Colis;
import app.models.Jointage;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 *
 * @author champlain
 */
public class JointageJpaController implements Serializable {

    public JointageJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Jointage jointage) throws PreexistingEntityException, Exception {
        if (jointage.getColisList() == null) {
            jointage.setColisList(new ArrayList<Colis>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Colis colisFinal = jointage.getColisFinal();
            if (colisFinal != null) {
                colisFinal = em.getReference(colisFinal.getClass(), colisFinal.getRefColis());
                jointage.setColisFinal(colisFinal);
            }
            List<Colis> attachedColisList = new ArrayList<Colis>();
            for (Colis colisListColisToAttach : jointage.getColisList()) {
                colisListColisToAttach = em.getReference(colisListColisToAttach.getClass(), colisListColisToAttach.getRefColis());
                attachedColisList.add(colisListColisToAttach);
            }
            jointage.setColisList(attachedColisList);
            em.persist(jointage);
            if (colisFinal != null) {
                colisFinal.getJointageList().add(jointage);
                colisFinal = em.merge(colisFinal);
            }
            for (Colis colisListColis : jointage.getColisList()) {
                colisListColis.getJointageList().add(jointage);
                colisListColis = em.merge(colisListColis);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findJointage(jointage.getRefJointage()) != null) {
                throw new PreexistingEntityException("Jointage " + jointage + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Jointage jointage) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Jointage persistentJointage = em.find(Jointage.class, jointage.getRefJointage());
            Colis colisFinalOld = persistentJointage.getColisFinal();
            Colis colisFinalNew = jointage.getColisFinal();
            List<Colis> colisListOld = persistentJointage.getColisList();
            List<Colis> colisListNew = jointage.getColisList();
            if (colisFinalNew != null) {
                colisFinalNew = em.getReference(colisFinalNew.getClass(), colisFinalNew.getRefColis());
                jointage.setColisFinal(colisFinalNew);
            }
            List<Colis> attachedColisListNew = new ArrayList<Colis>();
            for (Colis colisListNewColisToAttach : colisListNew) {
                colisListNewColisToAttach = em.getReference(colisListNewColisToAttach.getClass(), colisListNewColisToAttach.getRefColis());
                attachedColisListNew.add(colisListNewColisToAttach);
            }
            colisListNew = attachedColisListNew;
            jointage.setColisList(colisListNew);
            jointage = em.merge(jointage);
            if (colisFinalOld != null && !colisFinalOld.equals(colisFinalNew)) {
                colisFinalOld.getJointageList().remove(jointage);
                colisFinalOld = em.merge(colisFinalOld);
            }
            if (colisFinalNew != null && !colisFinalNew.equals(colisFinalOld)) {
                colisFinalNew.getJointageList().add(jointage);
                colisFinalNew = em.merge(colisFinalNew);
            }
            for (Colis colisListOldColis : colisListOld) {
                if (!colisListNew.contains(colisListOldColis)) {
                    colisListOldColis.getJointageList().remove(jointage);
                    colisListOldColis = em.merge(colisListOldColis);
                }
            }
            for (Colis colisListNewColis : colisListNew) {
                if (!colisListOld.contains(colisListNewColis)) {
                    colisListNewColis.getJointageList().add(jointage);
                    colisListNewColis = em.merge(colisListNewColis);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = jointage.getRefJointage();
                if (findJointage(id) == null) {
                    throw new NonexistentEntityException("The jointage with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Jointage jointage;
            try {
                jointage = em.getReference(Jointage.class, id);
                jointage.getRefJointage();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The jointage with id " + id + " no longer exists.", enfe);
            }
            Colis colisFinal = jointage.getColisFinal();
            if (colisFinal != null) {
                colisFinal.getJointageList().remove(jointage);
                colisFinal = em.merge(colisFinal);
            }
            List<Colis> colisList = jointage.getColisList();
            for (Colis colisListColis : colisList) {
                colisListColis.getJointageList().remove(jointage);
                colisListColis = em.merge(colisListColis);
            }
            em.remove(jointage);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Jointage> findJointageEntities() {
        return findJointageEntities(true, -1, -1);
    }

    public List<Jointage> findJointageEntities(int maxResults, int firstResult) {
        return findJointageEntities(false, maxResults, firstResult);
    }

    private List<Jointage> findJointageEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Jointage.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Jointage findJointage(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Jointage.class, id);
        } finally {
            em.close();
        }
    }

    public int getJointageCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Jointage> rt = cq.from(Jointage.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public JSONArray findJointageByDate(Date date) {
        EntityManager em = getEntityManager();
        try{
            Date d1 = new Date(date.getTime()), d2 = new Date(date.getTime());
            d1.setDate(d1.getDate()-1);
            d2.setDate(d2.getDate()+1); 

            Query q = em.createQuery("SELECT c from Colis c WHERE c.typeColis = 'J' AND c.createAt > :d1 and c.createAt < :d2  ", Colis.class);

            q.setParameter("d1", d1);// "'"+dateDebut.getDate()+"/"+dateDebut.getMonth()+"/"+(dateDebut.getYear()+1900)+"'");
            q.setParameter("d2", d2);
            List<Colis> liste = (List<Colis>)q.getResultList();
            JSONArray list = new JSONArray();
            liste.forEach((Colis c)->{
                try {
                    JSONObject o = new JSONObject();
                    o.put("liste", getColisForJointage(c.getJointage(), c));
                    o.put("jointage", c.getJointage());
                    list.put(o);
                } catch (JSONException ex) {

                }
            });
            return list;
        }finally{
            em.close();
        }
    }

    private List<Colis> getColisForJointage(Jointage jointage, Colis c) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("SELECT c FROM Colis c WHERE  c.jointage.refJointage = :jointage AND not(c.refColis = :colis)");
            q.setParameter("jointage", jointage.getRefJointage());
            q.setParameter("colis", c.getRefColis());
            return q.getResultList();
        }finally{
            em.close();
        }
    }
    
}
