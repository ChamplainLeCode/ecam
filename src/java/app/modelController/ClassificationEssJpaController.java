/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.models.ClassificationEss;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Essence;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class ClassificationEssJpaController implements Serializable {

    public ClassificationEssJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ClassificationEss classificationEss) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Essence refEssence = classificationEss.getRefEssence();
            if (refEssence != null) {
                refEssence = em.getReference(refEssence.getClass(), refEssence.getRefEssence());
                classificationEss.setRefEssence(refEssence);
            }
            em.persist(classificationEss);
            if (refEssence != null) {
                refEssence.getClassificationEssList().add(classificationEss);
                refEssence = em.merge(refEssence);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findClassificationEss(classificationEss.getRefClassification()) != null) {
                throw new PreexistingEntityException("ClassificationEss " + classificationEss + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ClassificationEss classificationEss) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ClassificationEss persistentClassificationEss = em.find(ClassificationEss.class, classificationEss.getRefClassification());
            Essence refEssenceOld = persistentClassificationEss.getRefEssence();
            Essence refEssenceNew = classificationEss.getRefEssence();
            if (refEssenceNew != null) {
                refEssenceNew = em.getReference(refEssenceNew.getClass(), refEssenceNew.getRefEssence());
                classificationEss.setRefEssence(refEssenceNew);
            }
            classificationEss = em.merge(classificationEss);
            if (refEssenceOld != null && !refEssenceOld.equals(refEssenceNew)) {
                refEssenceOld.getClassificationEssList().remove(classificationEss);
                refEssenceOld = em.merge(refEssenceOld);
            }
            if (refEssenceNew != null && !refEssenceNew.equals(refEssenceOld)) {
                refEssenceNew.getClassificationEssList().add(classificationEss);
                refEssenceNew = em.merge(refEssenceNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = classificationEss.getRefClassification();
                if (findClassificationEss(id) == null) {
                    throw new NonexistentEntityException("The classificationEss with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ClassificationEss classificationEss;
            try {
                classificationEss = em.getReference(ClassificationEss.class, id);
                classificationEss.getRefClassification();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The classificationEss with id " + id + " no longer exists.", enfe);
            }
            Essence refEssence = classificationEss.getRefEssence();
            if (refEssence != null) {
                refEssence.getClassificationEssList().remove(classificationEss);
                refEssence = em.merge(refEssence);
            }
            em.remove(classificationEss);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ClassificationEss> findClassificationEssEntities() {
        return findClassificationEssEntities(true, -1, -1);
    }

    public List<ClassificationEss> findClassificationEssEntities(int maxResults, int firstResult) {
        return findClassificationEssEntities(false, maxResults, firstResult);
    }

    private List<ClassificationEss> findClassificationEssEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ClassificationEss.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ClassificationEss findClassificationEss(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ClassificationEss.class, id);
        } finally {
            em.close();
        }
    }

    public int getClassificationEssCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ClassificationEss> rt = cq.from(ClassificationEss.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
