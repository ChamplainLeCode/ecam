/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.TypeParc;
import app.models.Billonnage;
import app.models.Parc;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.json.JSONArray;

/**
 *
 * @author champlain
 */
public class ParcJpaController implements Serializable {

    public ParcJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
        private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Parc parc) throws PreexistingEntityException, Exception {
        if (parc.getBillonnageList() == null) {
            parc.setBillonnageList(new ArrayList<Billonnage>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TypeParc refTypeparc = parc.getRefTypeparc();
            if (refTypeparc != null) {
                refTypeparc = em.getReference(refTypeparc.getClass(), refTypeparc.getRefTypeparc());
                parc.setRefTypeparc(refTypeparc);
            }
            TypeParc refTypeparcDepart = parc.getRefTypeparcDepart();
            if (refTypeparcDepart != null) {
                refTypeparcDepart = em.getReference(refTypeparcDepart.getClass(), refTypeparcDepart.getRefTypeparc());
                parc.setRefTypeparcDepart(refTypeparcDepart);
            }
            List<Billonnage> attachedBillonnageList = new ArrayList<Billonnage>();
            for (Billonnage billonnageListBillonnageToAttach : parc.getBillonnageList()) {
                billonnageListBillonnageToAttach = em.getReference(billonnageListBillonnageToAttach.getClass(), billonnageListBillonnageToAttach.getRefBillonnage());
                attachedBillonnageList.add(billonnageListBillonnageToAttach);
            }
            parc.setBillonnageList(attachedBillonnageList);
            em.persist(parc);
            if (refTypeparc != null) {
                refTypeparc.getParcList().add(parc);
                refTypeparc = em.merge(refTypeparc);
            }
            if (refTypeparcDepart != null) {
                refTypeparcDepart.getParcList().add(parc);
                refTypeparcDepart = em.merge(refTypeparcDepart);
            }
            for (Billonnage billonnageListBillonnage : parc.getBillonnageList()) {
                Parc oldRefParcOfBillonnageListBillonnage = billonnageListBillonnage.getRefParc();
                billonnageListBillonnage.setRefParc(parc);
                billonnageListBillonnage = em.merge(billonnageListBillonnage);
                if (oldRefParcOfBillonnageListBillonnage != null) {
                    oldRefParcOfBillonnageListBillonnage.getBillonnageList().remove(billonnageListBillonnage);
                    oldRefParcOfBillonnageListBillonnage = em.merge(oldRefParcOfBillonnageListBillonnage);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findParc(parc.getRefParc()) != null) {
                throw new PreexistingEntityException("Parc " + parc + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Parc parc) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Parc persistentParc = em.find(Parc.class, parc.getRefParc());
            TypeParc refTypeparcOld = persistentParc.getRefTypeparc();
            TypeParc refTypeparcNew = parc.getRefTypeparc();
            TypeParc refTypeparcDepartOld = persistentParc.getRefTypeparcDepart();
            TypeParc refTypeparcDepartNew = parc.getRefTypeparcDepart();
            List<Billonnage> billonnageListOld = persistentParc.getBillonnageList();
            List<Billonnage> billonnageListNew = parc.getBillonnageList();
            if (refTypeparcNew != null) {
                refTypeparcNew = em.getReference(refTypeparcNew.getClass(), refTypeparcNew.getRefTypeparc());
                parc.setRefTypeparc(refTypeparcNew);
            }
            if (refTypeparcDepartNew != null) {
                refTypeparcDepartNew = em.getReference(refTypeparcDepartNew.getClass(), refTypeparcDepartNew.getRefTypeparc());
                parc.setRefTypeparcDepart(refTypeparcDepartNew);
            }
            List<Billonnage> attachedBillonnageListNew = new ArrayList<Billonnage>();
            for (Billonnage billonnageListNewBillonnageToAttach : billonnageListNew) {
                billonnageListNewBillonnageToAttach = em.getReference(billonnageListNewBillonnageToAttach.getClass(), billonnageListNewBillonnageToAttach.getRefBillonnage());
                attachedBillonnageListNew.add(billonnageListNewBillonnageToAttach);
            }
            billonnageListNew = attachedBillonnageListNew;
            parc.setBillonnageList(billonnageListNew);
            parc = em.merge(parc);
            if (refTypeparcOld != null && !refTypeparcOld.equals(refTypeparcNew)) {
                refTypeparcOld.getParcList().remove(parc);
                refTypeparcOld = em.merge(refTypeparcOld);
            }
            if (refTypeparcNew != null && !refTypeparcNew.equals(refTypeparcOld)) {
                refTypeparcNew.getParcList().add(parc);
                refTypeparcNew = em.merge(refTypeparcNew);
            }
            if (refTypeparcDepartOld != null && !refTypeparcDepartOld.equals(refTypeparcDepartNew)) {
                refTypeparcDepartOld.getParcList().remove(parc);
                refTypeparcDepartOld = em.merge(refTypeparcDepartOld);
            }
            if (refTypeparcDepartNew != null && !refTypeparcDepartNew.equals(refTypeparcDepartOld)) {
                refTypeparcDepartNew.getParcList().add(parc);
                refTypeparcDepartNew = em.merge(refTypeparcDepartNew);
            }
            for (Billonnage billonnageListOldBillonnage : billonnageListOld) {
                if (!billonnageListNew.contains(billonnageListOldBillonnage)) {
                    billonnageListOldBillonnage.setRefParc(null);
                    billonnageListOldBillonnage = em.merge(billonnageListOldBillonnage);
                }
            }
            for (Billonnage billonnageListNewBillonnage : billonnageListNew) {
                if (!billonnageListOld.contains(billonnageListNewBillonnage)) {
                    Parc oldRefParcOfBillonnageListNewBillonnage = billonnageListNewBillonnage.getRefParc();
                    billonnageListNewBillonnage.setRefParc(parc);
                    billonnageListNewBillonnage = em.merge(billonnageListNewBillonnage);
                    if (oldRefParcOfBillonnageListNewBillonnage != null && !oldRefParcOfBillonnageListNewBillonnage.equals(parc)) {
                        oldRefParcOfBillonnageListNewBillonnage.getBillonnageList().remove(billonnageListNewBillonnage);
                        oldRefParcOfBillonnageListNewBillonnage = em.merge(oldRefParcOfBillonnageListNewBillonnage);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = parc.getRefParc();
                if (findParc(id) == null) {
                    throw new NonexistentEntityException("The parc with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Parc parc;
            try {
                parc = em.getReference(Parc.class, id);
                parc.getRefParc();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The parc with id " + id + " no longer exists.", enfe);
            }
            TypeParc refTypeparc = parc.getRefTypeparc();
            if (refTypeparc != null) {
                refTypeparc.getParcList().remove(parc);
                refTypeparc = em.merge(refTypeparc);
            }
            TypeParc refTypeparcDepart = parc.getRefTypeparcDepart();
            if (refTypeparcDepart != null) {
                refTypeparcDepart.getParcList().remove(parc);
                refTypeparcDepart = em.merge(refTypeparcDepart);
            }
            List<Billonnage> billonnageList = parc.getBillonnageList();
            for (Billonnage billonnageListBillonnage : billonnageList) {
                billonnageListBillonnage.setRefParc(null);
                billonnageListBillonnage = em.merge(billonnageListBillonnage);
            }
            em.remove(parc);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Parc> findParcEntities() {
        return findParcEntities(true, -1, -1);
    }

    public List<Parc> findParcEntities(int maxResults, int firstResult) {
        return findParcEntities(false, maxResults, firstResult);
    }

    private List<Parc> findParcEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            //CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            //cq.select(cq.from(Parc.class));
            Query q = em.createQuery("FROM Parc p where p.visible = 'T'");// em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Parc findParc(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Parc.class, id);
        } finally {
            em.close();
        }
    }

    public int getParcCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Parc> rt = cq.from(Parc.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    

    public Parc getParcByBille(String bille){
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("SELECT p from Parc p where p.numBille = :bille");
            q.setParameter("bille", bille);
            List l = q.getResultList();
            return (Parc) l.get(l.size()-1);
        }finally{
            em.close();
        }
    }

    public List<Parc> getParcByOrigine(Map<String, String[]> args) {
        EntityManager em = getEntityManager();
        System.out.println("args = "+args);
        try{
            Query q;
            if(args.get("statut") != null && args.get("statut")[0].equals("in")){
                String statut = " not(p.libelle = :statut) ";
                if(args.get("dateD") != null && args.get("dateD").length>0 && !args.get("dateD")[0].equalsIgnoreCase("null")){
                    String dateD = " p.create_at >= :dateD";
                    if(args.get("dateF") != null && args.get("dateF").length>0 && !args.get("dateF")[0].equalsIgnoreCase("null")){
                        String dateF = " p.create_at <= :dateF";
                            String origin = " p.refTypeparc.refTypeparc = :origine ";       
                            q = em.createQuery("from Parc p where p.visible = 'T' and "+origin+" and (p.create_at between :dateD and :dateF) and "+statut, Parc.class);       
                            q.setParameter("dateF", new Date(Long.parseLong(args.get("dateF")[0])));
                            q.setParameter("dateD", new Date(Long.parseLong(args.get("dateD")[0])));
                            q.setParameter("statut", Parc.MVT_MISE_PRODUCTION);
                            q.setParameter("origine", args.get("origine")[0]);
                    }else{
                            String origin = " p.refTypeparc.refTypeparc = :origine ";       
                            q = em.createQuery("from Parc p where p.visible = 'T' and "+origin+" and "+dateD+" and "+statut, Parc.class);       
                            q.setParameter("dateD", new Date(Long.parseLong(args.get("dateD")[0])));
                            q.setParameter("statut", Parc.MVT_MISE_PRODUCTION);
                            q.setParameter("origine", args.get("origine")[0]);
                    }
                } else{
                    if(args.get("dateF") != null && args.get("dateF").length>0 && !args.get("dateF")[0].equalsIgnoreCase("null")){
                        String dateF = " p.create_at <= :dateF";
                            String origin = " p.refTypeparc.refTypeparc = :origine ";       
                            q = em.createQuery("from Parc p where p.visible = 'T' and "+origin+" and "+dateF+" and "+statut, Parc.class);       
                            q.setParameter("dateF", new Date(Long.parseLong(args.get("dateF")[0])));
                            q.setParameter("statut", Parc.MVT_MISE_PRODUCTION);
                            q.setParameter("origine", args.get("origine")[0]);
                    }else{
                            String origin = " p.refTypeparc.refTypeparc = :origine ";       
                            q = em.createQuery("from Parc p where p.visible = 'T' and "+origin+" and "+statut, Parc.class);
                            q.setParameter("statut", Parc.MVT_MISE_PRODUCTION);
                            q.setParameter("origine", args.get("origine")[0]);
                    }

                }
            }else if(args.get("statut") != null && args.get("statut")[0].equals("out")){
                String statut = " p.libelle = :statut ";
                if(args.get("dateD") != null && args.get("dateD").length>0 && !args.get("dateD")[0].equalsIgnoreCase("null")){
                    String dateD = " p.create_at >= :dateD";
                    if(args.get("dateF") != null && args.get("dateF").length>0 && !args.get("dateF")[0].equalsIgnoreCase("null")){
                        String dateF = " p.create_at <= :dateF";
                            String origin = " p.refTypeparc.refTypeparc = :origine ";       
                            q = em.createQuery("from Parc p where p.visible = 'T' and "+statut+" and "+origin+" and (p.create_at between :dateD and :dateF) ", Parc.class);       
                            q.setParameter("dateF", new Date(Long.parseLong(args.get("dateF")[0])));
                            q.setParameter("dateD", new Date(Long.parseLong(args.get("dateD")[0])));
                            q.setParameter("statut", Parc.MVT_MISE_PRODUCTION); 
                            q.setParameter("origine", args.get("origine")[0]);
                    }else{
                            String origin = " p.refTypeparc.refTypeparc = :origine ";       
                            q = em.createQuery("from Parc p where p.visible = 'T' and "+origin+" and "+dateD+" and "+statut, Parc.class);       
                            q.setParameter("dateD", new Date(Long.parseLong(args.get("dateD")[0])));
                            q.setParameter("statut", Parc.MVT_MISE_PRODUCTION);
                            q.setParameter("origine", args.get("origine")[0]);
                    }
                } else{
                    if(args.get("dateF") != null && args.get("dateF").length>0 && !args.get("dateF")[0].equalsIgnoreCase("null")){
                        String dateF = " p.create_at <= :dateF";
                            String origin = " p.refTypeparc.refTypeparc = :origine ";       
                            q = em.createQuery("from Parc p where p.visible = 'T' and "+origin+" and "+dateF+" and "+statut, Parc.class);       
                            q.setParameter("dateF", new Date(Long.parseLong(args.get("dateF")[0])));
                            q.setParameter("statut", Parc.MVT_MISE_PRODUCTION);
                            q.setParameter("origine", args.get("origine")[0]);
                    }else{
                            String origin = " p.refTypeparc.refTypeparc = :origine ";       
                            q = em.createQuery("from Parc p where p.visible = 'T' and "+origin+" and "+statut, Parc.class);
                            q.setParameter("statut", Parc.MVT_MISE_PRODUCTION);
                            q.setParameter("origine", args.get("origine")[0]);
                    }

                }

            } else {
                System.out.println("not reachable"); 
                return allBilleInParc();
            }
            System.out.println("Query = "+q);
            return q.getResultList();
        }catch(Exception e){
            System.out.println(e.getMessage()); 
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    
    }

    public List<Parc> allBilleInParc() {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select parc2 from Parc parc2 where parc2.visible = 'T' and parc2.numBille not in "
                    + "(select parc1.numBille from Parc parc1, DetailReception drecept where parc1.numBille = drecept.numBille and drecept.numtravail <> null "
                    + " GROUP BY parc1.numBille HAVING COUNT(parc1.numBille)>=0 )"
                    );
            
            return q.getResultList();
        }finally{
            em.close();
        }
    }

    public List<Parc> getParcsByBille(String bille) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("SELECT p from Parc p where p.numBille = :bille");
            q.setParameter("bille", bille);
            return q.getResultList();
        }finally{
            em.close();
        }
    }

    public String getRecentlyOut() {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("Select dr.numBille, dr.numtravail from DetailReception dr where dr.numtravail is not null and dr.numBille not in (Select blng.numBille from Billonnage blng)");
            return new JSONArray(q.getResultList()).toString();
        }finally{
            em.close();
        }
    }
    
}
