/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.models.Massicot;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Massicotpaquet;
import app.modelController.exceptions.IllegalOrphanException;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class MassicotJpaController implements Serializable {

    public MassicotJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Massicot massicot) throws PreexistingEntityException, Exception {
        if (massicot.getMassicotpaquetList() == null) {
            massicot.setMassicotpaquetList(new ArrayList<Massicotpaquet>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Massicotpaquet> attachedMassicotpaquetList = new ArrayList<Massicotpaquet>();
            for (Massicotpaquet massicotpaquetListMassicotpaquetToAttach : massicot.getMassicotpaquetList()) {
                massicotpaquetListMassicotpaquetToAttach = em.getReference(massicotpaquetListMassicotpaquetToAttach.getClass(), massicotpaquetListMassicotpaquetToAttach.getRefMassicotpaquet());
                attachedMassicotpaquetList.add(massicotpaquetListMassicotpaquetToAttach);
            }
            massicot.setMassicotpaquetList(attachedMassicotpaquetList);
            em.persist(massicot);
            for (Massicotpaquet massicotpaquetListMassicotpaquet : massicot.getMassicotpaquetList()) {
                Massicot oldRefMassicotOfMassicotpaquetListMassicotpaquet = massicotpaquetListMassicotpaquet.getRefMassicot();
                massicotpaquetListMassicotpaquet.setRefMassicot(massicot);
                massicotpaquetListMassicotpaquet = em.merge(massicotpaquetListMassicotpaquet);
                if (oldRefMassicotOfMassicotpaquetListMassicotpaquet != null) {
                    oldRefMassicotOfMassicotpaquetListMassicotpaquet.getMassicotpaquetList().remove(massicotpaquetListMassicotpaquet);
                    oldRefMassicotOfMassicotpaquetListMassicotpaquet = em.merge(oldRefMassicotOfMassicotpaquetListMassicotpaquet);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMassicot(massicot.getRefMassicot()) != null) {
                throw new PreexistingEntityException("Massicot " + massicot + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Massicot massicot) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Massicot persistentMassicot = em.find(Massicot.class, massicot.getRefMassicot());
            List<Massicotpaquet> massicotpaquetListOld = persistentMassicot.getMassicotpaquetList();
            List<Massicotpaquet> massicotpaquetListNew = massicot.getMassicotpaquetList();
            List<String> illegalOrphanMessages = null;
            for (Massicotpaquet massicotpaquetListOldMassicotpaquet : massicotpaquetListOld) {
                if (!massicotpaquetListNew.contains(massicotpaquetListOldMassicotpaquet)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Massicotpaquet " + massicotpaquetListOldMassicotpaquet + " since its refMassicot field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Massicotpaquet> attachedMassicotpaquetListNew = new ArrayList<Massicotpaquet>();
            for (Massicotpaquet massicotpaquetListNewMassicotpaquetToAttach : massicotpaquetListNew) {
                massicotpaquetListNewMassicotpaquetToAttach = em.getReference(massicotpaquetListNewMassicotpaquetToAttach.getClass(), massicotpaquetListNewMassicotpaquetToAttach.getRefMassicotpaquet());
                attachedMassicotpaquetListNew.add(massicotpaquetListNewMassicotpaquetToAttach);
            }
            massicotpaquetListNew = attachedMassicotpaquetListNew;
            massicot.setMassicotpaquetList(massicotpaquetListNew);
            massicot = em.merge(massicot);
            for (Massicotpaquet massicotpaquetListNewMassicotpaquet : massicotpaquetListNew) {
                if (!massicotpaquetListOld.contains(massicotpaquetListNewMassicotpaquet)) {
                    Massicot oldRefMassicotOfMassicotpaquetListNewMassicotpaquet = massicotpaquetListNewMassicotpaquet.getRefMassicot();
                    massicotpaquetListNewMassicotpaquet.setRefMassicot(massicot);
                    massicotpaquetListNewMassicotpaquet = em.merge(massicotpaquetListNewMassicotpaquet);
                    if (oldRefMassicotOfMassicotpaquetListNewMassicotpaquet != null && !oldRefMassicotOfMassicotpaquetListNewMassicotpaquet.equals(massicot)) {
                        oldRefMassicotOfMassicotpaquetListNewMassicotpaquet.getMassicotpaquetList().remove(massicotpaquetListNewMassicotpaquet);
                        oldRefMassicotOfMassicotpaquetListNewMassicotpaquet = em.merge(oldRefMassicotOfMassicotpaquetListNewMassicotpaquet);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = massicot.getRefMassicot();
                if (findMassicot(id) == null) {
                    throw new NonexistentEntityException("The massicot with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Massicot massicot;
            try {
                massicot = em.getReference(Massicot.class, id);
                massicot.getRefMassicot();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The massicot with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Massicotpaquet> massicotpaquetListOrphanCheck = massicot.getMassicotpaquetList();
            for (Massicotpaquet massicotpaquetListOrphanCheckMassicotpaquet : massicotpaquetListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Massicot (" + massicot + ") cannot be destroyed since the Massicotpaquet " + massicotpaquetListOrphanCheckMassicotpaquet + " in its massicotpaquetList field has a non-nullable refMassicot field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(massicot);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Massicot> findMassicotEntities() {
        return findMassicotEntities(true, -1, -1);
    }

    public List<Massicot> findMassicotEntities(int maxResults, int firstResult) {
        return findMassicotEntities(false, maxResults, firstResult);
    }

    private List<Massicot> findMassicotEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Massicot.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Massicot findMassicot(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Massicot.class, id);
        } finally {
            em.close();
        }
    }

    public int getMassicotCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Massicot> rt = cq.from(Massicot.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
