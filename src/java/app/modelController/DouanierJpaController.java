/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.Douanier;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author champlain
 */
public class DouanierJpaController implements Serializable {

    public DouanierJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Douanier douanier) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(douanier);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDouanier(douanier.getMatriculeDouanier()) != null) {
                throw new PreexistingEntityException("Douanier " + douanier + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Douanier douanier) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            douanier = em.merge(douanier);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = douanier.getMatriculeDouanier();
                if (findDouanier(id) == null) {
                    throw new NonexistentEntityException("The douanier with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Douanier douanier;
            try {
                douanier = em.getReference(Douanier.class, id);
                douanier.getMatriculeDouanier();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The douanier with id " + id + " no longer exists.", enfe);
            }
            em.remove(douanier);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Douanier> findDouanierEntities() {
        return findDouanierEntities(true, -1, -1);
    }

    public List<Douanier> findDouanierEntities(int maxResults, int firstResult) {
        return findDouanierEntities(false, maxResults, firstResult);
    }

    private List<Douanier> findDouanierEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Douanier.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Douanier findDouanier(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Douanier.class, id);
        } finally {
            em.close();
        }
    }

    public int getDouanierCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Douanier> rt = cq.from(Douanier.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
