/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.Autorization;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Route;
import java.util.ArrayList;
import java.util.List;
import app.models.Personnel;
import app.models.Privilege;
import java.util.LinkedList;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class PrivilegeJpaController implements Serializable {

    public PrivilegeJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Privilege privilege) throws PreexistingEntityException, Exception {
        if (privilege.getRouteList() == null) {
            privilege.setRouteList(new ArrayList<Route>());
        }
        if (privilege.getPersonnelList() == null) {
            privilege.setPersonnelList(new ArrayList<Personnel>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Route> attachedRouteList = new ArrayList<Route>();
            for (Route routeListRouteToAttach : privilege.getRouteList()) {
                routeListRouteToAttach = em.getReference(routeListRouteToAttach.getClass(), routeListRouteToAttach.getNom());
                attachedRouteList.add(routeListRouteToAttach);
            }
            privilege.setRouteList(attachedRouteList);
            List<Personnel> attachedPersonnelList = new ArrayList<Personnel>();
            for (Personnel personnelListPersonnelToAttach : privilege.getPersonnelList()) {
                personnelListPersonnelToAttach = em.getReference(personnelListPersonnelToAttach.getClass(), personnelListPersonnelToAttach.getCniPersonnel());
                attachedPersonnelList.add(personnelListPersonnelToAttach);
            }
            privilege.setPersonnelList(attachedPersonnelList);
            em.persist(privilege);
            for (Route routeListRoute : privilege.getRouteList()) {
                routeListRoute.getPrivilegeList().add(privilege);
                routeListRoute = em.merge(routeListRoute);
            }
            for (Personnel personnelListPersonnel : privilege.getPersonnelList()) {
                Privilege oldFonctionOfPersonnelListPersonnel = personnelListPersonnel.getFonction();
                personnelListPersonnel.setFonction(privilege);
                personnelListPersonnel = em.merge(personnelListPersonnel);
                if (oldFonctionOfPersonnelListPersonnel != null) {
                    oldFonctionOfPersonnelListPersonnel.getPersonnelList().remove(personnelListPersonnel);
                    oldFonctionOfPersonnelListPersonnel = em.merge(oldFonctionOfPersonnelListPersonnel);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPrivilege(privilege.getRefPrivilege()) != null) {
                throw new PreexistingEntityException("Privilege " + privilege + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Privilege privilege) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Privilege persistentPrivilege = em.find(Privilege.class, privilege.getRefPrivilege());
            List<Route> routeListOld = persistentPrivilege.getRouteList();
            List<Route> routeListNew = privilege.getRouteList();
            List<Personnel> personnelListOld = persistentPrivilege.getPersonnelList();
            List<Personnel> personnelListNew = privilege.getPersonnelList();
            List<Route> attachedRouteListNew = new ArrayList<Route>();
            for (Route routeListNewRouteToAttach : routeListNew) {
                routeListNewRouteToAttach = em.getReference(routeListNewRouteToAttach.getClass(), routeListNewRouteToAttach.getNom());
                attachedRouteListNew.add(routeListNewRouteToAttach);
            }
            routeListNew = attachedRouteListNew;
            privilege.setRouteList(routeListNew);
            List<Personnel> attachedPersonnelListNew = new ArrayList<Personnel>();
            for (Personnel personnelListNewPersonnelToAttach : personnelListNew) {
                personnelListNewPersonnelToAttach = em.getReference(personnelListNewPersonnelToAttach.getClass(), personnelListNewPersonnelToAttach.getCniPersonnel());
                attachedPersonnelListNew.add(personnelListNewPersonnelToAttach);
            }
            personnelListNew = attachedPersonnelListNew;
            privilege.setPersonnelList(personnelListNew);
            privilege = em.merge(privilege);
            for (Route routeListOldRoute : routeListOld) {
                if (!routeListNew.contains(routeListOldRoute)) {
                    routeListOldRoute.getPrivilegeList().remove(privilege);
                    routeListOldRoute = em.merge(routeListOldRoute);
                }
            }
            for (Route routeListNewRoute : routeListNew) {
                if (!routeListOld.contains(routeListNewRoute)) {
                    routeListNewRoute.getPrivilegeList().add(privilege);
                    routeListNewRoute = em.merge(routeListNewRoute);
                }
            }
            for (Personnel personnelListOldPersonnel : personnelListOld) {
                if (!personnelListNew.contains(personnelListOldPersonnel)) {
                    personnelListOldPersonnel.setFonction(null);
                    personnelListOldPersonnel = em.merge(personnelListOldPersonnel);
                }
            }
            for (Personnel personnelListNewPersonnel : personnelListNew) {
                if (!personnelListOld.contains(personnelListNewPersonnel)) {
                    Privilege oldFonctionOfPersonnelListNewPersonnel = personnelListNewPersonnel.getFonction();
                    personnelListNewPersonnel.setFonction(privilege);
                    personnelListNewPersonnel = em.merge(personnelListNewPersonnel);
                    if (oldFonctionOfPersonnelListNewPersonnel != null && !oldFonctionOfPersonnelListNewPersonnel.equals(privilege)) {
                        oldFonctionOfPersonnelListNewPersonnel.getPersonnelList().remove(personnelListNewPersonnel);
                        oldFonctionOfPersonnelListNewPersonnel = em.merge(oldFonctionOfPersonnelListNewPersonnel);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = privilege.getRefPrivilege();
                if (findPrivilege(id) == null) {
                    throw new NonexistentEntityException("The privilege with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Privilege privilege;
            try {
                privilege = em.getReference(Privilege.class, id);
                privilege.getRefPrivilege();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The privilege with id " + id + " no longer exists.", enfe);
            }
            List<Route> routeList = privilege.getRouteList();
            for (Route routeListRoute : routeList) {
                routeListRoute.getPrivilegeList().remove(privilege);
                routeListRoute = em.merge(routeListRoute);
            }
            List<Personnel> personnelList = privilege.getPersonnelList();
            for (Personnel personnelListPersonnel : personnelList) {
                personnelListPersonnel.setFonction(null);
                personnelListPersonnel = em.merge(personnelListPersonnel);
            }
            em.remove(privilege);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Privilege> findPrivilegeEntities() {
        return findPrivilegeEntities(true, -1, -1);
    }

    public List<Privilege> findPrivilegeEntities(int maxResults, int firstResult) {
        return findPrivilegeEntities(false, maxResults, firstResult);
    }

    private List<Privilege> findPrivilegeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Privilege.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Privilege findPrivilege(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Privilege.class, id);
        } finally {
            em.close();
        }
    }

    public int getPrivilegeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Privilege> rt = cq.from(Privilege.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    

    public List<Route> getRouteForPrivilege(String refPrivilege) {
        LinkedList<Route> listeRoute = new LinkedList<Route>();
        EntityManager em = getEntityManager();
        RouteJpaController contRoute = new RouteJpaController(em.getEntityManagerFactory());
        try{
            Query q = em.createNamedQuery("Autorization.findByPrivilege");
            q.setParameter("privilege", refPrivilege);
            List<Autorization> l = q.getResultList();
            l.forEach((el) -> {
                listeRoute.add(contRoute.findRoute(el.getAutorizationPK().getRoute()));
            });
        }finally{
            em.close();
        }
        
        return listeRoute;
    }
    
    
}
