/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.models.Rebut;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.Billon;
import app.models.Database;
import app.models.Plot;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author champlain
 */
public class RebutJpaController implements Serializable {

    public static void setRebut(Class type, String ref) {
        try {
            Rebut r = new Rebut();
            r.setDateRebut(new Date());
            r.setRefRebut(new Date().getTime()+"");
            r.setTypeRebut(type.getSimpleName());
            r.setRef(ref);
            new RebutJpaController(Database.getEntityManager()).create(r);
            if(type == Billon.class){
                BillonJpaController cont = new BillonJpaController(Database.getEntityManager());
                Billon b = cont.findBillon(ref);
                b.setRebut();
                cont.edit(b);
            }else if(type == Plot.class){
                PlotJpaController cont = new PlotJpaController(Database.getEntityManager());
                Plot b = cont.findPlot(ref);
                b.setRebut();
                cont.edit(b);
            }
        } catch (Exception ex) {

        }
    }

    public static void unSetRebut(Class type, String objectRef, String ref) {
        try {
            if(type == Billon.class){
                BillonJpaController cont = new BillonJpaController(Database.getEntityManager());
                Billon b = cont.findBillon(objectRef);
                b.unSetRebut();
                cont.edit(b);
            }else if(type == Plot.class){
                PlotJpaController cont = new PlotJpaController(Database.getEntityManager());
                Plot b = cont.findPlot(objectRef);
                b.unSetRebut();
                cont.edit(b);
            }
            new RebutJpaController(Database.getEntityManager()).destroy(ref);
        } catch (Exception ex) {

        }
    }

    public RebutJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Rebut rebut) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(rebut);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findRebut(rebut.getRefRebut()) != null) {
                throw new PreexistingEntityException("Rebut " + rebut + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Rebut rebut) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            rebut = em.merge(rebut);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = rebut.getRefRebut();
                if (findRebut(id) == null) {
                    throw new NonexistentEntityException("The rebut with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Rebut rebut;
            try {
                rebut = em.getReference(Rebut.class, id);
                rebut.getRefRebut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The rebut with id " + id + " no longer exists.", enfe);
            }
            em.remove(rebut);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Rebut> findRebutEntities() {
        return findRebutEntities(true, -1, -1);
    }

    public List<Rebut> findRebutEntities(int maxResults, int firstResult) {
        return findRebutEntities(false, maxResults, firstResult);
    }

    private List<Rebut> findRebutEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Rebut.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Rebut findRebut(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Rebut.class, id);
        } finally {
            em.close();
        }
    }

    public int getRebutCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Rebut> rt = cq.from(Rebut.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
