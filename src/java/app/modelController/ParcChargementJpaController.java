/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Fournisseur;
import app.models.ParcChargement;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.Commande;
import app.models.Database;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;

/**
 *
 * @author champlain
 */
public class ParcChargementJpaController implements Serializable {

    public static List<ParcChargement> byFournisseur(String refFournisseur) {
        try{
            EntityManager em = Database.getEntityManager().createEntityManager();
            Query q = em.createQuery("from ParcChargement cmd where cmd.refFournisseur.refFournisseur = :fournisseur", ParcChargement.class);
            q.setParameter("fournisseur", refFournisseur);
            
            return q.getResultList();
        }catch(Exception e){
            return new LinkedList<>();
        }
    }

    public static ParcChargement byCommande(String commande) {
        try{
            EntityManager em = Database.getEntityManager().createEntityManager();
            Query q = em.createQuery("select p from ParcChargement p, Commande cmd where cmd.refCommande = :commande and cmd.refParc.refParc = p.refParc");
            q.setParameter("commande", commande);
            System.out.print(q.toString());
            return (ParcChargement) q.getSingleResult();
        }catch(Exception e){
            return null;
        }
    }

    public ParcChargementJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ParcChargement parcChargement) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Fournisseur refFournisseur = parcChargement.getRefFournisseur();
            if (refFournisseur != null) {
                refFournisseur = em.getReference(refFournisseur.getClass(), refFournisseur.getRefFournisseur());
                parcChargement.setRefFournisseur(refFournisseur);
            }
            em.persist(parcChargement);
            if (refFournisseur != null) {
                refFournisseur.getParcChargementList().add(parcChargement);
                refFournisseur = em.merge(refFournisseur);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findParcChargement(parcChargement.getRefParc()) != null) {
                throw new PreexistingEntityException("ParcChargement " + parcChargement + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ParcChargement parcChargement) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ParcChargement persistentParcChargement = em.find(ParcChargement.class, parcChargement.getRefParc());
            Fournisseur refFournisseurOld = persistentParcChargement.getRefFournisseur();
            Fournisseur refFournisseurNew = parcChargement.getRefFournisseur();
            if (refFournisseurNew != null) {
                refFournisseurNew = em.getReference(refFournisseurNew.getClass(), refFournisseurNew.getRefFournisseur());
                parcChargement.setRefFournisseur(refFournisseurNew);
            }
            parcChargement = em.merge(parcChargement);
            if (refFournisseurOld != null && !refFournisseurOld.equals(refFournisseurNew)) {
                refFournisseurOld.getParcChargementList().remove(parcChargement);
                refFournisseurOld = em.merge(refFournisseurOld);
            }
            if (refFournisseurNew != null && !refFournisseurNew.equals(refFournisseurOld)) {
                refFournisseurNew.getParcChargementList().add(parcChargement);
                refFournisseurNew = em.merge(refFournisseurNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = parcChargement.getRefParc();
                if (findParcChargement(id) == null) {
                    throw new NonexistentEntityException("The parcChargement with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ParcChargement parcChargement;
            try {
                parcChargement = em.getReference(ParcChargement.class, id);
                parcChargement.getRefParc();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The parcChargement with id " + id + " no longer exists.", enfe);
            }
            Fournisseur refFournisseur = parcChargement.getRefFournisseur();
            if (refFournisseur != null) {
                refFournisseur.getParcChargementList().remove(parcChargement);
                refFournisseur = em.merge(refFournisseur);
            }
            em.remove(parcChargement);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ParcChargement> findParcChargementEntities() {
        return findParcChargementEntities(true, -1, -1);
    }

    public List<ParcChargement> findParcChargementEntities(int maxResults, int firstResult) {
        return findParcChargementEntities(false, maxResults, firstResult);
    }

    private List<ParcChargement> findParcChargementEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery(ParcChargement.class);
            Root root = cq.from(ParcChargement.class);
            //root.fetch("titreList", JoinType.INNER);
            cq.select(root);
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            List l = q.getResultList();
            return  l;
        } finally {
            em.close();
        }
    }

    public ParcChargement findParcChargement(String id) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery(ParcChargement.class);
            Root root = cq.from(ParcChargement.class);
            root.fetch("titreList");
            Predicate p1 = cb.equal(root.get("refParc"), id);
            //cq.where(p1);
            cq.select(root);
            Query q = em.createQuery(cq);
            
            List<ParcChargement> list = q.getResultList();
            for(ParcChargement pc : list)
                if(pc.getRefParc().equalsIgnoreCase(id))
                    return pc;
            return null;
            //ResultList();
        } finally {
            em.close();
        }
    }

    public int getParcChargementCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ParcChargement> rt = cq.from(ParcChargement.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

        public List<ParcChargement> findParcChargementByFournisseur(String refFournisseur) {
            EntityManager em = getEntityManager();
            List liste =  new LinkedList();
            try {
                Query q = em.createQuery("from ParcChargement pc where pc.refFournisseur.refFournisseur = :refFournisseur");
                q.setParameter("refFournisseur", refFournisseur);
                liste = q.getResultList();
            }catch(Exception e){} finally {
                em.close();
            }
            return liste;
        }

    ParcChargement findParcChargementForColis(String refColis, String essence) {
        String numTravail = new PaquetJpaController(Database.getEntityManager()).findNumTravailFromPaquetforColis(refColis);   
        if(numTravail == null) return null;
        return new CommandeJpaController(Database.getEntityManager()).findCommandeForNumTravail(numTravail);
    }

}
