/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.models.Sechoir;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author champlain
 */
public class SechoirJpaController implements Serializable {

    public SechoirJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Sechoir sechoir) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(sechoir);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findSechoir(sechoir.getRefSechoir()) != null) {
                throw new PreexistingEntityException("Sechoir " + sechoir + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Sechoir sechoir) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            sechoir = em.merge(sechoir);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = sechoir.getRefSechoir();
                if (findSechoir(id) == null) {
                    throw new NonexistentEntityException("The sechoir with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sechoir sechoir;
            try {
                sechoir = em.getReference(Sechoir.class, id);
                sechoir.getRefSechoir();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The sechoir with id " + id + " no longer exists.", enfe);
            }
            em.remove(sechoir);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Sechoir> findSechoirEntities() {
        return findSechoirEntities(true, -1, -1);
    }

    public List<Sechoir> findSechoirEntities(int maxResults, int firstResult) {
        return findSechoirEntities(false, maxResults, firstResult);
    }

    private List<Sechoir> findSechoirEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Sechoir.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Sechoir findSechoir(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Sechoir.class, id);
        } finally {
            em.close();
        }
    }

    public int getSechoirCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Sechoir> rt = cq.from(Sechoir.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
