/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Essence;
import app.models.Titre;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.JoinType;

/**
 *
 * @author champlain
 */
public class TitreJpaController implements Serializable {

    public TitreJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Titre titre) throws PreexistingEntityException, Exception {
        if (titre.getEssenceList() == null) {
            titre.setEssenceList(new ArrayList<Essence>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Essence> attachedEssenceList = new ArrayList<Essence>();
            for (Essence essenceListEssenceToAttach : titre.getEssenceList()) {
                essenceListEssenceToAttach = em.getReference(essenceListEssenceToAttach.getClass(), essenceListEssenceToAttach.getRefEssence());
                attachedEssenceList.add(essenceListEssenceToAttach);
            }
            titre.setEssenceList(attachedEssenceList);
            em.persist(titre);
            for (Essence essenceListEssence : titre.getEssenceList()) {
                essenceListEssence.getTitreList().add(titre);
                essenceListEssence = em.merge(essenceListEssence);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTitre(titre.getNumTitre()) != null) {
                throw new PreexistingEntityException("Titre " + titre + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Titre titre) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Titre persistentTitre = em.find(Titre.class, titre.getNumTitre());
            List<Essence> essenceListOld = persistentTitre.getEssenceList();
            List<Essence> essenceListNew = titre.getEssenceList();
            List<Essence> attachedEssenceListNew = new ArrayList<Essence>();
            for (Essence essenceListNewEssenceToAttach : essenceListNew) {
                essenceListNewEssenceToAttach = em.getReference(essenceListNewEssenceToAttach.getClass(), essenceListNewEssenceToAttach.getRefEssence());
                attachedEssenceListNew.add(essenceListNewEssenceToAttach);
            }
            essenceListNew = attachedEssenceListNew;
            titre.setEssenceList(essenceListNew);
            titre = em.merge(titre);
            for (Essence essenceListOldEssence : essenceListOld) {
                if (!essenceListNew.contains(essenceListOldEssence)) {
                    essenceListOldEssence.getTitreList().remove(titre);
                    essenceListOldEssence = em.merge(essenceListOldEssence);
                }
            }
            for (Essence essenceListNewEssence : essenceListNew) {
                if (!essenceListOld.contains(essenceListNewEssence)) {
                    essenceListNewEssence.getTitreList().add(titre);
                    essenceListNewEssence = em.merge(essenceListNewEssence);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = titre.getNumTitre();
                if (findTitre(id) == null) {
                    throw new NonexistentEntityException("The titre with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Titre titre;
            try {
                titre = em.getReference(Titre.class, id);
                titre.getNumTitre();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The titre with id " + id + " no longer exists.", enfe);
            }
            List<Essence> essenceList = titre.getEssenceList();
            for (Essence essenceListEssence : essenceList) {
                essenceListEssence.getTitreList().remove(titre);
                essenceListEssence = em.merge(essenceListEssence);
            }
            em.remove(titre);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Titre> findTitreEntities() {
        return findTitreEntities(true, -1, -1);
    }

    public List<Titre> findTitreEntities(int maxResults, int firstResult) {
        return findTitreEntities(false, maxResults, firstResult);
    }

    private List<Titre> findTitreEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery(Titre.class);
            Root root = cq.from(Titre.class);
            root.fetch("essenceList", JoinType.INNER);
            cq.select(root);
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Titre findTitre(String id) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery(Titre.class);
            Root root = cq.from(Titre.class);
            root.fetch("essenceList");
            cq.select(root);
            cq.where(cb.equal(root.get("numTitre"), id));
            Query q = em.createQuery(cq);
            List<Titre> list = q.getResultList();
            return list.size() > 0 ? list.get(list.size()-1) : null;
        } finally {
            em.close();
        }
    }

    public int getTitreCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Titre> rt = cq.from(Titre.class); 
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<Titre> findTitreByRefParc(String refParc) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("SELECT t FROM Titre t WHERE t.refParc.refParc = :refParc");
            q.setParameter("refParc", refParc);
            System.out.println("q");
            return q.getResultList();
        }finally{
            em.close();
        }
    }

    public Titre findLastTitreByRefParc(String refParc) {
        EntityManager em = getEntityManager();
            try{
                Query q = em.createQuery("SELECT t FROM Titre t WHERE t.refParc.refParc = :refParc");
                q.setParameter("refParc", refParc);
            List<Titre> l = q.getResultList();
                if(l != null && l.size()>0){
                    return l.get(l.size()-1);
                }else{
                    return null;
                }
            }finally{
                em.close();
            }
    }

    public List<Titre> getTitreFor(String refFournisseur) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("Select t from Titre t, ParcChargement pc where t.refParc.refParc = pc.refParc and pc.refFournisseur.refFournisseur = :fournisseur and t.dateFin >= :date");
            q.setParameter("fournisseur", refFournisseur);
            q.setParameter("date", new Date());
            return q.getResultList();
        }finally{
            em.close();
        }
    }
    
}
