/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.TrancheMachine;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author champlain
 */
public class TrancheMachineJpaController implements Serializable {

    public TrancheMachineJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TrancheMachine trancheMachine) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(trancheMachine);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTrancheMachine(trancheMachine.getRefMachine()) != null) {
                throw new PreexistingEntityException("TrancheMachine " + trancheMachine + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TrancheMachine trancheMachine) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            trancheMachine = em.merge(trancheMachine);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = trancheMachine.getRefMachine();
                if (findTrancheMachine(id) == null) {
                    throw new NonexistentEntityException("The trancheMachine with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TrancheMachine trancheMachine;
            try {
                trancheMachine = em.getReference(TrancheMachine.class, id);
                trancheMachine.getRefMachine();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The trancheMachine with id " + id + " no longer exists.", enfe);
            }
            em.remove(trancheMachine);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TrancheMachine> findTrancheMachineEntities() {
        return findTrancheMachineEntities(true, -1, -1);
    }

    public List<TrancheMachine> findTrancheMachineEntities(int maxResults, int firstResult) {
        return findTrancheMachineEntities(false, maxResults, firstResult);
    }

    private List<TrancheMachine> findTrancheMachineEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TrancheMachine.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TrancheMachine findTrancheMachine(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TrancheMachine.class, id);
        } finally {
            em.close();
        }
    }

    public int getTrancheMachineCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TrancheMachine> rt = cq.from(TrancheMachine.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public TrancheMachine findTrancheMachineByName(String tranche) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("from TrancheMachine tm where tm.libelle = :tranche");
            q.setParameter("tranche", tranche);
            return (TrancheMachine) q.getSingleResult();
        } finally {
            em.close();
        }
    }
    
}
