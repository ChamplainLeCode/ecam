/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.DetailLv;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author champlain
 */
public class DetailLvJpaController1 implements Serializable {

    public DetailLvJpaController1(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DetailLv detailLv) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(detailLv);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDetailLv(detailLv.getRefDlv()) != null) {
                throw new PreexistingEntityException("DetailLv " + detailLv + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DetailLv detailLv) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            detailLv = em.merge(detailLv);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = detailLv.getRefDlv();
                if (findDetailLv(id) == null) {
                    throw new NonexistentEntityException("The detailLv with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailLv detailLv;
            try {
                detailLv = em.getReference(DetailLv.class, id);
                detailLv.getRefDlv();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The detailLv with id " + id + " no longer exists.", enfe);
            }
            em.remove(detailLv);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DetailLv> findDetailLvEntities() {
        return findDetailLvEntities(true, -1, -1);
    }

    public List<DetailLv> findDetailLvEntities(int maxResults, int firstResult) {
        return findDetailLvEntities(false, maxResults, firstResult);
    }

    private List<DetailLv> findDetailLvEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DetailLv.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DetailLv findDetailLv(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DetailLv.class, id);
        } finally {
            em.close();
        }
    }

    public int getDetailLvCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DetailLv> rt = cq.from(DetailLv.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
