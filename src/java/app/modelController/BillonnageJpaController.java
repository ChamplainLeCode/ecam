/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Parc;
import app.models.Billon;
import app.models.Billonnage;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class BillonnageJpaController implements Serializable {

    public BillonnageJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Billonnage billonnage) throws PreexistingEntityException, Exception {
        if (billonnage.getBillonList() == null) {
            billonnage.setBillonList(new ArrayList<Billon>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Parc refParc = billonnage.getRefParc();
            if (refParc != null) {
                refParc = em.getReference(refParc.getClass(), refParc.getRefParc());
                billonnage.setRefParc(refParc);
            }
            List<Billon> attachedBillonList = new ArrayList<Billon>();
            for (Billon billonListBillonToAttach : billonnage.getBillonList()) {
                billonListBillonToAttach = em.getReference(billonListBillonToAttach.getClass(), billonListBillonToAttach.getRefBillon());
                attachedBillonList.add(billonListBillonToAttach);
            }
            billonnage.setBillonList(attachedBillonList);
            em.persist(billonnage);
            if (refParc != null) {
                refParc.getBillonnageList().add(billonnage);
                refParc = em.merge(refParc);
            }
            for (Billon billonListBillon : billonnage.getBillonList()) {
                Billonnage oldRefBillonnageOfBillonListBillon = billonListBillon.getRefBillonnage();
                billonListBillon.setRefBillonnage(billonnage);
                billonListBillon = em.merge(billonListBillon);
                if (oldRefBillonnageOfBillonListBillon != null) {
                    oldRefBillonnageOfBillonListBillon.getBillonList().remove(billonListBillon);
                    oldRefBillonnageOfBillonListBillon = em.merge(oldRefBillonnageOfBillonListBillon);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findBillonnage(billonnage.getRefBillonnage()) != null) {
                throw new PreexistingEntityException("Billonnage " + billonnage + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Billonnage billonnage) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Billonnage persistentBillonnage = em.find(Billonnage.class, billonnage.getRefBillonnage());
            Parc refParcOld = persistentBillonnage.getRefParc();
            Parc refParcNew = billonnage.getRefParc();
            List<Billon> billonListOld = persistentBillonnage.getBillonList();
            List<Billon> billonListNew = billonnage.getBillonList();
            if (refParcNew != null) {
                refParcNew = em.getReference(refParcNew.getClass(), refParcNew.getRefParc());
                billonnage.setRefParc(refParcNew);
            }
            List<Billon> attachedBillonListNew = new ArrayList<Billon>();
            for (Billon billonListNewBillonToAttach : billonListNew) {
                billonListNewBillonToAttach = em.getReference(billonListNewBillonToAttach.getClass(), billonListNewBillonToAttach.getRefBillon());
                attachedBillonListNew.add(billonListNewBillonToAttach);
            }
            billonListNew = attachedBillonListNew;
            billonnage.setBillonList(billonListNew);
            billonnage = em.merge(billonnage);
            if (refParcOld != null && !refParcOld.equals(refParcNew)) {
                refParcOld.getBillonnageList().remove(billonnage);
                refParcOld = em.merge(refParcOld);
            }
            if (refParcNew != null && !refParcNew.equals(refParcOld)) {
                refParcNew.getBillonnageList().add(billonnage);
                refParcNew = em.merge(refParcNew);
            }
            for (Billon billonListOldBillon : billonListOld) {
                if (!billonListNew.contains(billonListOldBillon)) {
                    billonListOldBillon.setRefBillonnage(null);
                    billonListOldBillon = em.merge(billonListOldBillon);
                }
            }
            for (Billon billonListNewBillon : billonListNew) {
                if (!billonListOld.contains(billonListNewBillon)) {
                    Billonnage oldRefBillonnageOfBillonListNewBillon = billonListNewBillon.getRefBillonnage();
                    billonListNewBillon.setRefBillonnage(billonnage);
                    billonListNewBillon = em.merge(billonListNewBillon);
                    if (oldRefBillonnageOfBillonListNewBillon != null && !oldRefBillonnageOfBillonListNewBillon.equals(billonnage)) {
                        oldRefBillonnageOfBillonListNewBillon.getBillonList().remove(billonListNewBillon);
                        oldRefBillonnageOfBillonListNewBillon = em.merge(oldRefBillonnageOfBillonListNewBillon);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = billonnage.getRefBillonnage();
                if (findBillonnage(id) == null) {
                    throw new NonexistentEntityException("The billonnage with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Billonnage billonnage;
            try {
                billonnage = em.getReference(Billonnage.class, id);
                billonnage.getRefBillonnage();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The billonnage with id " + id + " no longer exists.", enfe);
            }
            Parc refParc = billonnage.getRefParc();
            if (refParc != null) {
                refParc.getBillonnageList().remove(billonnage);
                refParc = em.merge(refParc);
            }
            List<Billon> billonList = billonnage.getBillonList();
            for (Billon billonListBillon : billonList) {
                billonListBillon.setRefBillonnage(null);
                billonListBillon = em.merge(billonListBillon);
            }
            em.remove(billonnage);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Billonnage> findBillonnageEntities() {
        return findBillonnageEntities(true, -1, -1);
    }

    public List<Billonnage> findBillonnageEntities(int maxResults, int firstResult) {
        return findBillonnageEntities(false, maxResults, firstResult);
    }

    private List<Billonnage> findBillonnageEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Billonnage.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Billonnage findBillonnage(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Billonnage.class, id);
        } finally {
            em.close();
        }
    }

    public int getBillonnageCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Billonnage> rt = cq.from(Billonnage.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public Billonnage findBillonnageByTravail(String travail){
        EntityManager em = getEntityManager();
        try{
            Query q = em.createNamedQuery("Billonnage.findByNumTravail");
            q.setParameter("numTravail", travail);
            List<Billonnage> l = q.getResultList();
            if(l.size()>0)
                return l.get(0);
            return null;
        }finally{
            em.close();
        }
    }
}
