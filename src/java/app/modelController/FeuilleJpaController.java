/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.models.Feuille;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Plot;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class FeuilleJpaController implements Serializable {

    public FeuilleJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Feuille feuille) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Plot refPlot = feuille.getRefPlot();
            if (refPlot != null) {
                refPlot = em.getReference(refPlot.getClass(), refPlot.getRefPlot());
                feuille.setRefPlot(refPlot);
            }
            em.persist(feuille);
            if (refPlot != null) {
                refPlot.getFeuilleList().add(feuille);
                refPlot = em.merge(refPlot);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findFeuille(feuille.getRefFeuille()) != null) {
                throw new PreexistingEntityException("Feuille " + feuille + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Feuille feuille) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Feuille persistentFeuille = em.find(Feuille.class, feuille.getRefFeuille());
            Plot refPlotOld = persistentFeuille.getRefPlot();
            Plot refPlotNew = feuille.getRefPlot();
            if (refPlotNew != null) {
                refPlotNew = em.getReference(refPlotNew.getClass(), refPlotNew.getRefPlot());
                feuille.setRefPlot(refPlotNew);
            }
            feuille = em.merge(feuille);
            if (refPlotOld != null && !refPlotOld.equals(refPlotNew)) {
                refPlotOld.getFeuilleList().remove(feuille);
                refPlotOld = em.merge(refPlotOld);
            }
            if (refPlotNew != null && !refPlotNew.equals(refPlotOld)) {
                refPlotNew.getFeuilleList().add(feuille);
                refPlotNew = em.merge(refPlotNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = feuille.getRefFeuille();
                if (findFeuille(id) == null) {
                    throw new NonexistentEntityException("The feuille with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Feuille feuille;
            try {
                feuille = em.getReference(Feuille.class, id);
                feuille.getRefFeuille();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The feuille with id " + id + " no longer exists.", enfe);
            }
            Plot refPlot = feuille.getRefPlot();
            if (refPlot != null) {
                refPlot.getFeuilleList().remove(feuille);
                refPlot = em.merge(refPlot);
            }
            em.remove(feuille);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Feuille> findFeuilleEntities() {
        return findFeuilleEntities(true, -1, -1);
    }

    public List<Feuille> findFeuilleEntities(int maxResults, int firstResult) {
        return findFeuilleEntities(false, maxResults, firstResult);
    }

    private List<Feuille> findFeuilleEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Feuille.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Feuille findFeuille(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Feuille.class, id);
        } finally {
            em.close();
        }
    }

    public int getFeuilleCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Feuille> rt = cq.from(Feuille.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
