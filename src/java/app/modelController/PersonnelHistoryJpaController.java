/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.PersonnelHistory;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author champlain
 */
public class PersonnelHistoryJpaController implements Serializable {

    public PersonnelHistoryJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PersonnelHistory personnelHistory) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(personnelHistory);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPersonnelHistory(personnelHistory.getDateOperation()) != null) {
                throw new PreexistingEntityException("PersonnelHistory " + personnelHistory + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PersonnelHistory personnelHistory) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            personnelHistory = em.merge(personnelHistory);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Date id = personnelHistory.getDateOperation();
                if (findPersonnelHistory(id) == null) {
                    throw new NonexistentEntityException("The personnelHistory with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Date id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PersonnelHistory personnelHistory;
            try {
                personnelHistory = em.getReference(PersonnelHistory.class, id);
                personnelHistory.getDateOperation();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The personnelHistory with id " + id + " no longer exists.", enfe);
            }
            em.remove(personnelHistory);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PersonnelHistory> findPersonnelHistoryEntities() {
        return findPersonnelHistoryEntities(true, -1, -1);
    }

    public List<PersonnelHistory> findPersonnelHistoryEntities(int maxResults, int firstResult) {
        return findPersonnelHistoryEntities(false, maxResults, firstResult);
    }

    private List<PersonnelHistory> findPersonnelHistoryEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(PersonnelHistory.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PersonnelHistory findPersonnelHistory(Date id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PersonnelHistory.class, id);
        } finally {
            em.close();
        }
    }

    public int getPersonnelHistoryCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<PersonnelHistory> rt = cq.from(PersonnelHistory.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<PersonnelHistory> findPersonnelHistoryByPersonnel(String matricule) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createNamedQuery("PersonnelHistory.findByMatricule", PersonnelHistory.class);
            q.setParameter("matricule", matricule);
            return q.getResultList();
        }finally{
            em.close();
        }
    }
    
}
