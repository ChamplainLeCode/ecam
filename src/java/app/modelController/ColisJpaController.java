/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.modelController.exceptions.IllegalOrphanException;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.Colis;
import app.models.Database;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Equipe;
import app.models.Embarquement;
import java.util.ArrayList;
import java.util.List;
import app.models.Empotage;
import app.models.Jointage;
import app.models.Paquet;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
public class ColisJpaController implements Serializable {


    public ColisJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
   private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }


    public void create(Colis colis) throws PreexistingEntityException, Exception {
        if (colis.getEmpotageList() == null) {
            colis.setEmpotageList(new ArrayList<Empotage>());
        }
        if (colis.getPaquetList() == null) {
            colis.setPaquetList(new ArrayList<Paquet>());
        }
        if (colis.getEmbarquementList() == null) {
            colis.setEmbarquementList(new ArrayList<Embarquement>());
        }
        if (colis.getJointageList() == null) {
            colis.setJointageList(new ArrayList<Jointage>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Embarquement embarquement = colis.getEmbarquement();
            if (embarquement != null) {
                embarquement = em.getReference(embarquement.getClass(), embarquement.getNumPlomb());
                colis.setEmbarquement(embarquement);
            }
            Jointage jointage = colis.getJointage();
            if (jointage != null) {
                jointage = em.getReference(jointage.getClass(), jointage.getRefJointage());
                colis.setJointage(jointage);
            }
            List<Empotage> attachedEmpotageList = new ArrayList<Empotage>();
            for (Empotage empotageListEmpotageToAttach : colis.getEmpotageList()) {
                empotageListEmpotageToAttach = em.getReference(empotageListEmpotageToAttach.getClass(), empotageListEmpotageToAttach.getRefEmpotage());
                attachedEmpotageList.add(empotageListEmpotageToAttach);
            }
            colis.setEmpotageList(attachedEmpotageList);
            List<Paquet> attachedPaquetList = new ArrayList<Paquet>();
            for (Paquet paquetListPaquetToAttach : colis.getPaquetList()) {
                paquetListPaquetToAttach = em.getReference(paquetListPaquetToAttach.getClass(), paquetListPaquetToAttach.getRefPaquet());
                attachedPaquetList.add(paquetListPaquetToAttach);
            }
            colis.setPaquetList(attachedPaquetList);
            List<Embarquement> attachedEmbarquementList = new ArrayList<Embarquement>();
            for (Embarquement embarquementListEmbarquementToAttach : colis.getEmbarquementList()) {
                embarquementListEmbarquementToAttach = em.getReference(embarquementListEmbarquementToAttach.getClass(), embarquementListEmbarquementToAttach.getNumPlomb());
                attachedEmbarquementList.add(embarquementListEmbarquementToAttach);
            }
            colis.setEmbarquementList(attachedEmbarquementList);
            List<Jointage> attachedJointageList = new ArrayList<Jointage>();
            for (Jointage jointageListJointageToAttach : colis.getJointageList()) {
                jointageListJointageToAttach = em.getReference(jointageListJointageToAttach.getClass(), jointageListJointageToAttach.getRefJointage());
                attachedJointageList.add(jointageListJointageToAttach);
            }
            colis.setJointageList(attachedJointageList);
            em.persist(colis);
            if (embarquement != null) {
                embarquement.getColisList().add(colis);
                embarquement = em.merge(embarquement);
            }
            if (jointage != null) {
                Colis oldColisFinalOfJointage = jointage.getColisFinal();
                if (oldColisFinalOfJointage != null) {
                    oldColisFinalOfJointage.setJointage(null);
                    oldColisFinalOfJointage = em.merge(oldColisFinalOfJointage);
                }
                jointage.setColisFinal(colis);
                jointage = em.merge(jointage);
            }
            for (Empotage empotageListEmpotage : colis.getEmpotageList()) {
                Colis oldRefColisOfEmpotageListEmpotage = empotageListEmpotage.getRefColis();
                empotageListEmpotage.setRefColis(colis);
                empotageListEmpotage = em.merge(empotageListEmpotage);
                if (oldRefColisOfEmpotageListEmpotage != null) {
                    oldRefColisOfEmpotageListEmpotage.getEmpotageList().remove(empotageListEmpotage);
                    oldRefColisOfEmpotageListEmpotage = em.merge(oldRefColisOfEmpotageListEmpotage);
                }
            }
            for (Paquet paquetListPaquet : colis.getPaquetList()) {
                Colis oldRefColisOfPaquetListPaquet = paquetListPaquet.getRefColis();
                paquetListPaquet.setRefColis(colis);
                paquetListPaquet = em.merge(paquetListPaquet);
                if (oldRefColisOfPaquetListPaquet != null) {
                    oldRefColisOfPaquetListPaquet.getPaquetList().remove(paquetListPaquet);
                    oldRefColisOfPaquetListPaquet = em.merge(oldRefColisOfPaquetListPaquet);
                }
            }
            for (Embarquement embarquementListEmbarquement : colis.getEmbarquementList()) {
                embarquementListEmbarquement.getColisList().add(colis);
                embarquementListEmbarquement = em.merge(embarquementListEmbarquement);
            }
            for (Jointage jointageListJointage : colis.getJointageList()) {
                jointageListJointage.getColisList().add(colis);
                jointageListJointage = em.merge(jointageListJointage);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findColis(colis.getRefColis()) != null) {
                throw new PreexistingEntityException("Colis " + colis + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Colis colis) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Colis persistentColis = em.find(Colis.class, colis.getRefColis());
            Embarquement embarquementOld = persistentColis.getEmbarquement();
            Embarquement embarquementNew = colis.getEmbarquement();
            Jointage jointageOld = persistentColis.getJointage();
            Jointage jointageNew = colis.getJointage();
            List<Empotage> empotageListOld = persistentColis.getEmpotageList();
            List<Empotage> empotageListNew = colis.getEmpotageList();
            List<Paquet> paquetListOld = persistentColis.getPaquetList();
            List<Paquet> paquetListNew = colis.getPaquetList();
            List<Embarquement> embarquementListOld = persistentColis.getEmbarquementList();
            List<Embarquement> embarquementListNew = colis.getEmbarquementList();
            List<Jointage> jointageListOld = persistentColis.getJointageList();
            List<Jointage> jointageListNew = colis.getJointageList();
            List<String> illegalOrphanMessages = null;
            if (jointageOld != null && !jointageOld.equals(jointageNew)) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("You must retain Jointage " + jointageOld + " since its colisFinal field is not nullable.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (embarquementNew != null) {
                embarquementNew = em.getReference(embarquementNew.getClass(), embarquementNew.getNumPlomb());
                colis.setEmbarquement(embarquementNew);
            }
            if (jointageNew != null) {
                jointageNew = em.getReference(jointageNew.getClass(), jointageNew.getRefJointage());
                colis.setJointage(jointageNew);
            }
            List<Empotage> attachedEmpotageListNew = new ArrayList<Empotage>();
            for (Empotage empotageListNewEmpotageToAttach : empotageListNew) {
                empotageListNewEmpotageToAttach = em.getReference(empotageListNewEmpotageToAttach.getClass(), empotageListNewEmpotageToAttach.getRefEmpotage());
                attachedEmpotageListNew.add(empotageListNewEmpotageToAttach);
            }
            empotageListNew = attachedEmpotageListNew;
            colis.setEmpotageList(empotageListNew);
            List<Paquet> attachedPaquetListNew = new ArrayList<Paquet>();
            for (Paquet paquetListNewPaquetToAttach : paquetListNew) {
                paquetListNewPaquetToAttach = em.getReference(paquetListNewPaquetToAttach.getClass(), paquetListNewPaquetToAttach.getRefPaquet());
                attachedPaquetListNew.add(paquetListNewPaquetToAttach);
            }
            
            paquetListNew = attachedPaquetListNew;
            colis.setPaquetList(paquetListNew);
            List<Embarquement> attachedEmbarquementListNew = new ArrayList<Embarquement>();
            for (Embarquement embarquementListNewEmbarquementToAttach : embarquementListNew) {
                embarquementListNewEmbarquementToAttach = em.getReference(embarquementListNewEmbarquementToAttach.getClass(), embarquementListNewEmbarquementToAttach.getNumPlomb());
                attachedEmbarquementListNew.add(embarquementListNewEmbarquementToAttach);
            }
            embarquementListNew = attachedEmbarquementListNew;
            colis.setEmbarquementList(embarquementListNew);
            List<Jointage> attachedJointageListNew = new ArrayList<Jointage>();
            for (Jointage jointageListNewJointageToAttach : jointageListNew) {
                jointageListNewJointageToAttach = em.getReference(jointageListNewJointageToAttach.getClass(), jointageListNewJointageToAttach.getRefJointage());
                attachedJointageListNew.add(jointageListNewJointageToAttach);
            }
            jointageListNew = attachedJointageListNew;
            colis.setJointageList(jointageListNew);
            colis = em.merge(colis);
            if (embarquementOld != null && !embarquementOld.equals(embarquementNew)) {
                embarquementOld.getColisList().remove(colis);
                embarquementOld = em.merge(embarquementOld);
            }
            if (embarquementNew != null && !embarquementNew.equals(embarquementOld)) {
                embarquementNew.getColisList().add(colis);
                embarquementNew = em.merge(embarquementNew);
            }
            if (jointageNew != null && !jointageNew.equals(jointageOld)) {
                Colis oldColisFinalOfJointage = jointageNew.getColisFinal();
                if (oldColisFinalOfJointage != null) {
                    oldColisFinalOfJointage.setJointage(null);
                    oldColisFinalOfJointage = em.merge(oldColisFinalOfJointage);
                }
                jointageNew.setColisFinal(colis);
                jointageNew = em.merge(jointageNew);
            }
            for (Empotage empotageListOldEmpotage : empotageListOld) {
                if (!empotageListNew.contains(empotageListOldEmpotage)) {
                    empotageListOldEmpotage.setRefColis(null);
                    empotageListOldEmpotage = em.merge(empotageListOldEmpotage);
                }
            }
            for (Empotage empotageListNewEmpotage : empotageListNew) {
                if (!empotageListOld.contains(empotageListNewEmpotage)) {
                    Colis oldRefColisOfEmpotageListNewEmpotage = empotageListNewEmpotage.getRefColis();
                    empotageListNewEmpotage.setRefColis(colis);
                    empotageListNewEmpotage = em.merge(empotageListNewEmpotage);
                    if (oldRefColisOfEmpotageListNewEmpotage != null && !oldRefColisOfEmpotageListNewEmpotage.equals(colis)) {
                        oldRefColisOfEmpotageListNewEmpotage.getEmpotageList().remove(empotageListNewEmpotage);
                        oldRefColisOfEmpotageListNewEmpotage = em.merge(oldRefColisOfEmpotageListNewEmpotage);
                    }
                }
            }
            for (Paquet paquetListOldPaquet : paquetListOld) {
                if (!paquetListNew.contains(paquetListOldPaquet)) {
                    paquetListOldPaquet.setRefColis(null);
                    paquetListOldPaquet = em.merge(paquetListOldPaquet);
                }
            }
            for (Paquet paquetListNewPaquet : paquetListNew) {
                if (!paquetListOld.contains(paquetListNewPaquet)) {
                    Colis oldRefColisOfPaquetListNewPaquet = paquetListNewPaquet.getRefColis();
                    paquetListNewPaquet.setRefColis(colis);
                    paquetListNewPaquet = em.merge(paquetListNewPaquet);
                    if (oldRefColisOfPaquetListNewPaquet != null && !oldRefColisOfPaquetListNewPaquet.equals(colis)) {
                        oldRefColisOfPaquetListNewPaquet.getPaquetList().remove(paquetListNewPaquet);
                        oldRefColisOfPaquetListNewPaquet = em.merge(oldRefColisOfPaquetListNewPaquet);
                    }
                }
            }
            for (Embarquement embarquementListOldEmbarquement : embarquementListOld) {
                if (!embarquementListNew.contains(embarquementListOldEmbarquement)) {
                    embarquementListOldEmbarquement.getColisList().remove(colis);
                    embarquementListOldEmbarquement = em.merge(embarquementListOldEmbarquement);
                }
            }
            for (Embarquement embarquementListNewEmbarquement : embarquementListNew) {
                if (!embarquementListOld.contains(embarquementListNewEmbarquement)) {
                    embarquementListNewEmbarquement.getColisList().add(colis);
                    embarquementListNewEmbarquement = em.merge(embarquementListNewEmbarquement);
                }
            }
            for (Jointage jointageListOldJointage : jointageListOld) {
                if (!jointageListNew.contains(jointageListOldJointage)) {
                    jointageListOldJointage.getColisList().remove(colis);
                    jointageListOldJointage = em.merge(jointageListOldJointage);
                }
            }
            for (Jointage jointageListNewJointage : jointageListNew) {
                if (!jointageListOld.contains(jointageListNewJointage)) {
                    jointageListNewJointage.getColisList().add(colis);
                    jointageListNewJointage = em.merge(jointageListNewJointage);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = colis.getRefColis();
                if (findColis(id) == null) {
                    throw new NonexistentEntityException("The colis with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Colis colis;
            try {
                colis = em.getReference(Colis.class, id);
                colis.getRefColis();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The colis with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Jointage jointageOrphanCheck = colis.getJointage();
            if (jointageOrphanCheck != null) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Colis (" + colis + ") cannot be destroyed since the Jointage " + jointageOrphanCheck + " in its jointage field has a non-nullable colisFinal field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Embarquement embarquement = colis.getEmbarquement();
            if (embarquement != null) {
                embarquement.getColisList().remove(colis);
                embarquement = em.merge(embarquement);
            }
            List<Empotage> empotageList = colis.getEmpotageList();
            for (Empotage empotageListEmpotage : empotageList) {
                empotageListEmpotage.setRefColis(null);
                empotageListEmpotage = em.merge(empotageListEmpotage);
            }
            List<Paquet> paquetList = colis.getPaquetList();
            for (Paquet paquetListPaquet : paquetList) {
                paquetListPaquet.setRefColis(null);
                paquetListPaquet = em.merge(paquetListPaquet);
            }
            List<Embarquement> embarquementList = colis.getEmbarquementList();
            for (Embarquement embarquementListEmbarquement : embarquementList) {
                embarquementListEmbarquement.getColisList().remove(colis);
                embarquementListEmbarquement = em.merge(embarquementListEmbarquement);
            }
            List<Jointage> jointageList = colis.getJointageList();
            for (Jointage jointageListJointage : jointageList) {
                jointageListJointage.getColisList().remove(colis);
                jointageListJointage = em.merge(jointageListJointage);
            }
            em.remove(colis);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Colis> findColisEntities() {
        return findColisEntities(true, -1, -1);
    }

    public List<Colis> findColisEntities(int maxResults, int firstResult) {
        return findColisEntities(false, maxResults, firstResult);
    }

    private List<Colis> findColisEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Colis.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Colis findColis(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Colis.class, id);
        } finally {
            em.close();
        }
    }

    public int getColisCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Colis> rt = cq.from(Colis.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
        
    public List getColisOuverts() {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("from Colis cl where cl.dateFermeture is  null", Colis.class);
            return q.getResultList();
        }finally{
            em.close();
        }
    }    

    public List colisForContenaire() {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("from Empotage cl where cl.refColis.embarquement is null");// and cl.refColis in (Select emp.refColis.refColis from Empotage emp)", Colis.class);
            return q.getResultList();
        }finally{
            em.close();
        }
    }

    public List<Colis> colisForContenaire(String refContennaire) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("from Colis cl where cl.embarquement.numPlomb = :embarquement", Colis.class);
            q.setParameter("embarquement", refContennaire);
            return q.getResultList();
        }finally{
            em.close();
        }
    }

    
    public int getcolisCountForThisYear(int year) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select count(1) from Colis cl where cl.refColis LIKE :colisRefFormat");
            q.setParameter("colisRefFormat", year+"-%");
            return Integer.parseInt(q.getResultList().get(0).toString());// Integer.parseInt(q.getParameterValue(q.getFirstResult()).toString());
        }finally{
            em.close();
        }
    }

    public List getColisOuvertsByAtelier(String atelier) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select cl from Colis cl where cl.dateFermeture is null and cl.refColis LIKE :ref", Colis.class);
            q.setParameter("ref", '%'+atelier+'%');
            System.out.println("Atelier = "+atelier+" Query = "+q);
            return q.getResultList();
        }finally{
            em.close();
        }
    }

    public int getcolisCountForThisYearByAtelier(int year, String atelierRef) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select count(1) from Colis cl where cl.refColis LIKE :colisRefFormat");
            q.setParameter("colisRefFormat", year+"-"+atelierRef+"%");
            return Integer.parseInt(q.getResultList().get(0).toString());// Integer.parseInt(q.getParameterValue(q.getFirstResult()).toString());
        }finally{
            em.close();
        }
    }

    public String getQualityNEpaisseurForContenaire(String plomb) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select cl.qualite, cl.epaisseur from Colis cl where cl.refContennaire.plomb = :plomb");
            q.setParameter("plomb",plomb);
            Object[] result = (Object[])q.getResultList().get(0);
            return "{\"qualite\": \""+result[0].toString()+"\", \"epaisseur\": \""+result[1].toString()+"\"}";// Integer.parseInt(q.getParameterValue(q.getFirstResult()).toString());
        }finally{
            em.close();
        }
    }

    public List<Colis> getColisFermes() {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery(" FROM Colis cl where cl.dateFermeture is not null");
            return q.getResultList();
        }finally{
            em.close();
        }
    }

    Collection<? extends Colis> getColisJoints(Colis c) {
        Jointage j = c.getJointage();
        if(j == null)
            return new LinkedList<>();
        List<Colis> l = j.getColisList();
        l.remove(j.getColisFinal());
        return l;
        
    }

    public List<Colis> getColisForJointage(Jointage aThis) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("Select c from Colis c WHERE c.jointage.refJointage = :refJointage and difference(c.refColis,:refColisFinal) ");
            q.setParameter("refColisFinal", aThis.getColisFinal().getRefColis());
            q.setParameter("refJointage", aThis.getRefJointage());
            return q.getResultList();
        }finally{
            em.close();
        }
    }

    public List findColisExpedies(long...intervalTemps) {
        EntityManager em = getEntityManager();
        try{
            Query q;
            if(intervalTemps[0] == 0 && intervalTemps[1] == 0)
                q = em.createQuery("Select c from Colis c WHERE c.embarquement is not null and c.embarquement.numPlomb is not null");
            else if(intervalTemps[0] > 0 && intervalTemps[1]==0){ // NUMTODSINTERVAL(MMSTAMP, SECOND)
                q = em.createQuery("Select c from Colis c WHERE c.embarquement is not null and c.embarquement.numPlomb is not null AND c.createAt >= :date");
                q.setParameter("date", new Date(intervalTemps[0]));
            }else if(intervalTemps[0] == 0 && intervalTemps[1]>0){
                q = em.createQuery("Select c from Colis c WHERE c.embarquement is not null and c.embarquement.numPlomb is not null AND c.createAt <= :date");
                q.setParameter("date", new Date(intervalTemps[1]));
            }else if(intervalTemps[0]<intervalTemps[1]){
                q = em.createQuery("Select c from Colis c WHERE c.embarquement is not null and c.embarquement.numPlomb is not null AND c.createAt BETWEEN :dateD AND :dateF");
                q.setParameter("dateD", new Date(intervalTemps[0]));
                q.setParameter("dateF", new Date(intervalTemps[1]));
            }else
                q = em.createQuery("Select c from Colis c WHERE c.embarquement is not null and c.embarquement.numPlomb is not null");
            
            return q.getResultList();
        }catch(Exception e){
            System.err.println(e.getMessage());
            return new LinkedList();
        }finally{
            em.close();
        }
    }

    public List findColisEnStock(long...intervalTemps) {
        EntityManager em = getEntityManager();
        try{
            Query q;
            if(intervalTemps[0] == 0 && intervalTemps[1] == 0)
                q = em.createQuery("Select c from Colis c WHERE c.embarquement is null");
            else if(intervalTemps[0] > 0 && intervalTemps[1]==0){ // NUMTODSINTERVAL(MMSTAMP, SECOND)
                q = em.createQuery("Select c from Colis c WHERE c.embarquement is null AND c.createAt >= :date");
                q.setParameter("date", new Date(intervalTemps[0]));
            }else if(intervalTemps[0] == 0 && intervalTemps[1]>0){
                q = em.createQuery("Select c from Colis c WHERE c.embarquement is null AND c.createAt <= :date");
                q.setParameter("date", new Date(intervalTemps[1]));
            }else if(intervalTemps[0]<intervalTemps[1]){
                q = em.createQuery("Select c from Colis c WHERE c.embarquement is null AND c.createAt BETWEEN :dateD AND :dateF");
                q.setParameter("dateD", new Date(intervalTemps[0]));
                q.setParameter("dateF", new Date(intervalTemps[1]));
            }else
                q = em.createQuery("Select c from Colis c WHERE c.embarquement is null");
            return q.getResultList();
        }finally{
            em.close();
        }
    }

    public List<Colis> getColisFermesNonJoints() {

        EntityManager em = getEntityManager();
        try{
            /**
             * Les colis joints on une notation J sur leur reférence. donc on récupere ceux qui sont fermés et qui ne porte pas l'anotation
             */
            Query q = em.createQuery("select cl FROM Colis cl where cl.dateFermeture is not null");// and cl.refColis not like '%J%'");
            return q.getResultList();
        }finally{
            em.close();
        }
    }

    public List findColisByOrigin(String type, long...intervalTemps) {
        EntityManager em = getEntityManager();
        System.out.println("type = "+type);
        try{
            Query q;
            String s = (""
                    + "Select distinct c "
                    + "from Colis c, Paquet p, Parc pc, TypeParc tp, DetailReception dr "
                    + "WHERE (p.refCodebar LIKE CONCAT(c.refColis, '%') OR p.refColis.refColis = c.refColis) "
                    + "AND p.numTravail = dr.numtravail "
                    + "AND dr.numBille = pc.numBille "
                    + "AND pc.refTypeparc.refTypeparc = :type "
                    + "");

            if(intervalTemps[0] == 0 && intervalTemps[1] == 0)
                q = em.createQuery(s);
            else if(intervalTemps[0] > 0 && intervalTemps[1]==0){ // NUMTODSINTERVAL(MMSTAMP, SECOND)
                q = em.createQuery(s+" AND c.createAt >= :date");
                q.setParameter("date", new Date(intervalTemps[0]));
            }else if(intervalTemps[0] == 0 && intervalTemps[1]>0){
                q = em.createQuery(s+" AND c.createAt <= :date");
                q.setParameter("date", new Date(intervalTemps[1]));
            }else if(intervalTemps[0]<intervalTemps[1]){
                q = em.createQuery(s+" AND c.createAt BETWEEN :dateD AND :dateF");
                q.setParameter("dateD", new Date(intervalTemps[0]));
                q.setParameter("dateF", new Date(intervalTemps[1]));
            }else
                q = em.createQuery(s);
            q.setParameter("type", type);
            
            return q.getResultList();
        }catch(Exception e){
            System.err.println(e.getMessage());
            return new LinkedList();
        }finally{
            em.close();
        }
    }

    public String getCertificationColis(String refColis) {
        
        EntityManager em = getEntityManager();
        try{
            
            Query q = em.createQuery(
                "Select pr.refTypeparc.libelle from Parc pr, DetailReception dr, Paquet pt where :colis = pt.refColis.refColis and pt.numTravail = dr.numtravail and dr.numBille = pr.numBille");
            q.setParameter("colis", refColis);
            List<Object> list = q.getResultList();
            return list.size() > 0 ? list.get(0).toString() : null;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }

    public List<String> numTravailIn(Colis c) {
        EntityManager em = getEntityManager();
        try{
            List<String> liste = new LinkedList();
            TypedQuery q = em.createQuery("SELECT distinct pt.numTravail as travail from Paquet pt where pt.refColis.refColis = :colis", List.class);
            q.setParameter("colis", c.getRefColis());
            List<String> l = q.getResultList();
            l.forEach((e)->liste.add(e));
            return liste;
        }catch(Exception ex){
            ex.printStackTrace();
            return new LinkedList();
        }finally{
            em.close();
        }
    }

    public int getNumberColisJoint() {
        
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();
            Root<Colis> rt = cq.from(Colis.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            cq.where(cb.equal(rt.get("typeColis"), "J"));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<Colis> findColisJointage(long...intervalTemps) {
        EntityManager em = getEntityManager();
        try{
            Query q;
            if(intervalTemps[0] == 0 && intervalTemps[1] == 0)
                q = em.createQuery("Select c from Colis c WHERE c.typeColis = 'J'");
            else if(intervalTemps[0] > 0 && intervalTemps[1]==0){ // NUMTODSINTERVAL(MMSTAMP, SECOND)
                q = em.createQuery("Select c from Colis c, Jointage j WHERE c.jointeur = j.refJointage and j.dateJointage >= :date");
                q.setParameter("date", new Date(intervalTemps[0]));
            }else if(intervalTemps[0] == 0 && intervalTemps[1]>0){
                q = em.createQuery("Select c from Colis c, Jointage j WHERE c.jointeur = j.refJointage and j.dateJointage <= :date");
                q.setParameter("date", new Date(intervalTemps[1]));
            }else if(intervalTemps[0]<intervalTemps[1]){
                q = em.createQuery("Select c from Colis c, Jointage j WHERE c.jointeur = j.refJointage and j.dateJointage BETWEEN :dateD AND :dateF");
                q.setParameter("dateD", new Date(intervalTemps[0]));
                q.setParameter("dateF", new Date(intervalTemps[1]));
            }else
                q = em.createQuery("Select c from Colis c WHERE c.jointeur is not null");
            
            return q.getResultList();
        }catch(Exception e){
            System.err.println(e.getMessage());
            return new LinkedList();
        }finally{
            em.close();
        }
    }

    public JSONObject groupPaquetByNumber(String colis) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("SELECT p.nbreFeuille, COUNT(1) as nbrPaquet FROM Paquet p WHERE p.refColis.refColis = :colis GROUP BY p.nbreFeuille");
            q.setParameter("colis", colis);
            List<Object[]> result = q.getResultList();
            if(result.isEmpty())
                return null;
            JSONObject obj = new JSONObject();
            result.forEach((Object[] l)->{
                try {
                    obj.put(l[0].toString(), l[1]);
                } catch (JSONException ex) {}
            });
            return obj;
        }finally{
            em.close();
        }
    }

    /**
     * Ici on met à jour le nombre de paquet du colis, le volume, et on supprime les paquets superflus
     * @param req 
     */
    public void updateColis(HttpServletRequest req) {
        String[] colis = req.getParameterValues("colis[]");
        
        for(String refColis : colis){
            int taille = Integer.parseInt(req.getParameter("map["+refColis+"][taille]"));
            List<Integer> donnees = new LinkedList();
            for(int i=0; i<taille; i++){
                int nbrFeuille = Integer.parseInt(req.getParameter("colisUpdate["+refColis+"]["+i+"][groupe]"));
                int restPaquet = Integer.parseInt(req.getParameter("colisUpdate["+refColis+"]["+i+"][effectif]"));
                donnees.add(nbrFeuille);
                donnees.add(restPaquet);
                System.out.println("nbrFeuille = "+nbrFeuille+" restPaquet = "+restPaquet);
            }
            deletePaquetAndUpdateColis(refColis, donnees);
        }
    }

    /**
     * Cette fonction supprime les paquets superflus
     * puis met à jour les volumes
     * @param refColis colis
     * @param nbrFeuille le nombre de feuilles contenu dans chaque paquet à supprimer
     * @param restPaquet le nombre de paquets ayant nbrFeuille à qui doit rester dans la bd
     */
    private void deletePaquetAndUpdateColis(String refColis, List<Integer> donnees) {
        EntityManager em = getEntityManager();
        PaquetJpaController contPaquet = new PaquetJpaController(Database.getEntityManager());
        ColisJpaController contColis = new ColisJpaController(Database.getEntityManager());
        try{
            List<Paquet> liste = new LinkedList();
            for(int i=0; i<donnees.size()/2; i++){
                List<Paquet> paquets =  contPaquet.getPaquetInColisByNbrFeuille(refColis, donnees.get(i*2));
                System.out.println("*nbrFeuille = "+(donnees.get(i*2))+" restPaquet = "+donnees.get(i*2+1));
                if(donnees.get(i*2+1) == 0){
                    for(Paquet p : paquets)
                        contPaquet.destroy(p.getRefPaquet());
                }else{
                    for(int j=0; j<paquets.size()-donnees.get(i*2+1); j++){
                        contPaquet.destroy(paquets.get(j).getRefPaquet());
                        paquets.remove(paquets.get(j));
                    }
                    liste.addAll(paquets);
                    paquets.clear();
                }
            }
            
            Colis colis = contColis.findColis(refColis);
            double surface = 0D, volume = 0D;
            for(Paquet pqt : liste){
                surface += pqt.getSurface();
                volume  += pqt.getVolume();
            }
            colis.setSurfaceColis(surface);
            colis.setVoulumeColis(volume);
            colis.setFermetureColis("");
            colis.setDateFermeture(null);
            colis.setDateFin(null);
            colis.setPoidEmballage(null);
            colis.setPoidNet(null);
            colis.setPoidsColis(null);
            colis.setNbrePaquet(liste.size());
            new EmpotageJpaController(Database.getEntityManager()).deleteByColis(colis.getRefColis());
            if(colis.getNbrePaquet() == 0)
                contColis.destroy(colis.getRefColis());
            else
                contColis.edit(colis);
            System.out.println("Colis "+colis.getRefColis()+" clean surface = "+surface+" volume = "+volume+"\n\t-> "+colis+"\n\t -> "+liste.size());
        } catch (IllegalOrphanException ex) {
            System.out.println("Colis "+refColis+" pas clean 1 => "+ex.getMessage());
            ex.printStackTrace();
        } catch (NonexistentEntityException ex) {
            System.out.println("Colis "+refColis+" pas clean 2 => "+ex.getMessage());
            ex.printStackTrace();
        } catch (Exception ex) {
            System.out.println("Colis "+refColis+" pas clean 3 => "+ex.getMessage());
            ex.printStackTrace();
        }finally{
            em.close();
        }
    }

    public List findColisOuverts(long...intervalTemps) {
        EntityManager em = getEntityManager();
        try{
            Query q;
            if(intervalTemps[0] == 0 && intervalTemps[1] == 0)
                q = em.createQuery("Select c from Colis c WHERE c.dateFermeture is null");
            else if(intervalTemps[0] > 0 && intervalTemps[1]==0){ // NUMTODSINTERVAL(MMSTAMP, SECOND)
                q = em.createQuery("Select c from Colis c WHERE c.dateFermeture is null AND c.createAt >= :date");
                q.setParameter("date", new Date(intervalTemps[0]));
            }else if(intervalTemps[0] == 0 && intervalTemps[1]>0){
                q = em.createQuery("Select c from Colis c WHERE c.dateFermeture is null AND c.createAt <= :date");
                q.setParameter("date", new Date(intervalTemps[1]));
            }else if(intervalTemps[0]<intervalTemps[1]){
                q = em.createQuery("Select c from Colis c WHERE c.dateFermeture is null AND c.createAt BETWEEN :dateD AND :dateF");
                q.setParameter("dateD", new Date(intervalTemps[0]));
                q.setParameter("dateF", new Date(intervalTemps[1]));
            }else
                q = em.createQuery("Select c from Colis c WHERE c.dateFermeture is null");
            
            return q.getResultList();
        }catch(Exception e){
            System.err.println(e.getMessage());
            return new LinkedList();
        }finally{
            em.close();
        }
    }

    
}
