/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import app.models.Compagnie;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Conteneur;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class CompagnieJpaController implements Serializable {

    public CompagnieJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Compagnie compagnie) throws PreexistingEntityException, Exception {
        if (compagnie.getConteneurList() == null) {
            compagnie.setConteneurList(new ArrayList<Conteneur>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Conteneur> attachedConteneurList = new ArrayList<Conteneur>();
            for (Conteneur conteneurListConteneurToAttach : compagnie.getConteneurList()) {
                conteneurListConteneurToAttach = em.getReference(conteneurListConteneurToAttach.getClass(), conteneurListConteneurToAttach.getNumConteneur());
                attachedConteneurList.add(conteneurListConteneurToAttach);
            }
            compagnie.setConteneurList(attachedConteneurList);
            em.persist(compagnie);
            for (Conteneur conteneurListConteneur : compagnie.getConteneurList()) {
                Compagnie oldRefCompagnieOfConteneurListConteneur = conteneurListConteneur.getRefCompagnie();
                conteneurListConteneur.setRefCompagnie(compagnie);
                conteneurListConteneur = em.merge(conteneurListConteneur);
                if (oldRefCompagnieOfConteneurListConteneur != null) {
                    oldRefCompagnieOfConteneurListConteneur.getConteneurList().remove(conteneurListConteneur);
                    oldRefCompagnieOfConteneurListConteneur = em.merge(oldRefCompagnieOfConteneurListConteneur);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCompagnie(compagnie.getRefCompagnie()) != null) {
                throw new PreexistingEntityException("Compagnie " + compagnie + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Compagnie compagnie) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Compagnie persistentCompagnie = em.find(Compagnie.class, compagnie.getRefCompagnie());
            List<Conteneur> conteneurListOld = persistentCompagnie.getConteneurList();
            List<Conteneur> conteneurListNew = compagnie.getConteneurList();
            List<Conteneur> attachedConteneurListNew = new ArrayList<Conteneur>();
            for (Conteneur conteneurListNewConteneurToAttach : conteneurListNew) {
                conteneurListNewConteneurToAttach = em.getReference(conteneurListNewConteneurToAttach.getClass(), conteneurListNewConteneurToAttach.getNumConteneur());
                attachedConteneurListNew.add(conteneurListNewConteneurToAttach);
            }
            conteneurListNew = attachedConteneurListNew;
            compagnie.setConteneurList(conteneurListNew);
            compagnie = em.merge(compagnie);
            for (Conteneur conteneurListOldConteneur : conteneurListOld) {
                if (!conteneurListNew.contains(conteneurListOldConteneur)) {
                    conteneurListOldConteneur.setRefCompagnie(null);
                    conteneurListOldConteneur = em.merge(conteneurListOldConteneur);
                }
            }
            for (Conteneur conteneurListNewConteneur : conteneurListNew) {
                if (!conteneurListOld.contains(conteneurListNewConteneur)) {
                    Compagnie oldRefCompagnieOfConteneurListNewConteneur = conteneurListNewConteneur.getRefCompagnie();
                    conteneurListNewConteneur.setRefCompagnie(compagnie);
                    conteneurListNewConteneur = em.merge(conteneurListNewConteneur);
                    if (oldRefCompagnieOfConteneurListNewConteneur != null && !oldRefCompagnieOfConteneurListNewConteneur.equals(compagnie)) {
                        oldRefCompagnieOfConteneurListNewConteneur.getConteneurList().remove(conteneurListNewConteneur);
                        oldRefCompagnieOfConteneurListNewConteneur = em.merge(oldRefCompagnieOfConteneurListNewConteneur);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = compagnie.getRefCompagnie();
                if (findCompagnie(id) == null) {
                    throw new NonexistentEntityException("The compagnie with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Compagnie compagnie;
            try {
                compagnie = em.getReference(Compagnie.class, id);
                compagnie.getRefCompagnie();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The compagnie with id " + id + " no longer exists.", enfe);
            }
            List<Conteneur> conteneurList = compagnie.getConteneurList();
            for (Conteneur conteneurListConteneur : conteneurList) {
                conteneurListConteneur.setRefCompagnie(null);
                conteneurListConteneur = em.merge(conteneurListConteneur);
            }
            em.remove(compagnie);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Compagnie> findCompagnieEntities() {
        return findCompagnieEntities(true, -1, -1);
    }

    public List<Compagnie> findCompagnieEntities(int maxResults, int firstResult) {
        return findCompagnieEntities(false, maxResults, firstResult);
    }

    private List<Compagnie> findCompagnieEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Compagnie.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Compagnie findCompagnie(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Compagnie.class, id);
        } finally {
            em.close();
        }
    }

    public int getCompagnieCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Compagnie> rt = cq.from(Compagnie.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
