/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Compagnie;
import app.models.Conteneur;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class ConteneurJpaController implements Serializable {

    public ConteneurJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Conteneur conteneur) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Compagnie refCompagnie = conteneur.getRefCompagnie();
            if (refCompagnie != null) {
                refCompagnie = em.getReference(refCompagnie.getClass(), refCompagnie.getRefCompagnie());
                conteneur.setRefCompagnie(refCompagnie);
            }
            em.persist(conteneur);
            if (refCompagnie != null) {
                refCompagnie.getConteneurList().add(conteneur);
                refCompagnie = em.merge(refCompagnie);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findConteneur(conteneur.getNumConteneur()) != null) {
                throw new PreexistingEntityException("Conteneur " + conteneur + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Conteneur conteneur) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Conteneur persistentConteneur = em.find(Conteneur.class, conteneur.getNumConteneur());
            Compagnie refCompagnieOld = persistentConteneur.getRefCompagnie();
            Compagnie refCompagnieNew = conteneur.getRefCompagnie();
            if (refCompagnieNew != null) {
                refCompagnieNew = em.getReference(refCompagnieNew.getClass(), refCompagnieNew.getRefCompagnie());
                conteneur.setRefCompagnie(refCompagnieNew);
            }
            conteneur = em.merge(conteneur);
            if (refCompagnieOld != null && !refCompagnieOld.equals(refCompagnieNew)) {
                refCompagnieOld.getConteneurList().remove(conteneur);
                refCompagnieOld = em.merge(refCompagnieOld);
            }
            if (refCompagnieNew != null && !refCompagnieNew.equals(refCompagnieOld)) {
                refCompagnieNew.getConteneurList().add(conteneur);
                refCompagnieNew = em.merge(refCompagnieNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = conteneur.getNumConteneur();
                if (findConteneur(id) == null) {
                    throw new NonexistentEntityException("The conteneur with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Conteneur conteneur;
            try {
                conteneur = em.getReference(Conteneur.class, id);
                conteneur.getNumConteneur();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The conteneur with id " + id + " no longer exists.", enfe);
            }
            Compagnie refCompagnie = conteneur.getRefCompagnie();
            if (refCompagnie != null) {
                refCompagnie.getConteneurList().remove(conteneur);
                refCompagnie = em.merge(refCompagnie);
            }
            em.remove(conteneur);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Conteneur> findConteneurEntities() {
        return findConteneurEntities(true, -1, -1);
    }

    public List<Conteneur> findConteneurEntities(int maxResults, int firstResult) {
        return findConteneurEntities(false, maxResults, firstResult);
    }

    private List<Conteneur> findConteneurEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Conteneur.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Conteneur findConteneur(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Conteneur.class, id);
        } finally {
            em.close();
        }
    }

    public int getConteneurCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Conteneur> rt = cq.from(Conteneur.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
