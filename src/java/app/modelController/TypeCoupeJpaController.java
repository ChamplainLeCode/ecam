/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Coupe;
import app.models.TypeCoupe;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class TypeCoupeJpaController implements Serializable {

    public TypeCoupeJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TypeCoupe typeCoupe) throws PreexistingEntityException, Exception {
        if (typeCoupe.getCoupeList() == null) {
            typeCoupe.setCoupeList(new ArrayList<Coupe>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Coupe> attachedCoupeList = new ArrayList<Coupe>();
            for (Coupe coupeListCoupeToAttach : typeCoupe.getCoupeList()) {
                coupeListCoupeToAttach = em.getReference(coupeListCoupeToAttach.getClass(), coupeListCoupeToAttach.getRefCoupe());
                attachedCoupeList.add(coupeListCoupeToAttach);
            }
            typeCoupe.setCoupeList(attachedCoupeList);
            em.persist(typeCoupe);
            for (Coupe coupeListCoupe : typeCoupe.getCoupeList()) {
                TypeCoupe oldRefTypecoupeOfCoupeListCoupe = coupeListCoupe.getRefTypecoupe();
                coupeListCoupe.setRefTypecoupe(typeCoupe);
                coupeListCoupe = em.merge(coupeListCoupe);
                if (oldRefTypecoupeOfCoupeListCoupe != null) {
                    oldRefTypecoupeOfCoupeListCoupe.getCoupeList().remove(coupeListCoupe);
                    oldRefTypecoupeOfCoupeListCoupe = em.merge(oldRefTypecoupeOfCoupeListCoupe);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTypeCoupe(typeCoupe.getRefTypecoupe()) != null) {
                throw new PreexistingEntityException("TypeCoupe " + typeCoupe + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TypeCoupe typeCoupe) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TypeCoupe persistentTypeCoupe = em.find(TypeCoupe.class, typeCoupe.getRefTypecoupe());
            List<Coupe> coupeListOld = persistentTypeCoupe.getCoupeList();
            List<Coupe> coupeListNew = typeCoupe.getCoupeList();
            List<Coupe> attachedCoupeListNew = new ArrayList<Coupe>();
            for (Coupe coupeListNewCoupeToAttach : coupeListNew) {
                coupeListNewCoupeToAttach = em.getReference(coupeListNewCoupeToAttach.getClass(), coupeListNewCoupeToAttach.getRefCoupe());
                attachedCoupeListNew.add(coupeListNewCoupeToAttach);
            }
            coupeListNew = attachedCoupeListNew;
            typeCoupe.setCoupeList(coupeListNew);
            typeCoupe = em.merge(typeCoupe);
            for (Coupe coupeListOldCoupe : coupeListOld) {
                if (!coupeListNew.contains(coupeListOldCoupe)) {
                    coupeListOldCoupe.setRefTypecoupe(null);
                    coupeListOldCoupe = em.merge(coupeListOldCoupe);
                }
            }
            for (Coupe coupeListNewCoupe : coupeListNew) {
                if (!coupeListOld.contains(coupeListNewCoupe)) {
                    TypeCoupe oldRefTypecoupeOfCoupeListNewCoupe = coupeListNewCoupe.getRefTypecoupe();
                    coupeListNewCoupe.setRefTypecoupe(typeCoupe);
                    coupeListNewCoupe = em.merge(coupeListNewCoupe);
                    if (oldRefTypecoupeOfCoupeListNewCoupe != null && !oldRefTypecoupeOfCoupeListNewCoupe.equals(typeCoupe)) {
                        oldRefTypecoupeOfCoupeListNewCoupe.getCoupeList().remove(coupeListNewCoupe);
                        oldRefTypecoupeOfCoupeListNewCoupe = em.merge(oldRefTypecoupeOfCoupeListNewCoupe);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = typeCoupe.getRefTypecoupe();
                if (findTypeCoupe(id) == null) {
                    throw new NonexistentEntityException("The typeCoupe with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TypeCoupe typeCoupe;
            try {
                typeCoupe = em.getReference(TypeCoupe.class, id);
                typeCoupe.getRefTypecoupe();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The typeCoupe with id " + id + " no longer exists.", enfe);
            }
            List<Coupe> coupeList = typeCoupe.getCoupeList();
            for (Coupe coupeListCoupe : coupeList) {
                coupeListCoupe.setRefTypecoupe(null);
                coupeListCoupe = em.merge(coupeListCoupe);
            }
            em.remove(typeCoupe);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TypeCoupe> findTypeCoupeEntities() {
        return findTypeCoupeEntities(true, -1, -1);
    }

    public List<TypeCoupe> findTypeCoupeEntities(int maxResults, int firstResult) {
        return findTypeCoupeEntities(false, maxResults, firstResult);
    }

    private List<TypeCoupe> findTypeCoupeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TypeCoupe.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TypeCoupe findTypeCoupe(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TypeCoupe.class, id);
        } finally {
            em.close();
        }
    }

    public int getTypeCoupeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TypeCoupe> rt = cq.from(TypeCoupe.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
