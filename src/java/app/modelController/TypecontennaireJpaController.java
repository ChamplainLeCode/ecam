/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Embarquement;
import app.models.Typecontennaire;
import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class TypecontennaireJpaController implements Serializable {

    public TypecontennaireJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Typecontennaire typecontennaire) throws PreexistingEntityException, Exception {

        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(typecontennaire);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTypecontennaire(typecontennaire.getRefTypecont()) != null) {
                throw new PreexistingEntityException("Typecontennaire " + typecontennaire + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Typecontennaire typecontennaire) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Typecontennaire persistentTypecontennaire = em.find(Typecontennaire.class, typecontennaire.getRefTypecont());

            List<Embarquement> attachedContennaireListNew = new ArrayList<Embarquement>();

            typecontennaire = em.merge(typecontennaire);


            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = typecontennaire.getRefTypecont();
                if (findTypecontennaire(id) == null) {
                    throw new NonexistentEntityException("The typecontennaire with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Typecontennaire typecontennaire;
            try {
                typecontennaire = em.getReference(Typecontennaire.class, id);
                typecontennaire.getRefTypecont();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The typecontennaire with id " + id + " no longer exists.", enfe);
            }

            em.remove(typecontennaire);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Typecontennaire> findTypecontennaireEntities() {
        return findTypecontennaireEntities(true, -1, -1);
    }

    public List<Typecontennaire> findTypecontennaireEntities(int maxResults, int firstResult) {
        return findTypecontennaireEntities(false, maxResults, firstResult);
    }

    private List<Typecontennaire> findTypecontennaireEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Typecontennaire.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Typecontennaire findTypecontennaire(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Typecontennaire.class, id);
        } finally {
            em.close();
        }
    }

    public int getTypecontennaireCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Typecontennaire> rt = cq.from(Typecontennaire.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
