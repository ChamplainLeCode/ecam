/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.modelController;

import app.modelController.exceptions.NonexistentEntityException;
import app.modelController.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import app.models.Colis;
import app.models.Empotage;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author champlain
 */
public class EmpotageJpaController1 implements Serializable {

    public EmpotageJpaController1(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Empotage empotage) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Colis refColis = empotage.getRefColis();
            if (refColis != null) {
                refColis = em.getReference(refColis.getClass(), refColis.getRefColis());
                empotage.setRefColis(refColis);
            }
            em.persist(empotage);
            if (refColis != null) {
                refColis.getEmpotageList().add(empotage);
                refColis = em.merge(refColis);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findEmpotage(empotage.getRefEmpotage()) != null) {
                throw new PreexistingEntityException("Empotage " + empotage + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Empotage empotage) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Empotage persistentEmpotage = em.find(Empotage.class, empotage.getRefEmpotage());
            Colis refColisOld = persistentEmpotage.getRefColis();
            Colis refColisNew = empotage.getRefColis();
            if (refColisNew != null) {
                refColisNew = em.getReference(refColisNew.getClass(), refColisNew.getRefColis());
                empotage.setRefColis(refColisNew);
            }
            empotage = em.merge(empotage);
            if (refColisOld != null && !refColisOld.equals(refColisNew)) {
                refColisOld.getEmpotageList().remove(empotage);
                refColisOld = em.merge(refColisOld);
            }
            if (refColisNew != null && !refColisNew.equals(refColisOld)) {
                refColisNew.getEmpotageList().add(empotage);
                refColisNew = em.merge(refColisNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = empotage.getRefEmpotage();
                if (findEmpotage(id) == null) {
                    throw new NonexistentEntityException("The empotage with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Empotage empotage;
            try {
                empotage = em.getReference(Empotage.class, id);
                empotage.getRefEmpotage();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The empotage with id " + id + " no longer exists.", enfe);
            }
            Colis refColis = empotage.getRefColis();
            if (refColis != null) {
                refColis.getEmpotageList().remove(empotage);
                refColis = em.merge(refColis);
            }
            em.remove(empotage);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Empotage> findEmpotageEntities() {
        return findEmpotageEntities(true, -1, -1);
    }

    public List<Empotage> findEmpotageEntities(int maxResults, int firstResult) {
        return findEmpotageEntities(false, maxResults, firstResult);
    }

    private List<Empotage> findEmpotageEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Empotage.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Empotage findEmpotage(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Empotage.class, id);
        } finally {
            em.close();
        }
    }

    public int getEmpotageCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Empotage> rt = cq.from(Empotage.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
