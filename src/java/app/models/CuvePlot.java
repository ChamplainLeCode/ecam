/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "CUVE_PLOT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CuvePlot.findAll", query = "SELECT c FROM CuvePlot c")
    , @NamedQuery(name = "CuvePlot.findByRefCuveplot", query = "SELECT c FROM CuvePlot c WHERE c.refCuveplot = :refCuveplot")
    , @NamedQuery(name = "CuvePlot.findByDateDebut", query = "SELECT c FROM CuvePlot c WHERE c.dateDebut = :dateDebut")
    , @NamedQuery(name = "CuvePlot.findByDateFin", query = "SELECT c FROM CuvePlot c WHERE c.dateFin = :dateFin")})
public class CuvePlot implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_CUVEPLOT")
    private String refCuveplot;
    @Column(name = "DATE_DEBUT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateDebut;
    @Column(name = "DATE_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFin;
    @JoinColumn(name = "REF_CUVE", referencedColumnName = "REF_CUVE")
    @ManyToOne
    private Cuve refCuve;
    @JoinColumn(name = "REF_PLOT", referencedColumnName = "REF_PLOT")
    @ManyToOne
    private Plot refPlot;

    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    public CuvePlot() {
    }

    public CuvePlot(String refCuveplot) {
        this.refCuveplot = refCuveplot;
    }

    public String getRefCuveplot() {
        return refCuveplot;
    }

    public void setRefCuveplot(String refCuveplot) {
        this.refCuveplot = refCuveplot;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public Cuve getRefCuve() {
        return refCuve;
    }

    public void setRefCuve(Cuve refCuve) {
        this.refCuve = refCuve;
    }

    public Plot getRefPlot() {
        return refPlot;
    }

    public void setRefPlot(Plot refPlot) {
        this.refPlot = refPlot;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refCuveplot != null ? refCuveplot.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CuvePlot)) {
            return false;
        }
        CuvePlot other = (CuvePlot) object;
        if ((this.refCuveplot == null && other.refCuveplot != null) || (this.refCuveplot != null && !this.refCuveplot.equals(other.refCuveplot))) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
        return "{" + 
                "\"ref\" : \"" + refCuveplot + "\", "
                + "\"dateDebut\" :" + dateDebut.getTime() + ", "
                + "\"dateFin\" : " + (dateFin == null ? null : dateFin.getTime()) + ", "
                + "\"cuve\" : \"" + refCuve.getCuve() + "\", "
                + "\"plot\" : \"" + refPlot.getRefPlot() + "\""
                + "}";
    }    
}
