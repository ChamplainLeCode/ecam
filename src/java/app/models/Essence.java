/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "ESSENCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Essence.findAll", query = "SELECT e FROM Essence e")
    , @NamedQuery(name = "Essence.findByRefEssence", query = "SELECT e FROM Essence e WHERE e.refEssence = :refEssence")
    , @NamedQuery(name = "Essence.findByDensite", query = "SELECT e FROM Essence e WHERE e.densite = :densite")
    , @NamedQuery(name = "Essence.findByLibelle", query = "SELECT e FROM Essence e WHERE e.libelle = :libelle")
    , @NamedQuery(name = "Essence.findByPrixUnitaireAchat", query = "SELECT e FROM Essence e WHERE e.prixUnitaireAchat = :prixUnitaireAchat")
    , @NamedQuery(name = "Essence.findByPrixUnitaireVente", query = "SELECT e FROM Essence e WHERE e.prixUnitaireVente = :prixUnitaireVente")})
public class Essence implements Serializable {

    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "DENSITE")
    private Double densite;
    @Basic(optional = false)
    @Column(name = "CREATE_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt = new Date();
    @JoinTable(name = "TITRE_ESSENCE", joinColumns = {
        @JoinColumn(name = "ESSENCE", referencedColumnName = "REF_ESSENCE")}, inverseJoinColumns = {
        @JoinColumn(name = "TITRE", referencedColumnName = "NUM_TITRE")})
    @ManyToMany
    private List<Titre> titreList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refEssence")
    private List<DetailReception> detailReceptionList;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_ESSENCE")
    private String refEssence;
    @Column(name = "LIBELLE")
    private String libelle;
    @Column(name = "PRIX_UNITAIRE_ACHAT")
    private Long prixUnitaireAchat;
    @Column(name = "PRIX_UNITAIRE_VENTE")
    private Long prixUnitaireVente;
    @OneToMany(mappedBy = "refEssence")
    private List<ClassificationEss> classificationEssList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refEssence")
    private List<DetailCommande> detailCommandeList;

    public Essence() {
    }

    public Essence(String refEssence) {
        this.refEssence = refEssence;
    }

    public String getRefEssence() {
        return refEssence;
    }

    public void setRefEssence(String refEssence) {
        this.refEssence = refEssence;
    }


    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Long getPrixUnitaireAchat() {
        return prixUnitaireAchat;
    }

    public void setPrixUnitaireAchat(Long prixUnitaireAchat) {
        this.prixUnitaireAchat = prixUnitaireAchat;
    }

    public Long getPrixUnitaireVente() {
        return prixUnitaireVente;
    }

    public void setPrixUnitaireVente(Long prixUnitaireVente) {
        this.prixUnitaireVente = prixUnitaireVente;
    }

    @XmlTransient
    public List<ClassificationEss> getClassificationEssList() {
        return classificationEssList;
    }

    public void setClassificationEssList(List<ClassificationEss> classificationEssList) {
        this.classificationEssList = classificationEssList;
    }

    @XmlTransient
    public List<DetailCommande> getDetailCommandeList() {
        return detailCommandeList;
    }

    public void setDetailCommandeList(List<DetailCommande> detailCommandeList) {
        this.detailCommandeList = detailCommandeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refEssence != null ? refEssence.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Essence)) {
            return false;
        }
        Essence other = (Essence) object;
        if ((this.refEssence == null && other.refEssence != null) || (this.refEssence != null && !this.refEssence.equals(other.refEssence))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        try {
            JSONObject res = new JSONObject();
            res.put("ref", refEssence);
            res.put("libelle", libelle);
            res.put("densite", densite);
            res.put("prixUAchat", prixUnitaireAchat);
            res.put("prixUVente", prixUnitaireVente);
            return res.toString();
        } catch (JSONException ex) {
            return "{}";
        }
    }
    
    public String toBaseModel() {
        try {
            JSONObject res = new JSONObject();
            res.put("ref", refEssence);
            res.put("libelle", libelle);
            return "\""+libelle+"\""; //res.toString();
        } catch (JSONException ex) {
            return "{}";
        }
    }
    


    @XmlTransient
    public List<DetailReception> getDetailReceptionList() {
        return detailReceptionList;
    }

    public void setDetailReceptionList(List<DetailReception> detailReceptionList) {
        this.detailReceptionList = detailReceptionList;
    }

    public Double getDensite() {
        return densite;
    }

    public void setDensite(Double densite) {
        this.densite = densite;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    @XmlTransient
    public List<Titre> getTitreList() {
        return titreList;
    }

    public void setTitreList(List<Titre> titreList) {
        this.titreList = titreList;
    }
    
}
