/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "COMPAGNIE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Compagnie.findAll", query = "SELECT c FROM Compagnie c")
    , @NamedQuery(name = "Compagnie.findByRefCompagnie", query = "SELECT c FROM Compagnie c WHERE c.refCompagnie = :refCompagnie")
    , @NamedQuery(name = "Compagnie.findByLibelle", query = "SELECT c FROM Compagnie c WHERE c.libelle = :libelle")
    , @NamedQuery(name = "Compagnie.findByNumContribuable", query = "SELECT c FROM Compagnie c WHERE c.numContribuable = :numContribuable")})
public class Compagnie implements Serializable {

    @Basic(optional = false)
    @Column(name = "CREATE_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt = new Date();

    @OneToMany(mappedBy = "refCompagnie")
    private List<Embarquement> embarquement;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_COMPAGNIE")
    private String refCompagnie;
    @Column(name = "LIBELLE")
    private String libelle;
    @Column(name = "NUM_CONTRIBUABLE")
    private String numContribuable;
    @OneToMany(mappedBy = "refCompagnie")
    private List<Conteneur> conteneurList;

    public Compagnie() {
    }

    public Compagnie(String refCompagnie) {
        this.refCompagnie = refCompagnie;
    }

    public String getRefCompagnie() {
        return refCompagnie;
    }

    public void setRefCompagnie(String refCompagnie) {
        this.refCompagnie = refCompagnie;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getNumContribuable() {
        return numContribuable;
    }

    public void setNumContribuable(String numContribuable) {
        this.numContribuable = numContribuable;
    }

    @XmlTransient
    public List<Conteneur> getConteneurList() {
        return conteneurList;
    }

    public void setConteneurList(List<Conteneur> conteneurList) {
        this.conteneurList = conteneurList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refCompagnie != null ? refCompagnie.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Compagnie)) {
            return false;
        }
        Compagnie other = (Compagnie) object;
        if ((this.refCompagnie == null && other.refCompagnie != null) || (this.refCompagnie != null && !this.refCompagnie.equals(other.refCompagnie))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return ""
                + "{" 
                + "\"ref\": \"" + refCompagnie + "\", "
                + "\"libelle\": \"" + libelle + "\", "
                + "\"contribuable\": \"" + numContribuable + "\" "
                + '}';
    }


    @XmlTransient
    public List<Embarquement> getContennaireList() {
        return embarquement;
    }

    public void setContennaireList(List<Embarquement> embarquement) {
        this.embarquement = embarquement;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }
    
}
