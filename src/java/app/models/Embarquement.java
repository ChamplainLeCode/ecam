/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import app.modelController.ColisJpaController;
import app.modelController.ContennaireJpaController;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "EMBARQUEMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Embarquement.findAll", query = "SELECT e FROM Embarquement e")
    , @NamedQuery(name = "Embarquement.findByNumPlomb", query = "SELECT e FROM Embarquement e WHERE e.numPlomb = :numPlomb")
    , @NamedQuery(name = "Embarquement.findByDateChargement", query = "SELECT e FROM Embarquement e WHERE e.dateChargement = :dateChargement")
    , @NamedQuery(name = "Embarquement.findBySurface", query = "SELECT e FROM Embarquement e WHERE e.surface = :surface")
    , @NamedQuery(name = "Embarquement.findByVolumeContennaire", query = "SELECT e FROM Embarquement e WHERE e.volumeContennaire = :volumeContennaire")
    , @NamedQuery(name = "Embarquement.findByVolumeBrut", query = "SELECT e FROM Embarquement e WHERE e.volumeBrut = :volumeBrut")
    , @NamedQuery(name = "Embarquement.findByImmatriculation", query = "SELECT e FROM Embarquement e WHERE e.immatriculation = :immatriculation")
    , @NamedQuery(name = "Embarquement.findByPaysDestination", query = "SELECT e FROM Embarquement e WHERE e.paysDestination = :paysDestination")
    , @NamedQuery(name = "Embarquement.findByVilleDestination", query = "SELECT e FROM Embarquement e WHERE e.villeDestination = :villeDestination")
    , @NamedQuery(name = "Embarquement.findByLettreCamion", query = "SELECT e FROM Embarquement e WHERE e.lettreCamion = :lettreCamion")
    , @NamedQuery(name = "Embarquement.findByNbrColis", query = "SELECT e FROM Embarquement e WHERE e.nbrColis = :nbrColis")
    , @NamedQuery(name = "Embarquement.findByNbrPaquets", query = "SELECT e FROM Embarquement e WHERE e.nbrPaquets = :nbrPaquets")
    , @NamedQuery(name = "Embarquement.findByCreateAt", query = "SELECT e FROM Embarquement e WHERE e.createAt = :createAt")
    , @NamedQuery(name = "Embarquement.findByTelephone", query = "SELECT e FROM Embarquement e WHERE e.telephone = :telephone")
    , @NamedQuery(name = "Embarquement.findByPortDestination", query = "SELECT e FROM Embarquement e WHERE e.portDestination = :portDestination")
    , @NamedQuery(name = "Embarquement.findByPortDepart", query = "SELECT e FROM Embarquement e WHERE e.portDepart = :portDepart")
    , @NamedQuery(name = "Embarquement.findByAdresse", query = "SELECT e FROM Embarquement e WHERE e.adresse = :adresse")})
public class Embarquement implements Serializable {

    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SURFACE")
    private double surface;
    @Column(name = "VOLUME_CONTENNAIRE")
    private double volumeContennaire;
    @Column(name = "NBR_COLIS")
    private int nbrColis;
    @Column(name = "NBR_PAQUETS")
    private int nbrPaquets;
    @JoinTable(name = "EMBARQUEMENT_COLIS", joinColumns = {
        @JoinColumn(name = "EMBARQUEMENT_NUM_PLOMB", referencedColumnName = "NUM_PLOMB")}, inverseJoinColumns = {
        @JoinColumn(name = "COLISLIST_REF_COLIS", referencedColumnName = "REF_COLIS")})
    @ManyToMany
    private List<Colis> colisList;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "NUM_PLOMB")
    private String numPlomb;
    @Column(name = "DATE_CHARGEMENT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateChargement = new Date();
    @Column(name = "VOLUME_BRUT")
    private Double volumeBrut;
    @Column(name = "IMMATRICULATION")
    private String immatriculation;
    @Column(name = "PAYS_DESTINATION")
    private String paysDestination;
    @Column(name = "VILLE_DESTINATION")
    private String villeDestination;
    @Column(name = "LETTRE_CAMION")
    private String lettreCamion;
    @Basic(optional = false)
    @Column(name = "CREATE_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt = new Date() ;
    @Basic(optional = false)
    @Column(name = "TELEPHONE")
    private String telephone;
    @Basic(optional = false)
    @Column(name = "PORT_DESTINATION")
    private String portDestination;
    @Basic(optional = false)
    @Column(name = "PORT_DEPART")
    private String portDepart;
    @Basic(optional = false)
    @Column(name = "ADRESSE")
    private String adresse;
    @Column(name = "NOM_BATEAU")
    private String nomBateau;

    public String getNomBateau() {
        return nomBateau;
    }

    public void setNomBateau(String nomBateau) {
        this.nomBateau = nomBateau;
    }
    @JoinColumn(name = "REF_CHAUFFEUR", referencedColumnName = "REF_CHAUFFEUR")
    @ManyToOne
    private Chauffeur refChauffeur;
    @JoinColumn(name = "REF_CLIENT", referencedColumnName = "REF_CLIENT")
    @ManyToOne
    private Client refClient;
    @JoinColumn(name = "REF_COMPAGNIE", referencedColumnName = "REF_COMPAGNIE")
    @ManyToOne
    private Compagnie refCompagnie;
    @JoinColumn(name = "NUM_CONTENEUR", referencedColumnName = "NUM_CONTENEUR")
    @ManyToOne
    private Conteneur numConteneur;
    @JoinColumn(name = "REF_TRANSPORTEUR", referencedColumnName = "REF_TRANSPORTEUR")
    @ManyToOne
    private Transporteur refTransporteur;
    //@OneToMany(mappedBy = "numPlomb")
    //@OneToMany
    //private List<Colis> colisList;

    public Embarquement() {
    }

    public Embarquement(String numPlomb) {
        this.numPlomb = numPlomb;
    }

    public Embarquement(String numPlomb, Date createAt, String telephone, String portDestination, String portDepart, String adresse) {
        this.numPlomb = numPlomb;
        this.createAt = createAt;
        this.telephone = telephone;
        this.portDestination = portDestination;
        this.portDepart = portDepart;
        this.adresse = adresse;
    }

    public String getNumPlomb() {
        return numPlomb;
    }

    public void setNumPlomb(String numPlomb) {
        this.numPlomb = numPlomb;
    }

    public Date getDateChargement() {
        return dateChargement;
    }

    public void setDateChargement(Date dateChargement) {
        this.dateChargement = dateChargement;
    }


    public Double getVolumeContennaire() {
        return volumeContennaire;
    }

    public void setVolumeContennaire(Double volumeContennaire) {
        this.volumeContennaire = volumeContennaire;
    }

    public Double getVolumeBrut() {
        return volumeBrut;
    }

    public void setVolumeBrut(Double volumeBrut) {
        this.volumeBrut = volumeBrut;
    }

    public String getImmatriculation() {
        return immatriculation;
    }

    public void setImmatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
    }

    public String getPaysDestination() {
        return paysDestination;
    }

    public void setPaysDestination(String paysDestination) {
        this.paysDestination = paysDestination;
    }

    public String getVilleDestination() {
        return villeDestination;
    }

    public void setVilleDestination(String villeDestination) {
        this.villeDestination = villeDestination;
    }

    public String getLettreCamion() {
        return lettreCamion;
    }

    public void setLettreCamion(String lettreCamion) {
        this.lettreCamion = lettreCamion;
    }

    public int getNbrColis() {
        return nbrColis;
    }

    public void setNbrColis(int nbrColis) {
        this.nbrColis = nbrColis;
    }

    public int getNbrPaquets() {
        return nbrPaquets;
    }

    public void setNbrPaquets(int nbrPaquets) {
        this.nbrPaquets = nbrPaquets;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPortDestination() {
        return portDestination;
    }

    public void setPortDestination(String portDestination) {
        this.portDestination = portDestination;
    }

    public String getPortDepart() {
        return portDepart;
    }

    public void setPortDepart(String portDepart) {
        this.portDepart = portDepart;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Chauffeur getRefChauffeur() {
        return refChauffeur;
    }

    public void setRefChauffeur(Chauffeur refChauffeur) {
        this.refChauffeur = refChauffeur;
    }

    public Client getRefClient() {
        return refClient;
    }

    public void setRefClient(Client refClient) {
        this.refClient = refClient;
    }

    public Compagnie getRefCompagnie() {
        return refCompagnie;
    }

    public void setRefCompagnie(Compagnie refCompagnie) {
        this.refCompagnie = refCompagnie;
    }

    public Conteneur getNumConteneur() {
        return numConteneur;
    }

    public void setNumConteneur(Conteneur numConteneur) {
        this.numConteneur = numConteneur;
    }

    public Transporteur getRefTransporteur() {
        return refTransporteur;
    }

    public void setRefTransporteur(Transporteur refTransporteur) {
        this.refTransporteur = refTransporteur;
    }

    @XmlTransient
    public List<Colis> getColisList() {
        return colisList;
    }

    public void setColisList(List<Colis> colisList) {
        this.colisList = colisList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numPlomb != null ? numPlomb.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Embarquement)) {
            return false;
        }
        Embarquement other = (Embarquement) object;
        if ((this.numPlomb == null && other.numPlomb != null) || (this.numPlomb != null && !this.numPlomb.equals(other.numPlomb))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
           fetchColis();
        return "{" 
            + "\"ref\" : \"" + numPlomb + "\", "
            + "\"plomb\" : \"" + numPlomb + "\", "
            + "\"chauffeur\" : " + refChauffeur + ", "
            + "\"compagnie\" : " + refCompagnie + ", "
            + "\"conteneur\" : " + numConteneur + ", "
            + "\"client\" : " + refClient+", "
            + "\"transporteur\" : " + refTransporteur+ ", "
            + "\"immatriculation\" : \"" + immatriculation + "\", "
            + "\"pays\" : \"" + paysDestination + "\", "
            + "\"ville\" : \"" + villeDestination + "\", "
            + "\"date\" : " + dateChargement.getTime() + ", "
            + "\"lettre\" : \"" + lettreCamion + "\", "
            + "\"volume\" : \"" + volumeContennaire + "\", "
            + "\"volumeBrute\" : \"" + volumeBrut + "\", "
            + "\"nbrPaquets\" : \"" + nbrPaquets + "\", "
            + "\"nbrColis\" : \"" + nbrColis + "\", "
            + "\"surface\" : \"" + surface + "\", "
            + "\"epaisseur\" : \"" + getEpaisseur() + "\", "
            + "\"qualite\" : \"" + getQualite() + "\", "
            + "\"adresse\" : \"" + this.adresse + "\", "
            + "\"portDepart\" : \"" + this.portDepart + "\", "
            + "\"portDestination\" : \"" + this.portDestination + "\", "
            + "\"telephone\" : \"" + this.telephone + "\", "
            + "\"bateau\" : \"" + this.nomBateau + "\", "
            + "\"colis\" : " + colisList + "}";
    }

    private void fetchColis() {
        colisList = new ColisJpaController(Database.getEntityManager()).colisForContenaire(numPlomb);
    }
    public void loadContenaire() {
        colisList = new ColisJpaController(Database.getEntityManager()).colisForContenaire(this.numPlomb);
        System.out.println(colisList);
    }

    public void updateContentNumber(){
        nbrPaquets = 0;
        if(colisList != null && colisList.size()>0){
            nbrColis = colisList.size();
            for(int i=0; i<colisList.size(); nbrPaquets+=colisList.get(i++).getNbrePaquet());
            try {
                new ContennaireJpaController(Database.getEntityManager()).edit(this);
            } catch (Exception ex) {
                ex.printStackTrace();
            }            
        }
        
    }
    public float calculerMasseColis(){
        float masse = 0f;
        if(colisList == null || colisList.isEmpty()){
            fetchColis();
        }
        for(Colis e: colisList)
            masse += e.getPoidNet();
        
        return masse;
    }

    public String getQualite() {
        String qualite = "[";
        if(colisList != null && !colisList.isEmpty())
            for(Colis c : colisList)
                qualite += c.getQualite()+", ";
        qualite += "]";
        return qualite;
    }

    public String getEpaisseur() {
        String epaisseur = "[";
        if(colisList != null && !colisList.isEmpty())
            for(Colis c : colisList)
                epaisseur += c.getEpaisseur()+", ";
        epaisseur += "]";
        return epaisseur;
    }

    public double getSurface() {
        return surface;
    }

    public void setSurface(double surface) {
        this.surface = surface;
    }


    public void setVolumeContennaire(double volumeContennaire) {
        this.volumeContennaire = volumeContennaire;
    }


    
}
