/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author champlain
 */
final public class Database {
    
    protected static EntityManagerFactory emf;

    protected Database(){
        
    }
    
    public static EntityManagerFactory getEntityManager() {
        return emf == null ?  Persistence.createEntityManagerFactory("ECAMWEBPU") : emf ;
    }
    
}
