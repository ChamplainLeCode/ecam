/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "TRANSPORTEUR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Transporteur.findAll", query = "SELECT t FROM Transporteur t")
    , @NamedQuery(name = "Transporteur.findByRefTransporteur", query = "SELECT t FROM Transporteur t WHERE t.refTransporteur = :refTransporteur")
    , @NamedQuery(name = "Transporteur.findByNumContribuable", query = "SELECT t FROM Transporteur t WHERE t.numContribuable = :numContribuable")})
public class Transporteur implements Serializable {

    @Basic(optional = false)
    @Column(name = "CREATE_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt = new Date();

    @OneToMany(mappedBy = "refTransporteur")
    private List<Embarquement> contennaireList;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_TRANSPORTEUR")
    private String refTransporteur;
    @Column(name = "NUM_CONTRIBUABLE")
    private String numContribuable;
    @Column(name = "NOM")
    private String nom;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Transporteur() {
    }

    public Transporteur(String refTransporteur) {
        this.refTransporteur = refTransporteur;
    }

    public String getRefTransporteur() {
        return refTransporteur;
    }

    public void setRefTransporteur(String refTransporteur) {
        this.refTransporteur = refTransporteur;
    }

    public String getNumContribuable() {
        return numContribuable;
    }

    public void setNumContribuable(String numContribuable) {
        this.numContribuable = numContribuable;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refTransporteur != null ? refTransporteur.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transporteur)) {
            return false;
        }
        Transporteur other = (Transporteur) object;
        if ((this.refTransporteur == null && other.refTransporteur != null) || (this.refTransporteur != null && !this.refTransporteur.equals(other.refTransporteur))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{\"ref\": \"" + refTransporteur + "\", \"contribuable\": \"" + numContribuable + "\", \"nom\": \"" + nom + "\"}";
    }

    @XmlTransient
    public List<Embarquement> getContennaireList() {
        return contennaireList;
    }

    public void setContennaireList(List<Embarquement> contennaireList) {
        this.contennaireList = contennaireList;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    
}
