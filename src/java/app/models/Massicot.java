/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "MASSICOT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Massicot.findAll", query = "SELECT m FROM Massicot m")
    , @NamedQuery(name = "Massicot.findByRefMassicot", query = "SELECT m FROM Massicot m WHERE m.refMassicot = :refMassicot")
    , @NamedQuery(name = "Massicot.findByLibelle", query = "SELECT m FROM Massicot m WHERE m.libelle = :libelle")})
public class Massicot implements Serializable {

    private static final long serialVersionUID = 1L;

    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    @Id
    @Basic(optional = false)
    @Column(name = "REF_MASSICOT")
    private String refMassicot;
    @Column(name = "LIBELLE")
    private String libelle;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refMassicot")
    private List<Massicotpaquet> massicotpaquetList;

    public Massicot() {
    }

    public Massicot(String refMassicot) {
        this.refMassicot = refMassicot;
    }

    public String getRefMassicot() {
        return refMassicot;
    }

    public void setRefMassicot(String refMassicot) {
        this.refMassicot = refMassicot;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @XmlTransient
    public List<Massicotpaquet> getMassicotpaquetList() {
        return massicotpaquetList;
    }

    public void setMassicotpaquetList(List<Massicotpaquet> massicotpaquetList) {
        this.massicotpaquetList = massicotpaquetList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refMassicot != null ? refMassicot.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Massicot)) {
            return false;
        }
        Massicot other = (Massicot) object;
        if ((this.refMassicot == null && other.refMassicot != null) || (this.refMassicot != null && !this.refMassicot.equals(other.refMassicot))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{" + "\"ref\" : \"" + refMassicot + "\", \"libelle\": \"" + libelle + "\"}";
    }

}
