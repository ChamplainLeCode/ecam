/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import app.controllers.IndexController;
import app.modelController.RouteJpaController;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "PERSONNEL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Personnel.findAll", query = "SELECT p FROM Personnel p")
    , @NamedQuery(name = "Personnel.findByCniPersonnel", query = "SELECT p FROM Personnel p WHERE p.cniPersonnel = :cniPersonnel")
    , @NamedQuery(name = "Personnel.findByFonction", query = "SELECT p FROM Personnel p WHERE p.fonction = :fonction")
    , @NamedQuery(name = "Personnel.findByNomPersonnel", query = "SELECT p FROM Personnel p WHERE p.nomPersonnel = :nomPersonnel")
    , @NamedQuery(name = "Personnel.findByPrenomPersonnel", query = "SELECT p FROM Personnel p WHERE p.prenomPersonnel = :prenomPersonnel")})
public class Personnel implements Serializable {

    @Basic(optional = false)
    @Column(name = "CAN_ADD")
    private Character canAdd;
    @Column(name = "CAN_EDIT")
    private Character canEdit;
    @Basic(optional = false)
    @Column(name = "CAN_DELETE")
    private Character canDelete;
    @Basic(optional = false)
    @Column(name = "CAN_PRINT")
    private Character canPrint;
    @Basic(optional = false)
    
    @Column(name = "STATUT")
    private Character statut;
    @JoinColumn(name = "FONCTION", referencedColumnName = "REF_PRIVILEGE")
    @ManyToOne
    private Privilege fonction;

    private static final long serialVersionUID = 1L;
    public static Character ONLINE = 'O';
    public static Character OFFLINE = 'F';
    public static Character BLOCKLINE = 'B';

    @Id
    @Basic(optional = false)
    @Column(name = "CNI_PERSONNEL")
    private String cniPersonnel;
    @Column(name = "NOM_PERSONNEL")
    private String nomPersonnel;
    @Column(name = "PRENOM_PERSONNEL")
    private String prenomPersonnel;
    @Column(name="PASSWORD")
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password; 
    }

    public char getCanAdd() {
        return canAdd;
    }

    public void setCanAdd(char canAdd) {
        this.canAdd = canAdd;
    }

    public char getCanEdit() {
        return canEdit;
    }

    public void setCanEdit(char canEdit) {
        this.canEdit = canEdit;
    }

    public char getCanDelete() {
        return canDelete;
    }

    public void setCanDelete(char canDelete) {
        this.canDelete = canDelete;
    }

    public char getCanPrint() {
        return canPrint;
    }

    public void setCanPrint(char canPrint) {
        this.canPrint = canPrint;
    }
    @OneToMany(mappedBy = "refPersonnel")
    private List<Detailequipe> detailequipeList;

    public Personnel() {
    }

    public Personnel(String cniPersonnel) {
        this.cniPersonnel = cniPersonnel;
    }

    public String getCniPersonnel() {
        return cniPersonnel;
    }

    public void setCniPersonnel(String cniPersonnel) {
        this.cniPersonnel = cniPersonnel;
    }

    public String getNomPersonnel() {
        return nomPersonnel;
    }

    public void setNomPersonnel(String nomPersonnel) {
        this.nomPersonnel = nomPersonnel;
    }

    public String getPrenomPersonnel() {
        return prenomPersonnel;
    }

    public void setPrenomPersonnel(String prenomPersonnel) {
        this.prenomPersonnel = prenomPersonnel;
    }


    @XmlTransient
    public List<Detailequipe> getDetailequipeList() {
        return detailequipeList;
    }

    public void setDetailequipeList(List<Detailequipe> detailequipeList) {
        this.detailequipeList = detailequipeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cniPersonnel != null ? cniPersonnel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Personnel)) {
            return false;
        }
        Personnel other = (Personnel) object;
        if ((this.cniPersonnel == null && other.cniPersonnel != null) || (this.cniPersonnel != null && !this.cniPersonnel.equals(other.cniPersonnel))) {
            return false;
        }
        return true;
    }
    @Override
    public String toString() {
        return "{" + ""
                + "\"matricule\": \"" + cniPersonnel + "\", "
                + "\"nom\": \"" + nomPersonnel + "\", "
                + "\"prenom\": \"" + prenomPersonnel + "\", "
                + "\"fonction\": " + fonction + ","
                + "\"statut\": \"" + statut + "\","
                + "\"can_add\": \""+canAdd+"\","
                + "\"can_edit\": \""+canEdit+"\","
                + "\"can_delete\": \""+canDelete+"\", "
                + "\"can_print\": \""+canPrint+"\""
                + "}";
    }

    public static boolean accessAllow(Personnel p, String route) {
        
        if(p==null || route == null || p.getFonction() == null)
            return false;
        if(route.equals("/"))
            return true;
        p.getFonction().loadRoute();
        for(Route r : p.getFonction().getRouteList())
            if(r.getPath().equals(route)){
                return true;
            }
        System.out.println("no route");
        return false;

/*        return !(
            p == null || 
            route == null || 
            route.length() == 0 || 
            (
                route.equalsIgnoreCase(IndexController.REGISTER_PAGE) 
  //             && !p.getFonction().equalsIgnoreCase("INFORMATICIEN")
            )
        );*/
    }


    public void setCanAdd(Character canAdd) {
        this.canAdd = canAdd;
    }

    public void setCanEdit(Character canEdit) {
        this.canEdit = canEdit;
    }


    public void setCanDelete(Character canDelete) {
        this.canDelete = canDelete;
    }


    public void setCanPrint(Character canPrint) {
        this.canPrint = canPrint;
    }

    public Character getStatut() {
        return statut;
    }

    public void setStatut(Character statut) {
        this.statut = statut;
    }

    public Privilege getFonction() {
        return fonction;
    }

    public void setFonction(Privilege fonction) {
        this.fonction = fonction;
    }

    
}
