/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "PORT", catalog = "", schema = "ECAM", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"NOM_PORT"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Port.findAll", query = "SELECT p FROM Port p")
    , @NamedQuery(name = "Port.findByRefPort", query = "SELECT p FROM Port p WHERE p.refPort = :refPort")
    , @NamedQuery(name = "Port.findByNomPort", query = "SELECT p FROM Port p WHERE p.nomPort = :nomPort")
    , @NamedQuery(name = "Port.findByNomVille", query = "SELECT p FROM Port p WHERE p.nomVille = :nomVille")
    , @NamedQuery(name = "Port.findByNomPays", query = "SELECT p FROM Port p WHERE p.nomPays = :nomPays")})
public class Port implements Serializable {

    private static final Long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "REF_PORT", nullable = false, precision = 38, scale = 0)
    private Long refPort;
    @Basic(optional = false)
    @Column(name = "NOM_PORT", nullable = false, length = 50)
    private String nomPort;
    @Basic(optional = false)
    @Column(name = "NOM_VILLE", nullable = false, length = 50)
    private String nomVille;
    @Basic(optional = false)
    @Column(name = "NOM_PAYS", nullable = false, length = 50)
    private String nomPays;

    public Port() {
    }

    public Port(Long refPort) {
        this.refPort = refPort;
    }

    public Port(Long refPort, String nomPort, String nomVille, String nomPays) {
        this.refPort = refPort;
        this.nomPort = nomPort;
        this.nomVille = nomVille;
        this.nomPays = nomPays;
    }

    public Long getRefPort() {
        return refPort;
    }

    public void setRefPort(Long refPort) {
        this.refPort = refPort;
    }

    public String getNomPort() {
        return nomPort;
    }

    public void setNomPort(String nomPort) {
        this.nomPort = nomPort;
    }

    public String getNomVille() {
        return nomVille;
    }

    public void setNomVille(String nomVille) {
        this.nomVille = nomVille;
    }

    public String getNomPays() {
        return nomPays;
    }

    public void setNomPays(String nomPays) {
        this.nomPays = nomPays;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refPort != null ? refPort.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Port)) {
            return false;
        }
        Port other = (Port) object;
        if ((this.refPort == null && other.refPort != null) || (this.refPort != null && !this.refPort.equals(other.refPort))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{\"ref\": \"" + refPort + "\", \"nom\": \""+nomPort+"\", \"ville\": \""+nomVille+"\", \"pays\": \""+nomPays+"\"}";
    }
    
}
