/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "BILLONNAGE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Billonnage.findAll", query = "SELECT b FROM Billonnage b")
    , @NamedQuery(name = "Billonnage.findByRefBillonnage", query = "SELECT b FROM Billonnage b WHERE b.refBillonnage = :refBillonnage")
    , @NamedQuery(name = "Billonnage.findByNbreBillon", query = "SELECT b FROM Billonnage b WHERE b.nbreBillon = :nbreBillon")
    , @NamedQuery(name = "Billonnage.findByNumBille", query = "SELECT b FROM Billonnage b WHERE b.numBille = :numBille")
    , @NamedQuery(name = "Billonnage.findByNumTravail", query = "SELECT b FROM Billonnage b WHERE b.numTravail = :numTravail")
    , @NamedQuery(name = "Billonnage.findByRefEquippe", query = "SELECT b FROM Billonnage b WHERE b.refEquippe = :refEquippe")})
public class Billonnage implements Serializable {

    @Column(name = "NBRE_BILLON")
    private int nbreBillon;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_BILLONNAGE")
    private String refBillonnage;
    @Column(name = "NUM_BILLE")
    private String numBille;
    @Column(name = "NUM_TRAVAIL")
    private String numTravail;
    @Column(name = "REF_EQUIPPE")
    private String refEquippe;
    @JoinColumn(name = "REF_PARC", referencedColumnName = "REF_PARC")
    @ManyToOne
    private Parc refParc;
    @OneToMany(mappedBy = "refBillonnage")
    private List<Billon> billonList;

    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    public Billonnage() {
    }

    public Billonnage(String refBillonnage) {
        this.refBillonnage = refBillonnage;
    }

    public String getRefBillonnage() {
        return refBillonnage;
    }

    public void setRefBillonnage(String refBillonnage) {
        this.refBillonnage = refBillonnage;
    }

    public Integer getNbreBillon() {
        return nbreBillon;
    }

    public void setNbreBillon(Integer nbreBillon) {
        this.nbreBillon = nbreBillon;
    }

    public String getNumBille() {
        return numBille;
    }

    public void setNumBille(String numBille) {
        this.numBille = numBille;
    }

    public String getNumTravail() {
        return numTravail;
    }

    public void setNumTravail(String numTravail) {
        this.numTravail = numTravail;
    }

    public String getRefEquippe() {
        return refEquippe;
    }

    public void setRefEquippe(String refEquippe) {
        this.refEquippe = refEquippe;
    }

    public Parc getRefParc() {
        return refParc;
    }

    public void setRefParc(Parc refParc) {
        this.refParc = refParc;
    }

    @XmlTransient
    public List<Billon> getBillonList() {
        return billonList;
    }

    public void setBillonList(List<Billon> billonList) {
        this.billonList = billonList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refBillonnage != null ? refBillonnage.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Billonnage)) {
            return false;
        }
        Billonnage other = (Billonnage) object;
        if ((this.refBillonnage == null && other.refBillonnage != null) || (this.refBillonnage != null && !this.refBillonnage.equals(other.refBillonnage))) {
            return false;
        }
        return true;
    }



    @Override
    public String toString() {
        return "{" + "\"ref\" : \"" + refBillonnage + "\", \"bille\": \"" + numBille + "\", \"nbrBillon\" :\"" + nbreBillon + "\", \"equipe\" : \"" + refEquippe + "\", \"numTravail\" : \"" + numTravail + "\", \"parc\" : \"" + (refParc == null ? "{}" : refParc.getRefTypeparc().getLibelle()) + "\"}";
    }    

    public void setNbreBillon(int nbreBillon) {
        this.nbreBillon = nbreBillon;
    }


}
