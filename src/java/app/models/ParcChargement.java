/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import app.modelController.TitreJpaController;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "PARC_CHARGEMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ParcChargement.findAll", query = "SELECT p FROM ParcChargement p")
    , @NamedQuery(name = "ParcChargement.findByRefParc", query = "SELECT p FROM ParcChargement p WHERE p.refParc = :refParc")
    , @NamedQuery(name = "ParcChargement.findByLibelle", query = "SELECT p FROM ParcChargement p WHERE p.libelle = :libelle")})

public class ParcChargement implements Serializable {

    @OneToMany(mappedBy = "refParc")
    private List<Titre> titreList;
/*    @JoinColumn(name = "NUM_TITRE", referencedColumnName = "NUM_TITRE")
    @ManyToOne
    private Titre numTitre;*/

    @JoinColumn(name = "REF_TYPEPARC", referencedColumnName = "REF_TYPEPARC")
    @ManyToOne
    private TypeParc refTypeparc;

    @Basic(optional = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    @OneToMany(mappedBy = "refParc")
    private List<Commande> commandeList;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_PARC")
    private String refParc;
    @Column(name = "LIBELLE")
    private String libelle;
    @Column(name = "CERTIFICAT")
    private String certificat;
    @JoinColumn(name = "REF_FOURNISSEUR", referencedColumnName = "REF_FOURNISSEUR")
    @ManyToOne
    private Fournisseur refFournisseur;

    public ParcChargement() {
    }

    public String getCertificat() {
        return certificat;
    }

    public void setCertificat(String certificat) {
        this.certificat = certificat;
    }

    public ParcChargement(String refParc) {
        this.refParc = refParc;
    }

    public String getRefParc() {
        return refParc;
    }

    public void setRefParc(String refParc) {
        this.refParc = refParc;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Fournisseur getRefFournisseur() {
        return refFournisseur;
    }

    public void setRefFournisseur(Fournisseur refFournisseur) {
        this.refFournisseur = refFournisseur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refParc != null ? refParc.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParcChargement)) {
            return false;
        }
        ParcChargement other = (ParcChargement) object;
        if ((this.refParc == null && other.refParc != null) || (this.refParc != null && !this.refParc.equals(other.refParc))) {
            return false;
        }
        return true;
    }
    
    public void initTitre(){
        titreList =  new TitreJpaController(Database.getEntityManager()).findTitreByRefParc(this.refParc);
    }
    
    @Override
    public String toString() {
        //initTitre();
        return "{" + ""
                + "\"ref\": \"" + refParc + "\", "
                + "\"libelle\": \"" + libelle + "\", "
                + "\"origine\": " + refTypeparc + ", "
                + "\"titre\": " + new LinkedList() + ", "
                + "\"certificat\": \"" + certificat + "\", "
                + "\"fournisseur\": "+(refFournisseur == null ? null : refFournisseur.baseString())+"}";
    }

    @XmlTransient
    public List<Commande> getCommandeList() {
        return commandeList;
    }

    public void setCommandeList(List<Commande> commandeList) {
        this.commandeList = commandeList;
    }

    public TypeParc getRefTypeparc() {
        return refTypeparc;
    }

    public void setRefTypeparc(TypeParc refTypeparc) {
        this.refTypeparc = refTypeparc;
    }


    @XmlTransient 
    public List<Titre> getTitreList() {
        return titreList;
    }

    public void setTitreList(List<Titre> titreList) {
        this.titreList = titreList;
    }
/*
    public Titre getNumTitre() {
        return numTitre;
    }

    public void setNumTitre(Titre numTitre) {
        this.numTitre = numTitre;
    }
  */  
}
