/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "PAQUET")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Paquet.findAll", query = "SELECT p FROM Paquet p")
    , @NamedQuery(name = "Paquet.findByRefPaquet", query = "SELECT p FROM Paquet p WHERE p.refPaquet = :refPaquet")
    , @NamedQuery(name = "Paquet.findByNbreFeuille", query = "SELECT p FROM Paquet p WHERE p.nbreFeuille = :nbreFeuille")
    , @NamedQuery(name = "Paquet.findByNumTravail", query = "SELECT p FROM Paquet p WHERE p.numTravail = :numTravail")})
public class Paquet implements Serializable {

    @Column(name = "NBRE_FEUILLE")
    private int nbreFeuille;
    @Column(name = "REF_CODEBAR")
    private String refCodebar;

    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    @JoinColumn(name = "REF_COLIS", referencedColumnName = "REF_COLIS")
    @ManyToOne
    private Colis refColis;

    @Basic(optional = false)
    @Column(name = "LONGUEUR")
    private Double longueur;
    @Basic(optional = false)
    @Column(name = "LARGEUR")
    private Double largeur;
    @Basic(optional = false)
    @Column(name = "EPAISSEUR")
    private String epaisseur;
    
    private static final long serialVersionUID = 1L;

    @Column(name = "QUALITE_PAQUET")
    private String qualite = "";

    @Id
    @Basic(optional = false)
    @Column(name = "REF_PAQUET")
    private String refPaquet;
    @Column(name = "NUM_TRAVAIL")
    private String numTravail;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refPaquet")
    private List<Massicotpaquet> massicotpaquetList;
    @JoinColumn(name = "REF_PLOT", referencedColumnName = "REF_PLOT")
    @ManyToOne
    private Plot refPlot;

    public Paquet() {
    }

    public Paquet(String refPaquet) {
        this.refPaquet = refPaquet;
    }

    public String getRefPaquet() {
        return refPaquet;
    }

    public void setRefPaquet(String refPaquet) {
        this.refPaquet = refPaquet;
    }

    public Integer getNbreFeuille() {
        return nbreFeuille;
    }

    public void setNbreFeuille(Integer nbreFeuille) {
        this.nbreFeuille = nbreFeuille;
    }

    public String getNumTravail() {
        return numTravail;
    }

    public void setNumTravail(String numTravail) {
        this.numTravail = numTravail;
    }

    public String getQualite() {
        return qualite;
    }

    public void setQualite(String qualite) {
        this.qualite = qualite;
    }

    @XmlTransient
    public List<Massicotpaquet> getMassicotpaquetList() {
        return massicotpaquetList;
    }

    public void setMassicotpaquetList(List<Massicotpaquet> massicotpaquetList) {
        this.massicotpaquetList = massicotpaquetList;
    }

    public Plot getRefPlot() {
        return refPlot;
    }

    public void setNbreFeuille(int nbreFeuille) {
        this.nbreFeuille = nbreFeuille;
    }

    public void setLongueur(Double longueur) {
        this.longueur = longueur;
    }

    public void setLargeur(Double largeur) {
        this.largeur = largeur;
    }

    public void setEpaisseur(String epaisseur) {
        this.epaisseur = epaisseur;
    }
    public static double convertEpaisseur(String epaisseur){
        try {
            ScriptEngine script = new ScriptEngineManager().getEngineByName("JavaScript");
            return Double.parseDouble(script.eval(epaisseur).toString());
        } catch (ScriptException ex) {
            return 0D;
        }
    }
    public double getEpaisseur() {
        try {
            ScriptEngine script = new ScriptEngineManager().getEngineByName("JavaScript");
            return Double.parseDouble(script.eval(epaisseur).toString());
        } catch (ScriptException ex) {
            return 0D;
        }
    }

    public void setRefPlot(Plot refPlot) {
        this.refPlot = refPlot;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refPaquet != null ? refPaquet.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Paquet)) {
            return false;
        }
        Paquet other = (Paquet) object;
        if ((this.refPaquet == null && other.refPaquet != null) || (this.refPaquet != null && !this.refPaquet.equals(other.refPaquet))) {
            return false;
        }
        return true;
    }
    
    public double getSurface(){
        return longueur*largeur*nbreFeuille/Math.pow(100, 2);
    }
    
    public double getVolume(){
        try {
            ScriptEngine script = new ScriptEngineManager().getEngineByName("JavaScript");
            return getSurface()*(Float.parseFloat(script.eval(epaisseur).toString())/10);
        } catch (ScriptException ex) {
            return getSurface();
        }
    }
    
    @Override
    public String toString() {
        try {
            double surface;
            ScriptEngine script = new ScriptEngineManager().getEngineByName("JavaScript");
            return "{" + "\"ref\": \"" + refPaquet + "\", "
                    + "\"nbreFeuille\": \"" + nbreFeuille + "\", "
                    + "\"numTravail\": \"" + numTravail + "\", "
                    + "\"longueur\": \"" + longueur + "\", "
                    + "\"largeur\": \"" + largeur + "\", "
                    + "\"qualite\": \"" + qualite + "\", "
                    + "\"surface\": \"" + (surface = longueur*largeur*nbreFeuille/Math.pow(100, 2)) + "\", "
                    + "\"volume\": \"" + (surface*Float.parseFloat(script.eval(epaisseur).toString())/10)+ "\", " // on fait surface*epai/10 car l'épaisseur est en mm et la surface en cm² donc il faut convertir l'épaisseur en cm soit x/10
                    + "\"codeBare\": \"" + this.refCodebar + "\", "
                    + "\"createAt\": " + this.create_at.getTime() + ", "
                    + "\"colis\": \"" + (refColis == null ? ' ' : refColis.getRefColis()) + "\", "
                    + "\"plot\" : " + refPlot + "}";
        } catch (ScriptException ex) {
            double surface;
            return "{" + "\"ref\": \"" + refPaquet + "\", "
                    + "\"nbreFeuille\": \"" + nbreFeuille + "\", "
                    + "\"numTravail\": \"" + numTravail + "\", "
                    + "\"longueur\": \"" + longueur + "\", "
                    + "\"largeur\": \"" + largeur + "\", "                   
                    + "\"createAt\": " + this.create_at.getTime() + ", "
                    + "\"surface\": \"" + (surface = longueur*largeur*nbreFeuille/Math.pow(100, 2)) + "\", "
                    + "\"volume\": 0, "
                    + "\"codeBare\": \"" + this.refCodebar + "\", "
                    + "\"colis\": \"" +(refColis == null ? ' ' : refColis.getRefColis()) + "\", "
                    + "\"plot\" : " + refPlot + "}";
        }
    }

    public String getRefCodebar() {
        return refCodebar;
    }

    public void setRefCodebar(String refCodebar) {
        this.refCodebar = refCodebar;
    }

    public Colis getRefColis() {
        return refColis;
    }

    public void setRefColis(Colis refColis) {
        this.refColis = refColis;
    }
    
}
