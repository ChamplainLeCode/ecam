/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import app.modelController.TitreJpaController;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "TITRE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Titre.findAll", query = "SELECT t FROM Titre t")
    , @NamedQuery(name = "Titre.findByConcession", query = "SELECT t FROM Titre t WHERE t.concession = :concession")
    , @NamedQuery(name = "Titre.findByNumTitre", query = "SELECT t FROM Titre t WHERE t.numTitre = :numTitre")
    , @NamedQuery(name = "Titre.findByDateDebut", query = "SELECT t FROM Titre t WHERE t.dateDebut = :dateDebut")
    , @NamedQuery(name = "Titre.findByDateFin", query = "SELECT t FROM Titre t WHERE t.dateFin = :dateFin")
    , @NamedQuery(name = "Titre.findByRefParc", query = "SELECT t FROM Titre t WHERE t.refParc = :refParc")
    , @NamedQuery(name = "Titre.findByCreateAt", query = "SELECT t FROM Titre t WHERE t.createAt = :createAt")})
public class Titre implements Serializable {

    @JoinColumn(name = "REF_PARC", referencedColumnName = "REF_PARC")
    @ManyToOne
    private ParcChargement refParc;
    
//    @OneToMany(mappedBy = "refParc")
//    private List<ParcChargement> parcChargementList;

    @ManyToMany(mappedBy = "titreList")
    private List<Essence> essenceList;

    private static final long serialVersionUID = 1L;
    @Column(name = "CONCESSION")
    private String concession;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "NUM_TITRE")
    private String numTitre;
    @Column(name = "DATE_DEBUT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateDebut;
    @Column(name = "DATE_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFin = new Date();
    
    @Column(name = "CREATE_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt = new Date();

    public Titre() {
    }

    public Titre(String numTitre) {
        this.numTitre = numTitre;
    }

    public String getConcession() {
        return concession;
    }

    public void setConcession(String concession) {
        this.concession = concession;
    }

    public String getNumTitre() {
        return numTitre;
    }

    public void setNumTitre(String numTitre) {
        this.numTitre = numTitre;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numTitre != null ? numTitre.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Titre)) {
            return false;
        }
        Titre other = (Titre) object;
        if ((this.numTitre == null && other.numTitre != null) || (this.numTitre != null && !this.numTitre.equals(other.numTitre))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{" 
                + "\"essences\": " + new LinkedList(essenceList) +", "
                + "\"concession\": \"" + concession +"\", "
                + "\"numTitre\": \"" + numTitre +"\", "
                + "\"dateDebut\": " + dateDebut.getTime() +", "
                + "\"dateFin\": " + dateFin.getTime() +", "
                + "\"createAt\": \"" + createAt + "\"}";
    }


    @XmlTransient
    public List<Essence> getEssenceList() {
        return essenceList;
    }

    public void setEssenceList(List<Essence> essenceList) {
        this.essenceList = essenceList;
    }

    public ParcChargement getRefParc() {
        return refParc;
    }

    public void setRefParc(ParcChargement refParc) {
        this.refParc = refParc;
    }
/*
    @XmlTransient
    public List<ParcChargement> getParcChargementList() {
        return parcChargementList;
    }

    public void setParcChargementList(List<ParcChargement> parcChargementList) {
        this.parcChargementList = parcChargementList;
    }
*/    


    public static List<Titre> getTitreFor(String refFournisseur){
        return new TitreJpaController(Database.getEntityManager()).getTitreFor(refFournisseur);
    }
}
