/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "TYPECONTENNAIRE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Typecontennaire.findAll", query = "SELECT t FROM Typecontennaire t")
    , @NamedQuery(name = "Typecontennaire.findByRefTypecont", query = "SELECT t FROM Typecontennaire t WHERE t.refTypecont = :refTypecont")
    , @NamedQuery(name = "Typecontennaire.findByLibelleTypecontennaire", query = "SELECT t FROM Typecontennaire t WHERE t.libelleTypecontennaire = :libelleTypecontennaire")})
public class Typecontennaire implements Serializable {

    @Basic(optional = false)
    @Column(name = "CREATE_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt= new Date();

    @OneToMany(mappedBy = "refTypecont")
    private List<Conteneur> conteneurList;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_TYPECONT")
    private String refTypecont;
    @Column(name = "LIBELLE_TYPECONTENNAIRE")
    private String libelleTypecontennaire;
    

    public Typecontennaire() {
    }

    public Typecontennaire(String refTypecont) {
        this.refTypecont = refTypecont;
    }

    public String getRefTypecont() {
        return refTypecont;
    }

    public void setRefTypecont(String refTypecont) {
        this.refTypecont = refTypecont;
    }

    public String getLibelleTypecontennaire() {
        return libelleTypecontennaire;
    }

    public void setLibelleTypecontennaire(String libelleTypecontennaire) {
        this.libelleTypecontennaire = libelleTypecontennaire;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refTypecont != null ? refTypecont.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Typecontennaire)) {
            return false;
        }
        Typecontennaire other = (Typecontennaire) object;
        if ((this.refTypecont == null && other.refTypecont != null) || (this.refTypecont != null && !this.refTypecont.equals(other.refTypecont))) {
            return false;
        }
        return true;
    }
    @Override
    public String toString() {
        return "{" + "\"ref\" : \"" + refTypecont + "\", \"libelle\": \"" + libelleTypecontennaire + "\"}";
    }

    @XmlTransient
    public List<Conteneur> getConteneurList() {
        return conteneurList;
    }

    public void setConteneurList(List<Conteneur> conteneurList) {
        this.conteneurList = conteneurList;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    
    
}
