/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "CONTENEUR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Conteneur.findAll", query = "SELECT c FROM Conteneur c")
    , @NamedQuery(name = "Conteneur.findByNumConteneur", query = "SELECT c FROM Conteneur c WHERE c.numConteneur = :numConteneur")
    , @NamedQuery(name = "Conteneur.findByLibelle", query = "SELECT c FROM Conteneur c WHERE c.libelle = :libelle")})
public class Conteneur implements Serializable {

    @Basic(optional = false)
    @Column(name = "CREATE_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt = new Date();

    @OneToMany(mappedBy = "numConteneur")
    private List<Embarquement> contennaireList;

    @JoinColumn(name = "REF_TYPECONT", referencedColumnName = "REF_TYPECONT")
    @ManyToOne
    private Typecontennaire refTypecont;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "NUM_CONTENEUR")
    private String numConteneur;
    @Column(name = "LIBELLE")
    private String libelle;
    @JoinColumn(name = "REF_COMPAGNIE", referencedColumnName = "REF_COMPAGNIE")
    @ManyToOne
    private Compagnie refCompagnie;

    public Conteneur() {
    }

    public Conteneur(String numConteneur) {
        this.numConteneur = numConteneur;
    }

    public String getNumConteneur() {
        return numConteneur;
    }

    public void setNumConteneur(String numConteneur) {
        this.numConteneur = numConteneur;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Compagnie getRefCompagnie() {
        return refCompagnie;
    }

    public void setRefCompagnie(Compagnie refCompagnie) {
        this.refCompagnie = refCompagnie;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numConteneur != null ? numConteneur.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Conteneur)) {
            return false;
        }
        Conteneur other = (Conteneur) object;
        if ((this.numConteneur == null && other.numConteneur != null) || (this.numConteneur != null && !this.numConteneur.equals(other.numConteneur))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return ""
                + "{\"ref\": \""+numConteneur+"\", "
                + "\"libelle\": \"" + libelle + "\", "
                + "\"type\": " + refTypecont + ", "
                + "\"compagnie\": " + refCompagnie + "}";
    }

    public Typecontennaire getRefTypecont() {
        return refTypecont;
    }

    public void setRefTypecont(Typecontennaire refTypecont) {
        this.refTypecont = refTypecont;
    }

    @XmlTransient
    public List<Embarquement> getContennaireList() {
        return contennaireList;
    }

    public void setContennaireList(List<Embarquement> contennaireList) {
        this.contennaireList = contennaireList;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    
    
}
