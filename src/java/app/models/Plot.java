/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;

import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "PLOT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Plot.findAll", query = "SELECT p FROM Plot p")
    , @NamedQuery(name = "Plot.findByRefPlot", query = "SELECT p FROM Plot p WHERE p.refPlot = :refPlot")
    , @NamedQuery(name = "Plot.findByLongueur", query = "SELECT p FROM Plot p WHERE p.longueur = :longueur")})
public class Plot implements Serializable {

    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    @Column(name = "LONGUEUR")
    private int longueur;
    @Column(name = "FACE1")
    private int face1;
    @Column(name = "FACE2")
    private int face2;
    @Column(name = "FACE3")
    private int face3;
    
    @JoinColumn(name = "REF_BILLON", referencedColumnName = "REF_BILLON")
    @ManyToOne
    private Billon refBillon;


    @Column(name = "REBUT")
    private Character rebut;
    @Column(name = "FACE_M_N")
    private String faceMN;

    
    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_PLOT")
    private String refPlot;
    @Column(name = "NUM_TRAVAIL")
    private String numTravail;
    
    
    @JoinColumn(name = "REF_COUPE", referencedColumnName = "REF_COUPE")
    @ManyToOne
    private Coupe refCoupe;
    @OneToMany(mappedBy = "refPlot")
    private List<Tranche> trancheList;
    @OneToMany(mappedBy = "refPlot")
    private List<Feuille> feuilleList;
    @OneToMany(mappedBy = "refPlot")
    private List<CuvePlot> cuvePlotList;
    @OneToMany(mappedBy = "refPlot")
    private List<Paquet> paquetList;

    
    public String getNumTravail() {
        return numTravail;
    }

    public void setNumTravail(String numTravail) {
        this.numTravail = numTravail;
    }
     
    
    public void unSetRebut(){
        rebut = 'F';
        //RebutController.unSetRebut(this.getClass(), this.refPlot);
    }
    
    public void setRebut(){
        rebut = 'T';
        //RebutController.unSetRebut(this.getClass(), this.refPlot);
    }

    
    public Plot() {
    }

    public Plot(String refPlot) {
        this.refPlot = refPlot;
    }

    public String getRefPlot() {
        return refPlot;
    }

    public void setRefPlot(String refPlot) {
        this.refPlot = refPlot;
    }


    public Coupe getRefCoupe() {
        return refCoupe;
    }

    public void setRefCoupe(Coupe refCoupe) {
        this.refCoupe = refCoupe;
    }

    @XmlTransient
    public List<Tranche> getTrancheList() {
        return trancheList;
    }

    public void setTrancheList(List<Tranche> trancheList) {
        this.trancheList = trancheList;
    }

    @XmlTransient
    public List<Feuille> getFeuilleList() {
        return feuilleList;
    }

    public void setFeuilleList(List<Feuille> feuilleList) {
        this.feuilleList = feuilleList;
    }

    @XmlTransient
    public List<CuvePlot> getCuvePlotList() {
        return cuvePlotList;
    }

    public void setCuvePlotList(List<CuvePlot> cuvePlotList) {
        this.cuvePlotList = cuvePlotList;
    }

    @XmlTransient
    public List<Paquet> getPaquetList() {
        return paquetList;
    }

    public void setPaquetList(List<Paquet> paquetList) {
        this.paquetList = paquetList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refPlot != null ? refPlot.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Plot)) {
            return false;
        }
        Plot other = (Plot) object;
        if ((this.refPlot == null && other.refPlot != null) || (this.refPlot != null && !this.refPlot.equals(other.refPlot))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{" + "\"ref\": \"" + refPlot + "\", "
                + "\"longueur\": \"" + longueur + "\", "
                + "\"face1\" : \"" + face1 + "\", "
                + "\"face2\" : \"" + face2 + "\", "
                + "\"face3\" : \"" + face3 + "\", "
                + "\"faceMN\" : \"" + faceMN + "\", "
                + "\"rebut\" : \"" + rebut + "\", "
                + "\"coupe\" : " + refCoupe + ","
                + "\"travail\" : \"" + numTravail + "\"}";
    }


    public Billon getRefBillon() {
        return refBillon;
    }

    public void setRefBillon(Billon refBillon) {
        this.refBillon = refBillon;
    }



    public Character getRebut() {
        return rebut;
    }

    public void setRebut(Character rebut) {
        this.rebut = rebut;
    }


    public String getFaceMN() {
        return faceMN;
    }

    public void setFaceMN(String faceMN) {
        this.faceMN = faceMN;
    }

    public int getLongueur() {
        return longueur;
    }

    public void setLongueur(int longueur) {
        this.longueur = longueur;
    }

    public int getFace1() {
        return face1;
    }

    public void setFace1(int face1) {
        this.face1 = face1;
    }

    public int getFace2() {
        return face2;
    }

    public void setFace2(int face2) {
        this.face2 = face2;
    }

    public int getFace3() {
        return face3;
    }

    public void setFace3(int face3) {
        this.face3 = face3;
    }

}
