/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "TYPE_PARC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeParc.findAll", query = "SELECT t FROM TypeParc t")
    , @NamedQuery(name = "TypeParc.findByRefTypeparc", query = "SELECT t FROM TypeParc t WHERE t.refTypeparc = :refTypeparc")
    , @NamedQuery(name = "TypeParc.findByLibelle", query = "SELECT t FROM TypeParc t WHERE t.libelle = :libelle")})
public class TypeParc implements Serializable {

    @OneToMany(mappedBy = "refTypeparc")
    private List<ParcChargement> parcChargementList;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_TYPEPARC")
    private String refTypeparc;
    @Column(name = "LIBELLE")
    private String libelle;
    @OneToMany(mappedBy = "refTypeparc")
    private List<Parc> parcList;

    public TypeParc() {
    }

    public TypeParc(String refTypeparc) {
        this.refTypeparc = refTypeparc;
    }

    public String getRefTypeparc() {
        return refTypeparc;
    }

    public void setRefTypeparc(String refTypeparc) {
        this.refTypeparc = refTypeparc;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @XmlTransient
    public List<Parc> getParcList() {
        return parcList;
    }

    public void setParcList(List<Parc> parcList) {
        this.parcList = parcList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refTypeparc != null ? refTypeparc.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeParc)) {
            return false;
        }
        TypeParc other = (TypeParc) object;
        if ((this.refTypeparc == null && other.refTypeparc != null) || (this.refTypeparc != null && !this.refTypeparc.equals(other.refTypeparc))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{" + "\"ref\": \"" + refTypeparc + "\", \"libelle\" : \"" + libelle + "\"}";
    }

    @XmlTransient
    public List<ParcChargement> getParcChargementList() {
        return parcChargementList;
    }

    public void setParcChargementList(List<ParcChargement> parcChargementList) {
        this.parcChargementList = parcChargementList;
    }
    
}
