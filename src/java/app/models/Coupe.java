/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "COUPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Coupe.findAll", query = "SELECT c FROM Coupe c")
    , @NamedQuery(name = "Coupe.findByRefCoupe", query = "SELECT c FROM Coupe c WHERE c.refCoupe = :refCoupe")
    , @NamedQuery(name = "Coupe.findByNbrePlot", query = "SELECT c FROM Coupe c WHERE c.nbrePlot = :nbrePlot")})
public class Coupe implements Serializable {

    
    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    @Column(name = "NBRE_PLOT")
    private int nbrePlot;
    
    @JoinColumn(name = "REF_TYPECOUPE", referencedColumnName = "REF_TYPECOUPE")
    @ManyToOne
    private TypeCoupe refTypecoupe;

    @OneToMany(mappedBy = "coupe")
    private List<Billon> billonList;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_COUPE")
    private String refCoupe;
    @OneToMany(mappedBy = "refCoupe")
    private List<Plot> plotList;
    @JoinColumn(name = "REF_BILLON", referencedColumnName = "REF_BILLON")
    @ManyToOne
    private Billon refBillon;
    
    
    public Coupe() {
    }

    public Coupe(String refCoupe) {
        this.refCoupe = refCoupe;
    }

    public Coupe(String refCoupe, Integer nbrePlot) {
        this.refCoupe = refCoupe;
        this.nbrePlot = nbrePlot;
    }

    public String getRefCoupe() {
        return refCoupe;
    }

    public void setRefCoupe(String refCoupe) {
        this.refCoupe = refCoupe;
    }

    public Integer getNbrePlot() {
        return nbrePlot;
    }

    public void setNbrePlot(Integer nbrePlot) {
        this.nbrePlot = nbrePlot;
    }

    @XmlTransient
    public List<Plot> getPlotList() {
        return plotList;
    }

    public void setPlotList(List<Plot> plotList) {
        this.plotList = plotList;
    }

    public Billon getRefBillon() {
        return refBillon;
    }

    public void setRefBillon(Billon refBillon) {
        this.refBillon = refBillon;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refCoupe != null ? refCoupe.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Coupe)) {
            return false;
        }
        Coupe other = (Coupe) object;
        if ((this.refCoupe == null && other.refCoupe != null) || (this.refCoupe != null && !this.refCoupe.equals(other.refCoupe))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{" 
            + "\"ref\": \"" + refCoupe + "\", "
                + "\"nbrePlot\" : \"" + nbrePlot + "\", "
                + "\"billon\" : \"" + refBillon.getRefBillon() + "\","
                + "\"typeCoupe\": "+refTypecoupe
                + "}";
    }

    @XmlTransient
    public List<Billon> getBillonList() {
        return billonList;
    }

    public void setBillonList(List<Billon> billonList) {
        this.billonList = billonList;
    }


    public void setNbrePlot(int nbrePlot) {
        this.nbrePlot = nbrePlot;
    }

    public TypeCoupe getRefTypecoupe() {
        return refTypecoupe;
    }

    public void setRefTypecoupe(TypeCoupe refTypecoupe) {
        this.refTypecoupe = refTypecoupe;
    }


}
