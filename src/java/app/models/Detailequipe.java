/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "DETAILEQUIPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Detailequipe.findAll", query = "SELECT d FROM Detailequipe d")
    , @NamedQuery(name = "Detailequipe.findByRefDetail", query = "SELECT d FROM Detailequipe d WHERE d.refDetail = :refDetail")
    , @NamedQuery(name = "Detailequipe.findByRole", query = "SELECT d FROM Detailequipe d WHERE d.role = :role")})
public class Detailequipe implements Serializable {

    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_DETAIL")
    private String refDetail;
    @Column(name = "ROLE")
    private String role;
    @JoinColumn(name = "REF_EQUIPE", referencedColumnName = "REF_EQUIPE")
    @ManyToOne
    private Equipe refEquipe;
    @JoinColumn(name = "REF_PERSONNEL", referencedColumnName = "CNI_PERSONNEL")
    @ManyToOne
    private Personnel refPersonnel;

    public Detailequipe() {
    }

    public Detailequipe(String refDetail) {
        this.refDetail = refDetail;
    }

    public String getRefDetail() {
        return refDetail;
    }

    public void setRefDetail(String refDetail) {
        this.refDetail = refDetail;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Equipe getRefEquipe() {
        return refEquipe;
    }

    public void setRefEquipe(Equipe refEquipe) {
        this.refEquipe = refEquipe;
    }

    public Personnel getRefPersonnel() {
        return refPersonnel;
    }

    public void setRefPersonnel(Personnel refPersonnel) {
        this.refPersonnel = refPersonnel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refDetail != null ? refDetail.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Detailequipe)) {
            return false;
        }
        Detailequipe other = (Detailequipe) object;
        if ((this.refDetail == null && other.refDetail != null) || (this.refDetail != null && !this.refDetail.equals(other.refDetail))) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
        return "{\"ref\": \"" + refDetail + "\", \"role\": \"" + role + "\", \"refEquipe\": " + refEquipe + ", \"personnel\": " + refPersonnel + "}";
    }
    
}
