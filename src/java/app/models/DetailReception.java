/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "DETAIL_RECEPTION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailReception.findAll", query = "SELECT d FROM DetailReception d")
    , @NamedQuery(name = "DetailReception.findByRefDetail", query = "SELECT d FROM DetailReception d WHERE d.refDetail = :refDetail")
    , @NamedQuery(name = "DetailReception.findByDiaMoyen", query = "SELECT d FROM DetailReception d WHERE d.diaMoyen = :diaMoyen")
    , @NamedQuery(name = "DetailReception.findByGdDiam", query = "SELECT d FROM DetailReception d WHERE d.gdDiam = :gdDiam")
    , @NamedQuery(name = "DetailReception.findByLongueur", query = "SELECT d FROM DetailReception d WHERE d.longueur = :longueur")
    , @NamedQuery(name = "DetailReception.findByNbre", query = "SELECT d FROM DetailReception d WHERE d.nbre = :nbre")
    , @NamedQuery(name = "DetailReception.findByNumBille", query = "SELECT d FROM DetailReception d WHERE d.numBille = :numBille")
    , @NamedQuery(name = "DetailReception.findByPtitDiam", query = "SELECT d FROM DetailReception d WHERE d.ptitDiam = :ptitDiam")
    , @NamedQuery(name = "DetailReception.findByVolume", query = "SELECT d FROM DetailReception d WHERE d.volume = :volume")
    , @NamedQuery(name = "DetailReception.findByNumtravail", query = "SELECT d FROM DetailReception d WHERE d.numtravail = :numtravail")})
public class DetailReception implements Serializable {
    private static final long serialVersionUID = 1L;

    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "DIA_MOYEN")
    private Float diaMoyen;

    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    
    @Column(name = "GD_DIAM")
    private Float gdDiam;
    
    @Column(name = "LONGUEUR")
    private Double longueur;
    
    @Column(name = "NBRE")
    private Double nbre;
    
    @Column(name = "PTIT_DIAM")
    private Float ptitDiam;
    
    @Column(name = "PRIX")
    private Double prix;
    
    @Column(name = "STATUT")
    private String statut;
    
    @Column(name = "CERTIFICATION")
    private String certificat;

    @Id
    @Basic(optional = false)
    @Column(name = "REF_DETAIL")
    private String refDetail;
    
    @Column(name = "NUM_BILLE")
    private String numBille;
    
    @Column(name = "VOLUME")
    private Double volume;
    
    @Column(name = "NUMTRAVAIL")
    private String numtravail;
    
    @Column(name = "OBSERVATION")
    private String observation;
    

    
    @Column(name = "DATE_RECEPTION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateReception;
    
    @JoinColumn(name = "REF_RECEPTION", referencedColumnName = "REF_RECEPTION")
    @ManyToOne
    private Reception refReception;
    
    @JoinColumn(name = "REF_ESSENCE", referencedColumnName = "REF_ESSENCE")
    @ManyToOne(optional = false)
    private Essence refEssence;
    
    
    
    
    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getStatus() {
        return statut;
    }

    public void setStatus(String status) {
        this.statut = status;
    }


    public Essence getRefEssence() {
        return refEssence;
    }

    public void setRefEssence(Essence refEssence) {
        this.refEssence = refEssence;
    }

    public DetailReception() {
    }

    public DetailReception(String refDetail) {
        this.refDetail = refDetail;
    }

    public String getRefDetail() {
        return refDetail;
    }

    public void setRefDetail(String refDetail) {
        this.refDetail = refDetail;
    }

    public Float getDiaMoyen() {
        return diaMoyen;
    }

    public void setDiaMoyen(float diaMoyen) {
        this.diaMoyen = diaMoyen;
    }

    public float getGdDiam() {
        return gdDiam;
    }

    public void setGdDiam(float gdDiam) {
        this.gdDiam = gdDiam;
    }
    
    public String getNumBille() {
        return numBille;
    }

    public void setNumBille(String numBille) {
        this.numBille = numBille;
    }

    public float getPtitDiam() {
        return ptitDiam;
    }

    public void setPtitDiam(float ptitDiam) {
        this.ptitDiam = ptitDiam;
    }

    public Double getVolume() {
        return volume;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public String getNumtravail() {
        return numtravail;
    }

    public void setNumtravail(String numtravail) {
        this.numtravail = numtravail;
    }

    public Reception getRefReception() {
        return refReception;
    }

    public void setRefReception(Reception refReception) {
        this.refReception = refReception;
    }

    
    public void setDiaMoyen(Float diaMoyen) {
        this.diaMoyen = diaMoyen;
    }

    public void setGdDiam(Float gdDiam) {
        this.gdDiam = gdDiam;
    }

    public Double getLongueur() {
        return longueur;
    }

    public void setLongueur(Double longueur) {
        this.longueur = longueur;
    }

    public String getCertificat() {
        return certificat;
    }

    public void setCertificat(String certif) {
        this.certificat = certif;
    }

    public Double getNbre() {
        return nbre;
    }

    public void setNbre(Double nbre) {
        this.nbre = nbre;
    }

    public void setPtitDiam(Float ptitDiam) {
        this.ptitDiam = ptitDiam;
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    } 
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refDetail != null ? refDetail.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailReception)) {
            return false;
        }
        DetailReception other = (DetailReception) object;
        if ((this.refDetail == null && other.refDetail != null) || (this.refDetail != null && !this.refDetail.equals(other.refDetail))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{\"" + "ref\": \"" + refDetail + "\", "
                + "\"longueur\": \"" + longueur + "\", "
                + "\"nbre\": \"" + nbre + "\", "
                + "\"ptitDiam\": \"" + ptitDiam + "\", "
                + "\"gdDiam\": \"" + gdDiam + "\", "
                + "\"diaMoyen\": \"" + diaMoyen + "\", "
                + "\"volume\": \"" + volume + "\", "
                + "\"numBille\": \"" + numBille + "\", "
                + "\"observation\": \"" + observation + "\", "
                + "\"status\": \"" + statut + "\", "
                + "\"certificat\": \"" + certificat + "\", "
                + "\"prix\": \"" + prix + "\", "
                + "\"essence\": " + refEssence + ", "
                + "\"refReception\": \"" + refReception.getRefReception() + "\", "
                + "\"numTravail\": \""+numtravail+"\", "
                + "\"date\": \""+dateReception.getTime()+"\"}";
    }

    
}
