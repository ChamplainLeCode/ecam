/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import app.modelController.CommandeJpaController;
import app.modelController.ReceptionJpaController;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "FOURNISSEUR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fournisseur.findAll", query = "SELECT f FROM Fournisseur f")
    , @NamedQuery(name = "Fournisseur.findByRefFournisseur", query = "SELECT f FROM Fournisseur f WHERE f.refFournisseur = :refFournisseur")
    , @NamedQuery(name = "Fournisseur.findByContact", query = "SELECT f FROM Fournisseur f WHERE f.contact = :contact")
    , @NamedQuery(name = "Fournisseur.findByContribuable", query = "SELECT f FROM Fournisseur f WHERE f.contribuable = :contribuable")
    , @NamedQuery(name = "Fournisseur.findByNomFournisseur", query = "SELECT f FROM Fournisseur f WHERE f.nomFournisseur = :nomFournisseur")
})
public class Fournisseur implements Serializable {


    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    @Id
    @Basic(optional = false)
    @Column(name = "REF_FOURNISSEUR")
    private String refFournisseur;
    @Column(name = "CONTACT")
    private String contact;
    @Column(name = "CONTRIBUABLE")
    private String contribuable;
    @Column(name = "NOM_FOURNISSEUR")
    private String nomFournisseur;
    @OneToMany(mappedBy = "refFournisseur")
    private List<Commande> commandeList;
    @OneToMany(mappedBy = "refFournisseur")
    private List<ParcChargement> parcChargementList;

    public Fournisseur() {
    }

    public Fournisseur(String refFournisseur) {
        this.refFournisseur = refFournisseur;
    }

    public String getRefFournisseur() {
        return refFournisseur;
    }

    public void setRefFournisseur(String refFournisseur) {
        this.refFournisseur = refFournisseur;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContribuable() {
        return contribuable;
    }

    public void setContribuable(String contribuable) {
        this.contribuable = contribuable;
    }

    public String getNomFournisseur() {
        return nomFournisseur;
    }

    public void setNomFournisseur(String nomFournisseur) {
        this.nomFournisseur = nomFournisseur;
    }
    @XmlTransient
    public List<Commande> getCommandeList() {
        return commandeList;
    }

    public void setCommandeList(List<Commande> commandeList) {
        this.commandeList = commandeList;
    }

    @XmlTransient
    public List<ParcChargement> getParcChargementList() {
        return parcChargementList;
    }

    public void setParcChargementList(List<ParcChargement> parcChargementList) {
        this.parcChargementList = parcChargementList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refFournisseur != null ? refFournisseur.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fournisseur)) {
            return false;
        }
        Fournisseur other = (Fournisseur) object;
        if ((this.refFournisseur == null && other.refFournisseur != null) || (this.refFournisseur != null && !this.refFournisseur.equals(other.refFournisseur))) {
            return false;
        }
        return true;
    }

    
    @Override
    public String toString() {
        return "{\"" + "refFournisseur\": \"" + refFournisseur + "\", \"nomFournisseur\": \"" + nomFournisseur + "\", \"contribuable\": \"" + contribuable + "\", \"contact\": \"" + contact + "\", \"parcChargementList\": " + parcChargementList + "}";
    }


    public String baseString() {
        return "{" + "\"refFournisseur\":\"" + refFournisseur + "\", \"nomFournisseur\":\"" + nomFournisseur + "\", \"contribuable\":\"" + contribuable + "\", \"contact\":\"" + contact +"\"}";
    }

    public Date getCreateAt() {
        return create_at;
    }

    public void setCreateAt(Date createAt) {
        this.create_at = createAt;
    }
    
    public int getNombreTotalReception(){
        return new ReceptionJpaController(Database.getEntityManager()).getReceptionCountForFournisseur(this.refFournisseur);
    }
    public int getNombreTotalCommande(){
        return new CommandeJpaController(Database.getEntityManager()).getCommandeCountForFournisseur(this.refFournisseur);
    }
    
}
