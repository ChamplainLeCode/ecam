/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import app.modelController.DetailReceptionJpaController;
import app.modelController.ReceptionJpaController;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "RECEPTION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reception.findAll", query = "SELECT r FROM Reception r")
    , @NamedQuery(name = "Reception.findByRefReception", query = "SELECT r FROM Reception r WHERE r.refReception = :refReception")
    , @NamedQuery(name = "Reception.findByDateReception", query = "SELECT r FROM Reception r WHERE r.dateReception = :dateReception")})
public class Reception implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_RECEPTION")
    private String refReception;
    @Column(name = "DATE_RECEPTION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateReception;
    @JoinColumn(name = "REF_COMMANDE", referencedColumnName = "REF_COMMANDE")
    @ManyToOne
    private Commande refCommande;
    @JoinColumn(name = "REF_LETTRE", referencedColumnName = "REF_LETTRE")
    @ManyToOne
    private LettreCamion refLettre;
    
    @Temporal(TemporalType.TIMESTAMP)
    private final Date create_at = new Date();
    
    @OneToMany(mappedBy = "refReception")
    private List<DetailReception> detailReceptionList;

    public Reception() {
    }

    public Reception(String refReception) {
        this.refReception = refReception;
    }

    public String getRefReception() {
        return refReception;
    }

    public void setRefReception(String refReception) {
        this.refReception = refReception;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

    public Commande getRefCommande() {
        return refCommande;
    }

    public void setRefCommande(Commande refCommande) {
        this.refCommande = refCommande;
    }

    public LettreCamion getRefLettre() {
        return refLettre;
    }

    public void setRefLettre(LettreCamion refLettre) {
        this.refLettre = refLettre;
    }

    @XmlTransient
    public List<DetailReception> getDetailReceptionList() {
        return detailReceptionList;
    }

    public void setDetailReceptionList(List<DetailReception> detailReceptionList) {
        this.detailReceptionList = detailReceptionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refReception != null ? refReception.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reception)) {
            return false;
        }
        Reception other = (Reception) object;
        if ((this.refReception == null && other.refReception != null) || (this.refReception != null && !this.refReception.equals(other.refReception))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        this.detailReceptionList = DetailReceptionJpaController.findDetailReceptionByCommande(refCommande.getRefCommande());
        return "{" + "\"ref\": \"" + refReception + "\", \"date\": \"" + dateReception + "\", \"commande\": " + refCommande + ", \"lettre\": " + refLettre + ", \"detailReceptionList\": " + detailReceptionList + '}';
    }

    public float getVolumeRecu() {
        float volume = 1f;
        
        try{
            ReceptionJpaController cont = new ReceptionJpaController(Database.getEntityManager());
            volume = cont.getVolumeForCommande(getRefCommande().getRefCommande());
        } catch(Exception e){
            System.out.println(e.getMessage());
        }
        return volume;
        
    }

    
}
