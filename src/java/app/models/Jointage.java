/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import app.modelController.ColisJpaController;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "JOINTAGE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Jointage.findAll", query = "SELECT j FROM Jointage j")
    , @NamedQuery(name = "Jointage.findByRefJointage", query = "SELECT j FROM Jointage j WHERE j.refJointage = :refJointage")
    , @NamedQuery(name = "Jointage.findByNbrColisJoints", query = "SELECT j FROM Jointage j WHERE j.nbrColisJoints = :nbrColisJoints")
    , @NamedQuery(name = "Jointage.findByDateJointage", query = "SELECT j FROM Jointage j WHERE j.dateJointage = :dateJointage")})
public class Jointage implements Serializable {

    @Basic(optional = false)
    @Column(name = "NBR_COLIS_JOINTS")
    private int nbrColisJoints;
    @Basic(optional = false)
    @Column(name = "NBR_FEUILLE")
    private int nbrFeuille;
    @Basic(optional = false)
    @Column(name = "LONGUEUR")
    private int longueur;
    @Basic(optional = false)
    @Column(name = "EPAISSEUR")
    private float epaisseur;
    @Basic(optional = false)
    @Column(name = "LARGEUR")
    private int largeur;
    @Basic(optional = false)
    @Column(name = "NBR_PAQUET_RESTANT")
    private int nbrPaquetRestant;
    @Column(name = "NBR_FEUILLE_DEFECTUEUSE")
    private int nbrFeuilleDefectueuse;
    @Column(name = "NBR_FEUILLE_DERNIER_PAQUET")
    private int nbrFeuilleDernierPaquet;

    private static final long serialVersionUID = 1L;

    public int getNbrFeuille() {
        return nbrFeuille;
    }

    public int getNbrFeuilleDernierPaquet() {
        return nbrFeuilleDernierPaquet;
    }

    public void setNbrFeuilleDernierPaquet(int nbrFeuilleDernierPaquet) {
        this.nbrFeuilleDernierPaquet = nbrFeuilleDernierPaquet;
    }

    public void setNbrFeuille(int nbrFeuille) {
        this.nbrFeuille = nbrFeuille;
    }

    @Id
    @Basic(optional = false)
    @Column(name = "REF_JOINTAGE")
    private String refJointage;
    @Basic(optional = false)
    @Column(name = "DATE_JOINTAGE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateJointage;
    @ManyToMany(mappedBy = "jointageList")
    private List<Colis> colisList;
    @JoinColumn(name = "COLIS_FINAL", referencedColumnName = "REF_COLIS")
    @OneToOne(optional = false)
    private Colis colisFinal;

    public Jointage() {
    }

    public Jointage(String refJointage) {
        this.refJointage = refJointage;
    }

    public Jointage(String refJointage, int nbrColisJoints, Date dateJointage) {
        this.refJointage = refJointage;
        this.nbrColisJoints = nbrColisJoints;
        this.dateJointage = dateJointage;
    }

    public String getRefJointage() {
        return refJointage;
    }

    public void setRefJointage(String refJointage) {
        this.refJointage = refJointage;
    }

    public int getNbrColisJoints() {
        return nbrColisJoints;
    }

    public void setNbrColisJoints(int nbrColisJoints) {
        this.nbrColisJoints = nbrColisJoints;
    }

    public Date getDateJointage() {
        return dateJointage;
    }

    public void setDateJointage(Date dateJointage) {
        this.dateJointage = dateJointage;
    }

    @XmlTransient
    public List<Colis> getColisList() {
        try {
            ColisJpaController cont = new ColisJpaController(Database.getEntityManager());
            return cont.getColisForJointage(this);
        } catch (Exception e) {
            return new LinkedList();
        } 
//        return colisList;
    }

    public void setColisList(List<Colis> colisList) {
        this.colisList = colisList;
    }

    public Colis getColisFinal() {
        return colisFinal;
    }

    public void setColisFinal(Colis colisFinal) {
        this.colisFinal = colisFinal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refJointage != null ? refJointage.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jointage)) {
            return false;
        }
        Jointage other = (Jointage) object;
        if ((this.refJointage == null && other.refJointage != null) || (this.refJointage != null && !this.refJointage.equals(other.refJointage))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{"
                + "\"refJointage\": \"" + refJointage + "\", "
                + "\"nbrColis\": " + nbrColisJoints + ","
                + "\"nbrFeuille\": " + nbrFeuille + ", "
                + "\"longueur\": " + longueur + ", "
                + "\"largeur\": " + largeur + ", "
                + "\"epaisseur\": " + epaisseur + ", "
                + "\"feuillesDefectueuses\": " + nbrFeuilleDefectueuse + ", "
                + "\"paquetRestant\": " + nbrPaquetRestant + ", "
                + "\"feuillesDernierPaquet\": " + nbrFeuilleDernierPaquet + ", "
                + "\"date\": " + dateJointage.getTime() + ", "
                //+ "\"colis\": " + colisList + ", "
                + "\"colisFinal\": " + colisFinal + ""
                + "}";
    }

    

    public int getLongueur() {
        return longueur;
    }

    public void setLongueur(int longueur) {
        this.longueur = longueur;
    }

    public float getEpaisseur() {
        return epaisseur;
    }

    public void setEpaisseur(float epaisseur) {
        this.epaisseur = epaisseur;
    }

    public int getLargeur() {
        return largeur;
    }

    public void setLargeur(int largeur) {
        this.largeur = largeur;
    }

    public int getNbrPaquetRestant() {
        return nbrPaquetRestant;
    }

    public void setNbrPaquetRestant(int nbrPaquetRestant) {
        this.nbrPaquetRestant = nbrPaquetRestant;
    }

    public int getNbrFeuilleDefectueuse() {
        return nbrFeuilleDefectueuse;
    }

    public void setNbrFeuilleDefectueuse(int nbrFeuilleDefectueuse) {
        this.nbrFeuilleDefectueuse = nbrFeuilleDefectueuse;
    }

    
}
