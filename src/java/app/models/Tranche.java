/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "TRANCHE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tranche.findAll", query = "SELECT t FROM Tranche t")
    , @NamedQuery(name = "Tranche.findByRefTranche", query = "SELECT t FROM Tranche t WHERE t.refTranche = :refTranche")
    , @NamedQuery(name = "Tranche.findByDateDebut", query = "SELECT t FROM Tranche t WHERE t.dateDebut = :dateDebut")
    , @NamedQuery(name = "Tranche.findByEpaisseur", query = "SELECT t FROM Tranche t WHERE t.epaisseur = :epaisseur")
    , @NamedQuery(name = "Tranche.findByNumTravail", query = "SELECT t FROM Tranche t WHERE t.numTravail = :numTravail")})
public class Tranche implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_TRANCHE")
    private String refTranche;
    @Column(name = "DATE_DEBUT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateDebut;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "EPAISSEUR")
    private String epaisseur;
    @JoinColumn(name = "MACHINE", referencedColumnName = "REF_MACHINE")
    @ManyToOne
    private TrancheMachine machine;
    @Column(name = "NUM_TRAVAIL")
    private String numTravail;
    @JoinColumn(name = "REF_EQUIPE", referencedColumnName = "REF_EQUIPE")
    @ManyToOne
    private Equipe refEquipe;
    @JoinColumn(name = "REF_PLOT", referencedColumnName = "REF_PLOT")
    @ManyToOne
    private Plot refPlot;

    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    public Tranche() {
    }

    public Tranche(String refTranche) {
        this.refTranche = refTranche;
    }

    public String getRefTranche() {
        return refTranche;
    }

    public void setRefTranche(String refTranche) {
        this.refTranche = refTranche;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public String getEpaisseur() {
        return epaisseur;
    }

    public void setEpaisseur(String epaisseur) {
        this.epaisseur = epaisseur;
    }

    public String getNumTravail() {
        return numTravail;
    }

    public void setNumTravail(String numTravail) {
        this.numTravail = numTravail;
    }

    public Equipe getRefEquipe() {
        return refEquipe;
    }

    public void setRefEquipe(Equipe refEquipe) {
        this.refEquipe = refEquipe;
    }

    public Plot getRefPlot() {
        return refPlot;
    }

    public void setRefPlot(Plot refPlot) {
        this.refPlot = refPlot;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refTranche != null ? refTranche.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tranche)) {
            return false;
        }
        Tranche other = (Tranche) object;
        if ((this.refTranche == null && other.refTranche != null) || (this.refTranche != null && !this.refTranche.equals(other.refTranche))) {
            return false;
        }
        return true;
    }

    
    @Override
    public String toString() {
        return "{" + "\"ref\": \"" + refTranche + "\", "
                + "\"dateDebut\" : " + dateDebut.getTime() + ", "
                + "\"epaisseur\" :  \""+ epaisseur + "\", "
                + "\"machine\" :  "+ machine + ", "
                + "\"travail\":\"" + numTravail + "\", "
                + "\"equipe\": " + refEquipe + ", "
                + "\"plot\": " + refPlot + ""
                + "}";
    }

    public TrancheMachine getMachine() {
        return machine;
    }

    public void setMachine(TrancheMachine machine) {
        this.machine = machine;
    }
    
}
