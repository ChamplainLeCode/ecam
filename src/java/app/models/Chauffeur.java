/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "CHAUFFEUR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Chauffeur.findAll", query = "SELECT c FROM Chauffeur c")
    , @NamedQuery(name = "Chauffeur.findByRefChauffeur", query = "SELECT c FROM Chauffeur c WHERE c.refChauffeur = :refChauffeur")
    , @NamedQuery(name = "Chauffeur.findByNom", query = "SELECT c FROM Chauffeur c WHERE c.nom = :nom")
    , @NamedQuery(name = "Chauffeur.findByPrenom", query = "SELECT c FROM Chauffeur c WHERE c.prenom = :prenom")})
public class Chauffeur implements Serializable {

    @Basic(optional = false)
    @Column(name = "CREATE_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt = new Date();
    
    @OneToMany(mappedBy = "refChauffeur")
    private List<Embarquement> contennaireList;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_CHAUFFEUR")
    private String refChauffeur;
    @Column(name = "NOM")
    private String nom;
    @Column(name = "PRENOM")
    private String prenom;

    public Chauffeur() {
    }

    public Chauffeur(String refChauffeur) {
        this.refChauffeur = refChauffeur;
    }

    public String getRefChauffeur() {
        return refChauffeur;
    }

    public void setRefChauffeur(String refChauffeur) {
        this.refChauffeur = refChauffeur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refChauffeur != null ? refChauffeur.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Chauffeur)) {
            return false;
        }
        Chauffeur other = (Chauffeur) object;
        if ((this.refChauffeur == null && other.refChauffeur != null) || (this.refChauffeur != null && !this.refChauffeur.equals(other.refChauffeur))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return ""
                + "{" 
                + "\"ref\": \"" + refChauffeur + "\", "
                + "\"nom\": \"" + nom + "\", "
                + "\"prenom\": \"" + prenom + "\"}";
    }


    @XmlTransient
    public List<Embarquement> getContennaireList() {
        return contennaireList;
    }

    public void setContennaireList(List<Embarquement> contennaireList) {
        this.contennaireList = contennaireList;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    
}
