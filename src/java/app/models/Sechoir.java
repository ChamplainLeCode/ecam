/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "SECHOIR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sechoir.findAll", query = "SELECT s FROM Sechoir s")
    , @NamedQuery(name = "Sechoir.findByRefSechoir", query = "SELECT s FROM Sechoir s WHERE s.refSechoir = :refSechoir")
    , @NamedQuery(name = "Sechoir.findByLibelleSechoir", query = "SELECT s FROM Sechoir s WHERE s.libelleSechoir = :libelleSechoir")})
public class Sechoir implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_SECHOIR")
    private String refSechoir;
    @Column(name = "LIBELLE_SECHOIR")
    private String libelleSechoir;

    public Sechoir() {
    }

    public Sechoir(String refSechoir) {
        this.refSechoir = refSechoir;
    }

    public String getRefSechoir() {
        return refSechoir;
    }

    public void setRefSechoir(String refSechoir) {
        this.refSechoir = refSechoir;
    }

    public String getLibelleSechoir() {
        return libelleSechoir;
    }

    public void setLibelleSechoir(String libelleSechoir) {
        this.libelleSechoir = libelleSechoir;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refSechoir != null ? refSechoir.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sechoir)) {
            return false;
        }
        Sechoir other = (Sechoir) object;
        if ((this.refSechoir == null && other.refSechoir != null) || (this.refSechoir != null && !this.refSechoir.equals(other.refSechoir))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
           
     return "{" + "\"ref\": \"" + refSechoir + "\", \"libelle\" : \"" + libelleSechoir + "\"}";
    }
    
}
