/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "LETTRE_CAMION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LettreCamion.findAll", query = "SELECT l FROM LettreCamion l")
    , @NamedQuery(name = "LettreCamion.findByRefLettre", query = "SELECT l FROM LettreCamion l WHERE l.refLettre = :refLettre")
    , @NamedQuery(name = "LettreCamion.findByFichier", query = "SELECT l FROM LettreCamion l WHERE l.fichier = :fichier")})
public class LettreCamion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    @Id
    @Basic(optional = false)
    @Column(name = "REF_LETTRE")
    private String refLettre;
    @Column(name = "FICHIER")
    private String fichier;
    @OneToMany(mappedBy = "refLettre")
    private List<Reception> receptionList;

    public LettreCamion() {
    }

    public LettreCamion(String refLettre) {
        this.refLettre = refLettre;
    }

    public String getRefLettre() {
        return refLettre;
    }

    public void setRefLettre(String refLettre) {
        this.refLettre = refLettre;
    }


    public String getFichier() {
        return fichier;
    }

    public void setFichier(String fichier) {
        this.fichier = fichier;
    }

    @XmlTransient
    public List<Reception> getReceptionList() {
        return receptionList;
    }

    public void setReceptionList(List<Reception> receptionList) {
        this.receptionList = receptionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refLettre != null ? refLettre.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LettreCamion)) {
            return false;
        }
        LettreCamion other = (LettreCamion) object;
        if ((this.refLettre == null && other.refLettre != null) || (this.refLettre != null && !this.refLettre.equals(other.refLettre))) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
        return "{\"" + "ref\": \"" + refLettre + "\", \"fichier\": \"" + fichier.replaceAll(Matcher.quoteReplacement("\\"), Matcher.quoteReplacement("\\\\"))+"\"}";
    }    
}
