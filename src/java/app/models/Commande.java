/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import app.modelController.DetailCommandeJpaController;
import app.modelController.DetailReceptionJpaController;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "COMMANDE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Commande.findAll", query = "SELECT c FROM Commande c")
    , @NamedQuery(name = "Commande.findByRefCommande", query = "SELECT c FROM Commande c WHERE c.refCommande = :refCommande")
    , @NamedQuery(name = "Commande.findByDateCommande", query = "SELECT c FROM Commande c WHERE c.dateCommande = :dateCommande")
   })
public class Commande implements Serializable {

    @OneToMany(mappedBy = "refCommande")
    private List<Reception> receptionList;

    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_COMMANDE")
    private String refCommande;
    @Column(name = "DATE_COMMANDE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCommande;

    @Column(name = "TYPE_PAIE")
    private String typePaie;

    @Column(name = "PAIE_TRANSPORT")
    private int montantTransport = 0;

    @JoinColumn(name = "REF_PARC", referencedColumnName = "REF_PARC")
    @ManyToOne
    private ParcChargement refParc;
    @JoinColumn(name = "REF_FOURNISSEUR", referencedColumnName = "REF_FOURNISSEUR")
    @ManyToOne
    private Fournisseur refFournisseur;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refCommande")
    private List<DetailCommande> detailCommandeList;

    public Commande() {
    }

    public Commande(String refCommande) {
        this.refCommande = refCommande;
    }

    public String getRefCommande() {
        return refCommande;
    }

    public void setRefCommande(String refCommande) {
        this.refCommande = refCommande;
    }

    public Date getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(Date dateCommande) {
        this.dateCommande = dateCommande;
    }


    public Fournisseur getRefFournisseur() {
        return refFournisseur;
    }

    public void setRefFournisseur(Fournisseur refFournisseur) {
        this.refFournisseur = refFournisseur;
    }

    @XmlTransient
    public List<DetailCommande> getDetailCommandeList() {
        return detailCommandeList;
    }

    public void setDetailCommandeList(List<DetailCommande> detailCommandeList) {
        this.detailCommandeList = detailCommandeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refCommande != null ? refCommande.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commande)) {
            return false;
        }
        Commande other = (Commande) object;
        if ((this.refCommande == null && other.refCommande != null) || (this.refCommande != null && !this.refCommande.equals(other.refCommande))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        
        this.detailCommandeList = DetailCommandeJpaController.findDetailCommandeByCommande(refCommande);
        return " {" + 
                "\"ref\" : \"" + refCommande + "\", "
                + "\"date\" : \"" + dateCommande.getTime() + "\", "
                + "\"fournisseur\" : " + (refFournisseur == null ? "{}" : refFournisseur.baseString() )+ ", "
                + "\"parc\" : " + (refParc == null ? "{}" : refParc.toString())+ ", "
                + "\"detailCommande\": "+(this.detailCommandeList == null ? "[]" : this.detailCommandeList)+""
        + "}";
    }

    public String getTypePaie() {
        return typePaie;
    }

    public void setTypePaie(String typePaie) {
        this.typePaie = typePaie;
    }

    public int getMontantTransport() {
        return montantTransport;
    }

    public void setMontantTransport(int montantTransport) {
        this.montantTransport = montantTransport;
    }

    
    public float getVolumeTotal() {
        //System.out.println("############ "+refCommande);
        return  new  DetailReceptionJpaController(Database.getEntityManager()).getVolumeCommande(refCommande);
    }

    public ParcChargement getRefParc() {
        return refParc;
    }

    public void setRefParc(ParcChargement refParc) {
        this.refParc = refParc;
    }

    @XmlTransient
    public List<Reception> getReceptionList() {
        return receptionList;
    }

    public void setReceptionList(List<Reception> receptionList) {
        this.receptionList = receptionList;
    }
    public static String PRIX_RENDU = "PRIX RENDU";
    public static String PRIX_DEPART = "PRIX DÉPART";
    public void setTypePaie(String typePaie, String prix) {
        System.out.println("Type e= "+ typePaie+" prix = "+prix);
        if(!typePaie.equalsIgnoreCase("Prix Départ")){
            this.typePaie = PRIX_RENDU;
            montantTransport = Integer.parseInt(prix);
        }else{
            this.typePaie = PRIX_DEPART;
            montantTransport = -1;
        }
    }

    
    
}
