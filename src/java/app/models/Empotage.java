/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "EMPOTAGE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empotage.findAll", query = "SELECT e FROM Empotage e")
    , @NamedQuery(name = "Empotage.findByRefEmpotage", query = "SELECT e FROM Empotage e WHERE e.refEmpotage = :refEmpotage")
    , @NamedQuery(name = "Empotage.findByFermetureCaisse", query = "SELECT e FROM Empotage e WHERE e.fermetureCaisse = :fermetureCaisse")
    , @NamedQuery(name = "Empotage.findByHauteur", query = "SELECT e FROM Empotage e WHERE e.hauteur = :hauteur")
    , @NamedQuery(name = "Empotage.findByLargeur", query = "SELECT e FROM Empotage e WHERE e.largeur = :largeur")})
public class Empotage implements Serializable {

    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "HAUTEUR")
    private Double hauteur;
    @Column(name = "LARGEUR")
    private Double largeur;
    @Column(name = "LONGUEUR")
    private Double longueur;

    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_EMPOTAGE")
    private String refEmpotage;
    @Column(name = "FERMETURE_CAISSE")
    private String fermetureCaisse;
    @JoinColumn(name = "REF_COLIS", referencedColumnName = "REF_COLIS")
    @ManyToOne
    private Colis refColis;
    

    public Empotage() {
    }

    public Empotage(String refEmpotage) {
        this.refEmpotage = refEmpotage;
    }

    public String getRefEmpotage() {
        return refEmpotage;
    }

    public void setRefEmpotage(String refEmpotage) {
        this.refEmpotage = refEmpotage;
    }

    public String getFermetureCaisse() {
        return fermetureCaisse;
    }

    public void setFermetureCaisse(String fermetureCaisse) {
        this.fermetureCaisse = fermetureCaisse;
    }


    public Colis getRefColis() {
        return refColis;
    }

    public void setRefColis(Colis refColis) {
        this.refColis = refColis;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refEmpotage != null ? refEmpotage.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empotage)) {
            return false;
        }
        Empotage other = (Empotage) object;
        if ((this.refEmpotage == null && other.refEmpotage != null) || (this.refEmpotage != null && !this.refEmpotage.equals(other.refEmpotage))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{"
            + "\"" + "ref\": \"" + refEmpotage + "\", "
            + "\"fermetureCaisse\": \"" + fermetureCaisse + "\", "
            + "\"hauteur\": \"" + hauteur + "\", "
            + "\"largeur\": \"" + largeur + "\", "
            + "\"longueur\": \"" + longueur + "\", "
            + "\"refColis\": " + refColis + ""
        + "}";
    }

    public Double getHauteur() {
        return hauteur;
    }

    public void setHauteur(Double hauteur) {
        this.hauteur = hauteur;
    }

    public Double getLargeur() {
        return largeur;
    }

    public void setLargeur(Double largeur) {
        this.largeur = largeur;
    }

    public Double getLongueur() {
        return longueur;
    }

    public void setLongueur(Double longueur) {
        this.longueur = longueur;
    }
    
}
