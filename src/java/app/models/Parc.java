/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import app.controllers.DetailReceptionController;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "PARC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Parc.findAll", query = "SELECT p FROM Parc p")
    , @NamedQuery(name = "Parc.findByRefParc", query = "SELECT p FROM Parc p WHERE p.refParc = :refParc")
    , @NamedQuery(name = "Parc.findByDateMvt", query = "SELECT p FROM Parc p WHERE p.dateMvt = :dateMvt")
    , @NamedQuery(name = "Parc.findByLibelle", query = "SELECT p FROM Parc p WHERE p.libelle = :libelle")
    , @NamedQuery(name = "Parc.findByNumBille", query = "SELECT p FROM Parc p WHERE p.numBille = :numBille")
    , @NamedQuery(name = "Parc.findByTypeMvt", query = "SELECT p FROM Parc p WHERE p.typeMvt = :typeMvt")})
public class Parc implements Serializable {

    

    private static final long serialVersionUID = 1L;

    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    public static final String MVT_CHANGEMENT = "Changement de parc";
    public static final String MVT_MISE_PRODUCTION = "Mise en production";
    public static final String MVT_RETOUR_FOUNISSEUR = "Retour chez le fournisseur";
    public static final String MVT_CLASSER_EMBALLAGE = "Classer pour emballage";
    
    protected static String[] MVT = new String[]{
        MVT_CHANGEMENT, MVT_RETOUR_FOUNISSEUR, MVT_MISE_PRODUCTION, MVT_CLASSER_EMBALLAGE
    };
    
    public static String getMouvementListToString(){
            String liste = "[";
            for(String s: MVT){
                liste += "\""+s+"\",";
            }
            char[] chars = liste.toCharArray();
            chars[chars.length-1] = ']';
            liste = new String(chars);
            return liste;
    };

    
    public static String getMouvementListToSqlString(){
            String liste = "(";
            for(String s: MVT){
                liste += "'"+s+"',";
            }
            char[] chars = liste.toCharArray();
            chars[chars.length-1] = ')';
            liste = new String(chars);
            return liste;
    };

    @Id
    @Basic(optional = false)
    @Column(name = "REF_PARC")
    private String refParc;
    @Column(name = "DATE_MVT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMvt;
    @Column(name = "LIBELLE")
    private String libelle;
    @Column(name = "VISIBLE")
    private String visible = "F";
    @Column(name = "NUM_BILLE")
    private String numBille;
    @Column(name = "TYPE_MVT")
    private String typeMvt;
    @JoinColumn(name = "REF_TYPEPARC", referencedColumnName = "REF_TYPEPARC")
    @ManyToOne
    private TypeParc refTypeparc;
    @OneToMany(mappedBy = "refParc")
    private List<Billonnage> billonnageList;
    
    @JoinColumn(name = "REF_TYPEPARC_DEPART", referencedColumnName = "REF_TYPEPARC")
    @ManyToOne
    private TypeParc refTypeparcDepart;
    public Parc() {
    }

    public Parc(String refParc) {
        this.refParc = refParc;
    }

    public String getRefParc() {
        return refParc;
    }

    public void setRefParc(String refParc) {
        this.refParc = refParc;
    }

    public Date getDateMvt() {
        return dateMvt;
    }

    public void setDateMvt(Date dateMvt) {
        this.dateMvt = dateMvt;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getNumBille() {
        return numBille;
    }

    public void setNumBille(String numBille) {
        this.numBille = numBille;
    }

    public String getTypeMvt() {
        return typeMvt;
    }

    public void setTypeMvt(String typeMvt) {
        this.typeMvt = typeMvt;
    }

    public TypeParc getRefTypeparc() {
        return refTypeparc;
    }

    public void setRefTypeparc(TypeParc refTypeparc) {
        this.refTypeparc = refTypeparc;
    }

    @XmlTransient
    public List<Billonnage> getBillonnageList() {
        return billonnageList;
    }

    public void setBillonnageList(List<Billonnage> billonnageList) {
        this.billonnageList = billonnageList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refParc != null ? refParc.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parc)) {
            return false;
        }
        Parc other = (Parc) object;
        if ((this.refParc == null && other.refParc != null) || (this.refParc != null && !this.refParc.equals(other.refParc))) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
        return "{" + "\"ref\": \"" + refParc + "\", "
                + "\"numBille\": \"" + numBille + "\", "
                + "\"typeMvt\": \"" + typeMvt + "\", "
                + "\"dateMvt\": \"" + (dateMvt == null ? null : dateMvt.getTime()) + "\", "
                + "\"libelle\" : \"" + libelle + "\", "
                + "\"refTypeparc\": " + refTypeparc+ ", "
                + "\"refTypeparcDepart\": " + refTypeparcDepart+ ", "
                + "\"volume\": \""+DetailReceptionController.getVolumeForBille(this.getNumBille())+"\"}";
    }



    public TypeParc getRefTypeparcDepart() {
        return refTypeparcDepart;
    }

    public void setRefTypeparcDepart(TypeParc refTypeparcDepart) {
        this.refTypeparcDepart = refTypeparcDepart;
    }

    public void hide() {
        visible = "F";
    }

    public void show() {
        visible = "T";
    }

}
