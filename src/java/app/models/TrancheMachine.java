/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "TRANCHE_MACHINE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TrancheMachine.findAll", query = "SELECT t FROM TrancheMachine t")
    , @NamedQuery(name = "TrancheMachine.findByRefMachine", query = "SELECT t FROM TrancheMachine t WHERE t.refMachine = :refMachine")
    , @NamedQuery(name = "TrancheMachine.findByLibelle", query = "SELECT t FROM TrancheMachine t WHERE t.libelle = :libelle")})
public class TrancheMachine implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_MACHINE")
    private String refMachine;
    @Basic(optional = false)
    @Column(name = "LIBELLE")
    private String libelle;

    public TrancheMachine() {
    }

    public TrancheMachine(String refMachine) {
        this.refMachine = refMachine;
    }

    public TrancheMachine(String refMachine, String libelle) {
        this.refMachine = refMachine;
        this.libelle = libelle;
    }

    public String getRefMachine() {
        return refMachine;
    }

    public void setRefMachine(String refMachine) {
        this.refMachine = refMachine;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refMachine != null ? refMachine.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrancheMachine)) {
            return false;
        }
        TrancheMachine other = (TrancheMachine) object;
        if ((this.refMachine == null && other.refMachine != null) || (this.refMachine != null && !this.refMachine.equals(other.refMachine))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{\"ref\": \"" + refMachine + "\", \"libelle\": \"" + libelle + "\"}";
    }

    
}
