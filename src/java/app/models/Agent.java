/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "AGENT", catalog = "", schema = "ECAM")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Agent.findAll", query = "SELECT a FROM Agent a")
    , @NamedQuery(name = "Agent.findByMatriculeAgent", query = "SELECT a FROM Agent a WHERE a.matriculeAgent = :matriculeAgent")
    , @NamedQuery(name = "Agent.findByNomAgent", query = "SELECT a FROM Agent a WHERE a.nomAgent = :nomAgent")
    , @NamedQuery(name = "Agent.findByPrenom", query = "SELECT a FROM Agent a WHERE a.prenom = :prenom")
    , @NamedQuery(name = "Agent.findBySuspendu", query = "SELECT a FROM Agent a WHERE a.suspendu = :suspendu")})
public class Agent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "MATRICULE_AGENT", nullable = false, length = 50)
    private String matriculeAgent;
    @Basic(optional = false)
    @Column(name = "NOM_AGENT", nullable = false, length = 50)
    private String nomAgent;
    @Column(name = "PRENOM", length = 50)
    private String prenom;
    @Basic(optional = false)
    @Column(name = "SUSPENDU", nullable = false, length = 1)
    private String suspendu = "N";

    public Agent() {
    }

    public Agent(String matriculeAgent) {
        this.matriculeAgent = matriculeAgent;
    }

    public Agent(String matriculeAgent, String nomAgent, String suspendu) {
        this.matriculeAgent = matriculeAgent;
        this.nomAgent = nomAgent;
        this.suspendu = suspendu;
    }

    public String getMatriculeAgent() {
        return matriculeAgent;
    }

    public void setMatriculeAgent(String matriculeAgent) {
        this.matriculeAgent = matriculeAgent;
    }

    public String getNomAgent() {
        return nomAgent;
    }

    public void setNomAgent(String nomAgent) {
        this.nomAgent = nomAgent;
    }

    public String getPrenomAgent() {
        return prenom;
    }

    public void setPrenomAgent(String prenom) {
        this.prenom = prenom;
    }

    public String getSuspendu() {
        return suspendu;
    }

    public void setSuspendu(String suspendu) {
        this.suspendu = suspendu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (matriculeAgent != null ? matriculeAgent.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Agent)) {
            return false;
        }
        Agent other = (Agent) object;
        if ((this.matriculeAgent == null && other.matriculeAgent != null) || (this.matriculeAgent != null && !this.matriculeAgent.equals(other.matriculeAgent))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{\"matricule\": \"" + matriculeAgent + "\", \"nom\": \"" + nomAgent + "\", \"prenom\": \"" + prenom + "\", \"suspendu\": "+!suspendu.equalsIgnoreCase("n")+"}";
    }

    public void suspend() {
        suspendu = "O";
    }

    public void unSuspend() {
        suspendu = "N";
    }

}
