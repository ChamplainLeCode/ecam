/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "PERSONNEL_HISTORY")
@NamedQueries({
    @NamedQuery(name = "PersonnelHistory.findAll", query = "SELECT p FROM PersonnelHistory p")
    , @NamedQuery(name = "PersonnelHistory.findByDateOperation", query = "SELECT p FROM PersonnelHistory p WHERE p.dateOperation = :dateOperation")
    , @NamedQuery(name = "PersonnelHistory.findByMatricule", query = "SELECT p FROM PersonnelHistory p WHERE p.matricule = :matricule ORDER BY p.dateOperation DESC")
    , @NamedQuery(name = "PersonnelHistory.findByAction", query = "SELECT p FROM PersonnelHistory p WHERE p.action = :action")
    , @NamedQuery(name = "PersonnelHistory.findByIp", query = "SELECT p FROM PersonnelHistory p WHERE p.ip = :ip")})
public class PersonnelHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "DATE_OPERATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOperation;
    @Basic(optional = false)
    @Column(name = "MATRICULE")
    private String matricule;
    @Basic(optional = false)
    @Column(name = "ACTION")
    private String action;
    @Basic(optional = false)
    @Column(name = "IP")
    private String ip;

    public PersonnelHistory() {
    }

    public PersonnelHistory(Date dateOperation) {
        this.dateOperation = dateOperation;
    }

    public PersonnelHistory(Date dateOperation, String matricule, String action, String ip) {
        this.dateOperation = dateOperation;
        this.matricule = matricule;
        this.action = action;
        this.ip = ip;
    }

    public Date getDateOperation() {
        return dateOperation;
    }

    public void setDateOperation(Date dateOperation) {
        this.dateOperation = dateOperation; 
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dateOperation != null ? dateOperation.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PersonnelHistory)) {
            return false;
        }
        PersonnelHistory other = (PersonnelHistory) object;
        if ((this.dateOperation == null && other.dateOperation != null) || (this.dateOperation != null && !this.dateOperation.equals(other.dateOperation))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "app.models.PersonnelHistory[ dateOperation=" + dateOperation + " ]";
    }
    
}
