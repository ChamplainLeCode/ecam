/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "BATEAU", catalog = "", schema = "ECAM", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"LIBELLE_BEAU"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bateau.findAll", query = "SELECT b FROM Bateau b")
    , @NamedQuery(name = "Bateau.findByRefBateau", query = "SELECT b FROM Bateau b WHERE b.refBateau = :refBateau")
    , @NamedQuery(name = "Bateau.findByLibelleBeau", query = "SELECT b FROM Bateau b WHERE b.libelleBeau = :libelleBeau")})
public class Bateau implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_BATEAU", nullable = false, length = 50)
    private String refBateau;
    @Basic(optional = false)
    @Column(name = "LIBELLE_BEAU", nullable = false, length = 100)
    private String libelleBeau;

    public Bateau() {
    }

    public Bateau(String refBateau) {
        this.refBateau = refBateau;
    }

    public Bateau(String refBateau, String libelleBeau) {
        this.refBateau = refBateau;
        this.libelleBeau = libelleBeau;
    }

    public String getRefBateau() {
        return refBateau;
    }

    public void setRefBateau(String refBateau) {
        this.refBateau = refBateau;
    }

    public String getLibelleBeau() {
        return libelleBeau;
    }

    public void setLibelleBeau(String libelleBeau) {
        this.libelleBeau = libelleBeau;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refBateau != null ? refBateau.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bateau)) {
            return false;
        }
        Bateau other = (Bateau) object;
        if ((this.refBateau == null && other.refBateau != null) || (this.refBateau != null && !this.refBateau.equals(other.refBateau))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{\"ref\": \"" + refBateau + "\", \"libelle\": \""+libelleBeau+"\"}";
    }
    
}
