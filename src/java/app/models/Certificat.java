/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import app.controllers.CertificatController;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "CERTIFICAT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Certificat.findAll", query = "SELECT c FROM Certificat c")
    , @NamedQuery(name = "Certificat.findByIdentifiant", query = "SELECT c FROM Certificat c WHERE c.identifiant = :identifiant")
    , @NamedQuery(name = "Certificat.findByLibelle", query = "SELECT c FROM Certificat c WHERE c.libelle = :libelle")
    , @NamedQuery(name = "Certificat.findByRefFournisseur", query = "SELECT c FROM Certificat c WHERE c.refFournisseur = :refFournisseur")})
public class Certificat implements Serializable {
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "IDENTIFIANT")
    private String identifiant;
    @Basic(optional = false)
    @Column(name = "LIBELLE")
    private String libelle;
    @Basic(optional = false)
    @Column(name = "REF_FOURNISSEUR")
    private String refFournisseur;

    public Certificat() {
    }

    public Certificat(String identifiant) {
        this.identifiant = identifiant;
    }

    public Certificat(String identifiant, String libelle, String refFournisseur) {
        this.identifiant = identifiant;
        this.libelle = libelle;
        this.refFournisseur = refFournisseur;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getRefFournisseur() {
        return refFournisseur;
    }

    public void setRefFournisseur(String refFournisseur) {
        this.refFournisseur = refFournisseur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (identifiant != null ? identifiant.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Certificat)) {
            return false;
        }
        Certificat other = (Certificat) object;
        if ((this.identifiant == null && other.identifiant != null) || (this.identifiant != null && !this.identifiant.equals(other.identifiant))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{\"ref\": \"" + identifiant + "\", \"libelle\":\""+libelle+"\", \"fournisseur\":\""+refFournisseur+"\"}";
    }
    
    public static List<Certificat> getCertificatFor(String fournisseur){
        return CertificatController.getCertificatFor(fournisseur);
    }
    
}
