/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "FEUILLE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Feuille.findAll", query = "SELECT f FROM Feuille f")
    , @NamedQuery(name = "Feuille.findByRefFeuille", query = "SELECT f FROM Feuille f WHERE f.refFeuille = :refFeuille")})
public class Feuille implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_FEUILLE")
    private String refFeuille;
    @JoinColumn(name = "REF_PLOT", referencedColumnName = "REF_PLOT")
    @ManyToOne
    private Plot refPlot;

    public Feuille() {
    }

    public Feuille(String refFeuille) {
        this.refFeuille = refFeuille;
    }

    public String getRefFeuille() {
        return refFeuille;
    }

    public void setRefFeuille(String refFeuille) {
        this.refFeuille = refFeuille;
    }

    public Plot getRefPlot() {
        return refPlot;
    }

    public void setRefPlot(Plot refPlot) {
        this.refPlot = refPlot;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refFeuille != null ? refFeuille.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Feuille)) {
            return false;
        }
        Feuille other = (Feuille) object;
        if ((this.refFeuille == null && other.refFeuille != null) || (this.refFeuille != null && !this.refFeuille.equals(other.refFeuille))) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
        return "{\"" + "ref\": \"" + refFeuille + "\", \"refPlot\": \"" + refPlot + "\"}";
    }
    
    
}
