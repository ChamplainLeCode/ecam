/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "DOUANIER", catalog = "", schema = "ECAM")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Douanier.findAll", query = "SELECT d FROM Douanier d")
    , @NamedQuery(name = "Douanier.findByMatriculeDouanier", query = "SELECT d FROM Douanier d WHERE d.matriculeDouanier = :matriculeDouanier")
    , @NamedQuery(name = "Douanier.findByNomDouanier", query = "SELECT d FROM Douanier d WHERE d.nomDouanier = :nomDouanier")
    , @NamedQuery(name = "Douanier.findByPrenomDouanier", query = "SELECT d FROM Douanier d WHERE d.prenomDouanier = :prenomDouanier")})
public class Douanier implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "MATRICULE_DOUANIER", nullable = false, length = 50)
    private String matriculeDouanier;
    @Basic(optional = false)
    @Column(name = "NOM_DOUANIER", nullable = false, length = 50)
    private String nomDouanier;
    @Column(name = "PRENOM_DOUANIER", length = 50)
    private String prenomDouanier;
    @Column(name = "SUSPENDU", length = 1)
    private String suspendu = "N";

    public String getSuspendu() {
        return suspendu;
    }

    public void setSuspendu(String suspendu) {
        this.suspendu = suspendu;
    }

    public Douanier() {
    }

    public Douanier(String matriculeDouanier) {
        this.matriculeDouanier = matriculeDouanier;
    }

    public Douanier(String matriculeDouanier, String nomDouanier) {
        this.matriculeDouanier = matriculeDouanier;
        this.nomDouanier = nomDouanier;
    }

    public String getMatriculeDouanier() {
        return matriculeDouanier;
    }

    public void setMatriculeDouanier(String matriculeDouanier) {
        this.matriculeDouanier = matriculeDouanier;
    }

    public String getNomDouanier() {
        return nomDouanier;
    }

    public void setNomDouanier(String nomDouanier) {
        this.nomDouanier = nomDouanier;
    }

    public String getPrenomDouanier() {
        return prenomDouanier;
    }

    public void setPrenomDouanier(String prenomDouanier) {
        this.prenomDouanier = prenomDouanier;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (matriculeDouanier != null ? matriculeDouanier.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Douanier)) {
            return false;
        }
        Douanier other = (Douanier) object;
        if ((this.matriculeDouanier == null && other.matriculeDouanier != null) || (this.matriculeDouanier != null && !this.matriculeDouanier.equals(other.matriculeDouanier))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{\"matricule\": \"" + matriculeDouanier + "\", \"nom\": \"" + nomDouanier + "\", \"prenom\": \"" + prenomDouanier + "\", \"suspendu\": "+!suspendu.equalsIgnoreCase("n")+"}";
    }

    public void suspend() {
        suspendu = "O";
    }

    public void unSuspend() {
        suspendu = "N";
    }

}
