/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "CLASSIFICATION_ESS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClassificationEss.findAll", query = "SELECT c FROM ClassificationEss c")
    , @NamedQuery(name = "ClassificationEss.findByRefClassification", query = "SELECT c FROM ClassificationEss c WHERE c.refClassification = :refClassification")
    , @NamedQuery(name = "ClassificationEss.findByEpaisseur", query = "SELECT c FROM ClassificationEss c WHERE c.epaisseur = :epaisseur")})
public class ClassificationEss implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_CLASSIFICATION")
    private String refClassification;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "EPAISSEUR")
    private Double epaisseur;
    @JoinColumn(name = "REF_ESSENCE", referencedColumnName = "REF_ESSENCE")
    @ManyToOne
    private Essence refEssence;

    public ClassificationEss() {
    }

    public ClassificationEss(String refClassification) {
        this.refClassification = refClassification;
    }

    public String getRefClassification() {
        return refClassification;
    }

    public void setRefClassification(String refClassification) {
        this.refClassification = refClassification;
    }

    public Double getEpaisseur() {
        return epaisseur;
    }

    public void setEpaisseur(Double epaisseur) {
        this.epaisseur = epaisseur;
    }

    public Essence getRefEssence() {
        return refEssence;
    }

    public void setRefEssence(Essence refEssence) {
        this.refEssence = refEssence;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refClassification != null ? refClassification.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClassificationEss)) {
            return false;
        }
        ClassificationEss other = (ClassificationEss) object;
        if ((this.refClassification == null && other.refClassification != null) || (this.refClassification != null && !this.refClassification.equals(other.refClassification))) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
            return "{\"ref\": \""+refClassification+"\", "+
            "\"epaisseur\": "+epaisseur+", "+
            "\"essence\":"+  refEssence+"}";
    }

    
}
