/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "DETAIL_COMMANDE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailCommande.findAll", query = "SELECT d FROM DetailCommande d")
    , @NamedQuery(name = "DetailCommande.findByRefDetail", query = "SELECT d FROM DetailCommande d WHERE d.refDetail = :refDetail")
    , @NamedQuery(name = "DetailCommande.findByVolume", query = "SELECT d FROM DetailCommande d WHERE d.volume = :volume")
    , @NamedQuery(name = "DetailCommande.findByNumBille", query = "SELECT d FROM DetailCommande d WHERE d.numBille = :numBille")
    , @NamedQuery(name = "DetailCommande.findByGdDiametre", query = "SELECT d FROM DetailCommande d WHERE d.gdDiametre = :gdDiametre")
    , @NamedQuery(name = "DetailCommande.findByPtDiametre", query = "SELECT d FROM DetailCommande d WHERE d.ptDiametre = :ptDiametre")
    , @NamedQuery(name = "DetailCommande.findByMyDiamtre", query = "SELECT d FROM DetailCommande d WHERE d.myDiamtre = :myDiamtre")
    , @NamedQuery(name = "DetailCommande.findByObservation", query = "SELECT d FROM DetailCommande d WHERE d.observation = :observation")})
public class DetailCommande implements Serializable {


    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    @Basic(optional = false)
    @Column(name = "GD_DIAMETRE")
    private float gdDiametre;
    @Basic(optional = false)
    @Column(name = "PT_DIAMETRE")
    private float ptDiametre;
    @Basic(optional = false)
    @Column(name = "MY_DIAMTRE")
    private float myDiamtre;
    @Column(name = "LONGUEUR")
    private float longueur;
    @Column(name = "PRIX_UNITAIRE")
    private float prixUnitaire;
    @Basic(optional = false)
    @Column(name = "NUM_BILLE")
    private String numBille;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_DETAIL")
    private String refDetail;
    @Basic(optional = false)
    @Column(name = "VOLUME")
    private double volume;
    @Basic(optional = false)
    @Column(name = "OBSERVATION")
    private String observation;
    @JoinColumn(name = "REF_COMMANDE", referencedColumnName = "REF_COMMANDE")
    @ManyToOne(optional = false)
    private Commande refCommande;
    @JoinColumn(name = "REF_ESSENCE", referencedColumnName = "REF_ESSENCE")
    @ManyToOne(optional = false)
    private Essence refEssence;

    public DetailCommande() {
    }

    public DetailCommande(String refDetail) {
        this.refDetail = refDetail;
    }

    public DetailCommande(String refDetail, double volume, String numBille, float gdDiametre, float ptDiametre, float myDiamtre, String observation) {
        this.refDetail = refDetail;
        this.volume = volume;
        this.numBille = numBille;
        this.gdDiametre = gdDiametre;
        this.ptDiametre = ptDiametre;
        this.myDiamtre = myDiamtre;
        this.observation = observation;
    }

    public String getRefDetail() {
        return refDetail;
    }

    public void setRefDetail(String refDetail) {
        this.refDetail = refDetail;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public String getNumBille() {
        return numBille;
    }

    public void setNumBille(String numBille) {
        this.numBille = numBille;
    }

    public float getGdDiametre() {
        return gdDiametre;
    }

    public void setGdDiametre(float gdDiametre) {
        this.gdDiametre = gdDiametre;
    }

    public float getPtDiametre() {
        return ptDiametre;
    }

    public void setPtDiametre(float ptDiametre) {
        this.ptDiametre = ptDiametre;
    }

    public float getMyDiamtre() {
        return myDiamtre;
    }

    public void setMyDiamtre(float myDiamtre) {
        this.myDiamtre = myDiamtre;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Commande getRefCommande() {
        return refCommande;
    }

    public void setRefCommande(Commande refCommande) {
        this.refCommande = refCommande;
    }

    public Essence getRefEssence() {
        return refEssence;
    }

    public void setRefEssence(Essence refEssence) {
        this.refEssence = refEssence;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refDetail != null ? refDetail.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailCommande)) {
            return false;
        }
        DetailCommande other = (DetailCommande) object;
        if ((this.refDetail == null && other.refDetail != null) || (this.refDetail != null && !this.refDetail.equals(other.refDetail))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{" + ""
                + "\"numBille\": \"" + numBille + "\", "
                + "\"gdDiam\": \"" + gdDiametre + "\", "
                + "\"pttDiam\": \"" + ptDiametre + "\", "
                + "\"moyDiam\": \"" + myDiamtre + "\", "
                + "\"ref\": \"" + refDetail + "\", "
                + "\"prix\": \"" + prixUnitaire + "\", "
                + "\"volume\": \"" + volume + "\", "
                + "\"longueur\": \"" + longueur + "\", "
                + "\"observation\": \"" + observation + "\", "
                + "\"commande\": \"" + refCommande.getRefCommande() + "\", "
                + "\"essence\": " + refEssence + ""
                + "}";
    }
    
    public float getLongueur() {
        return longueur;
    }

    public void setLongueur(float longueur) {
        this.longueur = longueur;
    }

    public float getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(float prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }
    
}
