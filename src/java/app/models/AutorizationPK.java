/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author champlain
 */
@Embeddable
public class AutorizationPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "ROUTE")
    private String route;
    @Basic(optional = false)
    @Column(name = "PRIVILEGE")
    private String privilege;

    public AutorizationPK() {
    }

    public AutorizationPK(String route, String privilege) {
        this.route = route;
        this.privilege = privilege;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getPrivilege() {
        return privilege;
    }

    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (route != null ? route.hashCode() : 0);
        hash += (privilege != null ? privilege.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AutorizationPK)) {
            return false;
        }
        AutorizationPK other = (AutorizationPK) object;
        if ((this.route == null && other.route != null) || (this.route != null && !this.route.equals(other.route))) {
            return false;
        }
        if ((this.privilege == null && other.privilege != null) || (this.privilege != null && !this.privilege.equals(other.privilege))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "app.models.AutorizationPK[ route=" + route + ", privilege=" + privilege + " ]";
    }
    
}
