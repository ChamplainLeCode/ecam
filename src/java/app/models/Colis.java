/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;


import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "COLIS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Colis.findAll", query = "SELECT c FROM Colis c")
    , @NamedQuery(name = "Colis.findByRefColis", query = "SELECT c FROM Colis c WHERE c.refColis = :refColis")
    , @NamedQuery(name = "Colis.findByDateDebut", query = "SELECT c FROM Colis c WHERE c.dateDebut = :dateDebut")
    , @NamedQuery(name = "Colis.findByPoidEmballage", query = "SELECT c FROM Colis c WHERE c.poidEmballage = :poidEmballage")
    , @NamedQuery(name = "Colis.findByPoidNet", query = "SELECT c FROM Colis c WHERE c.poidNet = :poidNet")
    , @NamedQuery(name = "Colis.findByPoidsColis", query = "SELECT c FROM Colis c WHERE c.poidsColis = :poidsColis")
    , @NamedQuery(name = "Colis.findByVoulumeColis", query = "SELECT c FROM Colis c WHERE c.voulumeColis = :voulumeColis")
    , @NamedQuery(name = "Colis.findByNbrePaquet", query = "SELECT c FROM Colis c WHERE c.nbrePaquet = :nbrePaquet")
    , @NamedQuery(name = "Colis.findByLongueurColis", query = "SELECT c FROM Colis c WHERE c.longueurColis = :longueurColis")
    , @NamedQuery(name = "Colis.findByLargeurColis", query = "SELECT c FROM Colis c WHERE c.largeurColis = :largeurColis")
    , @NamedQuery(name = "Colis.findByFermetureColis", query = "SELECT c FROM Colis c WHERE c.fermetureColis = :fermetureColis")
    , @NamedQuery(name = "Colis.findByCodeBarColis", query = "SELECT c FROM Colis c WHERE c.codeBarColis = :codeBarColis")
    , @NamedQuery(name = "Colis.findByDateFin", query = "SELECT c FROM Colis c WHERE c.dateFin = :dateFin")
    , @NamedQuery(name = "Colis.findByDateFermeture", query = "SELECT c FROM Colis c WHERE c.dateFermeture = :dateFermeture")})
public class Colis implements Serializable {


    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "POID_EMBALLAGE")
    private Double poidEmballage;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "POID_NET")
    private Double poidNet;
    @Column(name = "POIDS_COLIS")
    private Double poidsColis;
    @Column(name = "TYPE_COLIS")
    private Character typeColis = 'P';
    @Column(name = "NBRE_PAQUET")
    private int nbrePaquet;
    @Column(name = "LONGUEUR_COLIS")
    private double longueurColis;
    @Column(name = "LARGEUR_COLIS")
    private double largeurColis;
    @Column(name = "EPAISEUR")
    private Double epaiseur;
    @Column(name = "JOINTEUR")
    private String jointeur;

    public String getJointeur() {
        return jointeur;
    }

    public void setJointeur(String jointeur) {
        this.jointeur = jointeur;
    }
    @ManyToMany(mappedBy = "colisList")
    private List<Embarquement> embarquementList;
    @JoinTable(name = "JOINTAGE_COLIS", joinColumns = {
        @JoinColumn(name = "REF_COLIS", referencedColumnName = "REF_COLIS")}, inverseJoinColumns = {
        @JoinColumn(name = "REF_JOINTAGE", referencedColumnName = "REF_JOINTAGE")})
    @ManyToMany
    private List<Jointage> jointageList;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "colisFinal")
    private Jointage jointage;
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
    @Basic(optional = false)
    @Column(name = "EPAISSEUR")
    private double epaisseur;

    public Character getTypeColis() {
        return typeColis;
    }

    public String getTypeColisFull() {
        return typeColis == 'J' ? "Jointage": "Placage";
    }

    public void setColisJoin() {
        this.typeColis = 'J';
    }

    public void setColisSimple() {
        this.typeColis = 'P';
    }
    @Basic(optional = false)
    @Column(name = "CREATE_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt = new Date();
    @Column(name = "ESSENCE")
    private String essence = "";
    @JoinColumn(name = "NUM_PLOMB", referencedColumnName = "NUM_PLOMB")
    @ManyToOne
    private Embarquement embarquement;
    @Column(name = "SURFACE_COLIS")
    private Double surfaceColis = 0.00D;
    @OneToMany(mappedBy = "refColis")
    private List<Paquet> paquetList;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_COLIS")
    private String refColis;
    @Column(name = "DATE_DEBUT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateDebut;
    @Column(name = "VOULUME_COLIS")
    private Double voulumeColis = 0D;
    @Column(name = "FERMETURE_COLIS")
    private String fermetureColis = "";
    @Column(name = "REF_CODEBARCOLIS")
    private String codeBarColis = ""+new Date().getTime();
    @Column(name = "QUALITE_COLIS")
    private String qualite = "";
    @Column(name = "DATE_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFin;
    @Column(name = "DATE_FERMETURE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFermeture;
    @OneToMany(mappedBy = "refColis")
    private List<Empotage> empotageList;
    
    
    
    public Colis() {
    }

    public Colis(String refColis) {
        this.refColis = refColis;
    }

    public String getRefColis() {
        return refColis;
    }

    public void setRefColis(String refColis) {
        this.refColis = refColis;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Double getPoidEmballage() {
        return poidEmballage;
    }

    public void setPoidEmballage(Double poidEmballage) {
        this.poidEmballage = poidEmballage;
    }

    public Double getPoidNet() {
        return poidNet;
    }

    public void setPoidNet(Double poidNet) {
        this.poidNet = poidNet;
    }

    public Double getPoidsColis() {
        return poidsColis;
    }

    public void setPoidsColis(Double poidsColis) {
        this.poidsColis = poidsColis;
    }

    public Double getVoulumeColis() {
        return voulumeColis;
    }

    public void setVoulumeColis(Double voulumeColis) {
        this.voulumeColis = voulumeColis;
    }

    public int getNbrePaquet() {
        return nbrePaquet;
    }

    public void setNbrePaquet(int nbrePaquet) {
        this.nbrePaquet = nbrePaquet;
    }

    public Double getLongueurColis() {
        return longueurColis;
    }

    public void setLongueurColis(Double longueurColis) {
        this.longueurColis = longueurColis;
    }

    public Double getLargeurColis() {
        return largeurColis;
    }

    public void setLargeurColis(Double largeurColis) {
        this.largeurColis = largeurColis;
    }

    public String getFermetureColis() {
        return fermetureColis;
    }

    public void setFermetureColis(String fermetureColis) {
        this.fermetureColis = fermetureColis;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public Date getDateFermeture() {
        return dateFermeture;
    }

    public void setDateFermeture(Date dateFermeture) {
        this.dateFermeture = dateFermeture;
    }

    @XmlTransient
    public List<Empotage> getEmpotageList() {
        return empotageList;
    }

    public void setEmpotageList(List<Empotage> empotageList) {
        this.empotageList = empotageList;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refColis != null ? refColis.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Colis)) {
            return false;
        }
        Colis other = (Colis) object;
        if ((this.refColis == null && other.refColis != null) || (this.refColis != null && !this.refColis.equals(other.refColis))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{" + "\"refColis\": \"" + refColis + "\", "
                + "\"dateDebut\": " + dateDebut.getTime() + ", "
                + "\"poidEmballage\": \"" + poidEmballage + "\", "
                + "\"poidNet\": \"" + poidNet + "\", "
                + "\"poidsColis\": \"" + poidsColis + "\", "
                + "\"surface\": \"" + surfaceColis + "\", "
                + "\"qualite\": \"" + qualite + "\", "
                + "\"epaisseur\": \"" + epaisseur + "\", "
                + "\"volume\": \"" + voulumeColis + "\", "
                + "\"contenaire\": \"" + (embarquement == null ? ' ' : embarquement.getNumPlomb()) + "\", "
                + "\"nbrePaquet\": \"" + nbrePaquet + "\", "
                + "\"longueurColis\": \"" + longueurColis + "\", "
                + "\"largeurColis\": \"" + largeurColis + "\", "
                + "\"fermetureColis\": \"" + fermetureColis + "\", "
                + "\"dateFin\": " + (dateFin == null ? null : dateFin.getTime()) + ", "
                + "\"dateFermeture\": " + (dateFermeture == null ? null : dateFermeture.getTime()) + ", "
                + "\"codeBarColis\": \"" + codeBarColis + "\", "
                + "\"essence\": \"" + essence + "\", "
                + "\"isJoin\": \"" + typeColis + "\", "
                + "\"embarquement\": \"" + (embarquement == null ? "" : embarquement.getNumPlomb()) + "\" "
        + "}";
    }

 public String getCodeBarColis() {
        return codeBarColis;
    }

    public void setCodeBarColis(String codeBarColis) {
        this.codeBarColis = codeBarColis;
    }


    public Double getSurfaceColis() {
        return surfaceColis;
    }

    public void setSurfaceColis(Double surfaceColis) {
        this.surfaceColis = surfaceColis;
    }

    @XmlTransient
    public List<Paquet> getPaquetList() {
        return paquetList;
    }

    public void setPaquetList(List<Paquet> paquetList) {
        this.paquetList = paquetList;
    }

    public void setQualite(String qualite) {
        this.qualite = qualite;
    }

    public String getQualite() {
        return qualite;
    }
//    public Embarquement getRefContennaire() {
//        return embarquement;
//    }

//    public void setRefContennaire(Embarquement embarquement) {
//        this.embarquement = embarquement;
//    }

    public void setEssence(String libelle) {
        essence = libelle;
    }


    public void setLongueurColis(double longueurColis) {
        this.longueurColis = longueurColis;
    }

    public void setLargeurColis(double largeurColis) {
        this.largeurColis = largeurColis;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }





    public Embarquement getNumPlomb() {
        return embarquement;
    }

    public void setNumPlomb(Embarquement numPlomb) {
        this.embarquement = numPlomb;
    }

    public void setPoidEmballage(double poidEmballage) {
        this.poidEmballage = poidEmballage;
    }


    public void setPoidNet(double poidNet) {
        this.poidNet = poidNet;
    }


    public void setPoidsColis(double poidsColis) {
        this.poidsColis = poidsColis;
    }
    public double getEpaisseur() {
        return epaisseur;
    }

    public void setEpaisseur(double epaisseur) {
        this.epaisseur = epaisseur;
    }

    public String getEssence() {
        return essence;
    }

    public Embarquement getEmbarquement() {
        return embarquement;
    }

    public void setLongueurColis(int longueurColis) {
        this.longueurColis = longueurColis;
    }

    public void setLargeurColis(int largeurColis) {
        this.largeurColis = largeurColis;
    }

    public Double getEpaiseur() {
        return epaiseur;
    }

    public void setEpaiseur(Double epaiseur) {
        this.epaiseur = epaiseur;
    }

    @XmlTransient
    public List<Embarquement> getEmbarquementList() {
        return embarquementList;
    }

    public void setEmbarquementList(List<Embarquement> embarquementList) {
        this.embarquementList = embarquementList;
    }

    @XmlTransient
    public List<Jointage> getJointageList() {
        return jointageList;
    }

    public void setJointageList(List<Jointage> jointageList) {
        this.jointageList = jointageList;
    }

    public Jointage getJointage() {
        return jointage;
    }

    public void setJointage(Jointage jointage) {
        this.jointage = jointage;
    }

    public void setEmbarquement(Embarquement embarquement) {
        this.embarquement = embarquement;
    }
}
