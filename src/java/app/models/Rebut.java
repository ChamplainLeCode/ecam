/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "REBUT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rebut.findAll", query = "SELECT r FROM Rebut r")
    , @NamedQuery(name = "Rebut.findByRefRebut", query = "SELECT r FROM Rebut r WHERE r.refRebut = :refRebut")
    , @NamedQuery(name = "Rebut.findByDateRebut", query = "SELECT r FROM Rebut r WHERE r.dateRebut = :dateRebut")
    , @NamedQuery(name = "Rebut.findByTypeRebut", query = "SELECT r FROM Rebut r WHERE r.typeRebut = :typeRebut")})
public class Rebut implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_REBUT")
    private String refRebut;
    @Column(name = "DATE_REBUT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateRebut;
    @Column(name = "TYPE_REBUT")
    private String typeRebut;
    @Column(name = "REF")
    private String ref;

    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Rebut() {
    }

    public Rebut(String refRebut) {
        this.refRebut = refRebut;
    }

    public String getRefRebut() {
        return refRebut;
    }

    public void setRefRebut(String refRebut) {
        this.refRebut = refRebut;
    }

    public Date getDateRebut() {
        return dateRebut;
    }

    public void setDateRebut(Date dateRebut) {
        this.dateRebut = dateRebut;
    }

    public String getTypeRebut() {
        return typeRebut;
    }

    public void setTypeRebut(String typeRebut) {
        this.typeRebut = typeRebut;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refRebut != null ? refRebut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rebut)) {
            return false;
        }
        Rebut other = (Rebut) object;
        if ((this.refRebut == null && other.refRebut != null) || (this.refRebut != null && !this.refRebut.equals(other.refRebut))) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
        return "{" + "\"ref\": \"" + refRebut + "\", \"typeRebut\": \"" + typeRebut + "\", \"objectRef\" : \"" + ref + "\", \"dateRebut\" : " + dateRebut.getTime() + "}";
    }
    
}
