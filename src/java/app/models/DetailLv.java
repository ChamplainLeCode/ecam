/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "DETAIL_LV", catalog = "", schema = "ECAM", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"BILLE"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailLv.findAll", query = "SELECT d FROM DetailLv d")
    , @NamedQuery(name = "DetailLv.findByRefDlv", query = "SELECT d FROM DetailLv d WHERE d.refDlv = :refDlv")
    , @NamedQuery(name = "DetailLv.findByBille", query = "SELECT d FROM DetailLv d WHERE d.bille = :bille")
    , @NamedQuery(name = "DetailLv.findByGdDiam", query = "SELECT d FROM DetailLv d WHERE d.gdDiam = :gdDiam")
    , @NamedQuery(name = "DetailLv.findByPttDiam", query = "SELECT d FROM DetailLv d WHERE d.pttDiam = :pttDiam")
    , @NamedQuery(name = "DetailLv.findByVolume", query = "SELECT d FROM DetailLv d WHERE d.volume = :volume")
    , @NamedQuery(name = "DetailLv.findByLongueur", query = "SELECT d FROM DetailLv d WHERE d.longueur = :longueur")})
public class DetailLv implements Serializable {

    @Basic(optional = false)
    @Column(name = "ESSENCE")
    private int essence;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_DLV", nullable = false, length = 255)
    private String refDlv;
    @Column(name = "BILLE", length = 50)
    private String bille;
    @Column(name = "CERTIFICAT", length = 50)
    private String certificat;
    @Basic(optional = false)
    @Column(name = "GD_DIAM", nullable = false)
    private double gdDiam;
    @Basic(optional = false)
    @Column(name = "PTT_DIAM", nullable = false)
    private double pttDiam;

    public String getCertificat() {
        return certificat;
    }

    public void setCertificat(String certificat) {
        this.certificat = certificat;
    }
    @Basic(optional = false)
    @Column(name = "VOLUME", nullable = false)
    private double volume;
    @Basic(optional = false)
    @Column(name = "LONGUEUR", nullable = false)
    private double longueur;

    public DetailLv() {
    }

    public DetailLv(String refDlv) {
        this.refDlv = refDlv;
    }

    public DetailLv(String refDlv, double gdDiam, double pttDiam, double volume, double longueur) {
        this.refDlv = refDlv;
        this.gdDiam = gdDiam;
        this.pttDiam = pttDiam;
        this.volume = volume;
        this.longueur = longueur;
    }

    public String getRefDlv() {
        return refDlv;
    }

    public void setRefDlv(String refDlv) {
        this.refDlv = refDlv;
    }

    public String getBille() {
        return bille;
    }

    public void setBille(String bille) {
        this.bille = bille;
    }

    public double getGdDiam() {
        return gdDiam;
    }

    public void setGdDiam(double gdDiam) {
        this.gdDiam = gdDiam;
    }

    public double getPttDiam() {
        return pttDiam;
    }

    public void setPttDiam(double pttDiam) {
        this.pttDiam = pttDiam;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public double getLongueur() {
        return longueur;
    }

    public void setLongueur(double longueur) {
        this.longueur = longueur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refDlv != null ? refDlv.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailLv)) {
            return false;
        }
        DetailLv other = (DetailLv) object;
        if ((this.refDlv == null && other.refDlv != null) || (this.refDlv != null && !this.refDlv.equals(other.refDlv))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{" + "\"ref\":\"" + refDlv + "\", \"bille\": \"" + bille + "\", \"gdDiam\": \"" + gdDiam + "\", \"pttDiam\": \"" + pttDiam + "\", \"volume\": \"" + volume + "\", \"longueur\": \"" + longueur + "\"}";
    }

    public int getEssence() {
        return essence;
    }

    public void setEssence(int essence) {
        this.essence = essence;
    }

}
