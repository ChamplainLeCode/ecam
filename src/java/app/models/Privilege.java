/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import app.modelController.PrivilegeJpaController;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "PRIVILEGE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Privilege.findAll", query = "SELECT p FROM Privilege p")
    , @NamedQuery(name = "Privilege.findByRefPrivilege", query = "SELECT p FROM Privilege p WHERE p.refPrivilege = :refPrivilege")
    , @NamedQuery(name = "Privilege.findByDescription", query = "SELECT p FROM Privilege p WHERE p.description = :description")
    , @NamedQuery(name = "Privilege.findByLibele", query = "SELECT p FROM Privilege p WHERE p.libele = :libele")})
public class Privilege implements Serializable {

    @JoinTable(name = "AUTORIZATION", joinColumns = {
        @JoinColumn(name = "PRIVILEGE", referencedColumnName = "REF_PRIVILEGE")}, inverseJoinColumns = {
        @JoinColumn(name = "ROUTE", referencedColumnName = "NOM")})
    @ManyToMany
    private List<Route> routeList = new LinkedList<>();


    @OneToMany(mappedBy = "fonction")
    private List<Personnel> personnelList;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_PRIVILEGE")
    private String refPrivilege;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "LIBELE")
    private String libele;

    
    
    public Privilege() {
    }

    public Privilege(String refPrivilege) {
        this.refPrivilege = refPrivilege;
    }

    public String getRefPrivilege() {
        return refPrivilege;
    }

    public void setRefPrivilege(String refPrivilege) {
        this.refPrivilege = refPrivilege;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLibele() {
        return libele;
    }

    public void setLibele(String libele) {
        this.libele = libele;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refPrivilege != null ? refPrivilege.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Privilege)) {
            return false;
        }
        Privilege other = (Privilege) object;
        if ((this.refPrivilege == null && other.refPrivilege != null) || (this.refPrivilege != null && !this.refPrivilege.equals(other.refPrivilege))) {
            return false;
        }
        return true;
    }


    public void loadRoute(){
        //routeList = new LinkedList<>();
        routeList = new PrivilegeJpaController(Database.getEntityManager()).getRouteForPrivilege(this.refPrivilege);
    }
    
    @Override
    public String toString() {
        return "{"
                + "\"" + "ref\": \"" + refPrivilege + "\", "
                + "\"libelle\": \"" + libele + "\", "
                + "\"description\": \"" + description + "\", "
                + "\"routes\": " + (routeList == null ? new LinkedList<>() : routeList).toString() + ""
            + "}";
    }


    @XmlTransient
    public List<Route> getRouteList() {
        return routeList;
    }

    public void setRouteList(List<Route> routeList) {
        this.routeList = routeList;
    }

    @XmlTransient
    public List<Personnel> getPersonnelList() {
        return personnelList;
    }

    public void setPersonnelList(List<Personnel> personnelList) {
        this.personnelList = personnelList;
    }
    
    
}
