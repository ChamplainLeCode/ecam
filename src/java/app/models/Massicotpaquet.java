/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "MASSICOTPAQUET")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Massicotpaquet.findAll", query = "SELECT m FROM Massicotpaquet m")
    , @NamedQuery(name = "Massicotpaquet.findByRefMassicotpaquet", query = "SELECT m FROM Massicotpaquet m WHERE m.refMassicotpaquet = :refMassicotpaquet")
    , @NamedQuery(name = "Massicotpaquet.findByNumTravail", query = "SELECT m FROM Massicotpaquet m WHERE m.numTravail = :numTravail")
    , @NamedQuery(name = "Massicotpaquet.findByLongueur", query = "SELECT m FROM Massicotpaquet m WHERE m.longueur = :longueur")
    , @NamedQuery(name = "Massicotpaquet.findByLargeur", query = "SELECT m FROM Massicotpaquet m WHERE m.largeur = :largeur")
    , @NamedQuery(name = "Massicotpaquet.findByEpaisseur", query = "SELECT m FROM Massicotpaquet m WHERE m.epaisseur = :epaisseur")
    , @NamedQuery(name = "Massicotpaquet.findByQualite", query = "SELECT m FROM Massicotpaquet m WHERE m.qualite = :qualite")})
public class Massicotpaquet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    @Id
    @Basic(optional = false)
    @Column(name = "REF_MASSICOTPAQUET")
    private String refMassicotpaquet;
    @Column(name = "NUM_TRAVAIL")
    private String numTravail;
    @Basic(optional = false)
    @Column(name = "LONGUEUR")
    private Double longueur;
    @Basic(optional = false)
    @Column(name = "LARGEUR")
    private Double largeur;
    @Basic(optional = false)
    @Column(name = "EPAISSEUR")
    private String epaisseur;
    @Basic(optional = false)
    @Column(name = "QUALITE")
    private String qualite;
    @Column(name = "LOCKED")
    private String locked = "FALSE";

    @JoinColumn(name = "REF_MASSICOT", referencedColumnName = "REF_MASSICOT")
    @ManyToOne(optional = false)
    private Massicot refMassicot;
    @JoinColumn(name = "REF_PAQUET", referencedColumnName = "REF_PAQUET")
    @ManyToOne(optional = false)
    private Paquet refPaquet;

    
    public String getLocked() {
        return locked;
    }

    public void setLocked(String locked) {
        this.locked = locked;
    }
    public Massicotpaquet() {
    }

    public Massicotpaquet(String refMassicotpaquet) {
        this.refMassicotpaquet = refMassicotpaquet;
    }

    public Massicotpaquet(String refMassicotpaquet, Double longueur, Double largeur, String epaisseur, String qualite) {
        this.refMassicotpaquet = refMassicotpaquet;
        this.longueur = longueur;
        this.largeur = largeur;
        this.epaisseur = epaisseur;
        this.qualite = qualite;
    }

    public String getRefMassicotpaquet() {
        return refMassicotpaquet;
    }

    public void setRefMassicotpaquet(String refMassicotpaquet) {
        this.refMassicotpaquet = refMassicotpaquet;
    }

    public String getNumTravail() {
        return numTravail;
    }

    public void setNumTravail(String numTravail) {
        this.numTravail = numTravail;
    }

    public Double getLongueur() {
        return longueur;
    }

    public void setLongueur(Double longueur) {
        this.longueur = longueur;
    }

    public Double getLargeur() {
        return largeur;
    }

    public void setLargeur(Double largeur) {
        this.largeur = largeur;
    }

    public String getEpaisseur() {
        return epaisseur;
    }

    public void setEpaisseur(String epaisseur) {
        this.epaisseur = epaisseur;
    }

    public String getQualite() {
        return qualite;
    }

    public void setQualite(String qualite) {
        this.qualite = qualite;
    }

    public Massicot getRefMassicot() {
        return refMassicot;
    }

    public void setRefMassicot(Massicot refMassicot) {
        this.refMassicot = refMassicot;
    }

    public Paquet getRefPaquet() {
        return refPaquet;
    }

    public void setRefPaquet(Paquet refPaquet) {
        this.refPaquet = refPaquet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refMassicotpaquet != null ? refMassicotpaquet.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Massicotpaquet)) {
            return false;
        }
        Massicotpaquet other = (Massicotpaquet) object;
        if ((this.refMassicotpaquet == null && other.refMassicotpaquet != null) || (this.refMassicotpaquet != null && !this.refMassicotpaquet.equals(other.refMassicotpaquet))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{" + "\"ref\": \"" + refMassicotpaquet + "\", "
                + "\"numTravail\": \"" + numTravail + "\", "
                + "\"longueur\": \"" + longueur + "\", "
                + "\"largeur\": \"" + largeur + "\", "
                + "\"epaisseur\": \"" + epaisseur + "\", "
                + "\"qualite\": \"" + qualite + "\", "
                + "\"locked\": \"" + locked + "\", "
                + "\"massicot\": " + refMassicot + ", "
                + "\"paquet\": " + refPaquet + "}";
    }



}
