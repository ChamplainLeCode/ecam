/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "CLASSIFICATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Classification.findAll", query = "SELECT c FROM Classification c")
    , @NamedQuery(name = "Classification.findByRefClassification", query = "SELECT c FROM Classification c WHERE c.refClassification = :refClassification")
    , @NamedQuery(name = "Classification.findByLibelle", query = "SELECT c FROM Classification c WHERE c.libelle = :libelle")})
public class Classification implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_CLASSIFICATION")
    private String refClassification;
    @Column(name = "LIBELLE")
    private String libelle;

    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    public Classification() {
    }

    public Classification(String refClassification) {
        this.refClassification = refClassification;
    }

    public String getRefClassification() {
        return refClassification;
    }

    public void setRefClassification(String refClassification) {
        this.refClassification = refClassification;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refClassification != null ? refClassification.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Classification)) {
            return false;
        }
        Classification other = (Classification) object;
        if ((this.refClassification == null && other.refClassification != null) || (this.refClassification != null && !this.refClassification.equals(other.refClassification))) {
            return false;
        }
        return true;
    }
    @Override
    public String toString() {
        return "{" + "\"ref\" : \"" + refClassification + "\", \"libelle\" : \"" + libelle + "\"}";
    }

}
