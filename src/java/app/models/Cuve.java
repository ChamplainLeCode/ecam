/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "CUVE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cuve.findAll", query = "SELECT c FROM Cuve c")
    , @NamedQuery(name = "Cuve.findByRefCuve", query = "SELECT c FROM Cuve c WHERE c.refCuve = :refCuve")
    , @NamedQuery(name = "Cuve.findByCuve", query = "SELECT c FROM Cuve c WHERE c.cuve = :cuve")
    , @NamedQuery(name = "Cuve.findByLibelle", query = "SELECT c FROM Cuve c WHERE c.libelle = :libelle")})
public class Cuve implements Serializable {

    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_CUVE")
    private String refCuve;
    @Column(name = "CUVE")
    private String cuve;
    @Column(name = "LIBELLE")
    private String libelle;
    @Column(name = "ETAT")
    private String etat = "VIDE";
    @OneToMany(mappedBy = "refCuve")
    private List<CuvePlot> cuvePlotList;

    public Cuve() {
    }

    public Cuve(String refCuve) {
        this.refCuve = refCuve;
    }

    public String getRefCuve() {
        return refCuve;
    }

    public void setRefCuve(String refCuve) {
        this.refCuve = refCuve;
    }

    public String getCuve() {
        return refCuve;
    }

    public void setCuve(String cuve) {
        this.cuve = cuve;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @XmlTransient
    public List<CuvePlot> getCuvePlotList() {
        return cuvePlotList;
    }

    public void setCuvePlotList(List<CuvePlot> cuvePlotList) {
        this.cuvePlotList = cuvePlotList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refCuve != null ? refCuve.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cuve)) {
            return false;
        }
        Cuve other = (Cuve) object;
        if ((this.refCuve == null && other.refCuve != null) || (this.refCuve != null && !this.refCuve.equals(other.refCuve))) {
            return false;
        }
        return true;
    }

    public void setVide(){
        this.etat = "VIDE";
    }
    public void setPlein(){
        this.etat = "PLEINE";
    }

    @Override
    public String toString() {
        return "{" + "\"refCuve\" : \"" + refCuve + "\", \"libelle\" : \"" + libelle + "\", \"cuve\" : \"" + cuve + "\", \"etat\": \""+etat+"\"}";
    }    
}
