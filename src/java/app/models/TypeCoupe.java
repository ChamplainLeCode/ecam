/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "TYPE_COUPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeCoupe.findAll", query = "SELECT t FROM TypeCoupe t")
    , @NamedQuery(name = "TypeCoupe.findByRefTypecoupe", query = "SELECT t FROM TypeCoupe t WHERE t.refTypecoupe = :refTypecoupe")
    , @NamedQuery(name = "TypeCoupe.findByLibelle", query = "SELECT t FROM TypeCoupe t WHERE t.libelle = :libelle")})
public class TypeCoupe implements Serializable {

    @OneToMany(mappedBy = "refTypecoupe")
    private List<Coupe> coupeList;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_TYPECOUPE")
    private String refTypecoupe;
    @Column(name = "LIBELLE")
    private String libelle;
    

    public TypeCoupe() {
    }

    public TypeCoupe(String refTypecoupe) {
        this.refTypecoupe = refTypecoupe;
    }

    public String getRefTypecoupe() {
        return refTypecoupe;
    }

    public void setRefTypecoupe(String refTypecoupe) {
        this.refTypecoupe = refTypecoupe;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refTypecoupe != null ? refTypecoupe.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeCoupe)) {
            return false;
        }
        TypeCoupe other = (TypeCoupe) object;
        if ((this.refTypecoupe == null && other.refTypecoupe != null) || (this.refTypecoupe != null && !this.refTypecoupe.equals(other.refTypecoupe))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{\"" + "ref\": \"" + refTypecoupe + "\", \"libelle\": \"" + libelle + "\"}";
    }

    @XmlTransient
    public List<Coupe> getCoupeList() {
        return coupeList;
    }

    public void setCoupeList(List<Coupe> coupeList) {
        this.coupeList = coupeList;
    }

}
