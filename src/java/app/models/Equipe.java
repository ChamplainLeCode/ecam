/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "EQUIPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Equipe.findAll", query = "SELECT e FROM Equipe e")
    , @NamedQuery(name = "Equipe.findByRefEquipe", query = "SELECT e FROM Equipe e WHERE e.refEquipe = :refEquipe")
    , @NamedQuery(name = "Equipe.findByDateEquipe", query = "SELECT e FROM Equipe e WHERE e.dateEquipe = :dateEquipe")
    , @NamedQuery(name = "Equipe.findByDateExecution", query = "SELECT e FROM Equipe e WHERE e.dateExecution = :dateExecution")})
public class Equipe implements Serializable {

    @Basic(optional = false)
    @Column(name = "CREATE_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt = new Date();

    @Column(name = "LIBELLE")
    private String libelle;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_EQUIPE")
    private String refEquipe;
    @Column(name = "DATE_EQUIPE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEquipe;
    @Column(name = "DATE_EXECUTION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateExecution;
    @OneToMany(mappedBy = "refEquipe")
    private List<Tranche> trancheList;
/*
    @OneToMany(mappedBy = "refEquipe")
    private List<Colis> colisList;
*/
    @OneToMany(mappedBy = "refEquipe")
    private List<Detailequipe> detailequipeList;

    public Equipe() {
    }

    public Equipe(String refEquipe) {
        this.refEquipe = refEquipe;
    }

    public String getRefEquipe() {
        return refEquipe;
    }

    public void setRefEquipe(String refEquipe) {
        this.refEquipe = refEquipe;
    }

    public Date getDateEquipe() {
        return dateEquipe;
    }

    public void setDateEquipe(Date dateEquipe) {
        this.dateEquipe = dateEquipe;
    }

    public Date getDateExecution() {
        return dateExecution;
    }

    public void setDateExecution(Date dateExecution) {
        this.dateExecution = dateExecution;
    }

    @XmlTransient
    public List<Tranche> getTrancheList() {
        return trancheList;
    }

    public void setTrancheList(List<Tranche> trancheList) {
        this.trancheList = trancheList;
    }
/*
    @XmlTransient
    public List<Colis> getColisList() {
        return colisList;
    }

    public void setColisList(List<Colis> colisList) {
        this.colisList = colisList;
    }
*/
    @XmlTransient
    public List<Detailequipe> getDetailequipeList() {
        return detailequipeList;
    }

    public void setDetailequipeList(List<Detailequipe> detailequipeList) {
        this.detailequipeList = detailequipeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refEquipe != null ? refEquipe.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Equipe)) {
            return false;
        }
        Equipe other = (Equipe) object;
        if ((this.refEquipe == null && other.refEquipe != null) || (this.refEquipe != null && !this.refEquipe.equals(other.refEquipe))) {
            return false;
        }
        return true;
    }

    
    public String fullString() {
        String s = "[";
        for(Detailequipe de: this.detailequipeList){
            s += de.toString()+", ";
        }
        char[] tab = s.toCharArray();
        tab[tab.length-1] =  ']';
        return "{\"" + "ref\": \"" + refEquipe + "\", \"dateEquipe\": \"" + dateEquipe.getTime() + "\", \"dateExecution\": \"" + dateExecution.getTime() + "\", \"libelle\": " + libelle + "\", \"liste\": \""+(tab.length == 1 ? "[]" : new String(tab))+"\"}";
    }

    @Override
    public String toString() {
        return "{\"" + "ref\": \"" + refEquipe + "\", \"dateEquipe\": \"" + dateEquipe.getTime() + "\", \"dateExecution\": \"" + dateExecution.getTime() + "\", \"libelle\": \"" + libelle + "\"}";
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

}
