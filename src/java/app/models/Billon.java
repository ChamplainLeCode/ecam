/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "BILLON")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Billon.findAll", query = "SELECT b FROM Billon b")
    , @NamedQuery(name = "Billon.findByRefBillon", query = "SELECT b FROM Billon b WHERE b.refBillon = :refBillon")
//    , @NamedQuery(name = "Billon.findByGdDiam", query = "SELECT b FROM Billon b WHERE b.gdDiam = :gdDiam")
    , @NamedQuery(name = "Billon.findByLongueur", query = "SELECT b FROM Billon b WHERE b.longueur = :longueur")
    , @NamedQuery(name = "Billon.findByNumTravail", query = "SELECT b FROM Billon b WHERE b.numTravail = :numTravail")
//    , @NamedQuery(name = "Billon.findByPtitDiam", query = "SELECT b FROM Billon b WHERE b.ptitDiam = :ptitDiam")
    , @NamedQuery(name = "Billon.findByVolume", query = "SELECT b FROM Billon b WHERE b.volume = :volume")})
public class Billon implements Serializable {

    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
/*    @Column(name = "GD_DIAM")
    private Double gdDiam;
    @Column(name = "PTIT_DIAM")
    private Double ptitDiam; */
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "GD_DIAM")
    private Double gdDiam;
    @Column(name = "LONGUEUR")
    private int longueur;
    @Column(name = "PTIT_DIAM")
    private Double ptitDiam;
    @Column(name = "VOLUME")
    private Double volume;
    @Column(name = "REBUT")
    private Character rebut;
    @OneToMany(mappedBy = "refBillon")
    private List<Plot> plotList;
    @JoinColumn(name = "REF_COUPE", referencedColumnName = "REF_COUPE")
    @ManyToOne
    private Coupe coupe;
    @Temporal(TemporalType.TIMESTAMP)
    private Date create_at = new Date();

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_BILLON")
    private String refBillon; 
    @Column(name = "NUM_TRAVAIL")
    private String numTravail;
    @OneToMany(mappedBy = "refBillon")
    private List<Coupe> coupeList;
    @JoinColumn(name = "REF_BILLONNAGE", referencedColumnName = "REF_BILLONNAGE")
    @ManyToOne
    private Billonnage refBillonnage;
    
    
   

    public Billon() {
    }

    public Billon(String refBillon) {
        this.refBillon = refBillon;
    }

    public String getRefBillon() {
        return refBillon;
    }

    public void setRefBillon(String refBillon) {
        this.refBillon = refBillon;
    }


    public String getNumTravail() {
        return numTravail;
    }

    public void setNumTravail(String numTravail) {
        this.numTravail = numTravail;
    }

    @XmlTransient
    public List<Coupe> getCoupeList() {
        return coupeList;
    }

    public void setCoupeList(List<Coupe> coupeList) {
        this.coupeList = coupeList;
    }

    public Billonnage getRefBillonnage() {
        return refBillonnage;
    }

    public void setRefBillonnage(Billonnage refBillonnage) {
        this.refBillonnage = refBillonnage;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refBillon != null ? refBillon.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Billon)) {
            return false;
        }
        Billon other = (Billon) object;
        if ((this.refBillon == null && other.refBillon != null) || (this.refBillon != null && !this.refBillon.equals(other.refBillon))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{"
                + "\"ref\" : \"" + refBillon + "\", "
                + "\"longueur\" : \"" + longueur + "\", "
                + "\"create_at\" : \"" + create_at + "\", "
/*                + "\"gdDiam\" : \"" + gdDiam + "\", "
*/                + "\"volume\" : \"" + volume + "\", "
                + "\"travail\" : \"" + numTravail + "\", "
                + "\"rebut\" : \"" + rebut + "\", "
                + "\"billonnage\" : " + refBillonnage 
                + "}";
    }


    public Coupe getCoupe() {
        return coupe;
    }
    public void setCoupe(Coupe coupe) {
        this.coupe = coupe;
    }



    @XmlTransient
    public List<Plot> getPlotList() {
        return plotList;
    }

    public void setPlotList(List<Plot> plotList) {
        this.plotList = plotList;
    }

    public Double getGdDiam() {
        return gdDiam;
    }

    public void setGdDiam(Double gdDiam) {
        this.gdDiam = gdDiam;
    }


    public Double getPtitDiam() {
        return ptitDiam;
    }

    public void setPtitDiam(Double ptitDiam) {
        this.ptitDiam = ptitDiam;
    }


    public Character getRebut() {
        return rebut;
    }

    public void unSetRebut(){
        rebut = 'F';
        //RebutController.unSetRebut(this.getClass(), this.refPlot);
    }
    
    public void setRebut(){
        rebut = 'T';
        //RebutController.unSetRebut(this.getClass(), this.refPlot);
    }

    public void setLongueur(int longueur) {
        this.longueur = longueur;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

}
