/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "AUTORIZATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Autorization.findAll", query = "SELECT a FROM Autorization a")
    , @NamedQuery(name = "Autorization.findByRoute", query = "SELECT a FROM Autorization a WHERE a.autorizationPK.route = :route")
    , @NamedQuery(name = "Autorization.findByPrivilege", query = "SELECT a FROM Autorization a WHERE a.autorizationPK.privilege = :privilege")})
public class Autorization implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AutorizationPK autorizationPK;

    public Autorization() {
    }

    public Autorization(AutorizationPK autorizationPK) {
        this.autorizationPK = autorizationPK;
    }

    public Autorization(String route, String privilege) {
        this.autorizationPK = new AutorizationPK(route, privilege);
    }

    public AutorizationPK getAutorizationPK() {
        return autorizationPK;
    }

    public void setAutorizationPK(AutorizationPK autorizationPK) {
        this.autorizationPK = autorizationPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (autorizationPK != null ? autorizationPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Autorization)) {
            return false;
        }
        Autorization other = (Autorization) object;
        if ((this.autorizationPK == null && other.autorizationPK != null) || (this.autorizationPK != null && !this.autorizationPK.equals(other.autorizationPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "app.models.Autorization[ autorizationPK=" + autorizationPK + " ]";
    }
    
}
