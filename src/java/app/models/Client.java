/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "CLIENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Client.findAll", query = "SELECT c FROM Client c")
    , @NamedQuery(name = "Client.findByRefClient", query = "SELECT c FROM Client c WHERE c.refClient = :refClient")
    , @NamedQuery(name = "Client.findByEmail", query = "SELECT c FROM Client c WHERE c.email = :email")
    , @NamedQuery(name = "Client.findByNationalite", query = "SELECT c FROM Client c WHERE c.nationalite = :nationalite")
    , @NamedQuery(name = "Client.findByNomClient", query = "SELECT c FROM Client c WHERE c.nomClient = :nomClient")
    , @NamedQuery(name = "Client.findByPays", query = "SELECT c FROM Client c WHERE c.pays = :pays")
    , @NamedQuery(name = "Client.findByTelephones", query = "SELECT c FROM Client c WHERE c.telephones = :telephones")
    , @NamedQuery(name = "Client.findByVille", query = "SELECT c FROM Client c WHERE c.ville = :ville")
    , @NamedQuery(name = "Client.findByContribuable", query = "SELECT c FROM Client c WHERE c.contribuable = :contribuable")})
public class Client implements Serializable {

    @Basic(optional = false)
    @Column(name = "CREATE_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt = new Date();

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_CLIENT")
    private String refClient;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "NATIONALITE")
    private String nationalite;
    @Column(name = "NOM_CLIENT")
    private String nomClient;
    @Column(name = "PAYS")
    private String pays;
    @Column(name = "TELEPHONES")
    private String telephones;
    @Column(name = "VILLE")
    private String ville;
    @Column(name = "CONTRIBUABLE")
    private String contribuable;
    @OneToMany(mappedBy = "refClient")
    private List<Embarquement> contennaireList;

    public Client() {
    }

    public Client(String refClient) {
        this.refClient = refClient;
    }

    public String getRefClient() {
        return refClient;
    }

    public void setRefClient(String refClient) {
        this.refClient = refClient;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNationalite() {
        return nationalite;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }

    public String getNomClient() {
        return nomClient;
    }

    public void setNomClient(String nomClient) {
        this.nomClient = nomClient;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getTelephones() {
        return telephones;
    }

    public void setTelephones(String telephones) {
        this.telephones = telephones;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getContribuable() {
        return contribuable;
    }

    public void setContribuable(String contribuable) {
        this.contribuable = contribuable;
    }

    @XmlTransient
    public List<Embarquement> getContennaireList() {
        return contennaireList;
    }

    public void setContennaireList(List<Embarquement> contennaireList) {
        this.contennaireList = contennaireList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refClient != null ? refClient.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Client)) {
            return false;
        }
        Client other = (Client) object;
        if ((this.refClient == null && other.refClient != null) || (this.refClient != null && !this.refClient.equals(other.refClient))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        try {
            JSONObject obj = new JSONObject();
            obj.put("email", email);
            obj.put("nationalite", nationalite);
            obj.put("nom", nomClient);
            obj.put("ref", refClient);
            obj.put("pays", pays);
            obj.put("contribuable", contribuable);
            obj.put("telephone", telephones);
            obj.put("ville", ville);
            return obj.toString();
        } catch (JSONException ex) {
            return "{}";
        }    
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

}
