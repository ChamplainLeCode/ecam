/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author champlain
 */
@Entity
@Table(name = "ARMATEUR", catalog = "", schema = "ECAM")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Armateur.findAll", query = "SELECT a FROM Armateur a")
    , @NamedQuery(name = "Armateur.findByRefArmateur", query = "SELECT a FROM Armateur a WHERE a.refArmateur = :refArmateur")
    , @NamedQuery(name = "Armateur.findByLibelle", query = "SELECT a FROM Armateur a WHERE a.libelle = :libelle")})
public class Armateur implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REF_ARMATEUR")
    private String refArmateur;
    @Basic(optional = false)
    @Column(name = "LIBELLE")
    private String libelle;

    public Armateur() {
    }

    public Armateur(String refArmateur) {
        this.refArmateur = refArmateur;
    }

    public Armateur(String refArmateur, String libelle) {
        this.refArmateur = refArmateur;
        this.libelle = libelle;
    }

    public String getRefArmateur() {
        return refArmateur;
    }

    public void setRefArmateur(String refArmateur) {
        this.refArmateur = refArmateur;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refArmateur != null ? refArmateur.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Armateur)) {
            return false;
        }
        Armateur other = (Armateur) object;
        if ((this.refArmateur == null && other.refArmateur != null) || (this.refArmateur != null && !this.refArmateur.equals(other.refArmateur))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{\"ref\": \"" + refArmateur + "\", \"libelle\": \""+libelle+"\"}";
    }
    
}
