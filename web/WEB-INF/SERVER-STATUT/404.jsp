<!DOCTYPE html>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta id="meta" charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Lockscreen</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="/resources/css/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="/resources/css/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="/resources/Ionicons/css/ionicons.min.css">
        
        <link rel="stylesheet" href="/resources/css/dataTables.bootstrap.min.css">      <!-- Theme style -->
        
        <link rel="stylesheet" href="/resources/css/AdminLTE.min.css">
        <link rel="stylesheet" href="/resources/css/skin-blue.min.css">
        <link rel="stylesheet" href="/resources/iCheck/all.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->

    </head>
<body class="hold-transition lockscreen">
        <p style="position: fixed; top: 10px; left: 10px;"><img src="/resources/image/IMG-20190401-WA0011.jpg"></p>
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
  <div class="lockscreen-logo">
    <a href="/"><b>ECAM-</b>Placages</a>
  </div>
  <!-- User name -->
  <div class="text-center">
    <a href="/login">Connectez-vous ici</a>
  </div>
  <div class="lockscreen-footer text-center" style="position: absolute; top: 90%; left: 45%;">
    Copyright � 2019<b><a href="https://www.bixterprise.com" class="text-black"> Bixterprise Coorp</a></b><br>
    All rights reserved
  </div>
</div>
<!-- /.center -->

<!-- jQuery 3 -->
    <script src="/resources/js/my/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="/resources/css/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/resources/js/my/adminlte.min.js"></script>
    <script src="/resources/js/my/demo.js"></script><script type="text/javascript">
    
    sessionStorage.setItem('lockscreen', $('#meta').attr("data"));
</script>
    
    
</body></html>