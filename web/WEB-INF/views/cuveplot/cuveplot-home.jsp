<%-- 
    Document   : cuveplot-home
    Created on : 6 févr. 2019, 12:08:15
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Ecam Web</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <%@include file="/WEB-INF/views/layouts/style.jsp" %>
        <link rel="stylesheet" href="/resources/css/select2/select2.min.css">
      <link rel="stylesheet" href="/resources/css/dataTables.bootstrap.min.css">    
    </head>
    <!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
    -->
    <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <%@include file="/WEB-INF/views/layouts/topBar.jsp" %>

      <!-- Left side column. contains the logo and sidebar -->
      <%@include file="/WEB-INF/views/layouts/sideBar.jsp" %>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Page Header
            <small>Optional description</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
          </ol>
          
          
        </section>

        <!-- Main content -->
        <section class="content container-fluid">

            <div class="row">


                <div class="modal fade modal-default" id="modal-default" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close btn-flat" style="background-color: transparent;" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true" style="color: white">×</span></button>
                                <h4 class="modal-title">Ajouter une nouveau cuveplot</h4>
                            </div>
                            <div class="modal-body" id="modal-body">

                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <div class="modal fade modal-primary" id="modal-edit" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close btn-flat" style="background-color: transparent;" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true" style="color: white">×</span></button>
                                <h4 class="modal-title">Modifier la cuveplot</h4>
                            </div>
                            <div class="modal-body" id="modal-body">

                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <p class="status-del-classification pull-left" style="padding: 5px; border-radius: 5px;"></p>

                <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-imprimer" class="pull-left btn btn-primary">
                    <i class="fa fa-print "></i> Imprimer
                </button>

                <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-addCuvePlot" class="pull-right btn btn-default" data-toggle="modal" data-target="#modal-default">
                    <i class="fa fa-plus "></i> Add cuveplot
                </button>

            </div>
            
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs pull-right">
                        <li class="active pull-left" ><H3><b><a href="#tab_1-1" data-toggle="tab" aria-expanded="false"><button type="button" style="background: transparent;" class="btn btn-flat" id="btn-lister">LISTE</button></a></b></H3></li>
                        <li class=""><H3><b><button type="button" href="#tab_2-2" aria-expanded="false" data-toggle="tab" class="btn btn-danger" id="btn-fin-cuisson"> FIN DE CUISSON</button></b></H3></li>
                      
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="tab_1-1">
                          
                            <div id="liste_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="liste" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="liste_info">
                                            <thead>
                                                <tr role="row">
                                                    <th class="sorting_asc" style="width: 79.75px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Reférence</th>
                                                    <th class="sorting" style="width: 73.3333px;" aria-label="Browser: activate to sort column ascending">DateDebut</th>
                                                     <th class="sorting" style="width: 73.3333px;" aria-label="Browser: activate to sort column ascending">DateFin</th>
                                                      <th class="sorting" style="width: 73.3333px;" aria-label="Browser: activate to sort column ascending">Cuve</th>
                                                       <th class="sorting" style="width: 73.3333px;" aria-label="Browser: activate to sort column ascending">Plot</th>
                                                       <th class="sorting" style="width: 73.3333px;" aria-label="Browser: activate to sort column ascending">Certif</th>
                                                    <th class="sorting" style="width: 86.5px;" aria-label="Platform(s): activate to sort column ascending">Opérations</th>
                                                </tr>
                                            </thead>
                                            <tbody id="table-body"></tbody>
                                      </table>
                                    </div>
                                </div>
                            </div>
            
                      </div>
                      <!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_2-2">
                            <!-- form start -->
                            <form class="form-horizontal">
                                <div class="form-group">
                                  <label for="zone-cuve" class="col-sm-2 control-label">Cuve</label>

                                  <div class="col-sm-10">
                                      <select id="zone-cuve" required="" name="cuve" class="select2" style="width: 100%;"></select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="zone-cuve" class="col-sm-2 control-label">N° Travail</label>

                                  <div class="col-sm-10">
                                      <label style="font-style: italic; color: red;">"all" pour vider</label>
                                      <input id="zone-travail" required="" name="travail" class="form-control form-control-static" placeholder="N° de travail à vider">
                                  </div>
                                </div>
                              <div class="box-footer">
                                  <button type="button" onclick="sendFinCuisson()" id="terminerFinCuisson" class="btn btn-info pull-right">Terminer</button>
                              </div>
                            </form>
                      <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                  </div>
                </div>
                

            
        </section>
        
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <%@include file="/WEB-INF/views/layouts/footer.jsp" %>

    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->
    <%@include file="/WEB-INF/views/layouts/script.jsp" %>
    <script src="/resources/js/cuveplot/cuveplot-home.js"></script>
    <script src="/resources/js/jquery.dataTables.min.js"></script>
    <script src="/resources/js/dataTables.bootstrap.min.js"></script>
        <script src="/resources/js/select2/select2.full.min.js"></script>
    <script>
        $(function(){
            $('#btn-lister').click(function(){
                loadData();
            }).click();
            
            $('#btn-fin-cuisson').click(function(){
                setCuve();
            });
            
            $('#zone-cuve').change(function(){
//               getPlotsInCuve($(this).val()); 
            });
            
            $('select').select2();
            
            $('#terminerFinCuisson').click(function(){
                $.ajax({
                    url: '/cuve_plot/fin_cuisson', 
                    method: 'POST',
                    dataType: 'json',
                    data: {cuve: $('#zone-cuve').val(), travail: ($('#zone-travail').val() == 'all' || $('#zone-travail').val() == '' ? 'all' : $('#zone-travail').val())},
                    complete: function (jqXHR, textStatus ) {
                        //setCuve();
                        location.pathname = "/cuveplot";
                        document.getElementById("zone-plot").innerHTML = '';
                    }
                });
            });
            
            
            document.getElementById("btn-imprimer").onclick = function(){
                tableToExcel('liste', 'W3C Example Table');
            };
//            setCuve();

        });
    </script>
    </body>
</html>