<%-- 
    Document   : cuveplot-edit
    Created on : 6 févr. 2019, 12:08:04
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-cuveplot">
    <p class="status-edit-cuveplot" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Reférence</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<% out.print(request.getParameter("ref"));%>" required="" class="form-control" id="zone-ref" placeholder="Rérérence">
          </div>
        </div>
        <div class="form-group">
          <label for="zone- dateDebut" class="col-sm-2 control-label">DateDebut</label>

          <div class="col-sm-10">
              <input type="date" required="" value="<% out.print(request.getParameter("dateDebut"));%>" class="form-control" id="zone-dateDebut" placeholder="Date de Debut du cuveplot">
          </div>
        </div>
          <div class="form-group">
          <label for="zone- dateFin" class="col-sm-2 control-label">DateFin</label>

          <div class="col-sm-10">
              <input type="date" required="" value="<% out.print(request.getParameter("dateFin"));%>" class="form-control" id="zone-dateFin" placeholder="Date de Fin du cuveplot">
          </div>
        </div>
          <div class="form-group">
          <label for="zone- epaisseur" class="col-sm-2 control-label">CuveRef</label>

          <div class="col-sm-10">
              <input type="text" required="" value="<% out.print(request.getParameter("cuveRef"));%>" class="form-control" id="zone-cuveRef" placeholder="CuveRef du cuveplot">
          </div>
        </div>
          <div class="form-group">
          <label for="zone- numTravail" class="col-sm-2 control-label">PlotRef</label>

          <div class="col-sm-10">
              <input type="text" required="" value="<% out.print(request.getParameter("plotRef"));%>" class="form-control" id="zone-plotRef" placeholder="numTravail de la cuveplot">
          </div>
        </div>
          
          
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-cuveplot"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
    <script type="text/javascript">
        $("#btn-edit-cuveplot").click(function(){
            editData($('#zone-ref').val(), $('#zone-dateDebut').val(), $('#zone-dateFin').val(), $('#zone-cuveRef').val(),
            $('#zone-plotRef').val());
            $('#form-edit-cuveplot .overlay').removeClass('hide');
            $('#form-edit-cuveplot #btn-edit-cuveplot').addClass("disabled");
        });

    </script>
</div>

