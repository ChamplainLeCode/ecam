<%-- 
    Document   : cuveplot-add
    Created on : 6 févr. 2019, 12:07:54
    Author     : KOUNOU BESSALA ERIC
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-add-cuveplot">
    <p class="status-add-cuveplot" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
        <div class="box-body">
            <div class="form-group">
              <label for="zone-ref" class="col-sm-2 control-label">Reférence</label>

              <div class="col-sm-10">
                  <input type="text" required="" class="form-control" id="zone-ref" placeholder="Rérérence">
              </div>
            </div>
            <div class="form-group">
              <label for="zone- dateDebut" class="col-sm-2 control-label">DateDebut</label>

              <div class="col-sm-10">
                  <input type="date" required="" class="form-control" id="zone-dateDebut" placeholder="date de Debut du CuvePlot">
              </div>
            </div>
            <!--div class="form-group">
              <label for="zone- dateFin" class="col-sm-2 control-label">DateFin</label>

              <div class="col-sm-10">
                  <input type="date" required="" class="form-control" id="zone-dateFin" placeholder="Date de fin du CuvePlot">
              </div>
            </div-->
              <div class="form-group">
              <label for="zone- epaisseur" class="col-sm-2 control-label">Cuve </label>

              <div class="col-sm-10">
                  <select style="width: 100%" required="" class="select2" id="zone-cuve"></select>
              </div>
            </div>
            <div id="zone-type"><legend>Plots</legend>
                
            </div>
            <button type="button" id="btn-plus" class="btn btn-info" style="width: 50px; height: 50px; border-radius: 25px;"><i class="fa fa-plus"></i></button>
        </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-save-cuveplot">Enregistrer</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>

        <script src="/resources/js/select2/select2.full.min.js"></script>
        <script type="text/javascript">
        $("#zone-ref").val('REF_CLASS_'+(new Date()).getTime());
        
        var index = 0;
        $('#btn-plus').click(function(){
            
            var select = document.getElementById("zone-type");
            var div = document.createElement("div");
                div.className = "row";
                div.style.marginTop = "10px";
            var label = document.createElement("label");
                label.className = "col-sm-2 control-label";
                label.innerHTML = "Par: "
            var input = document.createElement("input");
                input.id = "zone-in-travail"+index;
                input.type = "text";
                input.placeholder = "N° Travail";
                input.className = "col-sm-4";
                input.style.marginLeft = "5px";
                input.style.height = "40px";
                
            var selectVal = document.createElement("select");
                selectVal.className = "col-sm-4";
                selectVal.multiple = "multiple";
                selectVal.style.height = "80px";
                selectVal.style.marginLeft = "5px";
                $(selectVal).hide();
                selectVal.id = "zone-plot"+index;


            var selectType = document.createElement("select");
                var opt = document.createElement("option");
                selectType.className = "col-sm-4";
                selectType.id = "zone-type"+index;
                
                opt.innerText = "N° Travail";
                opt.value = "travail";
                selectType.appendChild(opt);
                opt = document.createElement("option");
                opt.innerText = "N° Billon";
                opt.value = "billon";
                selectType.appendChild(opt);
                opt = document.createElement("option");
                opt.innerText = "N° Plot";
                opt.value = "plot";
                selectType.appendChild(opt);
                selectType.onchange = function(){
                    if(this.value === 'travail'){
                        $(input).show(); //style = "visible";
                        $(selectVal).hide(); //.style.visibility = "hidden";
                    }else if(this.value === 'billon'){
                        $(input).hide();//.style.visibility = "hidden";
                        setPlots(selectVal, 'billon');
                        $(selectVal).show(); //.style.visibility = "visible";
                    }else{
                        $(input).hide();//.style.visibility = "hidden";
                        setPlots(selectVal, 'plot');
                        $(selectVal).show(); //.style.visibility = "visible";
                    }
                };
                selectType.style.height = "40px";
                
            var button = document.createElement("button");
                button.type = "button";
                button.innerHTML = "<i class=\"fa fa-trash\"></i>";
                button.className = "btn btn-danger";
                button.style.width = "30px;";
                button.style.height = "40px";
                button.style.marginLeft = "5px";
                button.onclick = function(){
                    select.removeChild(div);
                    index--;
                };
                
                div.appendChild(label);
                div.appendChild(selectType);
                div.appendChild(selectVal);
                div.appendChild(input);
                div.appendChild(button);
                select.appendChild(div);
            index++;
        });
        
        function setCuve(){
            $.ajax({
                url: '/api/cuve/vide/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                        var data = jqXHR.responseJSON;
                        
                    var select = document.getElementById("zone-cuve");
                    var opt = document.createElement("option");
                    opt.innerHTML = "-- Selectionnez une cuve --";
                    opt.value = "";
                    select.appendChild(opt);
                    for(var i = 0; i<data.length; i++){
                        opt = document.createElement("option");
                        opt.innerHTML = data[i].cuve;
                        opt.value = data[i].refCuve;
                        select.appendChild(opt);
                    }
                }
            });
        }
        
        
        function setPlots(selectVal = null, nature = null){
            $.ajax({
                url: '/api/plot/list/for_cuve',
                method: 'GET',
                data: {nature: nature},
                complete: function (jqXHR, textStatus ) {
                        var data = jqXHR.responseJSON;
                    var select;
                    if(selectVal === null)
                        select = document.getElementById("zone-plot");
                    else
                        select = selectVal;
                    
                    select.innerHTML = '';
                    var opt;

                    for(var i = 0; i<data.length; i++){
                        opt = document.createElement("option");
                        opt.innerHTML = data[i];
                        opt.value = data[i];
                        select.appendChild(opt);
                    }
                }
            });
        }
        
        setCuve();
        setPlots();
        
        $('select').select2();
        $("#btn-save-cuveplot").click(function(){
            var data = [];
            for(var i=0; i<index; i++){
                data[i] = {
                    type: $('#zone-type'+i).val(),
                    value: ($('#zone-type'+i).val() === 'travail' ? ((new Date().getFullYear())+"-")+$('#zone-in-travail'+i).val() : $('#zone-plot'+i).val())
                };
            }
            saveData( $('#zone-ref').val(),$('#zone-dateDebut').val(),$('#zone-cuve').val(), data);
            $('#form-add-cuveplot .overlay').removeClass('hide');
            $('#form-add-cuveplot #btn-save-cuveplot').addClass("disabled");
        });

    </script>
</div>
