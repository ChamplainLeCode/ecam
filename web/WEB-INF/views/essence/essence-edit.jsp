<%-- 
    Document   : essence-add
    Created on : 17 déc. 2018, 15:06:30
    Author     : champlain
--%>
<%@page import="app.models.Essence"%>
<%
    Essence essence = (Essence)request.getAttribute("essence");
    %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
            <div class="box box-success col-sm-6">
            <div class="box-header with-border">
              <h3 class="box-title">Formulaire</h3>
              <div class="pull-right" id="status-modification"></div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group group-ref">
                  <label for="reference" class="col-sm-2 control-label has-error">Reférence</label>

                  <div class="col-sm-10">
                      <input type="text" required="" disabled="" value="${essence.getRefEssence()}" class="form-control" id="reference" name="reference" placeholder="reference de l'essence">
                      <span class="help-block col-sm-10"></span>
                  </div>
                </div>
                <div class="form-group group-libelle">
                  <label for="libelle" class="col-sm-2 control-label has-error">Libelle</label>

                  <div class="col-sm-10">
                      <input type="text" required="" value="${essence.getLibelle()}" class="form-control" id="libelle" name="libelle" placeholder="libelle de l'essence">
                      <span class="help-block col-sm-10"></span>
                  </div>
                </div>
                <div class="form-group group-densite">
                  <label for="densite" class="col-sm-2 control-label">Densité</label>

                  <div class="col-sm-10">
                      <input type="number" min="0"  required="" value="${essence.getDensite()}" class="form-control" id="densite" name="densité" placeholder="densité de l'essence">
                      <span class="help-block col-sm-10"></span>
                  </div>
                </div>
              </div>
                
              <div class="box-footer">
                  <button type="button"  id="btn-update" class="btn btn-success pull-right" data-dismiss="#zoneEditEssence"><i class="glyphicon glyphicon-edit"></i> Modifer</button>
              </div>
            </form>
          </div>
            <div id="error-zone"></div>
            <script src="/resources/js/essence/edit.js"></script>