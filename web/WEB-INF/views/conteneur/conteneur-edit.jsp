<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-conteneur">
    <p class="status-edit-conteneur" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Code</label>

          <div class="col-sm-10">
              <input type="text" required="" value="<%=request.getParameter("ref")%>" class="form-control" id="zone-ref" placeholder="Rérérence">
          </div>
        </div>
        <div class="form-group">
          <label for="zone- libelle" class="col-sm-2 control-label">Libelle</label>

          <div class="col-sm-10">
              <input type="text" required="" value="<%=request.getParameter("libelle")%>" class="form-control" id="zone-libelle" placeholder="libelle de la Conteneur">
          </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Type</label>
            <div class="col-sm-10">
                <select class="select2" required="" name="type" style="width: 100%;" id="zone-type"></select>
            </div>
        </div>
        <div class="form-group">
          <label for="zone- libelle" class="col-sm-2 control-label">Compagnie</label>

          <div class="col-sm-10">
              <select required="" class="select2" id="zone-compagnie-data" style="width: 100%;"></select>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
          <button type="button"  data-dismiss="modal" id="btn-close" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-conteneur"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.min.js"></script>
    <script type="text/javascript">
        $("#btn-edit-conteneur").click(function(){
            editData($('#zone-ref').val(), $('#zone-libelle').val(), $('#zone-compagnie-data').val(), $('#zone-type').val());
            $('#form-edit-conteneur .overlay').removeClass('hide');
            $('#btn-close').click();
            $('#form-edit-conteneur #btn-edit-conteneur').addClass("disabled");
        });
        $('select').select2();
        
        (function(){
            $.ajax({
                url: '/api/compagnie/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    
                    var select = document.getElementById('zone-compagnie-data');
                    var data = jqXHR.responseJSON;
                    var option = document.createElement("option");
                    option.innerHTML = '-- Selectionnez --';
                    option.value = '';
                    select.appendChild(option);
                    
                    for(var i=0; i<data.length; i++){
                        option = document.createElement("option");
                        option.value = data[i].ref;
                        option.innerText = data[i].libelle;
                        if(data[i].ref === '<%=request.getParameter("compagnie[ref]")%>')
                            option.selected = true;
                        select.appendChild(option);
                    }
                }
            });
        })();

        (function getTypeContenaire(){
            $.ajax({
                url: '/api/type_contenaire/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    
                    var select = document.getElementById('zone-type');
                    var data = jqXHR.responseJSON;
                    var option = document.createElement("option");
                    option.innerHTML = '-- Selectionnez --';
                    option.value = '';
                    select.appendChild(option);
                    
                    for(var i=0; i<data.length; i++){
                        option = document.createElement("option");
                        option.value = data[i].ref;
                        option.innerText = data[i].libelle;
                        select.appendChild(option);
                        if(data[i].ref === '<%=request.getParameter("type[ref]")%>')
                            option.selected = true;
                    }
                }
            });
        })();

    </script>
</div>
