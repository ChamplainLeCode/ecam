<!-- 
    Document   : totoPage
    Created on : 8 déc. 2018, 14:42:16
    Author     : champlain
-->

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Ecam Web</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <%@include file="/WEB-INF/views/layouts/style.jsp" %>
      <link rel="stylesheet" href="/resources/css/dataTables.bootstrap.min.css">    
      <link rel="stylesheet" href="/resources/css/select2/select2.min.css">
    </head>
    <!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
    -->
    <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <%@include file="/WEB-INF/views/layouts/topBar.jsp" %>

      <!-- Left side column. contains the logo and sidebar -->
      <%@include file="/WEB-INF/views/layouts/sideBar.jsp" %>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Billonnages
            <small>Liste des billonnages</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="">ecam</li>
            <li class="active">billonnage</li>
          </ol>
          
          
        </section>

        <!-- Main content -->
        <section class="content container-fluid">

            <div class="row">
                
                <div class="modal fade modal-default" id="modal-fiche" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close btn-flat" style="background-color: transparent;" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true" style="color: white">×</span></button>
                                <h4 class="modal-title">Choisir les billes du Massicotage</h4>
                            </div>
                            <div class="modal-body" id="modal-body">
                                <fieldset style="border: #595959 2px solid; padding: 10pt;">
                                    <legend style="text-decoration: none;">Selection les billes</legend>
                                    <label>Travail</label>
                                    <select class="select2" id="zone-bille-fiche" style="width: 100%;" multiple=""></select>
                                    <label id="label-fiche-error" style="color: red;"></label><br/>
                                    <button type="button" id="btn-imprimer-fiche" class="btn btn-primary" style="margin-top: 20pt"><i class="glyphicon glyphicon-print"></i> Imprimer</button>
                                </fieldset>
                                
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-yahoo"  data-dismiss="modal">Fermer</button>
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>


                
                <div class="modal fade"  id="modal-default" style="display: none;">
                    <div class="modal-dialog" style="width: 80%;">
                        <div class="modal-content col-sm-12">
                            <div class="modal-header">
                                <button type="button" class="close btn-flat" style="background-color: transparent;" id="btn-close-modal" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true" style="color: white">×</span></button>
                                <h4 class="modal-title">Ajouter une nouveau Billonnage</h4>
                            </div>
                            <div class="modal-body" id="modal-body" style="width: 100%;">

                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <div class="modal fade modal-default" id="modal-edit" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close btn-flat" style="background-color: transparent;" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true" style="color: white">×</span></button>
                                <h4 class="modal-title">Modifier billonnage</h4>
                            </div>
                            <div class="modal-body" id="modal-body">

                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <p class="status-del-billonnage pull-left" style="padding: 5px; border-radius: 5px;"></p>

                <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-addBillonnage" class="pull-right btn btn-default" data-toggle="modal" data-target="#modal-default">
                    <i class="fa fa-plus "></i> Add Billonnage
                </button>
                
                <button style="margin-right: 40px; margin-bottom: 20px; background-color: transparent;" type="button" onclick="history.back()" class="pull-left btn btn-flat" data-toggle="modal" data-target="#modal-default">
                    <i class="fa fa-arrow-left "></i> Retour
                </button>
                <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-imprimer" class="pull-left btn btn-primary">
                    <i class="fa fa-print "></i> Imprimer la liste
                </button>

                <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-open-fiche" class="pull-left btn btn-yahoo" data-toggle="modal" data-target="#modal-fiche">
                    <i class="fa fa-file-text "></i> Imprimer Fiche de Production
                </button>           
            </div>
            <div class="box">
            <div class="box-header">
              <h3 class="box-title">Liste des billonnages</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div id="liste_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="liste" class="table table-responsive table-bordered table-striped dataTable" role="grid" aria-describedby="liste_info">
                                <thead>
                                    <tr role="row">
                                        <!--th class="sorting_asc" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 79.75px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Ref</th-->
                                        <th class="sorting">N° Bille</th>
                                        <th class="sorting">Certification</th>
                                        <th class="sorting">Essence</th>
                                        <th class="sorting">Nbr Billons</th>
                                        <th class="sorting">Equipe</th>
                                        <th class="sorting">N° Travail</th>
                                        <!--th class="sorting">Origine</th-->
                                        <th class="sorting">Opérations</th>
                                    </tr>
                                </thead>
                                <tbody id="table-body"></tbody>
                          </table>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <%@include file="/WEB-INF/views/layouts/footer.jsp" %>

    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->
    <%@include  file="/WEB-INF/views/layouts/script.jsp" %>
    <script src="/resources/js/billonnage/billonnage-home.js"></script>
    <script src="/resources/js/jquery.dataTables.min.js"></script>
    <script src="/resources/js/select2/select2.min.js"></script>
    <script src="/resources/js/dataTables.bootstrap.min.js"></script>

    <script>
        $(function () {
            window.$ = $;
            
    
            $('select').select2();
            loadData();
            
            document.getElementById("btn-imprimer").onclick = function(){
                tableToExcel('liste', 'W3C Example Table');
            };
        });
    </script>
    </body>
</html>
