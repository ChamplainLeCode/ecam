<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="/resources/css/select2/select2.min.css">


<table id="equipe-liste">
    
    <tbody></tbody>
</table>

<div id="form-add-billonnage">
    <p class="status-add-billonnage text-red" style="padding: 5px; border-radius: 5px; font-weight: bold;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Reférence</label>
          <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="zone-ref" placeholder="Reférence du Billonnage">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-bille" class="col-sm-2 control-label">N° Bille</label>

          <div class="col-sm-10">
              <select class="form-control select2" style="width:100%;" id="zone-bille"></select>
          </div>
        </div>
        <div class="form-group">
          <label for="zone-travail" class="col-sm-2 control-label">N° Travail</label>
          <div class="col-sm-10">
              <input type="text" class="form-control" id="zone-travail"/>
          </div>
        </div>
        <div class="form-group">
          <label for="zone-billon" class="col-sm-2 control-label">Nombre de billon</label>
          <div class="col-sm-10">
              <input type="number" min="1" required="" class="form-control" id="zone-billon" max="10">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-equipe" class="col-sm-2 control-label">Equipe</label>
          <div class="col-sm-10">
              <select class="select2" id="zone-equipe" required="" style="width: 100%;"></select>

                <!--input class="form-control col-sm-8" id="zone-equipe" readonly="">
                <div class="btn-group">
                    <button id="btn-select-equipe" type="button" class="btn btn-flat left-side" data-toggle="modal" data-target="#modal-equipe" style="background-color: transparent;">Sélectionnez</button>
                    <a href="equipe" target="#equipe-liste"><button id="btn-create-equipe" class="btn btn-primary center "><i class="fa fa-plus"></i> Nouvelle équipe <i class="fa fa-group"></i></button></a>
                    <button onclick="$('#btn-select-equipe').click();" class="btn btn-success right-side"><i class="fa fa-refresh"></i> Actualiser</button>
                </div-->
            </div>
        </div>
        <div class="form-group">
          <label for="zone-parc" class="col-sm-2 control-label">Parc</label>
          <div class="col-sm-10">
              <input class="form-control" id="zone-parc" readonly="">
          </div>
        </div>
          <div class="container-fluid">
              <table class="table table-responsive table-striped">
                  <thead>
                      <tr><th colspan="4"><b>Details sur les billons</th></tr>
                      <tr>
                          <th>N° Billon</th>
                          <!--th>Diamètre 1</th>
                          <th>Diamètre 2</th-->
                          <th>Longueur (Cm)</th>
                      </tr>
                  </thead>
                  <tbody id="table-details-billon"></tbody>
              </table>
          </div>
      </div>        
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" class="btn btn-default">Fermer</button>
            <label id="label-check-travail" class="label text-red pull-right" style="font-size: 10pt;">En attente de validation du Numéro de Travail</label>
            <button type="button" class="btn btn-info pull-right " id="btn-save-billonnage">Enregistrer</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>

    <script src="/resources/js/jquery.dataTables.min.js"></script>
    
    <script src="/resources/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
        $('#zone-ref').val('REF_BLG_'+(new Date()).getTime()); 
        $('#zone-bille').change(function(){$('#zone-billon').change();});
        $('#zone-billon').change(function(){
           var val = $(this).val();
           var tableau = document.getElementById('table-details-billon');
           tableau.innerHTML = '';
           
            for(var i=0; i<val; i++){
                var ligne = document.createElement("tr");
//                var celDiam1 = document.createElement("td");
//                var celDiam2 = document.createElement("td");
                var celVol = document.createElement("td");
                var celRef = document.createElement("td");
                var input = document.createElement("input");
                input.setAttribute("readonly","");
                input.value = $('#zone-travail').val()+'-'+(i+1); //$('#zone-bille').val()+'-'+(i+1);
                input.id = 'zone-billon-ref-'+i
                celRef.appendChild(input);
/*
                input = document.createElement("input");
                input.type = "number";
                input.min = 0;
                input.value = 0;
                input.id = 'zone-billon-diam1-'+i
                celDiam1.appendChild(input);
                
                
                input = document.createElement("input");
                input.type = "number";
                input.min = 0;
                input.value = 0;
                input.id = 'zone-billon-diam2-'+i
                celDiam2.appendChild(input);
                
  */              
                input = document.createElement("input");
                input.type = "number";
                input.min = 0;
                input.value = 0;
                input.id = 'zone-billon-vol-'+i
                celVol.appendChild(input);
                
                ligne.appendChild(celRef);
    //            ligne.appendChild(celDiam1);
    //            ligne.appendChild(celDiam2);
                ligne.appendChild(celVol);
                
                tableau.appendChild(ligne);
           }
        });
        

       
        $.ajax({
            url: '/api/reception/list/numero_bille/no_numTravail',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var data = JSON.parse(jqXHR.responseText);
                var select = document.getElementById('zone-bille');
                var selected=-1;
                for(var i=0; i<data.length; i++){
                    var opt = document.createElement("option");
                    // Numéro de la bille
                    opt.value = data[i][0];
                    opt.innerHTML = data[i][0];
                    
                    // Numéro de travail
                    opt.setAttribute("data", data[i][1]);
                    select.appendChild(opt);
                }
                document.getElementById('zone-bille').selectedIndex = selected;
                $('select').select2();
            }
        });
        
        $('#zone-bille').change(function (){
            $.ajax({
                url: "/api/parc/by_bille",
                method: "GET",
                data: {bille: $(this).val()},
                dataType: 'json',
                complete: function(result, textS){
                    var data = result.responseJSON;
                    $("#zone-parc").val(data.refTypeparc.libelle)
                                   .attr('data', data.ref);
                }
            });
            // Quand on change la bille selectionnée
           // $('#zone-travail').val(document.getElementById('zone-bille').options[document.getElementById('zone-bille').selectedIndex].getAttribute("data"));
        });
        
        function controlData(){
            var errorLabel = $('.status-add-billonnage');
            
            if($('#zone-ref').val() === ''){errorLabel.html('Veuillez entrer le numéro de référence du Billonnnage'); return false;}
            if($('#zone-bille').val() === ''){errorLabel.html('Veuillez Selectionner un numéro de Bille'); return false;}
            if($('#zone-travail').val() === ''){errorLabel.html('Veuillez entrer le numéro de travail'); return false;}
            if($('#zone-billon').val() === ''){errorLabel.html('Veuillez entrer le nombre de billon'); return false;}
            if($('#zone-parc').val() === ''){errorLabel.html('Attention impossible de trouver le parc. Contactez l\'administrateur'); return false;}
            errorLabel.html('');
            return true;
        }
        
        $("#btn-save-billonnage").click(function(){
            
            var data = [];
            for(var i=0; i<parseInt($('#zone-billon').val()); i++){
                data[i] = {
      //              diam1: $('#zone-billon-diam1-'+i).val(),
      //              diam2: $('#zone-billon-diam2-'+i).val(),
                    longueur: $('#zone-billon-vol-'+i).val(),
                    ref: $('#zone-billon-ref-'+i).val()
                };
            }
            if(controlData() === false || data.length === 0){
                return;
            }

            saveData($('#zone-ref').val(), $('#zone-bille').val(), $('#zone-billon').val(), $("#zone-equipe").val(), $('#zone-travail').val(), $("#zone-parc").attr('data'), data);
            $('#form-add-billonnage .overlay').removeClass('hide');
            $('#form-add-billonnage #btn-save-billonnage').addClass("disabled");
        });
        
        $('#zone-travail').focusout(function(e){
            /**
             * @return {Boolean} true if le num de travail existe et false sinon
             */
            $.ajax({
                url: '/api/check/travail',
                method: 'GET',
                data: {travail: $('#zone-travail').val()},
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    try{
                        if(jqXHR.responseJSON.resultat === false){
                            $('#btn-save-billonnage').removeClass('hide');
                            $('#label-check-travail').html('');
                        }else{
                            $('#btn-save-billonnage').addClass('hide');
                            $('#label-check-travail').html('Ce numéro de travail existe déjà');
                            $('#zone-travail').val('');
                        }
                    }catch(e){
                        $('#btn-save-billonnage').addClass('hide');
                        $('#zone-travail').val('');
                        $('#label-check-travail').html('Ce numéro de travail existe déjà');
                    }
                }
            });
        });
        
        $.ajax({
            url: '/api/equipe/list',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var select = document.getElementById("zone-equipe");
                var data = jqXHR.responseJSON;
                if(data.length <= 0){
                    var option = document.createElement("optiongroupe");
                    option.innerText = "Aucune equipe";
                    select.appendChild(option);
                    return;
                }
                select.innerHTML = '';
                var option = document.createElement("option");
                option.innerHTML = "-- Selectionnez l'équipe --";
                option.value = "";
                select.appendChild(option);
                for(var i = 0; i< data.length; i++){
                    option = document.createElement("option");
                    option.value = data[i].ref;
                    option.innerText = data[i].libelle;
                    select.appendChild(option);
                }
            }
        });
        
    </script>
</div>
