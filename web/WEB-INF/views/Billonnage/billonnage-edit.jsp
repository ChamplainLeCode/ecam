<%@page import="org.json.JSONObject"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="/resources/css/select2/select2.min.css">


<table id="equipe-liste">
    <tbody></tbody>
</table>

<div id="form-edit-billonnage">
    <p class="status-add-billonnage" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Reférence</label>
          <div class="col-sm-10">
              <input type="text" required="" value="<%=request.getParameter("ref")%>" class="form-control" id="zone-ref" placeholder="Reférence du Billonnage">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-bille" class="col-sm-2 control-label">N° Bille</label>

          <div class="col-sm-10">
              <select class="form-control select2" style="width:100%;" id="zone-bille"></select>
          </div>
        </div>
        <div class="form-group">
          <label for="zone-billon" class="col-sm-2 control-label">Nombre de billon</label>
          <div class="col-sm-10">
              <input type="number" min="0" value="<%=request.getParameter("nbrBillon")%>" required="" class="form-control" id="zone-billon">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-equipe" class="col-sm-2 control-label">Equipe</label>
          <div class="col-sm-10">
              <input class="form-control col-sm-8" value="<%=request.getParameter("equipe")%>" id="zone-equipe" readonly="">
              <button id="btn-select-equipe" type="button" class="btn btn-flat" data-toggle="modal" data-target="#modal-equipe" style="background-color: transparent;">Sélectionnez</button>
          </div>
        </div>
        <div class="form-group">
          <label for="zone-travail" class="col-sm-2 control-label">N° Travail</label>
          <div class="col-sm-10">
              <input type="text" class="form-control" readonly="" value="<%=request.getParameter("numTravail")%>" id="zone-travail">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-parc" class="col-sm-2 control-label">Parc</label>
          <div class="col-sm-10">
              <select class="form-control select2" style="width:100%;" id="zone-parc"></select>
          </div>
        </div>
      </div>        
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-billonnage">Enregistrer</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>

    <script src="/resources/js/jquery.dataTables.min.js"></script>
    
    <script src="/resources/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
        $('#btn-select-equipe').click(function(){ 
            $.ajax({
                url: '/api/equipe/list',
                method: 'GET',
                beforeSend: function (xhr) {
                    $('#equipe-liste tbody').html('<div class="overlay">'+
                        '<i class="fa fa-refresh fa-spin"></i>'+
                      '</div>')
                },
                complete: function (jqXHR, textStatus ) {
                    var data = JSON.parse(jqXHR.responseText);
                    var equipe = $('#equipe-liste tbody').html(''); 
                    
                    for(var i=0; i<data.length; i++){
                        var e = data[i];
                        equipe.html(equipe.html()+'<tr><td><div class="box col-md-4 box-widget widget-user">'+
                        '<!-- Add the bg color to the header using any of the bg-* classes -->'+
                        '<div class="widget-user-header bg-aqua-active">'+
                          '<h3 class="widget-user-username">'+e.refPersonnel.nom+' '+e.refPersonnel.prenom+'</h3>'+
                          '<h5 class="widget-user-desc">'+e.refPersonnel.fonction+'</h5>'+
                          '<input type="radio" name="equipe_selected" value="'+e.ref+'" class="pull-right">'+
                        '</div>'+
                        '<div class="widget-user-image">'+
                          '<img class="img-circle" src="/resources/image/avatar5.png" alt="User Avatar">'+
                        '</div>'+
                        '<div class="box-footer">'+
                          '<div class="row">'+
                           ' <div class="col-sm-4 border-right">'+
                              '<div class="description-block">'+
                               ' <h5 class="description-header">'+e.ref+'</h5>'+
                                '<span class="description-text">Reférence</span>'+
                             ' </div>'+
                             ' <!-- /.description-block -->'+
                            ' </div>'+
                           ' <!-- /.col -->'+
                            '<div class="col-sm-4 border-right">'+
                             ' <div class="description-block">'+
                               ' <h5 class="description-header">'+new Date(parseFloat(e.dateEquipe)).toDateString()+'</h5>'+
                               ' <span class="description-text">Date</span>'+
                             ' </div>'+
                             ' <!-- /.description-block -->'+
                            '</div>'+
                            '<!-- /.col -->'+
                            '<div class="col-sm-4">'+
                             ' <div class="description-block">'+
                                '<h5 class="description-header">'+new Date(parseFloat(e.dateExecution)).toDateString()+'</h5>'+
                                '<span class="description-text">Exécution</span>'+
                             ' </div>'+
                              '<!-- /.description-block -->'+
                            '</div>'+
                            '<!-- /.col -->'+
                          '</div>'+
                         ' <!-- /.row -->'+
                       ' </div>'+
                      '</div><br><br></td></tr>');
                    }
                    $('input[name="equipe_selected"]').change(function(){$('#zone-equipe').val($(this).val()); $("#equipe-liste").hide(1000); $('#form-edit-billonnage').show(1000);});
                    var listEquipe = document.getElementsByName("equipe_selected");
                    for(var i=0; i<listEquipe.length; i++){
                        if(listEquipe[i].value === $('#zone-equipe').val()){
                            listEquipe[i].checked = true;
                            break;
                        }
                    }
                    $('#form-edit-billonnage').hide(1000)
                    $('#equipe-liste').show(1000).DataTable();
                    
                    
                }
            });

        });

        
        $.ajax({
            url: '/api/parc/list',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var data = JSON.parse(jqXHR.responseText);
                var select = document.getElementById('zone-parc');
                var selected=-1;
                for(var i=0; i<data.length; i++){
                    var opt = document.createElement("option");
                    opt.value = data[i].ref;
                    opt.innerHTML = data[i].libelle+' ('+data[i].refTypeparc.libelle+')';
                    select.appendChild(opt);
                    if(data[i].ref === "<%=request.getParameter("parc")%>"){
                        selected= i;
                    }
                }
                document.getElementById('zone-parc').selectedIndex = selected;
                $('select').select2();
            }
        });
        $.ajax({
            url: '/api/reception/list/numero_bille',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var data = JSON.parse(jqXHR.responseText);
                var select = document.getElementById('zone-bille');
                var selected=-1;
                for(var i=0; i<data.length; i++){
                    var opt = document.createElement("option");
                    opt.value = data[i];
                    opt.innerHTML = data[i];
                    select.appendChild(opt);
                    if(data[i] == "<%=request.getParameter("bille")%>"){
                        selected= i;
                    }
                }
                document.getElementById('zone-bille').selectedIndex = selected;
                $('select').select2();
            }
        });
        $("#btn-edit-billonnage").click(function(){
            editData($('#zone-ref').val(), $('#zone-bille').val(), $('#zone-billon').val(), $("#zone-equipe").val(), $('#zone-travail').val(), $("#zone-parc").val());
            $('#form-edit-billonnage .overlay').removeClass('hide');
            $('#form-edit-billonnage #btn-save-billonnage').addClass("disabled");
        });

    </script>
</div>
