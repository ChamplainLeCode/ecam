<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-classification">
    <p class="status-edit-classification" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Reférence</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<% out.print(request.getParameter("ref"));%>" required="" class="form-control" id="zone-ref" placeholder="Rérérence">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Épaisseur (cm)</label>

          <div class="col-sm-10">
              <input type="number" min="0" max="200" required="" value="<% out.print(request.getParameter("epaisseur"));%>" class="form-control" id="zone-epaisseur" placeholder="epaisseur de la classification">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-essence" class="col-sm-2 control-label">Essence</label>
          <div class="col-sm-10">
              <select class="form-control select2" id="zone-essence" style="width:100%;">
              </select>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-classification"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        $.ajax({
            url: '/api/essence/list',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var data = JSON.parse(jqXHR.responseText);
                var select = document.getElementById('zone-essence');
                var selected=-1;
                for(var i=0; i<data.length; i++){
                    var opt = document.createElement("option");
                    opt.value = data[i].ref;
                    opt.innerHTML = data[i].libelle+ '('+data[i].densite+')';
                    if(data[i].ref === '<% out.print(request.getParameter("essence[ref]"));%>'){
                        selected = i;
                    }
                    select.appendChild(opt);
                }
                document.getElementById('zone-essence').selectedIndex = selected;
                $('select').select2();
            }
        });
        $("#btn-edit-classification").click(function(){
            editData($('#zone-epaisseur').val(), $('#zone-ref').val(), $('#zone-essence').val());
            $('#form-edit-classification .overlay').removeClass('hide');
            $('#form-edit-classification #btn-edit-classification').addClass("disabled");
        });

    </script>
</div>
