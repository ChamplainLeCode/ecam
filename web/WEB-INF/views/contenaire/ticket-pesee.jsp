<%@page import="app.modelController.ColisJpaController"%>
<%@page import="app.controllers.IndexController"%>
<%@page import="app.models.Essence"%>
,  ,,<%-- 
    Document   : Bon de reception
    Created on : 19 Avr. 2019, 19:42:16
    Author     : champlain
--%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="app.modelController.PaquetJpaController"%>
<%@page import="app.modelController.PlotJpaController"%>
<%@page import="app.modelController.EmpotageJpaController"%>
<%@page import="java.util.LinkedList"%>
<%@page import="app.models.Database"%>
<%@page import="app.modelController.EssenceJpaController"%>
<%@page import="app.models.ParcChargement"%>
<%@page import="java.util.HashMap"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.util.List"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.util.Date"%>
<%@page import="app.models.Colis"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="app.models.Embarquement"%>
<%
  
    Embarquement embarquement = (Embarquement)request.getAttribute("embarquement");
    Calendar date = Calendar.getInstance();
    if(embarquement != null){
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ECAM PLACAGES</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        
                <link rel="stylesheet" href="/resources/css/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="/resources/css/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="/resources/Ionicons/css/ionicons.min.css">
        
        <link rel="stylesheet" href="/resources/css/dataTables.bootstrap.min.css">      <!-- Theme style -->
        
        <link rel="stylesheet" href="/resources/css/skin-blue.min.css">
        <link rel="stylesheet" href="/resources/iCheck/all.css">
        
        
        <link rel="stylesheet" href="/resources/css/tracabilite.css" media="screen" />
        <link rel="stylesheet" href="/resources/css/tracabilite-print.css" media="print" />
        
        <script type="text/javascript">
            function updatePoids(){
                var poidsColis = document.getElementById("place-poids-colis"),
                    poidsTotal = document.getElementById("place-poids-total"),
                    poidsCont  = document.getElementById("place-poids-container"),
                    zonePoidsColis = document.getElementById("zone-poids-colis"),
                    zonePoidsC = document.getElementById("zone-poids-container"),
                    BordereauMasseTotale = document.getElementById("zone-masse-total");
                    
                    poidsColis.innerHTML = zonePoidsColis.value;
                    poidsCont.innerHTML = zonePoidsC.value;
                    poidsTotal.innerHTML = BordereauMasseTotale.innerHTML = parseInt(zonePoidsColis.value)+parseInt(zonePoidsC.value);
            };

        </script>
        <style>
            @page 
            {
                size: auto;   /* auto is the current printer page size */
                margin-top: 0mm;
                margin-bottom: 0mm;
                padding-bottom: 30mm;/* this affects the margin in the printer settings */
            }

            .to-print{
                visibility: hidden;
            }
    @media print {
                .no-print{
                    display: none;
                }
                .print{
                    color: #1234A0;
                }
                .saut-page{
                }
                .to-print{
                    visibility: visible;
                }
            .saut-page
            {
                /* saut de page apres le bloc*/
                page-break-after : always;
                /* saut de page avant le bloc*/
                page-break-before : always;
                
                font-size: 8px;
                height: 100%;
                min-height: 720px;
                position: relative;
                width: 100%;
                background: url(/resources/image/bois.png);
                background-position-x: 2%;
                background-position-y: 100%;
                background-size: 300px 100px;
                background-repeat: no-repeat;
                margin-top: 200px;
            }
        }
            .value{
                font-size: 8pt;
                font-weight: normal;
                margin-left: 20px;
            }
            .logo-bois{
                float: left;
                position: relative;
                margin-bottom: 100px;
                height: 100px;
                page-break-after: always;
            }
            @page  {
                size: A4;
                @top-center { content: "Preliminary edition" };
                @bottom-center { content: counter(page) };
                
            }
            input{
                border: none;
            }
        </style>
    </head>

    <body style="font-size: 10px;">
        <div class="container saut-page">
            <div style="background-color: white; padding: 20px;">

                <div style="background: white; font-size: 10pt; margin-top: 30px;" >

                    <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-3 pull-left">
                                <p style="font-weight: bold;">ECAM PLACAGES S.A.</p>
                                <p style="font-weight: bold; margin-bottom: 5px;">SERVICES EMBALLAGE & PESEE</p>
                                <p style="font-weight: bold; margin-bottom: 5px; ">CERTIFICAT DE VERIFICATION</p>
                                <p style="font-weight: bold; margin-bottom: 5px; ">B.P. 76 Mbalmayo</p>
                                <p style="font-weight: bold; margin-bottom: 5px; ">Téléphone: (237) 699.41.48.94</p>
                                <p style="font-weight: bold; margin-bottom: 5px; ">Email: ecamplacage@ecamplacages.com</p>
                            </div>
                            <div class="col-sm-6 center-block" style=" top: 100px;margin-bottom: 100px; background: white; ; border-radius: 10px; text-align: center; ">
                                <p style="font-weight: bold; margin-bottom: 5px; text-align: center;">N°: <input type="text" placeholder="N° Ticket" class="no-print" onchange="document.getElementById('spanNumTicket').innerHTML = this.value;"><span id="spanNumTicket" style="font-weight: bold"></span> du: <%=String.format("%02d", date.get(Calendar.DATE))+"/"+String.format("%02d", (date.get(Calendar.MONTH)+1))+"/"+String.format("%02d", (date.get(Calendar.YEAR)))%></p>
                            </div>
                            <div class="col-sm-3 pull-right">
                                <img src="/resources/image/IMG-20190401-WA0011.jpg" style="width: 200px;">
                            </div>

                    </div>

                    <div class="row">
                            <h2 style="font-weight: bold; margin-bottom: 25px; text-align: center;">TICKET DE PESÉE</h2>
                    </div>
                </div>
            </div>

            <div class="row" style="border-radius: 10px; margin: 20px; border: 1px black; border-style: solid">
                <table class="table">
                    <tr>
                        <td>Méthode de Pesée <span style="font-weight: bold;">[2]</span></td>
                        <td>Container de Type <span style="font-weight: bold;"><%=embarquement.getNumConteneur().getRefTypecont().getRefTypecont()%></span></td>
                    </tr>
                    <tr>
                        <td>N° Container<span style="font-weight: bold;"><%=embarquement.getNumConteneur().getNumConteneur()%></span></td>
                        <td>Transporteur <span style="font-weight: bold;"><%=embarquement.getRefTransporteur().getNom()%></span></td>
                    </tr>
                    <tr>
                        <td>N° Plomb <span style="font-weight: bold;"> <%=embarquement.getNumPlomb()%></span></td>
                        <td>N° Véhicule <span style="font-weight: bold;"><%=embarquement.getImmatriculation()%></span></td>
                    </tr>
                    <tr>
                        <td>N° Lettre voiture <span style="font-weight: bold;"><%=embarquement.getLettreCamion()%></span></td>
                        <td>Nom Chauffeur <span style="font-weight: bold;"><%=embarquement.getRefChauffeur().getNom()%></span></td>
                    </tr>
                </table>
            </div>

            <div class="row" style="border-radius: 10px; margin: 20px">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>TARE(Kg) CONTAINER<span style="font-weight: bold;">[1]</span></td>
                            <th>NOMBRE DE COLIS<span style="font-weight: bold;"></span></td>
                            <th>POIDS NET COLIS(Kg)<span style="font-weight: bold;">[2]</span></td>
                            <th>VGM(Kg)(1+2) TOTAL<span style="font-weight: bold;"></span></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><h2><span class="print " id="place-poids-container" style="font-weight: bold;"></span></h2><input type="number" id="zone-poids-container" value="0" onchange="updatePoids()" class="no-print"/></td>
                            <td><h2><span style="font-weight: bold;"><%=embarquement.getColisList().size()+""%></span></h2></td>
                            <td><h2><span class="print" style="font-weight: bold;" id="place-poids-colis"></span></h2><input type="number" id="zone-poids-colis" value="0" onchange="updatePoids()" class="no-print"/></td>
                            <td><h2><span class="print" style="font-weight: bold;" id="place-poids-total"></span></h2></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="row col-sm-4 pull-right" style=" border: black 1px;  border-style: solid; margin-right: 20px">
                <span style="font-weight: bold;">Fait à Mbalmayo, le </span><span class="zone-date"><%=String.format("%02d", date.get(Calendar.DATE))+"/"+String.format("%02d", (date.get(Calendar.MONTH)+1))+"/"+String.format("%02d", (date.get(Calendar.YEAR)))%></span>
                <hr style="background-color: black; border-color: black;">
                <span style="font-weight: bold; text-align: center"> OPÉRATEUR DE PESÉE</span><br>
                <hr style="background-color: black; border-color: black;"><br><br><br><br><br>
            </div>
        </div>
                
        <div class="saut-page container" id="zone-bordoreau">
            <div style="background-color: white; padding: 20px; font-size: 8pt;">
                <div style="background: white; margin-top: 30px; font-size: 8pt;" >

                    <div class="row" style="margin-bottom: 10px; font-size: 8pt;">
                            <div class="pull-right" style=" float: right; margin-top: 0px; background: white; border: black 1px; border-radius: 10px; text-align: center; font-size: 8pt;">
                                <p style="font-weight: bold; font-size: 8pt; margin-bottom: 5px;">RÉPUBLIQUE DU CAMEROUN</p>
                                <p style="font-weight: bold; margin-bottom: 5px; text-align: center">Paix-Travail-Patrie</p>
                            </div>
                            <div class="pull-left" style="float:left; background: white; border-radius: 10px; text-align: center;  font-size: 8pt;">
                                <p style="font-weight: bold;">MINISTÈRE <span style="text-decoration: black dashed underline ">DES  FORETS ET</span> DE LA FAUNE</p>
                                <p style="font-weight: bold; margin-bottom: 10px;">DÉLÉGATION RÉGIONALE DU CENTRE</p>
                                <p style="font-weight: bold; margin-bottom: 10px; ">DÉLÉGATION DÉPARTEMENT DU NYONG-ET-SO</p>
                                <p style="font-weight: bold; margin-bottom: 10px; ">SECTION DÉPARTEMENTALE DE LA<br> TRANSFORMATION ET DE LA 
                                    PROMOTION<br> DES PRODUITS FORESTIERS
                                </p>
                            </div>
                    </div>

                    <div class="row">
                        <p style="font-weight: bold; font-size: 12px; margin-bottom: 5px; text-align: center;">N° Dossier : <input style="width: 30px" type="text"> / <%=String.format("%04d", (date.get(Calendar.YEAR)))%></p>
                    </div>
                </div>
            </div>
            <div class="row" style="border-radius: 10px; margin: 20px;">     
                <table class="table" style="border: none; border-color: transparent;  font-size: 8pt;">
                    <thead style="margin-bottom: 20px;">
                <tr>
                    <th colspan="2" style="font-weight: normal; font-size: 8pt; ">
                        <span style="font-weight: bold">BORDEREAU DE SPÉCIFICATION DES PRODUITS TRANSPORTÉS </span>N°.................................../BSTP/MINFOF/DDFOF.NS/STP
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="col-sm-6">
                        <label class="courriel">Origine: <span class="value" > Cameroun</span></label>
                    </td>
                    <td class="col-sm-6">
                        <label class="courriel">Expediteur: <span class="value">ECAM PLACAGES</span></label>
                    </td>
                </tr>
                <tr>
                    <td class="col-sm-6">
                        <label class="courriel">Destination: <span class="value"><%=embarquement.getVilleDestination()+" - "+embarquement.getPaysDestination()%></span></label>
                    </td>
                    <td class="col-sm-6">
                        <label class="courriel">N° Contribuable: <span class="value"><%=embarquement.getRefCompagnie().getNumContribuable()%></span></label>
                    </td>
                </tr>
                <tr>
                    <td class="col-sm-6">
                        <label class="courriel">Container N°: <span class="value"><%=embarquement.getNumConteneur().getNumConteneur()%></span></label>
                    </td>
                    <td class="col-sm-6">
                        <label class="courriel">Transporteur: <span class="value"><%=embarquement.getRefTransporteur().getNom()%></span></label>
                    </td>
                </tr>
                <tr>
                    <td class="col-sm-6">
                        <label class="courriel">N° Immatriculation: <span class="value"><%=embarquement.getImmatriculation()%></span></label>
                    </td>
                    <td class="col-sm-6">
                        <label class="courriel">N° Contribuable Transporteur: <span class="value"><%=embarquement.getRefTransporteur().getNumContribuable()%></span></label>
                    </td>
                </tr>
                <tr>
                    <td class="col-sm-6">
                        <label class="courriel">Plomb N°: <span class="value"><%=embarquement.getNumPlomb()%></span></label>
                    </td>
                    <td class="col-sm-6">
                        <label class="courriel">Camion: <span class="value"><%=embarquement.getLettreCamion()%></span></label> 
                    </td>
                </tr>
                <tr>
                    <td class="col-sm-6">
                        <label class="courriel">Chauffeur: <span class="value"><%=embarquement.getRefChauffeur().getNom()%></span></label>
                    </td>
                    <td class="col-sm-6">
                        <label class="courriel">Type Container: <span class="value"><%=embarquement.getNumConteneur().getRefTypecont().getLibelleTypecontennaire()%></span></label> 
                    </td>
                </tr>
            </tbody>
        </table>
            </div>
            <div class="row" style="border-radius: 10px; margin: 20px">
                
                <table class="table table-striped table-hover table-condensed" style=" font-size: 8pt;">
                    <tr>
                        <th>N° Colis</th>
                        <th>Essences</th>
                        <th>Longueur (cm)</th>
                        <th>Lageur(cm)</th>
                        <th>Epaisseur (cm)</th>
                        <th>Volume (m3)</th>
                    </tr>
                    <% for(Colis c : embarquement.getColisList()) {%>
                    <tr>
                        <td><%=c.getRefColis()%></td>
                        <td><%=c.getEssence()%></td>
                        <td><%=c.getLongueurColis()%></td>
                        <td><%=c.getLargeurColis()%></td>
                        <td><%=c.getEpaisseur()%></td>
                        <td><%=Math.round(c.getVoulumeColis())%></td>
                    </tr>
                    <% }%>
               </table>
            </div>
               <div class="width-full pull-right" style="width: 100%;">
                
                <div class="row col-sm-3 pull-right" style="margin-left: 20px; margin-right: 10px; border: black 1px; border-style: solid; ">
                    <span style="font-weight: bold;">Fait à Mbalmayo, le </span><span class="zone-date"><%=String.format("%02d", date.get(Calendar.DATE))+"/"+String.format("%02d", (date.get(Calendar.MONTH)+1))+"/"+String.format("%02d", (date.get(Calendar.YEAR)))%></span>
                    <hr style="background-color: black; border-color: black;">
                    <span style="font-weight: bold; text-align: center"> OPÉRATEUR DE PESÉE</span><br>
                    <hr style="background-color: black; border-color: black;"><br><br><br><br><br>
                </div>
                <div class="pull-right col-sm-3" style="text-align: left; font-weight: bold; font-size: 8px;">
                    <p>TOTAL COLIS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <span style="float: right"><%=embarquement.getNbrColis()%></span></p>
                    <p>TOTAL ELEMENTS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span style="float: right"><%=embarquement.getNbrPaquets()%></span></p>
                    <p>TONNAGE TOTAL &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <span style="float: right" id="zone-masse-total"></span></p>
                    <p>VOLUME TOTAL NET&nbsp;&nbsp; :<span style="float: right"><%=embarquement.getVolumeContennaire()%></span></p>
                    <p>VOLUME TOTAL BRUT&nbsp;: <span style="float: right"><%=String.format("%.2f", embarquement.getVolumeBrut())%></span></p>
                    <p>TOTAL COLIS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span style="float: right"><%=embarquement.getNbrColis()%></span></p>
                </div>
            </div>
            <!--table class="to-print" style="position: absolute; bottom: 0; width: 95%;">
                <tbody>
                        <tr>
                            <td>
                                <img src="/resources/image/bois.png" style="width: 300px; height: 80px; margin-bottom: 50px;">
                            </td>
                        </tr>
                </tbody>
            </table-->

        </div>

        <div id="fiche-tracabilite" style="padding-top: 20px;" class="saut-page">
            <div id="entete" style="text-align: center; margin: 5px;" class="row">
              <h3>FICHE DE TRAÇABILITE DES PRODUITS PAR CONTAINER</h3>
              <h3>[ECAM PLACAGES S.A ]</h3>
              <div id="info-entete">
                <label for="dateContainer">Date Container </label>
                <label><%=String.format("%02d", date.get(Calendar.DATE))+"/"+String.format("%02d", (date.get(Calendar.MONTH)+1))+"/"+String.format("%02d", (date.get(Calendar.YEAR)))%></label>
                <label for="numLettre">N Lettre Voiture : </label>
                <label><%=embarquement.getLettreCamion()%></label>
                <br />
                <label for="numContainer">N container : </label>
                <label><%=embarquement.getNumConteneur().getNumConteneur()%></label>
                <label for="numPlomb">N Plomb : </label>
                <label><%=embarquement.getNumPlomb()%></label>
                <br/>
                <label for="exercice">Excercice :  </label>
                <label><%=date.get(Calendar.YEAR)%></label>
              </div>
            </div>
            <!-- CONTENU -->
            <%
                List<HashMap<String, Object>> liste = (List) request.getAttribute("tracabilite");
                if(liste != null || !liste.isEmpty()){
                %>
            <div id="contenu" class="row" style=" width: 100%; font-size: 8pt;">
                <div id="table1" class="table ">
                    <table class="table table-bordered" style=" font-size: 8pt;">
                  <thead>
                    <th>Concession</th>
                    <th>N Titre</th>
                    <th>Code</th>
                    <th>Essence</th>
                    <th>Certification[origine]</th>
                    <th>Volume</th>
                  </thead>
                  <tbody>
                    <!-- Ligne 1 -->
                    <%
                        HashMap<String, Object> essenceMap = new HashMap();
                        EssenceJpaController essCont = new EssenceJpaController(Database.getEntityManager());
                        ColisJpaController colisCont = new ColisJpaController(Database.getEntityManager());
                        DecimalFormat formater = new DecimalFormat(".###");
                        
                        for(HashMap o : liste){
                            ParcChargement parc = ((ParcChargement)o.get("parc")); 
                    %>
                    <tr>
                        <td><%=(parc.getTitreList().isEmpty() ? "" : parc.getTitreList().get(parc.getTitreList().size()-1).getConcession())%></td>
                        <td><%=(parc.getTitreList().isEmpty() ? "" : parc.getTitreList().get(parc.getTitreList().size()-1).getNumTitre())%></td>
                        <td><%
                                String essDS = essCont.getEssenceRefByName(o.get("essence").toString());
                            if(essenceMap.get("libelle-"+o.get("essence").toString()) == null){
                                    essenceMap.put("code-"+o.get("essence").toString(), essDS);
                                    essenceMap.put("origin-"+o.get("essence").toString(), parc.getCertificat());
                                    essenceMap.put("libelle-"+o.get("essence").toString(), o.get("essence").toString());
                                    essenceMap.put("volume-"+o.get("essence").toString(), o.get("volume").toString());
                                    JSONArray obj = new JSONArray();
                                    obj.put(o.get("essence"));
                                    essenceMap.put("essenceLibelle", obj);
                                    out.print('['+essDS+"]");//essenceMap.get("code-"+o.get("essence").toString()));
                            }else{
                                    essenceMap.put("volume-"+o.get("essence"), (Float.parseFloat(essenceMap.get("volume-"+o.get("essence")).toString())+Float.parseFloat(o.get("volume").toString()))+"");
                                    JSONArray obj = ((JSONArray)essenceMap.get("essenceLibelle"));
                                    int i=-1;
                                    for(i=0; i<obj.length(); i++)
                                        if(obj.getString(i).equalsIgnoreCase(o.get("essence").toString()))
                                            break;
                                    if(i>=obj.length())
                                        obj.put(o.get("essence"));
                                    essenceMap.put("essenceLibelle", obj);
                                    out.print("\" \"");
                            }
                        %></td>
                        <td><%=o.get("essence")%></td>
                        <td><%=colisCont.getCertificationColis(o.get("colis").toString())%></td>
                        <td><%=formater.format(Float.parseFloat(o.get("volume").toString()))%></td>
                    </tr>
                    <%}%>
                  </tbody>
                </table>
                    
                  <table class="table table-bordered" style=" font-size: 8pt;">
                  <thead>
                    <th>Code</th>
                    <th>Designation essence</th>
                    <th>Certification [origine]</th>
                    <th>volume (M3)</th>
                  </thead> 
                  <tbody>
                    <!-- Ligne 1 -->
                    <% 
                        JSONArray rapport = (JSONArray)essenceMap.get("essenceLibelle");
                        
                        if(rapport != null)
                        for(int i=0; i<rapport.length(); i++){
                    %>
                    <tr>
                        <td><%=essenceMap.get("code-"+rapport.getString(i))%></td>
                        <td><%=rapport.getString(i)%></td>
                        <td><%=essenceMap.get("origin-"+rapport.getString(i))%></td>
                        <td><%=essenceMap.get("volume-"+rapport.getString(i))%></td>



                    </tr>
                    <% }%>
                  </tbody>
                </table>
              </div>
            </div>
                <%  
                    }
                %>
            <div class="row" style="position: relative; width: 100%; padding-left: 50px;">
                <div id="" style="font-size: 12px;" class="pull-right">
                    <div class="pull-right col-lg-5 col-md-5 col-sm-5 col-xl-12" style="border: solid 1px black; height: 170px;">
                        <p style="height: 30px; text-align: right; font-size-adjust: 0pt;">
                          Fait a Mbalmayo, le
                          <input type="text" placeholder="Date" type="date" value="<%=String.format("%02d", date.get(Calendar.DATE))+"/"+String.format("%02d", (date.get(Calendar.MONTH)+1))+"/"+String.format("%02d", (date.get(Calendar.YEAR)))%>"  />
                        </p>
                        <div class="hr"></div>
                        <p style="text-align: center">DIRECTEUR GENERAL</p>
                        <div class="" style="height: 100px;"></div>
                      </div>
                    <div class="pull-left  col-lg-4 col-md-4 col-sm-4 col-xl-12" style="border: solid 1px black; font-size: 8pt; text-align: justify;  padding: 5px; font-weight: bold;  height: 170px;">
                        <p>VISA:</p>
                        <P style="margin-bottom: 50px;">
                          DÉLÉGATION DÉPARTEMENTALE DES FORETS ET DE LA FAUNE DU NYONG ET SO'O
                        </P>
                        <div class="hr" style="margin-left: -5px;position: relative;"></div>
                        <p>Le delegue departemental</p>
                    </div>
                </div>
            </div>
            <table class="to-print" style="position: absolute; bottom: 0; width: 95%; border: none; ">
                <tbody>
                        <tr>
                            <td>
                                <img src="/resources/image/bois.png" style="width: 300px; height: 80px; margin-bottom: 50px;">
                            </td>
                        </tr>
                </tbody>
            </table>

        </div>
                
        <div class="saut-page print" id="fiche-specification" style="display: none;     color: #1234A0;">
            
                <img src="/resources/image/4_5913703756481954944.png" style="width: 100%;"/>
                <div style="width: 100%;">
                    <div id="entete-droite" style=" width: 40%; float: right">
                        <div id="cadre-entete">
                          <h4><%=embarquement.getRefTransporteur().getNom()%></h4>
                          <p><%=embarquement.getNomBateau()%></p>
                          <p><%=embarquement.getRefClient().getTelephones()+" "+embarquement.getRefClient().getNomClient()+" "%></p>
                        </div>
                        <div>Mbalmayo, Le <input type="text" style="border:none;" value="<%=new Date().getDay()+"/"+new Date().getMonth()+"/"+(new Date().getYear()+1900)%>" placeholder="Entrez la date" /></div>
                    </div>
                    <div id="entete-gauche" class="pull-left" style="width: 60%; float: left;">
                        <h4 style="color: #1234A0">SPECIFICATION N: <input type="text" placeholder="N° de la spécification" style="border: none; background: transparent"></h4>
                        <table>
                          <tr class="form-group">
                              <td><label for="par">By / Par: </label></td>
                              <td><label><span class="user-nom"></span></label></td>
                          </tr>
                          <tr class="form-group">
                                <td><label for="du">Du : </label></td>
                                <td><label><input type="date" style="border: none; background: transparent"/></label></td>
                            </tr>
                          <tr class="form-group">
                                <td ><label for="destination">Destination: </label></td>
                                <td><label><%=embarquement.getVilleDestination()+ '('+embarquement.getPaysDestination()+")"%></label></td>
                          </tr>
                          <tr class="form-group">
                                <td style="width: 300px;">
                                    <label for="portDest">Port de Destination : </label></td>
                                <td style="width:300px;"><label><%=embarquement.getPortDepart()+"["+embarquement.getPaysDestination()+"]"%></label></td>
                          </tr>
                          <tr class="form-group">
                            <td><label for="marque">Marque : </label></td>
                            <td><label><%=embarquement.getNomBateau()%></label></td>
                          </tr>
                        </table><br><br>
                    </div>  
                </div>
                <br><br><br>
                <div style="width: 100%;">
                    <div id="contenu">
                        <div id="table1">
                        <table  class="table table-bordered table-condensed table-striped">
                          <thead>
                            <th>N Colis</th>
                            <th>Etat Colis</th>
                            <th>Qualité</th>
                            <th>Essences [Libelle]</th>
                            <th>Essences [Code]</th>
                            <th>Pds Brut (kg)</th>
                            <th>Long. M (cm)</th>
                            <th>Larg. M (cm)</th>
                            <th>Haut. M (cm)</th>
                            <th>Nb. Feuilles</th>
                            <th>Volumes (M3)</th>
                          </thead>
                          <tbody>
                            <!-- Ligne 1 -->
                            <%
                                HashMap<String, List<Colis>> colisMap = new HashMap();
                                LinkedList listEssence = new LinkedList();
                                
                                DecimalFormat _2DigitFormateur = new DecimalFormat("00");
                                
                                HashMap<String, String[]> donneesEssencePhytoSanitaire = new HashMap();
                                
                                for(Colis c : embarquement.getColisList()){
                                    if(colisMap.get(c.getEssence()) == null){
                                        List<Colis> l = new LinkedList();
                                        /**
                                         * Informations pour la fiche phytosanitaire
                                         */
                                        String[] donneesPhytoSanitaire = new String[]{
                                          new EssenceJpaController(Database.getEntityManager()).getEssenceRefByName(c.getEssence()),
                                          ""  
                                        };
                                        donneesEssencePhytoSanitaire.put(c.getEssence(), donneesPhytoSanitaire);
                                        /**
                                         * Fin en bas on va récupérer les surfaces cumulées
                                         */
                                        l.add(c);
                                        colisMap.put(c.getEssence(), l);
                                        listEssence.add(c.getEssence());
                                    }else{
                                        colisMap.get(c.getEssence()).add(c);
                                    }
                                }
                                String last = "";
                                double volumeTotal = 0, surfaceTotale = 0, poidsTotal = 0;
                                int nbrFeuillesTotal = 0, colisTotal = 0;
                                Object[] listEssenceArray = listEssence.toArray();
                                for(Object o : listEssenceArray){
                                    List<Colis> l = colisMap.get(o.toString());
                                    float poids = 0, volume = 0, surface = 0;
                                    double nbrFeuilles = 0, feuilles=0;
                                    for(int i=0; i<l.size(); i++){
                                        Colis c = l.get(i);
                            %>
                                <tr>
                                    <td><%=c.getRefColis()%></td>
                                    <td><%=c.getTypeColisFull()%></td>
                                    <td><%=c.getQualite()%></td>
                                    <td><%=i==0 ? c.getEssence() : "" %></td>
                                  <td><%=new EssenceJpaController(Database.getEntityManager()).getEssenceRefByName(c.getEssence())%></td>
                                  <td><%poids += c.getPoidNet(); out.print(c.getPoidNet());%></td>
                                  <td><%=c.getLongueurColis()%></td>
                                  <td><%=c.getLargeurColis()%></td>
                                  <td><%=new EmpotageJpaController(Database.getEntityManager()).getHauteurForColis(c.getRefColis())%></td>
                                  <td><% feuilles += (nbrFeuilles=new PaquetJpaController(Database.getEntityManager()).countFeuilleByColis(c.getRefColis())); out.print(nbrFeuilles);%></td>
                                  <td><% volume += c.getVoulumeColis(); out.print(c.getVoulumeColis());%></td>
                                </tr>
                                <%
                                    surface += c.getSurfaceColis();
                                    };
                                %>
                                <!-- Ligne 2 -->
                                <tr class="colis">
                                    <td colspan="4"><%colisTotal+=l.size(); out.print(l.size());%> Colis</td>
                                  <td></td>
                                  <td><%poidsTotal += poids; out.print(poids);%></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td><%nbrFeuillesTotal += feuilles; out.print(feuilles);%></td>
                                  <td><%volumeTotal += volume; out.print(volume);%></td>
                                </tr>
                                <%
                                    surfaceTotale += surface;
                                    donneesEssencePhytoSanitaire.get(o.toString())[1] = surface+"";}
                            %>
                          </tbody>
                          <tfoot>
                              
                                <tr class="colis">
                                    <td colspan="8"></td>
                                </tr>
                              
                                <tr class="colis" style="font-weight: bold">
                                    <td colspan="4"><%=colisTotal%><%="\t\t"%>Colis</td>
                                  <td></td>
                                  <td><%=poidsTotal%></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td><%=nbrFeuillesTotal%></td>
                                  <td><%=volumeTotal%></td>
                                </tr>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                </div>
                <div id="grapper-specification"></div>
                <img src="/resources/image/bois.png" class="logo-bois image">
            <!-- CONTENU -->
            
        </div>
                                
        <div class="saut-page container" style=" width: 100%; font-size: 14px; padding-top: 50px; margin-top: 200px;">
            <div id="phyto" class="row" style="width: 100%;">
                <h4 class="pull-left">FICHE DE SUIVI PHYTOSANITAIRE</h4>
            </div>
            <!-- CONTENU -->
            <div id="sanitaire">
                <div id="table-sanitaire">
                    <table class="table" style="font-size: 8px;">
                        <thead>
                            <tr>
                                <th  style="font-size: 8px;">N°</th>
                                <th  style="font-size: 8px;">DESIGNATION</th>
                                <th style="font-size: 8px;">DETAILS</th>
                                <th style="font-size: 8px;">QUANTITE(m2)</th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 8pt;">
                            <tr>
                                <td>01</td>
                                <td>Nom et adresse de l'exportateur</td>
                                <td>
                                  COMPAGNIE EXPLOITATION INDUSTRIELLE DES BOIS DU CAMEROUN
                                  S.A(ECAM PLACAGES) B.P. 76 MBYO MBALMAYO
                                </td>
                                <td></td>
                            </tr>

                            <tr>
                                <td>02</td>
                              <td>Destinataire</td>
                              <td>
                                    <%=
                                         embarquement.getRefClient().getNomClient()+", VIA "
                                        +embarquement.getRefTransporteur().getNom()+", "
                                        +embarquement.getVilleDestination()+", "
                                        +embarquement.getPaysDestination()+", Tel:"
                                        +embarquement.getTelephone()
                                    %>
                              </td>
                              <td></td>
                            </tr>
                            <tr>
                                <td>03</td>
                                <td>Nature du Bois et quantite (m2)</td>
                                <td>
                                  <%
                                      for(Object e : listEssenceArray){

                                  %>
                                  [<%=donneesEssencePhytoSanitaire.get(e.toString())[0]%>] <%=e%> <br>
                                  <%
                                      };
                                  %>
                                </td>
                                <td>
                                  <%
                                      for(Object e: listEssenceArray){
                                  %>
                                      <%=donneesEssencePhytoSanitaire.get(e.toString())[1]%><br>
                                  <%
                                      };
                                  %>
                                </td>
                            </tr>
                            <tr>
                                <td>04</td>
                                <td>Quantite Totale (m2)</td>
                                <td>
                                    Facture N 0<%=Calendar.getInstance().get(Calendar.DATE)%>/<%=Calendar.getInstance().get(Calendar.YEAR)%> Du <%=_2DigitFormateur.format(Calendar.getInstance().get(Calendar.DATE))+"/"+_2DigitFormateur.format(Calendar.getInstance().get(Calendar.MONTH))+"/"+Calendar.getInstance().get(Calendar.YEAR)%>
                                </td>
                                <td>
                                  <%=volumeTotal%>
                                  <br />
                                  <%=surfaceTotale%>
                                </td>
                            </tr>

                            <tr>
                                <td>05</td>
                                <td>Origine</td>
                                <td>
                                  CAMEROUN
                                </td>
                                <td></td>
                            </tr>

                            <tr>
                                <td>06</td>
                                <td>Moyen de Transport</td>
                                <td>
                                  Voie Maritime
                                </td>
                              <td></td>
                            </tr>

                            <tr>
                                <td>07</td>
                                <td>Conditionnement</td>
                                <td>
                                  Protege par plastique sur palettes en bois et mis en container
                                  <%=embarquement.getNumConteneur().getRefTypecont().getRefTypecont()%><!--HIGH CUBE 40'4/4(ACLU)-->
                                </td>
                                <td></td>
                            </tr>

                            <tr>
                                <td>08</td>
                                <td>Reference des documents phytosanitaires</td>
                                <td>
                                  RAS
                                </td>
                                <td></td>
                            </tr>

                            <tr>
                                <td>09</td>
                                <td>Etat phytosanitaire du bois</td>
                                <td>
                                  A.-BON: OUI B.-MAUVAIS: ///
                                </td>
                                <td></td>
                            </tr>

                            <tr>
                                <td>10</td>
                                <td>Date de traitement</td>
                                <td><input type="date" style="width: 150px;"></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td>11</td>
                                <td>Mode de traitement utilise</td>
                                <td>
                                    Traitement Thermique (100/150 C) en sechoir dynamique
                                </td>
                              <td></td>
                            </tr>

                            <tr>
                                <td>12</td>
                                <td>Commissaire agree en Douane</td>
                              <td>
                                ITAL SERVICES Sarl, BP.: 12536 DOUALA
                              </td>
                              <td></td>
                            </tr>

                            <tr>
                                <td>13</td>
                                <td>Traitement</td>
                                <td>
                                  Exportateur: ECAM PLACAGES S.A
                                </td>
                                <td></td>
                            </tr>

                            <tr>
                                <td>14</td>
                                <td>Observations</td>
                                <td><p style="text-align: justify;">Les palettes sont fabriquées avec les déchets de production du placage dont 
                                    la grume a été maintenue dans les bacs à vapeur pour au moins 24 heures, dont 
                                    le processesus a été agrée par la décision N°01109/18/D/MINADER/SG/DRCQ/SDRP/SRP 
                                    du 24/08/2018 Réf : IPPC : CM-023 HT complètant la N° 0287/D/MINADER/SG/DRCQ/SDRP/SRP 
                                    du 24/02/2016 Réf : IPPC : CMR-023-16-SDRP HT</p></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td>15</td>
                                <td>L'exportateur / son representant</td>
                                <td>
                                  MBALMAYO ,<%=new Date().toGMTString()%>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <table class="to-print" style="position: absolute; bottom: 0; width: 95%; border: none;">
                <tbody>
                        <tr>
                            <td>
                                <img src="/resources/image/bois.png" style="width: 300px; height: 80px; margin-bottom: 50px;">
                            </td>
                        </tr>
                </tbody>
            </table>

        </div>
                                
        <div style="width: 100%; font-size: 14px; padding-top: 50px;" id="certificat-empotage" class="saut-page">
            <div id="entete-bordereau" class="container">
                <div style="font-size: 14px;" class="pull-left" >
                    <p>MINISTERE DES FORETS ET DE LA FAUNE</p>
                    <div class="hr"></div>
                    <p>DELEGATION REGIONALE DU CENTRE</p>
                    <div class="hr"></div>
                    <p>DELEGATION DEPARTEMENTALE DU NYONG-ET-SO'O</p>
                    <div class="hr"></div>
                    <p>
                      SECTION DEPARTEMENTALE DE LA TRANSFORMATION
                      <br />
                      DE LA PROMOTION DES PRODUITS FORESTIERS
                    </p>
                </div>
                <div class="pull-right" >
                    <p style="text-align: center">REPUBLIQUE DU CAMEROUN</p>
                    <p style=";text-align: center">Paix-Travail-Patrie</p>
                </div>
              </div>
            <div class="container" id="num-dossier" style="text-align: center">
                <br />
                <span style="font-style: italic; font-weight: normal">
                  N Dossier: 
                  <span style="font-weight: bold"><input type="text" id="zoneCode"></span>/MINFI/DGD/SDCE/GADC/SAMBYO</span>
              </div>
              <h4 style="text-align: center; text-decoration-line: underline">
                CERTIFICAT D'EMPOTAGE
              </h4>

              <div id="rapport" style="text-align: justify">
                <div>
                  Nous Soussignés
                  <span style="font-weight: bold" style="margin-left: 1em">
                      <select style="width: 400px;" class="no-print select2" id="zoneNomDouanier"></select>
                      <span id="spanNomDouanier" style="font-weight: bold"></span></span>
                  <div class="hr" style="width: 90%; margin-left: 120px"></div>
                </div>
                <div>
                  (Noms, Prenoms,grades) des douanes en service à Mbalmayo
                </div>
                <br />
                <p>Certifie (certifions) avoir :</p>
                <div>
                  assiste ce jour
                  <span
                    class="
                  bold"
                    style="padding-right: 35px; border-bottom: solid 1px black"
                    ><%=Calendar.getInstance().get(Calendar.DATE)%>/<%=Calendar.getInstance().get(Calendar.MONTH)%>/<%=Calendar.getInstance().get(Calendar.YEAR)%></span
                  >
                  a l'usine
                  <span style="font-weight: bold">d'ECAM PLACAGES - MBALMAYO</span>
                </div>
                <p>
                  Sur demande
                  <span style="font-weight: bold">d'ECAM PLACAGES</span>
                </p>
                <p>
                  (Exportateur) a l'empotage du (des) container(s) N
                  <span
                    style="font-weight: bold"
                    style="padding-right: 35px; border-bottom: solid 1px black"
                    ><%=embarquement.getNumConteneur().getNumConteneur()%>
                  </span>
                </p>
                <br />
                <div>
                  Reconnu et constate
                  <span style="font-weight: bold">
                      [<%=embarquement.getNbrColis()%> COLIS DE BOIS EN PLACAGES M3:<%DecimalFormat df = new DecimalFormat(); df.setMaximumFractionDigits(3); out.print(df.format(volumeTotal));%> - <%=poidsTotal/1000 >= 1 ? Math.floor(poidsTotal/1000)+"TONNE(S)"+(poidsTotal%1000)+"KILOGRAMMES": poidsTotal+" KILOGRAMMES"%>]
                    ->
                  </span>
                  <div class="hr" style="width: 90%; margin-left: 8.4em"></div>
                  <br />
                  <span style="font-weight: bold"><%=embarquement.getNbrColis()%> COLIS DE BOIS EN PLACAGES </span>
                  <div class="hr"></div>
                  <br />
                  <span style="font-weight: bold" style="padding-left: 25%;"> CUBANT: <%out.print(df.format(volumeTotal));%></span
                  ><span style="font-weight: bold" style="padding-left: 12%;"
                    >SOIT : <%=poidsTotal/1000 >= 1 ? Math.floor(poidsTotal/1000)+"TONNE(S)"+(poidsTotal%1000)+"KILOGRAMMES": poidsTotal+" KILOGRAMMES"%></span
                  >
                  <span
                    >(Nombre de colis, designation commerciale, marque et n des colis,
                    volume)</span
                  >
                </div>
                <span>En presence de : </span>
                <br />
                <div>
                  Mr./Mme/Mlle
                  <span style="font-weight: bold" style="width: 90%; border-bottom: solid 1px black">[<span class="user-nom"></span>] [<span class="user-fonction"></span>]</span>
                </div>
                <br />
                <div>
                    (<span class="user-fonction"></span>) agissant au nom et pour compte
                  <span style="font-weight: bold">d'ECAM PLACAGES</span>
                </div>
                <br />
                <div>
                  Nous avons aussi assiste a l'operation du scellemet les douaniers en
                  charge
                </div>
                <br />
                <div>
                  Avec le plomb N :
                  <span style="font-weight: bold"><%=embarquement.getNumPlomb()%></span>
                  <div class="hr" style="width: 90%; margin-left: 8.4em"></div>
                </div>
                <br />
                <div>
                  Contrat N : <span style="font-weight: bold">[<%=embarquement.getRefClient().getNomClient()%>]</span>
                  <div class="hr"></div>
                </div>
                <br />
                <div>
                  Port d'embarquement : <span style="font-weight: bold"><%=embarquement.getPortDepart()+"[CAMEROUN]"%></span>
                  <div class="hr"></div>
                </div>
                <br />
                <div>
                  Port de debarquement : <span style="font-weight: bold"><%=embarquement.getPortDestination()+"["+embarquement.getPaysDestination()%>]</span>
                  <div class="hr"></div>
                </div>
                <br />
                <div>
                  Le present certificat a ete etabli pour servir et valoir ce que de
                  droit./-
                </div>
                <br />
              </div>
                <table id="pied" class="table " style="width: 100%; height: 100px;">
                    <tbody style="width:100%;">
                        <tr style="width: 100%">
                              <td style="font-weight: bold; width: 33%; text-decoration-line: underline">Demandeur</td>
                              <td style="font-weight: bold; width: 30%; text-decoration-line: underline">Ecoreur</td>
                              <td style="font-weight: bold; width: 30%; text-decoration-line: underline">Le Commandant</td>
                          </tr>
                    </tbody>
                </table>
              <div style="text-align: right">
                <p>Fait a Mbalmayo le <span style="font-weight: bold"><%=String.format("%02d", date.get(Calendar.DATE))+"/"+String.format("%02d", (date.get(Calendar.MONTH)+1))+"/"+String.format("%02d", (date.get(Calendar.YEAR)))%></span></p>
              </div>
        </div>
                                
        <div style="width: 100%; font-size: 14px; padding-top: 50px; margin-top: 500px;" id="rapport-empotage" class="saut-page">
            <div id="entete-bordereau" class="container">
                <div style="font-size: 14px;" class="pull-left" >
                    <p>MINISTERE DES FORETS ET DE LA FAUNE</p>
                    <div class="hr"></div>
                    <p>DELEGATION REGIONALE DU CENTRE</p>
                    <div class="hr"></div>
                    <p>DELEGATION DEPARTEMENTALE DU NYONG-ET-SO'O</p>
                    <div class="hr"></div>
                    <p>
                      SECTION DEPARTEMENTALE DE LA TRANSFORMATION
                      <br />
                      DE LA PROMOTION DES PRODUITS FORESTIERS
                    </p>
                </div>
                <div class="pull-right" >
                    <p style="text-align: center">REPUBLIQUE DU CAMEROUN</p>
                    <p style=";text-align: center">Paix-Travail-Patrie</p>
                </div>
              </div>
            <div id="num-dossier" class="row" style="text-align: center">
                
                <span style="font-style: italic; font-weight: normal">
                  N Dossier :
                  <span style="font-weight: bold"><input type="text" id="zoneCode"></span>/MINFOF/DPCE/DDOF-NS/EP/<%=Calendar.getInstance().get(Calendar.YEAR)%></span>
              </div>
              <h4 style="text-align: center; text-decoration-line: underline">
                RAPPORT D'EMPOTAGE
              </h4>

              <div id="rapport" style="text-align: justify">
                <div>
                  Nous Soussignés
                  <span style="font-weight: bold" style="margin-left: 1em">
                      <select style="width: 400px;" class="no-print select2" id="zoneNomForet"></select>
                      <span id="spanNomForet" style="font-weight: bold"></span></span>
                  <div class="hr" style="width: 90%; margin-left: 120px"></div>
                </div>
                <div>
                  (Noms, Prenoms, Grades) des corps des Eaux et Foret en service a Mbalmayo
                </div>
                <div>
                    Assiste de :
                    <span style="font-weight: bold" style="margin-left: 1em">
                        <select style="width: 400px;" class="no-print select2" id="zoneNomForetAssistant"></select>
                        <span id="spanNomForetAssistant" style="font-weight: bold"></span></span>
                    <div class="hr" style="width: 90%; margin-left: 120px"></div>
                    <div class="hr" style="width: 92%; margin-left: 80px"></div>
                </div><br />
                <p>Certifie (certifions) avoir :</p>
                <div>
                  Assiste ce jour
                  <span
                    style="font-weight: bold"
                    style="padding-right: 35px; border-bottom: solid 1px black"
                    ><%=Calendar.getInstance().get(Calendar.DATE)%>/<%=Calendar.getInstance().get(Calendar.MONTH)%>/<%=Calendar.getInstance().get(Calendar.YEAR)%></span
                  >
                  à l'usine
                  <span style="font-weight: bold">d'ECAM PLACAGES - MBALMAYO</span>
                </div>
                <p>
                  Sur demande
                  <span style="font-weight: bold">d'ECAM PLACAGES</span>
                </p>
                <p>
                  (Exportateur) à l'empotage du (des) container(s) N
                  <span
                    style="font-weight: bold"
                    style="padding-right: 35px; border-bottom: solid 1px black"
                    ><%=embarquement.getNumConteneur().getNumConteneur()%>
                  </span>
                </p>
                <br />
                <div>
                  Reconnu et constate
                  <span style="font-weight: bold">
                      [<%=embarquement.getNbrColis()%> COLIS DE BOIS EN PLACAGES M3:<%=df.format(volumeTotal)%> - <%=poidsTotal/1000 >= 1 ? Math.floor(poidsTotal/1000)+"TONNE(S)"+(poidsTotal%1000)+"KILOGRAMMES": poidsTotal+" KILOGRAMMES"%>]
                    ->
                  </span>
                  <div class="hr" style="width: 90%; margin-left: 8.4em"></div>
                  <br />
                  <span style="font-weight: bold"><%=embarquement.getNbrColis()%> COLIS DE BOIS EN PLACAGES </span>
                  <div class="hr"></div>
                  <span style="font-weight: bold" style="padding-left: 25%;"> CUBANT: <%out.print(df.format(volumeTotal));%></span
                  ><span style="font-weight: bold" style="padding-left: 12%;">
                      VOIR DÉTAILS: <%=embarquement.getLettreCamion()%> du <%=Calendar.getInstance().get(Calendar.DATE)%>/<%=Calendar.getInstance().get(Calendar.MONTH)%>/<%=Calendar.getInstance().get(Calendar.YEAR)%> annexée au présent rapport.</span
                  >
                  <span
                    >(Nombre de colis, désignation commerciale, marque et n des colis,
                    volume)</span
                  >
                </div>
                <span>En presence de : </span>
                <br />
                <div>
                  Mr./Mme/Mlle
                  <span style="font-weight: bold" style="width: 90%; border-bottom: solid 1px black">[<span class="user-nom"></span>] [<span class="user-fonction"></span>]</span>
                </div>
                <br />
                <div>
                    (<span class="user-fonction"></span>) agissant au nom et pour compte
                  <span style="font-weight: bold">d'ECAM PLACAGES</span>
                </div>
                <br />
                <div>
                  Nous avons aussi assiste a l'operation du scellemet les douaniers en
                  charge
                </div>
                <br />
                <div>
                  Avec le plomb N :
                  <span style="font-weight: bold"><%=embarquement.getNumPlomb()%></span>
                  <div class="hr" style="width: 90%; margin-left: 8.4em"></div>
                </div>
                <br />
                <div>
                  Contrat N : <span style="font-weight: bold">[<%=embarquement.getRefClient().getNomClient()%>]</span>
                  <div class="hr"></div>
                </div>
                <div>
                  Port d'embarquement : <span style="font-weight: bold"><%=embarquement.getPortDepart()+"[CAMEROUN]"%></span>
                  <div class="hr"></div>
                </div>
                <br />
                <div>
                  Port de debarquement : <span style="font-weight: bold"><%=embarquement.getPortDestination()+"["+embarquement.getPaysDestination()%>]</span>
                  <div class="hr"></div>
                </div>
                <div>
                  Le present certificat a ete etabli pour servir et valoir ce que de
                  droit./-
                </div>
              </div>
                <table id="pied2" class="table " style="width: 100%; height: 100px;">
                    <tbody style="width:100%;">
                        <tr style="width: 100%">
                              <td style="font-weight: bold; width: 33%; text-decoration-line: underline">Demandeur</td>
                              <td style="font-weight: bold; width: 30%; text-decoration-line: underline">Ecoreur</td>
                              <td style="font-weight: bold; width: 30%; text-decoration-line: underline">Le Commandant</td>
                          </tr>
                    </tbody>
                </table>
              <div style="text-align: right;" class="pull-right">
                <p>Fait a Mbalmayo le <span style="font-weight: bold"><%=String.format("%02d", date.get(Calendar.DATE))+"/"+String.format("%02d", (date.get(Calendar.MONTH)+1))+"/"+String.format("%02d", (date.get(Calendar.YEAR)))%></span></p>
              </div>
            <table class="to-print" style="position: absolute; bottom: 0; width: 95%; border: none;">
                <tbody>
                        <tr>
                            <td>
                                <img src="/resources/image/bois.png" style="width: 300px; height: 80px; margin-bottom: 50px;">
                            </td>
                        </tr>
                </tbody>
            </table>
              
        </div>
        
        <div class="row">
          <button style="" onclick="window.print()" type="button" id="btn-imprimer no-print" class="pull-left btn btn-primary no-print">
              <i class="fa fa-print "></i> Imprimer
          </button><br>

        </div>
                                
    <script src="/resources/js/my/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="/resources/css/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    
    <script src="/resources/js/dataTables.bootstrap.min.js"></script>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. -->
        <script type="text/javascript">
            window.onload = function(){
                setTimeout(function(){
                    window.stop();
                    
                    var user = JSON.parse(localStorage.getItem("user"));
                    $('.user-nom').html(user.nom+" "+user.prenom);
                    $('.user-fonction').html(user.fonction.libelle);
                    $('#zoneNomDouanier').change(function(){
                        $('#spanNomDouanier').html($(this).val());
                    });
                    $('#zoneNomForet').change(function(){
                        $('#spanNomForet').html(this.value);
                    });
                    $('#zoneNomForetAssistant').change(function(){
                        $('#spanNomForetAssistant').html(this.value);
                    });
                    $('#zoneCode').change(function(){
                        $('#spanCode').html(this.value);
                    });
                    $('#zoneCode1').change(function(){
                        $('#spanCode1').html(this.value);
                    });
                }, 2000);
                var max = 1200;
                var a, b;
                a = parseInt($('#zone-bordoreau').css('height'));
                b = max*(Math.floor(a/max)+1)-a-150;
                $('#grapper-bordoreau').css('height', b);
                a = parseInt($('#fiche-tracabilite').css('height'));
                b = max*(Math.floor(a/max)+1)-a-50;
                $('#grapper-tracabilite').css('height', b);
                a = parseInt($('#fiche-specification').css('height'));
                b = max*(Math.floor(a/max)+1)-a;
                $('#grapper-specification').css('height', b);
                
                
                 function getAgent(){

                    $.ajax({
                        url: '/api/douanier/list',
                        method: 'GET',
                        complete: function (jqXHR, textStatus ) {

                            var selectPA1 = document.getElementById('zoneNomForet');
                            var selectPA2 = document.getElementById('zoneNomForetAssistant');
                            var data = jqXHR.responseJSON;
                            var option = document.createElement("option");
                            selectPA1.innerHTML = '';
                            selectPA2.innerHTML = '';
                            option.innerHTML = '-- Selectionnez le douanier --';
                            option.value = '';
                            selectPA1.appendChild(option);
                            selectPA2.appendChild(option.cloneNode(true));

                            for(var i=0; i<data.length; i++){
                                if(data[i].suspendu === true)
                                    continue;
                                option = document.createElement("option");
                                option.value = '[ '+data[i].matricule+" ] "+ data[i].nom+" "+data[i].prenom+" ";
                                option.innerText = data[i].nom+" "+data[i].prenom+" ";
                                selectPA1.appendChild(option);
                                selectPA2.appendChild(option.cloneNode(true));
                            }
                            //$(selectPA).select2();
                        }
                    });
                };
                
                
                 function getDouanier(){

                    $.ajax({
                        url: '/api/douanier/list',
                        method: 'GET',
                        complete: function (jqXHR, textStatus ) {

                            var selectPA = document.getElementById('zoneNomDouanier');
                            var data = jqXHR.responseJSON;
                            var option = document.createElement("option");
                            selectPA.innerHTML = '';
                            option.innerHTML = '-- Selectionnez le douanier --';
                            option.value = '';
                            selectPA.appendChild(option);

                            for(var i=0; i<data.length; i++){
                                if(data[i].suspendu === true)
                                    continue;
                                option = document.createElement("option");
                                option.value = '[ '+data[i].matricule+" ] "+ data[i].nom+" "+data[i].prenom+" ";
                                option.innerText = data[i].nom+" "+data[i].prenom+" ";
                                selectPA.appendChild(option);
                            }
                            //$(selectPA).select2();
                        }
                    });
                };
                
                getDouanier();
                getAgent();
            };
            
            
        </script>
    </body>
</html>

<% }else{
%>
        <h1>Impossible d'afficher cette page</h1>
        <h1><button type="button" onclick="history.back()">Cliquez ici</button> pour retourner</h1>
    <%
}%>