
<!DOCTYPE html>
<%

%>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/resources/css/bootstrap/dist/css/bootstrap.min.css">
	<title>ECAM Specification</title>
	<style>
		@page{
			margin: 0.5cm;
			font-size: 8pt;
		}
		@media print{
			tr>td{
				margin: 0px;
				padding: 0px;
				font-size: 10pt;
			}
		}
	</style>
</head>
<body style="color: #123456">
    <div class="container" style="padding: 40px; margin-top: 40px; margin-bottom: 40px; border: 2px #123456 solid; border-radius: 20px;">
        <table  style="font-weight: bold; font-size: 8pt; position: relative; width: 100%; color: #123456; padding-left: 10px;">
            <tbody>
                            <tr>
                                    <td style="font-size: 10pt; text-align: center; font-size: 10pt;" colspan="3">****************************************************</td>
                            </tr>
                            <tr>
                                <td style="font-size: 10pt; text-align: center; font-size: 14pt;" colspan="3">RENDEMENT GRUMES</td>
                            </tr>
                            <tr>
                                    <td style="font-size: 10pt; text-align: center;" colspan="3">-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </td>
                            </tr>
                            <tr>
                                    <td style="font-size: 10pt; width: 25%;" ></td>
                                    <td style="font-size: 10pt;" >P�riode du: </td>
                                    <td style="text-align: center; text-align: left" colspan="2"><input type="date" style="border: none;" id="date-debut"> au: <input  style="border: none;" type="date" id="date-fin"></td>
                            </tr>
                            <tr>
                                    <td ></td>
                                    <td></td>
                                    <td></td>
                            </tr>
                            <tr>
                                    <td>.</td>
                                    <td >Option</td>
                                    <td style="text-align: left">:[�tat Global]</td>
                            </tr>
                            <tr>
                                    <td >.</td>
                                    <td >Dimensions</td>
                                    <td style="text-align: left">:[Achats R�fractionn�s ou A Facturer</td>
                            </tr>
                            <tr>
                                    <td >.</td>
                                    <td >Date utilis�e</td>
                                    <td style="text-align: left">:[Entr�es aux Parcs]</td>
                            </tr>
            </tbody>
        </table><br><br>
    </div>
        <table class="table table-bordered table-striped" border="1" style="border-color: black; text-align: center; position: relative; font-size: 5pt; width: 100%; ">
		<thead>
			<tr style="height: 30px;">
                            <th style="text-align: center; font-size: 10pt">Essences</th>
                            <th style="text-align: center; font-size: 10pt">Cubage[m�]</th>
                            <th style="text-align: center; font-size: 10pt">Sortie[m�]</th>
                            <th style="text-align: center; font-size: 10pt">Sortie[m�]</th>
			</tr>
		</thead>
                <tbody id="data">
			<tr>
				<td></td>
                                <td></td>
                                <td></td>
                                <td></td>
			</tr>
			<tr>
				<td></td>
                                <td></td>
                                <td></td>
                                <td></td>
			</tr>
			<tr>
				<td></td>
                                <td></td>
                                <td></td>
                                <td></td>
			</tr>
			<tr>
				<td></td>
                                <td></td>
                                <td></td>
                                <td></td>
			</tr>
			<tr>
				<td></td>
                                <td></td>
                                <td></td>
                                <td></td>
			</tr>
			<tr>
				<td></td>
                                <td></td>
                                <td></td>
                                <td></td>
			</tr>
			<tr>
				<td></td>
                                <td></td>
                                <td></td>
                                <td></td>
			</tr>
			<tr>
				<td></td>
                                <td></td>
                                <td></td>
                                <td></td>
			</tr>
			<tr>
				<td></td>
                                <td></td>
                                <td></td>
                                <td></td>
			</tr>
			<tr>
				<td></td>
                                <td></td>
                                <td></td>
                                <td></td>
			</tr>
			<tr>
				<td></td>
                                <td></td>
                                <td></td>
                                <td></td>
			</tr>
			<tr>
				<td></td>
                                <td></td>
                                <td></td>
                                <td></td>
			</tr>
		</tbody>

        <script src="/resources/js/my/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="/resources/css/bootstrap/dist/js/bootstrap.min.js"></script>
        
        <script>
        
        function parseDate(dateStringify = ''){
            console.log(dateStringify);
            var date = dateStringify.split('-');
            // //console.log(date);
            return new Date(parseInt(date[0]), parseInt(date[1]), parseInt(date[2]));
        }
        
    
    $('#date-debut, #date-fin').change(function(){
        console.log('debut = '+$('#date-debut').val()+" fin = "+$('#date-fin').val());
            if($('#date-debut').val() ==='' || $('#date-fin').val() === '')
                return;
            

            let dateD = parseDate($('#date-debut').val()).getTime(); 
            let dateF = parseDate($('#date-fin').val()).getTime(); 



            $.ajax({
               url: '/api/essence/check',
               data: {
                   dd: dateD,
                   df: dateF,
               },
               dataType: 'json',
               async: false,
               complete: function (jqXHR, textStatus ) {
                   let list = jqXHR.responseJSON;
                   $('#data').html("");
                   for(let i=0; i<list.length; i++){
                       $('#data').html($('#data').html()+'<tr><td>'+list[i].libelle+'</td><td>'+parseFloat(list[i].volume).toFixed(3)+'</td><td>'+parseFloat(list[i].parc.volume).toFixed(3)+'</td><td>'+parseFloat(list[i].parc.surface).toFixed(3)+'</td></tr>');
                   }
                }

           });
            });
        </script>
</body>
</html>