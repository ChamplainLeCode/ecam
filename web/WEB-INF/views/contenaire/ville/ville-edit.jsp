<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div id="form-edit-personnel">
    <div class="register-box-body">
          <h2 class="login-box-msg">Modifier un compte </h2>

        <form   action="#">
          <div class="form-group has-feedback">
              <legend><h4>Code <span class="" style="color:red; font-size: 10pt;" id="status"></span></h4></legend>
              <input type="text" id="zone-code" required="" value="<%=request.getParameter("ref")%>" readonly=""  class="form-control" placeholder="Nom ">
            <span class="fa fa-ship form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
              <legend><h4>Ville <span class="" style="color:red; font-size: 10pt;" id="status"></span></h4></legend>
              <input type="text" id="zone-nom" required="" value="<%=request.getParameter("nom")%>"  class="form-control" placeholder="Nom ">
            <span class="fa fa-ship form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
              <legend><h4>Pays <span class="" style="color:red; font-size: 10pt;" id="status"></span></h4></legend>
              <input type="text" id="zone-pays" required="" value="<%=request.getParameter("pays").equalsIgnoreCase("null") ? "" : request.getParameter("pays")%>" class="form-control" placeholder="Pays ">
            <span class="fa fa-ship form-control-feedback"></span>
          </div>
          <div class="row">
              <!-- /.col -->
            <div class="col-xs-4 pull-left">
                <button type="btn" data-dismiss="modal"  id="btn-close-editPane" class="btn form-control btn-block btn-flat">Fermer</button>
            </div>
            <div class="pull-right col-xs-4">
                <button type="button" id="btn-save-ville" class="btn btn-primary form-control btn-block btn-flat">Enregistrer</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
      </div>

    
</div>
<script type="text/javascript">
        $('#btn-save-ville').click(function(){
            if($('#zone-nom').val().length === 0){
                $('#status').html('Veuillez indiquer l\'ville');
                return;
            }
            if($('#zone-code').val().length === 0){
                $('#status').html('Contactez l\'administrateur');
                return;
            }
            $.ajax({
                url: '/ville/edit',
                method: 'POST',
                async: false,
                data: {ref: $('#zone-code').val(), nom: $('#zone-nom').val(), pays: $('#zone-pays').val()},
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    $('#btn-close-editPane').click();
                    loadData();
                }
            });
            return false;
        });
    </script>
