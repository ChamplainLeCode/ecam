<%-- 
    Document   : contenaire-home
    Created on : 6 févr. 2019, 13:29:53
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Ecam Web</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

      <%@include file="/WEB-INF/views/layouts/style.jsp" %>
      
    </head>
    <!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
    -->
    <body class="hold-transition skin-blue sidebar-collapse">
    <div class="wrapper">

      <!-- Main Header -->
      <%@include file="/WEB-INF/views/layouts/topBar.jsp" %>

      <!-- Left side column. contains the logo and sidebar -->
      <%@include file="/WEB-INF/views/layouts/sideBar.jsp" %>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Page Header
            <small>Optional description</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="">ecam</li>
            <li class="active">contenaire</li>
          </ol>
          
          
        </section>

        <!-- Main content -->
        <section class="content container-fluid">

            <div class="row">
                
                <div class="modal fade modal-default" id="modal-default" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close btn-flat" style="background-color: transparent; color: black" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true" class="fa fa-close" style="color: black;"></span></button>
                                <h4 class="modal-title">Ajouter un nouveau Conteneur</h4>
                            </div>
                            <div class="modal-body" id="modal-body">

                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                
                <div class="modal fade modal-primary" id="modal-edit" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close btn-flat" style="background-color: transparent;" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true" style="color: white">×</span></button>
                                <h4 class="modal-title">Modifier Conteneur</h4>
                            </div>
                            <div class="modal-body" id="modal-body">

                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                
                <div class="modal fade " id="modal-view" style="display: none; background-color: transparent; background: none;">
                    <div class="modal-dialog pull-right col-sm-pull-0" style="width: 100%">
                        <div class="modal-content col-sm-12 col-md-12 col-lg-12 col-xs-12 col-sm-offset-0 col-md-offset-0 col-lg-offset-0" style=" background-color: transparent; background: none;">
                            <div class="modal-header">
                                
                                    <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-view-commande_client" class="hide" data-toggle="modal" data-target="#modal-view"></button>
                            </div>
                            <div class="modal-body" id="modal-body" style=" background-color: transparent ">
                                <div class="box box-widget widget-user-2">
                                        <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-navy">
                                        <!-- /.widget-user-image -->
                                        <h4 class="widget-user-desc">N° Contenaire <span id="vzone-ref" style="margin-left:100px;" ></span></h4>
                                        <!--h5 class="widget-user-desc" >Client <span id="vzone-fournisseur" style="margin-left:100px;" ></span></h5-->
                                        <h5 class="widget-user-desc ">Client: <span  id="vzone-client" style="margin-left:100px;" ></span></h5>
                                        <h5 class="widget-user-desc">Chauffeur <span  id="vzone-chauffeur" style="margin-left:100px;" ></span></h5>
                                        <h5 class="widget-user-desc">Compagnie <span  id="vzone-compagnie" style="margin-left:100px;" ></span></h5>
                                        <h5 class="widget-user-desc">Transporteur <span  id="vzone-transporteur" style="margin-left:100px;" ></span></h5>
                                        <h5 class="widget-user-desc">Immatriculation - Pays: <span  id="vzone-immatriculation" style="margin-left:100px;" ></span></h5>
                                        <h5 class="widget-user-desc">Ville - Pays: <span  id="vzone-type" style="margin-left:100px;" ></span></h5>
                                        <h5 class="widget-user-desc">Date: <span  id="vzone-date" style="margin-left:100px;" ></span></h5>
                                        <h5 class="widget-user-desc">Volume: <span  id="vzone-volume" style="margin-left:100px;" ></span></h5>
                                        <h5 class="widget-user-desc">Volume Brut: <span  id="vzone-volume-brute" style="margin-left:100px;" ></span></h5>
                                        <h5 class="widget-user-desc">Surface: <span  id="vzone-surface" style="margin-left:100px;" ></span></h5>
                                        <h5 class="widget-user-desc">Nbr Colis <span  id="vzone-nbr-colis" style="margin-left:100px;" ></span></h5>
                                        <h5 class="widget-user-desc">Nbr Paquets <span  id="vzone-nbr-paquets" style="margin-left:100px;" ></span></h5>
                                    </div>
                                    <div class="box-footer no-padding" style="margin-top: 25px">
                                        <ul class="nav nav-stacked" style="">
                                            <li><div class="box box-success">
                                                    <div class="box-header with-border">
                                                      <h3 class="box-title">Info. Contenu</h3>

                                                      <div class="box-tools pull-right">
                                                          <button type="button" class="btn btn-flat" >
                                                              <i class="fa fa-caret-down"></i>
                                                              <span style="margin-left: 10px;" class="pull-right badge bg-green" id="vzone-nbr-detail"></span>
                                                        </button>
                                                      </div>
                                                      <!-- /.box-tools -->
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div id="vzone-detail" style="padding: 30px;" class="box-body">
                                                        <table id="liste-view" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="liste_info">
                                                            <thead>
                                                                <tr role="row">
                                                                    <th style="text-align: center;">Reférence</th>
                                                                    <th>NbrePaquet</th>
                                                                    <th>LongueurColis</th>
                                                                    <th>LargeurColis</th>
                                                                    <th>PoidsColis</th>
                                                                    <th>Equipe</th>
                                                                    <th>Surface [m²]</th>
                                                                    <th>Volume [m³]</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="table-view-detail-contenaire"></tbody>
                                                        </table>

                                                    </div>
                                                    <!-- /.box-body -->
                                                </div>
                                            </li>
                                            <li><button type="button" class=" btn btn-flat pull-right" style="background-color: transparent; margin: 5px;" data-dismiss="modal"> Fermer</button></li>
                                            <li><button type="button" id="btn-print-detail" class=" btn btn-bitbucket pull-left" style="margin: 5px;" ><a target="_blank" style="text-decoration: none; color: white;"> Imprimer Rendu Spécification</a></button></li>
                                            <li><button type="button" class=" btn btn-success pull-left" style="margin: 5px;" ><a target="_blank" style="text-decoration: none; color: white;" id="vzone-lien"> Imprimer Rapport Empotage</a></button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

                
                <p class="status-del-contenaire pull-left" style="padding: 5px; border-radius: 5px;"></p>

                <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-addContenaire" class="pull-right btn btn-default" data-toggle="modal" data-target="#modal-default">
                    <i class="fa fa-plus "></i> Ajouter Embarcation
                </button>
                
                <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-imprimer" class="pull-left btn btn-primary">
                    <i class="fa fa-print "></i> Imprimer la liste
                </button>
            </div>
            <div class="box">
            <div class="box-header">
              <h3 class="box-title">Liste des Embarcations</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div id="liste_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-12" style="overflow-x: auto;">
                            <table id="liste" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="liste_info">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc">N° Conteneur</th>
                                        <th >N° Plomb</th>
                                        <th >DateChargement</th>
                                        <th >Pays - Ville</th>
                                        <th >Volume [m³]</th>
                                        <th >Surface [m²]</th>
                                        <th >Nbr Colis</th>
                                        <th >Nbr Paquets</th>
                                        <th >Epaisseur</th>
                                        <th >Essence</th>
                                        <th >Qualite</th>
                                        <th >Opérations</th>
                                    </tr>
                                </thead>
                                <tbody id="table-body"></tbody>
                          </table>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            
        </section>
        
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <%@include file="/WEB-INF/views/layouts/footer.jsp" %>

    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->

    <%@include file="/WEB-INF/views/layouts/script.jsp" %>
    
    <script src="/resources/js/jquery.dataTables.min.js"></script>
    <script src="/resources/js/dataTables.bootstrap.min.js"></script>
    <script src="/resources/js/codeBarre/JsBarcode.all.min.js"></script>
    <script src="/resources/js/contenaire/contenaire-home.js"></script>
    <script>
        $(function () {
            loadData();
            
            document.getElementById("btn-imprimer").onclick = function(){
                tableToExcel('liste', 'W3C Example Table');
            };
            
            document.getElementById("btn-print-detail").onclick = function(){
//                console.log("/contenaire/specification?plomb="+this.getAttribute('data'));
//                window.open("/contenaire/specification?plomb="+this.getAttribute('data'), "ECAM PLACAGE | specification", "directories=no,titlebar=no,status=no,menubar=no,scrollbars=yes,resizable=no,width="+screen.width+",height="+screen.height);
            };
        });
    </script>
    </body>
</html>
