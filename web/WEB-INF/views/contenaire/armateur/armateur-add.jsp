<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-armateur">
    <p class="status-edit-armateur" style="padding: 5px; border-radius: 5px; color: red;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Armateur</label>

          <div class="col-sm-10">
              <input required="" class="form-control" multiple="" style="width: 100%;" id="zone-armateur">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-add-armateur"><i class="fa fa-pencil"></i> Ajouter</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        
        
        $("#btn-add-armateur").click(function(){
            if($('#zone-armateur').val() === ''){
                $('.status-edit-armateur').html('Veuillez entrer l\'armateur');
                return;
            }
            saveData($('#zone-armateur').val());
            $('#form-edit-armateur .overlay').removeClass('hide');
            $('#form-edit-armateur #btn-add-armateur').addClass("hide");
        });
        $('select').select2();
    </script>
</div>
