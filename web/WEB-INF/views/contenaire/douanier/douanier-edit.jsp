<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div id="form-edit-personnel">
    <div class="register-box-body">
             <h2 class="login-box-msg">Modifier un pays </h2>

        <form   action="#">
            
            <div class="form-group has-feedback">
                <legend><h4>Matricule <span class="" style="color:red; font-size: 10pt;" id="status-matricule"></span></h4></legend>
                <input type="text" id="zone-matricule" required="" value="<%=request.getParameter("matricule")%>" readonly=""  class="form-control" placeholder="Matricule du douanier">
              <span class="fa fa-ship form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <legend><h4>Nom du Douanier <span class="" style="color:red; font-size: 10pt;" id="status-nom"></span></h4></legend>
                <input type="text" id="zone-nom" required="" value="<%=request.getParameter("nom")%>" class="form-control" placeholder="Nom du douanier ">
              <span class="fa fa-ship form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <legend><h4>Prénom du douanier <span class="" style="color:red; font-size: 10pt;" id="status-prenom"></span></h4></legend>
                <input type="text" id="zone-prenom" required="" value="<%=request.getParameter("prenom")%>" class="form-control" placeholder="Prénom du douanier">
              <span class="fa fa-ship form-control-feedback"></span>
            </div>
          <div class="row">
              <!-- /.col -->
            <div class="col-xs-4 pull-left">
                <button type="btn" data-dismiss="modal"  id="btn-close-editPane" class="btn form-control btn-block btn-flat">Fermer</button>
            </div>
            <div class="pull-right col-xs-4">
                <button type="button" id="btn-save-pays" class="btn btn-primary form-control btn-block btn-flat">Enregistrer</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
      </div>

    
</div>
<script type="text/javascript">
        $('#btn-save-pays').click(function(){
            var t = false;
            
            if($('#zone-matricule').val().length === 0){
                $('#status-matricule').html('Contactez L\'administrateur');
                t = true;
            }else $('#status-matricule').html('');
            
            if($('#zone-nom').val().length === 0){
                $('#status-nom').html('Veuillez indiquer le nom du douanier');
                t = true;
            }else $('#status-nom').html('');
            
            if($('#zone-prenom').val().length === 0){
                $('#status-prenom').html('Veuillez indiquer le nom de la prenom');
                t = true;
            }else $('#status-prenom').html('');
            
            
            if(t === true)
                return;
            $.ajax({
                url: '/douanier/edit',
                method: 'POST',
                async: false,
                data: {matricule: $('#zone-matricule').val(), nom: $('#zone-nom').val(), prenom: $('#zone-prenom').val()},
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    $('#btn-close-editPane').click();
                    loadData();
                }
            });
            return false;
        });
    </script>
