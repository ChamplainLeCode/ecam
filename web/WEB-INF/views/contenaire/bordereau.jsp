<%@page import="app.models.Colis"%>
<%@page import="java.util.Calendar"%>
<%@page import="app.models.Embarquement"%>
<%
  
    Embarquement embarquement = (Embarquement)request.getAttribute("embarquement");
    Calendar date = Calendar.getInstance();
    if(embarquement != null){
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <html>
  <head>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        
                <link rel="stylesheet" href="/resources/css/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
  <style>
            @page 
            {
                size: auto;   /* auto is the current printer page size */
                margin-top: 0mm;
                margin-bottom: 0mm;
                font-size: 8pt;
                padding-bottom: 0mm;/* this affects the margin in the printer settings */
            }

    @media print {
            .to-print{
                visibility: visible;
            }
            .no-print{
                display: none;
            }
            .print{
                color: #1234A0;
            }
            .saut-page{
            }
                
            .saut-page
            {
                /* saut de page apres le bloc*/
                page-break-after : always;
                /* saut de page avant le bloc*/
                page-break-before : always;
                
                font-size: 8pt;
                height: 100%;
                min-height: 720px;
                position: relative;
                width: 100%;
                background: url(/resources/image/bois.png);
                background-position-x: 2%;
                background-position-y: 100%;
                background-size: 300px 100px;
                background-repeat: no-repeat;
            }
        } 
            .value{
                font-size: 8pt;
                font-weight: normal;
                margin-left: 20px;
            }
            .saut-page
            {
                /* saut de page apres le bloc*/
                page-break-after : always;
                /* saut de page avant le bloc*/
                page-break-before : auto;
                
                font-size: 5pt;
                height: 100%;
                min-height: 720px;
                position: relative;
                width: 100%;
                
                margin-top: 400px;
                margin-bottom: 400px;
            }
            .logo-bois{
                float: left;
                position: relative;
                margin-bottom: 100px;
                height: 100px;
                page-break-after: always;
            }
            @page  {
                size: A4;
                @top-center { content: "Preliminary edition" };
                @bottom-center { content: counter(page) };
                
            }
            input{
                border: none;
            }
        </style>
        <title>ECAM PLACAGES | BORDEREAU SPECIFICATION</title>
  </head>
  <body style="font-size: 8pt;">
  
        <div style="background-color: white; padding: 20px;">
            <div style="background: white; margin-top: 30px;" >

                <div class="row" style="margin-bottom: 10px;">
                        <div class="pull-right" style="font-size: 5pt; float: right; margin-top: 0px; background: white; border: black 1px; border-radius: 10px; text-align: center; ">
                            <p style="font-weight: bold; font-size: 5pt; margin-bottom: 5px;">RÉPUBLIQUE DU CAMEROUN</p>
                            <p style="font-weight: bold; margin-bottom: 5px; text-align: center">Paix-Travail-Patrie</p>
                        </div>
                        <div class="pull-left" style="font-size: 5pt; float:left; background: white; border-radius: 10px; text-align: center; ">
                            <p style="font-weight: bold;">MINISTÈRE <span style="text-decoration: black dashed underline ">DES  FORETS ET</span> DE LA FAUNE</p>
                            <p style="font-weight: bold; margin-bottom: 10px;">DÉLÉGATION RÉGIONALE DU CENTRE</p>
                            <p style="font-weight: bold; margin-bottom: 10px; ">DÉLÉGATION DÉPARTEMENT DU NYONG-ET-SO</p>
                            <p style="font-weight: bold; margin-bottom: 10px; ">SECTION DÉPARTEMENTALE DE LA<br> TRANSFORMATION ET DE LA 
                                PROMOTION<br> DES PRODUITS FORESTIERS
                            </p>
                        </div>
                </div>

                <div class="row">
                    <p style="font-weight: bold; font-size: 8pt; margin-bottom: 5px; text-align: center;">N° Dossier : <input style="width: 30" type="text"> / <%=String.format("%04d", (date.get(Calendar.YEAR)))%></p>
                </div>
            </div>
        </div>
        <div class="row" style="border-radius: 10px; margin: 20px;">     
            <table class="table" style="border: none; border-color: transparent; font-size: 5pt;">
                <thead style="margin-bottom: 20px;">
            <tr>
                <th colspan="2" style="font-weight: normal; font-size: 5pt; ">
                    <span style="font-weight: bold">BORDEREAU DE SPÉCIFICATION DES PRODUITS TRANSPORTÉS </span>N°.................................../BSTP/MINFOF/DDFOF.NS/STP
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="col-sm-6">
                    <label class="courriel">Origine: <span class="value"> Cameroun</span></label>
                </td>
                </td>
                <td class="col-sm-6">
                    <label class="courriel">Expediteur: <span class="value">ECAM PLACAGES</span></label>
                </td>
            </tr>
            <tr>
                <td class="col-sm-6">
                    <label class="courriel">Destination: <span class="value"><%=embarquement.getVilleDestination()+" - "+embarquement.getPaysDestination()%></span></label>
                </td>
                <td class="col-sm-6">
                    <label class="courriel">N° Contribuable: <span class="value"><%=embarquement.getRefCompagnie().getNumContribuable()%></span></label>
                </td>
            </tr>
            <tr>
                <td class="col-sm-6">
                    <label class="courriel">Container N°: <span class="value"><%=embarquement.getNumConteneur().getNumConteneur()%></span></label>
                </td>
                <td class="col-sm-6">
                    <label class="courriel">Transporteur: <span class="value"><%=embarquement.getRefTransporteur().getNom()%></span></label>
                </td>
            </tr>
            <tr>
                <td class="col-sm-6">
                    <label class="courriel">N° Immatriculation: <span class="value"><%=embarquement.getImmatriculation()%></span></label>
                </td>
                <td class="col-sm-6">
                    <label class="courriel">N° Contribuable Transporteur: <span class="value"><%=embarquement.getRefTransporteur().getNumContribuable()%></span></label>
                </td>
            </tr>
            <tr>
                <td class="col-sm-6">
                    <label class="courriel">Plomb N°: <span class="value"><%=embarquement.getNumPlomb()%></span></label>
                </td>
                <td class="col-sm-6">
                    <label class="courriel">Camion: <span class="value"><%=embarquement.getLettreCamion()%></span></label> 
                </td>
            </tr>
            <tr>
                <td class="col-sm-6">
                    <label class="courriel">Chauffeur: <span class="value"><%=embarquement.getRefChauffeur().getNom()%></span></label>
                </td>
                <td class="col-sm-6">
                    <label class="courriel">Type Container: <span class="value"><%=embarquement.getNumConteneur().getRefTypecont().getLibelleTypecontennaire()%></span></label> 
                </td>
            </tr>
        </tbody>
    </table>
        </div>
        <div class="row" style="border-radius: 10px; margin: 20px; font-size: 5pt;">

            <table class="table table-striped table-hover table-condensed">
                <tr>
                    <th>N° Colis</th>
                    <th>Essences</th>
                    <th>Longueur (cm)</th>
                    <th>Lageur(cm)</th>
                    <th>Epaisseur (cm)</th>
                    <th>Volume (m3)</th>
                </tr>
                <% for(Colis c : embarquement.getColisList()) {%>
                <tr>
                    <td><%=c.getRefColis()%></td>
                    <td><%=c.getEssence()%></td>
                    <td><%=c.getLongueurColis()%></td>
                    <td><%=c.getLargeurColis()%></td>
                    <td><%=c.getEpaisseur()%></td>
                    <td><%=Math.round(c.getVoulumeColis())%></td>
                </tr>
                <% }%>
           </table>
        </div>
           <div class="width-full pull-right" style="width: 100%;; font-size: 5pt;">

            <div class="row col-sm-3 pull-right" style="margin-left: 20px; margin-right: 10px; border: black 1px; border-style: solid; ">
                <span style="font-weight: bold;">Fait à Mbalmayo, le </span><span class="zone-date"><%=String.format("%02d", date.get(Calendar.DATE))+"/"+String.format("%02d", (date.get(Calendar.MONTH)+1))+"/"+String.format("%02d", (date.get(Calendar.YEAR)))%></span>
                <hr style="background-color: black; border-color: black;">
                <span style="font-weight: bold; text-align: center"> OPÉRATEUR DE PESÉE</span><br>
                <hr style="background-color: black; border-color: black;"><br><br><br><br><br>
            </div>
            <div class="pull-right col-sm-3" style="text-align: left; font-weight: bold; font-size: 5pt;">
                <p>TOTAL COLIS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <span style="float: right"><%=embarquement.getNbrColis()%></span></p>
                <p>TOTAL ELEMENTS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span style="float: right"><%=embarquement.getNbrPaquets()%></span></p>
                <p>TONNAGE TOTAL &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <span style="float: right" id="zone-masse-total"></span></p>
                <p>VOLUME TOTAL NET&nbsp;&nbsp; :<span style="float: right"><%=embarquement.getVolumeContennaire()%></span></p>
                <p>VOLUME TOTAL BRUT&nbsp;: <span style="float: right"><%=String.format("%.2f", embarquement.getVolumeBrut())%></span></p>
                <p>TOTAL COLIS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span style="float: right"><%=embarquement.getNbrColis()%></span></p>
            </div>
        </div>
        <table class="to-print" style="position: absolute; bottom: 0; width: 95%;">
            <tbody>
                    <tr>
                            <td></td>
                            <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td rowspan="1" style="border: 1px black solid; width: 300px; margin-right: 30px;"><p style="text-align: center;">DIRECTEUR GÉNÉRAL</p><hr style="background-color: black; color: black; height: 1px;"><p><br></p><br/></td>
                    <tr>
                    <tr>
                        <td>
                            <img src="/resources/image/bois.png" style="width: 300px; height: 80px;">
                        </td>
                    </tr>
            </tbody>
	</table>
        <div class="to-print"  style="position: absolute; bottom: 0; width: 95%;">
            <img src="/resources/image/bois.png"  class="image logo-bois pull-left col-sm-3" style="margin-bottom: 100px">
        </div>
  </body>
</html>
<%}%>