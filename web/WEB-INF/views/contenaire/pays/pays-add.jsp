<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-pays">
    <p class="status-edit-pays" style="padding: 5px; border-radius: 5px; color: red;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Code</label>

          <div class="col-sm-10">
              <input required="" class="form-control" style="width: 100%;" id="zone-code">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Pays</label>

          <div class="col-sm-10">
              <input required="" class="form-control" style="width: 100%;" id="zone-pays">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-add-pays"><i class="fa fa-pencil"></i> Ajouter</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        
        
        $("#btn-add-pays").click(function(){
            if($('#zone-pays').val() === ''){
                $('.status-edit-pays').html('Veuillez entrer le nom du pays');
                return;
            }
            if($('#zone-code').val() === ''){
                $('.status-edit-pays').html('Veuillez entrer le code du pays');
                return;
            }
            saveData($('#zone-code').val(), $('#zone-pays').val());
            $('#form-edit-pays .overlay').removeClass('hide');
            $('#form-edit-pays #btn-add-pays').addClass("hide");
        });
        $('select').select2();
    </script>
</div>
