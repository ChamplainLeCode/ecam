<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div id="form-edit-personnel">
    <div class="register-box-body">
             <h2 class="login-box-msg">Modifier un pays </h2>

        <form   action="#">
          <div class="form-group has-feedback">
              <legend><h4>Code & Pays <span class="" style="color:red; font-size: 10pt;" id="status"></span></h4></legend>
              <input type="text" id="zone-code" required="" value="<%=request.getParameter("code")%>" readonly=""  class="form-control" placeholder="Nom ">
            <span class="fa fa-ship form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
              <input type="text" id="zone-libelle" required="" value="<%=request.getParameter("libelle")%>"  class="form-control" placeholder="Pays">
            <span class="glyphicon  form-control-feedback"  style="margin-right: 10px;"></span>
          </div>
          <div class="row">
              <!-- /.col -->
            <div class="col-xs-4 pull-left">
                <button type="btn" data-dismiss="modal"  id="btn-close-editPane" class="btn form-control btn-block btn-flat">Fermer</button>
            </div>
            <div class="pull-right col-xs-4">
                <button type="button" id="btn-save-pays" class="btn btn-primary form-control btn-block btn-flat">Enregistrer</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
      </div>

    
</div>
<script type="text/javascript">
        $('#btn-save-pays').click(function(){
            if($('#zone-libelle').val().length === 0){
                $('#status').html('Veuillez indiquer l\'pays');
                return;
            }
            if($('#zone-code').val().length === 0){
                $('#status').html('Contactez l\'administrateur');
                return;
            }
            $.ajax({
                url: '/pays/edit',
                method: 'POST',
                async: false,
                data: {ref: $('#zone-code').val(), libelle: $('#zone-libelle').val()},
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    $('#btn-close-editPane').click();
                    loadData();
                }
            });
            return false;
        });
    </script>
