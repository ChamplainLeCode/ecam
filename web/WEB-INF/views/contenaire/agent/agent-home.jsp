<!-- 
    Document   : totoPage
    Created on : 8 déc. 2018, 14:42:16
    Author     : champlain
-->

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Ecam Web</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

      <%@include file="/WEB-INF/views/layouts/style.jsp" %>
      <link rel="stylesheet" href="/resources/css/dataTables.bootstrap.min.css">    
      
    </head>
    <!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
    -->
    <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <%@include file="/WEB-INF/views/layouts/topBar.jsp" %>

      <!-- Left side column. contains the logo and sidebar -->
      <%@include file="/WEB-INF/views/layouts/sideBar.jsp" %>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Page Header
            <small>Optional description</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="">ecam</li>
            <li class="active">acces</li>
          </ol>
          
          
        </section>

        <!-- Main content -->
        <section class="content container-fluid">

            <div class="row">
                <div class="modal fade" id="modal-default" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close btn-flat" style="background-color: transparent;" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true" style="color: white">×</span></button>
                                <h4 class="modal-title">Ajouter une nouvelle Agent</h4>
                            </div>
                            <div class="modal-body" id="modal-body">

                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <div class="modal fade" id="modal-edit" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body" id="modal-body">

                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <p class="status-del-acces pull-left" style="padding: 5px; border-radius: 5px;"></p>

                <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-addAgent" class="pull-right btn btn-default" data-toggle="modal" data-target="#modal-default">
                    <i class="fa fa-edit "></i> Ajouter un Agent
                </button>
                
                <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-imprimer" class="pull-left btn btn-primary" >
                    <i class="fa fa-print "></i> Imprimer la liste
                </button>
            </div>
            <div class="box">
            <div class="box-header">
              <h3 class="box-title">Liste des Agent</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div id="liste_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="liste" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="liste_info">
                                <thead>
                                    <tr role="row">
                                        <th>Matricule</th>
                                        <th>Nom</th>
                                        <th>Prenom</th>
                                        <th>État</th>
                                        <th>Opérations</th>
                                    </tr>
                                </thead>
                                <tbody id="table-body"></tbody>
                          </table>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            
        </section>
        
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <%@include file="/WEB-INF/views/layouts/footer.jsp" %>

    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->

    <%@include file="/WEB-INF/views/layouts/script.jsp" %>
    
    <script src="/resources/js/jquery.dataTables.min.js"></script>
    <script src="/resources/js/dataTables.bootstrap.min.js"></script>
    <script src="/resources/js/contenaire/agent-home.js"></script>
    <script src="/resources/js/jquery.dataTables.min.js"></script>
    <script src="/resources/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            loadData();
            
            document.getElementById("btn-imprimer").onclick = function(){
                tableToExcel('liste', 'W3C Example Table');
            };
        });
    </script>
    </body>
</html>
