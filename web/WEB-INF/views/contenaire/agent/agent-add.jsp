<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-agent">
    <p class="status-edit-agent" style="padding: 5px; border-radius: 5px; color: red;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
          <legend><i class="fa fa-user"></i> Agent </legend>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Matricule</label>
          <div class="col-sm-10">
              <input required="" class="form-control" style="width: 100%;" id="zone-matricule">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Nom Agent</label>

          <div class="col-sm-10">
              <input required="" class="form-control" style="width: 100%;" id="zone-nom">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Prénom Agent</label>

          <div class="col-sm-10">
              <input required="" class="form-control" style="width: 100%;" id="zone-prenom">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-add-agent"><i class="fa fa-pencil"></i> Ajouter</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        
        
        $("#btn-add-agent").click(function(){
            if($('#zone-matricule').val() === ''){
                $('.status-edit-agent').html('Veuillez entrer le matricule du agent');
                return;
            }
            if($('#zone-nom').val() === ''){
                $('.status-edit-agent').html('Veuillez entrer le nom du agent');
                return;
            }
            if($('#zone-prenom').val() === ''){
                $('.status-edit-agent').html('Veuillez entrer le prenom du agent');
                return;
            }
            saveData($('#zone-matricule').val(), $('#zone-nom').val(), $('#zone-prenom').val());
            $('#form-edit-agent .overlay').removeClass('hide');
            $('#form-edit-agent #btn-add-agent').addClass("hide");
        });
        $('select').select2();
    </script>
</div>
