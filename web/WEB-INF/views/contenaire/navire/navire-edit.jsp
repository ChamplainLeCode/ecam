<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-navire">
    <p class="status-edit-navire" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Code</label>

          <div class="col-sm-10">
              <input type="text" required="" readonly="" value="<%=request.getParameter("ref")%>" class="form-control" id="zone-ref" placeholder="Rérérence">
          </div>
        </div>
        <div class="form-group">
          <label for="zone- libelle" class="col-sm-2 control-label">Libelle</label>

          <div class="col-sm-10">
              <input type="text" required="" value="<%=request.getParameter("libelle")%>" class="form-control" id="zone-libelle" placeholder="libelle de la Navire">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
          <button type="button"  data-dismiss="modal" id="btn-close" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-navire"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.min.js"></script>
    <script type="text/javascript">
        $("#btn-edit-navire").click(function(){
            editData($('#zone-ref').val(), $('#zone-libelle').val());
            $('#form-edit-navire .overlay').removeClass('hide');
            $('#btn-close').click();
            $('#form-edit-navire #btn-edit-navire').addClass("disabled");
        });
        
    </script>
</div>
