<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="form-add-Navire">
    <p class="status-add-Navire" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
          <legend>Navire</legend>
        <div class="form-group">
          <label for="zone- libelle" class="col-sm-2 control-label">Libelle</label>

          <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="zone-libelle" placeholder="libelle de la Navire">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-save-Navire">Enregistrer</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
    <script type="text/javascript">
        

        $("#zone-ref").val((new Date()).getTime());
        $("#btn-save-Navire").click(function(){
            var data = {
                libelle: $('#zone-libelle').val(),
            };
            save(data, 'navire');
        });
    </script>
</div>
