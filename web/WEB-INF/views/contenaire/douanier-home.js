
/* global user */

$(function(){
   
   $('#btn-addDouanier').click(function(){
      
       $.ajax({
            url: "/douanier/add",
            method: "GET",
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                $('#modal-default #modal-body').html(data);
           },
            error: function (jqXHR, textStatus, errorThrown) {
                 $('#modal-default #modal-body').html(jqXHR.responseText);
            }
       });
   });
    if(user.can_add !== 'O')       
        $('#btn-addArmateur').remove();
   

     function deleteData(data, source){
        $.ajax({
            url: "/douanier/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            complete: function (jqXHR, textStatus) {
                loadData();
            }
        });
    };   

     function suspendre(data, source){
        $.ajax({
            url: "/douanier/edit/suspendre",
            method: "POST",
            data: data,
            dataType: 'json',
            complete: function (jqXHR, textStatus) {
                loadData();
            }
        });
    }

     function unSuspendre(data, source){
        $.ajax({
            url: "/douanier/edit/unSuspendre",
            method: "POST",
            data: data,
            dataType: 'json',
            complete: function (jqXHR, textStatus) {
                loadData();
            }
        });
    }   
   
    function getTableRow(dataRow){
        var row = document.createElement("tr"),
                
        celCode = document.createElement("td"),
        celNom = document.createElement("td"),
        celPrenom = document.createElement("td"),
        celEtat = document.createElement("td"),
        celOpe = document.createElement("td"),
        btnDel = document.createElement("button"),
        btnSus = document.createElement("button"),
        btnEdit = document.createElement("button"),
        icon = document.createElement("i");
        
        //saveData(dataRow.code, dataRow.libelle);
        celCode.innerHTML = dataRow.matricule;
        celNom.innerHTML = dataRow.nom;
        celPrenom.innerHTML = dataRow.prenom;
        celEtat.innerHTML = dataRow.suspendu === true ? "SUSPENDU": "VALIDE";
        
        btnSus.type = "button";
        btnSus.className = dataRow.suspendu === true ? "btn btn-primary" : "btn btn-yahoo";
            icon.className = dataRow.suspendu === true ? "fa fa-check" : "fa fa-remove";
        btnSus.appendChild(icon);
        btnSus.onclick = function(){dataRow.suspendu === true ? unSuspendre(dataRow, this) : suspendre(dataRow, this);};
        btnSus.id = "btn-sus-"+dataRow.code;
        btnSus.style.marginRight = "5px";
        btnSus.title = dataRow.suspendu === true ? 'Annuler la suspension'  :  'Suspendre ' ;
        
        icon = document.createElement("i");
        btnDel.type = "button";
        btnDel.className = "btn btn-warning";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.code;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        icon = document.createElement("i");
        btnEdit.type = "button";
        btnEdit.className = "btn btn-success";
            icon.className = "fa fa-edit";
        btnEdit.setAttribute("data-toggle", "modal");
        btnEdit.setAttribute("data-target", "#modal-edit");
        btnEdit.appendChild(icon);
        btnEdit.onclick = function(){
            $.ajax({
                url: "/douanier/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        btnEdit.id = "btn-del-"+dataRow.matricule;
        btnEdit.style.marginRight = "5px";
        btnEdit.title = 'Modifier ';
        
        
        

        if(user.can_edit === 'O'){
            celOpe.appendChild(btnEdit);
            celOpe.appendChild(btnSus);
        }
        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        row.appendChild(celCode);
        row.appendChild(celNom);
        row.appendChild(celPrenom);
        row.appendChild(celEtat);
        row.appendChild(celOpe);
        return row;
    }

    


window.loadData = function (){
        $("#table-body").html('');
        $.ajax({
            url: "/api/douanier/list",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                
                for(var i=0; i<data.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(data[i]));
                }
                $('table').DataTable();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("processus erroné");
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
                console.log("Le processus est terminé");
            }
        });
    };
    
window.saveData = function(matricule, nom, prenom){
     $.ajax({
            url: "/douanier/add",
            method: "POST",
            data: {
                nom: nom,
                matricule: matricule,
                prenom: prenom
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-addPane').click();
                loadData();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            complete: function (jqXHR, textStatus) {
                console.log("terminé");
            }
        });
};
    
window.delete = function(ref){
     $.ajax({
            url: "/douanier/delete",
            method: "POST",
            data: {
                matricule: ref,
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-addPane').click();
                loadData();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            complete: function (jqXHR, textStatus) {
                console.log("terminé");
            }
        });
};

    
window.editData = function(matricule, nom, prenom){
     $.ajax({
            url: "/douanier/edit",
            method: "POST",
            data: {
                nom: nom,
                matricule: matricule,
                prenom: prenom
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-editPane').click();
                loadData();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
};

});