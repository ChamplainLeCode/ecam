<%@page import="app.modelController.PaquetJpaController"%>
<%@page import="app.modelController.ColisJpaController"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="app.modelController.EmpotageJpaController"%>
<%@page import="app.controllers.IndexController"%>
<%@page import="app.modelController.EssenceJpaController"%>
<%@page import="app.models.Database"%>
<%@page import="app.models.Colis"%>
<%@page import="app.models.Embarquement"%>
<!DOCTYPE html>
<%
    Embarquement e = (Embarquement)request.getAttribute("embarquement") ;
%>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/resources/css/bootstrap/dist/css/bootstrap.min.css">
	<title>ECAM Specification</title>
        <script>
            var tableToExcel = (function() {
             var uri = 'data:application/vnd.ms-excel;base64,'
               , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
               , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
               , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
             return function(table, name) {
               if (!table.nodeType) table = document.getElementById(table)
               var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
               window.location.href = uri + base64(format(template, ctx))
             }
           })();
        </script>
	<style>
		@page{
			margin-top: 0cm;
			margin-bottom: 0cm;
			margin-right: 0.5cm;
			margin-left: 0.5cm;
			padding-left: 0.5cm;
			padding-right: 0.5cm;
			font-size: 6pt;
		}
                .to-print{
                    visibility: hidden;
                }
		@media print{
                    .no-print{
                        display: none;
                    }
			tr>td{
				margin: 0px;
				padding: 0px;
				font-size: 6pt;
			}
                        .to-print{
                            visibility: visible;
                            margin-bottom: 50px;
                        }
		}
	</style>
</head>
<body style="color: #123456">
    <img src="/resources/image/4_5913703756481954944.png" style="width: 100%;" class="row img-responsive image">
                <table  style="font-weight: bold; font-size: 6pt; position: relative; width: 100%; color: #123456; margin-left: 30px;">
		<tbody>
				<tr>
					<td style="font-size: 8pt;" colspan="2">SP�CIFICATION N� : <input type="text" style="width: 40px; border: none;">/2019</td>
					<td style="text-align: center"><%=e.getRefClient().getVille()+" - "+e.getRefClient().getPays()%></td>
				</tr>
				<tr>
					<td >ESSENCE :</td>
					<td>.</td>
					<td style="text-align: center"><%=e.getRefClient().getEmail()%></td>
				</tr>
				<tr>
					<td >By / Par</td>
					<td>.</td>
					<td style="text-align: center"><%=e.getRefClient().getTelephones()%></td>
				</tr>
				<tr>
					<td >Du</td>
					<td>.</td>
					<td style="text-align: center"></td>
				</tr>
				<tr>
					<td >Port de Destination: <%=e.getPortDestination()%></td>
					<td>.</td>
					<td style="text-align: center"></td>
				</tr>
				<tr>
					<td >Destination finale: <%=e.getVilleDestination()%></td>
					<td>.</td>
					<td style="text-align: center"></td>
				</tr>
				<tr>
					<td >Marque: <%=e.getNomBateau()%></td>
					<td></td>
                                        <td style="text-align: center">Mbalmayo, le <%=new Date().toLocaleString()%></td>
				</tr>
		</tbody>
            </table><br><br>

            <table class="table-condensed" id="liste" border="2" style="border: solid 2px #123456; text-align: center; position: relative; font-size: 5pt; width: 96%; margin-left: 20px; ">
		<thead>
			<tr style="height: 30px; ">
                            <th style="text-align: center; border: solid 2px #123456;">NATURE</th>
                            <th style="text-align: center; border: solid 2px #123456;">PAYS ORIGINE</th>
                            <th style="text-align: center; border: solid 2px #123456;">N� COLIS</th>
                            <th style="text-align: center; border: solid 2px #123456;">CODE</th>
                            <th style="text-align: center; border: solid 2px #123456;">CERT.</th>
                            <th style="text-align: center; border: solid 2px #123456;">ESSENCE</th>
                            <th style="text-align: center; border: solid 2px #123456;">QUALITE</th>
                            <th style="text-align: center; border: solid 2px #123456;">POIDS BRUT (KG)</th>
                            <th style="text-align: center; border: solid 2px #123456;">LONG. M (cm)</th>
                            <th style="text-align: center; border: solid 2px #123456;">LARG. M (cm)</th>
                            <th style="text-align: center; border: solid 2px #123456;">HAUT. M (cm)</th>
                            <th style="text-align: center; border: solid 2px #123456;">NB FEUILLES</th>
                            <th style="text-align: center; border: solid 2px #123456;">VOLUME (m�)</th>
			</tr>
		</thead>
		<tbody>
                    <%
                      DecimalFormat df = new DecimalFormat("####");
                        if(e != null)
                            for(Colis c : e.getColisList()){
                    %>
			<tr>
                            <td style="border: solid 2px #123456;"><%=c.getTypeColis()%></td>
                                <td style="border: solid 2px #123456;"><input type="text" style="border:none; text-align: center;"></td>
				<td style="border: solid 2px #123456;"><%=c.getRefColis()%></td>
				<td style="border: solid 2px #123456;"><%=new EssenceJpaController(Database.getEntityManager()).getEssenceRefByName(c.getEssence())%></td>
				<td style="border: solid 2px #123456;"><%=IndexController.getColisCertification(c.getRefColis())%></td>
				<td style="border: solid 2px #123456;"><%=c.getEssence()%></td>
				<td style="border: solid 2px #123456;"><%=c.getQualite()%></td>
				<td style="border: solid 2px #123456;"><%=c.getPoidsColis()+" - "+c.getEpaisseur()%></td>
				<td style="border: solid 2px #123456;"><%=c.getLongueurColis()%></td>
				<td style="border: solid 2px #123456;"><%=c.getLargeurColis()%></td>
                                <td style="border: solid 2px #123456;"><%=new EmpotageJpaController(Database.getEntityManager()).getHauteurForColis(c.getRefColis())%></td>
                                <td style="border: solid 2px #123456;"><%=new PaquetJpaController(Database.getEntityManager()).countFeuilleByColis(c.getRefColis())%></td>
                                <td style="border: solid 2px #123456;"><%=df.format(c.getVoulumeColis())%></td>
			</tr>
                        <%
                            }
                        %>
		</tbody>
	</table>
        <table class="to-print" style="position: absolute; bottom: 0; width: 95%;">
		<tbody>
                    <tr style="margin-bottom: 20px;">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td rowspan="1" style="border: 1px black solid; width: 200px; margin-right: 30px"><p style="text-align: center;">DIRECTEUR G�N�RAL</p><hr style="background-color: black; color: black; height: 1px;"><p><br></p><br/></td>
                        </tr>
                        <TR><td style="width:25%;">. </td><td style="width:25%;"> .</td><td style="width:25%;"> .</td><td style="width:25%;"> .</td></TR>
                        <tr >
                            <td style="width:25%;"><img src="/resources/image/bois.png" style="width: 100%; height: 80px;"></td>
                            <td style=" border: 1px black solid; width: 25%;height: 80px; display: none;">
                                <img class="pull-left" alt="(LOGO)" style="width: 30%">
                                <p class="pull-right" style="width: 70%;">
                                    (CODE DE REFERENCE OLB ET 
                                    PERIODE DE VALIDITE 
                                    ORGANISME DE CERTIFICATION 
                                    UNITE DE TRANSFORMATION DU BOIS 
                                    CERTIFIE OLB)
                                </p>
                            </td>
                            <td style="margin-top: 20px; width: 25%;"></td>
                            <td style="margin-top: 20px; width: 25%;"></td>
			</tr>
		</tbody>
	</table>
                <button class="btn btn-primary no-print" style="margin-top: 100px;" onclick="if(confirm('Exporter sous excel ?')) tableToExcel('liste', 'Rapport de sp�cification'); if(confirm('Imprimer la fiche de sp�cification')) window.print();"> <i class="fa fa-print"></i> Imprimer</button>
    <script src="/resources/js/my/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="/resources/css/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>