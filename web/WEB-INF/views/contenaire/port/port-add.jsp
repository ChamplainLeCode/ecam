<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-port">
    <p class="status-edit-port" style="padding: 5px; border-radius: 5px; color: red;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Nom Port</label>
          <div class="col-sm-10">
              <input required="" class="form-control" style="width: 100%;" id="zone-port">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Nom Ville</label>

          <div class="col-sm-10">
              <select required="" class="select2" style="width: 100%;" id="zone-ville"></select>
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Nom Pays</label>

          <div class="col-sm-10">
              <select required="" class="select2" style="width: 100%;" id="zone-pays"></select>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-add-port"><i class="fa fa-pencil"></i> Ajouter</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        
        
        $("#btn-add-port").click(function(){
            if($('#zone-port').val() === ''){
                $('.status-edit-port').html('Veuillez entrer le nom du port');
                return;
            }
            if($('#zone-ville').val() === ''){
                $('.status-edit-port').html('Veuillez entrer le nom de la ville');
                return;
            }
            if($('#zone-pays').val() === ''){
                $('.status-edit-port').html('Veuillez entrer le nom du pays');
                return;
            }
            saveData($('#zone-port').val(), $('#zone-ville').val(), $('#zone-pays').val());
            $('#form-edit-port .overlay').removeClass('hide');
            $('#form-edit-port #btn-add-port').addClass("hide");
        });
        
    
        window.getVilles = function(){
           
            $.ajax({
                url: '/api/ville/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    
                    var select = document.getElementById('zone-ville');
                    var data = jqXHR.responseJSON;
                    var option = document.createElement("option");
                    select.innerHTML = '';
                    option.innerHTML = '-- Selectionnez --';
                    option.value = '';
                    select.appendChild(option);
                    for(var i=0; i<data.length; i++){
                        option = document.createElement("option");
                        option.value = data[i].nom;
                        option.innerText = data[i].nom;
                        select.appendChild(option);
                    }
                    $(select).select2();
                }
            });
        };
    
        window.getPays = function(){
           
            $.ajax({
                url: '/api/pays/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    
                    var select = document.getElementById('zone-pays');
                    var data = jqXHR.responseJSON;
                    var option = document.createElement("option");
                    select.innerHTML = '';
                    option.innerHTML = '-- Selectionnez --';
                    option.value = '';
                    select.appendChild(option);
                    //console.log(data);
                    for(var i=0; i<data.length; i++){
                        option = document.createElement("option");
                        option.value = data[i].libelle;
                        option.innerText = data[i].libelle;
                        select.appendChild(option);
                    }
                    $(select).select2();
                }
            });
        };
        
        getPays();
        getVilles();
        //$('select').select2();
    </script>
</div>
