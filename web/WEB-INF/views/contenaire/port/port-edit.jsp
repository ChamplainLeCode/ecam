<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div id="form-edit-personnel">
    <div class="register-box-body">
             <h2 class="login-box-msg">Modifier un pays </h2>

        <form   action="#">
            
            <div class="form-group has-feedback">
                <legend><h4>Code <span class="" style="color:red; font-size: 10pt;" id="status-ref"></span></h4></legend>
                <input type="text" id="zone-ref" required="" value="<%=request.getParameter("ref")%>" readonly=""  class="form-control" placeholder="Nom ">
              <span class="fa fa-ship form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <legend><h4>Nom Port <span class="" style="color:red; font-size: 10pt;" id="status-nom"></span></h4></legend>
                <input type="text" id="zone-nom" required="" value="<%=request.getParameter("nom")%>" class="form-control" placeholder="Nom Port ">
              <span class="fa fa-ship form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <legend><h4>Ville <span class="" style="color:red; font-size: 10pt;" id="status-ville"></span></h4></legend>
                <input type="text" id="zone-ville" required="" value="<%=request.getParameter("ville")%>" class="form-control" placeholder="Nom Ville">
              <span class="fa fa-ship form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <legend><h4>Pays <span class="" style="color:red; font-size: 10pt;" id="status-pays"></span></h4></legend>
                <input type="text" id="zone-pays" required="" value="<%=request.getParameter("pays")%>" class="form-control" placeholder="Nom Pays">
              <span class="fa fa-ship form-control-feedback"></span>
            </div>
          <div class="row">
              <!-- /.col -->
            <div class="col-xs-4 pull-left">
                <button type="btn" data-dismiss="modal"  id="btn-close-editPane" class="btn form-control btn-block btn-flat">Fermer</button>
            </div>
            <div class="pull-right col-xs-4">
                <button type="button" id="btn-save-pays" class="btn btn-primary form-control btn-block btn-flat">Enregistrer</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
      </div>

    
</div>
<script type="text/javascript">
        $('#btn-save-pays').click(function(){
            var t = false;
            
            if($('#zone-ref').val().length === 0){
                $('#status-ref').html('Contactez L\'administrateur');
                t = true;
            }else $('#status-ref').html('');
            
            if($('#zone-nom').val().length === 0){
                $('#status-nom').html('Veuillez indiquer le nom du Port');
                t = true;
            }else $('#status-nom').html('');
            
            if($('#zone-ville').val().length === 0){
                $('#status-ville').html('Veuillez indiquer le nom de la ville');
                t = true;
            }else $('#status-ville').html('');
            
            if($('#zone-pays').val().length === 0){
                $('#status-pays').html('Veuillez indiquer le nom du pays');
                t = true;
            }else $('#status-pays').html('');
            
            
            if(t === true)
                return;
            $.ajax({
                url: '/port/edit',
                method: 'POST',
                async: false,
                data: {ref: $('#zone-ref').val(), nom: $('#zone-nom').val(), ville: $('#zone-ville').val(), pays: $('#zone-pays').val()},
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    $('#btn-close-editPane').click();
                    loadData();
                }
            });
            return false;
        });
    </script>
