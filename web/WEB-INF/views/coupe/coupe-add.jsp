<%-- 
    Document   : coupe-add
    Created on : 1 févr. 2019, 09:36:59
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-coupe">
    <p class="status-edit-coupe" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REFERENCE</label>

          <div class="col-sm-10">
              <input type="text" required="" class="form-control" readonly="" id="zone-ref" placeholder="Reference de la Coupe">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Bille</label>

          <div class="col-sm-10">
              <select style="width: 100%;" id="zone-bille"></select>
          </div>
        </div>
          <div class="form-group">
          <label for="zone-billon" class="col-sm-2 control-label">Billon</label>

          <div class="col-sm-10">
              <select style="width: 100%;" id="zone-billon"></select>
          </div>
        </div>
          
          <div class="form-group">
          <label for="zone-coupe" class="col-sm-2 control-label">Coupe</label>

          <div class="col-sm-10">
              <select style="width: 100%;" id="zone-coupe"></select>
          </div>
        </div>
          
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">NombrePlot</label>

          <div class="col-sm-10">
              <input type="number"  required="" max="5" class="form-control" min="0" id="zone-nombrePlot" placeholder="NombrePlot du Coupe">
          </div>
        </div>
          
          <div class="container-fluid">
              <table class="table table-responsive table-striped">
                  <thead>
                      <tr><th colspan="5"><b>Details sur les Plots</th></tr>
                      <tr>
                          <th>N° Plot</th>
                          <!--th>Face</th-->
                          <th>Longueur (Cm)</th>
                          <th>N° Travail</th-->
                      </tr>
                  </thead>
                  <tbody id="table-details-plots"></tbody>
              </table>
          </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-add-coupe"><i class="fa fa-pencil"></i> Ajouter</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">

        /**
         * Contient la valeur du numéro de travail associé au billon sur lequel on veut 
         * effectuer la coupe.
         * Cette valeur change lorsqu'on change le billon selectionné
         * @type String
         */
        window.travail  = "";
        /**
         * Contient la longueur du billon sur le quel on veut effecter la coupe
         * Cette valeur est modifiée lorsqu'on change le billon selectionné
         * utilisé comme longueur des plots
         * @type String
         */
        window.longueur = "";
        
        $("#btn-add-coupe").click(function(){
            
            var data = [];
            
            for(var i=0; i<parseInt($('#zone-nombrePlot').val()); i++){
                data[i] = {
                    ref: $('#zone-plot-ref-'+i).val(),
                    face: $('#zone-plot-face-'+i).val(),
                    longueur : $('#zone-plot-longueur-'+i).val()
                };
            }
            saveData($('#zone-ref').val(),  $('#zone-billon').val(),$('#zone-nombrePlot').val(), $('#zone-coupe').val(), data);
            $('#form-edit-coupe .overlay').removeClass('hide');
            $('#form-edit-coupe #btn-add-coupe').addClass("hide");
        });
        
        getBille();
        getTypeCoupe();
// $('#zone-billon').attr("data") contient le numéro de travail du billon
        
        $('#zone-bille').change(function(){getBillonForBille($(this).val()); setNombrePlots();});
        $('#zone-billon').change(function(){
            window.longueur = this.options[this.selectedIndex].getAttribute("longueur");
            window.travail = this.options[this.selectedIndex].getAttribute("travail");
            $('#zone-nombrePlot').val(0).change();
        });
        $('#zone-ref').val(new Date().getTime());
        $('select').select2();
    </script>
</div>
