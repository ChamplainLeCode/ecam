<%-- 
    Document   : coupe-edit
    Created on : 1 févr. 2019, 09:37:28
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-coupe">
    <p class="status-edit-coupe" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REFERENCE</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<% out.print(request.getParameter("cni"));%>" required="" class="form-control" id="zone-cni" placeholder="Reference de la Coupe">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Billon</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("billon"));%>" class="form-control" id="zone-billon" placeholder="Billon de la Coupe">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">NombrePlot</label>

          <div class="col-sm-10">
              <input type="number"  required="" value="<% out.print(request.getParameter("nombrePlot"));%>" class="form-control" id="zone-nombrePlot" placeholder="NombrePlot du Coupe">
          </div>
        </div>
        
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">TypeCoupe</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("typeCoupe"));%>" class="form-control" id="zone-typeCoupe" placeholder="TypeCoupe de coupe">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" id="btn-close-editPane" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-coupe"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        $("#btn-edit-coupe").click(function(){
            editData($('#zone-cni').val(), $('#zone-nombrePlot').val(), $('#zone-billon').val(), $('#zone-typeCoupe').val());
            $('#form-edit-coupe .overlay').removeClass('hide');
            $('#form-edit-coupe #btn-edit-coupe').addClass("disabled");
        });

    </script>
</div>
