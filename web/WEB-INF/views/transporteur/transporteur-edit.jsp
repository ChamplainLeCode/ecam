<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-classification">
    <p class="status-edit-classification" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Reférence</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<%=request.getParameter("ref")%>" required="" class="form-control" id="zone-ref" placeholder="Rérérence">
          </div>
        </div>
        <div class="form-group">
          <label for="zone- libelle" class="col-sm-2 control-label">Contribuable</label>

          <div class="col-sm-10">
              <input type="text" required="" value="<%=request.getParameter("contribuable")%>" class="form-control" id="zone-contribuable" placeholder="valeur du Contribuable">
          </div>
        </div>
          
        <div class="form-group">
          <label for="zone- libelle" class="col-sm-2 control-label">Nom</label>

          <div class="col-sm-10">
              <input type="text" value="<%=request.getParameter("nom")%>" required="" class="form-control" id="zone-nom" placeholder="Nom du transporteur">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
          <button type="button"  data-dismiss="modal" id="btn-close" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-classification"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
    <script type="text/javascript">
        $("#btn-edit-classification").click(function(){
            editData($('#zone-ref').val(), $('#zone-contribuable').val(), $('#zone-nom').val());
            $('#form-edit-classification .overlay').removeClass('hide');
            $('#form-edit-classification #btn-edit-classification').addClass("disabled");
        });

    </script>
</div>
