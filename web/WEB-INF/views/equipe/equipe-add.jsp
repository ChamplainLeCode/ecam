<%-- 
    Document   : equipe-add
    Created on : 6 févr. 2019, 00:53:59
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-equipe">
    <p class="status-edit-equipe" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal " onsubmit="return save();">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REFERENCE</label>

          <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="zone-ref" placeholder="Reference Equipe">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">DateEquipe</label>

          <div class="col-sm-10">
              <input type="date"  required="" class="form-control" id="zone-dateEquipe" placeholder="DateEquipe de l'Equipe ">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">DateExec</label>

          <div class="col-sm-10">
              <input type="date"  required="" class="form-control" id="zone-dateExec" placeholder="DateExec de l'Equipe">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Nom de l'équipe</label>

          <div class="col-sm-10">
              <input type="text"  required="" class="form-control" id="zone-libelle"></select>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="submit" class="btn btn-info pull-right" id="btn-add-equipe"><i class="fa fa-pencil"></i> Ajouter</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        $('#zone-ref').val("REF_EQ_"+new Date().getTime()).attr("readonly","");
         function save(){
            saveData($('#zone-ref').val(), $('#zone-dateEquipe').val(), $('#zone-dateExec').val(), $('#zone-libelle').val());
            $('#form-edit-equipe .overlay').removeClass('hide');
            $('#form-edit-equipe #btn-add-equipe').addClass("hide");
            return false;
        };
    </script>
</div>
