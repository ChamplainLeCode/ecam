<%-- 
    Document   : equipe-edit
    Created on : 6 févr. 2019, 00:53:30
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-equipe">
    <p class="status-edit-equipe" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal " onsubmit="return edit();">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REFERENCE</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<% out.print(request.getParameter("ref"));%>" required="" class="form-control" id="zone-ref" placeholder="Reference Equipe">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">DateEquipe</label>

          <div class="col-sm-10">
              <input type="date"  required="" value="<% out.print(request.getParameter("dateEquipe"));%>" class="form-control" id="zone-dateEquipe" placeholder="DateEquipe de l'Equipe">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">DateExec</label>

          <div class="col-sm-10">
              <input type="date"  required="" value="<% out.print(request.getParameter("dateExec"));%>" class="form-control" id="zone-dateExec" placeholder="DateExec de l'Equipe">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Nom Equipes</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("libelle"));%>" class="form-control" id="zone-libelle" placeholder="Nom de l'Equipe">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" id="btn-close-editPane" class="btn btn-default">Fermer</button>
            <button type="submit" class="btn btn-info pull-right" id="btn-edit-equipe"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        function edit(){
            editData($('#zone-ref').val(), $('#zone-dateEquipe').val(), $('#zone-dateExec').val(), $('#zone-libelle').val());
            $('#form-edit-equipe .overlay').removeClass('hide');
            $('#form-edit-equipe #btn-edit-equipe').addClass("disabled");
            return false;
        };

    </script>
</div>
