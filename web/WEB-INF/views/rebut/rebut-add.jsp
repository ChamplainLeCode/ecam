<%-- 
    Document   : rebut-add
    Created on : 11 janv. 2019, 06:09:11
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="form-add-rebut">
    <p class="status-add-rebut" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Reférence</label>

          <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="zone-ref" placeholder="Rérérence">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-date" class="col-sm-2 control-label">Date</label>

          <div class="col-sm-10">
              <input type="date" required="" class="form-control" id="zone-date" placeholder="datecreation  du rebut">
          </div>
        </div>
          
           <div class="form-group">
          <label for="zone-typeRebut" class="col-sm-2 control-label">TypeRebut</label>

          <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="zone-typeRebut" placeholder="TypeRebut">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-save-rebut">Enregistrer</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
    <script type="text/javascript">
       
        $("#btn-save-rebut").click(function(){
            saveData($('#zone-ref').val(), $('#zone-date').val(), $('#zone-typeRebut').val());
            $('#form-add-rebut .overlay').removeClass('hide');
            $('#form-add-rebut #btn-save-rebut').addClass("disabled");
        });

    </script>
</div>

