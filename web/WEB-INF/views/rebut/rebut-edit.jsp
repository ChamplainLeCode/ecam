<%-- 
    Document   : rebut-edit
    Created on : 11 janv. 2019, 06:09:27
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-rebut">
    <p class="status-edit-rebut" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Reférence</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<% out.print(request.getParameter("ref"));%>" required="" class="form-control" id="zone-ref" placeholder="Rérérence">
          </div>
        </div>
        <div class="form-group">
          <label for="zone- date" class="col-sm-2 control-label">Date</label>

          <div class="col-sm-10">
              <input type="text" required="" value="<% out.print(request.getParameter("date"));%>" class="form-control" id="zone-date" placeholder="Date  du rebut">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-typeRebut" class="col-sm-2 control-label">TypeRebut</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<% out.print(request.getParameter("typeRebut"));%>" required="" class="form-control" id="zone-typeRebut" placeholder="TypeRebut">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-rebut"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
    <script type="text/javascript">
        $("#btn-edit-rebut").click(function(){
            editData($('#zone-ref').val(), $('#zone-date').val(), $('#zone-typeRebut').val());
            $('#form-edit-rebut .overlay').removeClass('hide');
            $('#form-edit-rebut #btn-edit-rebut').addClass("disabled");
        });

    </script>
</div>
