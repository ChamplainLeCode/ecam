<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>ECAM registration</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="/resources/css/bootstrap/dist/css/bootstrap.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="/resources/css/font-awesome/css/font-awesome.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="/resources/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="/resources/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="/resources/css/blue.css">
        <link rel="stylesheet" href="/resources/iCheck/minimal/aero.css">
        <link rel="stylesheet" href="/resources/css/select2/select2.css">

    </head>
    <body class="hold-transition register-page">
        <p style="position: fixed; top: 10px; left: 10px;"><img src="/resources/image/IMG-20190401-WA0011.jpg"></p>
        <div class="register-box" style="position: relative;">
      <div class="register-logo">
          <a href="/login"><p><b>ECAM-</b>Placages</p></a>
      </div>

      <div class="register-box-body">
          <p class="login-box-msg">Nouveau compte <span class="label" style="display: none;" id="status"></span></p>

        <form  method="post" post="/" onsubmit="return send(this);">
          <div class="form-group has-feedback">
              <legend><h4>Info Personnelles.</h4></legend>
              <input type="text" id="nom" required="" class="form-control" placeholder="Nom ">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
              <input type="text" id="prenom" required="" class="form-control" placeholder="Pr�nom">
            <span class="glyphicon  form-control-feedback"  style="margin-right: 10px;"></span>
          </div>
          <div class="form-group has-feedback select2">
                <legend>Fonction            <span class="glyphicon glyphicon-wrench  form-control-feedback"></span></legend>
                <label>Privil�ge</label>
                <select id="fonction" required="" class="form-control">
                    <option value="">Privilege</option></select>
          </div>
          <div class="form-group has-feedback select2">
              <label>Droits<span class="glyphicon glyphicon-alert form-control-feedback"></span></label>
              <table class="table table-hover table-responsive">
                    <tr>
                        <td><i class="fa fa-plus"></i></td>
                        <td><i class="fa fa-edit"></i></td>
                        <td><i class="fa fa-trash"></i></td>
                        <td><i class="fa fa-print"></i></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" onchange="if(this.value === 'N'){this.value = 'O';}else{this.value = 'N';}" value="N" id="can_add"/></td>
                        <td><input type="checkbox" onchange="if(this.value === 'N'){this.value = 'O';}else{this.value = 'N';}" value="N" id="can_edit"/></td>
                        <td><input type="checkbox" onchange="if(this.value === 'N'){this.value = 'O';}else{this.value = 'N';}" value="N" id="can_delete"/></td>
                        <td><input type="checkbox" onchange="if(this.value === 'N'){this.value = 'O';}else{this.value = 'N';}" value="N" id="can_print"/></td>
                    </tr>
              </table>
            
          </div>
            <div class="form-group has-feedback">
                <legend>Authentification</legend>
                <input type="text" id="matricule" required="" class="form-control" placeholder="Matricule">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" id="password" required="" class="form-control" placeholder="Password">
            </div>
          <div class="row">
              <!-- /.col -->
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary form-control btn-block btn-flat">Register</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
      </div>
      <!-- /.form-box -->
    </div>
    <!-- /.register-box -->

    <script src="/resources/js/my/jquery.min.js"></script>
    <script src="/resources/js/popper.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="/resources/css/bootstrap/dist/js/bootstrap.js"></script>
    <!-- iCheck -->
    <script src="/resources/iCheck/icheck.js"></script>
    <script src="/resources/js/select2/select2.full.js"></script>
    
    <script>
      $(function () {
        $('select').select2();
        
        $.ajax({
            url: '/api/privilege/list',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var select = document.getElementById("fonction");
                
                var option;
                var data = jqXHR.responseJSON;
                
                for(var i=0; i<data.length; i++){
                    option = document.createElement("option");
                    option.value = data[i].ref;
                    option.innerText = data[i].libelle;
                    select.appendChild(option);
                }
                
            }
        });
        
        window.send = function(self){
            var process = confirm("\nAssistance de cr�ation de compte\n\n\n\
                \nNom       = "+$('#nom').val()+"\n\
                \nPrenom  = "+$('#prenom').val()+"\n\
                \nPrivil�ge = "+$('#fonction').val()+"\n\
                \n\tPeut Ajouter     = "+($('#can_add').val() === 'O' ? 'Oui' : 'Non')+"\n\
                \n\tPeut Modier      = "+($('#can_edit').val() === 'O' ? 'Oui' : 'Non')+"\n\
                \n\tPeut Supprimer = "+($('#can_delete').val() === 'O' ? 'Oui' : 'Non')+"\n\
                \n\tPeut Imprimer   = "+($('#can_print').val() === 'O' ? 'Oui' : 'Non')+"\n\
                \nMatricule = "+$('#matricule').val()+"\n\
                \nMot de Passe = "+$('#password').val()+"\n\n\nConfirmez-vous ces informations?");
            if(process === true){
                $.ajax({
                    url: '/register/new',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        nom: $('#nom').val(),
                        prenom: $('#prenom').val(),
                        fonction: $('#fonction').val(),
                        can_add: $('#can_add').val(),
                        can_edit: $('#can_edit').val(),
                        can_delete: $('#can_delete').val(),
                        can_print: $('#can_print').val(),
                        matricule: $('#matricule').val(),
                        password: $('#password').val()
                    }, 
                    complete: function (jqXHR, textStatus ) {
                        var result = jqXHR.responseJSON;
                        if(result.resultat === 'true'){
                            $('#nom').val('');
                            $('#prenom').val('');
                            $('#fonction').val('');
                            document.getElementById('can_add').checked = false;
                            document.getElementById('can_edit').checked = false;
                            document.getElementById('can_delete').checked = false;
                            document.getElementById('can_print').checked = false;
                            $('#matricule').val('');
                            $('#password').val('');
                            $('#status').css('margin-left','10px').addClass("label-success").html('cr�e').show();
                            scrollTo(screenLeft, screenTop);
                            setTimeout(()=>$('#status').hide(), 10000);
                        }else{
                            switch(result.type){
                                case 'redondance':
                                    $('#status').css('margin-left','10px').addClass("label-danger").html(result.reason).show();
                                    scrollTo(screenLeft, screenTop);
                                    setTimeout(()=>$('#status').hide(), 10000);
                                    break;
                                case 'database': 
                                    $('#status').css('margin-left','10px').addClass("label-danger").html(result.reason).show();
                                    scrollTo(screenLeft, screenTop);
                                    setTimeout(()=>$('#status').hide(), 10000);
                                    break;
                                case 'any': 
                                    $('#status').css('margin-left','10px').addClass("label-danger").html(result.reason).show();
                                    scrollTo(screenLeft, screenTop);
                                    setTimeout(()=>$('#status').hide(), 10000);
                                    break;

                            }
                        }
                    }
                });
            }else{
                scrollTo(screenLeft, screenTop);
                $('#status').css('margin-left', '10px').addClass(' label-primary').html('Op�ration annul�e');
                setTimeout(()=>$('#status').hide(), 5000);
            }
        
            return false;
        }
        
      });
    </script>


    </body>
</html>