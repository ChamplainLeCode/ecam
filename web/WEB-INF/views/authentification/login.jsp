<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ECAM-Placages | Login</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="/resources/css/bootstrap/dist/css/bootstrap.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="/resources/css/font-awesome/css/font-awesome.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="/resources/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="/resources/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="/resources/css/blue.css">
        <script>
                localStorage.removeItem('user');
                sessionStorage.removeItem('lockscreen');
                window.persis = function(some){
                    localStorage.setItem('user', JSON.stringify(some));
                    sessionStorage.setItem("lockscreen", some.matricule);
                }
        </script>
        </head>
        <body class="hold-transition login-page">
            <p style="position: fixed; top: 70%; " class="pull-right"><img src="/resources/image/IMG-20190401-WA0011.jpg"></p>
        <div class="login-box">
          <div class="login-logo">
            <a href="/login"><b>ECAM-</b>Placages</a>
          </div>
          <!-- /.login-logo -->
          <div class="login-box-body">
            <p class="login-box-msg">Commencer une nouvelle session</p>
            <p><span id="status"></span></p>
            <form onsubmit="return charger();">
              <div class="form-group has-feedback">
                  <input type="text" class="form-control" id="zone-matricule" placeholder="Matricule">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                  <input type="password" class="form-control" id="zone-password" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              </div>
              <div  class="row">
                <!-- /.col -->
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Se connecter</button>
                </div>
                <!-- /.col -->
              </div>
            </form><br><br><br>
            <a href="#">Mot de passe oubli�</a><br>

          </div>
          <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->

        <!-- jQuery 3 -->
        <script src="/resources/js/my/jquery.min.js"></script>
        <script src="/resources/js/popper.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="/resources/css/bootstrap/dist/js/bootstrap.js"></script>
        <!-- iCheck -->
        <script src="/resources/iCheck/icheck.js"></script>
        <script>
          $(function () {
            function getIPs(callback) {
                var ip_dups = {};

                //compatibility for firefox and chrome
                var RTCPeerConnection = window.RTCPeerConnection
                  || window.mozRTCPeerConnection
                  || window.webkitRTCPeerConnection;
                var useWebKit = !!window.webkitRTCPeerConnection;

                //bypass naive webrtc blocking using an iframe
                if(!RTCPeerConnection){
                  //NOTE: you need to have an iframe in the page right above the script tag
                  //
                  //<iframe id="iframe" sandbox="allow-same-origin" style="display: none"></iframe>
                  //<script>...getIPs called in here...
                  //
                  var win = iframe.contentWindow;
                  RTCPeerConnection = win.RTCPeerConnection
                    || win.mozRTCPeerConnection
                    || win.webkitRTCPeerConnection;
                  useWebKit = !!win.webkitRTCPeerConnection;
                }

                //minimal requirements for data connection
                var mediaConstraints = {
                  optional: [{RtpDataChannels: true}]
                };

                //firefox already has a default stun server in about:config
                //    media.peerconnection.default_iceservers =
                //    [{"url": "stun:stun.services.mozilla.com"}]
                var servers = undefined;

                //add same stun server for chrome
                if(useWebKit)
                  servers = {iceServers: [{urls: "stun:stun.services.mozilla.com"}]};

                //construct a new RTCPeerConnection
                var pc = new RTCPeerConnection(servers, mediaConstraints);

                function handleCandidate(candidate){
                  //match just the IP address
                  var ip_regex = /([0-9]{1,3}(\.[0-9]{1,3}){3})/
                  var ip_addr = ip_regex.exec(candidate)[1];

                  //remove duplicates
                  if(ip_dups[ip_addr] === undefined)
                      callback(ip_addr);

                  ip_dups[ip_addr] = true;
                }

                //listen for candidate events
                pc.onicecandidate = function(ice){
                  //skip non-candidate events
                  if(ice.candidate)
                    handleCandidate(ice.candidate.candidate);
                };

                //create a bogus data channel
                pc.createDataChannel("");

                //create an offer sdp
                pc.createOffer(function(result){
                  //trigger the stun server request
                  pc.setLocalDescription(result, function(){}, function(){});
                }, function(){});

                //wait for a while to let everything done
                setTimeout(function(){
                  //read candidate info from local description
                  var lines = pc.localDescription.sdp.split('\n');
                  lines.forEach(function(line){
                    if(line.indexOf('a=candidate:') === 0) {
                      handleCandidate(line);
                    }
                  });
                }, 1000);
            }
            
            window.charger = function(){

                getIPs(function(ip){
                    $.ajax({
                        url: '/login',
                        method: 'POST', 
                        dataType: 'json',
                        data: {matricule: $('#zone-matricule').val(), password: $('#zone-password').val(), ip: ip},
                        complete: function (jqXHR, textStatus ) {
                            var result = jqXHR.responseJSON;
                            if(result.resultat === true){
                                persis(result.user);
                                location.href = "/?user="+result.user.matricule+"&page=/";
                            }else{
                                $('#status').addClass("btn  btn-block btn-danger").html(result.reason).show();
                            }
                        }
                    });
                });

                return false;
            };
          });
        </script>


    </body>
</html>