<!DOCTYPE html>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta id="meta" charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Lockscreen</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <%@include file="/WEB-INF/views/layouts/style.jsp" %>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->

    </head>
<body class="hold-transition lockscreen">
        <p style="position: fixed; top: 10px; left: 10px;"><img src="/resources/image/IMG-20190401-WA0011.jpg"></p>
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
  <div class="lockscreen-logo">
    <a href="/"><b>ECAM-</b>Placages</a>
  </div>
  <!-- User name -->
  <div class="lockscreen-name user-name">Champlain N. Bakop</div>

  <!-- START LOCK SCREEN ITEM -->
  <div class="lockscreen-item">
    <!-- lockscreen image -->
    <div class="lockscreen-image">
        <img src="/resources/image/default-user.png" alt="User Image">
    </div>
    <!-- /.lockscreen-image -->

    <!-- lockscreen credentials (contains the form) -->
    <form class="lockscreen-credentials">
      <div class="input-group">
        <input type="password" class="form-control" placeholder="password">

        <div class="input-group-btn">
          <button type="button" class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
        </div>
      </div>
    </form>
    <!-- /.lockscreen credentials -->

  </div>
  <!-- /.lockscreen-item -->
  <div class="help-block text-center">
    Entrez votre mot de passe pour déverrouiller votre session
  </div>
  <div class="text-center">
    <a href="/login">Ou vous connecter en tantqu'un autre utilisateur</a>
  </div>
  <div class="lockscreen-footer text-center" style="position: absolute; top: 90%; left: 45%;">
    Copyright © 2019<b><a href="https://www.bixterprise.com" class="text-black"> Bixterprise Coorp</a></b><br>
    All rights reserved
  </div>
</div>
<!-- /.center -->

<!-- jQuery 3 -->
<%@include file="/WEB-INF/views/layouts/script.jsp" %>
<script type="text/javascript">
    
    sessionStorage.setItem('lockscreen', $('#meta').attr("data"));
</script>
    
    
</body></html>