<%-- 
    Document   : empotage-add
    Created on : 14 janv. 2019, 09:33:07
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-empotage">
    <p class="status-edit-empotage" style=" border-radius: 5px;"></p>
    <form class="form-horizontal" action="/empotage/add" method="POST">
      <div class="box-body">
        <div>
            <legend>Fermeture Info</legend>
            <div class="form-group">
              <label for="zone-epaisseur" class="col-sm-3 control-label">Fermeture Caisse</label>

              <div class="col-sm-9">
                    <span style="color: gray; font-weight: bold;">[ année - mois / jour ]; Ex: 2019-06-23</span>
                  <input type="date" name="fermetureCaisse" required="" class="form-control" id="zone-fermetureCaisse" placeholder="Date de fermeture Caisse">
              </div>
            </div>

            <div class="form-group">
                <label for="zone-epaisseur" class="col-sm-3 control-label">Colis</label>

                <div class="col-sm-9">
                    <select id="zone-colis" class="select2" name="colis" required="" style="width:100%;"></select>
                </div>
            </div>
            <div class="form-group">
                <label for="zone-ref" class="col-sm-3 control-label">REFERENCE</label>

                <div class="col-sm-9">
                    <svg class="code-bar" id="zone-codeBarColis-add" style="height: 60px;"></svg>
                </div>
            </div>
            <div class="form-group">
              <label for="zone-epaisseur"  class="col-sm-3 control-label">Hauteur</label>

              <div class="col-sm-9">
                  <input type="number" min="0" name="hauteur" required="" class="form-control" id="zone-hauteur" placeholder="Hauteur  La Caisse">
              </div>
            </div>
            <div class="form-group">
              <label for="zone-epaisseur" class="col-sm-3 control-label">Longueur</label>

              <div class="col-sm-9">
                  <input type="number" min="0" name="longueur" required="" class="form-control" id="zone-longueur" placeholder="Longueur La Caisse">
              </div>
            </div>

            <div class="form-group">
              <label for="zone-epaisseur" class="col-sm-3 control-label">Largeur</label>

              <div class="col-sm-9">
                  <input type="number" min="0" name="largeur" required="" class="form-control" id="zone-largeur" placeholder="Largeur La Caisse">
              </div>
            </div>           
            <div class="form-group hide">
                    <label for="zone-poids-emballage" class="col-sm-3 control-label">Poids Embal. [Kg]</label>

                    <div class="col-sm-9">
                        <input id="zone-poids-emballage" value="0" type="number" min="0" class="form-control" name="emballage-poids">
                    </div>
                </div>
                <div class="form-group hide">
                    <label for="zone-poids-colis" class="col-sm-3 control-label poids">Poids Colis [kg]</label>

                    <div class="col-sm-9">
                        <input id="zone-poids-colis" type="number" value="0" min="0" class="form-control poids" name="colis-poids">
                    </div>
                </div>
                <div class="form-group">
                    <label for="zone-poids-total"  class="col-sm-3 control-label">Poids Total</label>

                    <div class="col-sm-9">
                        <input id="zone-poids-total" type="number" min="0" class="form-control" name="total-poids" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="zone-poids-total"  class="col-sm-3 control-label">Essence</label>

                    <div class="col-sm-9">
                        <input id="zone-essence" type="text" readonly="" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="zone-poids-total"  class="col-sm-3 control-label">Surface</label>

                    <div class="col-sm-9">
                        <input id="zone-surface" type="number" min="0" class="form-control" name="zone-surface" required="">
                        <input type="hidden" name="user"><input type="hidden" name="page" value="/">
                    </div>
                </div>
            

        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="submit" class="btn btn-info pull-right"  id="btn-add-empotagke"><i class="fa fa-save"></i> Enregistré</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
    
    <script src="/resources/js/select2/select2.full.min.js"></script>
    <script src="/resources/js/codeBarre/JsBarcode.all.min.js"></script>
    <script type="text/javascript">
        $('#zone-ref').val(new Date().getTime()).attr("readonly","");
        $("#btn-add-empotage").click(function(){
            saveData($('#zone-ref').val(), $('#zone-fermetureCaisse').val(), $('#zone-hauteur').val(), $('#zone-largeur').val()
                    , $('#zone-colis').val(), $('#zone-poids-total').val());
            $('#form-edit-empotage .overlay').removeClass('hide');
            $('#form-edit-empotage #btn-add-empotage').addClass("hide");
        });
        $('input[name="user"]').val(user.matricule);
        $('.poids').change(function(){
            $('#zone-poids-total').val(parseInt($('#zone-poids-colis').val())+parseInt($('#zone-poids-emballage').val()));
        });
        (function getColis(){
            $.ajax({
                url: '/api/colis/for_empotage/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    var select = document.getElementById("zone-colis");
                    var option = document.createElement("option");
                    option.innerHTML = "-- Selectionner --";
                    option.value = "";
                    select.appendChild(option);
                    var data = jqXHR.responseJSON;
                    
                    for(var i=0; i<data.length; i++){
                        option = document.createElement("option");
                        option.value = data[i].refColis;
                        option.innerHTML = data[i].refColis;//'<svg id="code'+data[i].refColis+'" data="'+data[i].codeBarColis+'" class="code-bar"></svg>';
                        option.setAttribute("surface", data[i].surface);
                        option.setAttribute("essence", data[i].essence);
                        select.appendChild(option);
                    } 
                   $(select).change(function(){
                        if(this.value === ""){
                            $('#zone-codeBarColis-add').html('');
                        }else{
                            JsBarcode('#zone-codeBarColis-add',$(this).val(), { height: 60});
                            $('#zone-surface').val(select.options[select.selectedIndex].getAttribute("surface"));//$('#zone-adresse').val(select.seletedItem.getAttribute("data"));//[select.selectedIndex].getAttribute("data"));
                            $('#zone-essence').val(select.options[select.selectedIndex].getAttribute("essence"));//$('#zone-adresse').val(select.seletedItem.getAttribute("data"));//[select.selectedIndex].getAttribute("data"));
                        }
                   });
                }
            });
        })();
        $('select').select2();

    </script>
</div>
