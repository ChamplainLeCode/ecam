<%-- 
    Document   : empotage-edit
    Created on : 14 janv. 2019, 09:33:21
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-empotage">
    <p class="status-edit-empotage" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REFERENCE</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<% out.print(request.getParameter("ref"));%>" required="" class="form-control" id="zone-ref" placeholder="Reference Empotage">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">FermetureCaisse</label>

          <div class="col-sm-10">
              <input type="number"  required="" value="<% out.print(request.getParameter("fermetureCaisse"));%>" class="form-control" id="zone-fermetureCaisse" placeholder="FermetureCaisse  Empotage">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Hauteur</label>

          <div class="col-sm-10">
              <input type="number"  required="" value="<% out.print(request.getParameter("hauteur"));%>" class="form-control" id="zone-hauteur" placeholder="Hauteur  Empotage">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Largeur</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("largeur"));%>" class="form-control" id="zone-largeur" placeholder="Largeur  Empotage">
          </div>
        </div
        
        
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Colis</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("colis"));%>" class="form-control" id="zone-colis" placeholder="Colis Empotage ">
          </div>
        </div>

      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" id="btn-close-editPane" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-empotage"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        $("#btn-edit-empotage").click(function(){
            editData($('#zone-ref').val(), $('#zone-hauteur').val(), $('#zone-largeur').val(), $('#zone-largeur').val(), $('#zone-colis').val());
            $('#form-edit-empotage .overlay').removeClass('hide');
            $('#form-edit-empotage #btn-edit-empotage').addClass("disabled");
        });

    </script>
</div>

