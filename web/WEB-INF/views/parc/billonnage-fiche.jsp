<%-- 
    Document   : billonnage-fiche
    Created on : 22 juin 2019, 04:13:21
    Author     : champlain
--%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="app.modelController.ParcJpaController"%>
<%@page import="app.models.DetailReception"%>
<%@page import="java.util.LinkedList"%>
<%@page import="java.util.List"%>
<%@page import="app.models.Database"%>
<%@page import="app.modelController.DetailReceptionJpaController"%>
<%
    
    DetailReceptionJpaController cont = new DetailReceptionJpaController(Database.getEntityManager());
    int n = Integer.parseInt(request.getParameter("taille"));
    List<DetailReception> liste = new LinkedList();
    
    for(int i=0; i<n; i++){
        DetailReception d = cont.findDetailReceptionByNumBille(request.getParameter("billes["+i+"][bille]"));
        liste.add(d);
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title>MODULE DE BILLONNAGE</title>
    <link rel="stylesheet" href="/resources/css/font-awesome/css/font-awesome.min.css">
        <link href="/resources/css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<style type="text/css">
		@page { margin: 0cm; padding-left: 40px; padding-right: 40px;}
		p { margin-bottom: 0.25cm; line-height: 120% }
		td p { margin-bottom: 0cm }
                body{
                    font-size: 8pt;
                    padding: 30px;
                }
                @media print{
                    .no-print{
                        display: none;
                    }
                }
	</style>
</head>
    <body lang="fr-FR" dir="ltr">
        <button class="no-print btn-lg btn-primary" onclick="window.print();"><i class="fa fa-print"></i> Imprimer</button>
	<div style="text-align: center; background-color: #808080; padding: 10px; width: 70%; margin-left: 15%; border-radius: 20px; margin-bottom: 50pt;">
		<p ><b>******************************</b></p>
                <p style="font-size: 18pt"><b>MODULE DE BILLONNAGE</b></p>
		<p style="font-weight: normal">………………………………………</p>
		<p style="font-size: 18pt"><b>N°</b><span style="font-weight: normal">:
                    </span><b style="margin-left: 150px;">Du</b><span style="font-weight: normal">
		 </span></p>
	</div>
        <table class="table table-bordered table-striped ">
            <tr><td><span style="font-weight: bold">Epaisseur </span> <input type="text" placeholder="saisissez l'épaisseur" style="border:none" ></td></tr>
        </table>
            <table class="table table-striped"  cellpadding="3" cellspacing="0">
                <tr valign="top" style="border: 1px solid rgba(18, 52, 86, 0.7);">
                        <td  height="15" style="border: 1px solid rgba(18, 52, 86, 0.7); padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                <p><font  style="font-size: 8pt">N° Bille</font></p>
                        </td>
                        <td  style="border: 1px solid rgba(18, 52, 86, 0.7); border: 1px solid rgba(18, 52, 86, 0.7);  padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                <p><font  style="font-size: 8pt">N° Travail</font></p>
                        </td>
                        <td  style="border: 1px solid rgba(18, 52, 86, 0.7);border: 1px solid rgba(18, 52, 86, 0.7);   padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                <p><font  style="font-size: 8pt">Essence</font></p>
                        </td>
                        <td  style="border: 1px solid rgba(18, 52, 86, 0.7);border: 1px solid rgba(18, 52, 86, 0.7);   padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                <p><font  style="font-size: 8pt">Fournisseurs</font></p>
                        </td>
                        <td  style="border: 1px solid rgba(18, 52, 86, 0.7);border: 1px solid rgba(18, 52, 86, 0.7);   padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                <p><font  style="font-size: 8pt">Dim Recept</font></p>
                        </td>
                        <td  style="border: 1px solid rgba(18, 52, 86, 0.7);border: 1px solid rgba(18, 52, 86, 0.7);   padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                <p><font  style="font-size: 8pt">Refraction</font></p>
                        </td>
                        <td  style="border: 1px solid rgba(18, 52, 86, 0.7); padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                <p><font  style="font-size: 8pt">Certificat</font></p>
                        </td>
                        <td colspan="5"  style="border: 1px solid rgba(18, 52, 86, 0.7); padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                <p align="center"><font  style="font-size: 8pt">Dim
                                Travaillées</font></p>
                        </td>
                </tr>
                <% double vol = 0; DecimalFormat df = new DecimalFormat("#.###"); for(DetailReception dr : liste){ %>
                <tr valign="top">
                        <td  style="border: 1px solid rgba(18, 52, 86, 0.7);font-size: 8pt; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                <p><br/>
                                    <%=dr.getNumBille()%>
                                </p>
                        </td>
                        <td  style="border: 1px solid rgba(18, 52, 86, 0.7); padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                <p><br/>
                                </p>
                        </td>
                        <td  style="border: 1px solid rgba(18, 52, 86, 0.7);font-size: 8pt;    padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                <p><br/><%="["+dr.getRefEssence().getRefEssence()+"]  "+dr.getRefEssence().getLibelle()%></p>
                        </td>
                        <td  style="border: 1px solid rgba(18, 52, 86, 0.7); padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                            <p style="font-size: 8pt"><br/>
                                    <%
                                        if(dr.getRefReception() != null && dr.getRefReception().getRefCommande()!=null && dr.getRefReception().getRefCommande().getRefFournisseur() != null && dr.getRefReception().getRefCommande().getRefFournisseur().getNomFournisseur() !=null)
                                            out.println(dr.getRefReception().getRefCommande().getRefFournisseur().getNomFournisseur());
                                    %>
                                </p>
                        </td>
                        <td  style="border: 1px solid rgba(18, 52, 86, 0.7);font-size: 10px;     padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                <p style="font-size: 8pt"><br/>
                                    <%vol += dr.getLongueur()*dr.getDiaMoyen()/10000; out.print(df.format(dr.getLongueur()/100)+" * "+df.format(dr.getDiaMoyen()/100)+" = "+df.format(dr.getLongueur()*dr.getDiaMoyen()/10000));%>
                                </p>
                        </td>
                        <td  style="border: 1px solid rgba(18, 52, 86, 0.7); padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                <p style="font-size: 8pt"><br/>
                                    <%=dr.getObservation()%>
                                </p>
                        </td>
                        <td  style="border: 1px solid rgba(18, 52, 86, 0.7);padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                <p style="font-size: 8pt"><br/>
                                    <%=new ParcJpaController(Database.getEntityManager()).getParcByBille(dr.getNumBille()).getRefTypeparc().getLibelle()%>
                                </p>
                        </td>
                        <td  style="border: 1px solid rgba(18, 52, 86, 0.7);width: 90px; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                <p><br/>

                                </p>
                        </td>
                        <td  style="border: 1px solid rgba(18, 52, 86, 0.7);width: 90px; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                <p><br/>

                                </p>
                        </td>
                        <td  style="border: 1px solid rgba(18, 52, 86, 0.7);width: 90px; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                <p><br/>

                                </p>
                        </td>
                        <td  style="border: 1px solid rgba(18, 52, 86, 0.7);width: 90px; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                <p><br/>

                                </p>
                        </td>
                        <td  style="border: 1px solid rgba(18, 52, 86, 0.7);width: 90px; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                <p><br/>

                                </p>
                        </td>
                </tr>
                <% } %>
                <tr valign="top">
                        <td colspan="4"  style="border: 1px solid rgba(18, 52, 86, 0.7); padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                <p><font  style="font-size: 8pt">Total&nbsp;: <%=liste.size()%> Billes</font></p>
                        </td>
                        <td  style="border: 1px solid rgba(18, 52, 86, 0.7); padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                            <p><font  style="font-size: 8pt">Vol. Reception: <%=df.format(vol)%></font></p>
                        </td>
                </tr>
        </table>
        <p style="margin-bottom: 0cm; line-height: 100%"><br/>

        </p>
        <script src="/resources/js/my/jquery.min.js"></script>
        <script src="/resources/css/bootstrap/dist/js/bootstrap.min.js"></script>
    </body>
</html>