<%-- 
    Document   : parc-add
    Created on : 5 janv. 2019, 14:13:25
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-parc">
    <p class="status-edit-parc" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REF PARC</label>

          <div class="col-sm-10">
              <input type="text" readonly="" required="" class="form-control" id="zone-ref" placeholder="Reference du Parc">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">LIBELLE</label>

          <div class="col-sm-10">
              <input type="text"  required="" class="form-control" id="zone-libelle" placeholder="libelle du parc">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">NUM BILLE</label>

          <div class="col-sm-10">
              <select required="" style="width:100%" id="zone-numBille" ></select>
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">TYPE MVT</label>

          <div class="col-sm-10">
              <select style="width: 100%;" id="zone-typeMvt"></select>
          </div>
          </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">DATE MVT</label>

          <div class="col-sm-10">
              <input type="date"  required="" class="form-control" id="zone-dateMvt" >
          </div>
          </div>
          <div class="form-group">
                <label for="zone-epaisseur" class="col-sm-2 control-label">REF TYPEPARC</label>

                <div class="col-sm-10">
                    <select style="width: 100%;" class="form-control" id="zone-refTypeparc"></select>
                </div>
        </div>
        </div>
        </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-add-parc"><i class="fa fa-pencil"></i> Ajouter</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        $("#btn-add-parc").click(function(){
            saveData($('#zone-ref').val(), $('#zone-libelle').val(), $('#zone-numBille').val(), $('#zone-typeMvt').val()
                    , $('#zone-dateMvt').val(), $('#zone-refTypeparc').val());
            $('#form-edit-parc .overlay').removeClass('hide');
            $('#form-edit-parc #btn-add-parc').addClass("hide");
        });
        $('#zone-ref').val("REF_PARC_"+(new Date().getTime()));
         $.ajax({
            url: '/api/type_parc/list',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var data = JSON.parse(jqXHR.responseText);
                var select = document.getElementById('zone-refTypeparc');
                var selected=-1;
                for(var i=0; i<data.length; i++){
                    var opt = document.createElement("option");
                    opt.value = data[i].ref;
                    opt.innerHTML = data[i].libelle;
                    select.appendChild(opt);
                }
                document.getElementById('zone-refTypeparc').selectedIndex = selected;
                $('select').select2();
            }
        });
        
        
         $.ajax({
            url: '/api/reception/list/numero_bille',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var data = JSON.parse(jqXHR.responseText);
                var select = document.getElementById('zone-numBille');
                var selected=0;
                var opt = document.createElement("option");
                opt.value = "";
                opt.innerHTML = "-- Selectionnez une bille --";
                select.appendChild(opt);
                for(var i=0; i<data.length; i++){
                    opt = document.createElement("option");
                    opt.value = data[i];
                    opt.innerHTML = data[i];
                    select.appendChild(opt);
                }
                document.getElementById('zone-numBille').selectedIndex = selected;
                $('select').select2();
            }
        });
        
         $.ajax({
            url: '/api/type_mouvement_parc',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var data = JSON.parse(jqXHR.responseText);
                var select = document.getElementById('zone-typeMvt');
                var selected=0;
                var opt = document.createElement("option");
                opt.value = "";
                opt.innerHTML = "-- Selectionnez un mouvement --";
                select.appendChild(opt);
                for(var i=0; i<data.length; i++){
                    opt = document.createElement("option");
                    opt.value = data[i];
                    opt.innerHTML = data[i];
                    select.appendChild(opt);
                }
                document.getElementById('zone-typeMvt').selectedIndex = selected;
                $('select').select2();
            }
        });
    </script>
</div>

