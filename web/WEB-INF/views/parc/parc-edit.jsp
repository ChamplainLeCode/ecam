<%-- 
    Document   : parc-edit
    Created on : 5 janv. 2019, 14:13:54
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-parc">
    <p class="status-edit-parc" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REF PARC</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<% out.print(request.getParameter("ref"));%>" required="" class="form-control" id="zone-refParc" placeholder="Reference du Parc">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">LIBELLE</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("libelle"));%>" class="form-control" id="zone-libelle" placeholder="libelle du parc">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">NUM BILLE</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("numBille"));%>" class="form-control" id="zone-numBille" placeholder="numero de la bille">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">TYPE MVT</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("typeMvt"));%>" class="form-control" id="zone-typeMvt" placeholder="type de mouvement">
          </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">DATE MVT</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("dateMvt"));%>" class="form-control" id="zone-dateMvt" placeholder="date du mouvement">
          </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">REF TYPEPARC</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("refTypeparc"));%>" class="form-control" id="zone-refTypeparc" placeholder="reference du type de parc">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" id="btn-close-editPane" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-parc"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        $("#btn-edit-parc").click(function(){
            editData($('#zone-refParc').val(), $('#zone-libelle').val(), $('#zone-numBille').val(), $('#zone-typeMvt').val(), $('#zone-dateMvt').val()
                    , $('#zone-refTypeparc').val());
            $('#form-edit-parc .overlay').removeClass('hide');
            $('#form-edit-parc #btn-edit-parc').addClass("disabled");
        });

    </script>
</div>

