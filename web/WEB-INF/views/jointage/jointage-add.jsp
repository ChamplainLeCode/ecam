<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-add-jointage">
    <p class="status-add-jointage text-red" id="status-add-jointage" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal " onsubmit="return save()">
      <div class="box-body">
            <div class="container-fluid" >
                <div class="form-group col-sm-4 col-lg-4 col-md-4">
                    <label>Colis à joindre</label>
                    <select class="select2" multiple="" required="" style="width: 100%;" id="zone-colise"></select>
               </div>
                <div class="pull-left">
                    <label class="label-danger" style="padding: 10px; margin-left: 20px; display: none; color: white;" id="status-count"></label>
                </div>
                <div class="form-group col-sm-4 col-lg-4 col-md-4 pull-right">
                    <label>Épaisseur</label>
                    <input type="text" readonly=""  required="" class="form-control" id="zone-epaisseur" placeholder="Épaisseur de la feuille">   
               </div>
            </div>
            <table class="table" style="text-align: center; cursor: pointer">
                <thead>
                    <tr>
                        <th class="btn-success disabled" style="text-align: center;" colspan="5">Contenu</th>
                        <th class="btn-primary" style="text-align: center;" colspan="2">Colis</th>
                        <th class="btn-warning" style="text-align: center;" colspan="2">Reste</th>
                    </tr>
                    <tr>
                        <th style="text-align: center;"class="btn-success" ><label for="zone-longueur-feuille">Longueur</label></th>
                      <th style="text-align: center;"class="btn-success" >Largeur</th>
                      <th style="text-align: center;"class="btn-success" >Nbr Feuil/Pqt</th>
                      <th style="text-align: center;"class="btn-success" >Nbr Paquets</th>
                      <th style="text-align: center;"class="btn-success" >Feuil Dern. Pqt</th>
                      <th style="text-align: center;"class="btn-primary" >Longueur</th>
                      <th style="text-align: center;"class="btn-primary" >Largeur</th>
                      <th style="text-align: center;"class="btn-warning" >Pqt Restant</th>
                      <th style="text-align: center;"class="btn-warning" >Feuil Defect</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="btn-success"  ><input type="number" required="" pattern="[1234567890]+" id="zone-longueur-feuille" placeholder="0" style="text-align: center;" class="form-control"></td>
                        <td class="btn-success" ><input type="number" required="" pattern="[1234567890]+" id="zone-largeur-feuille" placeholder="0" style="text-align: center;" class="form-control"></td>
                        <td class="btn-success" ><input type="number" required="" pattern="[1234567890]+" id="zone-feuille" placeholder="0" style="text-align: center;" class="form-control"></td>
                        <td class="btn-success" ><input type="number" required="" pattern="[1234567890]+" id="zone-paquet" placeholder="0" style="text-align: center;" class="form-control"></td>
                        <td class="btn-success" ><input type="number" required="" pattern="[1234567890]+" id="zone-paquet-dernier" placeholder="0" style="text-align: center;" class="form-control"></td>
                        <td class="btn-primary" ><input type="number" required="" pattern="[1234567890]+" id="zone-longueur-colis" placeholder="0" style="text-align: center;" class="form-control"></td>
                        <td class="btn-primary" ><input type="number" required="" pattern="[1234567890]+" id="zone-largeur-colis" placeholder="0" style="text-align: center;" class="form-control"></td>
                        <td class="btn-warning" ><input type="number" required="" pattern="[1234567890]+" id="zone-paquet-rest" placeholder="0" style="text-align: center;" class="form-control"></td>
                        <td class="btn-warning" ><input type="number" required="" pattern="[1234567890]+" id="zone-defectueuse" placeholder="0" style="text-align: center;" class="form-control"></td>
                    </tr>
                </tbody>
            </table>
          
          <table class="table table-bordered table-hover" style="margin-top: 30px;">
              <thead>
                  <tr>
                      <th>N° Colis</th>
                      <th>Nbr Paquets Restants</th>
                  </tr>
              </thead>
              <tbody id="table-jointage"></tbody>
          </table>
          
          
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
          <button type="button" id="btn-close" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="submit" class="btn btn-info pull-right" id="btn-save-jointage">Enregistrer</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        
        getColis(document.getElementById('zone-colise'));
            
        window.save = function(){
            var data = [];
            
            let pqtrest = $('#zone-paquet-rest').val();
            let pqtByNbr = $('.pqt-rest');
            let v = 0; 
            
            pqtByNbr.each(function(){
               v += parseInt($(this).val()); 
            });
            

            
            if(pqtrest != v){
                $('#status-count').html('Veuillez revérifier le nombre de paquets restants').show();
                setTimeout(()=>$('#status-count').hide(),10000);
                return false;
            }
            data = $('#zone-colise').val();
/*            $('.zone-colis').each(function(){
                data.push(this.value);
            });
*/          
            let colisUpdate = {};
            pqtByNbr.each(function(){
                /**
                 * On groupe les colis puis pour chacun on indique le nbre de paquet restant / effectif
                 * c'est à dire pour un colis on aura
                 *  [
                 *      { groupe: 32, effectif: 1 },
                 *      { groupe: 30, effectif: 0 }
                 *  ]
                 *  il reste 1 paquet de 32 feuilles et 0 paquet de 30 feuilles
                 */
                if(colisUpdate[$(this).attr('data')] === undefined)
                    colisUpdate[$(this).attr('data')] = [];
                colisUpdate[$(this).attr('data')+""].push({groupe: $(this).attr('effectif'), effectif : $(this).val()});
            });
            let map = {};
            let donnees = $('#zone-colise').val();
            for(var i = 0; i < donnees.length; i++)
                map[donnees[i]] = {taille: colisUpdate[donnees[i]].length};
            console.log(map);
            $.ajax({
                url: '/jointage/add',
                method: 'POST',
                data: {
                    paquet: $('#zone-paquet').val(),
                    feuilleParPaquet: $('#zone-feuille').val(),
                    epaisseur: $('#zone-epaisseur').val(),
                    longueurFeuille: $('#zone-longueur-feuille').val(),
                    largeurFeuille: $('#zone-largeur-feuille').val(),
                    longueurColis: $('#zone-longueur-colis').val(),
                    largeurColis: $('#zone-largeur-colis').val(),
                    colis: $('#zone-colise').val(),
                    nbrPaquetRestant: $('#zone-paquet-rest').val(),
                    nbrFeuilleDefectueuse: $('#zone-defectueuse').val(),
                    nbrFeuilleDernierPaquet: $('#zone-paquet-dernier').val(),
                    colisUpdate: colisUpdate,
                    map: map
                },
                complete: function (jqXHR, textStatus ) {
                    var data = jqXHR.responseJSON;
                    if(data.error !== undefined && data.error !== null){
                        $('.status-add-jointage').html(data.error).show();
                        $('#status-add-jointage').html(data.error).show();
                        return;
                    }
                    if(data.resultat === true){
                        $('#btn-close').click();
                        loadData();
                    }
                }
            });
            return false;
        };
        
        var nbr = 0;
        $('#btn-add-zone-colis').click(function(){
            var zone = document.createElement("div");
            var label = document.createElement("label");
            var select = document.createElement("select");
            var button = document.createElement("button");
            var div = document.createElement("div");
            var i = document.createElement("icon");
            
            label.setAttribute("class", "col-sm-3");
            label.innerHTML = "N° Colis "+(nbr+1);
            div.setAttribute("class","col-sm-8");
            select.style.width = "100%";
            select.setAttribute("required","");
            select.id = "zone-colis-"+nbr;
            select.setAttribute("class", "select2 zone-colis");
            button.className = "btn btn-danger";
            button.onclick = function(){zone.remove();};
            i.setAttribute("class", "fa fa-trash");
            button.appendChild(i);
            zone.setAttribute("style", "margin-bottom: 5px;");
            div.appendChild(select);
            zone.appendChild(label);
            zone.appendChild(button);
            zone.appendChild(div);
            document.getElementById('zone-colis').appendChild(zone);
            
            $(select).select2();
            getColis(select);
            nbr++;
        });
        
        $('#zone-colise').change(function(){
            let table = document.querySelector('#table-jointage');
            table.innerHTML = '';
            let colis = $(this).val();
            for(let i=0; i<colis.length; i++){
                let row = document.createElement("tr");
                let td = document.createElement("td");
                td.innerHTML = colis[i];
                row.appendChild(td);
                td = document.createElement("td");
                td.innerHTML = '';
                    $.ajax({
                        url: '/api/colis/group_by/paquet_number',
                        method: 'GET',
                        data: {colis: colis[i]},
                        dataType: 'json',
                        async: false,
                        complete: function (jqXHR, textStatus ) {
                            let data = jqXHR.responseJSON;
                            if(data === null)
                                return;
                            for(let ti in data){
                                td.innerHTML += '<span > Paquet de '+ti+'   ==>      <input data="'+colis[i]+'" effectif="'+ti+'" class="pqt-rest" type="text" pattern="[1234567890]+" style="text-align: center; border: 1px red solid; width: 50px; margin-top: 5px;" required="" min="0" max="'+data[ti]+'" value="0"> / '+data[ti]+"<br>";
                            }
                        }
                    });
                row.appendChild(td);
                table.appendChild(row);
                
            }
            
        });
        $('select').select2();
    </script>
</div>
