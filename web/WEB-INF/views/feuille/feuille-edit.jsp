<%-- 
    Document   : feuille-edit
    Created on : 30 janv. 2019, 17:32:39
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-feuille">
    <p class="status-edit-feuille" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REFERENCE</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<% out.print(request.getParameter("ref"));%>" required="" class="form-control" id="zone-ref" placeholder="Reference de la Feuille">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">RefPlot</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("plot"));%>" class="form-control" id="zone-plot" placeholder="Plot du Feuille">
          </div>
        </div>
        
        
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" id="btn-close-editPane" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-feuille"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        $("#btn-edit-feuille").click(function(){
            editData($('#zone-cni').val(), $('#zone-plot').val());
            $('#form-edit-feuille .overlay').removeClass('hide');
            $('#form-edit-feuille #btn-edit-feuille').addClass("disabled");
        });

    </script>
</div>
