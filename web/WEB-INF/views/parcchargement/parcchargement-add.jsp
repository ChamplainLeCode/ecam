<%-- 
    Document   : parcchargement-add
    Created on : 13 janv. 2019, 19:50:24
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-parcchargement">
    <p class="status-edit-parcchargement" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal" onsubmit="return save();">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REFERENCE</label>

          <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="zone-ref" placeholder="Reference Parc de Chargement">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-libelle" class="col-sm-2 control-label">Libelle</label>

          <div class="col-sm-10">
              <input type="text"  required="" class="form-control" id="zone-libelle" placeholder="libelle du parc de chargement">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-origine" class="col-sm-2 control-label">Origine </label>

          <div class="col-sm-10">
              <select required="" style="width:100%;" class="select2" id="zone-origine" ></select>
          </div>
        </div>
        <div class="form-group">
          <label for="zone-fournisseur" class="col-sm-2 control-label">Fournisseur </label>

          <div class="col-sm-10">
              <select required="" style="width:100%;" class="select2" id="zone-fournisseur" ></select>
          </div>
        </div>
        <div class="form-group">
          <label for="zone-certificat" class="col-sm-2 control-label">Certificat</label>

          <div class="col-sm-10">
              <input type="text"  required="" class="form-control" id="zone-certificat" placeholder="certificat Fournisseur">
          </div>
        </div>
          
        <div>
            <legend>Titre</legend>
            
            <div class="form-group">
              <label for="zone-titre-ref" class="col-sm-2 control-label">N° Titre</label>
              <div class="col-sm-10">
                  <input type="text"  required="" class="form-control" id="zone-titre-ref" placeholder="N° du titre">
              </div>
            </div>
            
            <div class="form-group">
              <label for="zone-titre-concession" class="col-sm-2 control-label">Concession</label>

              <div class="col-sm-10">
                  <input type="text"  required="" class="form-control" id="zone-titre-concession" placeholder="Concession du titre">
              </div>
            </div>
            
            <div class="form-group">
              <label for="zone-date-debut" class="col-sm-2 control-label">Date Début</label>

              <div class="col-sm-10">
                  <input type="date"  required="" class="form-control" id="zone-titre-date-debut">
              </div>
            </div>
            
            <div class="form-group">
              <label for="zone-date-fin" class="col-sm-2 control-label">Date Fin</label>

              <div class="col-sm-10">
                  <input type="date"  required="" class="form-control" id="zone-titre-date-fin">
              </div>
            </div>
            
            <div class="form-group">
              <label for="zone-titre-essence" class="col-sm-2 control-label">Essences</label>

              <div class="col-sm-10">
                  <select required="" multiple="" style="width: 100%;" class="select2" id="zone-titre-essence"></select>
              </div>
            </div>
        </div>
        
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="submit" class="btn btn-info pull-right" id="btn-add-parcchargement"><i class="fa fa-pencil"></i> Ajouter</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        function parseDate(dateStringify = (new Date().getYear()+1990)+"-"+new Date().getMonth()+"-"+new Date().getDate()){
            var date = dateStringify.split('-');
            return new Date(parseInt(date[0]), parseInt(date[1]), parseInt(date[2]));
        }
        window.save = function(){
            saveData(
            $('#zone-ref').val(), 
            $('#zone-libelle').val(), 
            $('#zone-origine').val(), 
            $('#zone-fournisseur').val(), 
            $('#zone-certificat').val(),
            $('#zone-titre-ref').val(),
            $('#zone-titre-concession').val(),
            parseDate($('#zone-titre-date-debut').val()).getTime(),
            parseDate($('#zone-titre-date-fin').val()).getTime(),
            $('#zone-titre-essence').val()
        );
            $('#form-edit-parcchargement .overlay').removeClass('hide');
            $('#form-edit-parcchargement #btn-add-parcchargement').addClass("hide");
            return false;
        }
        
        $.ajax({
                url: '/api/fournisseur/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    var fournisseurs = jqXHR.responseJSON; 
                    var select = document.getElementById('zone-fournisseur');
                    var selected=-1;
                    var opt = document.createElement("option");
                        opt.value = '';
                        opt.innerHTML = '-- Veuillez selectionner --';
                        opt.setAttribute("data", -1);
                        select.appendChild(opt);
                    for(var i=0; i<fournisseurs.length; i++){
                        var opt = document.createElement("option");
                        opt.value = fournisseurs[i].refFournisseur;
                        opt.innerHTML = fournisseurs[i].nomFournisseur+'('+fournisseurs[i].contact+")";
                        opt.setAttribute("data", i);
                        
                        select.appendChild(opt);
                    }
                }
            });

        $.ajax({
            url: '/api/type_parc/list',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var typeParcs = jqXHR.responseJSON; 
                var select = document.getElementById('zone-origine');
                var selected=-1;
                var opt = document.createElement("option");
                    opt.value = '';
                    opt.innerHTML = '-- Veuillez selectionner --';
                    opt.setAttribute("data", -1);
                    select.appendChild(opt);
                for(var i=0; i<typeParcs.length; i++){
                    var opt = document.createElement("option");
                    opt.value = typeParcs[i].ref;
                    opt.innerHTML = typeParcs[i].libelle;
                    opt.setAttribute("data", i);

                    select.appendChild(opt);
                }
            }
        });       

        $.ajax({
            url: '/api/essence/list',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var essence = jqXHR.responseJSON; 
                var select = document.getElementById('zone-titre-essence');
                var selected=-1;
                select.innerHTML = '';
                var opt = document.createElement("option");
                    opt.value = '';
                    opt.innerHTML = '-- Veuillez selectionner --';
                    select.appendChild(opt);
                for(var i=0; i<essence.length; i++){
                    var opt = document.createElement("option");
                    opt.value = essence[i].ref;
                    opt.innerHTML = essence[i].libelle;
                    select.appendChild(opt);
                }
            }
        });       
        
        $('select').select2();
    </script>
</div>
