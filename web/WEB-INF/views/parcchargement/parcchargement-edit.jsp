<%-- 
    Document   : parcchargement-edit
    Created on : 13 janv. 2019, 19:50:38
    Author     : KOUNOU BESSALA ERIC
--%>
<%@page import="app.models.Essence"%>
<%@page import="app.modelController.EssenceJpaController"%>
<%@page import="app.models.Titre"%>
<%@page import="app.models.Database"%>
<%@page import="app.modelController.TitreJpaController"%>
<%@page import="java.util.List"%>
<%@page import="java.util.List"%>
<%
    out.println(request.getParameter("ref"));
    List<Titre> titres = new TitreJpaController(Database.getEntityManager()).findTitreByRefParc(request.getParameter("ref"));
    Titre titre = titres.size()>0 ? titres.get(titres.size()-1) : null;
    List<Essence> listeEssence = new EssenceJpaController(Database.getEntityManager()).findEssenceEntities();
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-parcchargement">
    <p class="status-edit-parcchargement" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REFERENCE</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<%=request.getParameter("ref")%>" required="" class="form-control" id="zone-ref" placeholder="Reference Parc de Chargement">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Libelle</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<%=request.getParameter("libelle")%>" class="form-control" id="zone-libelle" placeholder="libelle du parc de chargement">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-origine" class="col-sm-2 control-label">Origine </label>

          <div class="col-sm-10">
              <select required="" style="width:100%;" class="select2" id="zone-origine" ></select>
          </div>
        </div>
        <div class="form-group">
          <label for="zone-fournisseur" class="col-sm-2 control-label">Fournisseur </label>

          <div class="col-sm-10">
              <select required="" style="width:100%;" class="select2" id="zone-fournisseur" ></select>
          </div>
        </div>
        <!--div class="form-group">
          <label for="zone-certificat" class="col-sm-2 control-label">Certificat</label>

          <div class="col-sm-10">
              <input type="text" value="<=request.getParameter("certificat")%>" required="" class="form-control" id="zone-certificat" placeholder="certificat Fournisseur">
          </div>
        </div-->
          <div>
            <legend>Titre</legend>
            
            <div class="form-group">
              <label for="zone-titre-ref" class="col-sm-2 control-label">N° Titre</label>
              <div class="col-sm-10">
                  <input type="text"  required="" class="form-control" readonly="" value="<%=(titre != null ? titre.getNumTitre() : "")%>" id="zone-titre-ref" placeholder="N° du titre">
              </div>
            </div>
            
            <!--div class="form-group">
              <label for="zone-titre-concession" class="col-sm-2 control-label">Concession</label>

              <div class="col-sm-10">
                  <input type="text"  required="" class="form-control" value="<=(titre != null ? titre.getConcession() : "")%>" id="zone-titre-concession" placeholder="Concession du titre">
              </div>
            </div>
            
            <div class="form-group">
              <label for="zone-date-debut" class="col-sm-2 control-label">Date Début</label>

              <div class="col-sm-10">
                  <input type="date"  required="" class="form-control" value="<=(titre != null ? titre.getDateDebut().getTime(): "")%>" id="zone-titre-date-debut">
              </div>
            </div>
            
            <div class="form-group">
              <label for="zone-date-fin" class="col-sm-2 control-label">Date Fin</label>

              <div class="col-sm-10">
                  <input type="date"  required="" class="form-control" value="<=(titre != null ? titre.getDateFin().getTime() : "")%>" id="zone-titre-date-fin">
              </div>
            </div-->
            
            <div class="form-group">
              <label for="zone-titre-essence" class="col-sm-2 control-label">Essences</label>

              <div class="col-sm-10"><option ></option>
                  <select required="" multiple="" style="width: 100%;" class="select2" id="zone-titre-essence">
                    <%
                        for(Essence e : listeEssence){
                            out.print("<option "+(titre.getEssenceList().contains(e) ? "selected" : "")+"  value=\""+e.getRefEssence()+"\">"+e.getLibelle()+"</option>");
                        }
                    %>
                  </select>
              </div>
            </div>
        </div>  
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" id="btn-close-editPane" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-parcchargement"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        $("#btn-edit-parcchargement").click(function(){
            editData(
                $('#zone-ref').val(), 
                $('#zone-libelle').val(), 
                $('#zone-origine').val(), 
                $('#zone-fournisseur').val(), 
                $('#zone-certificat').val(),
                $('#zone-titre-ref').val(),
                $('#zone-titre-essence').val()
        );
            $('#form-edit-parcchargement .overlay').removeClass('hide');
            $('#form-edit-parcchargement #btn-edit-parcchargement').addClass("disabled");
        });


        $.ajax({
                url: '/api/fournisseur/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    var fournisseurs = jqXHR.responseJSON; 
                    var select = document.getElementById('zone-fournisseur');
                    var selected=-1;
                    var opt = document.createElement("option");
                        opt.value = '';
                        opt.innerHTML = '-- Veuillez selectionner --';
                        opt.setAttribute("data", -1);
                        select.appendChild(opt);
                    for(var i=0; i<fournisseurs.length; i++){
                        var opt = document.createElement("option");
                        if(fournisseurs[i].refFournisseur === '<% out.print(request.getParameter("fournisseur[refFournisseur]"));%>') opt.selected = true;
                        opt.value = fournisseurs[i].refFournisseur;
                        opt.innerHTML = fournisseurs[i].nomFournisseur+'('+fournisseurs[i].contact+")";
                        opt.setAttribute("data", i);
                        
                        select.appendChild(opt);
                    }
                }
            });


        $.ajax({
                url: '/api/type_parc/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    var fournisseurs = jqXHR.responseJSON; 
                    var select = document.getElementById('zone-origine');
                    var selected=-1;
                    var opt = document.createElement("option");
                        opt.value = '';
                        opt.innerHTML = '-- Veuillez selectionner --';
                        opt.setAttribute("data", -1);
                        select.appendChild(opt);
                    for(var i=0; i<fournisseurs.length; i++){
                        var opt = document.createElement("option");
                        if(fournisseurs[i].ref === '<% out.print(request.getParameter("origine[ref]"));%>') opt.selected = true;
                        opt.value = fournisseurs[i].ref;
                        opt.innerHTML = fournisseurs[i].libelle;
                        opt.setAttribute("data", i);
                        
                        select.appendChild(opt);
                    }
                }
            });
        $('select').select2();
    </script>
</div>
