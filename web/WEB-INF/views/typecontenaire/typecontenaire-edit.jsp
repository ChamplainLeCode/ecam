<%-- 
    Document   : typecontenaire-edit
    Created on : 10 janv. 2019, 20:04:22
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-typecontenaire">
    <p class="status-edit-typecontenaire" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Reférence</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<% out.print(request.getParameter("ref"));%>" required="" class="form-control" id="zone-ref" placeholder="Rérérence">
          </div>
        </div>
        <div class="form-group">
          <label for="zone- libelle" class="col-sm-2 control-label">Libelle</label>

          <div class="col-sm-10">
              <input type="text" required="" value="<% out.print(request.getParameter("libelle"));%>" class="form-control" id="zone-libelle" placeholder="libelle de la typecontenaire">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-typecontenaire"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
    <script type="text/javascript">
        $("#btn-edit-typecontenaire").click(function(){
            editData($('#zone-libelle').val(), $('#zone-ref').val());
            $('#form-edit-typecontenaire .overlay').removeClass('hide');
            $('#form-edit-typecontenaire #btn-edit-typecontenaire').addClass("disabled");
        });

    </script>
</div>
