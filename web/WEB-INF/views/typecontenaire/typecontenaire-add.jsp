<%-- 
    Document   : typecontenaire-add
    Created on : 10 janv. 2019, 20:04:09
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-typecontenaire">
    <p class="status-edit-typecontenaire" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REFERENCE</label>

          <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="zone-ref" placeholder="reference typecontenaire">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Libelle</label>

          <div class="col-sm-10">
              <input type="text"  required="" class="form-control" id="zone-libelle" placeholder="libelle  du typecontenaire">
          </div>
        </div>
        
             </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-add-typecontenaire"><i class="fa fa-save"></i> Enregistrer</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        $("#btn-add-typecontenaire").click(function(){
            saveData($('#zone-ref').val(), $('#zone-libelle').val());
            $('#form-edit-typecontenaire .overlay').removeClass('hide');
            $('#form-edit-typecontenaire #btn-add-typecontenaire').addClass("hide");
        });

    </script>
</div>

