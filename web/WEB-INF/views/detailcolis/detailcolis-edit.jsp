<%-- 
    Document   : detail_colis-edit
    Created on : 6 févr. 2019, 11:06:07
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-detail_colis">
    <p class="status-edit-detail_colis" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REF</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<% out.print(request.getParameter("ref"));%>" required="" class="form-control" id="zone-ref" placeholder="Reference Detail_Colis">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Largeur</label>

          <div class="col-sm-10">
              <input type="number"  required="" value="<% out.print(request.getParameter("largeur"));%>" class="form-control" id="zone-largeur" placeholder="Largeur Detail_Colis">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Longueur</label>

          <div class="col-sm-10">
              <input type="number"  required="" value="<% out.print(request.getParameter("longueur"));%>" class="form-control" id="zone-longueur" placeholder="Longueur DeatilColis">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">ColisRef</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("colisRef"));%>" class="form-control" id="zone-colisRef" placeholder="ColisRef du Detail_Colis">
          </div>
        </div>
          
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">PaquetRef</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("paquetRef"));%>" class="form-control" id="zone-paquetRef" placeholder="PaquetRef du Detail_Colis">
          </div>
        </div>
          
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Volume</label>

          <div class="col-sm-10">
              <input type="float"  required="" value="<% out.print(request.getParameter("volume"));%>" class="form-control" id="zone-volume" placeholder="Volume du Detail_Colis">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" id="btn-close-editPane" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-detail_colis"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        $("#btn-edit-detail_colis").click(function(){
            editData($('#zone-ref').val(), $('#zone-largeur').val(), $('#zone-longueur').val(), $('#zone-colisRef').val(), $('#zone-paquetRef').val()
                    , $('#zone-volume').val());
            $('#form-edit-detail_colis .overlay').removeClass('hide');
            $('#form-edit-detail_colis #btn-edit-detail_colis').addClass("disabled");
        });

    </script>
</div>

