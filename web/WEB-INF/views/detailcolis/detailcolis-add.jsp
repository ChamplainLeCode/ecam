<%-- 
    Document   : detail_colis-add
    Created on : 6 févr. 2019, 11:05:48
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-detail_colis">
    <p class="status-edit-detail_colis" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REF</label>

          <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="zone-ref" placeholder="Reference Detail_Colis">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Largeur</label>

          <div class="col-sm-10">
              <input type="number"  required="" class="form-control" id="zone-largeur" placeholder="Largeur Detail_Colis">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Longueur</label>

          <div class="col-sm-10">
              <input type="number"  required="" class="form-control" id="zone-longueur" placeholder="Longueur DeatilColis">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">ColisRef</label>

          <div class="col-sm-10">
              <input type="text"  required="" class="form-control" id="zone-colisRef" placeholder="ColisRef du Detail_Colis">
          </div>
        </div>
          
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">PaquetRef</label>

          <div class="col-sm-10">
              <input type="text"  required="" class="form-control" id="zone-paquetRef" placeholder="PaquetRef du Detail_Colis">
          </div>
        </div>

          
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Volume</label>

          <div class="col-sm-10">
              <input type="float"  required="" class="form-control" id="zone-volume" placeholder="Volume du Detail_Colis">
          </div>
        </div>

      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-add-detail_colis"><i class="fa fa-pencil"></i> Ajouter</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        $("#btn-add-detail_colis").click(function(){
            saveData($('#zone-ref').val(), $('#zone-largeur').val(), $('#zone-longueur').val(), $('#zone-colisRef').val(), $('#zone-paquetRef').val()
                    , $('#zone-volume').val());
            $('#form-edit-detail_colis .overlay').removeClass('hide');
            $('#form-edit-detail_colis #btn-add-detail_colis').addClass("hide");
        });

    </script>
</div>
