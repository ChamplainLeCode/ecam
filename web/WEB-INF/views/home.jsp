<%-- 
    Document   : totoPage
    Created on : 8 déc. 2018, 14:42:16
    Author     : champlain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Ecam Home</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <%@include file="/WEB-INF/views/layouts/style.jsp" %>

       </head>
    <!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
    -->
    <body class="hold-transition skin-blue sidebar-collapse">
    <div class="wrapper">

      <!-- Main Header -->
      <%@include file="/WEB-INF/views/layouts/topBar.jsp" %>

      <!-- Left side column. contains the logo and sidebar -->
      <%@include file="/WEB-INF/views/layouts/sideBar.jsp" %>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" style="background-color: rgba(0, 0, 0, 0.5)">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          
          
        </section>

        <!-- Main content -->
        <section class="content container-fluid" >
            <div class="container" style=" width: 900px; color: pink; font-size: 30px; height: 50px; margin-bottom: 50px;"><div class="bound pull-left" style="margin-left: -110px; margin-right: 5px;"><i class="fa fa-arrow-up " style="animation: infinite; "></i></div> <span id="explication-message"> Cliquez-ici </span></div>
            <div class="container" style="width: 900px; margin-top: 100px;">
                <p class="animate animate-first" style="text-align: center; font-size: 101px; color: whitesmoke">ECAM-PLACAGES</p>
                <p class="animate animate-second" style="text-align: center; color: whitesmoke; height: 20px; border: whitesmoke 4px solid;"></p>
                <p class="animate animate-thirth" style="text-align: center; font-size: 33px; color: whitesmoke">Compagnie d'explotation industrielle des bois du Cameroun</p>
            </div>
          <!--------------------------
            | Your Page Content Here |
            -------------------------->

        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <%@include file="/WEB-INF/views/layouts/footer.jsp" %>

    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->
    <%@include file="/WEB-INF/views/layouts/script.jsp" %>
    <style>
        .bound{
            position: relative;
            animation-name: bounceup;
            animation-duration: 0.5s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
            transform-origin: bottom;
        }
        @keyframes bounceleft{
            0%   { transform:  translateX(0px); }
            50%  { transform:  translateX(-20px); }
            100% { transform:  translateX(0px); }
        }
        @keyframes bounceup{
            0%   { transform:  translateY(0px); }
            50%  { transform:  translateY(-20px); }
            100% { transform:  translateY(0px); }
        }
        .animate{
            position: relative;
            left: 150%;
        }
    </style>
    <script>               
        $(function(){
            let step = 0;
            $('#toggle-navigation').click(function(){
                if(step === 0){
                    $('.bound>i').attr('class', 'fa fa-arrow-left').css({'color': 'white'});
                    $('.bound').css({'animation-name': 'bounceleft'});
                    $('#explication-message').html('<span style="font-size: 25px; color: orange">Menu principal<br>Cliquez pour effectuer une tache</span>');
                    step++;
                }else{
                    $('.bound>i').attr('class', 'fa fa-arrow-up');
                    $('.bound').css({'animation-name': 'bounceup'});
                    $('#explication-message').html('<span >Cliquez ici</span>');
                    step = 0;
                
                }
            });
            
                setTimeout(()=>{
                    $('.animate-first').animate({
                        'left': '0px',
                    }, 1000);
                    setTimeout(()=>{
                        $('.animate-first').animate({
                            'left': '-150%',
                        }, 1000);
                        setTimeout(()=>{
                            $('.animate-first').animate({'left': '150%'}, 0);
                        },1000);
                    }, 4000);
                }, 1000);
                setTimeout(()=>{
                    $('.animate-second').animate({
                        'left': '0px',
                    }, 1000);
                    setTimeout(()=>{
                        $('.animate-second').animate({
                            'left': '-150%',
                        }, 1000);
                        
                    }, 4000);
                }, 1500);
                setTimeout(()=>{
                    $('.animate-thirth').animate({
                        'left': '0px',
                    }, 1000);
                    setTimeout(()=>{
                        $('.animate-thirth').animate({
                            'left': '-150%',
                        }, 1000);
                        setTimeout(()=>{
                            $('.animate-thirth').animate({'left': '150%'}, 0);
                        },1000);
                    }, 4000);
                }, 2000);
          
            setInterval(()=>{

                setTimeout(()=>{
                    $('.animate-first').animate({
                        'left': '0px',
                    }, 1000);
                    setTimeout(()=>{
                        $('.animate-first').animate({
                            'left': '-150%',
                        }, 1000);
                        setTimeout(()=>{
                            $('.animate-first').animate({'left': '150%'}, 0);
                        },1000);
                    }, 4000);
                }, 1000);
                setTimeout(()=>{
                    $('.animate-second').animate({
                        'left': '0px',
                    }, 1000);
                    setTimeout(()=>{
                        $('.animate-second').animate({
                            'left': '-150%',
                        }, 1000);
                        
                    }, 4000);
                }, 1500);
                setTimeout(()=>{
                    $('.animate-thirth').animate({
                        'left': '0px',
                    }, 1000);
                    setTimeout(()=>{
                        $('.animate-thirth').animate({
                            'left': '-150%',
                        }, 1000);
                        setTimeout(()=>{
                            $('.animate-thirth').animate({'left': '150%'}, 0);
                        },1000);
                    }, 4000);
                }, 2000);
            }, 6000);
        });
    </script>
    </body>
</html>
