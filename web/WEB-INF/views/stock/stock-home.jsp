<%-- 
    Document   : contenaire-home
    Created on : 6 févr. 2019, 13:29:53
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Ecam Web</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

      <%@include file="/WEB-INF/views/layouts/style.jsp" %>
      <link rel="stylesheet" href="/resources/css/select2/select2.min.css">
      <style>
          body{
              font-size: 8pt;
          }
      </style>
    </head>
    <!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
    -->
    <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <%@include file="/WEB-INF/views/layouts/topBar.jsp" %>

      <!-- Left side column. contains the logo and sidebar -->
      <%@include file="/WEB-INF/views/layouts/sideBar.jsp" %>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Page Header
            <small>Optional description</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="">ecam</li>
            <li class="active">contenaire</li>
          </ol>
          
          
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <form onsubmit="return loadFilter();"> 
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title pull-right"><i class="fa fa-filter"></i> Filtre</h3>
                    </div>

                    <div class="box-body form-inline">

                        <div class="form-group col-sm-3">
                            <label class=" col-sm-12">Données</label>
                            <select required="" class="select2 " id="zone-donnee" style="width: 100%;"> 
                                <option value="">--Selectionnez--</option>
                                <option value="billon">Billon </option>
                                <option value="billonnage">Billonnage </option>
                                <option value="client">Client</option>
                                <option value="colis">Colis</option>
                                <option value="embarquement">Embarquement</option>
                                <option value="empotage">Empotage</option>
                                <option value="fournisseur">Fournisseur</option>
                                <option value="lettre">Lettre Camion</option>
                                <option value="paquet">Paquet</option>
                                <option value="parc">Parc</option>
                                <option value="plot">Plot</option>
                            </select>
                        </div>


                        <div class="form-group col-sm-2">
                            <label class="col-sm-12"> Date début</label>
                            <input required="" type="date" id="zone-debut" class="form-control">
                        </div>

                        <div class="form-group col-sm-2">
                            <label class=" col-sm-12"> Date Fin</label>
                            <input required="" type="date" id="zone-fin" class="form-control">
                        </div>

                        <div class="form-group col-sm-2">
                            <label class=" col-sm-12">. </label>
                            <button type="submit" id="btn-charger" class="btn btn-primary">
                                <i class="fa fa-refresh"></i> Charger
                            </button>
                        </div>

                        <div class="form-group col-sm-3">
                            <label class=" col-sm-12">. </label>
                            <button type="button" id="btn-achat-grume" class="btn btn-success">
                                <i class="fa fa-shopping-cart"></i> Achats Grumes
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            
            <div class="box" s>
                <div class="box-header">
                  <h3 class="box-title">Données</h3>
                  <button class="btn btn-primary btn-block pull-right" style="width:150px;" id="btn-imprimer"> <i class="fa fa-print"></i> Imprimer</button>
                </div>

                <div class="box-body">
                    <div id="liste_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="liste" class="table table-hover table-bordered table-striped dataTable" role="grid" aria-describedby="liste_info">
                                    
                                    <tbody id="table-body"></tbody>
                              </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </section>
        
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <%@include file="/WEB-INF/views/layouts/footer.jsp" %>

    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->

    <%@include file="/WEB-INF/views/layouts/script.jsp" %>
    <script src="/resources/js/select2/select2.full.min.js"></script>
    <script src="/resources/js/jquery.dataTables.min.js"></script>
    <script src="/resources/js/dataTables.bootstrap.min.js"></script>
    <script src="/resources/js/codeBarre/JsBarcode.all.min.js"></script>
    <script src="/resources/js/stock/stock.js"></script>
    <script>
        $(function () {
            
            $('#zone-donnee').select2();
            document.getElementById("btn-imprimer").onclick = function(){
                tableToExcel('liste', 'W3C Example Table');
            };
            $('#btn-achat-grume').click(function(){
               window.open('/contenaire/essence', 'ECAM - ACHATS GRUM'); 
            });
        });
    </script>
    </body>
</html>
