<%-- 
    Document   : contenaire-add
    Created on : 6 févr. 2019, 13:29:42
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<style>
    
    .select2{
        color: black;
    }
</style>
<script>
    function add(arg){
        var url = "/"+arg+'/add';
        $.ajax({
            url: url,
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                $('#section-setting').html(jqXHR.responseText).show();
                $('#section').hide();
                $('#zone-footeur-section').addClass('hide');
            }
        });
    }
    function reload(arg){
        
        switch(arg){
            case 'chauffeur': getChauffeur(); break;
            case 'conteneur': getContenaire(); break;
            case 'compagnie' : getCompagnie(); break;
            case 'transporteur': getTransporteur(); break;
        }
    }
    
    function save(data, type){
        ////console.log('data = '+JSON.stringify(data)+' type = '+type);
        $.ajax({
            url: '/'+type+"/add",
            method: 'POST',
            dataType: 'json',
            data: data,
            complete: function (jqXHR, textStatus ) {
                $('#section-setting').html('').hide();
                $('#section').show();
                $('#zone-footeur-section').removeClass('hide');
                reload(type);
            }
        })
    }
</script>
<div id="form-edit-contenaire">
    <p class="status-edit-contenaire bg-red-active" style="padding: 5px; display: none; color: black; border-radius: 5px;"></p>
    <form class="form-horizontal" action="/contenaire/add" method="POST">
      <div class="box-body">
            <div id="section">
                <div id="section1">
                    <div class="form-group">
                        <label for="zone-ref" class="col-sm-2 control-label">N° Contenaire</label>
                        <div class="col-sm-10">
                            <select required="" class="pull-left select2 col-sm-8" style="width: 60%;" id="zone-conteneur" name="contenaire"></select>
                              <div class="col-sm-4 pull-right">
                                  <button class="btn btn-dropbox" onclick="add('conteneur')" style="margin-right: 5px;" type="button" id="btn-plus-contenaire" title="Ajouter un contenaire"><i class="fa fa-plus"></i></button>
                                  <button class="btn btn-dropbox" onclick="reload('conteneur')" type="button" id="btn-refresh-transporteur" title="Recharger la liste de contenaire"><i class="fa fa-refresh"></i></button>
                              </div>                  
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="zone-ref" class="col-sm-2 control-label">N° Plomb</label>
                      <div class="col-sm-10">
                          <input type="text" required="" class="form-control" id="zone-plomb" name="ref" placeholder="N° du Plomb">
                      </div>
                    </div>
                    <div class="form-group">
                      <label  class="col-sm-2 control-label">Date du Chargement</label>
                      <div class="col-sm-10">
                          <input type="date"  required="" name="date" class="form-control" id="zone-dateChargement" placeholder="DateChargement du Contenaire">
                      </div>
                    </div>
                </div>
                <div style="display: none;" id="section2">

                    <div class="form-group">
                        <div class="col-sm-4">
                            <label  class="col-sm-12 control-label" style="text-align: center;">Volume</label>

                          <div class="col-sm-12">
                              <input type="number" name="volume" required="" class="form-control" id="zone-volume" placeholder="Volume" readonly="">
                          </div>
                        </div>

                        <div class="col-sm-4">
                          <label  class="col-sm-12 control-label" style="text-align: center;">Volume Brut.</label>

                          <div class="col-sm-12">
                              <input type="number" name="volumeBrute" required="" class="form-control" id="zone-volume-brute" placeholder="brute" readonly="">
                          </div>
                        </div>

                        <div class="col-sm-4">
                          <label  class="col-sm-12 control-label" style="text-align: center;">Surface</label>

                          <div class="col-sm-12">
                              <input type="number" name="volume" required="" class="form-control" id="zone-surface" placeholder="Surface" readonly="">
                          </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <legend>Colis à embarquer</legend>
                        <table class="col-sm-12">
                            <thead><tr><th></th><th></th><th></th><th></th></tr></thead>
                            <tbody id="panel-colis"></tbody>
                        </table>
                    </div>
                </div>
                <div style="display: none;" id="section3">
                    <div class="form-group">
                      <label for="zone-ref" class="col-sm-2 control-label">Compagnie</label>
                      <div class="col-sm-10">
                        <div class="col-sm-8 pull-left">
                            <select class="select2" required="" name="compagnie" style="width: 100%; float: left" id="zone-compagnie">
                                <option value="">Selectionnez</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-dropbox" onclick="add('compagnie')" style="margin-right: 5px;" type="button" id="btn-plus-compagnie" title="Ajouter un compagnie"><i class="fa fa-plus"></i></button>
                            <button class="btn btn-dropbox" onclick="reload('compagnie')" type="button" id="btn-refresh-compagnie" title="Recharger la liste de compagnie"><i class="fa fa-refresh"></i></button>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                        <label for="zone-ref" class="col-sm-2 control-label">Chauffeur</label>
                        <div class="col-sm-10">
                              <div class="col-sm-8 pull-left">
                                  <select class="select2" required="" name="chauffeur" style="width: 100%; float: left" id="zone-chauffeur">
                                      <option value="">Selectionnez</option>
                                  </select>
                              </div>
                              <div class="col-sm-4">
                                  <button class="btn btn-dropbox" onclick="add('chauffeur')" style="margin-right: 5px;" type="button" id="btn-plus-chauffeur" title="Ajouter un chauffeur"><i class="fa fa-plus"></i></button>
                                  <button class="btn btn-dropbox" onclick="reload('chauffeur')" type="button" id="btn-refresh-chauffeur" title="Recharger la liste de chauffeurs"><i class="fa fa-refresh"></i></button>
                              </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Transporteur</label>
                        <div class="col-sm-10">
                          <div class="col-sm-8 pull-left">
                              <select class="select2" required="" name="transporteur" style="width: 100%; float: left" id="zone-transporteur">
                                  <option value="">Selectionnez</option>
                              </select>
                          </div>
                          <div class="col-sm-4">
                              <button class="btn btn-dropbox" onclick="add('transporteur')" style="margin-right: 5px;" type="button" id="btn-plus-transporteur" title="Ajouter un transporteur"><i class="fa fa-plus"></i></button>
                              <button class="btn btn-dropbox" onclick="reload('transporteur')" type="button" id="btn-refresh-transporteur" title="Recharger la liste de transporteurs"><i class="fa fa-refresh"></i></button>
                          </div>                  
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Immatricul.</label>
                        <div class="col-sm-10">
                            <input type="text" placeholder="Immatriculation véhicule" class="col-sm-8" style="margin-left: 10px; height: 30px; border-color: gray; border-radius: 5px;" id="zone-immatriculation">
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">N° Lettre Camion.</label>
                        <div class="col-sm-10">
                            <input type="text" placeholder="N° Lettre camion" class="col-sm-8" style="margin-left: 10px; height: 30px; border-color: gray; border-radius: 5px;" id="zone-lettre">
                        </div>
                    </div>
                </div>
                
                <div id="section4" style="display: none;">
                    
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Client</label>
                        <div class="col-sm-10">
                            <select class="select2" style="width: 90%" id="zone-client"></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Pays</label>
                        <div class="col-sm-10">
                            <select class="select2" style="width: 90%" id="zone-pays"></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Ville</label>
                        <div class="col-sm-10">
                            <select class="select2" style="width: 90%" id="zone-ville"></select>
                        </div>
                    </div>
                </div>
            </div>
          
            <div id="section-setting" style="display: none"></div>
        </div>
            
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent" id="zone-footeur-section">
            <button type="button" class="btn btn-primary btn-section pull-left" style="display:none;" id="btn-next-section-previous"><i class="fa fa-arrow-left"></i> Précédent</button>
            <button type="button" id="btn-close-addPane" style="display: none;" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-success pull-right" style="display:none;" id="btn-add-contenaire"><i class="fa fa-save"></i> Enregistrer</button>
            <button type="button" class="btn btn-primary pull-right btn-section" style="" id="btn-next-section-next"> Suivant <i class="fa fa-arrow-right"></i></button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
</div>
          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        function parseDate(dateStringify = (new Date().getYear()+1990)+"-"+new Date().getMonth()+"-"+new Date().getDate()){
            var date = dateStringify.split('-');
            return new Date(parseInt(date[0]), parseInt(date[1]), parseInt(date[2]));
        }

        
        var activeSection = 1;
        $('#btn-next-section-next').click(function(){
           if(activeSection < 4){
               $('#section'+(activeSection++)).hide();
               $('#section'+(activeSection)).show();
               $('#btn-next-section-previous').show();
               $('#btn-add-contenaire').hide();
           }
           if(activeSection === 4){
               getClient();
               getPays();
               getVilles();
               $(this).hide();
               $('#btn-add-contenaire').show();
           }
        });
        $('#btn-next-section-previous').click(function(){
           if(activeSection > 1){
               $('#section'+(activeSection--)).hide();
               $('#section'+(activeSection)).show();
               $('#btn-next-section-next').show();
           }
           if(activeSection === 1){
               $(this).hide();
           }
               $('#btn-add-contenaire').hide();
        });
        
        
        var listeColis = [];
        var refTimer = undefined;
        $("#btn-add-contenaire").click(function(){
            
            if($('#zone-ref').val() === ''){
                $('.status-edit-contenaire').html('Veuillez renseigner la reférence du contenaire').show(500);
                setTimer();
                return;
            }
            
            if($('#zone-plomb').val() === ''){
                $('.status-edit-contenaire').html('Veuillez fournir le N° du plomb du contenaire').show(500);
                setTimer();
                return;
            }

            if($('#zone-dateChargement').val() === ''){
                $('.status-edit-contenaire').html('Veuillez renseigner la date').show(500);
                setTimer();
                return;
            }
            
            if($('#zone-surface').val() === '' || $('#zone-volume').val() === ''){
                $('.status-edit-contenaire').html('Veuillez Choisir des colis à embarquer').show(500);
                setTimer();
                return;
            }
            if( listeColis.length == 0){//$('.zone-colis').val() === '' || $('#zone-colis').val() ==='[]' || $('#zone-colis').val().length <= 0){
                $('.status-edit-contenaire').html('Veuillez selectionner les colis à embarquer').show(500);
                setTimer();
                return;
            }
            if( $('#zone-compagnie').val() === ''){
                $('.status-edit-contenaire').html('Veuillez selectionner la compagnie maritime').show(500);
                setTimer();
                return;
            }
            if( $('#zone-chauffeur').val() === ''){
                $('.status-edit-contenaire').html('Veuillez renseigner le nom du conducteur').show(500);
                setTimer();
                return;
            }
            if( $('#zone-transporteur').val() === ''){
                $('.status-edit-contenaire').html('Veuillez selectionner le transporteur').show(500);
                setTimer();
                return;
            }
            if( $('#zone-immatriculation').val() === ''){
                $('.status-edit-contenaire').html('Veuillez indiquer le N° d\'immatriculation du Grumier').show(500);
                setTimer();
                return;
            }
            if( $('#zone-lettre').val() === ''){
                $('.status-edit-contenaire').html('Veuillez indiquer le N° de la lettre camion').show(500);
                setTimer();
                return;
            }
            if( $('#zone-client').val() === ''){
                $('.status-edit-contenaire').html('Veuillez choisir le client de l\'embarquement').show(500);
                setTimer();
                return;
            }
            if( $('#zone-pays').val() === ''){
                $('.status-edit-contenaire').html('Veuillez indiquer le pays de destination').show(500);
                setTimer();
                return;
            }
            if( $('#zone-ville').val() === ''){
                $('.status-edit-contenaire').html('Veuillez indiquer la ville de destination').show(500);
                setTimer();
                return;
            }
           if(confirm('Voulez-vous vraiment terminer l\'embarcation?') === true){
                saveData(
                        $('#zone-conteneur').val(), 
                        parseDate($('#zone-dateChargement').val()).getTime(), 
                        $('#zone-volume').val(), 
                        $('#zone-volume-brute').val(), 
                        listeColis, 
                        $('#zone-plomb').val(), 
                        $('#zone-surface').val(),
                        $('#zone-compagnie').val(),
                        $('#zone-chauffeur').val(),
                        $('#zone-transporteur').val(),
                        $('#zone-immatriculation').val(),
                        $('#zone-client').val(),
                        $('#zone-pays').val(),
                        $('#zone-ville').val(),
                        $('#zone-lettre').val()
                );
        
                $('#form-edit-contenaire .overlay').removeClass('hide');
                $('#form-edit-contenaire #btn-add-contenaire').addClass("hide");
            }
        
        });

        function setTimer(){
            if(refTimer === undefined)
                refTimer = setTimeout(function(){$('.status-edit-contenaire').hide(500);}, 5000);
            else{
                clearTimeout(refTimer);
                refTimer = setTimeout(function(){$('.status-edit-contenaire').hide(500);}, 5000);
            }
        }
        
        window.getCompagnie = function(){
           
            $.ajax({
                url: '/api/compagnie/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    
                    var select = document.getElementById('zone-compagnie');
                    var data = jqXHR.responseJSON;
                    var option = document.createElement("option");
                    select.innerHTML = '';
                    option.innerHTML = '-- Selectionnez --';
                    option.value = '';
                    select.appendChild(option);
                    for(var i=0; i<data.length; i++){
                        option = document.createElement("option");
                        option.value = data[i].ref;
                        option.innerText = data[i].libelle;
                        select.appendChild(option);
                    }
        //            $(select).select2();
                }
            });
        };
    
    
        
        window.getContenaire = function(){
           
            $.ajax({
                url: '/api/conteneur/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    
                    var select = document.getElementById('zone-conteneur');
                    var data = jqXHR.responseJSON;
                    var option = document.createElement("option");
                    select.innerHTML = '';
                    option.innerHTML = '-- Selectionnez --';
                    option.value = '';
                    select.appendChild(option);
                    for(var i=0; i<data.length; i++){
                        option = document.createElement("option");
                        option.value = data[i].ref;
                        option.innerText = data[i].libelle;
                        select.appendChild(option);
                    }
        //            $(select).select2();
                }
            });
        };
        
            
        window.getTransporteur = function(){
           
            $.ajax({
                url: '/api/transporteur/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    
                    var select = document.getElementById('zone-transporteur');
                    var data = jqXHR.responseJSON;
                    var option = document.createElement("option");
                    select.innerHTML = '';
                    option.innerHTML = '-- Selectionnez --';
                    option.value = '';
                    select.appendChild(option);
                    for(var i=0; i<data.length; i++){
                        option = document.createElement("option");
                        option.value = data[i].ref;
                        option.innerText = data[i].contribuable;
                        select.appendChild(option);
                    }
        //            $(select).select2();
                }
            });
        };
    
        window.getChauffeur = function(){
           
            $.ajax({
                url: '/api/chauffeur/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    
                    var select = document.getElementById('zone-chauffeur');
                    var data = jqXHR.responseJSON;
                    var option = document.createElement("option");
                    select.innerHTML = '';
                    option.innerHTML = '-- Selectionnez --';
                    option.value = '';
                    select.appendChild(option);
                    for(var i=0; i<data.length; i++){
                        option = document.createElement("option");
                        option.value = data[i].ref;
                        option.innerText = data[i].nom+' '+data[i].prenom;
                        select.appendChild(option);
                    }
        //            $(select).select2();
                }
            });
        };
    
    
        window.getClient = function(){
           
            $.ajax({
                url: '/api/client/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    
                    var select = document.getElementById('zone-client');
                    var data = jqXHR.responseJSON;
                    var option = document.createElement("option");
                    select.innerHTML = '';
                    option.innerHTML = '-- Selectionnez --';
                    option.value = '';
                    select.appendChild(option);
                    for(var i=0; i<data.length; i++){
                        option = document.createElement("option");
                        option.value = data[i].ref;
                        option.innerText = data[i].nom+' ('+data[i].pays+')';
                        select.appendChild(option);
                    }
        //            $(select).select2();
                }
            });
        };
    
        window.getVilles = function(){
           
            $.ajax({
                url: '/api/ville/list',
                method: 'GET',
                async: false,
                complete: function (jqXHR, textStatus ) {
                    
                    var select = document.getElementById('zone-ville');
                    var data = jqXHR.responseJSON;
                    var option = document.createElement("option");
                    //console.log(data);
                    select.innerHTML = '';
                    option.innerHTML = '-- Selectionnez --';
                    option.value = '';
                    select.appendChild(option);
                    for(var i=0; i<data.length; i++){
                        option = document.createElement("option");
                        option.value = data[i].ref;
                        option.innerText = data[i].nom;
                        //console.log(data[i].nom);
                        select.appendChild(option);
                    }
                    $(select).select2();
                }
            });
        };
    
        window.getPays = function(){
           
            $.ajax({
                url: '/api/pays/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    
                    var select = document.getElementById('zone-pays');
                    var data = jqXHR.responseJSON;
                    var option = document.createElement("option");
                    select.innerHTML = '';
                    option.innerHTML = '-- Selectionnez --';
                    option.value = '';
                    select.appendChild(option);
                    for(var i=0; i<data.length; i++){
                        option = document.createElement("option");
                        option.value = data[i].code;
                        option.innerText = data[i].libelle;
                        select.appendChild(option);
                    }
        //            $(select).select2();
                }
            });
        };
    
        (function getColis(){

            $.ajax({
                url: '/api/colis/for_contenaire/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    
                    var select = document.getElementById('panel-colis');
                    /**
                     * data contient la liste des empotages destinés à l'embarquement
                     * @type 
                     */
                    var data = jqXHR.responseJSON;
                    var option = document.createElement("option");
                    var label, checkbox, span, row, td;
                    for(var i=0; i<data.length; i+=4){
                        row = document.createElement("tr");
                        for(var j=0; j<4 && i+j < data.length; j++){
                            td = document.createElement("td");
                            label = document.createElement("label");
                            span = document.createElement("span");
                            checkbox = document.createElement("input");
                            checkbox.value = data[i+j].refColis.refColis;
                            checkbox.type = "checkbox";
                            checkbox.name = "colis";
                            checkbox.setAttribute("class", "zone-colis");
                            span.innerHTML = data[i+j].refColis.refColis;
                            span.style.fontSize = "15px";
                            label.setAttribute("class", "label label-default");
                            label.appendChild(checkbox);
                            label.appendChild(span);
                            td.appendChild(label);
                            row.appendChild(td);
                            checkbox.setAttribute("volume", data[i].refColis.volume);
                            checkbox.setAttribute("surface", data[i].refColis.surface);
                            checkbox.setAttribute("volumeBrute", parseFloat((parseFloat(data[i].hauteur)*parseFloat(data[i].largeur)*parseFloat(data[i].longueur))/Math.pow(10, 6)+"").toFixed(2));
                            select.appendChild(option);
                        }
                        select.appendChild(row);
                    }
                    $('.zone-colis').change(function(){
                        var surface = 0, volume = 0, volumeBrute = 0;
                        listeColis = [];
                        $('.zone-colis').each(function(){
                            if(this.checked === true){
                                surface += parseFloat(this.getAttribute("surface"));
                                volume  += parseFloat(this.getAttribute("volume"));
                                volumeBrute += parseFloat(this.getAttribute("volumeBrute"));
                                listeColis.push(this.value);
                            }
                        });


                        $('#zone-volume').val(volume);
                        $('#zone-volume-brute').val(volumeBrute);
                        $('#zone-surface').val(surface);
                    });

                }
            });
        })();

        $('select').select2();
    </script>
</div>
