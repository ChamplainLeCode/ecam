<%-- 
    Document   : fiche_production
    Created on : 7 juil. 2019, 08:36:38
    Author     : champlain
--%>


<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="app.modelController.TrancheJpaController"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="app.modelController.PaquetJpaController"%>
<%@page import="app.models.DetailCommande"%>
<%@page import="java.util.HashMap"%>
<%@page import="app.models.DetailReception"%>
<%@page import="java.util.List"%>
<%@page import="app.controllers.StockController"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    HashMap<String, List> listeBilles = StockController.getBilleIntoEmbarquement(request.getParameter("plomb"));
    
    if(listeBilles != null){
        DecimalFormat formateur = new DecimalFormat(".###");
        List<DetailReception> ldr = listeBilles.get("dr");
        List<DetailCommande> ldc = listeBilles.get("dc");
        
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Fiche Production</title>
        <link rel="stylesheet" href="/resources/css/bootstrap/dist/css/bootstrap.min.css">
        <style>
            @page{
                margin: 0cm;
                padding: 1cm;
            }
            body{
                padding: 1cm;
                font-size: 7pt;
            }
        </style>
    </head>
    <body>
        
        <div class=WordSection1>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-2" style=" margin-bottom: 10pt; border: black 1px; border-style: solid; border-radius: 10px; text-align: center;">
                <p class="row" style="text-align: center; font-weight: bold">**********************</p>
                <p class="row" style="text-align: center; font-weight: bold; font-size: 18px; margin-bottom: 5px;">FICHE DE RENDEMENT</p>
                <p class="row" style="text-align: center; font-size: 10px;">-----------------------------------------------------------</p>
                <p>
                    <span class="pull-left"><span style="font-weight: bold"></span>N°:<%=request.getParameter("plomb")%></span> <span id="zone-ref"></span></span> 
                    <span class="pull-right"><span style="font-weight: bold">Date: <%=new Date().getDay()+"/"+new Date().getMonth()+"/"+(new Date().getYear()+1900)%><span> <span id="zone-date"></span></span> 
                </p>
            </div>
        </div>

        
        <table style="margin-top: 10pt;" class="table table-bordered  table-responsive table-striped">
            <thead>
                <tr>
                    <th>N° Bille<br/>N° Travail</th>
                    <th>Essence<br/>Fournisseur</th>
                    <th>M³ F<br/>M³ Parc</th>
                    <th>Prix<br>Transport</th>
                    <th>Qualite [M²]</th>
                    <th>Pourcentage(%)</th>
                    <th>Total M²</th>
                    <th>RESA</th>
                    <th>Vente/M²</th>
                    <th>Coût/M²</th>
                    <th>Rendement</th>
                </tr>
            </thead>
            <tbody id="tabA">
                <% for(int i=0; i<ldr.size(); i++){%>
                <tr>
                    <td><%=ldr.get(i).getNumBille()%><br><%=ldr.get(i).getNumtravail()%></td>
                    <td><%=ldr.get(i).getRefEssence().getRefEssence()%> <%=ldr.get(i).getRefEssence().getLibelle()%><br><%=ldr.get(i).getRefReception().getRefCommande().getRefFournisseur().getNomFournisseur()%></td>
                    <td><%=ldc.get(i).getVolume()%><br><%=ldr.get(i).getVolume()%></td>
                    <td><%=ldc.get(i).getPrixUnitaire()%><br><%=ldr.get(i).getRefReception().getRefCommande().getMontantTransport()%></td>
                    <td colspan="2">
                        <% 
                        List<Object[]> list = TrancheJpaController.getQualiteProportionForNumTravail(ldr.get(i).getNumtravail());
                        double surface = 0D;
                        for(Object[] o : list){
                            surface += Double.parseDouble(o[1].toString());
                        }
                        for(Object[] o : list){
                            out.println("<span class=\"pull-left\"> "+o[0]+" [ "+o[1]+" ]</span> <span class=\"pull-right\">"+formateur.format((Double.parseDouble(o[1].toString())*100)/surface)+"</span><br>");
//                            out.println("qual" +(String)o[0]+" surf "+o[1]+" travail = "+o[2]+"");
                        };
                        %>
                    </td>
                    <td><% String vol = PaquetJpaController.CalculVolumeForBilleByNumTravail(ldr.get(i).getNumtravail()); out.println(vol);%></td>
                    <td><%=formateur.format(surface/(ldr.get(i).getVolume()*1000))%></td>
                    <td></td>
                    <td></td>
                    <td><%=(new BigDecimal(vol).doubleValue()*TrancheJpaController.getEpaisseurForNumTravail(ldr.get(i).getNumtravail()))/ldr.get(i).getVolume()%></td>
                </tr>
                <% }%>
            </tbody>
        </table>

        </div>
        
        <script src="/resources/js/my/jquery.min.js"></script>
        <script src="/resources/css/bootstrap/dist/js/bootstrap.min.js"></script>
        
        <script type="text/javascript">
            function parseDate(dateStringify = (new Date().getYear()+1990)+"-"+new Date().getMonth()+"-"+new Date().getDate()){
                var date = dateStringify.split('-');
                // //console.log(date);
                return new Date(parseInt(date[0]), parseInt(date[1])-1, parseInt(date[2]));
            }
        
            function loadDataFor(date){
                $.ajax({
                    url: '/api/massicotage/fiche',
                    data: {date: date},
                    method: 'POST',
                    dataType: 'json',
                    complete: function (jqXHR, textStatus ) {
                        let donnees = jqXHR.responseJSON;
                        let tabA = document.getElementById("tabA");
                        let tabB = document.getElementById("tabB");
                        tabA.innerHTML = '';
                        tabB.innerHTML = '';
                        for(let data in donnees){
                            console.log(donnees[data]);
                            let tab = (donnees[data].colis.split('-')[1].charAt(0) === 'A' ? tabA : tabB);
                            let tr = document.createElement("tr");
                                let td = document.createElement("td");
                                td.innerHTML = donnees[data].data[0].colis;
                            tr.appendChild(td);
                                td = document.createElement("td");
                                td.setAttribute("colspan", "4");
                                td.innerHTML = donnees[data].data[0].essence;
                            tr.appendChild(td);
                                td = document.createElement("td");
                                for(let j=0; j<donnees[data].data.length; j++){
                                    td.innerHTML += donnees[data].data[j].bille+", ";
                                }
                            tr.appendChild(td);
                                td = document.createElement("td");
                                td.innerHTML = donnees[data].data[0].surface;
                            tr.appendChild(td);
                                td = document.createElement("td");
                                td.innerHTML = '<input type="text" style="border:none; width:100%;" class="form-control" >';//nees[data].data[0].surface;
                            tr.appendChild(td);
                            tab.appendChild(tr);
                        }
                    }
                });
            }
            
            $('#zone-date').change(function(){
               let date = parseDate($(this).val());
               loadDataFor(date.getTime());
            });
            
            
        </script>
    </body>
</html>
<%
    }else{
%>


<%
    }

%>