<%-- 
    Document   : detailequipe-edit
    Created on : 6 févr. 2019, 09:59:34
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-detailequipe">
    <p class="status-edit-detailequipe" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REFERENCE</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<% out.print(request.getParameter("ref"));%>" required="" class="form-control" id="zone-ref" placeholder="REFERENCE  du DetailEquipe">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">EquipeRef</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("equipeRef"));%>" class="form-control" id="zone-equipeRef" placeholder="EquipeRef du DetailEquipe">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Personnel</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("personnel"));%>" class="form-control" id="zone-personnel" placeholder="Personnel du DetailEquipe">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Role</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("role"));%>" class="form-control" id="zone-role" placeholder="Role du DetailEquipe">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" id="btn-close-editPane" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-detailequipe"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        $("#btn-edit-detailequipe").click(function(){
            editData($('#zone-ref').val(), $('#zone-equipeRef').val(), $('#zone-personnel').val(), $('#zone-role').val());
            $('#form-edit-detailequipe .overlay').removeClass('hide');
            $('#form-edit-detailequipe #btn-edit-detailequipe').addClass("disabled");
        });

    </script>
</div>
