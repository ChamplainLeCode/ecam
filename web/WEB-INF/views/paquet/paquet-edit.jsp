<%-- 
    Document   : paquet-edit
    Created on : 30 janv. 2019, 11:08:32
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-paquet">
    <p class="status-edit-paquet" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REFERENCE</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<% out.print(request.getParameter("ref"));%>" required="" class="form-control" id="zone-ref" placeholder="Numéro de CNI">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">NbrFeuilles</label>

          <div class="col-sm-10">
              <input type="number"  required="" value="<% out.print(request.getParameter("nbrFeuilles"));%>" class="form-control" id="zone-nbrFeuilles" placeholder="nbrFeuilles du paquet">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">NumTravail</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("numTravail"));%>" class="form-control" id="zone-numTravail" placeholder="NumTravail du paquet">
          </div>
        </div>
        <div class="form-group">

          <label for="zone-epaisseur" class="col-sm-2 control-label">Plot</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("fonction"));%>" class="form-control" id="zone-fonction" placeholder="Plot du paquet">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" id="btn-close-editPane" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-personnel"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        $("#btn-edit-personnel").click(function(){
            editData($('#zone-ref').val(), $('#zone-nbrFeuilles').val(), $('#zone-numTravail').val(), $('#zone-plot').val());
            $('#form-edit-personnel .overlay').removeClass('hide');
            $('#form-edit-personnel #btn-edit-personnel').addClass("disabled");
        });

    </script>
</div>
