<%-- 
    Document   : paquet-add
    Created on : 30 janv. 2019, 11:08:15
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-paquet">
    <p class="status-edit-paquet" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REFERENCE</label>

          <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="zone-ref" placeholder="Reference du paquet">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">NbrFeuilles</label>

          <div class="col-sm-10">
              <input type="nmber"  required="" class="form-control" id="zone-nbrFeuilles" placeholder="NbrFeuilles du paquet">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">NumTravail</label>

          <div class="col-sm-10">
              <input type="text"  required="" class="form-control" id="zone-numTravail" placeholder="NumTravail du paquet">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Plot</label>

          <div class="col-sm-10">
              <input type="text"  required="" class="form-control" id="zone-plot" placeholder="Plot du paquet">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-add-paquet"><i class="fa fa-pencil"></i> Ajouter</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        $("#btn-add-paquet").click(function(){
            saveData($('#zone-ref').val(), $('#zone-nbrFeuilles').val(), $('#zone-numTravail').val(), $('#zone-plot').val());
            $('#form-edit-paquet .overlay').removeClass('hide');
            $('#form-edit-paquet #btn-add-paquet').addClass("hide");
        });

    </script>
</div>
