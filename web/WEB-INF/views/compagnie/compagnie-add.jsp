<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="form-add-Compagnie">
    <p class="status-add-Compagnie" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Reférence</label>

          <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="zone-ref" placeholder="Rérérence">
          </div>
        </div>
        <div class="form-group">
          <label for="zone- libelle" class="col-sm-2 control-label">Libelle</label>

          <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="zone-libelle" placeholder="libelle de la Compagnie">
          </div>
        </div>
        <div class="form-group">
          <label for="zone- libelle" class="col-sm-2 control-label">Contribuable</label>

          <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="zone-contribuable" placeholder="contribuable de la Compagnie">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-save-Compagnie">Enregistrer</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
    <script type="text/javascript">
        $("#zone-ref").val('REF_CLASS_'+(new Date()).getTime());
        $("#btn-save-Compagnie").click(function(){
            var data = {
                libelle: $('#zone-libelle').val(),
                contribuable: $('#zone-contribuable').val(),
                ref: $('#zone-ref').val()
            };
            save(data, 'compagnie');
        });
    </script>
</div>
