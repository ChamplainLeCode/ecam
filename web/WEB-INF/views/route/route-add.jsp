<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-route">
    <p class="status-edit-route" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Nom</label>

          <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="zone-nom" placeholder="Nom">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Chemin</label>

          <div class="col-sm-10">
              <input type="text"  required="" class="form-control" id="zone-path" placeholder="Chemin d'accès">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-add-route"><i class="fa fa-pencil"></i> Ajouter</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        $("#btn-add-route").click(function(){
            saveData($('#zone-nom').val(), $('#zone-path').val());
            $('#form-edit-route .overlay').removeClass('hide');
            $('#form-edit-route #btn-add-route').addClass("hide");
        });

    </script>
</div>
