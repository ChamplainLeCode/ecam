<%-- 
    Document   : sechoir-edit
    Created on : 6 févr. 2019, 11:45:54
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-sechoir">
    <p class="status-edit-sechoir" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Id</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<% out.print(request.getParameter("ref"));%>" required="" class="form-control" id="zone-ref" placeholder="Reference de la Machine à Trancher">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Nom de la machine</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("libelle"));%>" class="form-control" id="zone-libelle" placeholder="Libelle de la Machine à Trancher">
          </div>
        </div>        
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" id="btn-close-editPane" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-sechoir"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        $("#btn-edit-sechoir").click(function(){
            editData($('#zone-ref').val(), $('#zone-libelle').val());
            $('#form-edit-sechoir .overlay').removeClass('hide');
            $('#form-edit-sechoir #btn-edit-sechoir').addClass("disabled");
        });

    </script>
</div>
