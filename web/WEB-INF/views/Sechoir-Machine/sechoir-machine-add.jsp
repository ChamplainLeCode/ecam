<%-- 
    Document   : sechoir-add
    Created on : 6 févr. 2019, 11:46:06
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-sechoir">
    <p class="status-edit-sechoir" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-4 control-label">Nom de la machine</label>

          <div class="col-sm-6">
              <input type="text"  required="" class="form-control" id="zone-libelle" placeholder="Libelle de la Machine à Secher">
          </div>
        </div>
        
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-add-sechoir"><i class="fa fa-pencil"></i> Ajouter</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        $("#btn-add-sechoir").click(function(){
            saveData($('#zone-libelle').val());
            $('#form-edit-sechoir .overlay').removeClass('hide');
            $('#form-edit-sechoir #btn-add-sechoir').addClass("hide");
        });

    </script>
</div>
