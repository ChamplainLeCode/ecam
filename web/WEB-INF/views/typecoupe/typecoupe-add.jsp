<%-- 
    Document   : typecoupe-add
    Created on : 9 janv. 2019, 18:16:02
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-typecoupe">
    <p class="status-edit-typecoupe" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Reference</label>

          <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="zone-ref" placeholder="Reference de type coupe">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Libelle</label>

          <div class="col-sm-10">
              <input type="text"  required="" class="form-control" id="zone-libelle" placeholder="libelle du typecoupe">
          </div>
        </div>
        
        </div>
        
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-add-typecoupe"><i class="fa fa-pencil"></i> Ajouter</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        $("#btn-add-typecoupe").click(function(){
            saveData($('#zone-ref').val(), $('#zone-libelle').val());
            $('#form-edit-typecoupe .overlay').removeClass('hide');
            $('#form-edit-typecoupe #btn-add-typecoupe').addClass("hide");
            $('.close').click();
            loadData();
        });

    </script>
</div>
