<%-- 
    Document   : lettrecamion-edit
    Created on : 13 janv. 2019, 17:18:06
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-lettrecamion">
    <p class="status-edit-lettrecamion" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REFERENCE</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<% out.print(request.getParameter("ref"));%>" required="" class="form-control" id="zone-ref" placeholder="Reference de la lettre">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Nom</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("certificat"));%>" class="form-control" id="zone-certificat" placeholder="Certificat de la lettrecamion">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Précertificat</label>

          <div class="col-sm-10">
              <input type="fichier"  required="" value="<% out.print(request.getParameter("fichier"));%>" class="form-control" id="zone-fichier" placeholder="Fichier de la lettrecamion">
          </div>
        </div>
        
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" id="btn-close-editPane" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-lettrecamion"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        $("#btn-edit-lettrecamion").click(function(){
            editData($('#zone-ref').val(), $('#zone-certificat').val(), $('#zone-fichier').val();
            $('#form-edit-lettrecamion .overlay').removeClass('hide');
            $('#form-edit-lettrecamion #btn-edit-lettrecamion').addClass("disabled");
        });

    </script>
</div>

