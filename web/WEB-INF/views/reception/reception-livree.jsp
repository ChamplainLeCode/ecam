<%-- 
    Document   : reception-livree
    Created on : 21 juin 2019, 22:14:07
    Author     : champlain
    0.3
--%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="app.modelController.DetailLvJpaController"%>
<%@page import="java.util.HashMap"%>
<%@page import="app.models.DetailLv"%>
<%@page import="app.models.DetailReception"%>
<%@page import="java.util.List"%>
<%@page import="app.modelController.DetailReceptionJpaController"%>
<%@page import="app.controllers.ReceptionController"%>
<%@page import="app.models.Database"%>
<pe@page
<%@page import="app.models.Reception"%>
<%@page import="app.modelController.ReceptionJpaController"%>
<%
    ReceptionJpaController var  = new ReceptionJpaController(Database.getEntityManager());
    Reception recept = var.findReception(request.getParameter("reception"));
    List<DetailReception> drecept = new DetailReceptionJpaController(Database.getEntityManager()).findDetailReceptionByReception(recept.getRefReception());
    HashMap<String, DetailLv> d_lv = new HashMap<>();
    for(DetailReception dr : drecept){
        d_lv.put(dr.getNumBille(), new DetailLvJpaController(Database.getEntityManager()).findDetailLvByNumBille(dr.getNumBille()));
    };
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title></title>
	<style type="text/css">
                @page{
                        margin: 0cm;
                        display: none;
                        padding-left: 40px; 
                        padding-right: 40px;
                        font-size: 8pt;
                }
                @media print{
                    .no-print{
                        display: none;
                    }
                    
                }
                td p { margin-bottom: 0cm; }
		p { margin-bottom: 0.25cm; }
                input{
                    border: none;
                    width: 70px;
                    
                }
                body{
                    font-size: 8pt;
                    padding-left: 40px;
                    padding-right: 40px;
                }
	</style>
</head>
<body lang="fr-FR" dir="ltr">
    <div class="container" style="padding-left: 10px; padding-right: 10px;">
            <div style="border: 1px black solid; text-align: center; background-color: #808080; padding: 10px; width: 70%; margin-left: 15%; border-radius: 20px; margin-bottom: 10pt;">
                    <p ><b>******************************</b></p>
                    <p style="font-size: 18px"><b>RÉCEPTION LIVRÉE </b></p>
                    <p style="font-weight: normal">………………………………………</p>
                    <p style="font-size: 18px"><b>N°</b><span style="font-weight: normal">:
                    <%=request.getParameter("num")%><%="\t"%>                </span><b>Du</b><span style="font-weight: normal">
                    <%  
                        DecimalFormat dff = new DecimalFormat("##"); 
                        out.print(dff.format(recept.getDateReception().getDate())+"/"+dff.format(recept.getDateReception().getMonth()+1)+"/"+dff.format((recept.getDateReception().getYear()+1900)));%> </span></p>
            </div>

            <table class="table table-bordered table-striped table-responsive" width="100%" cellpadding="3" cellspacing="0" style="page-break-inside: avoid">

            <tr valign="top">
                    <td width="50%" style="font-size:8pt">
                            <p><b>Processus de Transformation</b></p>
                    </td>
                    <td width="50%" style="font-size:8pt">
                            <p><br/>
                            </p>
                    </td>
            </tr>
            <tr valign="top">
                    <td width="50%" style="font-size:8pt">
                            <p><b>Ordonnateur                         :</b></p>
                    </td>
                    <td width="50%" style="font-size:8pt">
                            <p><b>Service émetteur               :</b></p>
                    </td>
            </tr>
            <tr valign="top">
                    <td width="50%" style="font-size:8pt">
                            <p><b>Fournisseur                           : </b><span style="font-weight: normal"> <%=recept.getRefCommande().getRefFournisseur().getNomFournisseur()%> </span></p>
                    </td>
                    <td width="50%" style="font-size:8pt">
                            <p><b>Compte N°                       :</b></p>
                    </td>
            </tr>
            <tr valign="top">
                    <td width="50%" style="font-size:8pt">
                        <p><b>Parc De Chargement             :</b> <input type="text" style="width: 300px; border:none; " placeholder="Saisissez"></p>
                    </td>
                    <td width="50%" style="font-size:8pt">
                            <p><b>N° Contribuable               :</b><%=recept.getRefCommande().getRefFournisseur().getContribuable()%></p>
                    </td>
            </tr>
            <tr valign="top">
                    <td width="50%" style="font-size:8pt">
                            <p><b>Acheteur                               : </b><span style="font-weight: normal">  </span></p>
                    </td>
                    <td width="50%" style="font-size:8pt">
                            <p><b>Parc de destination           :ECAM-PLACAGES</b></p>
                    </td>
            </tr>
            </table><br/>
                    
            <table class="table table-bordered table-responsive table-striped" width="100%" cellpadding="3" cellspacing="0" class="table table-bordered table-responsive table-striped">
                    <tr>
                            <td style="font-size:8pt; border: 1px solid #123456;">
                                    <p><font style="font-size: 8pt;"><b>N° Bille</b></font></p>
                            </td>
                            <td style="font-size:8pt; border: 1px solid #123456;">
                                    <p><font style="font-size: 8pt;"><b>Essence</b></font></p>
                            </td>
                            <td style="font-size:8pt; border: 1px solid #123456;">
                                    <p><font style="font-size: 8pt;"><b>Cert</b></font></p>
                            </td>
                            <td  style="font-size: 8pt; border: 1px solid #123456">
                                    <p align="center"><font style="font-size: 8pt; #123456"><b>Long</b></font></p>
                            </td>
                            <td  style="font-size: 8pt; border: 1px solid #123456">
                                    <p align="center"><font style="font-size: 8pt; #123456"><b>G . D</b></font></p>
                            </td>
                            <td  style="font-size: 8pt; border: 1px solid #123456">
                                    <p align="center"><font style="font-size: 8pt; #123456"><b>P . D</b></font></p>
                            </td>
                            <td  style="font-size: 8pt; border: 1px solid #123456">
                                    <p align="center"><font style="font-size: 8pt; #123456"><b>D . M</b></font></p>
                            </td>
                            <td  style="font-size: 8pt; border: 1px solid #123456">
                                    <p><font style="font-size: 8pt; #123456"><b>M³ Réel</b></font></p>
                            </td>
                            <td  style="font-size: 8pt; border: 1px solid #123456">
                                    <p align="center"><font style="font-size: 8pt; #123456"><b>Prix U.</b></font></p>
                            </td>
                            <td  style="font-size: 8pt; border: 1px solid #123456">
                                    <p><br/>M³ Parc</p>
                            </td>
                            <td  style="font-size: 8pt; border: 1px solid #123456">
                                    <p><br/>Obs</p>
                            </td>
                    </tr>
                <% 
                    float volume = 0, prix = 0;
                    DecimalFormat df = new DecimalFormat("#.###");
                    for(DetailReception dr : drecept){
                %>
                    <tr>
                            <td width="14%" style="font-size: 8pt; border: 1px solid #123456">
                                    <p><br/>
                                        <%=dr.getNumBille()%>
                                    </p>
                            </td>
                            <td width="21%" style="font-size: 8pt; border: 1px solid #123456">
                                    <p><br/>
                                        <%=dr.getRefEssence().getRefEssence()+" "+dr.getRefEssence().getLibelle()%>
                                    </p>
                            </td>
                            <td style="font-size: 8pt; border: 1px solid #123456">
                                    <p><br/>
                                        <%=dr.getCertificat()%>
                                    </p>
                            </td>
                            <td  style="font-size: 8pt; border: 1px solid #123456">
                                    <p><br/>
                                        <%=d_lv.get(dr.getNumBille()).getLongueur()%>
                                    </p>
                            </td>
                            <td  style="font-size: 8pt; border: 1px solid #123456">
                                    <p><br/>
                                        <%=d_lv.get(dr.getNumBille()).getGdDiam()%>
                                    </p>
                            </td>
                            <td  style="font-size: 8pt; border: 1px solid #123456">
                                    <p><br/>
                                        <%=d_lv.get(dr.getNumBille()).getPttDiam()%>
                                    </p>
                            </td>
                            <td  style="font-size: 8pt; border: 1px solid #123456">
                                    <p><br/>
                                        <%=df.format(d_lv.get(dr.getNumBille()).getGdDiam()+d_lv.get(dr.getNumBille()).getPttDiam()/2)%>
                                    </p>
                            </td>
                            <td  style="font-size: 8pt; border: 1px solid #123456">
                                    <p><br/>
                                        <%
                                            double rayon = (d_lv.get(dr.getNumBille()).getGdDiam()+d_lv.get(dr.getNumBille()).getPttDiam())/4;
                                            volume += (rayon*rayon)*(Math.PI)*d_lv.get(dr.getNumBille()).getLongueur(); 
                                            out.print(df.format(((rayon*rayon)*(Math.PI)*d_lv.get(dr.getNumBille()).getLongueur())/1000000));%>
                                    </p>
                            </td>
                            <td  style="font-size: 8pt; border: 1px solid #123456">
                                    <p><br/>
                                        <%prix += dr.getPrix(); out.print(dr.getPrix());%>
                                    </p>
                            </td>
                            <td  style="font-size: 8pt; border: 1px solid #123456">
                                    <p><br/>
                                        <%=dr.getVolume()%>
                                    </p>
                            </td>
                            <td  style="font-size: 8pt; border: 1px solid #123456">
                                    <p><br/>
                                        <%=dr.getObservation()%>
                                    </p>
                            </td>
                    </tr>
                <%
                    }
                %>

                    <tr valign="top" style="font-weight: bold">
                            <td colspan="6" width="55%" style="font-size: 8pt; border: 1px solid #123456;">
                                    <p><b>Total_&nbsp;: <%=drecept.size()%></b></p>
                            </td>
                            <td  style="font-size: 8pt; border: 1px solid #123456">
                                    <p><br/>
                                        <%=df.format(volume/1000000)%>
                                    </p>
                            </td>
                            <td  style="font-size: 8pt; border: 1px solid #123456">
                                    <p><br/>
                                        <%=prix%>
                                    </p>
                            </td>
                    </tr>
            </table>
            <p><br/>
            <br/>

            <button style="margin-right: 40px; margin-bottom: 20px;" onclick="window.print(); window.close();" type="button" id="btn-imprimer no-print" class="pull-left btn btn-primary no-print">
                <i class="fa fa-print "></i> Imprimer le bon
            </button>
            </p>
    </div>
        <script type="text/javascript">
            $(function(){

            });
        </script>
    </body>
</html>