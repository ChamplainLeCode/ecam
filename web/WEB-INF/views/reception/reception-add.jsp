<%-- 
    Document   : reception-add
    Created on : 14 janv. 2019, 08:12:00
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-reception">
    <p class="status-edit-reception" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REFERENCE</label>

          <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="zone-ref" placeholder="Reference de la Reception">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Date</label>

          <div class="col-sm-10">
              <input type="date"  required="" class="form-control" id="zone-date" placeholder="Date de la  Reception">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Commande</label>

          <div class="col-sm-10">
              <input type="text"  required="" class="form-control" id="zone-commande" placeholder="Reception de Commande">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">LettreCamion</label>

          <div class="col-sm-10">
              <input type="text"  required="" class="form-control" id="zone-lettreCamion" placeholder="LettreCamion  de la Commande">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-add-reception"><i class="fa fa-pencil"></i> Ajouter</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        $("#btn-add-reception").click(function(){
            saveData($('#zone-ref').val(), $('#zone-date').val(), $('#zone-commande').val(), $('#zone-lettreCamion').val());
            $('#form-edit-reception .overlay').removeClass('hide');
            $('#form-edit-reception #btn-add-reception').addClass("hide");
        });

    </script>
</div>
