<!-- 
    Document   : totoPage
    Created on : 8 déc. 2018, 14:42:16
    Author     : champlain
-->

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="FR">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" charset="UTF-8">
      <title>Ecam Web</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width,directories=no,titlebar=no,status=no,menubar=no,scrollbars=yes,resizable=no, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <%@include file="/WEB-INF/views/layouts/style.jsp" %>
        <link rel="stylesheet" href="/resources/css/select2/select2.min.css">

      <link rel="stylesheet" href="/resources/css/dataTables.bootstrap.min.css">  
      <style>.measure-unit{font-size: 10px;}
          .menu-certification-item:hover{border: 1px solid green;}
      </style>
    
      <script type="text/javascript">
          
            function setRef(){
                $('#zone-ref').val("REF_REC_"+(new Date().getTime())).attr('readonly','');
                $('#zone-parc-chargement').val('');
                $('#zone-origine').val('');
            }
      </script>
    </head>
    <!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
    -->
    <body class="hold-transition skin-blue sidebar-collapse">
    <div class="wrapper">

      <!-- Main Header -->
      <%@include file="/WEB-INF/views/layouts/topBar.jsp" %>

      <!-- Left side column. contains the logo and sidebar -->
      <%@include file="/WEB-INF/views/layouts/sideBar.jsp" %>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" onclick="window.closeDropdown()">
        <!-- Content Header (Page header) -->
        <section class="content-header" onclick="window.closeDropdown()">
          <h1>
            Reception
            <small>Grum</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="">ecam</li>
            <li class="active">receptions</li>
          </ol>
          
          
        </section>

        <!-- Main content -->
        <section class="content container-fluid">

            <div class="row">
                
                <p class="status-del-reception pull-left" style="padding: 5px; border-radius: 5px;"></p>
                
                <button style="margin-right: 40px; margin-bottom: 20px; background-color: transparent;" type="button" onclick="history.back()" class="pull-left btn btn-flat" data-toggle="modal" data-target="#modal-default">
                    <i class="fa fa-arrow-left "></i> Retour
                </button>
                <button style="margin-right: 40px; margin-bottom: 20px;" id="btn-display-form" type="button" onclick="$('#form-add-reception').show(500); setRef(); $('#form-liste-reception').hide()" class="btn btn-bitbucket pull-right" data-toggle="modal" data-target="#modal-default">
                    <i class="fa fa-plus"></i> Receptionner
                </button>
                <button class="pull-left btn btn-success hide"  style="margin-right: 40px; margin-bottom: 20px;" type="button" onclick="loadData(); $('#form-add-reception').hide(); $('#form-liste-reception').show(500);" data-toggle="modal" data-target="#modal-default">
                    <i class="fa fa-list-ul "></i> Liste
                </button> 
                <div class="pull-left btn-group-vertical "  style="margin-right: 40px;  margin-bottom: 20px;" type="button" onclick="$('#form-add-reception').hide(); $('#form-liste-reception').show(500);" data-toggle="modal" data-target="#modal-default">
                    <button onmouseover="window.openDropdown()" class="btn btn-success btn-liste" style="width: 100px; margin-bottom: 10pt;"> <i class="fa fa-list"></i> Liste</button>
                    <ul style="list-style: none; border: 1px solid green; padding: 1pt; position: absolute; z-index: 1000;" class="btn-group-vertical hide menu-certification">
                        <li style="width: 150px; text-align: left; padding:5pt; padding-left: 20px; height: 40px; background: #F0F0F0; " class="btn menu-certification-item" onclick="loadData(); closeDropdown();"> <i class="glyphicon glyphicon-list"></i> Tous</li>
                    </ul>
                </div>
                
            </div>

            <div class="row" style="margin:10px;" id="form-liste-reception">
                
                <div class="modal fade " id="modal-view" style="display: none; background-color: transparent; background: none;">
                    <div class="modal-dialog pull-right col-sm-pull-0" style="width: 100%">
                        <div class="modal-content col-sm-12 col-md-12 col-lg-12 col-xs-12 col-sm-offset-0 col-md-offset-0 col-lg-offset-0" style=" background-color: transparent; background: none;">
                            <div class="modal-header">
                                
                                    <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-view-commande_client" class="hide" data-toggle="modal" data-target="#modal-view"></button>
                            </div>
                            <div class="modal-body" id="modal-body" style=" background-color: transparent ">
                                <div class="box box-widget widget-user-2">
                                        <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-navy">
                                        <!-- /.widget-user-image -->
                                        <h4 class="widget-user-desc">N° Commande <span id="vzone-ref" class="pull-right" ></span></h4>
                                        <h5 class="widget-user-desc" >Fournisseur <span id="vzone-fournisseur" class="pull-right" ></span></h5>
                                        <h5 class="widget-user-desc">date: <span  id="vzone-date" class="pull-right" ></span></h5>
                                    </div>
                                    <div class="box-footer no-padding" style="margin-top: 25px">
                                        <ul class="nav nav-stacked" style="">
                                            <li><div class="box box-success">
                                                    <div class="box-header with-border">
                                                      <h3 class="box-title">Info. Bille</h3>

                                                      <div class="box-tools pull-right">
                                                          <button type="button" class="btn btn-flat" >
                                                              <i class="fa fa-caret-down"></i>
                                                              <span style="margin-left: 10px;" class="pull-right badge bg-green" id="vzone-nbr-detail"></span>
                                                        </button>
                                                      </div>
                                                      <!-- /.box-tools -->
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div id="vzone-detail" style="padding: 30px;" class="box-body">
                                                        <table id="tableau-print-detail" class=" table table-bordered table-hover dataTable form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <thead>
                                                                <tr>
                                                                    <th class="sorting">Essence</th>
                                                                    <th class="sorting">N° Bille</th>
                                                                    <th class="sorting">Certification</th>
                                                                    <th class="sorting">Longueur</th>
                                                                    <th class="sorting">Grand Diam</th>
                                                                    <th class="sorting">Ptt Diam</th>
                                                                    <th class="sorting">Moy Diam</th>
                                                                    <th class="sorting">Cubage</th>
                                                                    <th class="sorting">Prix</th>
                                                                    <th class="sorting">Observation</th>
                                                                    <th class="sorting">Status</th>
                                                                    <th class="sorting">Date</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="table-view-detail-reception"></tbody>
                                                        </table>
                                                    </div>
                                                    <!-- /.box-body -->
                                                </div>
                                            </li>
                                            <li><button type="button" class=" btn btn-flat pull-right" style="background-color: transparent; margin: 5px;" data-dismiss="modal"> Fermer</button></li>
                                            <li><button type="button" id="btn-print-detail" class=" btn btn-bitbucket pull-left" style="margin: 5px;" > Imprimer </button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

                <table style="text-align: center;" id="table-listeReception" class="table table-bordered table-hover dataTable form-group" role="grid" aria-describedby="table-addCommande_info">
                    <thead>
                        <tr role="row">
                            <th style="text-align: center;" class="sorting" >Date</th>
                            <th style="text-align: center;" class="sorting" >Fournisseur</th>
                            <th style="text-align: center;" class="sorting" >Commande</th>
                            <th style="text-align: center;" class="sorting" >Nbr Billes</th>
                            <th style="text-align: center;" class="sorting" >Lettre Voiture</th>
                            <th style="text-align: center;" class="sorting" >Volume Attendu</th>
                            <th style="text-align: center;" class="sorting" >Volume Receptionné</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="table-listeReception-body"></tbody>
                    <tfoot>
                    </tfoot>
                </table>

                
            </div>

            <form id="form-add-reception" onsubmit="return sendForm()" style="display: none;">

                        <fieldset>
                            <legend>Recept. Info</legend>
                            <div class="form-group pull-left col-sm-3 col-lg-3 col-md-3 col-xs-12">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default">
                                        N° Réception
                                    </span>
                                </div>
                                <input type="text" readonly="" required="" name="" id="zone-ref" class="form-control">
                            </div>
                            <div class="form-group pull-right col-sm-3 col-lg-3 col-md-3 col-xs-12">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default">
                                        Date
                                    </span>
                                </div>
                                <input type="text" readonly="" required="" id="zone-date" class="form-control " >
                            </div>
                        </fieldset>
                
                        <fieldset>
                            <legend>Fourn. Info</legend>
                            
                            <div class="form-group pull-left col-sm-3 col-lg-3 col-md-3 col-xs-12">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default">
                                        Fournisseur
                                    </span>
                                </div>
                                <select name="zone-fournisseur" required="" style="width:100%;" id="zone-fournisseur" class="form-control " ></select>
                            </div>
                            <div class="form-group pull-left col-sm-3 col-lg-3 col-md-3 col-xs-12">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default">
                                        N° Commande
                                    </span>
                                </div>
                                <select style="width:100%;" id="zone-commande" name="zone-commande" required="" class="form-control"> </select>
                            </div>
                            <div class="form-group pull-left col-sm-3 col-lg-3 col-md-3 col-xs-12 hide">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default">
                                        Société
                                    </span>
                                </div>
                                <input type="text" readonly="" class="form-control" id="zone-parc-chargement" name="zone-parc-chargement" required="">
                            </div>
                            <div class="form-group pull-left col-sm-3 col-lg-3 col-md-3 col-xs-12 hide">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default">
                                            Certificat
                                    </span>
                                </div>
                                <input type="text" readonly="" class="form-control" id="zone-origine" name="zone-origine" required="">
                            </div>
                            
                        </fieldset>
                
                        <fieldset id="billeInfo">
                            <legend>Bille. Info</legend>
                            
                                        <table id="table-addDetailReception" class="table table-bordered table-hover dataTable form-group" role="grid" aria-describedby="table-addDetailReception_info">
                                                <thead>
                                                    <tr role="row">
                                                        <th class="sorting">Essence</th>
                                                        <th class="sorting">N° Bille.........</th>
                                                        <th class="sorting ">Certificat</th>
                                                        <th class="sorting" aria-sort="descending">Long.</th>
                                                        <th class="sorting">Grand Diam</th>
                                                        <th class="sorting">Ptt Diam</th>
                                                        <th class="sorting hide">Moy Diam</th>
                                                        <th class="sorting">Volume Reçu</th>
                                                        <th class="sorting">Volume Attendu</th>
                                                        <th class="sorting">Obs</th>
                                                        <th class="sorting">Prix</th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="table-addDetailReception-body"></tbody>
                                                <tfoot>
                                                    <tr>
                                                      <td colspan="10">
                                                          <button  type="button" id="btn-add-detail" class="pull-right btn btn-primary " style="width:40px; height: 40px; border-radius: 20px;"><i class="glyphicon glyphicon-plus"></i></button>
                                                      </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                            
                        </fieldset>

                        <fieldset>
                            <legend>Lettre Voiture. Info</legend>
                            
                            <div class="form-group pull-left col-sm-4 col-lg-4 col-md-4 col-xs-12">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default">
                                        Numéro
                                    </span>
                                </div>
                                <Input  id="zone-lettre-ref" required="" placeholder="Numéro de la lettre" name="zone-lettre-ref" class="form-control " >
                            </div>
                            <div class="form-group pull-left col-sm-4 col-lg-4 col-md-4 col-xs-12">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default">
                                        Fichier
                                    </span>
                                </div>
                                <input required="" name="zone-lettre-fichier" id="zone-lettre-fichier" accept="image/*" class="form-control" type="file">
                            </div>
                        </fieldset>

                        <div class="row" style="margin-top: 50px;">
                            <button type="submit" class="btn btn-info pull-right" style="margin-right: 100px;" id="btn-addReception"> <i class="glyphicon glyphicon-save"></i> Enregistrer</button>
                        </div>
                    </form>
            
            
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <%@include file="/WEB-INF/views/layouts/footer.jsp" %>

    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->
    <%@include  file="/WEB-INF/views/layouts/script.jsp" %>
        <script src="/resources/fileUpload/jquery.ajaxfileupload.js"></script>
    <script src="/resources/js/reception/reception-home.js"></script>
    <script src="/resources/js/jquery.dataTables.min.js"></script>
    
    <script src="/resources/js/dataTables.bootstrap.min.js"></script>

          <script src="/resources/js/select2/select2.full.min.js"></script>
          

    <script>
        
        $(function () {
            window.$ = $;
            window.fournisseurs = [];
            window.parcs = [];
            var commandesDetails = [];
            window.currentDetail = null;
            window.detailCompteur = 0;
            var essences = [];
            var typeParcs = [];
            
            
            document.getElementById("btn-print-detail").onclick = function(){
                tableToExcel('tableau-print-detail', 'W3C Example Table');
            };
            
            /**
             * Close dropdown button
             */
            window.closeDropdown = function(){
                $('.menu-certification').addClass('hide');
            }
            
            /**
             * Open dropdown button
             */
            window.openDropdown = function(){
                $('.menu-certification').removeClass('hide');
            }
            /**
             * Ici on va récuperer la liste de certificats
             */
            $.ajax({
                url:'/api/type_parc/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    let menu = $('.menu-certification');
                    let data = jqXHR.responseJSON;
                    for(let i = 0; i<data.length; i++){
                        var li = document.createElement("li");
                        $(li).css({'padding': '5pt', 'height': '40px', 'text-align': 'left', 'padding-left': '20px', 'background': '#F0F0F0'})
                             .attr("class", "btn menu-certification-item")
                             .html('<i class="fa fa-key"></i> '+data[i].libelle)
                             .click(function(){ loadData({typeParc: data[i].ref}); closeDropdown();});
                        menu.append($(li));
                    }

                }
            });
            
            $('.btn-liste').click(function(){
               if($('.menu-certification').hasClass('hide')){
                   $('.menu-certification').removeClass('hide');
               }else
                   $('.menu-certification').addClass('hide');
            });
            
            $('.menu-certification').click(function(){
                
            });
            
            $('#zone-date').val(new Date().toUTCString()).attr("data", new Date().getTime());
            
            $('#btn-add-detail').click(function(){
               document.getElementById('table-addDetailReception-body').appendChild(addRow(detailCompteur++)); 
               //setTypeParc(detailCompteur-1);
               /*
                * ON definit le comportement du bouton pour supprimer la ligne
                */
               
               $('.btn-supprimer-detail').click(function(){
                    var index = $(this).attr('data');
                    document.getElementById('table-addDetailReception-body').removeChild(document.getElementById('table-detail-ligne'+index));
                });
                var index = detailCompteur-1;
                $('#zone-longueur'+index).change(function(){
                    var index = $(this).attr("data");
                    var rayon = parseInt($('#zone-moy-diam'+index).val())/2;
                    var hauteur = parseInt($(this).val());

                    $('#zone-cubage'+index).val( parseFloat(((rayon * rayon * Math.PI * hauteur) / Math.pow(10, 6))+"" ).toFixed(3));
                });


                /**
                 * Calcul automatique du diamètre moyen de la bille 
                 */
                $('#zone-ptt-diam'+index+', #zone-gd-diam'+index).change(function(){
                    var index = $(this).attr("data");
                    $('#zone-moy-diam'+index).val(parseInt(((parseInt($('#zone-ptt-diam'+index).val())+ parseInt($('#zone-gd-diam'+index).val()))/2)+"")); 

                    // Recalculer le volume en cas de modification du diamètre moyen

                    $('#zone-longueur'+index).change();
                });
               setEssence();
           });
            
            $.ajax({
                url: '/api/essence/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    essences = JSON.parse(jqXHR.responseText);
                }
            });
            
            function setEssence(essence = '', index = -1){
                    //var select = document.getElementById('zone-essence'+(detailCompteur-1));
                    var select;
                        
                    if(index !== -1){
                        select = $('#zone-essence'+index);
                        var opt = document.createElement("option");
                        opt.value = essence.ref;
                        opt.innerHTML = essence.libelle;
                        console.log('Index = '+index);
                        console.log(essence);
                        console.log(select);
                        select.append($(opt));
                    }else{
                        select = $(".zone-essence"); 

                        select.html('');
                        var selected=-1;
                        var opt = document.createElement("option");
                            opt.value = '';
                            opt.innerHTML = '-- Veuillez selectionner --';
                            select.innerHTML = '';
                            //select.appendChild(opt);
                            select.append($(opt));
                        for(var i=0; i<essences.length; i++){
                            var opt = document.createElement("option");
                            opt.value = essences[i].ref;
                            opt.innerHTML = essences[i].libelle;
                            select.append($(opt));
                            //select.appendChild(opt);
                            //if(essences[i].ref === essence){
                            //    opt.selected = true;
                            //}
                        }
                        select.selected = -1;
                    }
                   // $('.zone-essence').select2();
                    $(select).change(function(){
                       $('#zone-code-essence'+$(this).attr("data")).val($(this).val()); 
                    }).select2();

            }
            
            $.ajax({
                url: '/api/fournisseur/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    fournisseurs = JSON.parse(jqXHR.responseText); 
                    var client = JSON.parse(jqXHR.responseText);
                    var select = document.getElementById('zone-fournisseur');
                    var selected=-1;
                    var opt = document.createElement("option");
                        opt.value = '';
                        opt.innerHTML = '-- Veuillez selectionner --';
                        opt.setAttribute("data", -1);
                        select.appendChild(opt);
                    for(var i=0; i<client.length; i++){
                        var opt = document.createElement("option");
                        opt.value = client[i].refFournisseur;
                        opt.innerHTML = client[i].nomFournisseur+'('+client[i].contact+")";
                        opt.setAttribute("data", i);
                        
                        select.appendChild(opt);
                    }
                    $('#zone-fournisseur').change(function(){
                        
                        var selectCommande = document.getElementById("zone-commande");
                        let ind = document.getElementById("zone-fournisseur").selectedIndex-1;
                        if(ind >= 0){
                            parcs = fournisseurs[ind].parcChargementList;
                            $.ajax({
                                url: '/api/commande/list/ref',
                                method: 'GET',
                                data: {ref: $(this).val()},
                                dataType: 'json',
                                complete: function (cmd, textStatus ) {
                                    var commandes = JSON.parse(cmd.responseText);
                                    selectCommande.innerHTML = '';
                                    var opt = document.createElement("option");
                                    opt.value = '';
                                    opt.innerHTML = '-- Selectionner une commande --';
                                    selectCommande.appendChild(opt);
                                    for(var j=0; j<commandes.length; j++){
                                        opt = document.createElement("option");
                                        opt.value = commandes[j];
                                        opt.innerHTML = commandes[j];
                                        selectCommande.appendChild(opt);
                                        opt.selected = false;
                                    }
                                    //$('#zone-commande').change();
                                }
                            });
                        }else{
                            selectCommande.innerHTML = (''); 
                        }
                    });
                }
            });
            
            $('#zone-commande').change(function(){
                detailCompteur = 0;
               $('#zone-parc-chargement').html('');
               $.ajax({
                    url: '/api/detail_commande/by_commande',
                    dataType: 'json',
                    data: {commande: $(this).val()},
                    method: 'GET',
                    async: false,
                    complete: function (cmd, textStatus ) {
                        commandesDetails = cmd.responseJSON;
                        loadCertificat();
                        var tableau = document.getElementById('table-addDetailReception-body');
                        tableau.innerHTML = '';
                        
                        for(var i=0; i< commandesDetails.length; i++){
                            var obj = commandesDetails[i];
                            $.ajax({
                                url: '/api/detail_reception/by_bille',
                                dataType: 'json',
                                async: false,
                                data: {bille: commandesDetails[i].numBille},
                                method: 'GET',
                                complete: function (jqXHR, textStatus ) {
                                    var donnee = jqXHR.responseJSON;
                                    
                                    /**
                                     * Si la bille a déja été receptionnée ne pas l'afficher
                                     */
                                    if(donnee != null){
                                        return;
                                    }
                                    var ligne = document.createElement("tr");
                                    ligne.id = 'table-detail-ligne'+detailCompteur;
                                    
                                    commandesDetails[i].statut = 'Commande';
                                    
                                    ligne.innerHTML = getRowDetailCommande(commandesDetails[i], detailCompteur++,'', true);
                                    
                                    tableau.appendChild(ligne);

                                    ligne = document.createElement("tr");
                                    ligne.id = 'table-detail-ligne-lv'+detailCompteur;
                                    
                                    commandesDetails[i].statut = 'LV';
                                    
                                    ligne.innerHTML += getRowDetailCommande(commandesDetails[i], detailCompteur-1, '', false);
                                    
                                    tableau.appendChild(ligne);
                                                                        
                                    //setTypeParc(detailCompteur-1);
                                    
                                    $('#select'+(detailCompteur-1)).change(function(){
                                        
                                    });
                                    /**
                                     * Calcul automatique des volumes dans le parc et vu sur la Lettre Voiture
                                     * On commance par celui dans le parc
                                     */
                                    $('#zone-longueur'+(detailCompteur-1)).change(function(){
                                        var index = $(this).attr("data");
                                        var rayon = parseInt($('#zone-moy-diam'+index).val())/2;
                                        var hauteur = parseInt($(this).val());

                                        $('#zone-cubage'+index).val( (rayon * rayon * Math.PI * hauteur) / Math.pow(10, 6) );
                                    });

                                    
                                    $('#zone-longueur'+(detailCompteur-1)+'-lv').change(function(){
                                        var index = $(this).attr("data");
                                        var rayon = ((parseInt($('#zone-gd-diam'+index+'-lv').val())+parseInt($('#zone-ptt-diam'+index+'-lv').val()))/4);
                                        var hauteur = parseInt($(this).val());
                                        $('#zone-cubage'+index+'-lv').val( (rayon * rayon * Math.PI * hauteur) / Math.pow(10, 6) );
                                        if($('#zone-cubage'+index+'-lv').val().length === 0){
                                            document.getElementById('select'+index).checked = false;
                                            $('#zone-certificat'+index).removeAttr("required");
                                            $('#zone-essence'+index).removeAttr("required");
                                        }else{
                                            $('#zone-certificat'+index).attr("required","");
                                            $('#zone-essence'+index).attr("required","");
                                            document.getElementById('select'+index).checked = true;
                                        }

                                    });


                                    /**
                                     * Calcul automatique du diamètre moyen de la bille 
                                     */
                                    $('#zone-ptt-diam'+(detailCompteur-1)+', #zone-gd-diam'+(detailCompteur-1)).change(function(){
                                        var index = $(this).attr("data");
                                        $('#zone-moy-diam'+index).val((parseInt($('#zone-ptt-diam'+index).val())+ parseInt($('#zone-gd-diam'+index).val()))/2); 

                                        // Recalculer le volume en cas de modification du diamètre moyen

                                        $('#zone-longueur'+index).change();
                                    });
                                    $('#zone-ptt-diam'+(detailCompteur-1)+'-lv'+', #zone-gd-diam'+(detailCompteur-1)+'-lv').change(function(){
                                        var index = $(this).attr("data");
                                        // Recalculer le volume en cas de modification du diamètre moyen

                                        $('#zone-longueur'+index+'-lv').change();
                                    });

                                    setEssence(commandesDetails[i].essence, detailCompteur-1);
                                    
                                   //setCertificatList(i);

                                }
                            });
                        }
                        //setEssence();
                        setCertificatList();
                    }
               });
               
               $.ajax({
                   url: '/api/parc_chargement/by_commande',
                   method: 'GET',
                   dataType: 'json',
                   data: {commande: $(this).val()},
                   async: false,
                   complete: function (jqXHR, textStatus ) {
                        if(jqXHR.responseJSON !== null){
                            $('#zone-parc-chargement').val(jqXHR.responseJSON.ref);
                            $('#zone-origine').val((jqXHR.responseJSON.origine === null ? '': jqXHR.responseJSON.origine.ref))
                                          .attr('data', (jqXHR.responseJSON.origine == null ? '' : jqXHR.responseJSON.ref));
                        }
                   }
               });
            });
            
            $('#zone-commande-detail').change(function(){

            });
          
            loadData();
            
            $('select').select2();
        
        function getTypeParc(fonction = null){
            $.ajax({
                url: '/api/type_parc/list',
                method: 'GET',
                dataType: 'json',
                async: false,
                complete: function (cmd, textStatus ) {
                    typeParcs = cmd.responseJSON;
                    if(fonction !== null)
                        fonction();
                }
            });
        }
        
        function setTypeParc(index){
            getTypeParc(function(){
                var selectTypeParc = document.getElementById("zone-parc"+index);
                selectTypeParc.innerHTML = '';
                var o = document.createElement("option");
                o.value = '';
                o.innerHTML = '-- Selectionner Parc --';
                selectTypeParc.appendChild(o);
                for(var j=0; j<typeParcs.length; j++){
                    var opt = document.createElement("option");
                    opt.value = typeParcs[j].ref;
                    opt.innerHTML = typeParcs[j].libelle;
                    selectTypeParc.appendChild(opt);
                    opt.selected = false;
                }
                $('#zone-parc'+index).change();        
            });
        }
        
        var certificats = [];
        function loadCertificat(){
            $.ajax({
                url: '/api/type_parc/list',
                method: 'GET',
                async: false,
                complete: function (jqXHR, textStatus ) {
                    let data = jqXHR.responseJSON;
                    if(data === null){ certificats = []; return;}
                    certificats = data;
                }  
            });
        }
        
        function setCertificatList(index){
            // old
            //var data = certificats;
            // new
            var data = parcs;
            //var certificat = $("#zone-certificat"+index);
            var certificat = $('.zone-certificat');
            certificat.html('');
            certificat.css({width: '150px'});
            var o = document.createElement('option');
            o.innerHTML = '-- Select --';
            o.value = '';
            certificat.append($(o));
            for(var j=0; j<data.length; j++){
                var opt = document.createElement("option");
                // old
                // opt.value = data[j].libelle;
                // new
                opt.value = data[j].origine.ref;
                // old 
                // opt.innerHTML = data[j].libelle;
                // new
                opt.innerHTML = data[j].certificat;
                certificat.append($(opt));
            }
        }

        
        function getRowDetailCommande(detail, index=nbrDetailCommande, prix='', lettreVoitureData=false){
            var detailRow = detail;
            if(lettreVoitureData === false){
                return '<td colspan="2"><p style="width:200px; text-align: center; " class="text-bold">Données de la Lettre voiture</p><td>\
                        <td><input type="number" id="zone-longueur'+index+'-lv" min="0" class="form-control" data="'+index+'"></td>\
                        <td><input type="number" id="zone-gd-diam'+index+'-lv" min="0" class="form-control zone-diametre" data="'+index+'"></td>\
                        <td><input type="number" id="zone-ptt-diam'+index+'-lv" min="0" class="form-control zone-diametre" data="'+index+'"></td>\
                        <td><input type="number" id="zone-cubage'+index+'-lv" class="form-control" readonly=""></td>\
                        <td></td>\
                        <td></td>\
                        <td></td>\
                        <td></td>\
            ';
            }
            if(detailRow === null){
                detailRow = {
                    bille: '',
                    numBille: '',
                    longueur: 0,
                    gdDiam  :0,
                    pttDiam :0,
                    moyDiam :0,
                    volume  :0,
                    prix: prix,
                    observation: '',
                    statut: 'Commande',
                    essence:{prixUAchat:0}
                };
            }
            
            
            return '   \
                        <td><select class="select2 zone-essence" style="width: 100%" id="zone-essence'+index+'" data="'+index+'"></select></td>\
                        <td><input  type="text" id="zone-bille'+index+'" readonly="" placeholder="'+detailRow.numBille+'" value="'+detailRow.numBille+'" class="form-control" data="'+index+'"></td>\
                        <td><select id="zone-certificat'+index+'" class="select2 zone-certificat" style="width:100%" data="'+index+'"><option>-- Select --</option></select></td>\
                        <td><input  type="number" placeholder="'+detailRow.longueur+'" id="zone-longueur'+index+'" min="0" class="form-control zone-mesure" data="'+index+'"></td>\
                        <td><input  type="number" placeholder="'+detailRow.gdDiam+'" id="zone-gd-diam'+index+'" min="0" class="form-control zone-diametre" data="'+index+'"></td>\
                        <td><input  type="number" placeholder="'+detailRow.pttDiam+'" id="zone-ptt-diam'+index+'" min="0" class="form-control zone-diametre" data="'+index+'"></td>\
                        <td class="hide"><input type="number" placeholder="'+detailRow.moyDiam+'" id="zone-moy-diam'+index+'" min="0" class="form-control zone-diametre" readonly="" data="'+index+'"></td>\
                        <td><input type="number" placeholder="'+detailRow.volume+'" id="zone-cubage'+index+'" min="0" class="form-control" readonly="" data="'+index+'"></td>\
                        <td><input type="number" value="'+detailRow.volume+'" id="zone-attendu'+index+'" min="0" class="form-control" readonly="" data="'+index+'"></td>\
                        <td><input type="text" placeholder="'+detailRow.observation+'" id="zone-observation'+index+'" value="'+detailRow.observation+'" min="0" class="form-control" data="'+index+'">\
                        <td><input type="number" placeholder="'+detailRow.prix+'" id="zone-prix'+index+'" value="'+detailRow.prix+'" min="0" class="form-control" data="'+index+'"></td>\
                        <input  type="hidden" id="zone-statut'+index+'" value="'+detailRow.statut+'" data="'+index+'"></td>\
                        <td class="hide"><select id="zone-parc'+index+'" style="width:100%" data="'+index+'"></select></td>\
                        <td><input type="checkbox" id="select'+index+'" data="'+index+'"></td>\
            ';
        }
        
        function addRow(increment){
            
            var tr = document.createElement("tr");
            tr.id = 'table-detail-ligne'+increment;
            tr.setAttribute("class", "odd");
            tr.innerHTML =  '\
                        <td><select required="" class="select2 zone-essence" style="width: 100%" id="zone-essence'+increment+'" data="'+increment+'"></select></td>\
                        <td><input required="" type="text" placeholder="bille_'+increment+'" id="zone-bille'+increment+'" value="" class="form-control" data="'+increment+'"></td>\
                        <td><input required="" type="number" placeholder="0" id="zone-longueur'+increment+'" min="0" class="form-control zone-mesure" data="'+increment+'"></td>\
                        <td><input required="" type="number" placeholder="0" id="zone-gd-diam'+increment+'" min="0" class="form-control zone-diametre" data="'+increment+'"></td>\
                        <td><input required="" type="number" placeholder="0" id="zone-ptt-diam'+increment+'" min="0" class="form-control zone-diametre" data="'+increment+'"></td>\
                        <td class="hide"><input required="" type="number" placeholder="0" id="zone-moy-diam'+increment+'" min="0" class="form-control zone-diametre" readonly="" data="'+increment+'"></td>\
                        <td><input required="" type="number" placeholder="0" id="zone-cubage'+increment+'" min="0" class="form-control" readonly="" data="'+increment+'"></td>\
                        <td><input required="" type="number" placeholder="0" id="zone-attendu'+increment+'" value="0" min="0" class="form-control" readonly="" data="'+increment+'"></td>\
                        <td><input required="" type="text"   placeholder="B" id="zone-observation'+increment+'" class="form-control" data="'+increment+'">\
                        <td><input required="" type="number" placeholder="0" id="zone-prix'+increment+'" min="0" class="form-control" data="'+increment+'"></td>\
                            <input required="" type="hidden" value="Non Commande" id="zone-statut'+increment+'" data="'+increment+'"></td>\
                        <td class="hide"><select required="" id="zone-parc'+increment+'" style="width:100%" data="'+increment+'"></select></td>\
                        <td><input type="checkbox" id="select'+increment+'" data="'+increment+'" ></td>\
                        <td><button class="btn btn-danger btn-supprimer-detail" type="button" data="'+increment+'" title="Supprimer cette ligne"><i class="glyphicon glyphicon-trash"></i></button></td>\
            ';
            return tr;
        }
        
        /*** Ici on calcul le volume à partir de la refraction de la longueur et du diametre moyen
         * 
         */
        
        
    
    });
    </script>
    </body>
</html>
