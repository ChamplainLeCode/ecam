<%-- 
    Document   : Bon de reception
    Created on : 19 Avr. 2019, 19:42:16
    Author     : champlain
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="app.models.Commande"%>
<%@page import="java.util.List"%>

<%@page import="java.util.function.BiConsumer"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.PrintStream"%>
<%@page import="java.util.Map"%>

<%
    List<HashMap<String, String>> liste = (List<HashMap<String, String>>) request.getAttribute("data");
    Commande cmd = (Commande)request.getAttribute("commande");
    if(cmd == null || liste == null){
        %>
        <h1>Impossible d'afficher cette page</h1>
        <h1><button type="button" onclick="window.print(); window.close();">Cliquez ici</button> pour retourner</h1>
    <%
    }else{%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ECAM PLACAGES | Bon de Reception</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=0.6, maximum-scale=1, user-scalable=no" name="viewport">
        
                <link rel="stylesheet" href="/resources/css/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="/resources/css/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="/resources/Ionicons/css/ionicons.min.css">
        
        <link rel="stylesheet" href="/resources/css/dataTables.bootstrap.min.css">      <!-- Theme style -->
        
        <link rel="stylesheet" href="/resources/css/skin-blue.min.css">
        <link rel="stylesheet" href="/resources/iCheck/all.css">
        <style>
            @media print{
                .no-print{
                    visibility: hidden;
                }
            }
            
            @page{
                margin: 0cm;
                padding: 0.5cm;
            }
            
            body{
                font-size: 8pt;
            }
        </style>
    </head>

    <body>
    <div class="wrapper">



      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Main content -->
        <div class="row" style="background-color: white; padding: 40px;">
            <div style="margin-top: 30px; width: 95%;">

                <div class="container-fluid" style=" border-style: solid; border-width: 1px; padding: 20px;">

                    <button style="margin-right: 40px; margin-bottom: 20px;" onclick="window.print(); window.close();" type="button" id="btn-imprimer no-print" class="pull-left btn btn-primary no-print">
                        <i class="fa fa-print "></i> Imprimer le bon
                    </button>
                    <div class="col-sm-6" style=" margin-bottom: 10px; background: red; border-style: solid; border-radius: 10px; text-align: center;">
                        <p class="row" style="text-align: center; font-weight: bold">**********************</p>
                        <p class="row" style="text-align: center; font-weight: bold; font-size: 18px; margin-bottom: 5px;">BON DE RECEPTION</p>
                        <p class="row" style="text-align: center; font-size: 10px;">-----------------------------------------------------------</p>
                        <p>
                            <span class="pull-left"><span style="font-weight: bold"></span>N°:<%=request.getAttribute("num")%></span> <span id="zone-ref"></span></span> 
                            <span class="pull-right"><span style="font-weight: bold">Date:<%DecimalFormat df = new DecimalFormat("##"); out.print(df.format(new Date().getDay()+1)+"/"+df.format(new Date().getMonth()+1)+"/"+df.format((new Date().getYear()+1900)));%><span> <span id="zone-date"></span></span> 
                        </p>
                    </div>
                    
                    <p class="pull-left col-sm-6">Fournisseur: <%=cmd.getRefFournisseur().getNomFournisseur()%><span style="font-weight: bold"></span></p>
                    <p class="pull-left col-sm-6">Service Émetteur: <span ></span></p>
                    <p class="pull-left col-sm-6">Parc de Chargement:  <input type="text" class="form-control" style="border:none" placeholder="Saisissez le Parc">  <span style="font-weight: bold"></span></p>
                    <p class="pull-left col-sm-6">N° Contribuable:  <%=cmd.getRefFournisseur().getContribuable()%>  <span style="font-weight: bold"></span></p>
                    <p class="pull-left col-sm-6">Acheteur: <span style="font-weight: bold"></span></p>
                    <p class="pull-left col-sm-6">Parc de Destination: <span style="font-weight: bold">ECAM-PLACAGE</span></p>
                    
                    <table id="zone-table" class="table  table-striped table-hover" style="width: 100%;">
                        <thead>
                            <tr>
                                <th style=" border: 1px solid #123456;" rowspan="2">N° Bille</th>
                                <th style=" border: 1px solid #123456;" rowspan="2">Essence</th>
                                <th style=" border: 1px solid #123456;" rowspan="2">Certif</th>
                                <th style=" border: 1px solid #123456;" colspan="5">Dimensions</th>
                                <th style=" border: 1px solid #123456;" colspan="1">Prix</th>
                                <th style=" border: 1px solid #123456;" rowspan="2">M³ Réel</th>
                                <th style=" border: 1px solid #123456;" rowspan="2">Class</th>
                            </tr>
                            <tr>
                                <th style=" border: 1px solid #123456;">Long</th>
                                <th style=" border: 1px solid #123456;">G.D</th>
                                <th style=" border: 1px solid #123456;">P.D</th>
                                <th style=" border: 1px solid #123456;">D.M</th>
                                <th style=" border: 1px solid #123456;">M³ Ref</th>
                                <th style=" border: 1px solid #123456;">Prix Achat</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% float volRecu = 0f, prix = 0f, volAttendu = 0f;

                            for(HashMap<String, String> row : liste){%>
                                <tr><%System.out.println(row);%>
                                    <td style=" border: 1px solid #123456;"><%=row.get("bille")%></td>
                                    <td style=" border: 1px solid #123456;"><%=row.get("essence")%></td>
                                    <td style=" border: 1px solid #123456;"><%=row.get("certificat")%></td>
                                    <td style=" border: 1px solid #123456;"><%=row.get("longueur")%></td>
                                    <td style=" border: 1px solid #123456;"><%=row.get("gdDiam")%></td>
                                    <td style=" border: 1px solid #123456;"><%=row.get("pttDiam")%></td>
                                    <td style=" border: 1px solid #123456;"><%=row.get("moyDiam")%></td>
                                    <td style=" border: 1px solid #123456;"><%=row.get("volume")%></td>
                                    <td style=" border: 1px solid #123456;"><%=row.get("prix")%></td>
                                    <td style=" border: 1px solid #123456;"><%=row.get("oldVolume")%></td>
                                    <td style=" border: 1px solid #123456;"><%=row.get("observation")%></td>
                                </tr>
                            <% 
                            
                                volRecu    = volRecu + Float.parseFloat(row.get("volume"));
                                prix       = prix + Float.parseFloat(row.get("prix"));
                                volAttendu = volAttendu + Float.parseFloat(row.get("oldVolume").toString());
                                
                            }%>
                        </tbody>
                        <tfoot>
                            <tr style="font-weight: bold">
                                <td style=" border: 1px solid #123456;" colspan="6"><%="Total_:   "+liste.size()%></td>
                                <td style=" border: 1px solid #123456;"><%=volRecu+""%></td>
                                <td style=" border: 1px solid #123456;"><%=prix+""%></td>
                                <td style=" border: 1px solid #123456;"><%=volAttendu+""%></td>
                                <td style=" border: 1px solid #123456;"></td>
                            </tr>
                            <tr style="font-weight: bold">
                                <td colspan="10"><h2 style="font-size: 8pt;"><%=cmd.getTypePaie()+  ": "+(cmd.getTypePaie() == Commande.PRIX_RENDU ? cmd.getMontantTransport()+" FCFA" : "")%></h2></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <script src="/resources/js/my/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="/resources/css/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/resources/js/my/demo.js"></script>
    
    <script src="/resources/js/dataTables.bootstrap.min.js"></script>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. -->
    
    <script type="text/javascript">

        $(function(){
            document.getElementById("btn-imprimer").onclick = function(){
                tableToExcel('liste', 'W3C Example Table');
                
            };
            
        });
          window.stop();  

    </script>
    <script src="/resources/js/slimscroll/jquery.slimscroll.min.js"></script>
    </body>
</html>
<% }%>