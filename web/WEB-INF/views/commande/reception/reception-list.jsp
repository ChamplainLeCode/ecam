<%-- 
    Document   : totoPage
    Created on : 8 déc. 2018, 14:42:16
    Author     : champlain
--%>

<%@page import="app.models.Reception"%>
<%@page import="app.models.DetailReception"%>
<%@page import="app.models.Commande"%>
<%@page import="java.util.List"%>
<%@page import="app.models.Fournisseur"%>
<%@page import="java.util.Date"%>

<%
    
        List<Reception> liste = (List<Reception>) request.getAttribute("liste");
        out.print("Liste = "+liste.size());
%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!-- Main content --><div class="col-sm-2"></div>
        <section class="content container-fluid col-sm-8 ">

            <div class="box box-danger">
                <div class="box-header">
                  <h1 class=" pull-left">Liste de receptions</h1>
                </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="list-fournisseurs" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 227.3px;" aria-label="Browser: activate to sort column ascending">Num_Bille</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 179.75px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Longueur</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 206.917px;" aria-label="Platform(s): activate to sort column ascending">Petit Diam</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 153.933px;" aria-label="Engine version: activate to sort column ascending">Grand Diam</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 153.933px;" aria-label="Engine version: activate to sort column ascending">Moyen Diam</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 110.1px;" aria-label="CSS grade: activate to sort column ascending">Volume</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 110.1px;" aria-label="CSS grade: activate to sort column ascending">Nombre</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    <%
                                        if(liste.size() > 0){
                                            for (Reception reception : liste) {
                                    %>
                                    <tr role="row" class="odd command">
                                        <th class="sorting_1" colspan="6"><input type="checkbox" checked="" name="toprint" value="<% // id de la commande %>" style="vertical-align: bottom"> <% out.print(new Date().toGMTString()); %> (REF_RECEP129ZS93)</th>
                                        <th>
                                            <button class="btn btn-primary btn-print-command" data="<%  out.print(reception.getRefReception()); %>" title="Imprimer le PV"><i class="glyphicon glyphicon-print"></i></button>
                                        </th>
                                    </tr> 
                                    <%          for(DetailReception detail : reception.getDetailReceptionList()){ %>
                                    <tr role="row" class="odd command-detail">
                                        <td><% out.print(detail.getNumBille()); %></td>
                                        <td class="sorting_1">${detail.getLongueur()} (m)</td>
                                        <td>${detail.getPtitDiam()} (cm)</td>
                                        <td>${detail.getGdDiam()} (cm)</td>
                                        <td>${detail.getDiaMoyen()} (cm)</td>
                                        <td>${detail.getVolume()} (m³)</td>
                                        <td>${detail.getNombre()}</td>
                                    </tr>          
                                    <%
                                            }

                                        }
                                    } %> 
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
          </div>

        </section>
