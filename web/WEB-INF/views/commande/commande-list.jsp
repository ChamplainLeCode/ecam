<%-- 
    Document   : totoPage
    Created on : 8 déc. 2018, 14:42:16
    Author     : champlain
--%>

<%@page import="app.models.Commande"%>
<%@page import="java.util.List"%>
<%@page import="app.models.Fournisseur"%>
<%
    /**
        Fournisseur fournisseur = (Fournisseur) request.getAttribute("fournisseur");  */
        List<Commande> liste = (List<Commande>) request.getAttribute("liste");
        
        
  
%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>

        <link rel="stylesheet" href="/resources/daterangepicker/daterangepicker.css">
        <link rel="stylesheet" href="/resources/css/datepicker/css/bootstrap-datepicker.min.css">
        <link rel="stylesheet" href="/resources/colorpicker/css/bootstrap-colorpicker.min.css">

        <!-- Main content -->
        <section class="content container-fluid">

            <div class="modal fade" id="zoneReceptionList" tabindex="-1" role="dialog" aria-labelledby="zoneReceptionList" aria-hidden="true"></div>

            <div class="box">
                <div class="box-header">
                    <button class="btn btn-bitbucket pull-left" onclick="history.back()"><i class="glyphicon glyphicon-arrow-left"></i> Retour</button>
                    <button class="btn btn-google pull-left btn-print-all-commande" title="Imprimer la liste de toutes les commandes"><i class="glyphicon glyphicon-print"></i> Imprimer</button>
                    <button class="btn btn-google pull-right btn-add-commande" data-toggle="modal" data-target="#zoneReceptionList"  title="Ajouter une commande"><i class="glyphicon glyphicon-shopping-cart"></i> Ajouter une commande</button>
                </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="list-fournisseurs" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 179.75px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Reférence</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 227.3px;" aria-label="Browser: activate to sort column ascending">Volume</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 153.933px;" aria-label="Engine version: activate to sort column ascending">Essence</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 110.1px;" aria-label="CSS grade: activate to sort column ascending">Opération</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        if(liste.size() > 0){
                                         for (Commande elem : liste) {
                                    %>
                                    <tr role="row" class="odd command">
                                        <th class="sorting_1" colspan="4">
                                            <input type="checkbox" name="toprint" value="REF_12192919A<% // id de la commande %>" style="vertical-align: bottom"> 
                                            <span>${elem.getRefCommande()}</span>
                                        </th>
                                        <th>
                                            <button class="btn btn-primary btn-print-command" data="REF_12192919A<%  out.print(elem.getRefCommande()); %>" title="Imprimer cette commande"><i class="glyphicon glyphicon-print"></i></button>
                                            <button class="btn btn-success btn-reception" data-toggle="modal" data-target="#zoneReceptionList" data="<% out.print(elem.getRefCommande()); %>" title="Visualiser les receptions"><i class="glyphicon glyphicon-eye-close"></i></button>
                                            <button class="btn btn-pinterest btn-fournisseur"  data-toggle="modal" data-target="#zoneReceptionList" data="<%  out.print(elem.getRefCommande()); %>" title="voir le fournisseur"><i class="glyphicon glyphicon-user"></i></button>
                                        </th>
                                    </tr> 
                                    <tr role="row" class="odd command-detail">
                                        <td class="sorting_1"></td>
                                        <td>1'000 m³<% // out.print(elem.getNomFournisseur()); %></td>
                                        <td>403<% // out.print(elem.getContribuable()); %></td>
                                        <td>Bibiga <% // out.print(elem.getContact());%></td>
                                        <td></td>
                                    </tr>      
                                    <tr role="row" class="odd command-detail">
                                        <td class="sorting_1"></td>
                                        <td>1'000 m³<% // out.print(elem.getNomFournisseur()); %></td>
                                        <td>403<% // out.print(elem.getContribuable()); %></td>
                                        <td>Bibiga <% // out.print(elem.getContact());%></td>
                                        <td></td>
                                    </tr>      
                                    <tr role="row" class="odd command-detail">
                                        <td class="sorting_1"></td>
                                        <td>1'000 m³<% // out.print(elem.getNomFournisseur()); %></td>
                                        <td>403<% // out.print(elem.getContribuable()); %></td>
                                        <td>Bibiga <% // out.print(elem.getContact());%></td>
                                        <td></td>
                                    </tr>      
                                    <%
                                        }
                                    }
  
                                    %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
          </div>

        </section>

    <script src="/resources/min/moment.min.js"></script>
    <script src="/resources/daterangepicker/daterangepicker.js"></script>
    <script src="/resources/colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script src="/resources/css/datepicker/js/bootstrap-datepicker.min.js"></script>

    <script src="/resources/js/commande/commande-list.js"></script>
