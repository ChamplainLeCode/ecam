<%-- 
    Document   : totoPage
    Created on : 8 déc. 2018, 14:42:16
    Author     : champlain
--%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ecam commande list</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

            <%@include file="/WEB-INF/views/layouts/style.jsp" %>
    
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <%@include file="/WEB-INF/views/layouts/topBar.jsp" %>
      <!-- Left side column. contains the logo and sidebar -->
      <%@include file="/WEB-INF/views/layouts/sideBar.jsp" %>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Commandes
            <small>Liste des fournisseurs</small>
            <label class="label label-danger hide" id="status-fail"></label>
            <label class="label label-success hide" id="status-success"></label>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="">ecam</li>
            <li class="">fournisseurs</li>
            <li class="active">list</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content container-fluid " id="commande-container">

        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <!-- Main Footer -->
      <%@include file="/WEB-INF/views/layouts/footer.jsp" %>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <%@include file="/WEB-INF/views/layouts/script.jsp" %>    

    <script type="text/javascript">
        $(function(){
            $.ajax({
                url: '/commande/list',
                method: 'GET',
                success: function (data, textStatus, jqXHR) {
                    $('#commande-container').html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#commande-container').html(jqXHR.responseText);
                },
                beforeSend: function (xhr) {
                    $('#commande-container').html("<h2 style=\"color:gray\">Téléchargement des données en cours</h2>");
                }
            });
        });
    </script>
    </body>
</html>
