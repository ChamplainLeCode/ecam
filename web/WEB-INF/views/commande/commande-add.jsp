<br><br><br><br>
<div class="col-sm-3 col-sm-3"></div>
<div class="col-sm-6">
    <div class="box box-primary">
        <div class="box-header">
            Ajouter une commande
        </div>
        <div class="box-body">
            <div class="form-group col-sm-12">
                <div class="input-group col-sm-5 pull-left">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" placeholder="date" class="form-control pull-right" id="datepicker">
                </div>
                <div class="input-group col-sm-5 pull-right">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                    <input type="text" placeholder="Tel" class="form-control" data-inputmask="&quot;mask&quot;: &quot;(\+999) 999-9999&quot;" data-mask="">
                </div>
            </div>
            <div class="row">
                <button class="btn btn-success pull-right" style=" border-radius: 45px; margin-right: 10px;"><i class="glyphicon glyphicon-plus"></i></button>

            </div>
            <div class="command-detail"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#datepicker').datepicker({
      autoclose: true
    });
    $('[data-mask]').inputmask();
</script>