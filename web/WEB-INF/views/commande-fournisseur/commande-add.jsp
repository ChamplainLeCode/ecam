<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<style>.mesure-unite{font-size: 11px; margin-left: 3px; color: gray;}</style>


<div id="form-add-commande">
    <p class="status-add-commande" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal " onsubmit="return (function(){return save(this);})();">
      <div class="box-body">
            <div class="row">
                
                <div class="form-group col-sm-3  col-xs-12 pull-left">
                  <label for="zone-ref" class="col-sm-4 col-xs-12  control-label">N° Commande</label>
                  <div class="col-sm-8  col-xs-12 pull-right">
                      <input pattern="[0-9]+" required="" type="text" title="Veillez fournir le numéro de la commande  chiffres attendus" id="zone-ref" class="form-control">
                  </div>
                </div>
                
                <div class="form-group col-sm-3 col-xs-12 pull-left">
                  <label for="zone-fournisseur" class="col-sm-4  col-xs-12 control-label">Fournisseur</label>
                  <div class="col-sm-8  col-xs-12 pull-right">
                      <select required="" class="select2" style="width:100%" id="zone-fournisseur" ></select>
                  </div>
                </div>
                
                <div class="form-group col-sm-3 col-xs-12 pull-left hide">
                  <label for="zone-parc" class="col-sm-4  col-xs-12 control-label">Nom sociéte</label>
                  <div class="col-sm-8  col-xs-12 pull-right">
                      <select <%/*required=""*/%> class="select2" style="width:100%" id="zone-parc" ></select>
                  </div>
                </div>
                
                <div class="form-group col-sm-3  col-xs-12  pull-left">
                  <label for="zone-date" class="col-sm-4  col-xs-12 control-label">Date</label>
                  <div class="col-sm-8  col-xs-12 pull-right">
                      <input required="" type="date" id="zone-date" class="form-control date">
                  </div>
                </div>

            </div><br>
        <table id="table-addCommande" class="table table-bordered table-hover dataTable form-group" role="grid" aria-describedby="table-addCommande_info">
                <thead>
                    <tr role="row">
                        <th class="sorting" tabindex="0" aria-controls="table-addCommande" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending">Essence</th>
                        <th class="sorting" tabindex="0" aria-controls="table-addCommande" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">N° Bille.........</th>
                        <th class="sorting_desc" tabindex="0" aria-controls="table-addCommande" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" aria-sort="descending">Long.</th>
                        <th class="sorting" tabindex="0" aria-controls="table-addCommande" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Grand Diam</th>
                        <th class="sorting" tabindex="0" aria-controls="table-addCommande" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Ptt Diam</th>
                        <th class="sorting" tabindex="0" aria-controls="table-addCommande" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Moy Diam</th>
                        <th class="sorting" tabindex="0" aria-controls="table-addCommande" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Cubage</th>
                        <th class="sorting" tabindex="0" aria-controls="table-addCommande" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Prix(FCFA)</th>
                        <th class="sorting" tabindex="0" aria-controls="table-addCommande" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Obs</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="table-addCommande-body"></tbody>
                <tfoot>
                  <tr>
                    <td colspan="10">
                        <button type="button" id="btn-add-detail" class="pull-right btn btn-primary" style="width:40px; height: 40px; border-radius: 20px;"><i class="glyphicon glyphicon-plus"></i></button>
                    </td>
                  </tr>
                    <tr>
                        <td colspan="10">
                            <div>
                                <legend>Mode Facturation</legend>
                                <input id="zone-type-prix" class="form-control" type="checkbox" data-toggle="toggle" data-on="Prix Départ" data-off="Prix Rendu">
                                <input type="number" min="0" placeholder="Prix transport" id="zone-prix-transport" style="width: 150px; margin-left: 10px; height: 34px;">
                            </div>
                      </td>
                    </tr>
                </tfoot>
            </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
          <button type="button" id="btn-close-modal"  data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="submit" class="btn btn-info pull-right" id="btn-save-commande">Enregistrer</button>
            <div class="overlay hide" style="text-align:center;"> 
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>

    <script src="/resources/js/jquery.dataTables.min.js"></script>
    
    <script src="/resources/js/dataTables.bootstrap.min.js"></script>

    <link href="/resources/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="/resources/js/my/bootstrap-toggle.min.js"></script>

        <script>
            $(function() {
              $('#zone-type-prix').bootstrapToggle({
                on: 'Enabled',
                off: 'Disabled'
              }).change(function(){
                    if(this.checked){
                        $('#zone-prix-transport').hide();
                        $(this).val($(this).attr("data-on"));
                    }else{
                        $('#zone-prix-transport').show();
                        $(this).val($(this).attr("data-off"));
                    }
              });
            });
          </script>
    <script type="text/javascript">
        
        var numDetail = 0;
        var fournisseurs = [];
        var commande = [];
        var essence = [];
        var titres = {};
        /**
         * Initialisatin de la valeur de la ref commande
         */
        $('#zone-ref').attr('placeholder', '000000');
                

        
        getEssence();
    
    
        getFournisseur();
        
        $('#zone-parc').select2();
        
        $('#zone-fournisseur').change(function(){
            var value = $(this).val();
            $.ajax({
                url: '/api/parc_chargement/by_fournisseur',
                method: 'GET',
                dataType: 'json',
                data:{fournisseur: value},
                async: false
            }).then(function(data){
                var select = document.getElementById("zone-parc");
                select.innerHTML = '';
                var opt = document.createElement("option");
                opt.value = '';
                opt.innerHTML = '--Selectionnez--';
                select.appendChild(opt);
               for(var j=0; j<data.length; j++) {
                    opt = document.createElement("option");
                    opt.value = data[j].ref;
                    console.log(data[j].ref);
                    opt.innerHTML = data[j].libelle;
                    titres[data[j].ref] = data[j].titre;
                    select.appendChild(opt);
                }
                $(select).change(function(){
                    $('#table-addCommande-body').html('');
                    //console.log(titres[this.value]);
                    $('#zone-numero-titre').html('');
                    $('#zone-data-titre').html('');
                    $.ajax({
                        async: false,
                        url: '/api/titre/for_parc',
                        method: 'GET', 
                        dataType: 'json',
                        data: {refParc: $(this).val()},
                        complete: function (jqXHR, textStatus ) {
                            var titre = jqXHR.responseJSON;
                            console.log(titre);
                            if(titre === null){
                                window.essence = [];
                                $('#zone-data-titre').html('Aucun titre trouvé');
                                window.essence = [];
                                return;
                            }
                            window.essence = titre.essences;
                            $('#zone-numero-titre').html(titre.numTitre);
                            $('#zone-data-titre').html(new Date(titre.dateFin).toDateString());
                            var date = new Date(titre.dateFin);
                            if(date.getTime() < new Date().getTime()){
                                $('#zone-data-titre').html('Titre expiré');
                                $('#zone-numero-titre').html('');
                                window.essence = [];
                            }
                        }
                    });
                });
            });
        }).change();
        var numerosBilles = Array();
        $('#btn-add-detail').click(function(){
            var ligne = document.createElement("tr");
            ligne.id = "detail-"+numDetail;
            ligne.className = "odd"
            ligne.innerHTML = getRowDetailCommande(null);
            
        
        // Insertion de la ligne dans le tableau
            document.getElementById("table-addCommande-body").appendChild(ligne);
            console.log(numerosBilles);
            var oldIn = null;
            $('#zone-bille-'+numDetail)
                .focusin(function(){oldIn = $(this).val(); console.log('On save = '+oldIn); })
                .change(function(){
                    var ind = $(this).attr('id').split('-')[2];
                    numerosBilles.findIndex(function(e, index){
                        console.log("test donnée = "+oldIn+" cmp = "+e+" position = "+index+" rest "+(e == oldIn));
                        if(e == oldIn)
                            numerosBilles.splice(index, 1);
                    });
                    oldIn = null;
                    if($(this).val().length > 0){
                        let see = false;
                        for(let i = 0; i<numerosBilles.length; i++){
                            if(numerosBilles[i] == $(this).val()){
                                see = true;
                                break;
                            }
                        }
                        if(see === true){
                            $('#zone-bille-lab-'+ind).html('Existant '+$(this).val());
                            $(this).val('').css({"border": "red 1px solid"});
                        }else{
                            numerosBilles.push($(this).css({"border": ""}).val());
                            $('#zone-bille-lab-'+ind).html('');
                        }
                        oldIn = null;
                    }
                    console.log(numerosBilles);
                });
            
        // Insertion des données d'essence
            var select = document.getElementById('zone-essence-'+numDetail);
                var selected=-1;
                //console.log(window.essence);
                for(var i=0; i<window.essence.length; i++){
                    var opt = document.createElement("option");
                    opt.value = window.essence[i].ref;
                    opt.innerHTML = window.essence[i].libelle;
                    select.appendChild(opt);
                    
                    /** 
                     * Si il existe une ligne précédente, alors créer la ligne courant de même
                     * essence que la précedente
                     */
                    try{
                        if($("#zone-essence-"+(numDetail-1)).val() === essence[i].ref){
                            $('#zone-prix-'+numDetail).val(essence[i].prixUAchat);
                            opt.selected = true;
                        }
                    }finally{}
                }
                
                /**
                 * Si La valeur de l'essence vient à changer, modifier le prix d'achat aussi
                 */
            $('#zone-essence-'+numDetail).change(function(){
                $('#zone-prix-'+$(this).attr("data")).val(essence[document.getElementById('zone-essence-'+$(this).attr("data")).selectedIndex].prixUAchat);
            }).select2().change();
            
            /**
             * Calcul du cubage total de la bille
             */
            $('#zone-longueur-'+numDetail).change(function(){
                var index = $(this).attr("data");
                var rayon = parseInt($('#zone-moy-diam-'+index).val())/2;
                var hauteur = parseInt($(this).val());
                
                $('#zone-cubage-'+index).val( (rayon * rayon * Math.PI * hauteur) / Math.pow(10, 6) );
            });

            
            /**
             * Calcul automatique du diamètre moyen de la bille 
             */
            $('#zone-ptt-diam-'+numDetail+', #zone-gd-diam-'+numDetail).change(function(){
                var index = $(this).attr("data");
                $('#zone-moy-diam-'+index).val((parseInt($('#zone-ptt-diam-'+index).val())+ parseInt($('#zone-gd-diam-'+index).val()))/2); 
                
                // Recalculer le volume en cas de modification du diamètre moyen
                
                $('#zone-longueur-'+index).change();
            });
            
            /**
             * Bouton permettant de supprimer la ligne courante
             */
            $('.btn-supprimer-detail').click(function(){
               var index = $(this).attr("data"); 
               document.getElementById("table-addCommande-body").removeChild(document.getElementById("detail-"+index));
            });
            numDetail++;
            //for(var i=0; i<numDetail; i++){
            //}
        });

    
    
        function getRowDetailCommande(detail){
            var detailRow = detail;
            if(detailRow === null){
                detailRow = {
                    bille: '',
                    longueur: 0,
                    gdDiam  :0,
                    pttDiam :0,
                    moyDiam :0,
                    cubage  :0,
                    essence:{prixUAchat:0}
                };
            }
            return '    <td><select required="" class="select2 zone-essence" style="width: 100%" id="zone-essence-'+numDetail+'" data="'+numDetail+'"></select></td>\
                        <td><input required="" type="text" id="zone-bille-'+numDetail+'" value="'+detailRow.bille+'" class="form-control" data="'+numDetail+'"><span style="color: red;" id="zone-bille-lab-'+numDetail+'"></span></td>\
                        <td><input required="" type="number" value="'+detailRow.longueur+'" id="zone-longueur-'+numDetail+'" min="0" class="form-control zone-mesure" data="'+numDetail+'"></td>\
                        <td><input required="" type="number" value="'+detailRow.gdDiam+'" id="zone-gd-diam-'+numDetail+'" min="0" class="form-control zone-diametre" data="'+numDetail+'"></td>\
                        <td><input required="" type="number" value="'+detailRow.pttDiam+'" id="zone-ptt-diam-'+numDetail+'" min="0" class="form-control zone-diametre" data="'+numDetail+'"></td>\
                        <td><input required="" type="number" value="'+detailRow.moyDiam+'" id="zone-moy-diam-'+numDetail+'" min="0" class="form-control zone-diametre" readonly="" data="'+numDetail+'"></td>\
                        <td><input required="" type="number" value="'+detailRow.cubage+'" id="zone-cubage-'+numDetail+'" min="0" class="form-control" readonly="" data="'+numDetail+'"></td>\
                        <td><input required="" type="number" value="'+detailRow.essence.prixUAchat+'" id="zone-prix-'+numDetail+'" min="0" class="form-control" required="" data="'+numDetail+'"></td>\
                        <td><input required="" type="text" id="zone-observation-'+numDetail+'"  class="form-control" data="'+numDetail+'"></td>\
                        <td><button class="btn btn-danger btn-supprimer-detail" type="button" data="'+numDetail+'" title="Supprimer cette ligne"><i class="glyphicon glyphicon-trash"></i></button></td>\
            ';
        }

        function parseDate(dateStringify = (new Date().getYear()+1990)+"-"+new Date().getMonth()+"-"+new Date().getDate()){
            var date = dateStringify.split('-');
            // //console.log(date);
            return new Date(parseInt(date[0]), parseInt(date[1]), parseInt(date[2]));
        }
        
        function save(){
            var data = [];
            var i = 0;
            
            $('.zone-mesure').each(function(){
                i = parseInt($(this).attr('id').split('-')[2]);
                
                var volume = $('#zone-cubage-'+i).val();
//                console.log(parseFloat(volume).toFixed(3));
                volume = parseFloat(volume).toFixed(3);
                // //console.log(volume);
               data.push({
                   essence: $('#zone-essence-'+i).val(),
                   bille: $('#zone-bille-'+i).val(),
                   longueur: $('#zone-longueur-'+i).val(),
                   gdDiam: $('#zone-gd-diam-'+i).val(),
                   pttDiam: $('#zone-ptt-diam-'+i).val(),
                   prix: $('#zone-prix-'+i).val(),
                   moyDiam: $('#zone-moy-diam-'+i).val(),
                   volume: volume,
                   observation: $('#zone-observation-'+i).val()
               });
            });
            //console.log($('#zone-parc').val());
            if(confirm('Confirmer l\'enregistrement de cette commande?')){
                saveData(
                    $('#zone-ref').val(), 
                    $('#zone-fournisseur').val(), 
                    $('#zone-parc').val(), 
                    parseDate($('#zone-date').val()).getTime(), 
                    $('#zone-type-prix').val(), 
                    $('#zone-prix-transport').val() === '' ? 0 : $('#zone-prix-transport').val(), 
                    data
                );
                $('#form-add-commande .overlay').removeClass('hide');
                $('#form-add-commande #btn-save-commande').attr("disabled","");
            }
            return false;
        }

    </script>
</div>
