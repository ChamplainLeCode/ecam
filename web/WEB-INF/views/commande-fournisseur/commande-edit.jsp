
<%@page import="org.json.JSONObject"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<style>.mesure-unite{font-size: 11px; margin-left: 3px; color: gray;}</style>

<table id="equipe-liste">
    <tbody></tbody>
</table>

<div id="form-add-commande">
    <p class="status-add-commande" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal " onsubmit="return save(this)">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Reférence</label>
          <div class="col-sm-10">
              <input type="text" disabled="" value="<%=request.getParameter("ref")%>" class="form-control" id="zone-ref" placeholder="Reférence du Billon">
          </div>
        </div>
        <div class="form-group">
            <label for="zone-client" class="col-sm-2 control-label">Client</label>

          <div class="col-sm-10">
              <select class="form-control select2" required="" style="width:100%;" id="zone-client">
              </select>
          </div>
        </div>
        <div class="form-group">
          <label for="zone-type-coupe" class="col-sm-2 control-label">Type de coupe</label>
          <div class="col-sm-10">
              <select  class="form-control" required="" id="zone-type-coupe"></select>
          </div>
        </div>
          <table class="form-group" >
              <tbody id="panel-detail"></tbody>
              <tfoot>
                  <tr>
                      <td colspan="6">
                          <button type="button" id="btn-add-detail" class="pull-right btn btn-primary" style="width:50px; height: 50px; border-radius: 25px;"><i class="glyphicon glyphicon-plus"></i></button>
                      </td>
                  </tr>
              </tfoot>
          </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="submit" class="btn btn-info pull-right" id="btn-edit-commande">Enregistrer</button>
            <div class="overlay hide" style="text-align:center;"> 
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>

    <script src="/resources/js/jquery.dataTables.min.js"></script>
    
    <script src="/resources/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
        var nbrDetail = 0;
        var essences = [];
        
        var _data = (<%=(new JSONObject(request.getParameterMap())).toString()%>);

        $.ajax({
            url: '/api/essence/list',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                essence = JSON.parse(jqXHR.responseText);
                initDetailCommandeList();
            }
        });

        $.ajax({
            url: '/api/client/list',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var client = JSON.parse(jqXHR.responseText);
                var select = document.getElementById('zone-client');
                var selected=-1;
                for(var i=0; i<client.length; i++){
                    var opt = document.createElement("option");
                    opt.value = client[i].ref;
                    opt.innerHTML = client[i].nom+' ('+client[i].nationalite+' | '+client[i].telephone+')';
                    select.appendChild(opt);
                    if(client[i].nom+' ('+client[i].telephone+')' === '<%=request.getParameter("client")%>'){
                        opt.selected = true;
                    }
                }

                select.selected = selected;
                $('#zone-client').select2();
            }
        });
    
        
        $.ajax({
            url: '/api/type_coupe/list',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var type_coupe = JSON.parse(jqXHR.responseText);
                var select = document.getElementById('zone-type-coupe');
                var selected=-1;
                for(var i=0; i<type_coupe.length; i++){
                    var opt = document.createElement("option");
                    opt.value = type_coupe[i].ref;
                    opt.innerHTML = type_coupe[i].libelle;
                    select.appendChild(opt);
                    if(type_coupe[i].libelle === '<%=request.getParameter("typeCoupe")%>'){
                        opt.selected = true;
                    }
                }

                select.selected = selected;
                $('#zone-type-coupe').select2();
            }
        });
        
        function initDetailCommandeList(){
            for(var j =0; j<_data.taille; j++){
                var ligne = document.createElement("tr");
                ligne.id = "detail-"+nbrDetail;
                ligne.innerHTML = getRowDetailCommande({
                    largeur: _data["detailCommande["+j+"][volumeRecu]"], 
                    volume: _data["detailCommande["+j+"][volume]"], 
                    essence: null
                });

                document.getElementById("panel-detail").appendChild(ligne);
                var select = document.getElementById('zone-essence-'+nbrDetail);
                    var selected=-1;
                    for(var i=0; i<essence.length; i++){
                        var opt = document.createElement("option");
                        opt.value = essence[i].ref;
                        opt.innerHTML = essence[i].libelle;
                        if("'"+_data["detailCommande["+j+"][essence][ref]"]+"'" == "'"+essence[i].ref+"'"){
                            opt.selected = true;
                        }
                        select.appendChild(opt);
                    }
   //                 select.selected = selected;
                $('.zone-mesure').change(function(){getVolume($(this).attr("id").split('-')[2]);});
                $('#zone-essence-'+nbrDetail).select2();
                nbrDetail++;

            }
        }
        
        function getRowDetailCommande(detailRow){
            if(detailRow === null){
                detailRow = {longueur: 0, largeur: 0, epaisseur: 0.0, volume:0, essence:null};
            }
            return '<td>'+
                        '<button onclick="removeRow(\'detail-'+nbrDetail+'\')" class="btn btn-danger" type="button" style=""><i class="glyphicon glyphicon-remove"></i></button>'+
                    '</td><td></td>'+
                    '<td style="padding:5px;">'+
                        '<div class="form-group pull-right">'+
                          '<label for="zone-gd-diam" class="col-sm-12 control-label">Volume recu<span class="mesure-unite">(cm)</span></label>'+
                         ' <div class="col-sm-12">'+
                              '<input style="width:100px;" class=" value="0" readonly="" pull-right form-control zone-mesure zone-largeur" required="" id="zone-largeur-'+nbrDetail+'" value="'+detailRow.largeur+'" min="0" type="number">'+
                          '</div>'+
                        '</div>'+
                    '</td>'+
                    '<td style="padding:5px;">'+
                        '<div class="form-group">'+
                          '<label for="zone-gd-diam" class="col-sm-12 control-label">Volume<span class="mesure-unite">(m³)</span></label>'+
                          '<div class="col-sm-12">'+
                              '<input style="width:100px;" class="form-control pull-right zone-mesure zone-volume"  required="" id="zone-volume-'+nbrDetail+'" value="'+detailRow.volume+'" min="0" type="number">'+
                          '</div>'+
                        '</div>'+
                    '</td>'+
                    '<td style="padding:5px; width:50%">'+
                        '<div class="form-group">'+
                          '<label for="zone-gd-diam" style="text-align:center" class="col-sm-12 control-label">Essence</label>'+
                          '<div class="col-sm-12">'+
                              '<select class="form-control select2 zone-essence" required=""  style="width:100%" id="zone-essence-'+nbrDetail+'"></select>'+
                          '</div>'+
                        '</div>'+
                    '</td>';

        }
        
    
        $('#btn-add-detail').click(function(){
            var ligne = document.createElement("tr");
            ligne.id = "detail-"+nbrDetail;
            ligne.innerHTML = getRowDetailCommande({longueur:'', largeur: '', epaisseur: '', volume: '', essence: null});
                    
            document.getElementById("panel-detail").appendChild(ligne);
            var select = document.getElementById('zone-essence-'+nbrDetail);
                var selected=-1;
                for(var i=0; i<essence.length; i++){
                    var opt = document.createElement("option");
                    opt.value = essence[i].ref;
                    opt.innerHTML = essence[i].libelle;
                    select.appendChild(opt);
                }
                
//            $('.zone-mesure').change(function(){getVolume($(this).attr("id").split('-')[2]);});
            $('#zone-essence-'+nbrDetail).select2();
            nbrDetail++;
            //for(var i=0; i<nbrDetail; i++){
            //}
        });
    function getVolume(id){
        var rayon = parseFloat($('#zone-longueur-'+id).val())*parseFloat($('#zone-largeur-'+id).val())*parseFloat($('#zone-epaisseur-'+id).val());
            $('#zone-volume-'+id).val(rayon);
        }
        
        function removeRow(ligne){
            $('#'+ligne).remove();
        }
        function save(e){

            var data = [];
            var i = 0;
            
            $('.zone-longueur').each(function(){
                i = parseInt($(this).attr('id').split('-')[2]);
                
                var volume = $('#zone-volume-'+i).val();
                volume = (volume+'').substring(0,(volume.indexOf(',')+3));
                console.log(volume+" #######");
               data.push({
                   longueur: $('#zone-longueur-'+i).val(),
                   largeur: $('#zone-largeur-'+i).val(),
                   epaisseur: $('#zone-epaisseur-'+i).val(),
                   volume: volume,
                   essence: $('#zone-essence-'+i).val()
               });
               console.log(data);
            });
            editData($('#zone-ref').val(), $('#zone-client').val(), $('#zone-type-coupe').val(), data);
            $('#form-add-commande .overlay').removeClass('hide');
            $('#form-add-commande #btn-save-commande').attr("disabled","");
            return false;
        }

    </script>
</div>
