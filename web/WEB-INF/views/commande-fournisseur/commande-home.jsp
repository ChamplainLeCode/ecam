<!-- 
    Document   : totoPage
    Created on : 8 déc. 2018, 14:42:16
    Author     : champlain
-->


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" charset="UTF-8">
      <title>Ecam Web</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <%@include file="/WEB-INF/views/layouts/style.jsp" %>
      <link rel="stylesheet" href="/resources/css/dataTables.bootstrap.min.css">  
      <style>.measure-unit{font-size: 10px;}</style>
      <script type="text/javascript">

        
          function saveEdit(e){return save(e);}
      </script>
    </head>
    <!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
    -->
    <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <%@include file="/WEB-INF/views/layouts/topBar.jsp" %>

      <!-- Left side column. contains the logo and sidebar -->
      <%@include file="/WEB-INF/views/layouts/sideBar.jsp" %>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Commandes
            <small>Liste des commandes de </small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="">ecam</li>
            <li class="active">commandes</li>
            <li class="active">client</li>
          </ol>
          
          
        </section>

        <!-- Main content -->
        <section class="content container-fluid">

            <div class="row">
                
                <div class="modal fade"  id="modal-default" style="display: none;">
                    <div class="modal-dialog pull-right col-sm-pull-0" style="width: 100%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close btn-flat" style="background-color: transparent;" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true" style="color: white">×</span></button>
                                <h4 class="modal-title">Ajouter une nouvelle Commande </h4>
                                <br><h4 class="pull-left">N° Titre: <span style="color: red" id="zone-numero-titre"></span></h4>
                                <br><span class="pull-right">Valide jusqu'au: <span style="color: red" id="zone-data-titre"></span></span>
                            </div>
                            <div class="modal-body" id="modal-body">

                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                
                <div class="modal fade modal-default" id="modal-edit" style="display: none;">
                    <div class="modal-dialog pull-right col-sm-pull-0" style="width:100%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close btn-flat" style="background-color: transparent;" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true" style="color: white">×</span></button>
                                <h4 class="modal-title">Modifier la commande</h4>
                            </div>
                            <div class="modal-body" id="modal-body" style="">
                                <style>.mesure-unite{font-size: 11px; margin-left: 3px; color: gray;}</style>

                                <div id="form-add-commande">
                                    <p class="status-add-commande" style="padding: 5px; border-radius: 5px;"></p>
                                    <form class="form-horizontal " onsubmit="return (function(){return saveEdit(this);})();">
                                      <div class="box-body">
                                            <div class="row">

                                                <div class="form-group col-sm-4  col-xs-12 pull-left">
                                                  <label for="ezone-ref" class="col-sm-4 col-xs-12  control-label">N° Commande</label>
                                                  <div class="col-sm-8  col-xs-12 pull-right">
                                                      <input pattern="[0-9]+" readonly="" required="" type="text" title="Veillez fournir le numéro de la commande Uniquement des chiffres" id="ezone-ref" class="form-control">
                                                  </div>
                                                </div>

                                                <div class="form-group col-sm-4 col-xs-12 pull-left">
                                                  <label for="ezone-fournisseur" class="col-sm-4  col-xs-12 control-label">Fournisseur</label>
                                                  <div class="col-sm-8  col-xs-12 pull-right">
                                                      <input readonly="" type="text" required="" class="form-control" id="ezone-fournisseur" ></select>
                                                  </div>
                                                </div>

                                                <div class="form-group col-sm-4  col-xs-12  pull-left">
                                                  <label for="ezone-date" class="col-sm-4  col-xs-12 control-label">Date</label>
                                                  <div class="col-sm-8  col-xs-12 pull-right">
                                                      <input required="" type="date" id="ezone-date" class="form-control date">
                                                  </div>
                                                </div>

                                            </div><br>
                                        <table id="table-addCommande" class="table table-bordered table-hover dataTable form-group" role="grid" aria-describedby="table-addCommande_info">
                                                <thead>
                                                    <tr role="row">
                                                        <th class="sorting" tabindex="0" aria-controls="table-addCommande" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending">Essence</th>
                                                        <th class="sorting" tabindex="0" aria-controls="table-addCommande" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">N° Bille.........</th>
                                                        <th class="sorting_desc" tabindex="0" aria-controls="table-addCommande" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" aria-sort="descending">Long. <span class="mesure-unite">(cm)</span></th>
                                                        <th class="sorting" tabindex="0" aria-controls="table-addCommande" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Grand Diam <span class="mesure-unite">(cm)</span></th>
                                                        <th class="sorting" tabindex="0" aria-controls="table-addCommande" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Ptt Diam <span class="mesure-unite">(cm)</span></th>
                                                        <th class="sorting" tabindex="0" aria-controls="table-addCommande" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Moy Diam <span class="mesure-unite">(cm)</span></th>
                                                        <th class="sorting" tabindex="0" aria-controls="table-addCommande" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Cubage <span class="mesure-unite">(m³)</span></th>
                                                        <th class="sorting" tabindex="0" aria-controls="table-addCommande" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Prix <span class="mesure-unite">(FCFA)</span></th>
                                                        <th class="sorting" tabindex="0" aria-controls="table-addCommande" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Obs</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="table-editCommande-body"></tbody>
                                                <tfoot>
                                                  <tr>
                                                    <td colspan="10">
                                                        <button type="button" id="btn-add-detail-edit" class="pull-right btn btn-primary" style="width:40px; height: 40px; border-radius: 20px;"><i class="glyphicon glyphicon-plus"></i></button>
                                                    </td>
                                                  </tr>
                                                </tfoot>
                                            </table>
                                      </div>
                                      <!-- /.box-body -->
                                      <div class="box-footer" style="background-color: transparent">
                                          <button type="button" id="btn-close-modal"  data-dismiss="modal" class="btn btn-default">Fermer</button>
                                            <button type="submit" class="btn btn-info pull-right" id="btn-edit-commande">Enregistrer</button>
                                            <div class="overlay hide" style="text-align:center;"> 
                                                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
                                            </div>
                                      </div>
                                      <!-- /.box-footer -->
                                    </form>
                                </div>
                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

                <div class="modal fade " id="modal-view" style="display: none; background-color: transparent; background: none;">
                    <div class="modal-dialog pull-right col-sm-pull-0" style="width: 100%">
                        <div class="modal-content col-sm-12 col-md-12 col-lg-12 col-xs-12 col-sm-offset-0 col-md-offset-0 col-lg-offset-0" style=" background-color: transparent; background: none;">
                            <div class="modal-header">
                                
                                    <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-view-commande_client" class="hide" data-toggle="modal" data-target="#modal-view"></button>
                            </div>
                            <div class="modal-body" id="modal-body" style=" background-color: transparent ">
                                <div class="box box-widget widget-user-2">
                                        <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-navy">
                                        <!-- /.widget-user-image -->
                                        <h4 class="widget-user-desc">N° Commande <span id="vzone-ref" class="pull-right" ></span></h4>
                                        <h5 class="widget-user-desc" >Fournisseur <span id="vzone-fournisseur" class="pull-right" ></span></h5>
                                        <h5 class="widget-user-desc">date: <span  id="vzone-date" class="pull-right" ></span></h5>
                                    </div>
                                    <div class="box-footer no-padding" style="margin-top: 25px">
                                        <ul class="nav nav-stacked" style="">
                                            <li><div class="box box-success">
                                                    <div class="box-header with-border">
                                                      <h3 class="box-title">Info. Bille</h3>

                                                      <div class="box-tools pull-right">
                                                        <button type="button" id="btn-print-detail" class="btn btn-bitbucket" style="margin: 5px;" > Imprimer </button>
                                                          <button type="button" class="btn btn-flat" >
                                                              <i class="fa fa-caret-down"></i>
                                                              <span style="margin-left: 10px;" class="pull-right badge bg-green" id="vzone-nbr-detail"></span>
                                                        </button>
                                                      </div>
                                                      <!-- /.box-tools -->
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div id="vzone-detail" style="padding: 30px;" class="box-body">
                                                        <table id="table-print-detail" class=" table table-bordered table-hover dataTable form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <thead>
                                                                <tr>
                                                                    <th class="sorting">Essence</th>
                                                                    <th class="sorting">N° Bille</th>
                                                                    <!-- Old -->
                                                                    <!--th class="sorting">Certif</th-->
                                                                    <th class="sorting">Longueur</th>
                                                                    <th class="sorting">Grand Diam</th>
                                                                    <th class="sorting">Ptt Diam</th>
                                                                    <th class="sorting">Moy Diam</th>
                                                                    <th class="sorting">Cubage</th>
                                                                    <th class="sorting">Prix(FCFA)</th>
                                                                    <th class="sorting">Obs</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="table-view-detail-commande"></tbody>
                                                        </table>
                                                    </div>
                                                    <!-- /.box-body -->
                                                </div>
                                            </li>
                                            <li><button type="button" class=" btn btn-flat pull-right" style="background-color: transparent; margin: 5px;" data-dismiss="modal"> Fermer</button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

                <p class="status-del-commande pull-left" style="padding: 5px; border-radius: 5px;"></p>

                <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-addCommande" class="pull-right btn btn-default" data-toggle="modal" data-target="#modal-default">
                    <i class="fa fa-plus "></i> Add Commande 
                </button>
                
                <button style="margin-right: 40px; margin-bottom: 20px; background-color: transparent;" type="button" onclick="history.back()" class="pull-left btn btn-flat" data-toggle="modal" data-target="#modal-default">
                    <i class="fa fa-arrow-left "></i> Retour
                </button>
                <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-imprimer" class="pull-left btn btn-primary">
                    <i class="fa fa-print "></i> Imprimer la liste
                </button>
            </div>
                
            <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Liste des commandes</h3> 
                  <button type="button" onclick="window.print()">Imprimer</button>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="liste_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="liste" class="table table-responsive table-bordered table-striped dataTable" role="grid" aria-describedby="liste_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 79.75px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Reférence</th>
                                            <th class="sorting" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 73.3333px;" aria-label="Browser: activate to sort column ascending">Date</th>
                                            <th class="sorting" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 86.5px;" aria-label="Platform(s): activate to sort column ascending">Fournisseur</th>
                                            <th class="sorting" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 86.5px;" aria-label="Platform(s): activate to sort column ascending">Nbr Billes</th>
                                            <th class="sorting" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 86.5px;" aria-label="Platform(s): activate to sort column ascending">Opérations</th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-body"></tbody>
                              </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <%@include file="/WEB-INF/views/layouts/footer.jsp" %>

    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->
    <%@include  file="/WEB-INF/views/layouts/script.jsp" %>
    <script src="/resources/js/commande/commande-home.js"></script>
    <script src="/resources/js/jquery.dataTables.min.js"></script>
    <script src="/resources/js/dataTables.bootstrap.min.js"></script>

    <script>
        $(function () {
            window.$ = $;
            loadData();
        });
        function save(){
            var data = [];
            var i = 0;
            $('.zone-mesure').each(function(){
                i = parseInt($(this).attr('id').split('-')[2]);
                
                var volume = $('#ezone-cubage-'+i).val();
                volume = volume.substring(0,(volume.indexOf('.')+3));
               data.push({
                   essence: $('#ezone-essence-'+i).val(),
                   bille: $('#ezone-bille-'+i).val(),
                   longueur: $('#ezone-longueur-'+i).val(),
                   gdDiam: $('#ezone-gd-diam-'+i).val(),
                   pttDiam: $('#ezone-ptt-diam-'+i).val(),
                   moyDiam: $('#ezone-moy-diam-'+i).val(),
                   volume: volume,
                   observation: $('#ezone-observation-'+i).val()
               });
           });

            $.ajax({
                url: '/commande/edit',
                dataType: 'json',
                method: 'POST',
                data: {
                    ref: $('#ezone-ref').val(),
                    detail: data.length === 0 ? [] : data,
                    taille: data.length
                }, 
                complete: function (jqXHR, textStatus) {
                    $('#btn-close-modal').click();
                    console.log("fermé");
                }
            });
            return false;
        }
        
        document.getElementById("btn-imprimer").onclick = function(){
            tableToExcel('liste', 'Liste des commandes');
        };
        
        document.getElementById("btn-print-detail").onclick = function(){
            tableToExcel('table-print-detail', 'Détail de commande');
        };
           

    </script>
    </body>
</html>
