<%-- 
    Document   : massicot-edit
    Created on : 13 janv. 2019, 14:26:13
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-massicot">
    <p class="status-edit-massicot" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REFERENCE</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<% out.print(request.getParameter("ref"));%>" required="" class="form-control" id="zone-ref" placeholder="Reference">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Libelle</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("libelle"));%>" class="form-control" id="zone-libelle" placeholder="libelle du massicot">
          </div>
        </div>
        
        </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" id="btn-close-editPane" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-massicot"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        $("#btn-edit-massicot").click(function(){
            editData($('#zone-ref').val(), $('#zone-libelle').val());
            $('#form-edit-massicot .overlay').removeClass('hide');
            $('#form-edit-massicot #btn-edit-massicot').addClass("disabled");
        });

    </script>
</div>
