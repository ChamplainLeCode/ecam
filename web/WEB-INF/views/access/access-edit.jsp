<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-personnel">
    <div class="register-box-body">
          <h2 class="login-box-msg">Modifier un compte <span class="label" style="display: none;" id="status"></span></h2>

        <form  method="post" post="/personnel/edit" onsubmit="return send(this);">
          <div class="form-group has-feedback">
              <legend><h4>Info Personnelles.</h4></legend>
              <input type="text" id="zone-nom" required="" value="<% out.print(request.getParameter("nom"));%>" readonly=""  class="form-control" placeholder="Nom ">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
              <input type="text" id="zone-path" required="" value="<%=request.getParameter("path")%>"  class="form-control" placeholder="Prénom">
            <span class="glyphicon  form-control-feedback"  style="margin-right: 10px;"></span>
          </div>
          <div class="row">
              <!-- /.col -->
            <div class="col-xs-4 pull-left">
                <button type="btn" data-dismiss="modal"  id="btn-close-editPane" class="btn form-control btn-block btn-flat">Fermer</button>
            </div>
            <div class="pull-right col-xs-4">
                <button type="submit" class="btn btn-primary form-control btn-block btn-flat">Enregistrer</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
      </div>

          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        window.send = function(self){
            
            $.ajax({
                url: '/acces/edit',
                method: 'POST',
                data: {nom: $('#zone-nom').val(), path: $('#zone-path').val()},
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    $('#btn-close-editPane').click();
                    loadData();
                }
            });
            return false;
        }

    </script>
</div>
