<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-acces">
    <p class="status-edit-acces" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Privilege</label>

          <div class="col-sm-10">
              <select required="" style="width: 100%" id="zone-privilege"></select>
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Routes</label>

          <div class="col-sm-10">
              <select required="" multiple="" style="width: 100%" id="zone-route"></select>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-add-acces"><i class="fa fa-pencil"></i> Ajouter</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        
        $.ajax({
            url: '/api/privilege/list',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var select = document.getElementById('zone-privilege');
                var option;
                var data = jqXHR.responseJSON;
                for(var i=0; i<data.length; i++){
                    option = document.createElement("option");
                    option.value = data[i].ref;
                    option.innerText = data[i].libelle;
                    select.appendChild(option);
                }
            }
        });
        
        $.ajax({
            url: '/api/route/list',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var select = document.getElementById('zone-route');
                var option;
                var data = jqXHR.responseJSON;
                for(var i=0; i<data.length; i++){
                    option = document.createElement("option");
                    option.value = data[i].nom;
                    option.innerText = data[i].nom;
                    select.appendChild(option);
                }
            }
        });
        
        $("#btn-add-acces").click(function(){
            saveData($('#zone-privilege').val(), $('#zone-route').val());
            $('#form-edit-acces .overlay').removeClass('hide');
            $('#form-edit-acces #btn-add-acces').addClass("hide");
        });
        $('select').select2();
    </script>
</div>
