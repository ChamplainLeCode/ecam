<%-- 
    Document   : totoPage
    Created on : 8 déc. 2018, 14:42:16
    Author     : champlain
--%>

<%@page import="java.util.List"%>
<%@page import="app.models.Fournisseur"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ecam fournisseur list</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        
            <%@include file="/WEB-INF/views/layouts/style.jsp" %>
    
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <%@include file="/WEB-INF/views/layouts/topBar.jsp" %>
      <!-- Left side column. contains the logo and sidebar -->
      <%@include file="/WEB-INF/views/layouts/sideBar.jsp" %>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Fournisseurs
            <small>Liste des fournisseurs</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="">ecam</li>
            <li class="active">fournisseur</li>
          </ol>
          
          
        </section>

        <!-- Main content -->
        <section class="content container-fluid">

            <div class="row">
                <div class="modal fade modal-default" id="modal-default" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close btn-flat" style="background-color: transparent;" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true" style="color: white">×</span></button>
                                <h4 class="modal-title">Ajouter un nouveau Fournisseur</h4>
                            </div>
                            <div class="modal-body" id="modal-body">

                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

                <div class="modal fade modal-default" id="modal-see" style="display: none;">
                    <div class="modal-body" id="modal-body"></div>
                </div>

                <div class="modal fade modal-default" id="modal-edit" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close btn-flat" style="background-color: transparent;" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true" style="color: white">×</span></button>
                                <h4 class="modal-title">Modifier fournisseur</h4>
                            </div>
                            <div class="modal-body" id="modal-body">

                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <p class="status-del-fournisseur pull-left" style="padding: 5px; border-radius: 5px;"></p>

                <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-addFournisseur" class="pull-right btn btn-default" data-toggle="modal" data-target="#modal-default">
                    <i class="fa fa-plus "></i> Add Fournisseur
                </button>
                
                <button style="margin-right: 40px; margin-bottom: 20px; background-color: transparent;" type="button" onclick="history.back()" class="pull-left btn btn-flat" data-toggle="modal" data-target="#modal-default">
                    <i class="fa fa-arrow-left "></i> Retour
                </button>
                <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-imprimer" class="pull-left btn btn-primary">
                    <i class="fa fa-print "></i> Imprimer la liste
                </button>
            </div>
            <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Liste des fournisseurs</h3>
                    
                    <p class="pull-right" style="margin-left: 20px;">
                        <button class="btn btn-primary" style="height: 40px;" type="button" id="btn-filtre">
                            <i class="fa fa-search"></i> Rechercher
                        </button>
                    </p>

                    <p class="pull-right" style="margin-left: 20px;"><span>Origines </span>
                        <select class="select2" style="height: 40px;" id="zone-categorie-origine">
                            <option value="all">Tous</option>
                        </select>
                    </p>

                    <p class="pull-right" style="margin-left: 20px;"><span>Date Fin</span>
                        <input type="date" id="zone-date-fin-titre" style="height: 40px;">
                    </p>

                    <p class="pull-right" style="margin-left: 20px;"><span>Date Début </span>
                        <input type="date" id="zone-date-debut-titre" style="height: 40px;">
                    </p>
                </div>            <!-- /.box-header -->
            <div class="box-body">
                <div id="liste_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="liste" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="liste_info">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 79.75px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Reférence</th>
                                        <th class="sorting" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 73.3333px;" aria-label="Browser: activate to sort column ascending">Nom</th>
                                        <th class="sorting" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 86.5px;" aria-label="Platform(s): activate to sort column ascending">Contribuable</th>
                                        <th class="sorting" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 86.5px;" aria-label="Platform(s): activate to sort column ascending">Téléphone</th>
                                        <th class="sorting" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 86.5px;" aria-label="Platform(s): activate to sort column ascending">Titres</th>
                                        <th class="sorting" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 86.5px;" aria-label="Platform(s): activate to sort column ascending">Parcs</th>
                                        <th class="sorting" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 86.5px;" aria-label="Platform(s): activate to sort column ascending">Opérations</th>
                                    </tr>
                                </thead>
                                <tbody id="table-body"></tbody>
                          </table>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <!-- Main Footer -->
      <%@include file="/WEB-INF/views/layouts/footer.jsp" %>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <%@include file="/WEB-INF/views/layouts/script.jsp" %>    

    <script src="/resources/js/jquery.dataTables.min.js"></script>
    
    <script src="/resources/js/dataTables.bootstrap.min.js"></script>

    <script src="/resources/js/fournisseur/add.js"></script>

    <script src="/resources/js/fournisseur/list.js"></script>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. -->
    
    <script type="text/javascript">

        $(function(){
            function parseDate(dateStringify = (new Date().getYear()+1990)+"-"+new Date().getMonth()+"-"+new Date().getDate()){
                if(dateStringify == '')
                    return new Date(0);
                var date = dateStringify.split('-');
                return new Date(parseInt(date[0]), parseInt(date[1]), parseInt(date[2]));
            }

            loadData();
            document.getElementById("btn-imprimer").onclick = function(){
                tableToExcel('liste', 'W3C Example Table');
            };

            $('#btn-filtre').click(function(){
                
               loadData({
                   origine: $('#zone-categorie-origine').val(),
                   dateDebut: parseDate($('#zone-date-debut-titre').val()).getTime(),
                   dateFin:   parseDate($('#zone-date-fin-titre').val()).getTime()
                }); 
            });
            
            $.ajax({
                url: '/api/type_parc/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    var select = document.getElementById("zone-categorie-origine");
                    var data = jqXHR.responseJSON;
                    var option;
                    for(var i=0; i<data.length; i++){
                        option = document.createElement("option");
                        option.value = data[i].ref;
                        option.innerText = data[i].libelle;
                        select.appendChild(option);
                    }
                }
            });
        });
    </script>
    <script src="/resources/js/slimscroll/jquery.slimscroll.min.js"></script>
    </body>
</html>
