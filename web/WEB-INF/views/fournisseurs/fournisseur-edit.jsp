<%@page import="java.util.List"%>
<%@page import="app.models.Certificat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-fournisseur">
    <p class="status-edit-fournisseur" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
          <div class="row bg-red hide" style="margin-bottom: 20px; padding: 10px; border-radius: 5px;" id="zone-statut-edit">Fournisseur non modifié</div>
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Reférence</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<%=request.getParameter("refFournisseur")%>" required="" class="form-control" id="zone-ref" placeholder="Reférence">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-nom" class="col-sm-2 control-label">Nom</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<%=request.getParameter("nomFournisseur")%>" class="form-control" id="zone-nom" placeholder="nom du fournisseur">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-contribuable" class="col-sm-2 control-label">Contribuable</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<%=request.getParameter("contribuable")%>" class="form-control" id="zone-contribuable" placeholder="contribuable du fournisseur">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-contribuable" class="col-sm-2 control-label">Téléphone </label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<%=request.getParameter("contact")%>" class="form-control" id="zone-contact" placeholder="téléphone du fournisseur">
          </div>
        </div>
        <fieldset>
          <legend  style="color: #000" class="">Certificats</legend>

          <div class="row" id="panel-certificats">
              <% 
                  List<Certificat> list = Certificat.getCertificatFor(request.getParameter("refFournisseur"));
                  for(int i=0; i< list.size(); i++) {%>
                <div class="input-group">
                    <div class="col-sm-10 col-xs-10 col-md-10 col-lg-10">
                        <input type="text" class="form-control  pull-right zone-certif" placeholder="Certicat" style="margin-bottom: 5px; margin-left: 5px" value="<%=list.get(i).getLibelle()%>">
                    </div>
                        <button class="input-group-addon btn-remove-certif col-sm-2 col-xs-2 col-md-2 col-lg-2" type="button" onclick="deleteCertificat('<%=list.get(i).getIdentifiant()%>', $(this))"><i class="fa fa-remove"></i></button>
                </div>              
                  <%}%>
          </div>
          <button id="btn-add-certificat" type="button" class="btn btn-info" style="border-radius: 15px; width: 40px; height: 40px;"><i class="glyphicon glyphicon-plus"></i></button>
        </fieldset>
        
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" id="btn-close-editPane" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-fournisseur"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        $("#btn-edit-fournisseur").click(function(){
            var certifs = [];
            $('.zone-certif').each(function(){
               certifs.push($(this).val());
            });
            editData($('#zone-ref').val(), $('#zone-nom').val(), $('#zone-contribuable').val(), $('#zone-contact').val(), certifs);
            $('#form-edit-fournisseur .overlay').removeClass('hide');
            $('#form-edit-fournisseur #btn-edit-fournisseur').addClass("disabled");
        }); 
        $("#btn-add-parcChargemnet").click(function(){
            var zoneParc = document.getElementById("panel-parcs");
            var input = document.createElement("input");
            input.type = "text";
            input.className = "form-control zone-parc";
            input.placeholder = "Parc Chargement";
            input.style.marginLeft = input.style.marginBottom = '5px';
            input.required = "required";
            zoneParc.appendChild(input);
        });
        $("#btn-add-certificat").click(function(){
            var zoneParc = document.getElementById("panel-certificats");
            var input = document.createElement("input");
            input.type = "text";
            input.className = "form-control zone-certif";
            input.placeholder = "Certificat";
            input.style.marginLeft = input.style.marginBottom = '5px';
            input.required = "required";
            zoneParc.appendChild(input);
        });
        function deleteParc(id, source){
            $.ajax({
                url: '/parc_chargement/delete',
                method: 'POST',
                data: {ref: id},
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    if(data.resultat === "true"){
                        source.parent().hide(500);
                    }
                }
            })
        }
        function deleteCertificat(id, source){
            $.ajax({
                url: '/certificat/delete',
                method: 'POST',
                data: {ref: id},
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    if(data.resultat === "true"){
                        source.parent().hide(500);
                    }
                }
            })
        }
    </script>
</div>
