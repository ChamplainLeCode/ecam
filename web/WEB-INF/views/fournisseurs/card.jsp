<%@page import="app.models.Titre"%>
<%@page import="java.util.List"%>
<%@page import="app.models.Certificat"%>
<%@page import="app.models.ParcChargement"%>
<%@page import="org.json.JSONObject"%>
<%@page import="app.models.Fournisseur"%>

<%
    Fournisseur fournisseur = (Fournisseur)request.getAttribute("fournisseur");

    if(fournisseur != null ){
%>
        <br><br><div class="col-sm-5"></div>
        <div class="col-sm-4">
        <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-navy">
                <div class="widget-user-image">
                    <img class="img-circle pull-right"  style="position:relative; top:65px;" src="/resources/image/avatar.png" alt="User Avatar">
                </div>
                <!-- /.widget-user-image -->
                <h3 class="widget-user-username"><%=request.getParameter("nomFournisseur")%></h3>
                <h5 class="widget-user-desc"><%=request.getParameter("contribuable")%></h5>
                <h5 class="widget-user-desc">tel: <%=request.getParameter("contact")%></h5>
            </div>
            <div class="box-footer no-padding" style="margin-top: 25px">
                <ul class="nav nav-stacked" style="">
                    <li><a href="#">Commandes <span class="pull-right badge bg-blue"><%=fournisseur.getNombreTotalCommande()%></span></a></li>
                    <li><a href="#">Receptions <span class="pull-right badge bg-aqua"><%=fournisseur.getNombreTotalReception()%></span></a></li>
                    <li><div class="box box-success collapsed-box">
                            <div class="box-header with-border">
                              <h3 class="box-title">Parc Chargement</h3>

                              <div class="box-tools pull-right">
                                  <button type="button" class="btn btn-box-tool" onclick="if(seeParc === true){$('#zone-parc').show();}else{$('#zone-parc').hide();} seeParc = !seeParc;" data-widget="collapse">
                                      <i class="fa fa-caret-down"></i>
                                      <span style="margin-left: 10px;" class="pull-right badge bg-green"><%=fournisseur.getParcChargementList().size()%></span>
                                </button>
                              </div>
                              <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div id="zone-parc" class="box-body">
                                <% for(ParcChargement p : fournisseur.getParcChargementList()){%>
                                <label id="parc-<%=p.getRefParc()%>" style="height: 30px; font-size: 15px;" class="label label-primary"><%=p.getLibelle()%> <button class="img-circle image btn-danger" onclick="deleteParc('<%=p.getRefParc()%>', $(this))"><i style="margin-left: 5px;" class=" glyphicon glyphicon-remove"></i></button></label>
                                <% } %>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </li>
                    <li><div class="box box-success collapsed-box">
                            <div class="box-header with-border">
                              <h3 class="box-title">Titres</h3>

                              <div class="box-tools pull-right">
                                  <button type="button" class="btn btn-box-tool" onclick="if(seeCertif === true){$('#zone-certifs').show();}else{$('#zone-cetifs').hide();} seeCertif ^= true;" data-widget="collapse">
                                      <i class="fa fa-caret-down"></i>
                                      <span style="margin-left: 10px;" class="pull-right badge bg-green"><% List<Titre> titres = Titre.getTitreFor(fournisseur.getRefFournisseur()); out.print(titres.size());%></span>
                                </button>
                              </div>
                              <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div id="zone-certifs" class="box-body">
                                <% for(Titre c : titres){%>
                                <label id="certificat-<%=c.getNumTitre()%>" style="height: 30px; font-size: 15px;" class="label label-primary"><%=c.getNumTitre()%> <button class="img-circle image btn-danger" onclick="deleteTitre('<%=c.getNumTitre()%>', $(this))"><i style="margin-left: 5px;" class=" glyphicon glyphicon-remove"></i></button></label>
                                <% } %>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<% }%>

<script src="/resources/js/slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript">
    
    var seeParc=true;
    var seeCertif=true;
    
    function deleteParc(id, source){
        $.ajax({
            url: '/parc_chargement/delete',
            method: 'POST',
            data: {ref: id},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    source.parent().hide(500);
                }
            }
        })
    }
    function deleteTitre(id, source){
        $.ajax({
            url: '/titre/delete',
            method: 'POST',
            data: {ref: id},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    source.parent().hide(500);
                }
            }
        })
    }
</script>