<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-add-fournisseur">
    <p class="status-add-fournisseur" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
          <div class="row bg-red hide" style="margin-bottom: 20px; padding: 10px; border-radius: 5px;" id="zone-statut-add">Fournisseur non ajouté</div>
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Reférence</label>

          <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="zone-ref" placeholder="Numéro de reférence">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Nom</label>

          <div class="col-sm-10">
              <input type="text"  required="" class="form-control" id="zone-nom" placeholder="nom du fournisseur">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Contribuable</label>

          <div class="col-sm-10">
              <input type="text"  required="" class="form-control" id="zone-contribuable" placeholder="contribuable du fournisseur">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Contact</label>

          <div class="col-sm-10">
              <input type="text"  required="" class="form-control" id="zone-contact" placeholder="contact du fournisseur">
          </div>
        </div>
        <!--fieldset>
          <legend  style="color: #000" class="">Parc de Chargement</legend>

          <div class="row" id="panel-parcs">
              
          </div>
          <button id="btn-add-parcChargemnet" type="button" class="btn btn-info" style="border-radius: 15px; width: 40px; height: 40px;"><i class="glyphicon glyphicon-plus"></i></button>
        </fieldset>
        <fieldset>
          <legend  style="color: #000" class="">Certificats</legend>

          <div class="row" id="panel-certificats">
              
          </div>
          <button id="btn-add-certificat" type="button" class="btn btn-info" style="border-radius: 15px; width: 40px; height: 40px;"><i class="glyphicon glyphicon-plus"></i></button>
        </fieldset-->
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-add-fournisseur"><i class="fa fa-pencil"></i> Ajouter</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        $("#btn-add-fournisseur").click(function(){
            var parcs = [];
            $('.zone-parc').each(function(){
               parcs.push($(this).val());
            });
            var certifs = [];
            $('.zone-certificat').each(function(){
               certifs.push($(this).val());
            });
            saveData($('#zone-ref').val(), $('#zone-nom').val(), $('#zone-contribuable').val(), $('#zone-contact').val(), parcs, certifs);
            $('#form-add-fournisseur .overlay').removeClass('hide');
            $('#form-add-fournisseur #btn-add-fournisseur').addClass("disabled");
        });
        $("#btn-add-parcChargemnet").click(function(){
            var zoneParc = document.getElementById("panel-parcs");
            var input = document.createElement("input");
            input.type = "text";
            input.className = "form-control zone-parc";
            input.placeholder = "Parc Chargement";
            input.style.marginLeft = input.style.marginBottom = '5px';
            input.required = "required";
            zoneParc.appendChild(input);
        });
        $("#btn-add-certificat").click(function(){
            var zoneParc = document.getElementById("panel-certificats");
            var input = document.createElement("input");
            input.type = "text";
            input.className = "form-control zone-certificat";
            input.placeholder = "Certificat";
            input.style.marginLeft = input.style.marginBottom = '5px';
            input.required = "required";
            zoneParc.appendChild(input);
        });
    </script>
</div>
