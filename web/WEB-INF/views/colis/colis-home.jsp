<%-- 
    Document   : colis-home
    Created on : 6 févr. 2019, 10:39:42
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Ecam Web</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

      <%@include file="/WEB-INF/views/layouts/style.jsp" %>
      <link rel="stylesheet" href="/resources/css/dataTables.bootstrap.min.css">    
      <link rel="stylesheet" type="text/css" href="/resources/css/tooltip-tools/tooltipster.bundle.min.css" />
      
    </head>
    <!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
    -->
    <body class="hold-transition skin-blue sidebar-collapse">
    <div class="wrapper">

      <!-- Main Header -->
      <%@include file="/WEB-INF/views/layouts/topBar.jsp" %>

      <!-- Left side column. contains the logo and sidebar -->
      <%@include file="/WEB-INF/views/layouts/sideBar.jsp" %>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Page Header
            <small>Optional description</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="">ecam</li>
            <li class="active">colis</li>
          </ol>
          
          
        </section>

        <!-- Main content -->
        <section class="content container-fluid">

            <div class="row">
                <div class="modal fade modal-primary" id="modal-default" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close btn-flat" style="background-color: transparent;" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true" style="color: white">×</span></button>
                                <h4 class="modal-title">Ajouter un nouveau Colis</h4>
                            </div>
                            <div class="modal-body" id="modal-body">

                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <div class="modal fade modal-primary" id="modal-edit" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close btn-flat" style="background-color: transparent;" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true" style="color: white">×</span></button>
                                <h4 class="modal-title">Modifier colis</h4>
                            </div>
                            <div class="modal-body" id="modal-body">

                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <p class="status-del-colis pull-left" style="padding: 5px; border-radius: 5px;"></p>

                <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-addColis" class="hide pull-right btn btn-default" data-toggle="modal" data-target="#modal-default">
                    <i class="fa fa-plus "></i> Add Colis
                </button>
                
                <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-imprimer" class="pull-left btn btn-primary">
                    <i class="fa fa-print "></i> Imprimer la liste
                </button>
            </div>
            <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Liste du colis</h3>
                    
                    <p class="pull-right" style="margin-left: 20px;">
                        <button class="btn btn-primary" style="height: 40px;" type="button" id="btn-filtre">
                            <i class="fa fa-search"></i> Rechercher
                        </button>
                    </p>

                    <p class="pull-right" style="margin-left: 20px;"><span>Colis </span>
                        <select class="select2" style="height: 40px;" id="zone-categorie-colis">
                            <option value="stock">En Stock</option>
                            <option value="expedie">Expédiés</option>
                            <option value="jointage">Jointage</option>
                            <option value="ouvert">Ouverts</option>
                        </select>
                    </p>

                    <p class="pull-right" style="margin-left: 20px;"><span>Date Fin</span>
                        <input type="date" id="zone-date-fin-colis" style="height: 40px;">
                    </p>

                    <p class="pull-right" style="margin-left: 20px;"><span>Date Début </span>
                        <input type="date" id="zone-date-debut-colis" style="height: 40px;">
                    </p>
                    <p class="pull-right" style="margin-left: 20px;">
                        <button class="btn btn-yahoo" style="height: 40px;" type="button" onclick="window.open('/massicotage/fiche');">
                            <i class="fa fa-print"></i> Fiche Production Massicot
                        </button>
                    </p>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="liste_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="liste" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="liste_info">
                                    <thead>
                                        <tr role="row">
                                            <th>Reférence</th>
                                            <th>Certification</th>
                                            <th>NbPaquets</th>
                                            <th>LongueurColis</th>
                                            <th>LargeurColis</th>
                                            <th>PoidsColis</th>
                                            <th>Essence</th>
                                            <th>Qualite</th>
                                            <th>Surface [m²]</th>
                                            <th>Volume [m³]</th>
                                            <th>N° Départure</th>
                                            <th>Opérations</th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-body"></tbody>
                              </table>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </section>
        
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <%@include file="/WEB-INF/views/layouts/footer.jsp" %>

    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->

    <%@include file="/WEB-INF/views/layouts/script.jsp" %>
    
    <script src="/resources/js/jquery.dataTables.min.js"></script>
    <script src="/resources/js/dataTables.bootstrap.min.js"></script>
    <script src="/resources/js/codeBarre/JsBarcode.all.min.js"></script>
    <script src="/resources/js/tooltip-tools/tooltipster.bundle.min.js"></script>
    <script src="/resources/js/colis/colis-home.js"></script>
    <script src="/resources/js/jquery.dataTables.min.js"></script>
    <script src="/resources/js/dataTables.bootstrap.min.js"></script>


    <script>
        $(function () {
            function parseDate(dateStringify = (new Date().getYear()+1990)+"-"+new Date().getMonth()+"-"+new Date().getDate()){
                if(dateStringify === "")
                    return new Date(0);
                var date = dateStringify.split('-');
                return new Date(parseInt(date[0]), parseInt(date[1]), parseInt(date[2]));
            }


            
            loadData();
            
            $('#btn-filtre').click(function(){
                
               loadData({
                   categorie: $('#zone-categorie-colis').val(),
                   dateDebut: parseDate($('#zone-date-debut-colis').val()).getTime(),
                   dateFin:   parseDate($('#zone-date-fin-colis').val()).getTime()
                }); 
            });
            
            $.ajax({
                url: '/api/type_parc/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    var select = document.getElementById("zone-categorie-colis");
                    var data = jqXHR.responseJSON;
                    var option;
                    for(var i=0; i<data.length; i++){
                        option = document.createElement("option");
                        option.value = data[i].ref;
                        option.innerText = data[i].libelle;
                        select.appendChild(option);
                    }
                }
            });
            
            //JsBarcode("#barcode", "Hi world!",{dislayValue: false});
            document.getElementById("btn-imprimer").onclick = function(){
                tableToExcel('liste', 'W3C Example Table');
            };
                      
        });
    </script>
    </body>
</html>
