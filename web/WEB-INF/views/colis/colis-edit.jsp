<%-- 
    Document   : colis-edit
    Created on : 6 févr. 2019, 10:39:31
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-colis">
    <p class="status-edit-colis" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Reference</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<% out.print(request.getParameter("refColis"));%>" required="" class="form-control" id="zone-ref" placeholder="Reference du Colis">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">DateDebut</label>

          <div class="col-sm-10">
              <input type="date"  required="" value="<% out.print(request.getParameter("dateDebut"));%>" class="form-control" id="zone-dateDebut" placeholder="DateDebut du Colis">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">DateFermeture</label>

          <div class="col-sm-10">
              <input type="date"  required="" value="<% out.print(request.getParameter("dateFermeture"));%>" class="form-control" id="zone-dateFermeture" placeholder="DateFermeture du Colis">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">DateFin</label>

          <div class="col-sm-10">
              <input type="date"  required="" value="<% out.print(request.getParameter("dateFin"));%>" class="form-control" id="zone-dateFin" placeholder="Date Fin du Colis">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">PoidsEmballage</label>

          <div class="col-sm-10">
              <input type="number"  required="" value="<% out.print(request.getParameter("poidsEmballage"));%>" class="form-control" id="zone-poidsEmballage" placeholder="PoidsEmballage du Colis">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">PoidsNet</label>

          <div class="col-sm-10">
              <input type="number"  required="" value="<% out.print(request.getParameter("poidsNet"));%>" class="form-control" id="zone-poidsNet" placeholder="PoidsNet du Colis">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">NbrePaquet</label>

          <div class="col-sm-10">
              <input type="number"  required="" value="<% out.print(request.getParameter("nbrePaquet"));%>" class="form-control" id="zone-nbrePaquet" placeholder="NbrePaquet du Colis">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">LongueurColis</label>

          <div class="col-sm-10">
              <input type="number"  required="" value="<% out.print(request.getParameter("longueurColis"));%>" class="form-control" id="zone-longueurColis" placeholder="LongueurColis du Colis">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">LargeurColis</label>

          <div class="col-sm-10">
              <input type="number"  required="" value="<% out.print(request.getParameter("largeurColis"));%>" class="form-control" id="zone-largeurColis" placeholder="Fonction du Colis">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">CodeBarColis</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("codeBarColis"));%>" class="form-control" id="zone-codeBarColis" placeholder="CodeBarColis du Colis">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">PoidsColis</label>

          <div class="col-sm-10">
              <input type="number"  required="" value="<% out.print(request.getParameter("poidsColis"));%>" class="form-control" id="zone-poidsColis" placeholder="PoidsColis du Colis">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">volume</label>

          <div class="col-sm-10">
              <input type="float"  required="" value="<% out.print(request.getParameter("volume"));%>" class="form-control" id="zone-volume" placeholder="Volume du Colis">
          </div>
        </div>
          
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" id="btn-close-editPane" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-colis"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
           $('#zone-date').val(new Date().toUTCString()).attr("data", new Date().getTime());
        $("#btn-edit-colis").click(function(){
            editData($('#zone-ref').val(), $('#zone-dateDebut').val(), $('#zone-dateFermeture').val(), $('#zone-fonction').val());
            $('#form-edit-colis .overlay').removeClass('hide');
            $('#form-edit-colis #btn-edit-colis').addClass("disabled");
        });

    </script>
</div>
