<%-- 
    Document   : colis-add
    Created on : 6 févr. 2019, 10:39:23
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-colis">
    <p class="status-edit-colis" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REFERENCE</label>

          <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="zone-ref" placeholder="Reference Colis">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">DateDebut</label>

          <div class="col-sm-10">
              <input type="date"  required="" class="form-control" id="zone-dateDebut" placeholder="Date Debut Colis">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">DateFermeture</label>

          <div class="col-sm-10">
              <input type="date"  required="" class="form-control" id="zone-dateFermeture" placeholder="Date Fermeture Colis">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">DateFin</label>

          <div class="col-sm-10">
              <input type="date"  required="" class="form-control" id="zone-dateFin" placeholder="Date Fin Colis">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">PoidsEmballage</label>

          <div class="col-sm-10">
              <input type="number"  required="" class="form-control" id="zone-poidsEmballage" placeholder="Poids Emballage">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">PoidsNet</label>

          <div class="col-sm-10">
              <input type="number"  required="" class="form-control" id="zone-poidsNet" placeholder="Poids Net Colis">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">NbrePaquet</label>

          <div class="col-sm-10">
              <input type="number"  required="" class="form-control" id="zone-nbrePaquet" placeholder="Nbre De Paquet Dans le Colis">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">LongueurColis</label>

          <div class="col-sm-10">
              <input type="number"  required="" class="form-control" id="zone-longueurColis" placeholder="Longueur Du Colis">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">LargeurColis</label>

          <div class="col-sm-10">
              <input type="number"  required="" class="form-control" id="zone-largeurColis" placeholder="Largeur Du Colis">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">CodeBarColis</label>

          <div class="col-sm-10">
              <input type="text"  required="" class="form-control" id="zone-codeBarColis" placeholder="Code Barre du colis">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">PoidsColis</label>

          <div class="col-sm-10">
              <input type="number"  required="" class="form-control" id="zone-poidsColis" placeholder="Poids du Colis">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">EquipeRef</label>

          <div class="col-sm-10">
              <input type="number"  required="" class="form-control" id="zone-equipeRef" placeholder="EquipeRef du Colis">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Volume</label>

          <div class="col-sm-10">
              <input type="float"  required="" class="form-control" id="zone-volume" placeholder="Volume du Colis">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-add-colis"><i class="fa fa-pencil"></i> Ajouter</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
           $('#zone-date').val(new Date().toUTCString()).attr("data", new Date().getTime());
        $("#btn-add-colis").click(function(){
            saveData($('#zone-ref').val(), $('#zone-dateDebut').val(), $('#zone-dateFermeture').val(), $('#zone-dateFin').val(), $('#zone-poidsEmballage').val(), $('#zone-poidsNet').val()
                    , $('#zone-nbrePaquet').val(), $('#zone-longueurColis').val(), $('#zone-largeurColis').val(), $('#zone-codeBarColis').val(), $('#zone-poidsColis').val(), $('#zone-equipeRef').val(), $('#zone-volume').val());
            $('#form-edit-colis .overlay').removeClass('hide');
            $('#form-edit-colis #btn-add-colis').addClass("hide");
        });

    </script>
</div>
