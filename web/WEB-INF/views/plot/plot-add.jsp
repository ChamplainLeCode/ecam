<%-- 
    Document   : plot-add
    Created on : 30 janv. 2019, 19:35:53
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-plot">
    <p class="status-edit-plot" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
        <div class="box-body">
            <div class="form-group">
                <label for="zone-epaisseur" class="col-sm-2 control-label">Bille</label>

                <div class="col-sm-10">
                    <select style="width: 100%;" id="zone-bille"></select>
                </div>
            </div>
            <div class="form-group">
                <label for="zone-billon" class="col-sm-2 control-label">Billon</label>

                <div class="col-sm-10">
                    <select style="width: 100%;" id="zone-billon"></select>
                </div>
            </div>

            <div class="form-group">
                <label for="zone-epaisseur" class="col-sm-2 control-label">NombrePlot</label>

                <div class="col-sm-10">
                    <input type="number"  required="" max="5" min="0" class="form-control" id="zone-nombrePlot" placeholder="NombrePlot du Coupe">
                </div>
            </div>

            <div class="container-fluid">
                <table class="table table-responsive table-striped">
                    <thead>
                        <tr><th colspan="5"><b>Details sur les Plots</th></tr>
                        <tr>
                            <th>N° Plot</th>
                            <th>Longueur</th>
                            <th>N° Travail</th>
                        </tr>
                    </thead>
                    <tbody id="table-details-plots"></tbody>
                </table>
            </div>

        </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-add-plot"><i class="fa fa-pencil"></i> Ajouter</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        $("#btn-add-plot").click(function(){

            var data = [];

             for(var i=0; i<parseInt($('#zone-nombrePlot').val()); i++){
                 data[i] = {
                     ref: $('#zone-plot-ref-'+i).val(),
                     face: $('#zone-plot-face-'+i).val(),
                     longueur : $('#zone-plot-longueur-'+i).val()
                 };
             }
                  
            saveData({taille: data.length, data: data, billon: $('#zone-billon').val()});
            $('#form-edit-plot .overlay').removeClass('hide');
            $('#form-edit-plot #btn-add-plot').addClass("hide");
        });
        
        getBille();

        $('#zone-bille').change(function(){getBillonForBille($(this).val()); $('#zone-billon').change();});
        $('#zone-billon').change(function(){$('#zone-nombrePlot').val('0'); setNombrePlots();});
        $('#zone-nombrePlot').change(()=>setNombrePlots());
        $('#zone-ref').val(new Date().getTime());
        $('select').select2();

    </script>
</div>

