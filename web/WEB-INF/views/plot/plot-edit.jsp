<%-- 
    Document   : plot-edit
    Created on : 30 janv. 2019, 19:36:07
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-plot">
    <p class="status-edit-plot" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">REFERENCE</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<% out.print(request.getParameter("ref"));%>" required="" class="form-control" id="zone-ref" placeholder="Numéro de CNI">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">FACE</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("face"));%>" class="form-control" id="zone-face" placeholder="Face du Plot">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">CoupeRef</label>

          <div class="col-sm-10">
              <input type="text"  required="" value="<% out.print(request.getParameter("coupeRef"));%>" class="form-control" id="zone-coupeRef" placeholder="CoupeRef du Plot">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">VOLUME</label>

          <div class="col-sm-10">
              <input type="float"  required="" value="<% out.print(request.getParameter("volume"));%>" class="form-control" id="zone-volume" placeholder="Volume du Plot">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" id="btn-close-editPane" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-plot"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        $("#btn-edit-plot").click(function(){
            editData($('#zone-ref').val(), $('#zone-face').val(), $('#zone-coupeRef').val(), $('#zone-volume').val());
            $('#form-edit-plot .overlay').removeClass('hide');
            $('#form-edit-plot #btn-edit-plot').addClass("disabled");
        });

    </script>
</div>
