<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-privilege">
    <div class="register-box-body">
          <h2 class="login-box-msg">Modifier un privilege <span class="label" style="display: none;" id="status"></span></h2>

        <form  method="post" post="/privilege/edit" onsubmit="return send(this);">
            <div class="box-body">
              <div class="form-group">
                <label for="zone-epaisseur" class="col-sm-2 control-label">Libelle</label>

                <div class="col-sm-10">
                    <input type="text"  required="" value="<%=request.getParameter("libelle")%>" class="form-control" id="zone-libelle" placeholder="Libelle">
                </div>
              </div><br><br>
              <div class="form-group">
                <label for="zone-epaisseur" class="col-sm-2 control-label">Description</label>

                <div class="col-sm-10">
                    <textarea type="text"  required="" class="form-control" id="zone-description" style="resize: none;" placeholder="Description"><%=request.getParameter("description")%>
                    </textarea>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer" style="background-color: transparent">
                  <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
                  <button type="submit" class="btn btn-info pull-right" id="btn-add-privilege"><i class="fa fa-pencil"></i> Modifier</button>
                  <div class="overlay hide" style="text-align:center;">
                      Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
                  </div>
            </div>
        </form>
      </div>

          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        window.send = function(self){
                $.ajax({
                    url: '/privilege/edit',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        ref: '<%= request.getParameter("ref")%>',
                        libelle: $('#zone-libelle').val(),
                        description: $('#zone-description').val()
                    }, 
                    complete: function (jqXHR, textStatus ) {
                        loadData();
                        $('#btn-close-addPane').click();
                    }
                });
        
            return false;
        }

    </script>
</div>
