<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-personnel">
    <p class="status-edit-personnel" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">N° CNI</label>

          <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="zone-cni" placeholder="Numéro de CNI">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Nom</label>

          <div class="col-sm-10">
              <input type="text"  required="" class="form-control" id="zone-nom" placeholder="Nom du Personnel">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Prénom</label>

          <div class="col-sm-10">
              <input type="text"  required="" class="form-control" id="zone-prenom" placeholder="Prénom du Personnel">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Fonction</label>

          <div class="col-sm-10">
              <input type="text"  required="" class="form-control" id="zone-fonction" placeholder="Fonction du Personnel">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-add-personnel"><i class="fa fa-pencil"></i> Ajouter</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <!--script src="/resources/js/select2/select2.full.min.js"></script-->
    <script type="text/javascript">
        $("#btn-add-personnel").click(function(){
            saveData($('#zone-cni').val(), $('#zone-nom').val(), $('#zone-prenom').val(), $('#zone-fonction').val());
            $('#form-edit-personnel .overlay').removeClass('hide');
            $('#form-edit-personnel #btn-add-personnel').addClass("hide");
        });

    </script>
</div>
