<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-personnel">
    <div class="register-box-body">
          <h2 class="login-box-msg">Modifier un compte <span class="label" style="display: none;" id="status"></span></h2>

        <form  method="post" post="/personnel/edit" onsubmit="return send(this);">
          <div class="form-group has-feedback">
              <legend><h4>Info Personnelles.</h4></legend>
              <input type="text" id="nom" required="" value="<% out.print(request.getParameter("nom"));%>"  class="form-control" placeholder="Nom ">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
              <input type="text" id="prenom" required="" value="<% out.print(request.getParameter("prenom").equals("null") ? "" : request.getParameter("prenom"));%>"  class="form-control" placeholder="Prénom">
            <span class="glyphicon  form-control-feedback"  style="margin-right: 10px;"></span>
          </div>
          <div class="form-group has-feedback select2">
                <legend>Fonction            <span class="glyphicon glyphicon-wrench  form-control-feedback"></span></legend>
                <label>Privilège</label>
                <select id="fonction" required="" class="form-control">
                    <option value="">Privilege</option>
                </select>
          </div>
          <div class="form-group has-feedback select2">
              <label>Droits<span class="glyphicon glyphicon-alert form-control-feedback"></span></label>
              <table class="table table-hover table-responsive">
                    <tr>
                        <td><i class="fa fa-plus"></i></td>
                        <td><i class="fa fa-edit"></i></td>
                        <td><i class="fa fa-trash"></i></td>
                        <td><i class="fa fa-print"></i></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" <%  out.print(request.getParameter("can_add").equals("N") ? "value=\"N\"" : "checked=\"\" value=\"O\"");%> onchange="if(this.value === 'N'){this.value = 'O';}else{this.value = 'N';}" value="N" id="can_add"/></td>
                        <td><input type="checkbox" <%  out.print(request.getParameter("can_edit").equals("N") ? "value=\"N\"" : "checked=\"\" value=\"O\"");%> onchange="if(this.value === 'N'){this.value = 'O';}else{this.value = 'N';}" value="N" id="can_edit"/></td>
                        <td><input type="checkbox" <%  out.print(request.getParameter("can_delete").equals("N") ? "value=\"N\"" : "checked=\"\" value=\"O\"");%> onchange="if(this.value === 'N'){this.value = 'O';}else{this.value = 'N';}" value="N" id="can_delete"/></td>
                        <td><input type="checkbox" <%  out.print(request.getParameter("can_print").equals("N") ? "value=\"N\"" : "checked=\"\" value=\"O\"");%> onchange="if(this.value === 'N'){this.value = 'O';}else{this.value = 'N';}" value="N" id="can_print"/></td>
                    </tr>
              </table>
            
          </div>
            <div class="form-group has-feedback">
                <legend>Authentification</legend>
                <input type="text" id="matricule" required="" value="<% out.print(request.getParameter("matricule"));%>" readonly="" class="form-control" placeholder="Matricule">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" id="password" value="<% out.print(request.getParameter("password"));%>"  required="" class="form-control" placeholder="Password">
            </div>
          <div class="row">
              <!-- /.col -->
            <div class="col-xs-4 pull-left">
                <button type="btn" data-dismiss="modal"  id="btn-close-editPane" class="btn form-control btn-block btn-flat">Fermer</button>
            </div>
            <div class="pull-right col-xs-4">
                <button type="submit" class="btn btn-primary form-control btn-block btn-flat">Enregistrer</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
      </div>

          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        window.send = function(self){
            var process = confirm("\nAssistance de création de compte\n\n\n\
                \nNom       = "+$('#nom').val()+"\n\
                \nPrenom  = "+$('#prenom').val()+"\n\
                \nPrivilège = "+$('#fonction').val()+"\n\
                \n\tPeut Ajouter     = "+($('#can_add').val() === 'O' ? 'Oui' : 'Non')+"\n\
                \n\tPeut Modier      = "+($('#can_edit').val() === 'O' ? 'Oui' : 'Non')+"\n\
                \n\tPeut Supprimer = "+($('#can_delete').val() === 'O' ? 'Oui' : 'Non')+"\n\
                \n\tPeut Imprimer   = "+($('#can_print').val() === 'O' ? 'Oui' : 'Non')+"\n\
                \nMatricule = "+$('#matricule').val()+"\n\
                \nMot de Passe = "+$('#password').val()+"\n\n\nConfirmez-vous ces informations?");
            if(process === true){
                $.ajax({
                    url: '/personnel/edit',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        nom: $('#nom').val(),
                        prenom: $('#prenom').val(),
                        fonction: $('#fonction').val(),
                        can_add: $('#can_add').val(),
                        can_edit: $('#can_edit').val(),
                        can_delete: $('#can_delete').val(),
                        can_print: $('#can_print').val(),
                        matricule: $('#matricule').val(),
                        password: $('#password').val()
                    }, 
                    complete: function (jqXHR, textStatus ) {
                        var result = jqXHR.responseJSON;
                        if(result.resultat === 'true'){
                            $('#nom').val('');
                            $('#prenom').val('');
                            $('#fonction').val('');
                            document.getElementById('can_add').checked = false;
                            document.getElementById('can_edit').checked = false;
                            document.getElementById('can_delete').checked = false;
                            document.getElementById('can_print').checked = false;
                            $('#matricule').val('');
                            $('#password').val('');
                            $('#status').css('margin-left','10px').addClass("label-success").html('Modifié avec succès').show();
                            scrollTo(screenLeft, screenTop);
                            setTimeout(()=>$('#status').hide(), 10000);
                        }else{
                            switch(result.type){
                                case 'redondance':
                                    $('#status').css('margin-left','10px').addClass("label-danger").html(result.reason).show();
                                    scrollTo(screenLeft, screenTop);
                                    setTimeout(()=>$('#status').hide(), 10000);
                                    break;
                                case 'database': 
                                    $('#status').css('margin-left','10px').addClass("label-danger").html(result.reason).show();
                                    scrollTo(screenLeft, screenTop);
                                    setTimeout(()=>$('#status').hide(), 10000);
                                    break;
                                case 'any': 
                                    $('#status').css('margin-left','10px').addClass("label-danger").html(result.reason).show();
                                    scrollTo(screenLeft, screenTop);
                                    setTimeout(()=>$('#status').hide(), 10000);
                                    break;

                            }
                        }
                    }
                });
            }else{
                scrollTo(screenLeft, screenTop);
                $('#status').css('margin-left', '10px').addClass(' label-primary').html('Opération annulée');
                setTimeout(()=>$('#status').hide(), 5000);
            }
        
            return false;
        }
        $.ajax({
            url: '/api/privilege/list',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var select = document.getElementById("fonction");
                var option;
                var data = jqXHR.responseJSON;
                
                for(var i=0; i<data.length; i++){
                    option = document.createElement("option");
                    option.value = data[i].ref;
                    option.innerText = data[i].libelle;
                    select.appendChild(option);
                    if(data[i].ref == '<% out.print(request.getParameter("fonction[ref]"));%>')
                        option.selected = true;
                }
                
            }
        });

    </script>
</div>
