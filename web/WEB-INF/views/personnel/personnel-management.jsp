<%@page import="app.models.Personnel"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="app.models.PersonnelHistory"%>
<!-- 
    Document   : totoPage
    Created on : 8 d�c. 2018, 14:42:16
    Author     : champlain
-->

<%
    
    List<PersonnelHistory> liste = (List)request.getAttribute("liste");
    Personnel personnel = (Personnel) request.getAttribute("personnel");
    DecimalFormat formatter = new DecimalFormat("00");
    
%>

    <div class="row" style="background-color: gray; padding: 40px;">
        <div class="col-sm-3 col-lg-3 col-md-3" style="height: 200px; width: 200px;">
            <img style=" width: 200px; height: 200px; border-radius: 100px;" src="/resources/image/user1-128x128.jpg" >
        </div>
        <div class="col-sm-4 col-lg-4 col-md-4">
            <div class="form-group col-sm-12 col-lg-12 col-md-12">
                <span class=" col-sm-12 col-lg-12 col-md-12" style="color: #FFF; opacity: 0.4">Matricule</span>
                <label class="  col-sm-12 col-lg-12 col-md-12" style="color: #ece7e7; font-size: 18px;"><%=personnel.getCniPersonnel()%></label>
            </div>
            <div class="form-group col-sm-12 col-lg-12 col-md-12">
                <span class=" col-sm-12 col-lg-12 col-md-12" style="color: #FFF; opacity: 0.4">Nom</span>
                <label class="text-gray-light  col-sm-12 col-lg-12 col-md-12" style="color: #ece7e7; font-size: 18px"><%=personnel.getNomPersonnel()%></label>
            </div>
            <div class="form-group  col-sm-12 col-lg-12 col-md-12s">
                <span class=" col-sm-12 col-lg-12 col-md-12" style="color: #FFF; opacity: 0.4">Pr�nom</span>
                <label class="text-gray-light  col-sm-12 col-lg-12 col-md-12" style="color: #ece7e7; font-size: 18px;"><%=personnel.getPrenomPersonnel()%></label>
            </div>
        </div>
    </div>
                
    <div class="container-fluid" style="margin-left: 20px;  margin-top: 20px; widows: 100%;">
        <table id="activite_table" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="liste_info" style="width: 100%;">
            <thead>
                <tr role="row">
                    <th>Jour</th>
                    <th>Heure</th>
                    <th>Action</th>
                    <th>IP</th>
                </tr>
            </thead>
            <tbody id="table-body">
                <%
                    
                  for(PersonnelHistory p : liste){
                %>
                <tr>
                    <td><%=formatter.format(p.getDateOperation().getDate())+" / "+formatter.format(p.getDateOperation().getMonth()+1)+" / "+(p.getDateOperation().getYear()+1900)%></td>
                    <td><%=formatter.format(p.getDateOperation().getHours())+"h "+formatter.format(p.getDateOperation().getMinutes())+"min "+formatter.format(p.getDateOperation().getSeconds())+"sec"%></td>
                    <td><%=p.getAction()%></td>
                    <td><%=p.getIp()%></td>
                </tr>
                <%
                  };
                %>
            </tbody>
        </table>
    </div>
