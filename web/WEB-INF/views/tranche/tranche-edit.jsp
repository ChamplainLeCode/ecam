<%-- 
    Document   : tranche-edit
    Created on : 10 janv. 2019, 22:13:00
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div id="form-edit-tranche">
    <p class="status-edit-tranche" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Reférence</label>

          <div class="col-sm-10">
              <input type="text" readonly="" value="<% out.print(request.getParameter("ref"));%>" required="" class="form-control" id="zone-ref" placeholder="Rérérence">
          </div>
        </div>
        <div class="form-group">
          <label for="zone- dateDebut" class="col-sm-2 control-label">DateDebut</label>

          <div class="col-sm-10">
              <input type="date" required="" value="<% out.print(request.getParameter("dateDebut"));%>" class="form-control" id="zone-dateDebut" placeholder="date de Debut de la tranche">
          </div>
        </div>
          <div class="form-group">
          <label for="zone- dateFin" class="col-sm-2 control-label">DateFin</label>

          <div class="col-sm-10">
              <input type="date" required="" value="<% out.print(request.getParameter("dateFin"));%>" class="form-control" id="zone-dateFin" placeholder="date de Fin de la tranche">
          </div>
        </div>
          <div class="form-group">
          <label for="zone- epaisseur" class="col-sm-2 control-label">Epaisseur</label>

          <div class="col-sm-10">
              <input type="text" required="" value="<% out.print(request.getParameter("epaisseur"));%>" class="form-control" id="zone-epaisseur" placeholder="epaisseur de la tranche">
          </div>
        </div>
          <div class="form-group">
          <label for="zone- numTravail" class="col-sm-2 control-label">NumTravail</label>

          <div class="col-sm-10">
              <input type="text" required="" value="<% out.print(request.getParameter("numTravail"));%>" class="form-control" id="zone-numTravail" placeholder="numTravail de la tranche">
          </div>
        </div>
          <div class="form-group">
          <label for="zone- equipe" class="col-sm-2 control-label">Equipe</label>

          <div class="col-sm-10">
              <input type="text" required="" value="<% out.print(request.getParameter("equipe"));%>" class="form-control" id="zone-equipe" placeholder="date de Debut de la tranche">
          </div>
        </div>
          <div class="form-group">
          <label for="zone- plot" class="col-sm-2 control-label">Plot</label>

          <div class="col-sm-10">
              <input type="text" required="" value="<% out.print(request.getParameter("plot"));%>" class="form-control" id="zone-plot" placeholder="plot de la tranche">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-tranche"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
    <script type="text/javascript">
        $("#btn-edit-tranche").click(function(){
            editData($('#zone-ref').val(), $('#zone-dateDebut').val(), $('#zone-dateFin').val(), $('#zone-epaisseur').val(),
            $('#zone-numTravail').val(), $('#zone-equipe').val(), $('#zone-plot').val());
            $('#form-edit-tranche .overlay').removeClass('hide');
            $('#form-edit-tranche #btn-edit-tranche').addClass("disabled");
        });

    </script>
</div>

