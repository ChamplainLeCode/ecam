<%-- 
    Document   : tranche-add
    Created on : 10 janv. 2019, 22:12:41
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<table id="equipe-liste">
    
    <tbody></tbody>
</table>
<div id="form-add-tranche">
    <p class="status-add-tranche" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group hide">
          <label for="zone-ref" class="col-sm-2 control-label">Reférence</label>

          <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="zone-ref" placeholder="Rérérence">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-tranche" class="col-sm-2 control-label">Tranche</label>

          <div class="col-sm-10">
              <select required="" style="width:100%" class="select2" id="zone-tranche"></select>
          </div>
        </div>
        <div class="form-group">
          <label for="zone-dateDebut" class="col-sm-2 control-label">DateDebut</label>

          <div class="col-sm-10">
              <input type="date" required="" class="form-control" id="zone-dateDebut" placeholder="date de Debut de la tranche">
          </div>
        </div>
          <div class="form-group">
          <label for="zone-epaisseur" class="col-sm-2 control-label">Epaisseur</label>

          <div class="col-sm-10">
              <input type="text" pattern="[0-9]+/10" min="0" required="" class="form-control" id="zone-epaisseur" placeholder="epaisseur de la tranche">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-plot" class="col-sm-2 control-label">Plot</label>

          <div class="col-sm-10">
              <select  required="" style="width:100%;" class="select2" id="zone-plot"></select>
          </div>
        </div>
          <div class="form-group">
          <label for="zone-numTravail" class="col-sm-2 control-label">NumTravail</label>

          <div class="col-sm-10">
              <input type="text" required="" readonly="" class="form-control" id="zone-travail" placeholder="numTravail de la tranche">
          </div>
        </div>

        <div class="form-group">
          <label for="zone-equipe" class="col-sm-2 control-label">Equipe</label>
          <div class="col-sm-10">
              <select class="select2" id="zone-equipe" required="" style="width: 100%;"></select>
                <!--input class="form-control col-sm-8" id="zone-equipe" readonly="">
                <div class="btn-group">
                    <button id="btn-select-equipe" type="button" class="btn btn-flat left-side" data-toggle="modal" data-target="#modal-equipe" style="background-color: transparent;">Sélectionnez</button>
                    <a href="equipe" target="#equipe-liste"><button id="btn-create-equipe" class="btn btn-primary center "><i class="fa fa-plus"></i> Nouvelle équipe <i class="fa fa-group"></i></button></a>
                    <button onclick="$('#btn-select-equipe').click();" type="button" class="btn btn-success right-side"><i class="fa fa-refresh"></i> Actualiser</button>
                </div-->
            </div>
        </div>
          <div style="width: 120%;">
              
          <div class="form-group col-sm-3">
            <label for="zone-face1" class="col-sm-12 control-label left">Face 1</label>
            <div class="col-sm-12">
                <input type="number" required="" class="form-control" id="zone-face1" placeholder="Face 1">
            </div>
          </div>
          <div class="form-group col-sm-3">
            <label for="zone-face2" class="col-sm-12 control-label left">Face 2</label>
            <div class="col-sm-12">
                <input type="number" required="" class="form-control" id="zone-face2" placeholder="Face 2">
            </div>
          </div>
          <div class="form-group col-sm-3">
            <label for="zone-face3" class="col-sm-12 control-label left">Face 3</label>
            <div class="col-sm-12">
                <input type="number" required="" class="form-control" id="zone-face3" placeholder="Face 3">
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-save-tranche">Enregistrer</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        $("#zone-ref").val('REF_CLASS_'+(new Date()).getTime());
        $('select').select2();
        
        /**
         * Ici on doit récupérer les plots sortis de la chaudière (cuve ~= CuvePlot)
         */
        function setPlots(){
            $.ajax({
                url: '/api/tranche/plot_for_tranche/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                        var data = jqXHR.responseJSON;
                        
                    var select = document.getElementById("zone-plot");
                    var opt = document.createElement("option");
                    opt.innerHTML = "-- selectionnez le plot --";
                    opt.value = '';
                    select.appendChild(opt);
                    for(var i = 0; i<data.length; i++){
                        opt = document.createElement("option");
                        // data[i][0] contient le numéro du plot
                        opt.innerHTML = data[i][0];
                        opt.value = data[i][0];
                        // on définit comme attribut data le numéro de travail du plot data[i][0]
                        opt.setAttribute("data", data[i][1]+"");
                        select.appendChild(opt);
                    }
                }
            });
        }
        $.ajax({
            url: '/api/equipe/list',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var select = document.getElementById("zone-equipe");
                var data = jqXHR.responseJSON;
                if(data.length <= 0){
                    var option = document.createElement("optiongroupe");
                    option.innerText = "Aucune equipe";
                    select.appendChild(option);
                    return;
                }
                select.innerHTML = '';
                var option = document.createElement("option");
                option.innerHTML = "-- Selectionnez l'équipe --";
                option.value = "";
                select.appendChild(option);
                for(var i = 0; i< data.length; i++){
                    option = document.createElement("option");
                    option.value = data[i].ref;
                    option.innerText = data[i].libelle;
                    select.appendChild(option);
                }
            }
        });



        $.ajax({
            url: '/api/equipe/list',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var select = document.getElementById("zone-equipe");
                var data = jqXHR.responseJSON;
                if(data.length <= 0){
                    var option = document.createElement("optiongroupe");
                    option.innerText = "Aucune equipe";
                    select.appendChild(option);
                    return;
                }
                select.innerHTML = '';
                var option = document.createElement("option");
                option.innerHTML = "-- Selectionnez l'équipe --";
                option.value = "";
                select.appendChild(option);
                for(var i = 0; i< data.length; i++){
                    option = document.createElement("option");
                    option.value = data[i].ref;
                    option.innerText = data[i].libelle;
                    select.appendChild(option);
                }
            }
        });

        $.ajax({
            url: '/api/tranche-machine/list',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var select = document.getElementById("zone-tranche");
                var data = jqXHR.responseJSON;
                if(data.length <= 0){ 
                    return;
                }
                select.innerHTML = '';
                var option = document.createElement("option");
                option.innerHTML = "-- Selectionnez la tranche --";
                option.value = "";
                select.appendChild(option);
                for(var i = 0; i< data.length; i++){
                    option = document.createElement("option");
                    option.value = data[i].ref;
                    option.innerText = data[i].libelle;
                    select.appendChild(option);
                }
            }
        });


        
        $('#zone-plot').change(function(){
           var travail = document.getElementById("zone-plot").options[document.getElementById("zone-plot").selectedIndex].getAttribute("data");
           $('#zone-travail').val(travail);
        });
        
        
        setPlots();
        
        $("#btn-save-tranche").click(function(){
            saveData($('#zone-tranche').val(), $('#zone-ref').val(),$('#zone-dateDebut').val(), $('#zone-epaisseur').val(),
            $('#zone-travail').val(),$('#zone-equipe').val(),$('#zone-plot').val(), ($('#zone-face1').val() == '' ? '0' : $('#zone-face1').val()), ($('#zone-face2').val() == '' ? '0' : $('#zone-face2').val()), ($('#zone-face3').val() == '' ? '0' : $('#zone-face3').val()));
            $('#form-add-tranche .overlay').removeClass('hide');
            $('#form-add-tranche #btn-save-tranche').addClass("disabled");
        });
        /**
        $('#btn-select-equipe').click(function(){ 
            $.ajax({
                url: '/api/equipe/list',
                method: 'GET',
                beforeSend: function (xhr) {
                    $('#equipe-liste tbody').html('<div class="overlay">'+
                        '<i class="fa fa-refresh fa-spin"></i>'+
                      '</div>')
                },
                complete: function (jqXHR, textStatus ) {
                    var data = JSON.parse(jqXHR.responseText);
                    var equipe = $('#equipe-liste tbody').html(''); 
                    if(data.length <= 0){
                        $('#equipe-liste tbody').html(
                            '<tr>\
                                <td colspan="3"><div class="center ">\
                                    <center><h3>Aucune équipe constituée</h3></center>\
                                </div></td>\
                            </tr>\\n\
                            <tr><td><h1> </h1></td></tr>\
                            <tr>\
                                <td><button id="btn-close-equipe" class="btn btn-flat pull-left"><i class="fa fa-remove"></i> Quitter</button>\
                                <button onclick="$(\'#btn-select-equipe\').click();" class="btn btn-success pull-right"><i class="fa fa-refresh"></i> Actualiser</button>\
                                <a href="equipe" target="#equipe-liste"><button id="btn-create-equipe" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Nouvelle équipe <i class="fa fa-group"></i></button></a></td>\
                            </tr>'
 );
                
                        $('#btn-close-equipe').click(function(){
                            $("#equipe-liste tbody").html('');//hide(1000); 
                            $('#form-add-tranche').show(1000);
                        });
                        
                        $('#btn-create-equipe').click(function(){
                            
                        });
                    }else{
                        for(var i=0; i<data.length; i++){
                            var e = data[i];
                            equipe.html(equipe.html()+'<div style="display:block" class="box  box-widget widget-user">'+
                            '<!-- Add the bg color to the header using any of the bg-* classes -->'+
                            '<div class="widget-user-header bg-aqua-active">'+
                              '<h3 class="widget-user-username">'+e.refPersonnel.nom+' '+e.refPersonnel.prenom+'</h3>'+
                              '<h5 class="widget-user-desc">'+e.refPersonnel.fonction+'</h5>'+
                              '<input type="radio" name="equipe_selected" value="'+e.ref+'" class="pull-right">'+
                            '</div>'+
                            '<div class="widget-user-image">'+
                              '<img class="img-circle" src="/resources/image/avatar5.png" alt="User Avatar">'+
                            '</div>'+
                            '<div class="box-footer">'+
                              '<div class="row">'+
                               ' <div class="col-sm-4 border-right">'+
                                  '<div class="description-block">'+
                                   ' <h5 class="description-header">'+e.ref+'</h5>'+
                                    '<span class="description-text">Reférence</span>'+
                                 ' </div>'+
                                 ' <!-- /.description-block -->'+
                                ' </div>'+
                               ' <!-- /.col -->'+
                                '<div class="col-sm-4 border-right">'+
                                 ' <div class="description-block">'+
                                   ' <h5 class="description-header">'+new Date(parseFloat(e.dateEquipe)).toDateString()+'</h5>'+
                                   ' <span class="description-text">Date</span>'+
                                 ' </div>'+
                                 ' <!-- /.description-block -->'+
                                '</div>'+
                                '<!-- /.col -->'+
                                '<div class="col-sm-4">'+
                                 ' <div class="description-block">'+
                                    '<h5 class="description-header">'+new Date(parseFloat(e.dateExecution)).toDateString()+'</h5>'+
                                    '<span class="description-text">Exécution</span>'+
                                 ' </div>'+
                                  '<!-- /.description-block -->'+
                                '</div>'+
                                '<!-- /.col -->'+
                              '</div>'+
                             ' <!-- /.row -->'+
                           '</div>\
                          </div>');
                        }
                        equipe.html(equipe.html()+'<tr><td><h1> </h1></td></tr>\
                                <tr>\
                                    <button onclick="$(\'#btn-select-equipe\').click();" class="btn btn-success pull-right"><i class="fa fa-refresh"></i> Actualiser</button>\
                                    <a href="equipe" target="#equipe-liste"><button id="btn-create-equipe" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Nouvelle équipe <i class="fa fa-group"></i></button></a></td>\
                                </tr>');
                        $('input[name="equipe_selected"]')
                                .change(function(){
                                    $('#zone-equipe').val($(this).val()); 
                                    $("#equipe-liste").hide(1000); 
                                    $('#form-add-tranche').show(1000);
                                    $('#btn-save-tranche').removeAttr('disabled');
                                }).click(function(){
                                    $('#zone-equipe').val($(this).val()); 
                                    $("#equipe-liste").hide(1000); 
                                    $('#form-add-tranche').show(1000);
                                    $('#btn-save-tranche').removeAttr('disabled');
                                });
                        
                        var listEquipe = document.getElementsByName("equipe_selected");
                        for(var i=0; i<listEquipe.length; i++){
                            if(listEquipe[i].value === $('#zone-equipe').val()){
                                listEquipe[i].checked = true;
                                break;
                            }
                        }
                        $('#form-add-tranche').hide(1000)
                        $('#equipe-liste').show(1000).DataTable();
                    }
                    
                }
            });
        });
*/
    </script>
</div>
