<%-- 
    Document   : tranche-fiche
    Created on : 25 juin 2019, 19:34:01
    Author     : champlain
--%>
<%@page import="app.models.Billonnage"%>
<%@page import="app.models.Fournisseur"%>
<%@page import="app.models.Commande"%>
<%@page import="app.models.Reception"%>
<%@page import="app.models.DetailReception"%>
<%@page import="app.modelController.DetailReceptionJpaController"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.function.Consumer"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.LinkedList"%>
<%@page import="java.util.List"%>
<%@page import="app.models.Billon"%>
<%@page import="app.modelController.BillonJpaController"%>
<%@page import="app.models.Database"%>
<%@page import="app.modelController.BillonnageJpaController"%>
<%
  
    String[] donnees = request.getParameterValues("donnees[]");
    int taille = Integer.parseInt(request.getParameter("taille"));
    
    if(donnees == null || donnees.length == 0){
        out.print("<h1>Impossible d'afficher cette page Donnees incorrectes</h1>");
        
    }else{
        BillonnageJpaController cont = new BillonnageJpaController(Database.getEntityManager());
        List<List<Billon>> data = new LinkedList();
        
        for(String travail : donnees){
            Billonnage b = cont.findBillonnageByTravail(travail);
            if(b == null)
                continue;
            List<Billon> list = BillonJpaController.findByBillonnage(b.getRefBillonnage());
            data.add(list);
        }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title>MODULE DE PRODUCTION</title>
	<meta name="generator" content="LibreOffice 5.4.6.2 (Linux)"/>
	<meta name="created" content="2019-06-20T20:38:08.279484997"/>
	<meta name="changed" content="2019-06-20T20:50:47.901106903"/>
	<style type="text/css">
            @media print{
                input{
                    border: none;
                    background: transparent;
                }
                .no-print{
                    display: none;
                }
            }
		@page { margin: 0cm }
		p { margin-bottom: 0.25cm; line-height: 120% }
		td p { margin-bottom: 0cm }
                input {border: none; }
	</style>
</head>
    <body lang="fr-FR" dir="ltr">
        <button class="btn btn-primary no-print" onclick="window.print();" style="width:200px; height: 60px; border-radius: 9px; background-color: #123456; color: white; opacity: 0.8; font-size: 20pt;">Imprimer</button>
	<div style="text-align: center; background-color: #808080; padding: 10px; width: 70%; margin-left: 15%; border-radius: 20px; margin-bottom: 50pt;">
		<p ><b>******************************</b></p>
                <p style="font-size: 18pt"><b>MODULE DE PRODUCTION</b></p>
		<p style="font-weight: normal">………………………………………</p>
                <p style="font-size: 18pt"><b>N°</b><input type="text"/> &nbsp; &nbsp; &nbsp; <b>Du</b><input style="margin-left: 10pt;" type="date"></p>

        </div>
        
        <table class="table ">
            <tr><td><span style="font-weight: bold">Epaisseur </span> <input type="text" placeholder="saisissez l'épaisseur" style="border:none" ></td></tr>
        </table><br>
        <table width="100%" cellpadding="3" cellspacing="0">
                <thead>
                        <tr valign="top">
                                <th width="15%" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                        <span style="font-size: 8pt; border-top: none; padding-top: 0cm; padding-bottom: 0.07cm; padding-left: 0cm; padding-right: 0cm">
                                        N° Bille</span>
                                        <hr/>
                                        <span style="font-size: 8pt; ">N° Travail</span>
                                </th>
                                <th width="12%" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                        <p style="font-size: 8pt; ">Fournisseurs</p>
                                </th>
                                <th width="10%" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                        <p style="font-size: 8pt; ">Certificat</p>
                                </th>
                                <th width="10%" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                        <p style="font-size: 8pt; ">Essence</p>
                                </th>
                                <th colspan="5" width="30%" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                        <p style="font-size: 8pt; ">Trancheuse</p>
                                </th>
                                <th colspan="5" width="30%" style="border: 1px solid #000000; padding: 0.1cm">
                                        <p style="font-size: 8pt; ">Massicot</p>
                                </th>
                        </tr>
                </thead>
                <tbody style="text-align: center; font-size: 8pt;">
                    <% for(List<Billon> ligne: data){
                            String essence = "";
                            if(ligne.size()== 0)
                                continue;
                            HashMap<String, Integer> map = new HashMap(5);
                            for(Billon b : ligne){
                                if(b.getPlotList() != null && b.getPlotList().size()>0)
                                    if(map.get(b.getPlotList().get(0).getLongueur()+"") == null)
                                        map.put(b.getPlotList().get(0).getLongueur()+"", 1);
                                    else
                                        map.put(b.getPlotList().get(0).getLongueur()+"", map.get(b.getPlotList().get(0).getLongueur()+"")+1);
                            }
                    %>
                        <tr valign="top">
                                <td width="15%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                        <span>
                                            <%= ligne.get(0).getRefBillonnage().getNumBille() %>
                                            <hr/>
                                            <%= ligne.get(0).getNumTravail()%>
                                        </span>
                                </td>
                                <td width="16%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                        <p><br/>
                                            <%
                                                if(ligne !=null && ligne.size()>0){
                                                    DetailReceptionJpaController contDR = new DetailReceptionJpaController(Database.getEntityManager());
                                                    DetailReception dr = contDR.findDetailReceptionByNumTravail(ligne.get(0).getRefBillonnage().getNumTravail());
                                                    Reception recep  = dr.getRefReception();
                                                    if(recep != null){
                                                        essence = dr.getRefEssence().getRefEssence()+" "+dr.getRefEssence().getLibelle();
                                                        Commande cmd = recep.getRefCommande();
                                                        if(cmd != null){
                                                            Fournisseur f = cmd.getRefFournisseur();
                                                            if(f!=null)
                                                                out.print(f.getNomFournisseur());
                                                        }

                                                    }
                                                }
                                            %>
                                        </p>
                                </td>
                                <td width="10%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                        <p><br/>
                                            <%= ligne.get(0).getRefBillonnage().getRefParc().getRefTypeparc().getLibelle() %>
                                        </p>
                                </td>
                                <td width="10%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                        <p><br/>
                                            <%= essence%>
                                        </p>
                                </td>

                                <% for(int i=0; i<5; i++){ %>
                                <td width="5%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                        <p>
                                            <%="<input type=\"text\" class=\"form-control\" style=\"width:60px; height:50px; text-align: center;\" placeholder=\" \">" %>
                                        </p>
                                </td>
                                <% } %>
                                
                                <% for(int i=0; i<5; i++){ %>
                                <td width="5%" style="border-top: none; border-top: none; border-bottom: 1px solid #000000; <%=(i==0 ? " border-left: 1px solid #000000; ":"")%> border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                                        <p>
                                            <%="<input type=\"text\" class=\"form-control\" style=\"width:60px; height:50px; border: none; text-align: center;\" placeholder=\" \">" %>
                                        </p>
                                </td>
                                <% } %>
                        </tr>
                        <% } %>
                        
                </tbody>
        </table>
        <p style="margin-bottom: 0cm; line-height: 100%"><br/>

        </p>
    </body>
</html>

<% } %>