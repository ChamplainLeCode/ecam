<%-- 
    Document   : sideBar
    Created on : 10 déc. 2018, 15:13:30
    Author     : champlain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

      
<aside class="main-sidebar" style="height:100%;">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar" style="height: 100%; overflow-x: visible; overflow-y: auto;">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
                <img src="/resources/image/IMG-20190401-WA0011.jpg" class="img-circle user-photo" alt="User Image">
            </div>
            <div class="pull-left info">
                <p class="user-name">Alexander Pierce</p>
              <!-- Status -->
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- search form (Optional) -->
          <form action="#" method="get" class="sidebar-form hide">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                  <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                  </button>
                </span>
            </div>
          </form>
          <!-- /.search form -->

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu" data-widget="tree">
            <li class="header">ACTIVITÉS</li>
            <!-- Optionally, you can add icons to the links -->
            <% /*
               switch(request.getAttribute("privilege").toString()){
                   
                   case "OPERATRICE":
                <li class=""><a class="ecam-route" href="/billonnage"><i class="fa fa-truck"></i> <span>Billonnage</span></a></li>
                <li class=""><a class="ecam-route" href="/billon"><i class="fa fa-key"></i> <span>Billon</span></a></li>
                <li class=""><a class="ecam-route" href="/coupe"><i class="fa fa-key"></i> <span>Scierie</span></a></li>
                <li class=""><a class="ecam-route" href="/cuveplot"><i class="fa fa-key"></i> <span>CuvePlot</span></a></li>
                <li class=""><a class="ecam-route" href="/plot"><i class="fa fa-key"></i> <span>Plot</span></a></li>
                <li class=""><a class="ecam-route" href="/parc"><i class="fa fa-key"></i> <span>Parc</span></a></li>
                <li class=""><a class="ecam-route" href="/rebut"><i class="fa fa-key"></i> <span>Rebut</span></a></li>
                <li class=""><a class="ecam-route" href="/tranche"><i class="fa fa-key"></i> <span>Trancher</span></a></li>
                <%
                    break;
                    case "RECEPTION":
                <li class=""><a class="ecam-route" href="/reception"><i class="fa fa-key"></i> <span>Reception</span></a></li>
                <li class=""><a class="ecam-route" href="/commandes"><i class="fa fa-shopping-bag"></i> <span>Commande Fournisseur</span></a></li>
                <%
                    break;
                    case "EMPOTAGE":
                <li class=""><a class="ecam-route" href="/empotage"><i class="fa fa-key"></i> <span>Empotage</span></a></li>
                <li class=""><a class="ecam-route" href="/colis"><i class="fa fa-key"></i> <span>Colis</span></a></li>
                <%
                    break;
                    case "EMBARQUEMENT":
                <li class=""><a class="ecam-route" href="/contenaire"><i class="fa fa-ship"></i> <span>Embarquement</span></a></li>
                <li class=""><a class="ecam-route" href="/lettrecamion"><i class="fa fa-key"></i> <span>LettreCamion</span></a></li>
                <%
                    break;
                    case "COMMERCIAL":
                <li class=""><a class="ecam-route" href="/contenaire"><i class="fa fa-ship"></i> <span>Embarquement</span></a></li>
                <li class=""><a class="ecam-route" href="/colis"><i class="fa fa-key"></i> <span>Colis</span></a></li>
                <%
                    break;
                    case "MASSICOT":
                <li class=""><a class="ecam-route" href="/massicot"><i class="fa fa-key"></i> <span>Massicot</span></a></li>
                <li class=""><a class="ecam-route" href="/massicotpaquet"><i class="fa fa-key"></i> <span>MassicotPaquet</span></a></li>
                <%
                    break;
                    case "COMPTABILITE":
                <li class=""><a class="ecam-route" href="/comptabilite"><i class="glyphicon glyphicon-duplicate"></i> <span>Stocks</span></a></li>
                <%
                    break;
                    case "INFORMATICIEN":
                    case "informaticien":
                <li class="header"><h6>Personnes</h6></li>
                <li class=""><a class="ecam-route" href="/register"><i class="fa fa-key"></i> <span>New Compte</span></a></li>
                <li class=""><a class="ecam-route" href="/personnel"><i class="fa fa-key"></i> <span>Personnel</span></a></li>
                <li class=""><a class="ecam-route" href="/equipe"><i class="fa fa-key"></i> <span>Equipe</span></a></li>
                <li class=""><a class="ecam-route" href="/client"><i class="fa fa-key"></i> <span>Client</span></a></li>
                <li class=""><a class="ecam-route" href="/fournisseurs"><i class="fa fa-bus"></i> <span>Fournisseurs</span></a></li>
                <li class="header"><h6>Comptabilite</h6></li>
                <li class=""><a class="ecam-route" href="/comptabilite"><i class="fas fa-box"></i> <span>Stocks</span></a></li>
                <li class="header"><h6>Opératrice</h6></li>
                <li class=""><a class="ecam-route" href="/billonnage"><i class="fa fa-truck"></i> <span>Billonnage</span></a></li>
                <li class=""><a class="ecam-route" href="/billon"><i class="fa fa-key"></i> <span>Billon</span></a></li>
                <li class=""><a class="ecam-route" href="/coupe"><i class="fa fa-key"></i> <span>Scierie</span></a></li>
                <li class=""><a class="ecam-route" href="/cuveplot"><i class="fa fa-key"></i> <span>CuvePlot</span></a></li>
                <li class=""><a class="ecam-route" href="/plot"><i class="fa fa-key"></i> <span>Plot</span></a></li>
                <li class=""><a class="ecam-route" href="/parc"><i class="fa fa-key"></i> <span>Parc</span></a></li>
                <li class=""><a class="ecam-route" href="/rebut"><i class="fa fa-key"></i> <span>Rebut</span></a></li>
                <li class=""><a class="ecam-route" href="/tranche"><i class="fa fa-key"></i> <span>Trancher</span></a></li>
                <li class="header"><h6>Reception</h6></li>
                <li class=""><a class="ecam-route" href="/reception"><i class="fa fa-key"></i> <span>Reception</span></a></li>
                <li class=""><a class="ecam-route" href="/commandes"><i class="fa fa-shopping-bag"></i> <span>Commande Fournisseur</span></a></li>
                 <li class="header"><h6>Empotage</h6></li>
                <li class=""><a class="ecam-route" href="/empotage"><i class="fa fa-key"></i> <span>Empotage</span></a></li>
                <li class=""><a class="ecam-route" href="/colis"><i class="fa fa-key"></i> <span>Colis</span></a></li>
                <li class="header"><h6>Embarquement</h6></li>
                <li class=""><a class="ecam-route" href="/contenaire"><i class="fa fa-ship"></i> <span>Embarquement</span></a></li>
                <li class=""><a class="ecam-route" href="/lettrecamion"><i class="fa fa-key"></i> <span>LettreCamion</span></a></li>
                <li class="header"><h6>Massicot</h6></li>
                <li class=""><a class="ecam-route" href="/massicot"><i class="fa fa-key"></i> <span>Massicot</span></a></li>
                <li class=""><a class="ecam-route" href="/massicotpaquet"><i class="fa fa-key"></i> <span>MassicotPaquet</span></a></li>
                <li class="header"><h6>Management</h6></li>
                <li class=""><a class="ecam-route" href="/cuve"><i class="fa fa-key"></i> <span>Cuve</span></a></li>
                <li class=""><a class="ecam-route" href="/typecontenaire"><i class="fa fa-key"></i> <span>Typecontenaire</span></a></li>
                <li class=""><a class="ecam-route" href="/typecoupe"><i class="fa fa-key"></i> <span>TypeCoupe</span></a></li>
                <li class=""><a class="ecam-route" href="/tranche-machine"><i class="fa fa-key"></i> <span>Machine à Trancher</span></a></li>
                <li class=""><a class="ecam-route" href="/typeparc"><i class="fa fa-key"></i> <span>TypeParc</span></a></li>
                <li class=""><a class="ecam-route" href="/essences"><i class="fa fa-key"></i> <span>Essences</span></a></li>
                <li class=""><a class="ecam-route" href="/parcchargement"><i class="fa fa-key"></i> <span>ParcChargement</span></a></li>
                
                
                <li class=""><a class="ecam-route" href="/feuille"><i class="fa fa-key"></i> <span>Feuille</span></a></li>
            <li class=""><a class="ecam-route" href="/paquet"><i class="fa fa-key"></i> <span>Paquet</span></a></li>
            <li class=""><a class="ecam-route" href="/sechoir"><i class="fa fa-key"></i> <span>Sechoir</span></a></li>
            <li class=""><a class="ecam-route" href="/classification"><i class="fa fa-key"></i> <span>Classification</span></a></li>
            <li class=""><a class="ecam-route" href="/classification-ess"><i class="fa fa-key"></i> <span>Classification Ess</span></a></li>
              
            <li class="treeview">
              <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#">Link in level 2</a></li>
                <li><a href="#">Link in level 2</a></li>
              </ul>
            </li>
            
            <% } */%>
            
          </ul>
          <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>
