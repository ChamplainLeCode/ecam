    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <meta name="viewport" content="directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0"/>
    <link rel="stylesheet" href="/resources/css/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/resources/css/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" href="/resources/css/dataTables.bootstrap.min.css">      <!-- Theme style -->

    <link rel="stylesheet" href="/resources/css/AdminLTE.min.css">
    <link rel="stylesheet" href="/resources/css/skin-blue.min.css">
    <script src="/resources/js/app.js"></script>
    <style>
        @media print{
            .no-print{
                visibility: hidden;
            }
        }
    </style>