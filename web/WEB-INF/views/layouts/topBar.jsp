<%-- 
    Document   : topBar
    Created on : 10 déc. 2018, 15:13:20
    Author     : champlain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
      <header class="main-header">

        <!-- Logo -->
        <a data="/" href="/" class="logo ecam-route">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>E</b>CAM</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>ECAM BOIS</b></span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="" class="sidebar-toggle" id="toggle-navigation" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <img src="/resources/image/IMG-20190401-WA0011.jpg" class="user-image user-photo" alt="User Image">
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs user-name">Alexander Pierce</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                      <img src="/resources/image/IMG-20190401-WA0011.jpg" class="img-circle user-photo" alt="User Image">
                    
                    <p>
                        <span class="user-name">Alexander Pierce</span><br>
                      <span style="text-align: center" class="user-privilege"></span>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                        <a href="#" onclick="logout()" class="btn btn-default btn-flat">Déconnecter</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <script type="text/javascript">
          
        window.logout = function(){
            getIPs(function(ip){
                $.ajax({
                    url:'/user/disconnected',
                    method: 'POST',
                    data: {matricule: user.matricule, ip: ip},
                    dataType: 'json',
                    complete: function (jqXHR, textStatus ) {
                        localStorage.removeItem("user"); 
                        sessionStorage.removeItem('lockscreen'); 
                        location.href = "/";
                    }
                });
            });
        };
        window.block = function(){
            getIPs(function(ip){
                $.ajax({
                    url:'/user/block_me',
                    method: 'POST',
                    data: {matricule: user.matricule, ip: ip},
                    dataType: 'json',
                    complete: function (jqXHR, textStatus ) {
                        localStorage.removeItem("user"); 
                        sessionStorage.removeItem('lockscreen'); 
                        location.href = "/";
                    }
                });
            });
        };
          
    </script>