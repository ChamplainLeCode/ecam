    <script src="/resources/js/my/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="/resources/css/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/resources/js/my/adminlte.min.js"></script>
    <script src="/resources/js/my/demo.js"></script>
    
    
    <script type="text/javascript">
        
        
        $('.user-name').html(user.nom+" "+user.prenom);
        $('.user-privilege').html(user.fonction.libelle);
        //$('.user-photo').attr('src', user.photo);
        
        var route = $('.sidebar-menu').html();
        for(var i=0; i<user.fonction.routes.length; i++)
            if(user.fonction.routes[i].nom === 'EMPOTAGE'){
                //                          <li><a class="ecam-route" href="/bordereau">Bordereau</a></li>\

                route += '<li class="treeview">\
                        <a href="#"><i class="fa fa-link"></i> <span>EMPOTAGE</span>\
                          <span class="pull-right-container">\
                              <i class="fa fa-angle-left pull-right"></i>\
                            </span>\
                        </a>\
                        <ul class="treeview-menu">\
                          <li><a class="ecam-route" href="/armateur">Armateurs</a></li>\
                          <li><a class="ecam-route" href="/agent">Agents</a></li>\
                          <li><a class="ecam-route" href="/conteneur">Conteneurs</a></li>\
                          <li><a class="ecam-route" href="/douanier">Douaniers</a></li>\
                          <li><a class="ecam-route" href="/contenaire">Embarquement</a></li>\
                          <li><a class="ecam-route" href="/navire">Navire</a></li>\
                          <li><a class="ecam-route" href="/pays">Pays</a></li>\
                          <li><a class="ecam-route" href="/port">Ports</a></li>\
                          <li><a class="ecam-route" href="/transporteur">Transporteurs</a></li>\
                          <li><a class="ecam-route" href="/ville">Villes</a></li>\
                        </ul>\
                      </li>';
            }else{
        //    route += '<li class=""><a class="ecam-route"'+(user.fonction.routes[i].nom === 'Massicotage' ? ' target="_blank" rel="noopener noreferrer" ': '')+' href="'+user.fonction.routes[i].path+'"><i class="fa fa-key"></i> <span>'+user.fonction.routes[i].nom+'</span></a></li>';
                route += '<li class=""><a class="ecam-route" href="'+user.fonction.routes[i].path+'"><i class="fa fa-key"></i> <span>'+user.fonction.routes[i].nom+'</span></a></li>';
            }
        $('.sidebar-menu').html(route);
        
        $('.ecam-route').each(function(){
           $(this).attr('href', $(this).attr('href')+'?user='+user.matricule+"&page="+location.pathname); 
        });
        console.log("ws://"+location.host+"/user");
            var socket = new WebSocket("ws://"+location.host+"/user");
            console.log('<%=session.getId()%>');
            console.log(user.matricule);
            socket.onmessage = onMessage;
            socket.onopen    = onopen;
            
            function onMessage(event) {
                let message = JSON.parse(event.data);
                if(message.command === "off"){
                    logout();
                }else if(message.command === "block"){
                    block();
                }
            }
            function onopen(e){
                getIPs(function(ip){
                    socket.send(JSON.stringify({
                        user: user.matricule,
                        ip:   ip,
                        key: '<%=session.getId()%>',
                        message: 'init'
                    }));
                });
                
            }
        
    </script>