<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<style>.mesure-unite{font-size: 11px; margin-left: 3px; color: gray;}</style>

<table id="equipe-liste">
    <tbody></tbody>
</table>

<div id="form-add-billon">
    <p class="status-add-billon" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
        <div class="form-group">
          <label for="zone-ref" class="col-sm-2 control-label">Reférence</label>
          <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="zone-ref" placeholder="Reférence du Billon">
          </div>
        </div>
        <div class="form-group">
            <label for="zone-bille" class="col-sm-2 control-label">Longueur<span class="mesure-unite">(cm)</span></label>

          <div class="col-sm-10">
              <input class="form-control" type="number" min="0" style="width:100%;" id="zone-longueur">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-ptt-diam" class="col-sm-2 control-label">Ptt Diamètre<span class="mesure-unite">(cm)</span></label>
          <div class="col-sm-10">
              <input type="number" min="0" required="" class="form-control" id="zone-ptt-diam">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-gd-diam" class="col-sm-2 control-label">Grand Diamètre<span class="mesure-unite">(cm)</span></label>
          <div class="col-sm-10">
              <input class="form-control col-sm-8" id="zone-gd-diam" min="0" type="number">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-volume" class="col-sm-2 control-label">Volume<span class="mesure-unite">(cm³)</span></label>
          <div class="col-sm-10">
              <input type="text" class="form-control" readonly="" min="0" id="zone-volume">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-travail" class="col-sm-2 control-label">Travail</label>
          <div class="col-sm-10">
              <input type="text" class="form-control" style="width:100%;" id="zone-travail">
          </div>
        </div>
        <div class="form-group">
          <label for="zone-billonnage" class="col-sm-2 control-label">Billonnage</label>
          <div class="col-sm-10">
              <select type="text" class="form-control select2" style="width:100%;" id="zone-billonnage"></select>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-save-billon">Enregistrer</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>

    <script src="/resources/js/jquery.dataTables.min.js"></script>
    
    <script src="/resources/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">

    function getVolume(){
        var rayon = ((parseFloat($('#zone-ptt-diam').val())+parseFloat($('#zone-gd-diam').val()))/2);
            $('#zone-volume').val(Math.pow(rayon, 2)*Math.PI*parseFloat($('#zone-longueur').val()));
        }
        $('#zone-ptt-diam').change(()=>getVolume());
        $('#zone-gd-diam').change(function(){getVolume()});
        $('#zone-longueur').change(()=>getVolume());
        $('#zone-ref').val('REF_BLG_'+(new Date()).getTime()); 

        $.ajax({
            url: '/api/billonnage/list/reference',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                var data = JSON.parse(jqXHR.responseText);
                var select = document.getElementById('zone-parc');
                var selected=-1;
                for(var i=0; i<data.length; i++){
                    var opt = document.createElement("option");
                    opt.value = data[i].ref;
                    opt.innerHTML = data[i].libelle+' ('+data[i].refTypeparc.libelle+')';
                    select.appendChild(opt);
                }
                document.getElementById('zone-billonnage').selectedIndex = selected;
                $('select').select2();
            }
        });

    $("#btn-save-billon").click(function(){
            saveData($('#zone-ref').val(), $('#zone-longueur').val(), $('#zone-ptt-diam').val(), $("#zone-gd-diam").val(), $('#zone-volume').val(), $("#zone-travail").val(), $('#zone-billonnage').val());
            $('#form-add-billon .overlay').removeClass('hide');
            $('#form-add-billon #btn-save-billon').addClass("disabled");
        });

    </script>
</div>
