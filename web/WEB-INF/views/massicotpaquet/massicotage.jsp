<%-- 
    Document   : massicotpaquet-home
    Created on : 14 janv. 2019, 10:04:18
    Author     : KOUNOU BESSALA ERIC
--%>

<%-- 
    Document   : massicotpaquet-home
    Created on : 14 janv. 2019, 09:40:52
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ecam Web</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <%@include file="/WEB-INF/views/layouts/style.jsp" %>
          <link rel="stylesheet" href="/resources/css/select2/select2.min.css">
          <link rel="stylesheet" href="/resources/css/dataTables.bootstrap.min.css">  
        <style>
            .text-black{color: black;}
        </style>
        <script type="text/javascript">
            window.$$ = function(tag){return document.getElementById(tag);};
            $$.prototype.create = function(tag){return document.createElement(tag);};
        </script>
        
        <link href="/resources/css/handsontable/handsontable.min.css" rel="stylesheet" media="screen">

    </head>
    <!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
    -->
    <body class="hold-transition skin-blue sidebar-collapse">
    <div class="wrapper">

      <!-- Main Header -->
        <div class="nav bg-primary" style="width: 100%; height: 40px; position: fixed; z-index: 1000; ">
            <button class="btn btn-flat pull-left" style="background: transparent; color: white;" type="button" onclick="history.back();"><i class="fa fa-arrow-left"></i> Retour</button>
        </div>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Main content -->
        <section class="content container-fluid">
            <div class="row" style="margin-top: 50px; display: none;" id="section-visualisation">
                <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-info">
                        <!-- /.widget-user-image -->
                        <h4 class="widget-user-desc">Massicotage </h4>
                        <h4 class="btn-success pull-left" style="border-radius: 5px; padding: 10px; display: none;" id="zone-visualization-error"></h4>
                        <button type="type" class="btn btn-danger pull-right" id="btn-terminer" style="width: auto; height: 40px; border-radius: 20px; margin-right: 20px; margin-top: 3px;"><i class="fa fa-download"></i> Enregistrer</button>
                    </div><br>
                    <div class="box-footer no-padding" style="margin-top: 25px">
                        <ul class="nav nav-stacked" style="">
                            <li><div class="box box-default">
                                    <div class="box-header with-border">
                                      <h3 class="box-title">Visualisation des données</h3>

                                      <div class="box-tools pull-right">
                                          <button type="button" class="btn btn-flat" >
                                              <i class="fa fa-caret-down"></i>
                                              <span style="margin-left: 10px;" class="pull-right badge " id="vzone-nbr-detail"></span>
                                        </button>
                                      </div>
                                      <!-- /.box-tools -->
                                    </div>
                                    <!-- /.box-header -->
                                    <div id="vzone-detail" style="padding: 30px;" class="box-body">
                                        <table id="tableau-print-detail" class=" table table-bordered table-hover dataTable form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <thead>
                                                <tr>
                                                    <th class="sorting">Colis</th>
                                                    <th class="sorting">N° Paquet</th>
                                                    <th class="sorting">Nbr Feuilles</th>
                                                    <th class="sorting">Longueur</th>
                                                    <th class="sorting">Largeur</th>
                                                    <th class="sorting">Surface</th>
                                                </tr>
                                            </thead>
                                            <tbody id="table-massicotage-detail-commande"></tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </li>
                            <li><button type="button" class="btn btn-primary" id="btn-vider"  style="margin: 5px;"><span class="fa fa-trash"></span> Nettoyer</button></li>
                            <li><button type="button" id="btn-tableur_colis-unsee" class=" btn btn-flat pull-right" style="background-color: transparent; margin: 5px;"> Fermer</button></li>
                            <li><button type="button" class=" btn btn-danger bg-danger pull-left" id="btn-imprimer" style="margin: 5px;" ><span class="fa fa-print"></span> Imprimer </button></li>
                        </ul>
                    </div>
                </div>

            </div>
            
            <div class="row" style="margin-top: 50px" id="section-massicotage">
                <div class="row">
                    <div class="form-group col-sm-12" style=" text-align: center;">
                        <div class="col-sm-2  text-black" style="">
                            <label class="label col-sm-12 " style="color: black;">Travail</label>
                            <select class="select2" style="width:100%;" id="zone-bille"></select><br>
                            <div><legend>Liste des plots</legend><ul id="list-plots"></ul></div>
                        </div>
                        <div class="col-sm-2 text-black" style="text-align: center">
                            <label class="label col-sm-12 text-black">Essence</label>
                            <select class="select2" style="width: 100%" id="zone-essence"></select>
                        </div>
                        <div class="col-sm-2 text-black" style="ext-align: center">
                            <label class="label col-sm-12 text-black">Épaisseur</label>
                            <input type="text" class="form-control" pattern="[0-9]+(.[0-9])*" id="zone-epaisseur" placeholder="X/10">
                        </div>
                        <div class="col-sm-2 text-black" style="text-align: center">
                            <label class="label pull-left col-sm-12 text-black">Atelier</label>
                            <select id="zone-atelier" class="select2" style="width: 70%;">
                                <option value="">Selectionnez</option>
                                <option value="A">Atélier A</option>
                                <option value="B">Atélier B</option>
                            </select>
                            <button id="btn-load-colis" class="btn btn-primary" type="button" style="width: 40px; height: 30px;"><i class="fa fa-refresh"></i></button>
                        </div>
                        <div class="col-sm-2 text-black pull-right" style="text-align: center">
                        </div>
                        
                        <p class="text col-sm-3 bg-red-active pull-right" style="display: none; font-size: 16px;" id="zone-error"></p>
                    </div>
                </div>
                <div id="tableur" class="row col-sm-12" >
                    <div class="col-sm-5 pull-right">
                        <legend>Colis En Cours
                            <button class="btn btn-twitter pull-right" id="btn-tableur_colis-add-row" title="Ouvrir Colis" type="button" style="height: 30px; border-radius: 30px; margin-right: 20px; position: relative; top: -5px;"><i class="fa fa-plus"></i> Colis</button>
                            <button class="btn btn-twitter pull-right" id="btn-tableur_colis-see-row" title="Visualiser les données" type="button" style="height: 30px; border-radius: 30px; margin-right: 20px; position: relative; top: -5px;"><i class="fa fa-eye"></i> Visualisation</button>
                        </legend>
                        <div id="tableur-colis" class="col-sm-12" style="width: 100%; margin-right: 19px;"></div>
                    </div>
                    <div class="col-sm-7 pull-left">
                        <legend>Chargement de paquets
                            <button id="btn-tableur_paquet-transferer-row" class="btn btn-twitter pull-right" onclick="transfertPaquet()" type="button" style="height: 30px;  border-radius: 30px; margin-left: 20px;"><i class="fa fa-share"></i> Transferer </button>
                            <button id="btn-tableur_paquet-add-row" class="btn btn-twitter pull-right" type="button" style="height: 30px;  border-radius: 30px;"><i class="fa fa-plus"></i> Paquets</button>
                        </legend>
                        <div id="tableur-paquet" style="width: 100%; margin-right: 19px;"></div>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <%@include file="/WEB-INF/views/layouts/footer.jsp" %>

    </div>
    <!-- ./wrapper -->

        <%@include file="/WEB-INF/views/layouts/script.jsp" %>
    
        <script src="/resources/js/jquery.dataTables.min.js"></script>
        <script src="/resources/js/dataTables.bootstrap.min.js"></script>
        <script src="/resources/js/handsontable/handsontable.full.min.js"></script>
        <script src="/resources/js/select2/select2.full.min.js"></script>
        <script src="/resources/js/handsontable/SheetClip.js"></script>
        <script src="/resources/js/massicotpaquet/massicotpaquet.js"></script>
        <script type="text/javascript">
            
            document.getElementById("btn-imprimer").onclick = function(){
                tableToExcel('tableau-print-detail', 'W3C Example Table');
            };
        </script>
    </body>
</html>
