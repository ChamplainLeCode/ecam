<%-- 
    Document   : massicotpaquet-home
    Created on : 14 janv. 2019, 10:04:18
    Author     : KOUNOU BESSALA ERIC
--%>

<%-- 
    Document   : massicotpaquet-home
    Created on : 14 janv. 2019, 09:40:52
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Ecam Web</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

      <%@include file="/WEB-INF/views/layouts/style.jsp" %>
      <link rel="stylesheet" href="/resources/css/dataTables.bootstrap.min.css">    
      
    </head>
    <!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
    -->
    <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <%@include file="/WEB-INF/views/layouts/topBar.jsp" %>

      <!-- Left side column. contains the logo and sidebar -->
      <%@include file="/WEB-INF/views/layouts/sideBar.jsp" %>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Page Header
            <small>Optional description</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="">ecam</li>
            <li class="active">massicotpaquet</li>
          </ol>
          
          
        </section>

        <!-- Main content -->
        <section class="content container-fluid">

            <div class="row">
                <div class="modal fade modal-default" id="modal-default" style="display: none;">
                    <div class="modal-dialog" style="width: 90%;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close btn-flat" style="background-color: transparent;" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true" style="color: white">×</span></button>
                                <h4 class="modal-title">Ajouter de nouveaux paquets</h4>
                            </div>
                            <div class="modal-body" id="modal-body">

                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <div class="modal fade modal-default" id="modal-edit" style="display: none;">
                    <div class="modal-dialog"  style="width: 90%;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close btn-flat" style="background-color: transparent;" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true" style="color: white">×</span></button>
                                <h4 class="modal-title">Modifier les paquets</h4>
                            </div>
                            <div class="modal-body" id="modal-body">

                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <div class="modal modal-default" style="display: none" id="modal-view">
                    <div class="modal-dialog pull-right col-sm-pull-0" style="width: 100%">
                        <div class="modal-content " style=" background-color: transparent;">
                            <div class="modal-header">
                                <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-view-commande_client" class="hide" data-toggle="modal" data-target="#modal-view"></button>
                            </div>
                            <div class="modal-body" id="modal-body" style=" background-color: transparent ">
                                <div class="box box-widget widget-user-2">
                                        <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-navy">
                                        <!-- /.widget-user-image -->
                                        <h4 class="widget-user-desc">Opération: Massicot </h4>
                                        <h5 class="widget-user-desc" >Machine:  <span id="vzone-massicot" class="pull-right" ></span></h5>
                                        <h5 class="widget-user-desc">Plot:  <span  id="vzone-plot" class="pull-right" ></span></h5>
                                    </div>
                                    <div id="vzone-detail" style="padding: 30px;" class="box-body">
                                        <table id="tableau-print-detail" class=" table table-bordered table-hover dataTable form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <thead>
                                                <tr>
                                                    <th class="sorting">N° Paquet</th>
                                                    <th class="sorting">Longueur</th>
                                                    <th class="sorting">Largeur</th>
                                                    <th class="sorting">Épaisseur</th>
                                                    <th class="sorting">Surface</th>
                                                    <th class="sorting">Qualité</th>
                                                </tr>
                                            </thead>
                                            <tbody id="table-view-detail-reception"></tbody>
                                        </table>
                                    </div>
                                    
                                    <div class="box-footer no-padding" style="margin-top: 25px">
                                        <ul class="nav nav-stacked" style="">
                                            <li><button type="button" class=" btn btn-flat pull-right" style="background-color: transparent; margin: 5px;" data-dismiss="modal"> Fermer</button></li>
                                            <li><button type="button" id="btn-print-detail" class=" btn btn-bitbucket pull-left" style="margin: 5px;" > Imprimer </button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                
                
                <p class="status-del-massicotpaquet pull-left" style="padding: 5px; border-radius: 5px;"></p>

                <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-addMassicotPaquet" class="pull-right btn btn-default" data-toggle="modal" data-target="#modal-default">
                    <i class="fa fa-plus "></i> Ajouter les paquets
                </button>
                
                <a href="/massicotpaquet/massicotage"><button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-addMassicotPaquet" class="pull-right btn btn-default" data-toggle="modal" data-target="#modal-default">
                    <i class="fa fa-plus "></i> Massicotage
                </button></a>
                
                <button style="margin-right: 40px; margin-bottom: 20px;" type="button" id="btn-imprimer" class="pull-left btn btn-primary" data-toggle="modal" data-target="#modal-default">
                    <i class="fa fa-print "></i> Imprimer la liste
                </button>
                <label class="label label-info" style="display: none;" id="label-processus">Processus encours<i class="fa fa-spin fa-refresh"></i></label>
            </div>
            <div class="box">
            <div class="box-header">
              <h3 class="box-title">Liste du massicotpaquet</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div id="liste_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="liste" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="liste_info">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 79.75px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Plot</th>
                                        <!--th class="sorting" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 73.3333px;" aria-label="Browser: activate to sort column ascending">NumTravail</th-->
                                        <th class="sorting" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 86.5px;" aria-label="Platform(s): activate to sort column ascending">Massicot</th>
                                        <th class="sorting" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 86.5px;" aria-label="Platform(s): activate to sort column ascending">Paquet</th>
                                        <th class="sorting" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 86.5px;" aria-label="Platform(s): activate to sort column ascending">Face 1</th>
                                        <th class="sorting" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 86.5px;" aria-label="Platform(s): activate to sort column ascending">Face 2</th>
                                        <th class="sorting" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 86.5px;" aria-label="Platform(s): activate to sort column ascending">Face 3</th>
                                        <th class="sorting" tabindex="0" aria-controls="liste" rowspan="1" colspan="1" style="width: 86.5px;" aria-label="Platform(s): activate to sort column ascending">Opérations</th>
                                    </tr>
                                </thead>
                                <tbody id="table-body"></tbody>
                          </table>
                            
                        </div>
                    </div>
                </div>
            </div>
            </div>
            
        </section>
        
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <%@include file="/WEB-INF/views/layouts/footer.jsp" %>

    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->

    <%@include file="/WEB-INF/views/layouts/script.jsp" %>
    
    <script src="/resources/js/massicotpaquet/massicotpaquet-home.js"></script>
    <script src="/resources/js/jquery.dataTables.min.js"></script>
    <script src="/resources/js/dataTables.bootstrap.min.js"></script>
    <script>

    $(function () {
            loadData();
            document.getElementById("btn-imprimer").onclick = function(){
                tableToExcel('liste', 'W3C Example Table');
            };
            document.getElementById("btn-print-detail").onclick = function(){
                tableToExcel('tableau-print-detail', 'W3C Example Table');
            };
        });
    </script>
    </body>
</html>
