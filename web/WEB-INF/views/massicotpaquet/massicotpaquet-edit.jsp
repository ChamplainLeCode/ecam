<%-- 
    Document   : massicotpaquet-edit
    Created on : 14 janv. 2019, 09:40:28
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <link rel="stylesheet" href="/resources/css/select2/select2.min.css">

<div id="form-edit-massicotpaquet">
    <p class="status-edit-massicotpaquet" style="padding: 5px; border-radius: 5px;"></p>
    <form class="form-horizontal ">
      <div class="box-body">
            <div class="row">
                <div class="col-sm-3 pull-left">
                  <label for="zone-ref" class="col-sm-2 control-label">Plot</label>

                  <div class="col-sm-10">
                      <input type="text" value="<%=request.getParameter("ref")%>" id="zone-edit-plot" readonly="">
                  </div>
                </div>
                <div class="col-sm-3 pull-left">
                  <label for="zone-epaisseur" class="col-sm-2 control-label">Face</label>

                  <div class="col-sm-10">
                      <select class="select2"  style="width: 100%;" id="zone-edit-face">
                          <option value="Face1">Face 1</option>
                          <option value="Face2">Face 2</option>
                          <option value="Face3">Face 3</option>
                      </select>
                  </div>
                </div>
                <div class="col-sm-3 pull-left">
                    <button class="btn btn-bitbucket" id="btn-lookup" type="button">Charger <i class="fa fa-search"></i></button>
                </div>
            </div>
          
          <div class="row" style="margin-top: 50px;">
              <table id="table-edit-all" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <td>N° Paquet</td>
                            <td>Nombre Feuilles</td>
                            <td>Qualité</td>
                            <td>Opération</td>
                        </tr>
                    </thead>
                    <tbody id="table-edit"></tbody>
                </table>
            </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button"  data-dismiss="modal" id="btn-close-editPane" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-edit-massicotpaquet"><i class="fa fa-pencil"></i> Modifier</button>
            <div class="overlay hide" style="text-align:center;">
                Veuillez patienter s'il vous plaît  <i class="fa fa-refresh fa-spin"></i>
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        $("#btn-edit-massicotpaquet").click(function(){
            editData($('#zone-ref').val(), $('#zone-numTravail').val(), $('#zone-massicot').val(), $('#zone-paquet').val());
            $('#form-edit-massicotpaquet .overlay').removeClass('hide');
            $('#form-edit-massicotpaquet #btn-edit-massicotpaquet').addClass("disabled");
        });
        
        $('#btn-lookup').click(function(){
           $.ajax({
                url: '/api/massicot_paquet/paquet',
                method: 'GET',
                data: {
                    plot: $('#zone-edit-plot').val(),
                    face: $('#zone-edit-face').val()
                },
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    var data = jqXHR.responseJSON;
                    var table = $(document.getElementById("table-edit"));
                    table.children("tr").remove();
                    for(var i=0; i<data.length; i++){
                        /*
                        table.innerHTML += '<tr>\
                            <td><label id="zone-paquet-">'+data[i][0]+'</label></td>\
                            <td><input type="number" min="1" max="32" class="form-control" id="zone-nombreFeuilles-'+i+'"</td>\
                            <td><input type="text" class="form-control" id="zone-qualite-'+i+'"></td>\
                            <td><button type="button" class="btn btn-success" id="btn-valider-'+i+'"><i class="fa fa-check"></i></button></td>\
                        </tr>';*/
            
                        var tr = $(document.createElement("tr"));
                        var num = $(document.createElement("td")),
                            nbrF = $(document.createElement("td")), 
                            qual = $(document.createElement("td")),
                            feuil = $(document.createElement("input")),
                            qualite = $(document.createElement("input")),
                            btn = $(document.createElement("button")),
                            icon = $(document.createElement("i"));
                    
                            // Numéro du paquet
                            num.html(data[i][0]);
                            // Nombre de feuilles restantes
                            feuil.val(data[i][1]);
                            feuil.attr("type","number");
                            feuil.id = "zone-feuille-"+i;
                            
                            qualite.attr('class', 'form-control');
                            feuil.attr("class", "form-control");
                            qualite.attr("type", 'text');
                            qualite.attr('id', 'zone-qualite-'+i);

                            nbrF.html("<input type=\"text\" id=\"zone-feuil-"+i+"\" placeholder=\"Feuilles/Paquet\" class=\"form-control\">");
                            qual.html("<input type=\"text\" id=\"zone-qual-"+i+"\" placeholder=\"Qualite\" class=\"form-control\">");
                            
                            tr.append(num);
                            tr.append(nbrF);
                            tr.append(qual);
                            tr.append(btn);
                            
                            table.append(tr);

                            btn.attr("data", i);
                            btn.attr("ref", data[i][0]);
                            btn.attr("type", "button");
                            btn.attr("class","btn btn-success");
                            btn.css("margin-left", "10px");
                                icon.attr('class', "fa fa-check");
                            btn.append(icon);
                            btn.click(function (){
                                
                                if($('#zone-qual-'+$(this).attr("data")).val() === ''){
                                    alert('Précisez la qualité');
                                    return;
                                }
                                
                                if($('#zone-feuil-'+$(this).attr("data")).val() === ''){
                                    alert('Précisez le nombre de feuilles/paquet');
                                    return;
                                }
                                console.log('Qualiteé = "'+$('#zone-qual-'+$(this).attr("data")).val()+'"');
                                console.log('feuille = "'+$('#zone-feuil-'+$(this).attr("data")).val()+'"');
                                $.ajax({
                                    url: '/massicot_paquet/update_lock',
                                    method: 'POST',
                                    data: {
                                        face: $('#zone-edit-face').val(),
                                        feuille: $('#zone-feuil-'+$(this).attr("data")).val(),
                                        qualite: $('#zone-qual-'+$(this).attr("data")).val(),
                                        ref: $(this).attr("ref")
                                    },
                                    dataType: 'json',
                                    complete: function (jqXHR, textStatus ) {
                                        tr.html('');
                                    }
                                });
                            });
                            
                    }
                    
                   $('#table-edit-all').DataTable();;
                }
                
           });
           
        });
        
        $('select').select2();
    </script>
</div>
