<%-- 
    Document   : massicotpaquet-add
    Created on : 14 janv. 2019, 09:40:04
    Author     : KOUNOU BESSALA ERIC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="/resources/css/select2/select2.min.css">
<div id="form-edit-massicotpaquet" class="box box-default">
    <H3 class="status-add-massicotpaquet label-danger" style="padding: 5px; border-radius: 5px;"></H3>
    <form class="form-horizontal ">
      <div class="box-body">
        <table class="table table-striped table-bordered">
            <thead>
                    <td>
                        <label class="col-sm-12">Travail</label>
                    </td>
                    <td>
                        <label class="col-sm-12">Valeur</label>
                    </td>
                    <td>
                        <label class="col-sm-12" style="margin-bottom: 10px;">Massicot</label>
                    </td>
                    <td>
                        <label class="col-sm-12" style="margin-bottom: 10px; text-align: center;">Nombre de feuilles<br>/Paquet</label>
                    </td>
                    <td>
                        <label class="col-sm-12" style="margin-bottom: 10px;">Longueur <span class="measure-unit">(cm)</span></label>
                    </td>
                    <td>
                        <label class="col-sm-12" style="margin-bottom: 10px;">Largueur <span class="measure-unit">(cm)</span></label>
                    </td>
                    <td>
                        <label class="col-sm-12" style="margin-bottom: 10px;">Epaisseur <span class="measure-unit">(mm)</span></label>
                    </td>
                    <td>
                        <label class="col-sm-12" style="margin-bottom: 10px;">Qualité</label>
                    </td>
            </thead>
            <tbody id="table-item"></tbody>
            <tfoot>
                <tr>
                    <td colspan="8">
                        <button type="button" id="btn-add-row" class="pull-right btn btn-bitbucket" style="width: 40px; border-radius: 20px;">
                            <i class="fa fa-plus"></i>
                        </button>
                    </td>
                </tr>
            </tfoot>
        </table>
          
          
      </div>
          
        <!-- /.box-body -->
      <div class="box-footer" style="background-color: transparent">
            <button type="button" id="btn-close-addPane" data-dismiss="modal" class="btn btn-default">Fermer</button>
            <button type="button" class="btn btn-info pull-right" id="btn-add-massicotpaquet"><i class="fa fa-pencil"></i> Ajouter</button>
            <div class="overlay hide" style="text-align:center;">
                <h1><i class="fa fa-refresh fa-spin"></i> </h1> 
                <h1>Veuillez patienter s'il vous plaît </h1> 
            </div>
      </div>
      <!-- /.box-footer -->
    </form>
          <script src="/resources/js/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        
        $('#btn-close-addPane').click(function(){
            $('#label-processus').show();
            setTimeout(function(){
               loadData();
               $('#label-processus').hide();
            }, 15000);
           
        });
        var index = 0;
        $('#btn-add-row').click(function(){
            var div = document.createElement("tr"),
                type = document.createElement("select"),
                billon = document.createElement("select"),
                plot = document.createElement("select"),
                travail = document.createElement("input"),
                massicot = document.createElement("select"),
                nbrFPaq  = document.createElement("input"),
                longueur  = document.createElement("input"),
                largeur  = document.createElement("input"),
                epaisseur  = document.createElement("input"),
                qualite  = document.createElement("input"),
                opt = document.createElement("option");
                
                type.id = "zone-type-"+index;
                type.setAttribute("data", index);

                billon.id = "zone-billon-"+index;
                plot.id = "zone-plot-"+index;
                travail.id = "zone-travail-"+index;
                massicot.id = "zone-massicot-"+index;
                nbrFPaq.id = "zone-nbrFPaq-"+index;
                longueur.id = "zone-longueur-"+index;
                largeur.id = "zone-largeur-"+index;
                epaisseur.id = "zone-epaisseur-"+index;
                qualite.id = "zone-qualite-"+index;
                
                index++;
                
                epaisseur.setAttribute("readonly","");
                travail.placeholder = "N° travail";
                
                opt.value = "travail";
                opt.innerText = "Travail";
                type.appendChild(opt);
                opt = document.createElement("option");
                opt.value = "billon";
                opt.innerText = "Billon";
                type.appendChild(opt);
                
                 
                billon.style = "width: 100%; margin-bottom: 10px;";
                travail.style = "margin-bottom: 10px;";
                plot.style = "width: 100%; margin-top: 10px;";
                massicot.style = "width: 100%;";
                
                
                travail.className = "form-control";
                nbrFPaq.className = "form-control";
                longueur.className = "form-control";
                largeur.className = "form-control";
                epaisseur.className = "form-control";
                qualite.className = "form-control";
                type.className = "select2 item";
                billon.className = "select";
                plot.className = "select2";
                massicot.className = "select2";
                
                longueur.type = largeur.type = nbrFPaq.type = "number";
                longueur.min = largeur.min = nbrFPaq.min = 1;
                nbrFPaq.max = 32;
                longueur.value = largeur.value = nbrFPaq.value = 1;
                
                var td = document.createElement("td");
                td.appendChild(type);
                div.appendChild(td);
                td = document.createElement("td");
                td.appendChild(travail);
                td.appendChild(billon);
                td.appendChild(plot);
                div.appendChild(td);
                 td = document.createElement("td");
                td.appendChild(massicot);
                div.appendChild(td);
                 td = document.createElement("td");
                td.appendChild(nbrFPaq);
                div.appendChild(td);
                 td = document.createElement("td");
                td.appendChild(longueur);
                div.appendChild(td);
                 td = document.createElement("td");
                td.appendChild(largeur);
                div.appendChild(td);
                 td = document.createElement("td");
                td.appendChild(epaisseur);
                div.appendChild(td);
                 td = document.createElement("td");
                td.appendChild(qualite);
                div.appendChild(td);
           $('#table-item').append($(div));
           
           $(type).change(function(){
                if((this.selectedIndex+1) === 2){
                    $(billon).show();
                    $(plot).show();
                    $(travail).hide();
                     setBillon(billon);
                }else{
                    $(travail).show();
                    $(plot).show();
                    $(billon).hide();
                }
            })
            .select2();
            
            $(plot).change(function(){
                /**
                 * Quand on modifie le plot selectionné on récupère automatiquement l'épaisseur appliquée à la tranche sur ce plot
                 * @type void
                 */    
                $.ajax({
                    url: '/api/tranche/epaisseur_for_plot',
                    data: {plot: this.value},
                    dataType: 'json',
                    method: 'GET',
                    async: false,
                    complete: function (jqXHR, textStatus ) {
                        $(epaisseur).val(jqXHR.responseJSON[0]);
                    }
                });
            })
            .select2();
    
            $(travail)
                .focusout(() => setPlot(null, plot, travail.value))
                .show();
                
            $(billon)
                .change(function(){setPlot($(this).val(), plot);})
                .hide()
                .select();
        
            setMassicot(massicot);
    
            $(billon).hide();
            $(plot).hide();

        });
        
        
        $("#btn-add-massicotpaquet").click(function(){
            var list = [];
            var j = 0;
            $('.item').each(function(e){
                
                var id = $(this).attr("data");
                var data = {
                    type: $('#zone-type-'+id).val(),
                    typeValue: ($('#zone-type-'+id).val() === 'billon' ? $('#zone-billon-'+id).val() : $('#zone-travail-'+id).val()),
                    massicot: $('#zone-massicot-'+id).val(),
                    nbrFeuilles: $('#zone-nbrFPaq-'+id).val(),
                   // nbrFeuillesDefectueuses: $('#zone-nbrFeuilles-defectueuses').val(),
                    longueur: $('#zone-longueur-'+id).val(),
                    largeur: $('#zone-largeur-'+id).val(),
                    epaisseur: $('#zone-epaisseur-'+id).val(),
                    qualite: $('#zone-qualite-'+id).val(),
                    // Valeur du plot dont on souhait proceder à l'empaquetage des feuilles
                    plot: ($('#zone-plot-'+id).val() === '' || $('#zone-plot-'+id).val() === null ? setError($('#zone-plot-'+id)) : $('#zone-plot-'+id).val())
                };
                if(data.plot === ''){
                    return setError($('#zone-plot-'+id));
                }
                list[j++] = data;
            });
            
            saveData(list);
            $('#form-edit-massicotpaquet .overlay').removeClass('hide');
            $('#form-edit-massicotpaquet #btn-add-massicotpaquet').addClass("hide");
        });

        $('.status-add-massicotpaquet').hide();
        function setError(plotZone){
            console.log("Veuillez selectionner un plot");
            $('.status-add-massicotpaquet').html("Veuillez selectionner les plots pour toutes lignes").show();
            return '';
        }

        function setPlot(numBillon = null, plot = null, numTravail = null){
            $.ajax({
                url: '/api/plot/plot_for_massicot',
                method: 'GET',
                data: {
                    type: (numBillon === null ? 'travail' : 'billon'),
                    value: (numBillon === null ? numTravail :  numBillon)
                },
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    var select = plot;
                    var data = jqXHR.responseJSON;
                    var opt = document.createElement("option");
                    select.innerHTML = '';
                    opt.value = "";
                    opt.innerText = "Selectionnez le plot";
                    select.appendChild(opt);
                    
                    for(var i = 0; i<data.length; i++){
                        opt = document.createElement("option");
                        opt.value = data[i];
                        opt.innerText = data[i];
                        select.appendChild(opt);
                    }
                    
                    $(select).show();
                }
            });
        }


        function setBillon(billon = null){
            $.ajax({
                url: '/api/billon/billon_for_massicot',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    var select = (billon === null ? document.getElementById("zone-objet-travail-2") : billon);
                    var data = jqXHR.responseJSON;
                    select.innerHTML = '';
                    var opt = document.createElement("option");
                    opt.value = "";
                    opt.innerText = "Selectionnez le billon";
                    select.appendChild(opt);

                    for(var i = 0; i<data.length; i++){
                        opt = document.createElement("option");
                        opt.value = data[i];
                        opt.innerText = data[i];
                        select.appendChild(opt);
                    }
                }
            });
        }
        function setMassicot(massicot = null){
            $.ajax({
                url: '/api/massicot/list',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    var select = massicot === null ? document.getElementById("zone-massicot") : massicot;
                    var data = jqXHR.responseJSON;
                    var opt = document.createElement("option");
                    opt.value = "";
                    opt.innerText = "Selectionnez le massicot";
                    select.appendChild(opt);

                    for(var i = 0; i<data.length; i++){
                        opt = document.createElement("option");
                        opt.value = data[i].ref;
                        opt.innerText = data[i].libelle;
                        select.appendChild(opt);
                    }
                }
            });
        }
        $('select[name="zone-billon"]');
        
        //$('select').select2();
    </script>
</div>
