<%-- 
    Document   : fiche_production
    Created on : 7 juil. 2019, 08:36:38
    Author     : champlain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Fiche Production</title>
        
        <link rel="stylesheet" href="/resources/css/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="/resources/css/bootstrap/dist/css/bootstrap.min.css">
        <style>
            @media print{
                .no-print{
                    display: none;
                }
            }
            @page{
                margin: 0cm;
                padding: 1cm;
            }
            body{
                padding: 1cm;
                font-size: 7pt;
            }
        </style>
    </head>
    <body>
        
        <div class=WordSection1>
            <button class="btn btn-primary no-print" onclick="window.print();"><i class="fa fa-print"></i> Imprimer</button>
        <p class=MsoNormal>
            <b style='mso-bidi-font-weight:normal'>
                <span class="text-capitalize" style='font-size:10.0pt;line-height:107%;font-family:"Times New Roman","serif"'>PRODUCTION
                    <span class="">  </span>MASSICOT DU&nbsp;:<input type="date" id="zone-date" style="border:none;">
                </span>
            </b>
        </p>

        <p class="label label-default"><b style='mso-bidi-font-weight:normal'><span
        style='font-size:7.0pt;line-height:107%;font-family:"Times New Roman","serif"'>MASSICOT<span
        style='mso-spacerun:yes'>    </span>A<span style='mso-spacerun:yes'>       
        </span></span></b></p>
        
        <table style="margin-top: 10pt;" class="table table-bordered  table-responsive table-striped">
            <thead>
                <tr>
                    <th>N° Colis</th>
                    <th>[Code] - Essence [Qualité , Épaisseur]</th>
                    <th>N° Travail</th>
                    <th>Surface[m²]</th>
                    <th>Notes</th>
                </tr>
            </thead>
            <tbody id="tabA"></tbody>
        </table>

        <p class="label label-default"><b style='mso-bidi-font-weight:normal'><span
        style='font-size:7.0pt;line-height:107%;font-family:"Times New Roman","serif"'>MASSICOT<span
        style='mso-spacerun:yes'>    </span>B<span style='mso-spacerun:yes'>       
        </span></span></b></p>
        
        <table style="margin-top: 10px;" class="table table-bordered  table-responsive table-striped">
            <thead>
                <tr>
                    <th>N° Colis</th>
                    <th>[Code] - Essence [Qualité , Épaisseur]</th>
                    <th>N° Travail</th>
                    <th>Surface[m²]</th>
                    <th>Notes</th>
                </tr>
            </thead>
            <tbody id="tabB"></tbody>
        </table>
        

        <p class="label label-default"><b style='mso-bidi-font-weight:normal'><span
        style='font-size:7.0pt;line-height:107%;font-family:"Times New Roman","serif"'>Jointage<span
        style='mso-spacerun:yes'>    </span></span></b></p>
        
        <table style="margin-top: 10px;" class="table table-bordered  table-responsive table-striped">
            <thead>
                <tr>
                    <th width="15%">N° Colis Final</th>
                    <th>Nbr Feuil</th>
                    <th>Longeur</th>
                    <th>Largeur</th>
                    <th>Epaisseur</th>
                </tr>
            </thead>
            <tbody id="tabJ"></tbody>
        </table>

        </div>
        
        <script src="/resources/js/my/jquery.min.js"></script>
        <script src="/resources/css/bootstrap/dist/js/bootstrap.min.js"></script>
        
        <script type="text/javascript">
            function parseDate(dateStringify = (new Date().getYear()+1990)+"-"+new Date().getMonth()+"-"+new Date().getDate()){
                var date = dateStringify.split('-');
                // //console.log(date);
                return new Date(parseInt(date[0]), parseInt(date[1])-1, parseInt(date[2]));
            }
            function loadDataFor(date){
                //alert(new Date(date));
                $.ajax({
                    url: '/api/jointage/list/fiche',
                    data: {date: date},
                    dataType: 'json',
                    complete: function (jqXHR, textStatus ) {
                        let donnees = jqXHR.responseJSON;
                        let tab = document.getElementById("tabJ");
                        tab.innerHTML = '';
                        for(let data in donnees){
                            let jointage = JSON.parse(donnees[data].jointage);
                            let tr = document.createElement("tr");
                                let td = document.createElement("td");
                                td.innerHTML = jointage.colisFinal.refColis;
                            tr.appendChild(td);
                                td = document.createElement("td");
//                                td.setAttribute("colspan", "4");
                                //td.style.textAlign = "center";
                                td.innerHTML = jointage.nbrFeuille;
                            tr.appendChild(td);
                                td = document.createElement("td");
//                                td.setAttribute("colspan", "4");
                                //td.style.textAlign = "center";
                                td.innerHTML = jointage.longueur;
                            tr.appendChild(td);
                                td = document.createElement("td");
//                                td.setAttribute("colspan", "4");
                                //td.style.textAlign = "center";
                                td.innerHTML = jointage.largeur;
                            tr.appendChild(td);
                                td = document.createElement("td");
//                                td.setAttribute("colspan", "4");
                                //td.style.textAlign = "center";
                                td.innerHTML = jointage.colisFinal.epaisseur;
                            tr.appendChild(td);
                            tab.appendChild(tr);
                        }
                    }
                });
                
                $.ajax({
                    url: '/api/massicotage/fiche',
                    data: {date: date},
                    method: 'POST',
                    dataType: 'json',
                    complete: function (jqXHR, textStatus ) {
                        let donnees = jqXHR.responseJSON;
                        let tabA = document.getElementById("tabA");
                        let tabB = document.getElementById("tabB");
                        tabA.innerHTML = '';
                        tabB.innerHTML = '';
                        for(let data in donnees){
                            let tab = (donnees[data].colis.split('-')[1].charAt(0) === 'A' ? tabA : tabB);
                            let tr = document.createElement("tr");
                                let td = document.createElement("td");
                                td.innerHTML = donnees[data].data[0].colis;
                            tr.appendChild(td);
                                td = document.createElement("td");
//                                td.setAttribute("colspan", "4");
                                //td.style.textAlign = "center";
                                td.innerHTML = donnees[data].data[0].essence;
                            tr.appendChild(td);
                                td = document.createElement("td");
                                for(let j=0; j<donnees[data].data.length; j++){
                                    td.innerHTML += donnees[data].data[j].bille+", ";
                                }
                            tr.appendChild(td);
                                td = document.createElement("td");
                                td.innerHTML = donnees[data].data[0].surface;
                            tr.appendChild(td);
                                td = document.createElement("td");
                                td.innerHTML = 'Ce colis a produit des paquets dont la longueur max est de '+donnees[data].data[0].longueur+' cm et la largeur min est de '+donnees[data].data[0].largeur+' cm';//nees[data].data[0].surface;
                            tr.appendChild(td);
                            tab.appendChild(tr);
                        }
                    }
                });
            }
            
            $('#zone-date').change(function(){
               let date = parseDate($(this).val());
               loadDataFor(date.getTime());
            });
            
            
        </script>
    </body>
</html>
