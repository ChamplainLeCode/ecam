
/* global user */

$(function(){
   
   $('#btn-addRoute').click(function(){
      
       
       $.ajax({
            url: "/access/add",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                $('#modal-default #modal-body').html(data);
           },
            error: function (jqXHR, textStatus, errorThrown) {
                 $('#modal-default #modal-body').html(jqXHR.responseText);
            }
       });
   });
    if(user.can_add !== 'O')       
        $('#btn-addRoute').remove();
   
   
   
    function getTableRow(dataRow){
        var row = document.createElement("tr"),
                
        celPrivilege = document.createElement("td"),
        celRoutes = document.createElement("td"),
        celOpe = document.createElement("td"),
        
        btnDel = document.createElement("button"),
        icon = document.createElement("i");
        
        celPrivilege.innerHTML = dataRow.libelle;
        celRoutes.innerHTML = '';
        for(var i=0; i<dataRow.routes.length; i++){
            celRoutes.innerHTML += "<label>"+dataRow.routes[i].path+'</label><br>';
        }
        btnDel.type = "button";
        btnDel.className = "btn btn-warning";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.matricule;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        
        

        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        row.appendChild(celPrivilege);
        row.appendChild(celRoutes);
        row.appendChild(celOpe);
        return row;
    }

    window.deleteData = function(data, source){
        $.ajax({
            url: "/access/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            complete: function (jqXHR, textStatus) {
                loadData();
            }
        });
    };
    


window.loadData = function (){
        $("#table-body").html('');
        $.ajax({
            url: "/api/acces/list",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                
                for(var i=0; i<data.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(data[i]));
                }
                            $('table').DataTable();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("processus erroné");
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
                console.log("Le processus est terminé");
            }
        });
    };
    
window.saveData = function(privilege, route){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return ;
     $.ajax({
            url: "/access/add",
            method: "POST",
            data: {
                privilege: privilege,
                route: route
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-addPane').click();
                loadData();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            complete: function (jqXHR, textStatus) {
                console.log("terminé");
            }
        });
};

    
window.editData = function(nom, path){
     $.ajax({
            url: "/acces/edit",
            method: "POST",
            data: {
                nom: nom,
                path: path
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-editPane').click();
                loadData();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
};

});