
/* global user */

$(function(){
    window.$ = $;
    window.saveData = function saveData(libelle, ref){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return ;
        $.ajax({
            data: {
                ref: ref,
                libelle: libelle
            },
            dataType: 'json',
            url: "/classification/add",
            method: 'POST',
            success: function (data, textStatus, jqXHR) {
                // console.log(data);
                // console.log("looo");
                if(data.resultat == "true"){
                    $('#form-add-classification .overlay').addClass('hide');
                    $('#form-add-classification #btn-save-classification').removeClass("disabled");
                    $('.status-add-classification').removeClass('bg-red .disabled').addClass('bg-green disabled').html('Enregistré');
                    $('#form-add-classification #zone-ref').val('REF_CLASS_'+(new Date()).getTime());
                    $('#form-add-classification #zone-libelle').val('');
                    loadData();
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat == "false"){
                    $('#form-add-classification .overlay').addClass('hide');
                    $('#form-add-classification #btn-save-classification').removeClass("disabled");
                    $('.status-add-classification').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason);
                }else{
                    this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };

    $('#btn-addClassification').click(function(e){
        $.ajax({
            url: "/classification/add",
            method: "GET",
            success: function (data, textStatus, jqXHR) {
                $('#modal-body').html(data);
            },
            complete: function (jqXHR, textStatus ) {
            }
        });
    });
    if(user.can_add !== 'O')       
        $('#btn-addClassification').remove();
   
   
    
    function getTableRow(dataRow){
        var row = document.createElement("tr"),
        celRef = document.createElement("td"),
        celLib = document.createElement("td"),
        celOpe = document.createElement("td"),
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        icon = document.createElement("i");

        celRef.innerHTML = dataRow.ref;
        celLib.innerHTML = dataRow.libelle;
        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.ref;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete '+dataRow.libelle;
        
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-pencil";
        btnEdi.className = "btn btn-success btn-edit-classification";
        btnEdi.title = 'Modifier '+dataRow.libelle;
        btnEdi.appendChild(icon);
        btnEdi.onclick = function(){
            // console.log('data');
            // console.log(dataRow);
            $.ajax({
                url: "/classification/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        

        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
                row.appendChild(celRef);
        row.appendChild(celLib);
        row.appendChild(celOpe);
        return row;
    }
    
    window.loadData = function(){
        $('#table-body').html('');
        $.ajax({
            url: "/api/classification/list",
            method: "GET",
            success: function (donnees, textStatus, jqXHR) {
                // // console.log(donnees);
                for(var i=0; i<donnees.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(donnees[i]));
                }
            },
            complete: function (jqXHR, textStatus ) {
                $('#liste').DataTable();
                $(".btn-edit-classification").attr('data-toggle',"modal").attr('data-target',"#modal-edit");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                for(var i = 0; i<20; i++){
                    document.getElementById("table-body").appendChild(getTableRow({ref: 'Ref '+i, libelle: 'Libelle'+i}));
                }
            }
        });
    };
    window.deleteData = function(data, source){
        $.ajax({
            url: "/classification/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                // console.log("looo");
                if(data.resultat === "true"){
                    $('.status-del-classification').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-classification').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                    loadData();
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('.status-del-classification').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-classification').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    }
    
    window.editData = function(libelle, ref){
        $.ajax({
            url: "/classification/edit",
            method: "POST",
            data: {ref: ref, libelle: libelle},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat == "true"){
                    $('#form-edit-classification .overlay').addClass('hide');
                    $('#form-edit-classification #btn-edit-classification').removeClass("disabled");
                    $('.status-edit-classification').removeClass('bg-red .disabled').addClass('bg-green disabled').html('Modifié');
                    loadData();
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat == "false"){
                    $('#form-edit-classification .overlay').addClass('hide');
                    $('#form-edit-classification #btn-edit-classification').removeClass("disabled");
                    $('.status-edit-classification').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    }
});

