
/* global user */

$(function(){
    window.$ = $;
    window.saveData = function saveData(epaisseur, ref, essence){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return ;
        $.ajax({
            data: {
                ref: ref,
                epaisseur: epaisseur,
                essence: essence
            },
            dataType: 'json',
            url: "/classification-ess/add",
            method: 'POST',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat == "true"){
                    $('#form-add-classification .overlay').addClass('hide');
                    $('#form-add-classification #btn-save-classification').removeClass("disabled");
                    $('.status-add-classification').removeClass('bg-red .disabled').addClass('bg-green disabled').html('Enregistré');
                    $('#form-add-classification #zone-ref').val('REF_CLASS_'+(new Date()).getTime());
                    $('#form-add-classification #zone-libelle').val('');
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat == "false"){
                    $('#form-add-classification .overlay').addClass('hide');
                    $('#form-add-classification #btn-save-classification').removeClass("disabled");
                    $('.status-add-classification').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason);
                }else{
                    this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };

    $('#btn-addClassification').click(function(e){
        $.ajax({
            url: "/classification-ess/add",
            method: "GET",
            success: function (data, textStatus, jqXHR) {
                $('#modal-body').html(data);
            },
            complete: function (jqXHR, textStatus ) {
            }
        });
    });
    if(user.can_add !== 'O')       
        $('#btn-addClassification').remove();
   
   
    
    function getTableRow(dataRow){
        var row = document.createElement("tr"),
        celRef = document.createElement("td"),
        celLib = document.createElement("td"),
        celEss = document.createElement("td"),
        celOpe = document.createElement("td"),
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        icon = document.createElement("i");

        celRef.innerHTML = dataRow.ref;
        celLib.innerHTML = dataRow.epaisseur;
        celEss.innerHTML = dataRow.essence.libelle;
        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.ref;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-pencil";
        btnEdi.className = "btn btn-success btn-edit-classification";
        btnEdi.title = 'Modifier ';
        btnEdi.appendChild(icon);
        btnEdi.onclick = function(){
            $.ajax({
                url: "/classification_ess/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
//        console.log(dataRow);
  //      celEss.innerHTML = JSON.parse(dataRow.essence).libelle;
        

        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
                row.appendChild(celRef);
        row.appendChild(celLib);
        row.appendChild(celEss);
        row.appendChild(celOpe);
        return row;
    }
    
    window.loadData = function(){
        $('#table-body').html('');
        $.ajax({
            url: "/api/classification_ess/list/page",
            dataType: 'json',
            data: {page: 0},
            method: "GET",
            success: function (donnees, textStatus, jqXHR) {
                var data = (donnees);
                for(var i=0; i<data.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(data[i]));
                }
            },
            complete: function (jqXHR, textStatus ) {
                $('#liste').DataTable();
                $(".btn-edit-classification").attr('data-toggle',"modal").attr('data-target',"#modal-edit");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Couldn't load data for classification essence");
            }
        });
    };
    window.deleteData = function(data, source){
        alert(JSON.stringify(data));
        $.ajax({
            url: "/classification-ess/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('.status-del-classification').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-classification').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                    console.log(source.parentNode.parentNode);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('.status-del-classification').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-classification').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    
    window.editData = function(epaisseur, ref, essence){
        $.ajax({
            url: "/classification-ess/edit",
            method: "POST",
            data: {ref: ref, epaisseur: epaisseur, essenceRef: essence},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat == "true"){
                    $('#form-edit-classification .overlay').addClass('hide');
                    $('#form-edit-classification #btn-edit-classification').removeClass("disabled");
                    $('.status-edit-classification').removeClass('bg-red .disabled').addClass('bg-green disabled').html('Modifié');
                    loadData();
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat == "false"){
                    $('#form-edit-classification .overlay').addClass('hide');
                    $('#form-edit-classification #btn-edit-classification').removeClass("disabled");
                    $('.status-edit-classification').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
});

