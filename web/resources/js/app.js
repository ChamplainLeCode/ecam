    window.getIPs = function(callback) {
        var ip_dups = {};

        //compatibility for firefox and chrome
        var RTCPeerConnection = window.RTCPeerConnection
          || window.mozRTCPeerConnection
          || window.webkitRTCPeerConnection;
        var useWebKit = !!window.webkitRTCPeerConnection;

        //bypass naive webrtc blocking using an iframe
        if(!RTCPeerConnection){
          //NOTE: you need to have an iframe in the page right above the script tag
          //
          //<iframe id="iframe" sandbox="allow-same-origin" style="display: none"></iframe>
          //<script>...getIPs called in here...
          //
          var win = iframe.contentWindow;
          RTCPeerConnection = win.RTCPeerConnection
            || win.mozRTCPeerConnection
            || win.webkitRTCPeerConnection;
          useWebKit = !!win.webkitRTCPeerConnection;
        }

        //minimal requirements for data connection
        var mediaConstraints = {
          optional: [{RtpDataChannels: true}]
        };

        //firefox already has a default stun server in about:config
        //    media.peerconnection.default_iceservers =
        //    [{"url": "stun:stun.services.mozilla.com"}]
        var servers = undefined;

        //add same stun server for chrome
        if(useWebKit)
          servers = {iceServers: [{urls: "stun:stun.services.mozilla.com"}]};

        //construct a new RTCPeerConnection
        var pc = new RTCPeerConnection(servers, mediaConstraints);

        function handleCandidate(candidate){
          //match just the IP address
          var ip_regex = /([0-9]{1,3}(\.[0-9]{1,3}){3})/
          var ip_addr = ip_regex.exec(candidate)[1];

          //remove duplicates
          if(ip_dups[ip_addr] === undefined)
              callback(ip_addr);

          ip_dups[ip_addr] = true;
        }

        //listen for candidate events
        pc.onicecandidate = function(ice){
          //skip non-candidate events
          if(ice.candidate)
            handleCandidate(ice.candidate.candidate);
        };

        //create a bogus data channel
        pc.createDataChannel("");

        //create an offer sdp
        pc.createOffer(function(result){
          //trigger the stun server request
          pc.setLocalDescription(result, function(){}, function(){});
        }, function(){});

        //wait for a while to let everything done
        setTimeout(function(){
          //read candidate info from local description
          var lines = pc.localDescription.sdp.split('\n');
          lines.forEach(function(line){
            if(line.indexOf('a=candidate:') === 0) {
              handleCandidate(line);
            }
          });
        }, 1000);
      };

        /** localStorage.setItem('user', JSON.stringify({
    name: 'Champlain Marius',
    privilege: 'Receptioniste',
    ref: "REF_FOUR_1203923",
    photo: '/resources/image/user2-160x160.jpg'
}));
*/
// global
window.user = JSON.parse(localStorage.getItem('user'));
window.lockscreen = sessionStorage.getItem('lockscreen');


if((user === null || user === undefined || user.matricule === undefined || lockscreen === undefined) || user.matricule !== lockscreen)
    location.href = '/login';
    
var queryID = location.search.split('&')[0].split('=')[1];
    if(queryID !== lockscreen)
        location.href = '/login';
//if(user.matricule !== location.search)

          var tableToExcel = (function() {
             var uri = 'data:application/vnd.ms-excel;base64,'
               , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
               , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
               , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
             return function(table, name) {
               if (!table.nodeType) table = document.getElementById(table)
               var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
               window.location.href = uri + base64(format(template, ctx))
             }
           })();