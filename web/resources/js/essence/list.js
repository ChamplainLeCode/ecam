    /* global user */
   
   window.put = function(data){
       return getTableRow(data);
   };
   
var zoneAddFormVisible = false;
    
    $("#btnAddEssence").click(function(){
        var source = $('#zoneAddEssence');
        if(zoneAddFormVisible === true){
            source.hide(1500);
            zoneAddFormVisible = false;
        }else {
            $.ajax({
                url: "/essence/add",
                method: 'GET',
                data: {id: $(this).attr('data')},
                dataType: 'json',
                error: function (jqXHR, textStatus, errorThrown) {
                    source.html(jqXHR.responseText);
                    source.show(1500);
                },
                success: function (data, textStatus, jqXHR) {
                    source.html(data);
                    source.show(1500);
                }
            });
            zoneAddFormVisible = true;
        }
        
        $(this).addClass('hide');
        $('#closeAddEssence').removeClass('hide');
        $('#zoneEditEssence').hide(1500);
    });
    if(user.can_add !== 'O')       
        $('#btnAddEssence').remove();
   
   
    
    $("#closeAddEssence").click(function(){
        var source = $('#zoneAddEssence');
        if(zoneAddFormVisible === true){
            source.hide(1500);
            zoneAddFormVisible = false;
        }else {
            source.show(1500);
            zoneAddFormVisible = true;
        }
        $(this).addClass('hide');
        $('#btnAddEssence').removeClass('hide');
        $('#zoneEditEssence').hide(1500);
    });
     
    $(".btn-modifier").click(function(){
        $('#zoneAddEssence').hide(1500);
       $.ajax({
            url: "/essence/edit",
            method: 'GET',
            data: {ref: $(this).attr('data')},
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                $('#zoneEditEssence>.body').html(jqXHR.responseText).html(jqXHR.responseText);
            },
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                $('#zoneEditEssence>.body').html(data);
            }
            
       }) ; 
        zoneAddFormVisible = false;
        $('#closeAddEssence').addClass('hide');
        $('#btnAddEssence').removeClass('hide');
    
    });
    
    $('.btn-supprimer ').click(function(){
        var self = $(this);
        $.ajax({
            method: 'POST',
            url: '/essence/delete',
            data: {ref: $(this).attr('data')},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat == 'true'){
                    $('#status-success').html('Essence supprimée').removeClass('hide');
                    $('#status-fail').html('').addClass('hide');
                    self.parent().closest('tr').hide(500);
                }else if(data.resultat == 'false'){
                    $('#status-fail').html('erreur: Essence non supprimée').removeClass('hide');
                    $('#status-success').html('').addClass('hide');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#status-fail').html('erreur: Essence non supprimée').removeClass('hide');
                $('#status-success').html('').addClass('hide');

            }
        });
    });
    
    function getTableRow(dataRow){
        var row = document.createElement("tr"),
        celREF = document.createElement("td"),
        celLIB = document.createElement("td"),
        celDEN = document.createElement("td"),
        celOpe = document.createElement("td"),
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        icon = document.createElement("i");

        celREF.innerHTML = dataRow.ref;
        celLIB.innerHTML = dataRow.libelle;
        celDEN.innerHTML = dataRow.densite;
        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.ref;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-pencil";
        btnEdi.className = "btn btn-success btn-edit-client";
        btnEdi.title = 'Modifier ';
        btnEdi.appendChild(icon);
        btnEdi.onclick = function(){
            $.ajax({
                url: "/essence/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    $('#zoneEditEssence .body').html(jqXHR.responseText);
                }
            });
        };
        
        
        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
        row.appendChild(celREF);
        row.appendChild(celLIB);
        row.appendChild(celDEN);
        row.appendChild(celOpe);
        return row;
    }
    
    window.loadData = function(){
        $('#table_data').html('');
        $.ajax({
            url: "/api/essence/list",
            method: "GET",
            success: function (donnees, textStatus, jqXHR) {
                var data = (donnees);
                for(var i=0; i<data.length; i++){
                    document.getElementById("table_data").appendChild(getTableRow(data[i]));
                }
            },
            complete: function (jqXHR, textStatus ) {
                $('#list-essence').DataTable();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            }
        });
    };

    window.deleteData = function(data, source){
        $.ajax({
            url: "/essence/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat == "true"){
                    $('.status-del-client').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-client').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                    console.log(source.parentNode.parentNode);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat == "false"){
                    $('.status-del-client').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-client').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };