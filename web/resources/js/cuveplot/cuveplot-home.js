/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

   /* global user */

function setCuve(){
       $.ajax({
           url: '/api/cuve/pleine/list',
           method: 'GET',
            complete: function (jqXHR, textStatus) {
                var select = document.getElementById("zone-cuve");
                var option = document.createElement("option");
                select.innerHTML = '';
                option.value = '';
                option.innerHTML = '-- Selectionnez une cuve --';
                select.appendChild(option);
                var data = jqXHR.responseJSON;
                for(var i=0; i<data.length; i++){
                    option = document.createElement("option");
                    option.value = data[i].refCuve;
                    option.innerText = data[i].libelle;
                    select.appendChild(option);
                }
                
            }
       });
   }
   
   
    
   
   function getPlotsInCuve(cuve){
       $.ajax({
            url: '/api/cuve_plot/in_cuve',
            method: 'GET',
            dataType: 'json',
            data: {cuve: cuve},
            complete: function (jqXHR, textStatus) {
                var data = jqXHR.responseJSON;
                var select = document.getElementById("zone-plot");
                select.innerHTML = '';
                for(var i=0; i<data.length; i++){
                    var option = document.createElement("option");
                    option.value = option.innerText = data[i];
                    select.appendChild(option);
                }
            }
       });
   }

$(function(){
   
   
   $('#btn-addCuvePlot').click(function(){
      
       
       $.ajax({
            url: "/cuveplot/add",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                //console.log(data);
                $('#modal-default #modal-body').html(data);
           },
            error: function (jqXHR, textStatus, errorThrown) {
                 $('#modal-default #modal-body').html(jqXHR.responseText);
            }
       })
   });
   if(user.can_add !== 'O')       
       $('#btn-addCuvePlot').remove();

    function getTableRow(dataRow){
        var row = document.createElement("tr"),
                
        celREF = document.createElement("td"),
        celDATEDEBUT = document.createElement("td"),
        celDATEFIN = document.createElement("td"),
        celCuveRef = document.createElement("td"),
        celPlotRef = document.createElement("td"),
        celCertif = document.createElement("td"),
        celOpe = document.createElement("td"),
        
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        icon = document.createElement("i");

         celREF.innerHTML = dataRow.ref;
         
         var dateD = new Date(dataRow.dateDebut);
         dateD.setMonth(dateD.getMonth()-1);
        celDATEDEBUT.innerHTML = dateD.toDateString();
        celDATEFIN.innerHTML = dataRow.dateFin === null ? "--": new Date(dataRow.dateFin).toLocaleString();
        celCuveRef.innerHTML = dataRow.cuve;
        celPlotRef.innerHTML = dataRow.plot;
        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.cni;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-pencil";
        btnEdi.className = "btn btn-success btn-edit-cuveplot";
        btnEdi.title = 'Modifier '+dataRow.nom;
        btnEdi.setAttribute("data-toggle", "modal");
        btnEdi.setAttribute("data-target", "#modal-edit");
        btnEdi.appendChild(icon);
        btnEdi.onclick = function(){
            $.ajax({
                url: "/cuveplot/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    console.log(jqXHR);
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        
        let iref = dataRow.plot.split('-');
        
        $.ajax({
            url: '/api/certification/travail',
            method: 'GET',
            data: {ref: iref[0]+'-'+iref[1]},
            complete: function (jqXHR, textStatus) {
                celCertif.innerHTML = jqXHR.responseJSON.certification === null ? '' : jqXHR.responseJSON.certification;
            }
        });
        

        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
        row.appendChild( celREF);
        row.appendChild(celDATEDEBUT);
        row.appendChild(celDATEFIN);
        row.appendChild(celCuveRef);
        row.appendChild(celPlotRef);
        row.appendChild(celCertif);
        row.appendChild(celOpe);
        return row;
    }

    window.deleteData = function(data, source){
        $.ajax({
            url: "/cuveplot/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('.status-del-cuveplot').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-cuveplot').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('.status-del-cuveplot').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-cuveplot').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    


window.loadData = function (){
        $("#table-body tr").remove();//html('');
        $.ajax({
            url: "/api/cuve_plot/list",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                
                for(var i=0; i<data.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(data[i]));
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("processus erroné");
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
                $('table').DataTable();
            }
        });
    };
    
    function parseDate(dateStringify = (new Date().getYear()+1990)+"-"+new Date().getMonth()+"-"+new Date().getDate()){
        var date = dateStringify.split('-');
        // console.log(date);
        return new Date(parseInt(date[0]), parseInt(date[1]), parseInt(date[2]));
    }
        
    window.saveData = function(ref, dateDebut, cuve,data){
        if(confirm('Voulez-vous vraiment enregistrer?') === false)
            return ;
     $.ajax({
            url: "/cuveplot/add",
            method: "POST",
            data: {
                ref: ref,
                dateDebut: parseDate(dateDebut).getTime(),
               // dateFin: parseDate(dateFin).getTime(),
                cuve: cuve,
                data: data,
                taille: data.length
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-addPane').click();
                loadData();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            complete: function (jqXHR, textStatus) {
                $('.close').click();
            }
        });
};

    
window.editData = function(ref, dateDebut, dateFin, cuveRef,plotRef){
     $.ajax({
            url: "/cuveplot/edit",
            method: "POST",
            data: {
                ref: ref,
                dateDebut: dateDebut,
                dateFin: dateFin,
                cuveRef: cuveRef,
                plotRef: plotRef
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-editPane').click();
                loadData();
                console.log(data);
                console.log('icic');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
};

});
