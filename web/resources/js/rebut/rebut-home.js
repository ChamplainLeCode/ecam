
/* global user */

$(function(){
   
   $('#btn-addRebut').click(function(){
      
       
       $.ajax({
            url: "/rebut/add",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                $('#modal-default #modal-body').html(data);
           },
            error: function (jqXHR, textStatus, errorThrown) {
                 $('#modal-default #modal-body').html(jqXHR.responseText);
            }
       })
   });
    if(user.can_add !== 'O')       
        $('#btn-addRebut').remove();
   
    function getTableRow(dataRow){
        var row = document.createElement("tr"),
                
        celREF = document.createElement("td"),
        celDATE = document.createElement("td"),
        celTYPEREBUT = document.createElement("td"),
        celCertif = document.createElement("td"),
        celOpe = document.createElement("td"),
        
        btnDel = document.createElement("button"),
        btnRestaurer = document.createElement("button"),
        btnEdi = document.createElement("button"),
        icon = document.createElement("i");

        celREF.innerHTML = dataRow.objectRef;
        celDATE.innerHTML = (new Date(dataRow.dateRebut).toDateString());
        celTYPEREBUT.innerHTML = dataRow.typeRebut;
        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.cni;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-pencil";
        btnEdi.className = "btn btn-success btn-edit-rebut";
        btnEdi.title = 'Modifier '+dataRow.nom;
        btnEdi.setAttribute("data-toggle", "modal");
        btnEdi.setAttribute("data-target", "#modal-edit");
        btnEdi.appendChild(icon);
        btnEdi.onclick = function(){
            $.ajax({
                url: "/rebut/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    console.log(jqXHR);
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        
        let iref = dataRow.objectRef.split('-');
        
        $.ajax({
            url: "/api/certification/travail",
            method: "GET",
            data: {ref: iref[0]+'-'+iref[1]},
            dataType: 'json',
            complete: function (jqXHR, textStatus ) {
                celCertif.innerHTML = jqXHR.responseJSON.certification;
            }
        });
        
        btnRestaurer.type = "button";
        btnRestaurer.value = "Restaurer";
            icon = document.createElement('i');
            icon.className = "fa fa-transfert";
        btnRestaurer.className = "btn btn-info btn-rebut-billon";
        btnRestaurer.title = 'Restaurer';
        btnRestaurer.innerHTML = 'Restaurer';
        btnRestaurer.appendChild(icon);
        btnRestaurer.onclick = function(){
            $.ajax({
                url: "/rebut/un_rebut",
                method: "POST",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    if(jqXHR.responseJSON.result === true)
                        document.getElementById("table-body").removeChild(row);
                }
            });
        };
        
        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
        celOpe.appendChild(btnDel);
        celOpe.appendChild(btnEdi);
        celOpe.appendChild(btnRestaurer);
        row.appendChild(celREF);
        row.appendChild(celDATE);
        row.appendChild(celTYPEREBUT);
        row.appendChild(celCertif);
        row.appendChild(celOpe);
        return row;
    }

    window.deleteData = function(data, source){
        $.ajax({
            url: "/rebut/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('.status-del-rebut').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-rebut').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('.status-del-rebut').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-rebut').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    


window.loadData = function (){
        $("#table-body").html('');
        $.ajax({
            url: "/api/rebut/list",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                
                for(var i=0; i<data.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(data[i]));
                }
                            $('table').DataTable();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("processus erroné");
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
                console.log("Le processus est terminé");
            }
        });
    };
    
window.saveData = function(ref, date, typeRebut){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return ;
    $.ajax({
            url: "/rebut/add",
            method: "POST",
            data: {
                ref: ref,
                date: (new Date(date.split("-")[0], date.split("-")[1], date.split("-")[2]).getTime()),
                typeRebut: typeRebut
                },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-addPane').click();
                loadData();
                $('#form-add-rebut .overlay').addClass('hide');
                $('#form-add-rebut #btn-save-rebut').removeAttr("disabled");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#form-add-rebut .overlay').addClass('hide');
                $('#form-add-rebut #btn-save-rebut').removeAttr("disabled");
            },
            complete: function (jqXHR, textStatus) {
                console.log("terminé");
            }
        });
}

    
window.editData = function(ref, date, typeRebut){
     $.ajax({
            url: "/rebut/edit",
            method: "POST",
            data: {
                ref: ref,
                date: date,
                typeRebut: typeRebut,
                
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-editPane').click();
                loadData();
                console.log(data);
                console.log('icic');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
}

});