
/* global user */

$(function(){
      

   
   $('#btn-addEmpotage').click(function(){
      
       
       $.ajax({
            url: "/empotage/add",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                $('#modal-default #modal-body').html(data);
           },
            error: function (jqXHR, textStatus, errorThrown) {
                 $('#modal-default #modal-body').html(jqXHR.responseText);
            }
       });
   });
    if(user.can_add !== 'O')       
        $('#btn-addEmpotage').remove();
   
   
   
    function getTableRow(dataRow){
        var row = document.createElement("tr"),
                
        celREFERENCE = document.createElement("td"),
        celFermetureCaisse = document.createElement("td"),
        celCertification = document.createElement("td"),
        celHauteur = document.createElement("td"),
        celLargeur = document.createElement("td"),
        celLongueur = document.createElement("td"),
        celPoids = document.createElement("td"),
        celOpe = document.createElement("td"),
        
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        icon = document.createElement("i");
        
        celREFERENCE.innerHTML = '<svg id="code'+dataRow.refColis.refColis+'" data="'+dataRow.refColis.codeBarColis+'" class="code-bar"></svg>';//dataRow.ref;
        celFermetureCaisse.innerHTML = dataRow.fermetureCaisse;
        celHauteur.innerHTML = dataRow.hauteur;
        celLargeur.innerHTML = dataRow.largeur;
        celLongueur.innerHTML = dataRow.longueur;
        celPoids.innerHTML = dataRow.refColis.poidNet;//dataRow.colis;
        celCertification.innerHTML = '';
        
        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.ref;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        /*
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-pencil";
        btnEdi.className = "btn btn-success btn-edit-empotage";
        btnEdi.title = 'Modifier '+dataRow.fermetureCaisse;
        btnEdi.setAttribute("data-toggle", "modal");
        btnEdi.setAttribute("data-target", "#modal-edit");
        btnEdi.appendChild(icon);
        btnEdi.onclick = function(){
            $.ajax({
                url: "/empotage/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    console.log(jqXHR);
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        */
        
        $.ajax({
            url: "/api/colis/certification",
            method: "GET",
            data: {ref: dataRow.refColis.refColis},
            dataType: 'json',
            complete: function (jqXHR, textStatus ) {
                celCertification.innerHTML = jqXHR.responseJSON.certification == null ? '' : jqXHR.responseJSON.certification;
            }
        });

        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        /*if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
        */
        row.appendChild(celREFERENCE);
        row.appendChild(celCertification);
        row.appendChild(celFermetureCaisse);
        row.appendChild(celHauteur);
        row.appendChild(celLargeur);
        row.appendChild(celLongueur);
        row.appendChild(celPoids);
        row.appendChild(celOpe);
        return row;
    }

    window.deleteData = function(data, source){
        $.ajax({
            url: "/empotage/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('.status-del-empotage').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-empotage').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('.status-del-empotage').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-empotage').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    


window.loadData = function (){
        $("#table-body").html('');
        $.ajax({
            url: "/api/empotage/list",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                
                for(var i=0; i<data.length; i++){
                    if(data[i].refColis !== null && data[i].refColis !== 'null')
                        document.getElementById("table-body").appendChild(getTableRow(data[i]));
                }
                $('table').DataTable({
                                        "lengthMenu": [500, 1000, 1500, -1]
                });

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("processus erroné");
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
                $('.code-bar').each(function(){
                    JsBarcode('#'+this.id,this.getAttribute("data"), {displayValue: true, height: 40});
                });
            }
        });
    };
    
window.saveData = function(ref, fermetureCaisse, hauteur, largeur,colis, poids){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return ;
     $.ajax({
            url: "/empotage/add",
            method: "POST",
            data: {
                ref: ref,
                fermetureCaisse: fermetureCaisse,
                hauteur: hauteur,
                largeur: largeur,
                colis: colis, 
                poids: poids
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-addPane').click();
                loadData();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            complete: function (jqXHR, textStatus) {
                console.log("terminé");
            }
        });
};

    
window.editData = function(ref, fermetureCaisse, hauteur, largeur,colis){
     $.ajax({
            url: "/empotage/edit",
            method: "POST",
            data: {
                ref: ref,
                fermetureCaisse: fermetureCaisse,
                hauteur: hauteur,
                largeur: largeur,
                colis: colis
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-editPane').click();
                loadData();
                console.log(data);
                console.log('icic');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
};

});