
/* global user */

$(function(){

    $('#btn-open-fiche').click(function(){
        
        $.ajax({
            url: "/api/coupe/bille_for_coupe/list", // /api/bille/travail_for_massicot",
            method: "GET",
            complete: function (jqXHR, textStatus) {
                let opt;
                let data = jqXHR.responseJSON;
                let list = [];
                $('#zone-bille-fiche').html("");
                
                for(var i=0; i<data.length; i++){
                    opt = document.createElement("option");
                    opt.value = data[i];//[0];
                    opt.innerHTML = data[i];//[0];
                    //if(list.indexOf(data[i][0]) < 0){
                    if(list.indexOf(data[i]) < 0){
                        $('#zone-bille-fiche').append($(opt));
                        list.push(data[i]);//[0]);
                    }
                }
            }
       });
    });
    
    
    $('#btn-imprimer-fiche').click(function(){
        /*
        if($('#zone-bille-fiche').val().length === 0){
            $('fieldset').css('border', 'red 2px solid');
            $('#label-fiche-error').html("Veuillez selectionner des billes par numéro de travail");
            return;
        }
        */
        let donnees = $('#zone-bille-fiche').val();
        if(donnees == null || donnees.length === 0){
            $('#label-fiche-error').html("Veuillez selectionner aumoins un numéro de travail");
            $('fieldset').css('border', '1px red solid');
            return;
        }
        $('fieldset').css('border', '2px #595959 solid');
        $('#label-fiche-error').html("");

        $.ajax({
            url: "/tranche/fiche",
            method: "GET",
            data: {
                donnees: donnees,
                taille: donnees.length
            },
            dataType: 'json',
            complete: function (jqXHR, textStatus) {
                var printer = window.open("", "ECAM PLACAGE | MODULE DE PRODUCTION", "directories=no,titlebar=no,status=no,menubar=no,scrollbars=yes,resizable=no,width="+screen.width+",height="+screen.height);
                printer.document.write(jqXHR.responseText);
            }
       });
    });


   
    window.$ = $;
    window.saveData = function saveData(ref, bille, billon, equipe, travail, parc, data){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return ;

        $.ajax({
            data: {
                ref: ref,
                bille: bille,
                nombre: billon,
                equipeRef: equipe,
                numTravail: travail,
                parcRef: parc,
                data: data
            },
            dataType: 'json',
            url: "/billonnage/add",
            method: 'POST',
            complete: function (jqXHR, textStatus) {
                $('#btn-close-modal').click();
            },
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('#form-add-billonnage .overlay').addClass('hide');
                    $('#form-add-billonnage #btn-save-billonnage').removeClass("disabled");
                    $('.status-add-billonnage').removeClass('bg-red .disabled').addClass('bg-green disabled').html('Enregistré');
                    $('#form-add-billonnage #zone-ref').val('REF_CLASS_'+(new Date()).getTime());
                    $('#form-add-billonnage #zone-bille, #form-add-billonnage #zone-billon, #form-add-billonnage #zone-equipe, #form-add-billonnage #zone-travail, #form-add-billonnage #zone-parc').val('');
                    loadData();
                }else{
                    //this.error(data, textStatus, jqXHR);
                }
                console.log(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('#form-add-billonnage .overlay').addClass('hide');
                    $('#form-add-billonnage #btn-save-billonnage').removeClass("disabled");
                    $('.status-add-billonnage').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason);
                }else{
                    //this.success(jqXHR, textStatus, errorThrown);
                }
                console.log(message);
            }
        });
    };
    
    $('#btn-addBillonnage').click(function(e){
        $.ajax({
            url: "/billonnage/add",
            method: "GET",
            success: function (data, textStatus, jqXHR) {
                $('#modal-default #modal-body').html(data);
            },
            complete: function (jqXHR, textStatus ) {
            }
        });
    });
    if(user.can_add !== 'O')       
        $('#btn-addBillonnage').remove();
   
   
    
    function getTableRow(dataRow){
        var row = document.createElement("tr"),
        celRef = document.createElement("td"),
        celCertification = document.createElement("td"),
        celBille = document.createElement("td"),
        celBillon = document.createElement("td"),
        celEquipe = document.createElement("td"),
        celTravail = document.createElement("td"),
        celEssence = document.createElement("td"),
        celParc = document.createElement("td"),
        celOpe = document.createElement("td"),
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        icon = document.createElement("i");

        celRef.innerHTML = dataRow.bille; // dataRow.ref;
        celBille.innerHTML = dataRow.bille;
        celBillon.innerHTML = dataRow.nbrBillon;
        celEquipe.innerHTML = dataRow.equipe;
        celTravail.innerHTML = dataRow.numTravail;
        celParc.innerHTML = dataRow.parc;
        
        
        $.ajax({
            url: "/api/certification/travail",
            method: "GET",
            data: {ref: dataRow.numTravail},
            dataType: 'json',
            complete: function (jqXHR, textStatus ) {
                celCertification.innerHTML = jqXHR.responseJSON.certification;
            }
        });
        $.ajax({
            url: '/api/detail_reception/essence/by_bille',
            data: { bille: dataRow.bille},
            method: 'GET',
            dataType: 'json',
            async: false,
            complete: function (jqXHR, textStatus) {
                let essence = jqXHR.responseJSON;
                celEssence.innerHTML = essence === null ? '' : '['+essence.ref+'] '+essence.libelle;
            }
        })
        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.ref;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-pencil";
        btnEdi.className = "btn btn-success btn-edit-billonnage";
        btnEdi.title = 'Modifier ';
        btnEdi.appendChild(icon);
        btnEdi.onclick = function(){
            $.ajax({
                url: "/billonnage/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        
        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
        //row.appendChild(celRef);
        row.appendChild(celBille);
        row.appendChild(celCertification);
        row.appendChild(celEssence);
        row.appendChild(celBillon);
        row.appendChild(celEquipe);
        row.appendChild(celTravail);
        //row.appendChild(celParc);
        row.appendChild(celOpe);
        return row;
    }
    
    window.loadData = function(){
        $('#table-body').html('');
        $.ajax({
            url: "/api/billonnage/list",
            method: "GET",
            success: function (donnees, textStatus, jqXHR) {
                var data = (donnees);
                for(var i=0; i<data.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(data[i]));
                }
            },
            complete: function (jqXHR, textStatus ) {
                $('#liste').DataTable();
                $('.btn-edit-billonnage').attr('data-toggle','modal').attr('data-target','#modal-edit');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            }
        });
    };
    window.deleteData = function(data, source){
        $.ajax({
            url: "/billonnage/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat == "true"){
                    $('.status-del-billonnage').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-billonnage').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                    console.log(source.parentNode.parentNode);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat == "false"){
                    $('.status-del-billonnage').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-billonnage').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    
    window.editData = function(ref, bille, billon, equipe, travail, parc){
        $.ajax({
            url: "/billonnage/edit",
            method: "POST",
            data: {
                ref: ref, 
                bille: bille, 
                nbrBillon: billon,
                equipe: equipe,
                travail: travail,
                parc: parc
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('#form-edit-billonnage .overlay').addClass('hide');
                    $('#form-edit-billonnage #btn-edit-billonnage').removeClass("disabled");
                    $('.status-edit-billonnage').removeClass('bg-red .disabled').addClass('bg-green disabled').html('Modifié');
                    loadData();
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('#form-edit-billonnage .overlay').addClass('hide');
                    $('#form-edit-billonnage #btn-edit-billonnage').removeClass("disabled");
                    $('.status-edit-billonnage').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
});

