
/* global user */

$(function(){
       

   
    window.$ = $;
    window.saveData = function saveData(ref, longueur, pttDiam, gdDiam, volume, travail, billonnage){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return ;
    $.ajax({
            data: {
                ref: ref,
                longueur: longueur,
                pttDiam: pttDiam,
                gdDiam: gdDiam,
                volume: volume,
                travail: travail,
                billonnage: billonnage
            },
            dataType: 'json',
            url: "/billon/add",
            method: 'POST',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('#form-add-billon .overlay').addClass('hide');
                    $('#form-add-billon #btn-save-billon').removeClass("disabled");
                    $('.status-add-billon').removeClass('bg-red .disabled').addClass('bg-green disabled').html('Enregistré');
                    $('#form-add-billon #zone-ref').val('REF_CLASS_'+(new Date()).getTime());
                    $('#form-add-billon #zone-longueur, #form-add-billon #zone-volume, #form-add-billon #zone-ptt-diam, #form-add-billon #zone-gd-diam, #form-add-billon #zone-travail').val('');
                    loadData();
                }else{
                    //this.error(data, textStatus, jqXHR);
                }
                console.log(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('#form-add-billon .overlay').addClass('hide');
                    $('#form-add-billon #btn-save-billon').removeClass("disabled");
                    $('.status-add-billon').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason);
                }else{
                    //this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };

    $('#btn-addBillon').click(function(e){
        $.ajax({
            url: "/billon/add",
            method: "GET",
            success: function (data, textStatus, jqXHR) {
                $('#modal-default #modal-body').html(data);
            },
            complete: function (jqXHR, textStatus ) {
            }
        });
    });
    if(user.can_add !== 'O')       
        $('#btn-addBillon').remove();
   
   
    
    function getTableRow(dataRow){
        if(dataRow.rebut === 'T')
            return null;
        var row = document.createElement("tr"),
        //celRef = document.createElement("td"),
        celLongueur = document.createElement("td"),
        celCertification = document.createElement("td"),
        celPttDiam = document.createElement("td"),
        celGdDiam = document.createElement("td"),
        celVolume = document.createElement("td"),
        celTravail = document.createElement("td"),
        celBillonnage = document.createElement("td"),
        celEssence = document.createElement("td"),
        celOpe = document.createElement("td"),
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        btnRebut = document.createElement("button"),
        icon = document.createElement("i");

        
        $.ajax({
            url: "/api/certification/travail",
            method: "GET",
            data: {ref: dataRow.ref.split('-')[0]+'-'+dataRow.ref.split('-')[1]},
            dataType: 'json',
            async: false,
            complete: function (jqXHR, textStatus ) {
                celCertification.innerHTML = jqXHR.responseJSON.certification;
            }
        });

        //celRef.innerHTML = dataRow.ref;
        celLongueur.innerHTML = dataRow.longueur;
        celPttDiam.innerHTML = dataRow.pttDiam+"";
        celGdDiam.innerHTML = dataRow.gdDiam+"";
        celVolume.innerHTML = dataRow.volume+"";
        celTravail.innerHTML = dataRow.travail;
        celBillonnage.innerHTML = dataRow.travail+dataRow.ref.substring(dataRow.ref.lastIndexOf('-'));
        $.ajax({
            url: '/api/detail_reception/essence/by_bille',
            data: { bille: dataRow.billonnage.bille},
            method: 'GET',
            dataType: 'json',
            async: false,
            complete: function (jqXHR, textStatus) {
                let essence = jqXHR.responseJSON;
                celEssence.innerHTML = essence === null ? '' : '['+essence.ref+'] '+essence.libelle;
            }
        });
        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.ref;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-pencil";
        btnEdi.className = "btn btn-success btn-edit-billon";
        btnEdi.title = 'Modifier ';
        btnEdi.style.marginRight = "10px";
        btnEdi.appendChild(icon);
        btnEdi.onclick = function(){
            $.ajax({
                url: "/billon/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        
        btnRebut.type = "button";
        btnRebut.value = "Rebut";
            icon = document.createElement('i');
            icon.className = "fa fa-transfert";
        btnRebut.className = "btn btn-info btn-rebut-billon";
        btnRebut.title = 'Rebut';
        btnRebut.innerHTML = 'Rebut';
        btnRebut.style.marginLeft = '5px';
        btnRebut.appendChild(icon);
        btnRebut.onclick = function(){
            $.ajax({
                url: "/rebut/set_rebut",
                method: "POST",
                data: {class: "Billon", ref: dataRow.ref},
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    if(jqXHR.responseJSON.result === true)
                        document.getElementById("table-body").removeChild(row);
                }
            });
        };
        
        
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
        if(user.can_delete === 'O'){
            celOpe.appendChild(btnDel);
            celOpe.appendChild(btnRebut);
        }
        //row.appendChild(celRef);
        row.appendChild(celLongueur);
        row.appendChild(celCertification);
        row.appendChild(celEssence);
//        row.appendChild(celGdDiam);
        //row.appendChild(celVolume);
        row.appendChild(celBillonnage);
        row.appendChild(celOpe);
        return row;
    }
    
    window.loadData = function(){
        $('#table-body').html('');
        $.ajax({
            url: "/api/billon/list",
            method: "GET",
            success: function (donnees, textStatus, jqXHR) {
                var data = (donnees);
                for(var i=0; i<data.length; i++){
                    var v = getTableRow(data[i]);
                    if(v === null )
                        continue;
                    document.getElementById("table-body").appendChild(v);
                }
            },
            complete: function (jqXHR, textStatus ) {
                $('#liste').DataTable();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            }
        });
    };
    
    window.deleteData = function(data, source){
        $.ajax({
            url: "/billon/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat == "true"){
                    $('.status-del-billon').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-billon').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                    console.log(source.parentNode.parentNode);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('.status-del-billon').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-billon').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    
    window.editData = function(ref, longueur, pttDiam, gdDiam, volume, travail, billonnage){
        $.ajax({
            url: "/billon/edit",
            method: "POST",
            data: {
                ref: ref, 
                longueur: longueur, 
                pttDiam: pttDiam,
                gdDiam: gdDiam,
                volume: travail,
                travail:travail,
                billonnage: billonnage
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('#form-edit-billon .overlay').addClass('hide');
                    $('#form-edit-billon #btn-edit-billon').removeClass("disabled");
                    $('.status-edit-billon').removeClass('bg-red .disabled').addClass('bg-green disabled').html('Modifié');
                    loadData();
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('#form-edit-billon .overlay').addClass('hide');
                    $('#form-edit-billon #btn-edit-billon').removeClass("disabled");
                    $('.status-edit-billon').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
});

