
/* global user, parseFloat */

$(function(){
    function parseDate(dateStringify = (new Date().getYear()+1990)+"-"+new Date().getMonth()+"-"+new Date().getDate()){
      var date = dateStringify.split('-');
      //console.log("first = "+parseInt(date[0])+" second = "+(parseInt(date[1])-1)+" troisieme = "+parseInt(date[2]));
      //console.log(new Date(parseInt(date[0]), (parseInt(date[1])-1), parseInt(date[2])));
      return new Date(parseInt(date[0]), (parseInt(date[1])-1), parseInt(date[2]));
    }
       

   
    window.loadFilter = function(){
        val = $("#zone-donnee").val();
        debut = parseDate($("#zone-debut").val());
        fin = parseDate($('#zone-fin').val());
      
        $.ajax({
            url: '/api/comptabilite/'+val,
            method: 'GET',
            data: {
                debut: (debut.getTime()<fin.getTime() ? debut.getTime() : fin.getTime()),
                fin: (debut.getTime()>=fin.getTime() ? debut.getTime() : fin.getTime())
            },
            dataType: 'json',
            complete: function (jqXHR, textStatus) {
                var data = jqXHR.responseJSON;
                var val = $('#zone-donnee').val();
                
                var tableau = document.getElementById("liste");
                tableau.innerHTML = '';

                switch(val){
                    case 'billon':
                        tableau.innerHTML = '<thead>\
                                    <tr role="row">\
                                        <th>Longueur [cm]</th>\
                                        <th>Certification</th>\
                                        <th>Billonnage</th>\
                                    </tr>\
                                </thead>';
                        for(var i=0; i<data.length; i++){
                            tableau.appendChild(putBillon(data[i]));
                        }
                    break;
                    case 'billonnage':

                         tableau.innerHTML = '<thead>\
                                    <tr role="row">\
                                        <th>N° Bille</th>\
                                        <th>Nbr Billons</th>\
                                        <th>Equipe</th>\
                                        <th>N° Travail</th>\
                                        <th>Certification</th>\
                                    </tr>\
                                </thead>';

                        for(var i=0; i<data.length; i++){
                            tableau.appendChild(putBillonnage(data[i]));
                        }
                    break;
                    case 'client':
                        tableau.innerHTML = '\
                                    <thead>\
                                        <tr role="row">\
                                            <th>Code</th>\
                                            <th>Nom</th>\
                                            <th>Pays</th>\
                                            <th>Nationalité</th>\
                                            <th>Ville</th>\
                                            <th>Téléphone</th>\
                                            <th>Email</th>\
                                        </tr>\
                                    </thead>';
                        for(var i=0; i<data.length; i++){
                            tableau.appendChild(putClient(data[i]));
                        }
                    break;
                    case 'colis':
                        tableau.innerHTML = '\
                                    <thead>\
                                        <tr role="row">\
                                            <th style="text-align: center;">Reférence</th>\
                                            <th>NbrePaquet</th>\
                                            <th>Longueur</th>\
                                            <th>Largeur</th>\
                                            <th>Poids</th>\
                                            <th>Surface [m²]</th>\
                                            <th>Volume [m³]</th>\
                                            <th>Poids Embal</th>\
                                            <th>Poids Net</th>\
                                            <th>Date</th>\
                                            <th>N° Plomb</th>\
                                            <th>Certificat</th>\
                                        </tr>\
                                    </thead>';
                        for(var i=0; i<data.length; i++){
                            tableau.appendChild(putColis(data[i]));
                        }
                    break;
                    case 'cuve':
                        tableau.innerHTML = '\
                                <thead>\
                                    <tr role="row">\
                                        <th >REFERENCE</th>\
                                        <th >LIBELLE</th>\
                                        <th >CUVE</th>\
                                    </tr>\
                                </thead>';
                        for(var i=0; i<data.length; i++){
                            tableau.appendChild(putCuve(data[i]));
                        }
                    break;
                    case 'embarquement':
                        tableau.innerHTML = '<thead>\
                                        <tr role="row">\
                                            <th >N° Plomb</th>\
                                            <th >Date</th>\
                                            <th >Volume [m³]</th>\
                                            <th >Surface [m²]</th>\
                                            <th >Nbr Colis</th>\
                                            <th >Nbr Paquets</th>\
                                            <th >Ville</th>\
                                            <th >Pays</th>\
                                            <th >Volume Brût[m3]</th>\
                                            <th >Client</th>\
                                            <th >Chauffeur</th>\
                                            <th >Compagnie</th>\
                                            <th >Transporteur</th>\
                                            <th >Immatriculation</th>\
                                            <th ></th>\
                                        </tr>\
                                    </thead>';
                        for(var i=0; i<data.length; i++){
                            tableau.appendChild(putEmbarquement(data[i]));
                        }
                    break;
                    case 'empotage':
                        tableau.innerHTML = '<thead>\
                                    <tr role="row">\
                                        <th>Colis</th>\
                                        <th>FermetureCaisse</th>\
                                        <th>Hauteur</th>\
                                        <th>Largeur</th>\
                                        <th>Longueur</th>\
                                        <th>Poids </th>\
                                        <th>Certification</th>\
                                    </tr>\
                                </thead>\
                        ';
                        for(var i=0; i<data.length; i++){
                            tableau.appendChild(putEmpotage(data[i]));
                        }
                    break;
                    case 'fournisseur':
                        tableau.innerHTML = '<thead>\
                                    <tr role="row">\
                                        <th>Code</th>\
                                        <th>Nom</th>\
                                        <th>Contribuable</th>\
                                        <th>Téléphone</th>\
                                        <th>Parcs</th>\
                                    </tr>\
                                </thead>\
                        ';
                        for(var i=0; i<data.length; i++){
                            tableau.appendChild(putFournisseur(data[i]));
                        }
                    break;
                    case 'lettre':
                        tableau.innerHTML = '<thead>\
                                    <tr role="row">\
                                        <th>REFERENCE</th>\
                                        <th>Fichier</th>\
                                    </tr>\
                                </thead>\
                        ';
                        for(var i=0; i<data.length; i++){
                            tableau.appendChild(putLettre(data[i]));
                        }
                    break;
                    case 'parc':
                        tableau.innerHTML = '<thead>\
                                    <tr role="row">\
                                        <th class="sorting">Libelle</th>\
                                        <th class="sorting">N° bille</th>\
                                        <th class="sorting">Volume (m³)</th>\
                                        <th class="sorting">Type Mvt</th>\
                                        <th class="sorting">Date</th>\
                                        <th class="sorting">Type Parc</th>\
                                    </tr>\
                                </thead>\
                        ';
                        for(var i=0; i<data.length; i++){
                            tableau.appendChild(putParc(data[i]));
                        }
                    break;
                    case 'paquet':
                        tableau.innerHTML = '<thead>\
                                    <tr role="row">\
                                        <th class="sorting">REF</th>\
                                        <th class="sorting">Nbr Feuilles</th>\
                                        <th class="sorting">N° Travail</th>\
                                        <th class="sorting">Cert.</th>\
                                        <th class="sorting">Longueur</th>\
                                        <th class="sorting">Largeur</th>\
                                        <th class="sorting">Qualite</th>\
                                        <th class="sorting">Surface</th>\
                                        <th class="sorting">Volume</th>\
                                        <th class="sorting">Colis</th>\
                                    </tr>\
                                </thead>\
                        ';
                        for(var i=0; i<data.length; i++){
                            tableau.appendChild(putPaquet(data[i]));
                        }
                    break;
                    case 'plot':
                        tableau.innerHTML = '<thead>\
                                    <tr role="row">\
                                        <th class="sorting">REF</th>\
                                        <th class="sorting">Longueur</th>\
                                        <th class="sorting">Face 1</th>\
                                        <th class="sorting">Face 2</th>\
                                        <th class="sorting">Face 3</th>\
                                        <th class="sorting">Face MN</th>\
                                        <th class="sorting">Billon</th>\
                                        <th class="sorting">Coupe</th>\
                                        <th class="sorting">Travail</th>\
                                        <th class="sorting">Cert.</th>\
                                    </tr>\
                                </thead>\
                        ';
                        for(var i=0; i<data.length; i++){
                            tableau.appendChild(putPlot(data[i]));
                        }
                    break;
                }
                
            }
        });
        return false;
    };

    window.putPlot = function(dataRow){
        var row = document.createElement("tr"),
        celREFERENCE = document.createElement("td"),
        celFace1 = document.createElement("td"),
        celFace2 = document.createElement("td"),
        celFace3 = document.createElement("td"),
        celFaceMN = document.createElement("td"),
        celBillon = document.createElement("td"),
        celCoupe = document.createElement("td"),
        celTravail = document.createElement("td"),
        celLongueur = document.createElement("td"),
        celCertification = document.createElement("td");        

        celREFERENCE.innerHTML = dataRow.ref;
        celFace1.innerHTML = dataRow.face1;
        celFace2.innerHTML = dataRow.face2;
        celFace3.innerHTML = dataRow.face3; 
        celFace3.innerHTML = dataRow.face3;
        celFaceMN.innerHTML = dataRow.face3;
        celBillon.innerHTML = dataRow.coupe.billon;
        celCoupe.innerHTML = dataRow.coupe.typeCoupe.libelle;
        celTravail.innerHTML = dataRow.travail;
        celLongueur.innerHTML = dataRow.longueur;
        celCertification.innerHTML = '';

        $.ajax({
            url: "/api/certification/travail",
            method: "GET",
            data: {ref: dataRow.travail},
            dataType: 'json',
            complete: function (jqXHR, textStatus ) {
                celCertification.innerHTML = jqXHR.responseJSON.certification;
            }
        });
        
        row.appendChild(celREFERENCE);
        row.appendChild(celLongueur);
        row.appendChild(celFace1);
        row.appendChild(celFace2);
        row.appendChild(celFace3);
        row.appendChild(celFaceMN);
        row.appendChild(celBillon);
        row.appendChild(celCoupe);
        row.appendChild(celTravail);
        row.appendChild(celCertification);
        return row;
    };
   
window.putPaquet = function(dataRow){
    var row = document.createElement("tr"),
        celREFERENCE = document.createElement("td"),
        celColis = document.createElement("td"),
        celNbrFeuilles = document.createElement("td"),
        celNumTravail = document.createElement("td"),
        celCertification = document.createElement("td"),
        longueur = document.createElement("td"),
        largeur = document.createElement("td"),
        surface = document.createElement("td"),
        volume = document.createElement("td"),
        qualite = document.createElement("td"),
        celOpe = document.createElement("td"),
        
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        icon = document.createElement("i");

        celREFERENCE.innerHTML = dataRow.ref;
        celColis.innerHTML = dataRow.colis;
        celNbrFeuilles.innerHTML = dataRow.nbreFeuille;
        celNumTravail.innerHTML = dataRow.numTravail;
        longueur.innerHTML = dataRow.longueur;
        largeur.innerHTML = dataRow.largeur;
        surface.innerHTML = dataRow.surface;
        qualite.innerHTML = dataRow.qualite;
        volume.innerHTML = parseFloat(""+dataRow.volume).toFixed(3);
        $.ajax({
            url: "/api/certification/travail",
            method: "GET",
            data: {ref: dataRow.numTravail},
            dataType: 'json',
            complete: function (jqXHR, textStatus ) {

                celCertification.innerHTML = jqXHR.responseJSON.certification;
            }
        });

        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
        row.appendChild(celREFERENCE);
        row.appendChild(celNbrFeuilles);
        row.appendChild(celNumTravail);
        row.appendChild(celCertification);
        row.appendChild(longueur);
        row.appendChild(largeur);
        row.appendChild(qualite);
        row.appendChild(surface);
        row.appendChild(volume);
        row.appendChild(celColis);
        
        return row;
    };
   
    window.putParc = function(dataRow){
        var row = document.createElement("tr"),
        celLIBELLE = document.createElement("td"),
        celNUM  = document.createElement("td"),
        celTYPE = document.createElement("td"),
        celVolume = document.createElement("td"),
        celDATEMVT = document.createElement("td"),
        celREFTYPEPARC = document.createElement("td");
        
        celLIBELLE.innerHTML = dataRow.libelle;
        celNUM.innerHTML = dataRow.numBille;
        celVolume.innerHTML = dataRow.volume;
        
        celTYPE.innerHTML = (dataRow.typeMvt === 'null' ? '' : dataRow.typeMvt);

        celDATEMVT.innerHTML = dataRow.dateMvt === 'null' ? '-' : new Date(parseFloat(dataRow.dateMvt)).toDateString();
        celREFTYPEPARC.innerHTML = (dataRow.refTypeparc === null ? '-' : dataRow.refTypeparc.libelle);
        
        row.appendChild( celLIBELLE);
        row.appendChild(celNUM);
        row.appendChild(celVolume);
        row.appendChild(celTYPE);
        row.appendChild(celDATEMVT);
        row.appendChild(celREFTYPEPARC);
        return row;
   };

    window.putLettre = function(dataRow){
        var row = document.createElement("tr"),
        celREF = document.createElement("td"),
        celFICH = document.createElement("td");
        celREF.innerHTML = dataRow.ref;
        celFICH.innerHTML = '<a download="lettre_camion_'+dataRow.ref+'" href="'+dataRow.fichier+'"><i class="fa fa-download"></i></a>';
        row.appendChild(celREF);
        row.appendChild(celFICH);
        return row;
   };


    window.putFournisseur = function(dataRow){

        var row = document.createElement("tr"),
                
        celREF = document.createElement("td"),
        celNOM = document.createElement("td"),
        celCONT = document.createElement("td"),
        celTEL = document.createElement("td"),
        celPARCS = document.createElement("td");

        celREF.innerHTML = dataRow.refFournisseur;
        celNOM.innerHTML = dataRow.nomFournisseur;
        celCONT.innerHTML = dataRow.contribuable;
        celPARCS.innerHTML = '';
        celTEL.innerHTML = dataRow.contact;
        
        for(var i=0; i<dataRow.parcChargementList.length; i++)
            celPARCS.innerHTML += dataRow.parcChargementList[i].libelle+ '('+dataRow.parcChargementList[i].origine.ref+ ')<br>';
        row.appendChild(celREF);
        row.appendChild(celNOM);
        row.appendChild(celCONT);
        row.appendChild(celTEL);
        row.appendChild(celPARCS);
        return row;
   };

    window.putClient = function(dataRow){

        var row = document.createElement("tr"),
        celRef = document.createElement("td"),
        celNom = document.createElement("td"),
        celEma = document.createElement("td"),
        celTel = document.createElement("td"),
        celNat = document.createElement("td"),
        celPay = document.createElement("td"),
        celVil = document.createElement("td"),
        celOpe = document.createElement("td"),
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        icon = document.createElement("i");

        celRef.innerHTML = dataRow.ref;
        celNom.innerHTML = dataRow.nom;
        celEma.innerHTML = dataRow.email;
        celTel.innerHTML = dataRow.telephone;
        celNat.innerHTML = dataRow.nationalite;
        celPay.innerHTML = dataRow.pays;
        celVil.innerHTML = dataRow.ville;
        
        row.appendChild(celRef);
        row.appendChild(celNom);
        row.appendChild(celPay);
        row.appendChild(celNat);
        row.appendChild(celVil);
        row.appendChild(celTel);
        row.appendChild(celEma);
        row.appendChild(celOpe);
       return row;
   };
    window.putEmpotage = function(dataRow){
       var row = document.createElement("tr"),
                
        celREFERENCE = document.createElement("td"),
        celFermetureCaisse = document.createElement("td"),
        celHauteur = document.createElement("td"),
        celLargeur = document.createElement("td"),
        celCertification = document.createElement("td"),
        celLongueur = document.createElement("td"),
        celPoids = document.createElement("td");
        
        celREFERENCE.innerHTML = dataRow.refColis.refColis;//dataRow.ref;
        celFermetureCaisse.innerHTML = dataRow.fermetureCaisse;
        celHauteur.innerHTML = dataRow.hauteur;
        celLargeur.innerHTML = dataRow.largeur;
        celLongueur.innerHTML = dataRow.longueur;
        celPoids.innerHTML = dataRow.refColis.poidNet;
        
        
        $.ajax({
            url: "/api/colis/certification",
            method: "GET",
            data: {ref: dataRow.refColis.refColis},
            dataType: 'json',
            complete: function (jqXHR, textStatus ) {
                celCertification.innerHTML = jqXHR.responseJSON.certification;
            }
        });
            
        row.appendChild(celREFERENCE);
        row.appendChild(celFermetureCaisse);
        row.appendChild(celHauteur);
        row.appendChild(celLargeur);
        row.appendChild(celLongueur);
        row.appendChild(celPoids);
        row.appendChild(celCertification);

       return row;
   };
   
    window.putColis = function(dataRow){
       var row = document.createElement("tr"),
                
        celREFRENCE = document.createElement("td"),
        celCertification = document.createElement("td"),
        celNbrePaquet = document.createElement("td"),
        celLongueurColis = document.createElement("td"),
        celLargeurColis = document.createElement("td"),
        celPoidsColis = document.createElement("td"),
        celEquipeRef = document.createElement("td"),
        celSurface = document.createElement("td"),
        celVolume = document.createElement("td"),
        celPoidsEmbal = document.createElement("td"),
        celPoidsNet = document.createElement("td"),
        celDate = document.createElement("td"),
        celContenaire = document.createElement("td");
        
        celREFRENCE.innerHTML = dataRow.refColis;//appendChild(codeInput);// = dataRow.codeBarColis;
        celNbrePaquet.innerHTML = dataRow.nbrePaquet;
        celLongueurColis.innerHTML = dataRow.longueurColis;
        celLargeurColis.innerHTML = dataRow.largeurColis;
        //celCodeBarColis.innerHTML = dataRow.codeBarColis;
        celPoidsColis.innerHTML = dataRow.poidsColis;
//        celEquipeRef.innerHTML = dataRow.equipe.libelle;
        celSurface.innerHTML = dataRow.surface;
        celVolume.innerHTML = parseFloat(dataRow.volume).toFixed(2);
        celPoidsEmbal.innerHTML = dataRow.poidEmballage;
        celPoidsNet.innerHTML = dataRow.poidNet;
        let date =  new Date(parseFloat(dataRow.dateDebut));
        celDate.innerHTML = (1900+date.getYear())+"-"+date.getMonth()+"-"+date.getDay();//.toLocaleString();
        celContenaire.innerHTML = dataRow.contenaire;

        $.ajax({
                url: "/api/colis/certification",
                method: "GET",
                data: {ref: dataRow.refColis},
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    celCertification.innerHTML = jqXHR.responseJSON.certification;
                }
            });

        
        row.appendChild(celREFRENCE);
        row.appendChild(celNbrePaquet);
        row.appendChild(celLongueurColis);
        row.appendChild(celLargeurColis);
        row.appendChild(celPoidsColis);
        row.appendChild(celPoidsColis);
        row.appendChild(celPoidsColis);
        row.appendChild(celSurface);
        row.appendChild(celVolume);
        row.appendChild(celPoidsEmbal);
        row.appendChild(celPoidsNet);
        row.appendChild(celDate);
        row.appendChild(celContenaire);
        row.appendChild(celCertification);
        return row;
   };
   
    window.putCuve = function(dataRow){
        var row = document.createElement("tr"),
                
        celREFERENCE = document.createElement("td"),
        celLIBELLE = document.createElement("td"),
        celCUVE = document.createElement("td"),
        celOpe = document.createElement("td"),
        
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        icon = document.createElement("i");

        celREFERENCE.innerHTML = dataRow.refCuve;
        celLIBELLE.innerHTML = dataRow.libelle;
        celCUVE.innerHTML = dataRow.cuve;
        
        row.appendChild(celREFERENCE);
        row.appendChild(celLIBELLE);
        row.appendChild(celCUVE);
         row.appendChild(celOpe);
        return row;
    }
    
    window.putBillonnage = function(dataRow){
        var row = document.createElement("tr"),
        celRef = document.createElement("td"),
        celBille = document.createElement("td"),
        celBillon = document.createElement("td"),
        celEquipe = document.createElement("td"),
        celTravail = document.createElement("td"),
        celParc = document.createElement("td"),
        celOpe = document.createElement("td"),
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        icon = document.createElement("i");

        celRef.innerHTML = dataRow.bille; // dataRow.ref;
        celBille.innerHTML = dataRow.bille;
        celBillon.innerHTML = dataRow.nbrBillon;
        celEquipe.innerHTML = dataRow.equipe;
        celTravail.innerHTML = dataRow.numTravail;
        celParc.innerHTML = dataRow.parc;
        
        
        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
        //row.appendChild(celRef);
        row.appendChild(celBille);
        row.appendChild(celBillon);
        row.appendChild(celEquipe);
        row.appendChild(celTravail);
        row.appendChild(celParc);

       return row;
    }
   
    window.putBillon = function(dataRow){
       var row = document.createElement("tr"),
        celRef = document.createElement("td"),
        celLongueur = document.createElement("td"),
        celCertification = document.createElement("td");
        //celBillonnage = document.createElement("td");

        celRef.innerHTML = dataRow.ref;
        celLongueur.innerHTML = dataRow.longueur;
        celCertification.innerHTML = dataRow.travail;
        //celBillonnage.innerHTML = dataRow.travail+dataRow.ref.substring(dataRow.ref.lastIndexOf('-'));
        
        
        $.ajax({
            url: "/api/certification/travail",
            method: "GET",
            data: {ref: dataRow.ref.split('-')[0]+'-'+dataRow.ref.split('-')[1]},
            dataType: 'json',
            complete: function (jqXHR, textStatus ) {
                console.log(jqXHR.responseJSON.certification);
                celCertification.innerHTML = jqXHR.responseJSON.certification;
            }
        });

        row.appendChild(celLongueur);
        row.appendChild(celCertification);
        row.appendChild(celRef);
        
        return row;
   };    
       
    window.putEmbarquement = function(dataRow){
       var row = document.createElement("tr"),
                
        celPlomb = document.createElement("td"),
        celDateChargement = document.createElement("td"), 
        celVolume = document.createElement("td"),
        celSurface = document.createElement("td"),
        celNbrColis = document.createElement("td"),
        celNbrPaquets = document.createElement("td"),
        celVille= document.createElement("td"),
        celPays = document.createElement("td"),
        celEpaisseur= document.createElement("td"),
        celQualite = document.createElement("td"),
        volumeBrute= document.createElement("td"),
        surface= document.createElement("td"),
        client= document.createElement("td"),
        chauffeur= document.createElement("td"),
        compagnie= document.createElement("td"),
        transporter= document.createElement("td"),
        immatriculation = document.createElement("td"),
        nbrColis= document.createElement("td"),
        nbrPaquets = document.createElement("td"),
        celImprimer = document.createElement("td"),
        btnImprimer = document.createElement("button");

        celNbrColis.innerHTML = dataRow.nbrColis;
        celNbrPaquets.innerHTML = dataRow.nbrPaquets;
            
        celPlomb.innerHTML = dataRow.plomb;
        celDateChargement.innerHTML = new Date(dataRow.date).getDate()+" - "+new Date(dataRow.date).getMonth()+" - "+(new Date(dataRow.date).getYear()+1900);
        celVille.innerHTML = dataRow.ville;
        celPays.innerHTML =dataRow.pays;
        celSurface.innerHTML = parseFloat(dataRow.surface).toFixed(2);
        celVolume.innerHTML = parseFloat(dataRow.volume).toFixed(2);
        volumeBrute.innerHTML = parseFloat(dataRow.volumeBrute).toFixed(2)+' m³';
        surface.innerHTML = parseFloat(dataRow.surface).toFixed(2)+' m²';
        client.innerHTML = dataRow.client.nom+'('+dataRow.client.pays+', '+dataRow.client.telephone+')';
        chauffeur.innerHTML = dataRow.chauffeur.nom + ' - '+dataRow.chauffeur.prenom;
        compagnie.innerHTML = dataRow.compagnie.libelle+' ('+dataRow.compagnie.contribuable+")";
        transporter.innerHTML = (dataRow.transporteur.contribuable);
        immatriculation.innerHTML = dataRow.immatriculation;
        nbrColis.innerHTML = (dataRow.nbrColis);
        nbrPaquets.innerHTML = (dataRow.nbrPaquets);
        
        celImprimer.appendChild(btnImprimer);
        btnImprimer.innerHTML = '<i class="fa fa-print"></i>';
        btnImprimer.setAttribute("class","btn btn-primary");
        btnImprimer.title = 'Imprimer la fiche de rendement';
        btnImprimer.onclick = function(){
            window.open('/comptabilite/fiche_rendement?plomb='+dataRow.plomb,'Fiche rendement');
        };
        
        row.appendChild(celPlomb );
        row.appendChild(celDateChargement ); 
        row.appendChild(celVolume );
        row.appendChild(celSurface );
        row.appendChild(celNbrColis );
        row.appendChild(celNbrPaquets );
        //row.appendChild(celEpaisseur );
        //row.appendChild(celQualite );
        row.appendChild(celVille);
        row.appendChild(celPays );
        row.appendChild(volumeBrute);
        row.appendChild(client);
        row.appendChild(chauffeur);
        row.appendChild(compagnie);
        row.appendChild(transporter);
        row.appendChild(immatriculation);
        row.appendChild(celImprimer);
        
        $.ajax({
            url: '/api/contenaire/qualite_n_epaisseur', 
            data: {plomb: dataRow.plomb},
            dataType: 'json',
            method: 'GET',
            async: false,
            complete: function (jqXHR, textStatus) {
                celEpaisseur.innerHTML = jqXHR.responseJSON.epaisseur;
                celQualite.innerHTML = jqXHR.responseJSON.qualite;
            }
        });
        return row;
   };

    
});