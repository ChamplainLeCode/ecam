
/* global user */

$(function(){
   
   $('#btn-addJointage').click(function(){
       $.ajax({
            url: "/jointage/add",
            method: "GET",
            complete: function (data, textStatus, jqXHR) {
                $('#modal-default #modal-body').html(data.responseText);
                 //$('#modal-default #modal-body').html(jqXHR.responseText);
            }
       });
   });
    if(user.can_add !== 'O')       
        $('#btn-addJointage').remove();
   
    window.getColis = function(select){
            $.ajax({
                url: '/api/jointage/colis/for_jointage',
                method: 'GET',
                complete: function (jqXHR, textStatus ) {
                    var option = document.createElement("option");
                    option.innerHTML = "-- Selectionnez --";
                    option.value = "";
                    select.innerHTML = '';
                    //select.appendChild(option);
                    var data = jqXHR.responseJSON;
                    
                    for(var i=0; i<data.length; i++){
                        option = document.createElement("option");
                        option.value = data[i].refColis;
                        option.innerText = data[i].refColis; 
                        option.data = data[i].epaisseur;
                        select.appendChild(option);
                    }
                    select.onchange = function(){
                        $('#zone-epaisseur').val((select.options[select.selectedIndex].data*10)+'/10');
                    };
                    
                }
            });
        };
   
    function getTableRow(dataRow){
        var row = document.createElement("tr"),
                
                
        celReference = document.createElement("td"),
        nbrColis = document.createElement("td"),
        nbrFeuil = document.createElement("td"),
        epaisseur = document.createElement("td"),
        feuilDef = document.createElement("td"),
        dernierPaquet = document.createElement("td"),
        paquetRestant = document.createElement("td"),
        nbrPaquetRestant = document.createElement("td"),
        essence = document.createElement("td"),
        longueur = document.createElement("td"),
        largeur = document.createElement("td"),
        colisFinal = document.createElement("td"),
        qualite = document.createElement("td"),
        certif = document.createElement("td"),
        
        icon = document.createElement("i");

        celReference.innerHTML = dataRow.colisFinal.refColis;
        nbrColis.innerHTML = dataRow.nbrColis;
        nbrFeuil.innerHTML = dataRow.nbrFeuille;
        longueur.innerHTML = dataRow.longueur;
        essence.innerHTML = dataRow.colisFinal.essence;
        largeur.innerHTML = dataRow.largeur;
        epaisseur.innerHTML = dataRow.epaisseur;
        feuilDef.innerHTML = dataRow.feuillesDefectueuses;
        dernierPaquet.innerHTML = dataRow.feuillesDernierPaquet;
        paquetRestant.innerHTML = dataRow.nbrPaquetRestant;
        nbrPaquetRestant.innerHTML = dataRow.paquetRestant;
        qualite.innerHTML = dataRow.colisFinal.qualite;
        certif.innerHTML = '';
        
        //let iref = dataRow.split('-');
        $.ajax({
            url: "/api/certification/travail/colis",
            method: "GET",
            data: {ref: dataRow.colisFinal.refColis},
            dataType: 'json',
            complete: function (jqXHR, textStatus ) {
                certif.innerHTML = jqXHR.responseJSON.certification;
            }
        });
        
/*
        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
                celOpe.appendChild(btnSee);
*/        
        row.appendChild(celReference);
        row.appendChild(nbrColis);
        row.appendChild(nbrFeuil);
        row.appendChild(longueur);
         row.appendChild(largeur);
         row.appendChild(largeur);
         row.appendChild(epaisseur);
         row.appendChild(dernierPaquet);
         row.appendChild(feuilDef);
         row.appendChild(nbrPaquetRestant);
         row.appendChild(essence);
         row.appendChild(certif);
         row.appendChild(qualite);
        return row;
    }


    function setColis(colis = null){
        
        var tableau = document.getElementById("table-view-detail-jointage");
        tableau.innerHTML = '';
        
        if(colis === null)
            return;

        $('#vzone-ref').html(colis.conteneur);
        $('#vzone-date').html(new Date(colis.date).toLocaleString());
        $('#vzone-volume').html(parseFloat(colis.volume).toFixed(2)+' m³');
        $('#vzone-volume-brute').html(parseFloat(colis.volumeBrute).toFixed(2)+' m³');
        $('#vzone-surface').html(parseFloat(colis.surface).toFixed(2)+' m²');
        $('#vzone-type').html(colis.ville+' - '+colis.pays);
        $('#vzone-client').html(colis.client.nom+'('+colis.client.pays+', '+colis.client.telephone+')');
        $('#vzone-chauffeur').html(colis.chauffeur.nom + ' - '+colis.chauffeur.prenom);
        $('#vzone-compagnie').html(colis.compagnie.libelle+' ('+colis.compagnie.contribuable+")");
        $('#vzone-transporteur').html(colis.transporteur.contribuable);
        $('#vzone-immatriculation').html(colis.immatriculation);
        $('#vzone-nbr-colis').html(colis.nbrColis);
        $('#vzone-nbr-paquets').html(colis.nbrPaquets);
        $('#vzone-lien').attr("href", '/jointage/ticket-pesee?plomb='+colis.plomb);
        for(var i = 0; i<colis.colis.length; i++){
            tableau.appendChild(initColisView(colis.colis[i]));
        }
        
        $('.code-bar').each(function(){
            JsBarcode('#'+this.id,this.getAttribute("data"), {displayValue: true, height: 50});
        });
        $('#liste-view').DataTable();
    }

    function initColisView(dataRow){
        var row = document.createElement("tr"),
                
        celREFRENCE = document.createElement("td"),
        celNbrePaquet = document.createElement("td"),
        celLongueurColis = document.createElement("td"),
        celLargeurColis = document.createElement("td"),
        celPoidsColis = document.createElement("td"),
        celEquipeRef = document.createElement("td"),
        celSurface = document.createElement("td"),
        celVolume = document.createElement("td");

        var codeInput = document.createElement("svg");
        codeInput.id = 'code-'+dataRow.refColis;
        codeInput.data = dataRow.codeBarColis;
        codeInput.className = 'code-bare';
//        console.log(dataRow);
        
        celREFRENCE.innerHTML = '<svg id="code'+dataRow.refColis+'" data="'+dataRow.codeBarColis+'" class="code-bar"></svg>';//appendChild(codeInput);// = dataRow.codeBarColis;
        celNbrePaquet.innerHTML = dataRow.nbrePaquet;
        celLongueurColis.innerHTML = dataRow.longueurColis;
        celLargeurColis.innerHTML = dataRow.largeurColis;
        //celCodeBarColis.innerHTML = dataRow.codeBarColis;
        celPoidsColis.innerHTML = dataRow.poidsColis;
        //celEquipeRef.innerHTML = dataRow.equipe.libelle;
        celSurface.innerHTML = dataRow.surface;
        celVolume.innerHTML = dataRow.volume;
        
        row.appendChild(celREFRENCE);
        row.appendChild(celNbrePaquet);
        row.appendChild(celLongueurColis);
        row.appendChild(celLargeurColis);
        row.appendChild(celPoidsColis);
        row.appendChild(celEquipeRef);
        row.appendChild(celSurface);
        row.appendChild(celVolume);

        return row;
    }

    window.deleteData = function(data, source){
        $.ajax({
            url: "/jointage/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('.status-del-jointage').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-jointage').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('.status-del-jointage').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-jointage').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
   

window.loadData = function (){
        $("#table-body").html('');
        $.ajax({
            url: "/api/jointage/list",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                
                for(var i=0; i<data.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(data[i]));
                }
                            $('#liste').DataTable();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("processus erroné");
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
                console.log("Le processus est terminé");
            }
        });
    };
    
window.saveData = function(conteneur, date, volume, volumeBrute, colis, plomb, surface, compagnie, chauffeur, transporteur, immatri, client, pays, ville, lettre, adresse, portDepart, portDestination, telephone, bateau){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return ;
     $.ajax({
            url: "/jointage/add",
            method: "POST",
            data: {
                conteneur: conteneur,
                date: date,
                colis: colis,
                volume : volume,
                volumeBrut : volumeBrute,
                plomb: plomb,
                surface: surface,
                compagnie: compagnie,
                chauffeur: chauffeur,
                transporteur: transporteur,
                immatriculation: immatri,
                client: client,
                pays: pays,
                ville: ville,
                lettre: lettre, 
                adresse: adresse,
                portDepart: portDepart,
                portDestination: portDestination,
                bateau: bateau,
                telephone: telephone
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $.ajax({
                    url: '/jointage/ticket-pesee',
                    data: {plomb: plomb},
                    dataType: 'json',
                    method: "POST",
                    complete: function (jqXHR, textStatus) {
                        var printer = window.open("", "ECAM PLACAGE | Ticket Pesée", "directories=no,titlebar=no,status=no,menubar=no,scrollbars=yes,resizable=no,width="+screen.width+",height="+screen.height);
                        printer.document.write(jqXHR.responseText);
                        
                        //location.reload();
                    }
                });
                $('#btn-close-addPane').click();
                loadData();
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            complete: function (jqXHR, textStatus) {
                console.log("terminé");
            }
        });
};

    
window.editData = function(ref, dateChargement, hauteur, longueur,colisRef,empotageRef,typeJointageRef,volume){
     $.ajax({
            url: "/jointage/edit",
            method: "POST",
            data: {
                ref: ref,
                dateChargement: dateChargement,
                hauteur: hauteur,
                longueur: longueur,
                colisRef:colisRef,
                empotageRef: empotageRef,
                typeJointageRef: typeJointageRef,
                volume: volume
                
                
                
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-editPane').click();
                loadData();
                console.log(data);
                console.log('icic');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
};

});