
/* global user */

$(function(){
   
   $('#btn-addArmateur').click(function(){
      
       
       $.ajax({
            url: "/armateur/add",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                $('#modal-default #modal-body').html(data);
           },
            error: function (jqXHR, textStatus, errorThrown) {
                 $('#modal-default #modal-body').html(jqXHR.responseText);
            }
       });
   });
    if(user.can_add !== 'O')       
        $('#btn-addArmateur').remove();
   
   
   
    function getTableRow(dataRow){
        var row = document.createElement("tr"),
                
        celCode = document.createElement("td"),
        celArmateurs = document.createElement("td"),
        celOpe = document.createElement("td"),
        
        btnDel = document.createElement("button"),
        btnEdit = document.createElement("button"),
        icon = document.createElement("i");
        
        celCode.innerHTML = dataRow.ref;
        celArmateurs.innerHTML = dataRow.libelle;
        
        btnDel.type = "button";
        btnDel.className = "btn btn-warning";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.matricule;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        icon = document.createElement("i");
        btnEdit.type = "button";
        btnEdit.className = "btn btn-success";
            icon.className = "fa fa-edit";
        btnEdit.setAttribute("data-toggle", "modal");
        btnEdit.setAttribute("data-target", "#modal-edit");
        btnEdit.appendChild(icon);
        btnEdit.onclick = function(){
            $.ajax({
                url: "/armateur/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        btnEdit.id = "btn-del-"+dataRow.matricule;
        btnEdit.style.marginRight = "5px";
        btnEdit.title = 'Modifier ';
        
        
        

        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdit);
        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        row.appendChild(celCode);
        row.appendChild(celArmateurs);
        row.appendChild(celOpe);
        return row;
    }

    window.deleteData = function(data, source){
        $.ajax({
            url: "/armateur/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            complete: function (jqXHR, textStatus) {
                loadData();
            }
        });
    };
    


window.loadData = function (){
        $("#table-body").html('');
        $.ajax({
            url: "/api/contenaire/armateur/list",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                
                for(var i=0; i<data.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(data[i]));
                }
                            $('table').DataTable();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("processus erroné");
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
                console.log("Le processus est terminé");
            }
        });
    };
    
window.saveData = function(libelle){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return ;
     $.ajax({
            url: "/armateur/add",
            method: "POST",
            data: {
                libelle: libelle
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-addPane').click();
                loadData();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            complete: function (jqXHR, textStatus) {
                console.log("terminé");
            }
        });
};
    
window.delete = function(ref){
     $.ajax({
            url: "/armateur/delete",
            method: "POST",
            data: {
                ref: ref,
                libelle: libelle
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-addPane').click();
                loadData();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            complete: function (jqXHR, textStatus) {
                console.log("terminé");
            }
        });
};

    
window.editData = function(ref, libelle){
     $.ajax({
            url: "/armateur/edit",
            method: "POST",
            data: {
                ref: ref,
                libelle: libelle
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-editPane').click();
                loadData();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
};

});