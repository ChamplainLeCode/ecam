
/* global user */

$(function(){
   
   $('#btn-addContenaire').click(function(){
       $.ajax({
            url: "/contenaire/add",
            method: "GET",
            data: {},
            dataType: 'json',
            complete: function (data, textStatus, jqXHR) {
                $('#modal-default #modal-body').html(data.responseText);
                console.log(data);
                 //$('#modal-default #modal-body').html(jqXHR.responseText);
            }
       });
   });
    if(user.can_add !== 'O')       
        $('#btn-addContenaire').remove();
   
   
   
    function getTableRow(dataRow){
        var row = document.createElement("tr"),
                
                
        celReference = document.createElement("td"),
        celPlomb = document.createElement("td"),
        celDateChargement = document.createElement("td"),
        celEssence = document.createElement("td"),
        celType = document.createElement("td"),
        celVolume = document.createElement("td"),
        celSurface = document.createElement("td"),
        celNbrColis = document.createElement("td"),
        celNbrPaquets = document.createElement("td"),
        celEpaisseur = document.createElement("td"),
        celQualite = document.createElement("td"),
        celOpe = document.createElement("td"),
        
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        btnSee = document.createElement("button"),
        icon = document.createElement("i");

        celReference.innerHTML = dataRow.conteneur.ref;
        celPlomb.innerHTML = dataRow.plomb;
        celDateChargement.innerHTML = new Date(dataRow.date).toLocaleString();
        celType.innerHTML = dataRow.ville+' - '+dataRow.pays;
        celVolume.innerHTML = parseFloat(dataRow.volume).toFixed(2);
        celSurface.innerHTML = parseFloat(dataRow.surface).toFixed(2);
        celNbrColis.innerHTML = dataRow.nbrColis;
        celNbrPaquets.innerHTML = dataRow.nbrPaquets;
        celEpaisseur.innerHTML = dataRow.epaisseur;
        celQualite.innerHTML = dataRow.qualite;
        
        /**
         * On collecte toute les essences présente dans le contenaire
         */
        celEssence.innerHTML = "<ol>";
        for(var colisE in dataRow.colis)
            celEssence.innerHTML += (dataRow.colis[colisE].essence=="null"?"":"<li>"+dataRow.colis[colisE].essence+"</li>");
        celEssence.innerHTML += "</ol>";
        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.ref;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-plus-square";
        btnEdi.className = "btn btn-success btn-edit-contenaire hide";
        btnEdi.title = 'Modifier '+dataRow.dateChargement;
        btnEdi.setAttribute("data-toggle", "modal");
        btnEdi.setAttribute("data-target", "#modal-edit");
        btnEdi.appendChild(icon);
        btnEdi.onclick = function(){
            $.ajax({
                url: "/contenaire/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    console.log(jqXHR);
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        
        btnSee.type = "button";
        btnSee.value = "See";
        btnSee.id = "btn-see"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-eye";
        btnSee.className = "btn btn-primary btn-see-contenaire";
        btnSee.title = 'Détails du contenaire '+dataRow.ref;
        btnSee.style.marginLeft = '5px';
        btnSee.setAttribute("data-toggle", "modal");
        btnSee.setAttribute("data-target", "#modal-view");
        btnSee.appendChild(icon);
        btnSee.onclick = function(){
            setColis(dataRow);
        };
        
        

        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
                celOpe.appendChild(btnSee);
        
        row.appendChild(celReference);
        row.appendChild(celPlomb);
        row.appendChild(celDateChargement);
        row.appendChild(celType);
         row.appendChild(celVolume);
         row.appendChild(celSurface);
         row.appendChild(celNbrColis);
         row.appendChild(celNbrPaquets);
         row.appendChild(celEpaisseur);
         row.appendChild(celEssence);
         row.appendChild(celQualite);
        row.appendChild(celOpe);
        return row;
    }


    function setColis(colis = null){
        
        var tableau = document.getElementById("table-view-detail-contenaire");
        tableau.innerHTML = '';
        
        if(colis === null)
            return;

        $('#vzone-ref').html(colis.conteneur);
        $('#vzone-date').html(new Date(colis.date).toLocaleString());
        $('#vzone-volume').html(parseFloat(colis.volume).toFixed(2)+' m³');
        $('#vzone-volume-brute').html(parseFloat(colis.volumeBrute).toFixed(2)+' m³');
        $('#vzone-surface').html(parseFloat(colis.surface).toFixed(2)+' m²');
        $('#vzone-type').html(colis.ville+' - '+colis.pays);
        $('#vzone-client').html(colis.client.nom+'('+colis.client.pays+', '+colis.client.telephone+')');
        $('#vzone-chauffeur').html(colis.chauffeur.nom + ' - '+colis.chauffeur.prenom);
        $('#vzone-compagnie').html(colis.compagnie.libelle+' ('+colis.compagnie.contribuable+")");
        $('#vzone-transporteur').html(colis.transporteur.contribuable);
        $('#vzone-immatriculation').html(colis.immatriculation);
        $('#vzone-nbr-colis').html(colis.nbrColis);
        $('#vzone-nbr-paquets').html(colis.nbrPaquets);
        $('#btn-print-detail a').attr('href', "/contenaire/specification?plomb="+colis.plomb);
        $('#vzone-lien').attr("href", '/contenaire/ticket-pesee?plomb='+colis.plomb).click(function(){
            //window.open('/contenaire/specification?plomb='+colis.plomb);
        });
        for(var i = 0; i<colis.colis.length; i++){
            tableau.appendChild(initColisView(colis.colis[i]));
        }
        
        $('.code-bar').each(function(){
            JsBarcode('#'+this.id,this.getAttribute("data"), {displayValue: true, height: 50});
        });
        $('#liste-view').DataTable();
    }

    function initColisView(dataRow){
        var row = document.createElement("tr"),
                
        celREFRENCE = document.createElement("td"),
        celNbrePaquet = document.createElement("td"),
        celLongueurColis = document.createElement("td"),
        celLargeurColis = document.createElement("td"),
        celPoidsColis = document.createElement("td"),
        celEquipeRef = document.createElement("td"),
        celSurface = document.createElement("td"),
        celVolume = document.createElement("td");

        var codeInput = document.createElement("svg");
        codeInput.id = 'code-'+dataRow.refColis;
        codeInput.data = dataRow.codeBarColis;
        codeInput.className = 'code-bare';
//        console.log(dataRow);
        
        celREFRENCE.innerHTML = '<svg id="code'+dataRow.refColis+'" data="'+dataRow.codeBarColis+'" class="code-bar"></svg>';//appendChild(codeInput);// = dataRow.codeBarColis;
        celNbrePaquet.innerHTML = dataRow.nbrePaquet;
        celLongueurColis.innerHTML = dataRow.longueurColis;
        celLargeurColis.innerHTML = dataRow.largeurColis;
        //celCodeBarColis.innerHTML = dataRow.codeBarColis;
        celPoidsColis.innerHTML = dataRow.poidsColis;
        //celEquipeRef.innerHTML = dataRow.equipe.libelle;
        celSurface.innerHTML = dataRow.surface;
        celVolume.innerHTML = dataRow.volume;
        
        row.appendChild(celREFRENCE);
        row.appendChild(celNbrePaquet);
        row.appendChild(celLongueurColis);
        row.appendChild(celLargeurColis);
        row.appendChild(celPoidsColis);
        row.appendChild(celEquipeRef);
        row.appendChild(celSurface);
        row.appendChild(celVolume);

        return row;
    }

    window.deleteData = function(data, source){
        $.ajax({
            url: "/contenaire/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('.status-del-contenaire').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-contenaire').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('.status-del-contenaire').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-contenaire').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
   

window.loadData = function (){
        $("#table-body").html('');
        $.ajax({
            url: "/api/contenaire/list",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                
                for(var i=0; i<data.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(data[i]));
                }
                $('#liste').DataTable({
                    "lengthMenu": [500, 1000, 1500, -1]
                });

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("processus erroné");
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
                console.log("Le processus est terminé");
            }
        });
    };
    
window.saveData = function(conteneur, date, volume, volumeBrute, colis, plomb, surface, compagnie, chauffeur, transporteur, immatri, client, pays, ville, lettre, adresse, portDepart, portDestination, telephone, bateau){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return ;
     $.ajax({
            url: "/contenaire/add",
            method: "POST",
            data: {
                conteneur: conteneur,
                date: date,
                colis: colis,
                volume : volume,
                volumeBrut : volumeBrute,
                plomb: plomb,
                surface: surface,
                compagnie: compagnie,
                chauffeur: chauffeur,
                transporteur: transporteur,
                immatriculation: immatri,
                client: client,
                pays: pays,
                ville: ville,
                lettre: lettre, 
                adresse: adresse,
                portDepart: portDepart,
                portDestination: portDestination,
                bateau: bateau,
                telephone: telephone
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
/*                $.ajax({
                    url: '/contenaire/ticket-pesee',
                    data: {plomb: plomb},
                    dataType: 'json',
                    method: "POST",
                    complete: function (jqXHR, textStatus) {
                        confirm("Appuyez sur Ok pour afficher les rapports d'embarquement");
                        var printer = window.open("", "ECAM PLACAGE | Ticket Pesée", "directories=no,titlebar=no,status=no,menubar=no,scrollbars=yes,resizable=no,width="+screen.width+",height="+screen.height);
                        printer.document.write(jqXHR.responseText);
                        if(confirm("Imprimer la fiche de spécification?")){
                            printer = window.open("/contenaire/specification&plomb="+plomb, "ECAM PLACAGE | specification", "directories=no,titlebar=no,status=no,menubar=no,scrollbars=yes,resizable=no,width="+screen.width+",height="+screen.height);
                        }
                        //location.reload();
                    }
                });
*/
                $('#btn-close-addPane').click();
                loadData();
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            complete: function (jqXHR, textStatus) {
                console.log("terminé");
            }
        });
};

    
window.editData = function(ref, dateChargement, hauteur, longueur,colisRef,empotageRef,typeContenaireRef,volume){
     $.ajax({
            url: "/contenaire/edit",
            method: "POST",
            data: {
                ref: ref,
                dateChargement: dateChargement,
                hauteur: hauteur,
                longueur: longueur,
                colisRef:colisRef,
                empotageRef: empotageRef,
                typeContenaireRef: typeContenaireRef,
                volume: volume
                
                
                
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-editPane').click();
                loadData();
                console.log(data);
                console.log('icic');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
};

});