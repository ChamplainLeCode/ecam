
/* global user */

$(function(){
    
       window.getBille = function(){
        $.ajax({
            url: '/api/coupe/bille_for_coupe/list',
            method: 'GET',
            complete: function (jqXHR, textStatus) {
                var select = document.getElementById("zone-bille");
                var opt = document.createElement("option");
                var data = jqXHR.responseJSON;
                
                select.innerHTML = '';
                opt.value = '';
                opt.innerHTML = '-- Selectionnez --';
                select.appendChild(opt);
                $('#zone-nombrePlot').val('0');

                for(var i=0; i<data.length; i++){
                    opt = document.createElement('option');
                    opt.value = data[i];
                    opt.innerHTML = data[i];
                    select.appendChild(opt);
                }
            }
        });
    };
    
    window.getBillonForBille = function(numBille){
        $.ajax({
            url: '/api/coupe/billon_from_bille',
            method: 'GET',
            data: {bille: numBille},
            dataType: 'json',
            complete: function (jqXHR, textStatus) {
                var select = document.getElementById("zone-billon");
                var opt = document.createElement("option");
                var data = jqXHR.responseJSON;
                select.innerHTML = '';
                opt.value = '';
                opt.innerHTML = '-- Selectionnez --';
                select.appendChild(opt);
                $('#zone-nombrePlot').val('0');
                for(var i=0; i<data.length; i++){
                    opt = document.createElement('option');
                    opt.value = data[i][0];
                    opt.innerHTML = data[i][0];
                    select.appendChild(opt);
                    window.travail = data[i][1];
                }
            }
        });
    };
    
    var verrou = null;
    
    window.setNombrePlots = function(){
        if($('#zone-bille').val() === '' || $('#zone-billon').val() === '')
            return;
        
            var val = $("#zone-nombrePlot").val();
               var tableau = document.getElementById('table-details-plots');
               tableau.innerHTML = '';
               if($('#zone-billon').val() == '' || $('#zone-billon').val() == null)
                   return;
               $.ajax({
                    url: "/api/plot/get_last_face",
                    method: "GET",
                    dataType: 'json',
                    data: {billon: $('#zone-billon').val()},
                    async: false,
                    complete: function (jqXHR, textStatus) {
                        var indice = 0;
                        if(jqXHR.responseJSON.face === null)
                            indice = 1;
                        else{
                            var lastRef = jqXHR.responseJSON.face;
                            indice = parseInt(lastRef.substring(lastRef.lastIndexOf('-')+1))+1;//.lastIndexOf('-')charCodeAt(0)-'A'.charCodeAt(0)+1;
                        }
                        for(var i=0; i<val; i++){
                            var ligne = document.createElement("tr");
                            var celTravail = document.createElement("td");
                            var celVol = document.createElement("td");
                            var celRef = document.createElement("td");
                            var input = document.createElement("input");
                            input.setAttribute("readonly","");
                            input.value = window.travail+$('#zone-billon').val().substring($('#zone-billon').val().lastIndexOf('-'))+'-'+(indice+i);//String.fromCharCode(char.charCodeAt(0)+i);
                            input.id = 'zone-plot-ref-'+i;
                            celRef.appendChild(input);


                            input = document.createElement("input");
                            input.type = "number";
                            input.min = 0;
                            input.value = window.longueur;
                            input.id = 'zone-plot-longueur-'+i;
                            celVol.appendChild(input);

                            input = document.createElement("input");
                            input.type = "text";
                            input.value = window.travail+"";
                            input.id = 'zone-plot-travail-'+i;
                            input.setAttribute("readonly", "");
                            celTravail.appendChild(input);

                            ligne.appendChild(celRef);
                            ligne.appendChild(celVol);
                            ligne.appendChild(celTravail);

                            tableau.appendChild(ligne);
                       }
                       verrou = null;
                    }
               });
    };
    

   
   $('#btn-addPlot').click(function(){
      
       
       $.ajax({
            url: "/plot/add",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                $('#modal-default #modal-body').html(data);
           },
            error: function (jqXHR, textStatus, errorThrown) {
                 $('#modal-default #modal-body').html(jqXHR.responseText);
            }
       });
   });
   
    if(user.can_add !== 'O')       
        $('#btn-addPlot').remove();
   
   
    function getTableRow(dataRow){
        
        if(dataRow.rebut === 'T')
            return null;
        var row = document.createElement("tr"),
                
//        celREFBille = document.createElement("td"),
        celREFERENCE = document.createElement("td"),
        celFACE1 = document.createElement("td"),
        celFACE2 = document.createElement("td"),
        celFACE3 = document.createElement("td"),
        celFACEMN = document.createElement("td"),
        celCertif = document.createElement("td"),
        celVOLUME = document.createElement("td"),
        celOpe = document.createElement("td"),
        
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        btnRebut = document.createElement("button"),
        icon = document.createElement("i");

  //      celREFBille.innerHTML = dataRow.coupe.billon.split('-')[0];
        celREFERENCE.innerHTML = dataRow.ref;
        celFACE1.innerHTML = dataRow.face1;
        celFACE2.innerHTML = dataRow.face2;
        celFACE3.innerHTML = dataRow.face3;
        celFACEMN.innerHTML = dataRow.faceMN;
        celVOLUME.innerHTML = dataRow.longueur;
        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.ref;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-pencil";
        btnEdi.className = "btn btn-success btn-edit-plot";
        btnEdi.title = 'Modifier '+dataRow.face;
        btnEdi.setAttribute("data-toggle", "modal");
        btnEdi.setAttribute("data-target", "#modal-edit");
        btnEdi.appendChild(icon);
        btnEdi.onclick = function(){
            $.ajax({
                url: "/plot/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    console.log(jqXHR);
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        
        $.ajax({
            url: '/api/certification/travail',
            data: {ref: dataRow.travail},
            dataType: 'json',
            complete: function (jqXHR, textStatus) {
                celCertif.innerHTML = jqXHR.responseJSON.certification;
            }
        })
        btnRebut.type = "button";
        btnRebut.value = "Rebut";
            icon = document.createElement('i');
            icon.className = "fa fa-transfert";
        btnRebut.className = "btn btn-info btn-rebut-billon";
        btnRebut.title = 'Rebut';
        btnRebut.style.marginLeft = "5px";
        btnRebut.innerHTML = 'Rebut';
        btnRebut.appendChild(icon);
        btnRebut.onclick = function(){
            $.ajax({
                url: "/rebut/set_rebut",
                method: "POST",
                data: {class: "Plot", ref: dataRow.ref},
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    if(jqXHR.responseJSON.result === true)
                        document.getElementById("table-body").removeChild(row);
                }
            });
        };
        
        

        if(user.can_delete === 'O'){
            celOpe.appendChild(btnDel);
            celOpe.appendChild(btnRebut);
        }
        //if(user.can_edit === 'O')
          //  celOpe.appendChild(btnEdi);
    //    row.appendChild(celREFBille);
        row.appendChild(celREFERENCE);
        row.appendChild(celCertif);
        row.appendChild(celVOLUME);
        row.appendChild(celFACE1);
        row.appendChild(celFACE2);
        row.appendChild(celFACE3);
        row.appendChild(celFACEMN);
        row.appendChild(celOpe);
        return row;
    }

    window.deleteData = function(data, source){
        $.ajax({
            url: "/plot/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('.status-del-plot').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-plot').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('.status-del-plot').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-plot').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    


window.loadData = function (){
        $("#table-body").html('');
        $.ajax({
            url: "/api/plot/list",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                
                for(var i=0; i<data.length; i++){
                    var v = getTableRow(data[i]);
                    if(v === null)
                        continue;
                    document.getElementById("table-body").appendChild(v);
                }
                            $('table').DataTable();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("processus erroné");
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
                console.log("Le processus est terminé");
            }
        });
    };
    
window.saveData = function(data){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return ;
     $.ajax({
            url: "/plot/add",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-addPane').click();
                loadData();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            complete: function (jqXHR, textStatus) {
                console.log("terminé");
            }
        });
};

    
window.editData = function(ref, face, coupeRef, volume){
     $.ajax({
            url: "/plot/edit",
            method: "POST",
            data: {
                ref: ref,
                face: face,
                coupeRef: coupeRef,
                volume: volume
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-editPane').click();
                loadData();
                console.log(data);
                console.log('icic');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
};

});