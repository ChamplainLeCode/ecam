/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* global fournisseur */
$(function(){
    
    $('.btn-reception').click(function(e){
       $.ajax({
            url: '/commande/list/reception',
            dataType: 'json',
            data: {
                data: $(this).attr('data')
            },
            method: "GET",
            success: function (data, textStatus, jqXHR) {
                $("#zoneReceptionList").html(data);
                $('#openReceptionZone').click();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $("#zoneReceptionList").html(jqXHR.responseText);
                console.log(jqXHR.responseText);
                //$('#openReceptionZone').click();
            }
       })
    });

    $('.btn-print-commande').click(function(e){

    });

    $('.btn-print-all-commande').click(function(e){

    });

    $('.btn-fournisseur').click(function(e){
        $.ajax({
            url: '/fournisseur/card/commande',
            dataType: 'json',
            data: {
                data: $(this).attr('data')
            },
            method: "GET",
            success: function (data, textStatus, jqXHR) {
                $("#zoneReceptionList").html(data);
                $('#openReceptionZone').click();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $("#zoneReceptionList").html(jqXHR.responseText);
                console.log(jqXHR.responseText);
                $('#openReceptionZone').click();
            }
       });
    });

    $(".btn-add-commande").click(function(e){
        $.ajax({
            url: '/commande/add',
            method: "GET",
            data: {ref: 'ref'},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $("#zoneReceptionList").html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $("#zoneReceptionList").html(jqXHR.responseText);
            }
       });
    });
    if(user.can_add !== 'O')       
        $('.btn-add-commande').remove();
   
   

});