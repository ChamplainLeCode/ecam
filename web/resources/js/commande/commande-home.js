
$(function(){
    window.$ = $;
        
    

    window.saveData = function saveData( ref, fournisseur, parc, date, typePaiement, prixTransport, data){
        if(confirm('Voulez-vous vraiment enregistrer?') === false)
            return ;
                   
        let dataj = {
                ref: ref,
                fournisseur: fournisseur,
                parc: parc,
                date: date,
                typePaiement: typePaiement, 
                prixTransport: prixTransport,
                detail: data.length === 0 ? null : data,
                taille: data.length
            };
        $.ajax({
            data: dataj,
            dataType: 'json',
            url: "/commande/add",
            method: 'POST',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('#form-add-commande .overlay').addClass('hide');
                    $('#form-add-commande #btn-save-commande').removeClass("disabled");
                    $('.status-add-commande').removeClass('bg-red .disabled').addClass('bg-green disabled').html('Enregistré');
                    $('#form-add-commande #zone-ref').val('CMD_'+(new Date()).getTime());
                    $('#table-body').html('');
                    loadData();
                    $('#btn-close-modal').click();
                }else{
                    $('#form-add-commande .overlay').addClass('hide');
                    $('#form-add-commande #btn-save-commande').removeAttr("disabled");
                    $('.status-add-commande').removeClass('bg-red .disabled').addClass('bg-red disabled').html(data.reason);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('#form-add-commande .overlay').addClass('hide');
                    $('#form-add-commande #btn-save-commande').removeClass("disabled");
                    $('.status-add-commande').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason);
                }else{
                    //this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };

    $('#btn-addCommande').click(function(){

        $.ajax({
            url: "/commande/add",
            method: "GET",
            success: function (data, textStatus, jqXHR) {
                $('#modal-default #modal-body').html(data);
            },
            complete: function (jqXHR, textStatus ) {
            }
        });
    });
    if(user.can_add !== 'O')       
        $('#btn-addCommande').remove();
   
   
    
    function getTableRow(dataRow){
        var row = document.createElement("tr"),
        celDate = document.createElement("td"),
        celFournisseur = document.createElement("td"),
        celCertification = document.createElement("td"),
        celDetail = document.createElement("td"),
        celRef = document.createElement("td"),
        
        celOpe = document.createElement("td"),
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        btnView = document.createElement("button"),
        icon = document.createElement("i");

        var date = new Date(parseFloat(dataRow.date));
        date.setMonth(date.getMonth()-1);
        celDate.innerHTML = date.toDateString();                                                                                            
        celFournisseur.innerHTML = dataRow.fournisseur.nomFournisseur;
        celRef.innerHTML = dataRow.ref;
        // Old
        //celCertification.innerHTML = dataRow.parc.origine.libelle;
        celCertification.innerHTML = dataRow.detailCommande.length;
        
        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.ref;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-pencil";
            
        btnEdi.className = "btn btn-success btn-edit-commande";
        btnEdi.title = 'Modifier ';
        btnEdi.appendChild(icon);
        btnEdi.onclick = function(){
            dataRow.taille = dataRow.detailCommande.length;
            $('#ezone-fournisseur').val(dataRow.fournisseur.nomFournisseur+'('+dataRow.fournisseur.contact+')');
            $('#ezone-ref').val(dataRow.ref);
            var date = new Date(parseFloat(dataRow.date));
            $('#ezone-date').val(date.toLocaleString());
            //setDetailCommande(dataRow.detailCommande);
            /*
             * Ici on va récupérer la liste des essences sur, une fois terminé
             * on fusionne les détails de la commande(essences) aux essences recues
             */
            getEssence((()=>setDetailCommande(dataRow.detailCommande)));
        };
        
        
        btnView.type = "button";
        btnView.value = "View";
        btnView.style.marginLeft = "5px";
        btnView.id = "btn-view"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-eye";
        btnView.className = "btn btn-primary btn-view-commande_client";
        btnView.title = 'Visualiser ';
        btnView.appendChild(icon);
        btnView.onclick = function(){
            dataRow.taille = dataRow.detailCommande.length;
            (function(e){
                document.getElementById('vzone-ref').innerHTML = (e.ref);
                document.getElementById('vzone-fournisseur').innerHTML = (e.fournisseur.nomFournisseur+"("+e.fournisseur.contact+")");
                document.getElementById('vzone-date').innerHTML = new Date(parseFloat(e.date)).toLocaleString();
                /**
                 * Ici on affiche les details de la commande en tableau
                 */
                // Ancien
                //getRowDetailCommandeForView(dataRow.detailCommande, dataRow.parc.origine.libelle);
                // Nouveau
                getRowDetailCommandeForView(dataRow.detailCommande);
                document.getElementById('vzone-nbr-detail').innerHTML = dataRow.detailCommande.length;
                var div = document.getElementById("vzone-detail");

            })(dataRow);
//            $('#modal-view #modal-body').html(jqXHR.responseText);
            $('#btn-view-commande_client').click();
            
            
        };
        
        
        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        /*
         * ON interdit la modification d'une commande
         *   if(user.can_edit === 'O')
         *       celOpe.appendChild(btnEdi);
         */
        
        celOpe.appendChild(btnView);
        row.appendChild(celRef);
        row.appendChild(celDate);
        row.appendChild(celFournisseur);
        row.appendChild(celCertification);
        row.appendChild(celOpe);
        return row;
    }
    
    window.loadData = function(){
        $('#table-body').html('');
        $.ajax({
            url: "/api/commande/list",
            method: "GET",
            success: function (donnees, textStatus, jqXHR) {
                var data = (donnees);
                for(var i=0; i<data.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(data[i]));
                }
                $('.btn-edit-commande').attr("data-toggle","modal").attr("data-target","#modal-edit");
                $('#liste').DataTable();

                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // // console.log(jqXHR.responseText);
            }
        });
    };
    
    window.deleteData = function(data, source){
        $.ajax({
            url: "/commande/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat == "true"){
                    $('.status-del-commande').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-commande').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                    // // console.log(source.parentNode.parentNode);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('.status-del-commande').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-commande').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    
    window.editData = function(ref, client, typeCoupe, data){
        $.ajax({
            data: {
                ref: ref,
                client: client,
                typeCoupe: typeCoupe,
                detail: data,
                taille: data.length
            },
            dataType: 'json',
            url: "/commande/edit",
            method: 'POST',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('#form-add-commande .overlay').addClass('hide');
                    $('#form-add-commande #btn-save-commande').removeClass("disabled");
                    $('.status-add-commande').removeClass('bg-red .disabled').addClass('bg-green disabled').html('Enregistré');
                    $('#form-add-commande #zone-ref').val('REF_CLASS_'+(new Date()).getTime());
                    loadData();
                }else{
                    $('#form-add-commande .overlay').addClass('hide');
                    $('#form-add-commande #btn-save-commande').removeAttr("disabled");
                    $('.status-add-commande').removeClass('bg-red .disabled').addClass('bg-red disabled').html(data.reason);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('#form-add-commande .overlay').addClass('hide');
//                    $('#form-add-commande #btn-save-commande').removeClass("disabled");
                    $('.status-add-commande').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason);
                }else{
                    //this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    
    function getRowDetailCommandeForView(detail, certif){
            
            var tableau = document.getElementById("table-view-detail-commande");
            // On réinitialise le tableau à chaque fois pour afficher les détails de la commande encours
            tableau.innerHTML = '';
            var detailRow = detail;
            if(detailRow === null){
                detailRow = {
                    bille: '',
                    longueur: 0,
                    gdDiam  :0,
                    pttDiam :0,
                    moyDiam :0,
                    cubage  :0,
                    essence:{prixUAchat:0}
                };
            }
            for(var i=0; i<detail.length; i++){
                var ligne = document.createElement("tr");
                ligne.className = "odd";
                ligne.innerHTML = 
            /**
             * Old
             
                           '<td><span>'+detailRow[i].essence.libelle+'</span></td>\
                            <td><span>'+detailRow[i].numBille+'</span></td>\
                            <td><span>'+certif+'</span></td>\
                            <td><span>'+detailRow[i].longueur+'</span></td>\
                            <td><span>'+detailRow[i].gdDiam+'</span></td>\
                            <td><span>'+detailRow[i].pttDiam+'</span></td>\
                            <td><span>'+parseInt(detailRow[i].moyDiam)+'</span></td>\
                            <td><span>'+detailRow[i].volume+'</span></td>\
                            <td><span>'+detailRow[i].prix+'</span></td>\
                            <td><span>'+detailRow[i].observation+'</span></td>\
            */
                           '<td><span>'+detailRow[i].essence.libelle+'</span></td>\
                            <td><span>'+detailRow[i].numBille+'</span></td>\
                            <td><span>'+detailRow[i].longueur+'</span></td>\
                            <td><span>'+detailRow[i].gdDiam+'</span></td>\
                            <td><span>'+detailRow[i].pttDiam+'</span></td>\
                            <td><span>'+parseInt(detailRow[i].moyDiam)+'</span></td>\
                            <td><span>'+detailRow[i].volume+'</span></td>\
                            <td><span>'+detailRow[i].prix+'</span></td>\
                            <td><span>'+detailRow[i].observation+'</span></td>\
                ';
                tableau.appendChild(ligne);
            }
        } 
        
    window.getFournisseur = function (prefix=""){
        $.ajax({
            url: '/api/fournisseur/list',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                fournisseurs = JSON.parse(jqXHR.responseText);
                var select = document.getElementById(prefix+'zone-fournisseur');
                var selected=-1;
                var opt = document.createElement("option");
                opt.value = '';
                opt.innerHTML = "-- Sélectionnez un Parc --";
                select.appendChild(opt);
                for(var i=0; i<fournisseurs.length; i++){
                    var opt = document.createElement("option");
                    opt.value = fournisseurs[i].refFournisseur;
                    opt.innerHTML = fournisseurs[i].nomFournisseur+("("+fournisseurs[i].contact+")");
                    select.appendChild(opt);
                }
                try{
                    $('#'+prefix+'zone-fournisseur').select2();
                }catch(e){}
            }
        });
    };
    
    
    var essence = [];

    window.getEssence = function(f = null){
        // console.log('appel');
        $.ajax({
            url: '/api/essence/list',
            method: 'GET',
            complete: function (jqXHR, textStatus ) {
                window.essence = jqXHR.responseJSON;
                essence = jqXHR.responseJSON;
                if(f !== null)
                    f();
            }
        });
    };
    
    
    // Nombre total de détail de commande à imprimer à l'écran
    var nbrDetailCommande = 0;
    
    function setDetailCommande(details){
        //getEssence();// Ici on réinitialise les essences
        if(details !== null && details.length>0){
            document.getElementById("table-editCommande-body").innerHTML = '';
            nbrDetailCommande = details.length; // Cette donnée sera utilisé au cas où la modification de la commande implique une augmentation du nombre de détails
            // console.log(essence);
            for(var i=0; i<details.length; i++){
                var ligne = document.createElement("tr");
                ligne.id = "detail-"+i;
                ligne.className = "odd";
                ligne.innerHTML = getRowDetailCommande(details[i], i);
                
            // Insertion de la ligne dans le tableau
                document.getElementById("table-editCommande-body").appendChild(ligne);
                setEssenceEdit(i, details[i].essence);
            }
        }else{
            nbrDetailCommande = 0;
        }
    }

    function getRowDetailCommande(detail, index=nbrDetailCommande){
            var detailRow = detail;
            if(detailRow === null){
                detailRow = {
                    bille: '',
                    numBille: '',
                    longueur: 0,
                    gdDiam  :0,
                    pttDiam :0,
                    moyDiam :0,
                    volume  :0,
                    observation: '',
                    essence:{prixUAchat:0}
                };
            }
            
            return '    <td><select required="" class="select2 ezone-essence" style="width: 100%" id="ezone-essence-'+index+'" data="'+index+'"></select></td>\
                        <td><input required="" type="text" id="ezone-bille-'+index+'" value="'+detailRow.numBille+'" class="form-control" data="'+index+'"></td>\
                        <td><input required="" type="number" value="'+detailRow.longueur+'" id="ezone-longueur-'+index+'" min="0" class="form-control zone-mesure" data="'+index+'"></td>\
                        <td><input required="" type="number" value="'+detailRow.gdDiam+'" id="ezone-gd-diam-'+index+'" min="0" class="form-control zone-diametre" data="'+index+'"></td>\
                        <td><input required="" type="number" value="'+detailRow.pttDiam+'" id="ezone-ptt-diam-'+index+'" min="0" class="form-control zone-diametre" data="'+index+'"></td>\
                        <td><input required="" type="number" value="'+detailRow.moyDiam+'" id="ezone-moy-diam-'+index+'" min="0" class="form-control zone-diametre" readonly="" data="'+index+'"></td>\
                        <td><input required="" type="number" value="'+detailRow.volume+'" id="ezone-cubage-'+index+'" min="0" class="form-control" readonly="" data="'+index+'"></td>\
                        <td><input required="" type="number" value="'+detailRow.essence.prixUAchat+'" id="ezone-prix-'+index+'" min="0" class="form-control" readonly="" data="'+index+'"></td>\
                        <td><input required="" type="text"   value="'+detailRow.observation+'" id="ezone-observation-'+index+'"  class="form-control" data="'+index+'"></td>\
                        <td><button class="btn btn-danger btn-supprimer-detail" type="button" data="'+index+'" title="Supprimer cette ligne"><i class="glyphicon glyphicon-trash"></i></button></td>\
            ';
        }
        
        function setEssenceEdit(index, essenceData = {ref:'', libelle: '', prixUAchat: ''}){
        // Insertion des données d'essence
            var select = document.getElementById('ezone-essence-'+index);
            if(select === null)
                select = document.getElementById('ezone-essence-'+(nbrDetailCommande));
            var selected=-1;
                // console.log(window.essence);
                for(var i=0; i<essence.length; i++){
                    
                    var opt = document.createElement("option");
                    opt.value = essence[i].ref;
                    opt.innerHTML = essence[i].libelle;
                    if(essence[i].ref === essenceData.ref){
                        opt.selected = true;
                    }
                    select.appendChild(opt);
                    
                    /** 
                     * Si il existe une ligne précédente, alors créer la ligne courant de même
                     * essence que la précedente
                     */
                    try{
                        if($("#ezone-essence-"+(index-1)).val() == essence[i].ref){
                            $('#ezone-prix-'+index).val(essence[i].prixUAchat);
                            opt.selected = true;
                        }
                    }finally{}
                }
                
                /**
                 * Si La valeur de l'essence vient à changer, modifier le prix d'achat aussi
                 */
            $('#ezone-essence-'+index).change(function(){
                $('#ezone-prix-'+$(this).attr("data")).val(essence[document.getElementById('ezone-essence-'+$(this).attr("data")).selectedIndex].prixUAchat);
            }).select2().change();
            
            /**
             * Calcul du cubage total de la bille
             */
            $('#ezone-longueur-'+index).change(function(){
                var index = $(this).attr("data");
                var rayon = parseInt($('#ezone-moy-diam-'+index).val())/2;
                var hauteur = parseInt($(this).val());
                
                $('#ezone-cubage-'+index).val( (rayon * rayon * Math.PI * hauteur) / Math.pow(10, 6) );
            });

            
            /**
             * Calcul automatique du diamètre moyen de la bille 
             */
            $('#ezone-ptt-diam-'+index+', #ezone-gd-diam-'+index).change(function(){
                var index = $(this).attr("data");
                $('#ezone-moy-diam-'+index).val((parseInt($('#ezone-ptt-diam-'+index).val())+ parseInt($('#ezone-gd-diam-'+index).val()))/2); 
                
                // Recalculer le volume en cas de modification du diamètre moyen
                
                $('#ezone-longueur-'+index).change();
            });
            
            /**
             * Bouton permettant de supprimer la ligne courante
             */
            $('.btn-supprimer-detail').click(function(){
               var index = $(this).attr("data"); 
               document.getElementById("table-editCommande-body").removeChild(document.getElementById("detail-"+index));
            });
        }
        
    $('#btn-add-detail-edit').click(function(){getEssence(()=>{
            var ligne = document.createElement("tr");
            ligne.id = "detail-"+(nbrDetailCommande);
            ligne.className = "odd"
            ligne.innerHTML = getRowDetailCommande(null);
        
        // Insertion de la ligne dans le tableau
            document.getElementById("table-editCommande-body").appendChild(ligne);
            setEssenceEdit(nbrDetailCommande);
            //for(var i=0; i<index; i++){
            //}
            nbrDetailCommande++;
        });});
        
});

