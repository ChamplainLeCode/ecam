
/* global user */

$(function(){
   
    if(user.can_add !== 'O')       
        $('#btn-addPersonnel').remove();
   
   
   
    function getTableRow(dataRow){
        var row = document.createElement("tr"),
                
        celCNI = document.createElement("td"),
        celNOM = document.createElement("td"),
        celPRE = document.createElement("td"),
        celFCT = document.createElement("td"),
        celOpe = document.createElement("td"),
        
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        btnSee = document.createElement("button"),
        btnBlock = document.createElement("button"),
        btnDeconnect = document.createElement("button"),
        icon = document.createElement("i");
        
        var isOnline = document.createElement("label");
        isOnline.style.width =  "5px";
        isOnline.style.heigth =  "5px";
        isOnline.innerHTML = (dataRow.statut === "F" ? "Off" : "On");
        isOnline.setAttribute("class", "label " +(dataRow.statut === "F" ? "label-danger" : "label-success"))
        celCNI.appendChild(isOnline);
        isOnline = document.createElement("span");
        isOnline.innerHTML = " " +dataRow.matricule;
        celCNI.appendChild(isOnline);
        celNOM.innerHTML = dataRow.nom;
        celPRE.innerHTML = dataRow.prenom === 'null' ? "" : dataRow.prenom;
        celFCT.innerHTML = dataRow.fonction.libelle;
        btnDel.type = "button";
        btnDel.className = "btn btn-warning";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.matricule;
        btnDel.style.marginRight = "10px";
        btnDel.title = 'Delete ';
        
        btnSee.type = "button";
        btnSee.className = "btn btn-github";
            icon = document.createElement('i');
            icon.className = "fa fa-eye";
        btnSee.appendChild(icon);
        btnSee.setAttribute("data-toggle", "modal");
        btnSee.setAttribute("data-target", "#modal-default");
        btnSee.onclick = function(){
            $.ajax({
                url: "/personnel/management",
                method: "GET",
                data: {
                    matricule: dataRow.matricule
                },
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    console.log(data);
                    $('#modal-default #modal-body').html(data);
               },
                error: function (jqXHR, textStatus, errorThrown) {
                     $('#modal-default #modal-body').html(jqXHR.responseText);
                }
           });
        };
        btnSee.id = "btn-see-"+dataRow.matricule;
        btnSee.style.marginRight = "10px";
        btnSee.title = 'Historique de connexion ';
        
        btnDeconnect.type = "button";
        btnDeconnect.className = "btn btn-yahoo";
            icon = document.createElement('i');
            icon.className = "fa fa-sign-out";
        btnDeconnect.appendChild(icon);
        btnDeconnect.onclick = function(){
            $.ajax({
                url: "/user/logout",
                method: "POST",
                data: {
                    matricule: dataRow.matricule
                },
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    if(data.result === false)
                        $('#status-deconnexion').html('Utilisateur non déconnecté').show(500);
                    else
                        $('#status-deconnexion').html('Utilisateur déconnecté').show(500)
                    setTimeout(()=>$('#status-deconnexion').hide(), 10000);
                //    $('#modal-default #modal-body').html(data);
                }
           });
        };
        btnDeconnect.id = "btn-sign-out-"+dataRow.matricule;
        btnDeconnect.style.marginRight = "10px";
        btnDeconnect.title = 'Déconnecter '+dataRow.nom;
        
        btnBlock.type = "button";
        btnBlock.className = "btn btn-danger";
            icon = document.createElement('i');
            icon.className = "fa fa-lock";
        btnBlock.appendChild(icon);
        btnBlock.onclick = function(){
            $.ajax({
                url: "/user/block",
                method: "POST",
                data: {
                    matricule: dataRow.matricule
                },
                dataType: 'json',
                complete: function (data, textStatus, jqXHR) {
                    let donnees = data.responseJSON;
                    $('#status-deconnexion')
                        .html(donnees.result === false ? 'Impossible de bloquer l\'utilisateur dataRow.matricule' : donnees.message)
                        .show(500);
                    setTimeout(()=>$('#status-deconnexion').hide(), 10000);
                    //    $('#modal-default #modal-body').html(data);
                }
           });
        };
        btnBlock.id = "btn-block-out-"+dataRow.matricule;
        btnBlock.style.marginRight = "10px";
        btnBlock.title = 'Bloquer le compte de '+dataRow.nom;
        
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.matricule;
            icon = document.createElement('i');
            icon.className = "fa fa-pencil";
        btnEdi.className = "btn btn-success btn-edit-personnel";
        btnEdi.title = 'Modifier '+dataRow.nom;
        btnEdi.setAttribute("data-toggle", "modal");
        btnEdi.setAttribute("data-target", "#modal-edit");
        btnEdi.appendChild(icon);
        btnEdi.style.marginRight = "10px";
        btnEdi.onclick = function(){
            $.ajax({
                url: "/personnel/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    console.log(jqXHR);
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        
        

        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
        celOpe.appendChild(btnSee);
        celOpe.appendChild(btnDeconnect);
        celOpe.appendChild(btnBlock);
        row.appendChild(celCNI);
        row.appendChild(celNOM);
        row.appendChild(celPRE);
        row.appendChild(celFCT);
        row.appendChild(celOpe);
        return row;
    }

    window.deleteData = function(data, source){
        $.ajax({
            url: "/personnel/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('.status-del-personnel').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-personnel').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('.status-del-personnel').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-personnel').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    


window.loadData = function (){
        $("#table-body").html('');
        $.ajax({
            url: "/api/personnel/list",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                
                for(var i=0; i<data.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(data[i]));
                }
                            $('table').DataTable();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("processus erroné");
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
                console.log("Le processus est terminé");
            }
        });
    };
    
window.saveData = function(cni, nom, prenom, fonction){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return ;
     $.ajax({
            url: "/personnel/add",
            method: "POST",
            data: {
                cni: cni,
                nom: nom,
                prenom: prenom,
                fonction: fonction
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-addPane').click();
                loadData();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            complete: function (jqXHR, textStatus) {
                console.log("terminé");
            }
        });
};

    
window.editData = function(cni, nom, prenom, fonction){
     $.ajax({
            url: "/personnel/edit",
            method: "POST",
            data: {
                cni: cni,
                nom: nom,
                prenom: prenom,
                fonction: fonction
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-editPane').click();
                loadData();
                console.log(data);
                console.log('icic');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
};

});