/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global user */

$(function(){ 
    hideStatusSuccess();
    hideStatusFail();
    window.$ = $;
    $('#nom, #contribuable, #contact').val("");
    
    $('#btn-send').click(function(e){sendRegistration(e);});

    $('#btn-retour').click(function(){
       window.history.back(); 
    });
    
    $('#btn-gotoList').click(function(){
        window.location.pathname = ("/fournisseur/list");
    });
    
    $("#nom").keyup(function(e){
       if(e.key !== ' '){
           $('.group-nom').removeClass('has-error');
            $('.group-nom>div>span').html("");
            hideStatusFail();
            hideStatusSuccess();
       }; 
       $(this).val() === '' ? setNomError() : null;
    });
    $("#contact").keyup(function(e){
        if(e.key !== ' '){
            $('.group-contact').removeClass('has-error');
            $('.group-contact>div>span').html("");
            hideStatusFail();
            hideStatusSuccess();
        }; 
        $(this).val() === '' ? setContactError() : null;
    });
    $("#contribuable").keyup(function(e){
       if(e.key !== ' '){
            $('.group-contribuable').removeClass('has-error');
            $('.group-contribuable>div>span').html("");
            hideStatusFail();
            hideStatusSuccess();
       }; 
       $(this).val() === '' ? setContribuableError() : null;
    });

    function setNomError(){
        $('.group-nom').addClass("has-error");
        $('.group-nom>div>span').html("Veuillez entrer le nom");
        $('#nom').focus();
    }

    function setContactError(){
        $('.group-contact').addClass("has-error");
        $('.group-contact>div>span').html("Veuillez entrer le contact");
        $('#contact').focus();
    }

    function setContribuableError(){
        $('.group-contribuable').addClass("has-error");
        $('.group-contribuable>div>span').html("Veuillez entrer le contribuable");
        $('#contribuable').focus();
    }
    
    function showStatusSuccess(){
        $('#status-success').html('Fournisseur ajouté').removeClass('hide');
        setTimeout(function(){hideStatusSuccess();}, 3000);
    }
    
    function showStatusFail(){
        $('#status-fail').html("Fournisseur non ajouté").removeClass('hide');
        setTimeout(function(){hideStatusFail();}, 3000);
    }
        
    function hideStatusSuccess(){
        $('#status-success').addClass('hide');
    }
    
    function hideStatusFail(){
        $('#status-fail').addClass('hide');
    }
    
    function sendRegistration(e){
        var nom = $('#nom').val();
        var contribuable = $('#contribuable').val();
        var contact = $('#contact').val();
        var i= 0;

        if(nom === '' || contribuable === '' || contact === ''){
            if(nom === ''){
                setNomError();
                return false;
            }
            if(contribuable === ''){
                setContribuableError();
                return false;
            }
            if(contact === ''){
                setContactError();
                return false;
            }
        }

        for(i = 0; i<nom.length && nom[i] === ' '; i++);
        if(i >= nom.length){
            setNomError();
            return false;
        }

        for(i = 0; i<contribuable.length && contribuable[i] === ' '; i++);
        if(i >= contribuable.length){
            setContribuableError();
            return false;
        }

        for(i = 0; i<contact.length && contact[i] === ' '; i++);
        if(i >= contact.length){
            setContactError();
            return false;
        }

        $.ajax({
            dataType: 'json',
            url: "/fournisseur/add",
            method: 'POST',
            data: {
                nom: nom,
                contact: contact,
                contribuable: contribuable
            },
            success: function (data, textStatus, jqXHR) {
                if((data.error === true) === true){console.log('idi ->'); hideStatusSuccess(); showStatusFail(); return;}
                console.log('test === ' + (data.error == true) === true + ' value = '+ data.error);
                console.log(data.error);
                $('#nom').val("");
                $('#contribuable').val("");
                $('#contact').val("");
                showStatusFail();
                hideStatusSuccess();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("error");
                hideStatusSuccess();
                showStatusFail();
                    $('#status-fail').html('erreur: Fournisseur non modifié').removeClass('hide');
                    $('#status-success').html('').addClass('hide');
            }
        });
    }
    
    function getTableRow(dataRow){
        var row = document.createElement("tr"),
                
        celREF = document.createElement("td"),
        celNOM = document.createElement("td"),
        celCONT = document.createElement("td"),
        celTEL = document.createElement("td"),
        celPARCS = document.createElement("td"),
        celCERTIFS = document.createElement("td"),
        celOpe = document.createElement("td"),
        
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        btnCard = document.createElement("button"),
        icon = document.createElement("i");

        celREF.innerHTML = dataRow.refFournisseur;
        celNOM.innerHTML = dataRow.nomFournisseur;
        celCONT.innerHTML = dataRow.contribuable;
        celTEL.innerHTML = dataRow.contact;
        
        for(var i=0; i< dataRow.parcChargementList.length; i++){
            celPARCS.innerHTML += dataRow.parcChargementList[i].libelle+'<br>';
            celCERTIFS.innerHTML += dataRow.parcChargementList[i].certificat+"<br>";
        }
        /*$.ajax({
            url: '/api/certificat',
            data: {ref: dataRow.refFournisseur},
            dataType: 'json',
            method: 'get',
            success: function (data, textStatus, jqXHR) {
                for(var i=0; i<data.length; i++){
                    celCERTIFS.innerHTML += data[i].libelle+'<br>';
                }
            }
        });*/
        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.cni;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-pencil";
        btnEdi.className = "btn btn-success btn-edit";
        btnEdi.title = 'Modifier '+dataRow.nom;
        btnEdi.setAttribute("data-toggle", "modal");
        btnEdi.setAttribute("data-target", "#modal-edit");
        btnEdi.appendChild(icon);
        dataRow.taille = dataRow.parcChargementList.length;
        btnEdi.onclick = function(){
            $.ajax({
                url: "/fournisseur/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    console.log(jqXHR);
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        
        
        btnCard.type = "button";
        btnCard.value = "view";
        btnCard.id = "btn-view"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-eye";
        btnCard.className = "btn btn-primary btn-view";
        btnCard.title = 'Visualiser '+dataRow.nom;
        btnCard.setAttribute("data-toggle", "modal");
        btnCard.setAttribute("data-target", "#modal-see");
        btnCard.appendChild(icon);
        btnCard.onclick = function(){
            $.ajax({
                url: "/fournisseur/card",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    console.log(jqXHR);
                    $('#modal-see #modal-body').html(jqXHR.responseText);
                }
            });
        };
        
        
        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
        celOpe.appendChild(btnCard);
        row.appendChild(celREF);
        row.appendChild(celNOM);
        row.appendChild(celCONT);
        row.appendChild(celTEL);
        row.appendChild(celCERTIFS);
        row.appendChild(celPARCS);
        row.appendChild(celOpe);
        return row;
    }

    window.deleteData = function(data, source){
        console.log(data);
        $.ajax({
            url: "/fournisseur/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                if(data.resultat === "true"){
                    $('.status-del-personnel').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-personnel').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                }else{
                 //   this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('.status-del-personnel').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-personnel').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    


window.loadData = function (donnee={origine:'all', dateDebut:0, dateFin:0}){
        $("#table-body").html('');

        $.ajax({
            url: "/api/fournisseur/list",
            method: "GET",
            data: donnee,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                
                for(var i=0; i<data.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(data[i]));
                }
                            $('table').DataTable();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("processus erroné");
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
                console.log("Le processus est terminé");
            }
        });
    };
    
window.saveData = function(ref, nom, contribuable, contact, parcs, certifs){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return ;
     $.ajax({
            url: "/fournisseur/add",
            method: "POST",
            data: {
                ref: ref,
                nom: nom,
                contribuable: contribuable,
                contact: contact,
                parcs: parcs,
                taille: parcs.length,
                certificats: certifs,
                tailleCertificat: certifs.length
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('#liste tbody').html('');
                    $('#btn-close-addPane').click();
                    loadData();
                }else{
                    $('#zone-statut-add').removeClass('hide');
                    $('#form-add-fournisseur .overlay').addClass('hide');
                    $('#form-add-fournisseur #btn-add-fournisseur').removeClass("disabled");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            complete: function (jqXHR, textStatus) {
                console.log("terminé");
            }
        });
}

    
window.editData = function(ref, nom, contribuable, contact, certifs){
     $.ajax({
            url: "/fournisseur/edit",
            method: "POST",
            data: {
                ref: ref,
                nom: nom,
                contribuable: contribuable,
                contact: contact,
                certificats: certifs,
                tailleCertificat: certifs.length
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('#btn-close-editPane').click();
                    loadData();
                }else{
                    $('#zone-statut-edit').removeClass("hide");
                    $('#form-edit-fournisseur .overlay').addClass('hide');
                    $('#form-edit-fournisseur #btn-edit-fournisseur').removeClass("disabled");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
}

    
});
