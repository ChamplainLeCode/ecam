
/* global user */

$(function(){
    window.$ = $;
    window.saveData = function saveData(ref, nom, pays, nationalite, ville, telephone, email){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return ;
        $.ajax({
            data: {
                ref: ref,
                nom: nom,
                pays: pays,
                nationalite: nationalite,
                ville: ville,
                telephone: telephone,
                email: email
            },
            dataType: 'json',
            url: "/client/add",
            method: 'POST',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('#form-add-client .overlay').addClass('hide');
                    $('#form-add-client #btn-save-client').removeClass("disabled");
                    $('.status-add-client').removeClass('bg-red .disabled').addClass('bg-green disabled').html('Enregistré');
                    $('#form-add-client #zone-ref').val('REF_CLASS_'+(new Date()).getTime());
                    $('#form-add-client #zone-nom, #form-add-client #zone-pays, #form-add-client #zone-nationalite, #form-add-client #zone-ville, #form-add-client #zone-telephone, #form-add-client #zone-email').val('');
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('#form-add-client .overlay').addClass('hide');
                    $('#form-add-client #btn-save-client').removeClass("disabled");
                    $('.status-add-client').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason);
                }else{
                    this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };

    $('#btn-addClient').click(function(e){
        $.ajax({
            url: "/client/add",
            method: "GET",
            success: function (data, textStatus, jqXHR) {
                $('#modal-body').html(data);
            },
            complete: function (jqXHR, textStatus ) {
            }
        });
    });
    if(user.can_add !== 'O')       
        $('#btn-addClient').remove();
   
    
    function getTableRow(dataRow){
        var row = document.createElement("tr"),
        celNom = document.createElement("td"),
        celEma = document.createElement("td"),
        celTel = document.createElement("td"),
        celNat = document.createElement("td"),
        celPay = document.createElement("td"),
        celVil = document.createElement("td"),
        celOpe = document.createElement("td"),
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        icon = document.createElement("i");

        celNom.innerHTML = dataRow.nom;
        celEma.innerHTML = dataRow.email;
        celTel.innerHTML = dataRow.telephone;
        celNat.innerHTML = dataRow.nationalite;
        celPay.innerHTML = dataRow.pays;
        celVil.innerHTML = dataRow.ville;
        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.ref;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-pencil";
        btnEdi.className = "btn btn-success btn-edit-client";
        btnEdi.title = 'Modifier ';
        btnEdi.appendChild(icon);
        btnEdi.onclick = function(){
            $.ajax({
                url: "/client/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        
        

        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
                row.appendChild(celNom);
        row.appendChild(celPay);
        row.appendChild(celNat);
        row.appendChild(celVil);
        row.appendChild(celTel);
        row.appendChild(celEma);
        row.appendChild(celOpe);
        return row;
    }
    
    window.loadData = function(){
        $('#table-body').html('');
        $.ajax({
            url: "/api/client/list",
            method: "GET",
            success: function (donnees, textStatus, jqXHR) {
                var data = (donnees);
                for(var i=0; i<data.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(data[i]));
                }
            },
            complete: function (jqXHR, textStatus ) {
                $('#liste').DataTable();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            }
        });
    };
    window.deleteData = function(data, source){
        $.ajax({
            url: "/client/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat == "true"){
                    $('.status-del-client').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-client').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                    console.log(source.parentNode.parentNode);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat == "false"){
                    $('.status-del-client').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-client').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    
    window.editData = function(epaisseur, ref, essence){
        $.ajax({
            url: "/client/edit",
            method: "POST",
            data: {ref: ref, epaisseur: epaisseur, essenceRef: essence},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat == "true"){
                    $('#form-edit-client .overlay').addClass('hide');
                    $('#form-edit-client #btn-edit-client').removeClass("disabled");
                    $('.status-edit-client').removeClass('bg-red .disabled').addClass('bg-green disabled').html('Modifié');
                    loadData();
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat == "false"){
                    $('#form-edit-client .overlay').addClass('hide');
                    $('#form-edit-client #btn-edit-client').removeClass("disabled");
                    $('.status-edit-client').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
});

