

/* global user */

$(function(){
    $('#btn-fiche-billonnage').click(function(){
        let div = $('#modal-billonnage .modal-body');
        let table = document.createElement("table");
        var tr, td, btn, tfooter, tbody, select, input, corbeille, thead, btnPrint;
        let i = 0;
        
        div.html('');
        tfooter = document.createElement("tfoot");
        tbody = document.createElement("tbody");
        thead = document.createElement("thead");
        btnPrint = document.createElement('button');
        
        btnPrint.setAttribute("class","btn btn-primary pull-right");
        btnPrint.setAttribute("type", "button");
        btnPrint.innerHTML = 'Générer la fiche';
        btnPrint.onclick = function(){
            let donnees = [];
            $('.travail').each(function(e){
                let index = this.id.split('_')[1];
               let val = {
                   bille: $('#numBille_'+index).val().split(',')[0],
                   valeur: $('#travail_'+index).val()
               };
               donnees.push(val);               
            });
            $.ajax({
                url: '/billonnage/fiche',
                data: {billes: donnees, taille: donnees.length},
                dataType: 'json',
                method: "GET",
                async: false,
                complete: function (jqXHR, textStatus) {
                    var printer = window.open("", "MODULE BILLONNAGE | "+(new Date().getTime()), "directories=no,titlebar=no,status=no,menubar=no,scrollbars=yes,resizable=no,width="+screen.width+",height="+screen.height);
                    printer.document.write(jqXHR.responseText);
                }
            });
        };
        
        table.setAttribute("class", "table");
        tr = document.createElement("tr");
        td = document.createElement("td");
        td.innerHTML = 'N° Bille';
        tr.appendChild(td);
        td = document.createElement("td");
        td.innerHTML = 'N° Travail';
        tr.appendChild(td);
        td = document.createElement("td");
        td.innerHTML = 'Opération';
        tr.appendChild(td);
        thead.appendChild(tr);
        table.appendChild(thead);
        
        btn = document.createElement("button");
        btn.type = "button";
        btn.innerHTML = '<i class="fa fa-plus"></i>';
        btn.setAttribute("class", "btn btn-primary");
        btn.style.width = "50px";
        btn.style.height = "50px";
        btn.style.borderRadius = "25px";
        
        btn.onclick = function(){
            tr = document.createElement("tr");
            select = document.createElement("select");
            input = document.createElement("input");
            corbeille = document.createElement("button");
            
            
            input.type = "text";
            input.setAttribute("class", "form-control travail");
            input.id = "travail_"+i;
            input.setAttribute("readonly", "");
            
            setChildFromRecentlyOutGrum(select, input);
            select.setAttribute("class", "select2 ");
            select.id = "numBille_"+(i++);
            //$(select).select2();
            
            corbeille.innerHTML = '<i class="fa fa-trash"></i>';
            corbeille.setAttribute("class", "btn btn-danger");
            corbeille.style.width = "50px";
            corbeille.style.height = "50px";
            corbeille.style.borderRadius = "25px";
            corbeille.onclick = () => $(tr).remove();
            
            td = document.createElement("td");
            td.appendChild(select);
            td.style.padding = "5px";
            tr.appendChild(td);
            td = document.createElement("td");
            td.appendChild(input);
            td.style.padding = "5px";
            tr.appendChild(td);
            td = document.createElement("td");
            td.appendChild(corbeille);
            td.style.padding = "5px";
            tr.style.marginTop = '5px';
            tr.appendChild(td);
            tbody.appendChild(tr);
        }
        
        tfooter.appendChild(btnPrint);
        tfooter.appendChild(btn);
        table.appendChild(tbody);
        table.appendChild(tfooter);
        div.append($(table));
    });
    
   $('#btn-addParc').click(function(){
      
       
       $.ajax({
            url: "/parc/add",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                // console.log(data);
                $('#modal-default #modal-body').html(data);
           },
            error: function (jqXHR, textStatus, errorThrown) {
                 $('#modal-default #modal-body').html(jqXHR.responseText);
            }
       });
   });
    if(user.can_add !== 'O')       
        $('#btn-addParc').remove();
   
   
   
   function setChildFromRecentlyOutGrum(select, input){
       var opt = document.createElement("option");
       
        $.ajax({
            url: '/api/parc/recently/out',
            method: "GET",
            async: false,
            complete: function (jqXHR, textStatus) {
                var data = jqXHR.responseJSON;
                opt.value ='';
                opt.innerHTML = 'Choisir';
                select.appendChild(opt);
                if(data.length > 0){
                    for(let numBille in data){
                        opt = document.createElement("option");
                        opt.value = data[numBille];
                        opt.innerHTML = data[numBille][0];
                        opt.setAttribute("data", data[numBille][1])
                        select.appendChild(opt);
                    }
                    select.onchange = function(){
                        input.value = this.children[this.selectedIndex].getAttribute('data');
                    }
                }
            }
        });
   }
   
   
    function getTableRow(dataRow, index){
        var row = document.createElement("tr"),
        celOpe = document.createElement("td"),
        celREF = document.createElement("td"),
        celLIBELLE = document.createElement("td"),
        celNUM  = document.createElement("td"),
        celTYPE = document.createElement("td"),
        celVolume = document.createElement("td"),
        celDATEMVT = document.createElement("date"),
        celREFTYPEPARC = document.createElement("td"),
        celTravail = document.createElement("td"),

        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        btnView = document.createElement("button"),
        icon = document.createElement("i");
        
        row.id = 'ligne-'+index;
        
        // console.log(dataRow);
         celREF.innerHTML = dataRow.ref;
        celLIBELLE.innerHTML = dataRow.libelle;
        celNUM.innerHTML = dataRow.numBille;
        celVolume.innerHTML = dataRow.volume;
        
        var select = document.createElement("select");
        select.className = "zone-type-mvt";
        select.setAttribute("data", dataRow.numBille);
        select.id = "zone-type-mvt-"+index;
        select.setAttribute("index", index);
        
        var o = document.createElement("option");
        o.value = o.innerHTML = '-- Mouvement --';
        select.appendChild(o);
        o = document.createElement("option");
        o.value = o.innerHTML = 'Changement de Parc';
        select.appendChild(o);
        o = document.createElement("option");
        o.value = o.innerHTML = 'Mise en production';
        select.appendChild(o);
        o = document.createElement("option");
        o.value = o.innerHTML = 'Retour chez le fournisseur';
        select.appendChild(o);
        /*o = document.createElement("option");
        o.value = o.innerHTML = 'Classer pour emballage';
        select.appendChild(o);
        */
       celTYPE.appendChild(select);
        celTYPE.appendChild(document.createElement("br"));
        
        var input = document.createElement("input");
        input.id = "zone-travail-"+index;
        input.placeholder = "N° Travail";
        input.className = "form-control hide";
        input.value = '532600';
        celTYPE.appendChild(input);

        celDATEMVT.innerHTML = dataRow.dateMvt == 'null' ? '-' : new Date(parseFloat(dataRow.dateMvt)).toDateString();
        celREFTYPEPARC.innerHTML = (dataRow.refTypeparc === null ? '' : dataRow.refTypeparc.libelle);
        
        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.refparc;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-check";
        btnEdi.className = "btn btn-success btn-edit-parc";
        btnEdi.title = 'Modifier '+dataRow.libelle;
        //btnEdi.setAttribute("data-toggle", "modal");
        //btnEdi.setAttribute("data-target", "#modal-edit");
        btnEdi.appendChild(icon);
        btnEdi.setAttribute("index", index);
        btnEdi.onclick = function(){
            var myIndex = $(this).attr("index")
            switch($('#zone-type-mvt-'+myIndex).val()){
                case 'Mise en production':
                    var travail = $('#zone-travail-'+myIndex);
                    $.ajax({
                        url: '/detail_reception/init_travail',
                        method: 'POST',
                        data: {
                            refBille: dataRow,
                            travail: travail.val()
                        },
                        dataType: 'json',
                        complete: function (jqXHR, textStatus) {
                            document.getElementById('table-body').removeChild(document.getElementById('ligne-'+myIndex));
                        }
                    });
                    break;
                case 'Changement de parc': 
                    
                    break;
                case 'Retour chez le fournisseur': 
                    
                    $.ajax({
                        url: '/detail_reception/go_back_fournisseur',
                        method: 'POST',
                        data: {
                            refBille: dataRow,
                        },
                        dataType: 'json',
                        complete: function (jqXHR, textStatus) {
                            document.getElementById('table-body').removeChild(document.getElementById('ligne-'+myIndex));
                        }
                    });
                    
                    break;
            }
            
            
            /*$.ajax({
                url: "/parc/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    // console.log(jqXHR);
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });*/
        };
        
        
        
        btnView.type = "button";
        btnView.value = "View";
        btnView.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-eye";
        btnView.className = "btn btn-primary btn-edit-parc hide";
        btnView.title = 'Modifier '+dataRow.libelle;
        btnView.setAttribute("data-toggle", "modal");
        btnView.setAttribute("data-target", "#modal-edit");
        btnView.appendChild(icon);
        btnView.onclick = function(){
            $.ajax({
                url: "/parc/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    // console.log(jqXHR);
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        
        
        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
        celOpe.appendChild(btnView);
        //row.appendChild(celREF);
        row.appendChild( celLIBELLE);
        row.appendChild(celNUM);
        row.appendChild(celVolume);
        row.appendChild(celTYPE);
        row.appendChild(celDATEMVT);
        row.appendChild(celREFTYPEPARC);
        row.appendChild(celOpe);
        return row;
    }

    window.deleteData = function(data, source){
        $.ajax({
            url: "/parc/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('.status-del-parc').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-parc').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('.status-del-parc').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-parc').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    


window.loadData = function (urlPath = null, dataPath=null){

        $("#table-body").html('');
        
        var url = "/api/parc/list";
        
        var data = {};
        
        if(urlPath !== null && dataPath !== null){
            if(dataPath.origine !== "%%all%%"){
                url = urlPath;
                data = dataPath;
            }
        }
        
        var settings = {
            url: url,
            method: "GET",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                //  data = JSON.stringify(data);
                var volumeTotal = 0;
                for(var i=0; i<data.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(data[i], i));
                    $('#zone-type-mvt-'+i).change(function(){
                       
                        if($(this).val() ==  'Mise en production'){
                        //    $('#zone-travail-'+$(this).attr("index")).removeClass("hide");
                        }else{
                            $('#zone-travail-'+$(this).attr("index")).addClass("hide");
                        }
                    });
                    volumeTotal += parseFloat(data[i].volume);
                }
                var td = document.createElement("td");
                
                td.innerHTML = "<p class=\"pull-right\" style=\"margin-right:50px\"><span style=\"font-weight: bold;margin-right: 10px;\">Volume total = </span>"+parseFloat(volumeTotal).toFixed(3) + 'm³</p>';
                td.setAttribute("colspan", "8");
                var tr = document.createElement("tr");
                tr.appendChild(td);
                
                document.getElementById("table-body").appendChild(tr);
                
                $('table').DataTable();
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                this.success(jqXHR.responseText, textStatus, errorThrown)
            }
        };
        
        $.ajax(settings);
    };
    
    function parseDate(string){
        var splitedDate = string.split("-");
        var date = splitedDate;
//        var time = splitedDate[1].split(":");
        return new Date(parseInt(date[0]), parseInt(date[1]), parseInt(date[2]));//, time[0], time[1]).getTime();
        
    }
    
    
window.saveData = function(refParc,libelle,numBille,typeMvt,dateMvt,refTypeparc){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return ;
    $.ajax({
            url: "/parc/add",
            method: "POST",
            dataType: 'json',
            data: {
                refparc: refParc,
                libelle: libelle,
                numBille: numBille,
                typeMvt: typeMvt,
                dateMvt: parseDate(dateMvt).getTime(),
                refTypeparc: refTypeparc
            },
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true") {
                    $('#btn-close-addPane').click();
                    loadData();
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // console.log(jqXHR);
                // console.log(errorThrown);
            },
            complete: function (jqXHR, textStatus) {
               // console.log("terminé");
            }
        });
}

    
window.editData = function(refparc,libelle,numBille,typeMvt,dateMvt,refTypeparc){
     $.ajax({
            url: "/personnel/edit",
            method: "POST",
            data: {
                refparc: refparc,
                libelle: libelle,
                numBille: numBille,
                typeMvt: typeMvt,
                dateMvt: dateMvt,
                refTypeparc: refTypeparc
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-editPane').click();
                loadData();
                // console.log(data);
                // console.log('icic');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
}

window.recherche = function(){
    
    var statut = $('#zone-statut').val();
    var dateD =  $('#zone-date-debut-parc').val();
    var dateF =  $('#zone-date-fin-parc').val();
    var origin =  $('#zone-origine').val();
        console.log('"'+dateD+'"');
        console.log('""');
     loadData('/api/parc/list/by_origine', {
        statut: statut,
        dateD: dateD === '' ? 'null' : parseDate(dateD).getTime(), 
        dateF: dateF === '' ? 'null' : parseDate(dateF).getTime(),
        origine: origin,
    });
    
    
    return false;
}

    $.ajax({
        url: '/api/type_parc/list',
        method: 'GET',
        complete: function (jqXHR, textStatus) {
            
            var liste = jqXHR.responseJSON;
            var select = document.getElementById("zone-origine");
            var o = document.createElement("option");
            o.value = '';
            o.innerHTML = "-- Selectionnez --";
            select.appendChild(o);
            
            for(var i=0; i<liste.length; i++){
                var opt = document.createElement("option");
                opt.value = liste[i].ref;
                opt.innerHTML = liste[i].libelle;
                select.appendChild(opt);
            }
        }
    });

});