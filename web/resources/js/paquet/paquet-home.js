   
   window.putPaquet = function(dataRow){
var row = document.createElement("tr"),
                
        celREFERENCE = document.createElement("td"),
        celColis = document.createElement("td"),
        celNbrFeuilles = document.createElement("td"),
        celNumTravail = document.createElement("td"),
        celCertification = document.createElement("td"),
        longueur = document.createElement("td"),
        largeur = document.createElement("td"),
        surface = document.createElement("td"),
        volume = document.createElement("td"),
        qualite = document.createElement("td"),
        celOpe = document.createElement("td"),
        
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        icon = document.createElement("i");

        celREFERENCE.innerHTML = dataRow.ref;
        celColis.innerHTML = dataRow.colis;
        celNbrFeuilles.innerHTML = dataRow.nbreFeuille;
        celNumTravail.innerHTML = dataRow.numTravail;
        longueur.innerHTML = dataRow.longueur;
        largeur.innerHTML = dataRow.largeur;
        surface.innerHTML = dataRow.surface;
        qualite.innerHTML = dataRow.qualite;
        volume.innerHTML = parseFloat(""+dataRow.volume).toFixed(3);
        
        console.log('detail');
        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
        row.appendChild(celREFERENCE);
        row.appendChild(celColis);
        row.appendChild(celNumTravail);
        row.appendChild(qualite);
        row.appendChild(celNbrFeuilles);
        row.appendChild(longueur);
        row.appendChild(largeur);
        row.appendChild(surface);
        row.appendChild(volume);
        row.appendChild(celOpe);
        
        return row;
    };
   
/* global user */

$(function(){
   
   $('#btn-addPaquet').click(function(){
      
       
       $.ajax({
            url: "/paquet/add",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                $('#modal-default #modal-body').html(data);
           },
            error: function (jqXHR, textStatus, errorThrown) {
                 $('#modal-default #modal-body').html(jqXHR.responseText);
            }
       })
   });
    if(user.can_add !== 'O')       
        $('#btn-addPaquet').remove();
   
   
   
    function getTableRow(dataRow){
        var row = document.createElement("tr"),
                
        celREFERENCE = document.createElement("td"),
        celColis = document.createElement("td"),
        celNbrFeuilles = document.createElement("td"),
        celCertification = document.createElement("td"),
        celNumTravail = document.createElement("td"),
        longueur = document.createElement("td"),
        largeur = document.createElement("td"),
        surface = document.createElement("td"),
        volume = document.createElement("td"),
        qualite = document.createElement("td"),
        date = document.createElement("td"),
        celOpe = document.createElement("td"),
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        icon = document.createElement("i");

        celREFERENCE.innerHTML = dataRow.ref;
        celColis.innerHTML = dataRow.colis;
        celNbrFeuilles.innerHTML = dataRow.nbreFeuille;
        celNumTravail.innerHTML = dataRow.numTravail;
        longueur.innerHTML = dataRow.longueur;
        largeur.innerHTML = dataRow.largeur;
        surface.innerHTML = dataRow.surface;
        qualite.innerHTML = dataRow.qualite;
        volume.innerHTML = parseFloat(""+dataRow.volume).toFixed(3);
        
        let d = new Date(dataRow.createAt);
        date.innerHTML = d.getDate()+"/"+d.getMonth()+"/"+d.getFullYear();
        
        $.ajax({
            url: "/api/certification/travail",
            method: "GET",
            data: {ref: dataRow.numTravail},
            dataType: 'json',
            async: true,
            complete: function (jqXHR, textStatus ) {
                celCertification.innerHTML = jqXHR.responseJSON.certification;
            }
        });

        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.ref;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-pencil";
        btnEdi.className = "btn btn-success btn-edit-paquet";
        btnEdi.title = 'Modifier '+dataRow.nbrFeuilles;
        btnEdi.setAttribute("data-toggle", "modal");
        btnEdi.setAttribute("data-target", "#modal-edit");
        btnEdi.appendChild(icon);
        btnEdi.onclick = function(){
            $.ajax({
                url: "/paquet/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    console.log(jqXHR);
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        
        
/*        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi); */
        row.appendChild(celREFERENCE);
        row.appendChild(celColis);
        row.appendChild(celNumTravail);
        row.appendChild(celCertification);
        row.appendChild(qualite);
        row.appendChild(celNbrFeuilles);
        row.appendChild(longueur);
        row.appendChild(largeur);
        row.appendChild(surface);
        row.appendChild(volume);
        row.appendChild(date);
//        row.appendChild(celOpe);
        return row;
    }

    window.deleteData = function(data, source){
        $.ajax({
            url: "/paquet/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('.status-del-paquet').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-paquet').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('.status-del-paquet').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-paquet').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    


window.loadData = function (){
        $("#table-body").html('');
        $.ajax({
            url: "/api/paquet/list",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                
                for(var i=0; i<data.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(data[i]));
                }
                $('#liste').DataTable();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("processus erroné");
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
                console.log("Le processus est terminé");
            }
        });
    };
    
window.saveData = function(ref, nbrFeuilles, numTravail, plot){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return ;
     $.ajax({
            url: "/paquet/add",
            method: "POST",
            data: {
                ref: ref,
                nbrFeuilles: nbrFeuilles,
                numTravail: numTravail,
                plot: plot
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-addPane').click();
                loadData();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            complete: function (jqXHR, textStatus) {
                console.log("terminé");
            }
        });
}

    
window.editData = function(ref, nbrFeuilles, numTravail, plot){
     $.ajax({
            url: "/paquet/edit",
            method: "POST",
            data: {
                ref: ref,
                nbrFeuilles: nbrFeuilles,
                numTravail: numTravail,
                plot: plot
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-editPane').click();
                loadData();
                console.log(data);
                console.log('icic');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
}

});