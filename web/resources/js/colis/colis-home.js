
/* global user */

$(function(){
      

   
   $('#btn-addColis').click(function(){
      
       
       $.ajax({
            url: "/colis/add",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                $('#modal-default #modal-body').html(data);
           },
            error: function (jqXHR, textStatus, errorThrown) {
                 $('#modal-default #modal-body').html(jqXHR.responseText);
            }
       });
   });
    if(user.can_add !== 'O')       
        $('#btn-addColis').remove();
   
   
   
    function getTableRow(dataRow){
        var row = document.createElement("tr"),
        celCode = document.createElement("span"),
        celREFRENCE = document.createElement("td"),
        celCertificat = document.createElement("td"),
        celNbrePaquet = document.createElement("td"),
        celLongueurColis = document.createElement("td"),
        celLargeurColis = document.createElement("td"),
        celPoidsColis = document.createElement("td"),
        celQualite = document.createElement("td"),
        celEssence = document.createElement("td"),
        celSurface = document.createElement("td"),
        celVolume = document.createElement("td"),
        celEmbarquement = document.createElement("td"),
        celOpe = document.createElement("td"),
        
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        btnView = document.createElement("button"),
        icon = document.createElement("i");

        var codeInput = document.createElement("svg");
        codeInput.id = 'code-'+dataRow.refColis;
        codeInput.data = dataRow.codeBarColis;
        codeInput.className = 'code-bare';
//        console.log(dataRow);
        
        celCode.innerHTML = dataRow.refColis;
        celEmbarquement.innerHTML = dataRow.embarquement;
        celCode.setAttribute("class","hide");
        celOpe.appendChild(celCode);
        celREFRENCE.innerHTML = '<svg id="code'+dataRow.refColis+'" data="'+dataRow.codeBarColis+'" class="code-bar"></svg>'; //=//appendChild(codeInput);// = dataRow.codeBarColis;
        celNbrePaquet.innerHTML = dataRow.nbrePaquet+' <span style="font-weight:bold; font-sizse:16"> - '+dataRow.isJoin+'</span>';
        celLongueurColis.innerHTML = dataRow.longueurColis;
        celLargeurColis.innerHTML = dataRow.largeurColis;
        //celCodeBarColis.innerHTML = dataRow.codeBarColis;
        celQualite.innerHTML = dataRow.qualite;
        celEssence.innerHTML = dataRow.essence === "null" || dataRow.essence === null ? "" : dataRow.essence;
        celPoidsColis.innerHTML = dataRow.poidsColis;
        //celEquipeRef.innerHTML = dataRow.equipe.libelle;
        celSurface.innerHTML = dataRow.surface;
        celVolume.innerHTML = parseFloat(dataRow.volume).toFixed(2);
        
        $.ajax({
                url: "/api/colis/certification",
                method: "GET",
                data: {ref: dataRow.refColis},
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    celCertificat.innerHTML = jqXHR.responseJSON.certification;
                }
            });
        
        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.cni;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-pencil";
        btnEdi.className = "btn btn-success btn-edit-colis";
        btnEdi.title = 'Modifier '+dataRow.nom;
        btnEdi.setAttribute("data-toggle", "modal");
        btnEdi.setAttribute("data-target", "#modal-edit");
        btnEdi.appendChild(icon);
        btnEdi.onclick = function(){
            $.ajax({
                url: "/colis/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    console.log(jqXHR);
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        
        btnView.setAttribute("class","tootltip");
        btnView.className = 'btn btn-flat';
        btnView.type = 'button';
        btnView.setAttribute("data-tooltip-content", "#tooltip-"+dataRow.refColis);
            icon = document.createElement("i");
            icon.className = "fa fa-eye";
        btnView.appendChild(icon);
        btnView.style.marginLeft = '5px';
        btnView.title = displayPopup(dataRow);
        btnView.setAttribute("data-toggle", "tooltip");
        btnView.setAttribute("data-html", "true");
        btnView.setAttribute("data-placement", "left");
        btnView.appendChild(icon);
        $(btnView).tooltip();
        $(btnView).hover(
            function() {
              var id = this.getAttribute("aria-describedBy");
                $(' #'+id+' .tooltip-inner div').css({'background': 'rgba(0,0,0,0.5)'});
            }
          );
        

        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        //if(user.can_edit === 'O')
            //celOpe.appendChild(btnEdi);
        celOpe.appendChild(btnView);
        
        row.appendChild(celREFRENCE);
        row.appendChild(celCertificat);
        row.appendChild(celNbrePaquet);
        row.appendChild(celLongueurColis);
        row.appendChild(celLargeurColis);
        row.appendChild(celPoidsColis)
        row.appendChild(celEssence);
        row.appendChild(celQualite);
        row.appendChild(celSurface);
        row.appendChild(celVolume);
        row.appendChild(celEmbarquement);
        row.appendChild(celOpe);
        return row;
    }
    
    
   function displayPopup(data){
       return '<div style="text-align: left">\
            <b>Pds Embal</b>: '+( data.poidEmballage==='null' ? '-':data.poidEmballage)+' kg<br>\
            <b>Pds Net</b>: '+( data.poidNet === 'null' ?'-':data.poidNet)+' kg<br>\
            <b>Pds Total</b>: '+(data.poidsColis==='null'?'-':data.poidsColis)+' kg<br>\
            <b>Date</b>: '+(data.dateDebut === null ? '-' : new Date(data.dateDebut).toDateString())+' <br>\
            <!--button class="btn btn-primary" type="button"><i class="fa fa-print"></i></button-->\
        </div>';
   }

    window.deleteData = function(data, source){
        $.ajax({
            url: "/colis/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('.status-del-colis').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-colis').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('.status-del-colis').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-colis').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    


window.loadData = function (donnees={}){
        $("#table-body").html('');
        $.ajax({
            url: "/api/colis/list",
            method: "GET",
            data: donnees,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {

                for(var i=0; i<data.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(data[i]));
                }
                

                $('.code-bar').each(function(){
                    JsBarcode('#'+this.id,this.getAttribute("data"), {displayValue: true});
                    document.getElementById(this.id,this.getAttribute("data")).setAttribute("width", "150px");
                });
                $('.tooltip').click(function(){
                    var id = this.getAttribute("aria-describedBy");
                    $('#'+id).css('width', '500px');
                    console.log($('#'+id));
                    tooltipster();
                });
                
                $('#liste').DataTable({
                    "lengthMenu": [[100, 250, 500, -1], [100, 250, 500, "All"]]
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("processus LOAD COLIS erroné");
                console.log(jqXHR.responseText);
            }
        });
    };
    
window.saveData = function(ref, dateDebut, dateFermeture, dateFin,poidsEmballage,poidsNet,nbrePaquet,longueurColis,largeurColis,codeBarColis,poidsColis,
equipeRef,volume){
        if(confirm('Voulez-vous vraiment enregistrer?') === false)
            return ;
     $.ajax({
            url: "/colis/add",
            method: "POST",
            data: {
                ref: ref,
                dateDebut: dateDebut,
                dateFermeture: dateFermeture,
                dateFin: dateFin,
                poidsEmballage: poidsEmballage,
                poidsNet:poidsNet,
                nbrePaquet: nbrePaquet,
                longueurColis:longueurColis,
                largeurColis:largeurColis,
                codeBarColis:codeBarColis,
                poidsColis:poidsColis,
                equipeRef:equipeRef,
                volume:volume
                
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-addPane').click();
                loadData();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            complete: function (jqXHR, textStatus) {
                console.log("terminé");
            }
        });
};

    
window.editData = function(ref, dateDebut, dateFermeture, dateFin,poidsEmballage,poidsNet,nbrePaquet,longueurColis,largeurColis,codeBarColis,poidsColis, equipeRef,volume){
     $.ajax({
            url: "/colis/edit",
            method: "POST",
            data: {
                ref: ref,
                dateDebut: dateDebut,
                dateFermeture: dateFermeture,
                dateFin: dateFin,
                poidsEmballage: poidsEmballage,
                poidsNet: poidsNet,
                nbrePaquet:nbrePaquet,
                longueurColis:longueurColis,
                largeurColis:largeurColis,
                codeBarColis:codeBarColis,
                poidsColis:poidsColis,
                equipeRef:equipeRef,
                volume:volume
                
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-editPane').click();
                loadData();
                console.log(data);
                console.log('icic');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
};

});