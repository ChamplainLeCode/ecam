/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function(){
    window.$ = $;
    window.saveData = function saveData(ref, libelle){
        if(confirm('Voulez-vous vraiment enregistrer?') === false)
            return ;
        $.ajax({
            data: {
                ref: ref,
                libelle: libelle,
                
            },
            dataType: 'json',
            url: "/typecontenaire/add",
            method: 'POST',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('#form-add-typecontenaire .overlay').addClass('hide');
                    $('#form-add-typecontenaire #btn-save-typecontenaire').removeClass("disabled");
                    $('.status-add-typecontenaire').removeClass('bg-red .disabled').addClass('bg-green disabled').html('Enregistré');
                    $('#form-add-typecontenaire #zone-ref').val('REF_TYPECONT_'+(new Date()).getTime());
                    $('#form-add-typecontenaire #zone-libelle').val('');
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('#form-add-typecontenaire .overlay').addClass('hide');
                    $('#form-add-typecontenaire #btn-save-typecontenaire').removeClass("disabled");
                    $('.status-add-typecontenaire').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason);
                }else{
                    this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };

    $('#btn-addTypecontenaire').click(function(e){
        $.ajax({
            url: "/typecontenaire/add",
            method: "GET",
            success: function (data, textStatus, jqXHR) {
                $('#modal-default #modal-body').html(data);
            },
            complete: function (jqXHR, textStatus ) {
            }
        });
    });
    if(user.can_add !== 'O')       
        $('#btn-addTypecontenaire').remove();
   
   
    
    function getTableRow(dataRow){
        var row = document.createElement("tr"),
        celRef = document.createElement("td"),
        celLibelle = document.createElement("td"),
        celOpe = document.createElement("td"),
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        icon = document.createElement("i");

        celRef.innerHTML = dataRow.ref;
        celLibelle.innerHTML = dataRow.libelle;
        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.ref;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-pencil";
        btnEdi.className = "btn btn-success btn-edit-typecontenaire";
        btnEdi.title = 'Modifier ';
        btnEdi.appendChild(icon);
        btnEdi.onclick = function(){
            $.ajax({
                url: "/typecontenaire/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        
        

        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
        row.appendChild(celRef);
        row.appendChild(celLibelle);
        row.appendChild(celOpe);
        return row;
    }
    
    window.loadData = function(){
        $('#table-body').html('');
        $.ajax({
            url: "/api/type_contenaire/list",
            method: "GET",
            success: function (donnees, textStatus, jqXHR) {
                var data = (donnees);
                for(var i=0; i<data.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(data[i]));
                }
            },
            complete: function (jqXHR, textStatus ) {
                $('#liste').DataTable();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            }
        });
    };
    window.deleteData = function(data, source){
        $.ajax({
            url: "/typecontenaire/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat == "true"){
                    $('.status-del-typecontenaire').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-typecontenaire').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                    console.log(source.parentNode.parentNode);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat == "false"){
                    $('.status-del-typecontenaire').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-typecontenaire').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    
    window.editData = function( ref, libelle){
        $.ajax({
            url: "/typecontenaire/edit",
            method: "POST",
            data: {ref: ref, libelle: libelle},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat == "true"){
                    $('#form-edit-typecontenaire .overlay').addClass('hide');
                    $('#form-edit-typecontenaire #btn-edit-typecontenaire').removeClass("disabled");
                    $('.status-edit-typecontenaire').removeClass('bg-red .disabled').addClass('bg-green disabled').html('Modifié');
                    loadData();
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat == "false"){
                    $('#form-edit-typecontenaire .overlay').addClass('hide');
                    $('#form-edit-typecontenaire #btn-edit-typecontenaire').removeClass("disabled");
                    $('.status-edit-typecontenaire').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
});


