
/* global user */

$(function(){
   
   $('#btn-addParcChargement').click(function(){
      
       
       $.ajax({
            url: "/parcchargement/add",
            method: "GET",
            data: {},
            dataType: 'json', 
            complete: function (data, textStatus, jqXHR) {
                $('#modal-default #modal-body').html(data.responseText);
            }
       });
   });
       if(user.can_add !== 'O')       
        $('#btn-addParcChargement').remove();
   
   

    function getTableRow(dataRow){
        var row = document.createElement("tr"),
                
        celREFERENCE = document.createElement("td"),
        celLibelle = document.createElement("td"),
        celOrigine = document.createElement("td"),
        celFournisseurRef = document.createElement("td"),
        celCertificat = document.createElement("td"),
        celOpe = document.createElement("td"),
        
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        icon = document.createElement("i");

        celREFERENCE.innerHTML = dataRow.ref;
        celLibelle.innerHTML = dataRow.libelle;
        celOrigine.innerHTML = (dataRow.origine === null ? '' : dataRow.origine.libelle);
        celFournisseurRef.innerHTML = dataRow.fournisseur.nomFournisseur+' ('+dataRow.fournisseur.contact+")";
            celCertificat.innerHTML += "<span>"+dataRow.certificat+"</span><br>";
        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.ref;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-pencil";
        btnEdi.className = "btn btn-success btn-edit-REFERENCE";
        btnEdi.title = 'Modifier '+dataRow.libelle;
        btnEdi.setAttribute("data-toggle", "modal");
        btnEdi.setAttribute("data-target", "#modal-edit");
        btnEdi.appendChild(icon);
        btnEdi.onclick = function(){
            $.ajax({
                url: "/parcchargement/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        
        
        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
        row.appendChild(celREFERENCE);
        row.appendChild(celLibelle);
        row.appendChild(celOrigine);
        row.appendChild(celFournisseurRef);
        row.appendChild(celCertificat);
        row.appendChild(celOpe);
        return row;
    }

    window.deleteData = function(data, source){
        $.ajax({
            url: "/REFERENCE/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('.status-del-REFERENCE').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-REFERENCE').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('.status-del-REFERENCE').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-REFERENCE').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    


window.loadData = function (){
        $("#table-body").html('');
        $.ajax({
            url: "/api/parc_chargement/list",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                
                for(var i=0; i<data.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(data[i]));
                }
                            $('table').DataTable();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("processus erroné");
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
                console.log("Le processus est terminé");
            }
        });
    };
    
window.saveData = function(ref, libelle, origine, fournisseur, certificat, 
                            titre_ref, titre_concession, titre_dateDebut, titre_dateFin, titreEssence){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return ;
     $.ajax({
            url: "/parcchargement/add",
            method: "POST",
            data: {
                ref: ref,
                libelle: libelle,
                fournisseur: fournisseur,
                certificat: certificat,
                origine: origine,
                titreRef: titre_ref,
                concession: titre_concession,
                dateDebut: titre_dateDebut,
                dateFin: titre_dateFin,
                essence: titreEssence               
            },
            dataType: 'json',
            complete: function (jqXHR, textStatus) {
                $('#liste tbody').html('');
                $('#btn-close-addPane').click();
                loadData();
            }
        });
}

    
window.editData = function(ref, libelle, origine, fournisseurRef, certificat, titre, essence){
     $.ajax({
            url: "/parcchargement/edit",
            method: "POST",
            data: {
                ref: ref,
                libelle: libelle,
                fournisseur: fournisseurRef,
                certificat: certificat,
                origine: origine,
                titre: titre,
                essences: essence
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-editPane').click();
                loadData();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
}

});