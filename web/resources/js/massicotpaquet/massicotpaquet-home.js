
$(function(){
   
   $('#btn-addMassicotPaquet').click(function(){
      
       
       $.ajax({
            url: "/massicotpaquet/add",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                $('#modal-default #modal-body').html(data);
           },
            error: function (jqXHR, textStatus, errorThrown) {
                 $('#modal-default #modal-body').html(jqXHR.responseText);
            }
       })
   });
    if(user.can_add !== 'O')       
        $('#btn-addMassicotPaquet').remove();
   
   
   function displayPopup(data){
       return '<ul style="text-align:left; font-size:19px; width: 300px; margin-right: 30px;">\
            <li>Longueur: '+data.longueur+'cm</li>\
            <li>Largeur: '+data.largeur+'cm</li>\
            <li>Épaisseur: '+data.epaisseur+'mm</li>\
            <li>Qualite: '+data.qualite+'</li>\
            <li>'+data.paquet.nbreFeuille+' Feuilles/Paquet</li>\
        </ul>';
   }
   
    function getTableRow(dataRow){
        var row = document.createElement("tr"),
                
        celREFERENCE = document.createElement("td"),
        //celNumTravail = document.createElement("td"),
        celMassicot = document.createElement("td"),
        celFace1 = document.createElement("td"),
        celFace2 = document.createElement("td"),
        celFace3 = document.createElement("td"),
        celPaquet = document.createElement("td"),
        celOpe = document.createElement("td"),
        
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        btnView = document.createElement("button"),
        icon = document.createElement("i");

        var total = dataRow.face1+dataRow.face2+dataRow.face3;

        celREFERENCE.innerHTML = dataRow.ref;
        //celNumTravail.innerHTML = dataRow.numTravail;
        celMassicot.innerHTML = dataRow.massicot;
        celPaquet.innerHTML = total > 0 ? total+ " Paquets" : total+" Paquet";
        celFace1.innerHTML = dataRow.face1+(dataRow.face1 > 0 ? " Paquets" : " Paquet");
        celFace2.innerHTML = dataRow.face2+(dataRow.face2 > 0 ? " Paquets" : " Paquet");
        celFace3.innerHTML = dataRow.face3+(dataRow.face3 > 0 ? " Paquets" : " Paquet");
        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.ref;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        
        btnView.type = "button";
        btnView.value = "Edit";
            icon = document.createElement('i');
            icon.className = "fa fa-eye";
        btnView.setAttribute("style","margin-left:5px;");
        btnView.className = "btn btn-secondary btn-primary";
        btnView.title = displayPopup(dataRow);
        btnView.setAttribute("data-toggle", "tooltip");
        btnView.setAttribute("data-html", "true");
        btnView.setAttribute("data-placement", "top");
        btnView.appendChild(icon);
        $(btnView).tooltip();
        btnView.setAttribute("data-toggle", "modal");
        btnView.setAttribute("data-target", "#modal-view");
        btnView.onclick = function(){
        
        };
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-pencil";
        btnEdi.className = "btn btn-success btn-edit-massicotpaquet";
        btnEdi.title = 'Modifier '+dataRow.numTravail;
        btnEdi.setAttribute("data-toggle", "modal");
        btnEdi.setAttribute("data-target", "#modal-edit");
        btnEdi.appendChild(icon);
        btnEdi.onclick = function(){
            $.ajax({
                url: "/massicotpaquet/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    console.log(jqXHR);
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        
        
        celOpe.appendChild(btnDel);
        celOpe.appendChild(btnEdi);
        celOpe.appendChild(btnView);
        row.appendChild(celREFERENCE);
        //row.appendChild(celNumTravail);
        row.appendChild(celMassicot);
        row.appendChild(celPaquet);
        row.appendChild(celFace1);
        row.appendChild(celFace2);
        row.appendChild(celFace3);
        row.appendChild(celOpe);
        return row;
    }

    window.deleteData = function(data, source){
        $.ajax({
            url: "/massicotpaquet/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            beforeSend: function (xhr) {
                var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
            },
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('.status-del-massicotpaquet').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-massicotpaquet').hide(1000);},4000);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('.status-del-massicotpaquet').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-massicotpaquet').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    

function groupByNumTravail(data = []){
    
    
    if(data.length <= 0)
        return [];
    
    var collection = {};
    
    for(var i=0; i<data.length; i++){
        var ref = data[i].ref.split('-Face')[0];
        var map = collection[ref];
        var idFace = parseInt(data[i].paquet.ref.split("Face")[1].split("-")[0]);

        if(map === undefined){
            map = {
                ref: ref,
                travail: data[i].numTravail,
                massicot: data[i].massicot.libelle,
                face1: idFace === 1 ? 1 : 0,
                face2: idFace === 2 ? 1 : 0,
                face3: idFace === 3 ? 1 : 0,
                paquet: data[i].paquet,
                longueur: data[i].longueur,
                largeur: data[i].largeur,
                epaisseur: data[i].epaisseur,
                qualite: data[i].qualite
            };
            collection[ref] = map;
        }else{
            switch (idFace){
                case 1: map.face1++;break;
                case 2: map.face2++;break;
                case 3: map.face3++;break;
            }
        }
//        i
/*        
*/        
    }
    var valeurs = [];
    for(var key in collection)
        valeurs[valeurs.length] = collection[key];
    return valeurs;
}

window.loadData = function (){
        $("#table-body").html('');
        $.ajax({
            url: "/api/massicot_paquet/list",
            method: "GET",
            data: {},
            dataType: 'json',
            beforeSend: function (xhr) {
                var table = document.getElementById("table-body");
                var tr = document.createElement("tr");
                var td = document.createElement("td");
                td.colspan = "7";
                td.style.width = "500%;";
                table.innerHTML = '\
                    <div class="overlay" style="text-align:center;">\
                        <h1><i class="fa fa-refresh fa-spin"></i> </h1> \
                        <h1>Veuillez patienter s\'il vous plaît </h1> \
                    </div>';
                //tr.appendChild(td);
                //table.appendChild(tr);
            },
            success: function (data, textStatus, jqXHR) {
                var donnees = groupByNumTravail(data);
                document.getElementById("table-body").innerHTML = '';
                for(var i=0; i<donnees.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(donnees[i]));
                }
                           // $('table').DataTable();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("processus erroné");
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
                console.log("Le processus est terminé");
            }
        });
    };
    
window.saveData = function(data){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return ;
     $.ajax({
            url: "/massicotpaquet/add",
            method: "POST",
            data: {data: data, taille: data.length},
            dataType: 'json',
            beforeSend: function (xhr) {
                $('#btn-close-addPane').click();
            },
            success: function (data, textStatus, jqXHR) {
                loadData();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            complete: function (jqXHR, textStatus) {
                console.log("terminé");
            }
        });
}

    
window.editData = function(ref, numTravail, massicot, paquet){
     $.ajax({
            url: "/massicotpaquet/edit",
            method: "POST",
            data: {
                ref: ref,
                numTravail: numTravail,
                massicot: massicot,
                paquet: paquet
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-editPane').click();
                loadData();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
}

});