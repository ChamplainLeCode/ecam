
        /* global SheetClip, parseFloat */

$('#zone-certif').val('');
$('#zone-epaisseur').val('');


var donneesPaquet =  Array(1).fill().map(()=>Array('<input type="checkbox" name="selectedPaquet">','','','','','0','0', '','')),//Array(9).fill('')),
            donneesColis =  Array(1).fill().map(()=>Array('<input type="radio" name="currentColis">','','','','0', '0',false)),
            tableurPaquet,
            tableurColis,
            keys = [];
        
        $('body').css("font-size","12px");
        
        $('#btn-tableur_colis-add-row').click(function(){
            addRow(tableurColis, 'colis');
        });
        
        $$('btn-tableur_paquet-add-row').onclick = function(){
            addRow(tableurPaquet, 'paquet');
        };
        
        $('#btn-tableur_colis-see-row').click(function(){
            if($('#zone-atelier').val() === ''){
                showError('Veuillez sélectionner votre atélier');
                return;
            }
           $('#section-massicotage').hide();
           $('#section-visualisation').show();
           // Chargement des données de la bd Locale
           printVisual();
        });
        
        $('#btn-tableur_colis-unsee').click(function(){
           $('#section-massicotage').show();
           $('#section-visualisation').hide();
        });
        
        $('#btn-vider').click(function(){
            
            localStorage.setItem('essence_colis', '[]');
            localStorage.setItem('massicotage', '[]');
            printVisual();
            location.href = location.href;
        })
        
        $('#btn-terminer').click(function(){
            if(confirm('Voulez-vous vraiment enregistrer ces données?') === true){
                if(donneesPretes.length === 0){
                    showVisualizationInfo('Aucune donnée à enregistrer');
                    return;
                }
                showVisualizationInfo('Enregistrement encours');
                if(donneesPretes.length === 0){
                    showVisualizationInfo('Aucune donnée à enregistrer');
                    return;
                }
                
                $.ajax({
                    url: '/massicot_paquet/add/massicotage',
                    method: 'POST',
                    data: {data: donneesPretes, taille: donneesPretes.length, atelier: $('#zone-atelier').val()},
                    dataType: 'json',
                    complete: function (jqXHR, textStatus) {
                        var response = jqXHR.responseJSON;
                        if(response !== undefined && response.result === true){
                            emptyMassicotage();
                            printVisual();
                            showVisualizationInfo('Données enregistrées');
                        }else{
                            emptyMassicotage();
                            showVisualizationInfo('Tentative de création des paquets existants');
                        }
                    }
                });
            }else{
                showVisualizationInfo("Enregistrement annulé");
            }
        });
        if(user.can_add !== 'O')       
            $('#btn-terminer').remove();
   
   
        
        $('#btn-load-colis').click(function(){
            setColisOuverts($('#zone-atelier').val());
        });
        
        function emptyMassicotage(){
            localStorage.setItem('massicotage', '[]');
            localStorage.setItem('essence_colis', '[]');
        }
        
        function showVisualizationInfo(msg){
            $('#zone-visualization-error').html(msg).show(500);
            setTimeout(()=>$('#zone-visualization-error').html('').hide(500), 15000);
        }
        
        /*
         * Contient l'ensemble de numéro de travail dans le stockage
         * @type Array
         * 
         */
        var travailYetHere = [];
        var lastTravailSelected = -1;
        (function setTravail(){
            $.ajax({
                url: '/api/bille/travail_for_massicot',
                method: '',
                complete: function (jqXHR, textStatus ) {
                    var select = $$("zone-bille");
                    var data = jqXHR.responseJSON;
                    var opt = document.createElement("option");
                    
                    if(data === undefined)
                        return;
                    opt.value = "";
                    opt.innerText = "--      --";
                    opt.setAttribute("style","font-size:10px;");
                    select.appendChild(opt);
                    for(var i = 0; i<data.length; i++){
                        if(travailYetHere[data[i][0]] !== undefined){
                            travailYetHere[data[i][0]].plots.push(data[i][1]);
                            continue;
                        }
                        travailYetHere[data[i][0]] = {travail: data[i][0], plots: [data[i][1]], essence: data[i][2], epaisseur: data[i][3]};
                        opt = document.createElement("option");
                        opt.value = opt.innerText = data[i][0];
                        opt.setAttribute("essence", data[i][2]);
                        opt.setAttribute("plot", data[i][1]);
                        select.appendChild(opt);
                    }
                    
                    select.onchange = function(){
                        var liste = document.getElementById('list-plots');
                        liste.innerHTML = '';
                        $.ajax({
                            url: '/api/certification/travail',
                            data: {ref: travailYetHere[this.value].travail},
                            dataType: 'json',
                            complete: function (jqXHR, textStatus) {
                                $('#zone-certif').val(jqXHR.responseJSON.certification);
                            }
                        });
                        try{
                            for(var i=0; i<lastTravailSelected; i++)
                                deleteRow(tableurPaquet, "tableur", 0);
                        }catch(e){
                            
                        }
                        if(this.value === ''){
//                            console.log('No essence = '+this.value);
                            $('#zone-epaisseur').val('');
                            setEssence('');
                            return;
                        }else
                            setEssence(travailYetHere[this.value].essence);
                        //donneesPaquet = [];
                        //tableurPaquet.render();
                        
                        lastTravailSelected = 0;
                        
                            // On vide le tableau de paquet
                            $('#tableur-paquet-body').html('');
                        for(var i=0; i<travailYetHere[this.value].plots.length; i++){
                            var li = document.createElement('span');

                            li.innerHTML = "<label style=\"margin-left: 5px\" class=\"label label-default\">"+travailYetHere[this.value].plots[i]+"</label>";
                            addRow(tableurPaquet, "paquet", {ref: travailYetHere[this.value].plots[i]});
                            liste.appendChild(li);
                            lastTravailSelected++;
                        }
                        $('#zone-epaisseur').val(travailYetHere[this.value].epaisseur);
                    };
                }
            });
        })();
        
        
        
        function setColisOuverts(atelier){
            if($('#zone-atelier').val() === ''){
                showError('Veuillez sélectionner votre atélier');
                return;
            }
            
            $('#tableur-colis-body').html("");
            $.ajax({
                url: '/api/colis/opened/atelier',
                method: 'GET',
                data: {atelier: atelier},
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    var data = jqXHR.responseJSON;
                    
                    if(data === null || data.length === 0)
                        return;
                    for(var i=0; i<data.length; i++)
                        addRow(tableurColis, "colis", data[i]);
                }
            });
        };
        document.getElementById('zone-atelier').onchange = function(){setColisOuverts($(this).val());};
        
        function setEssence(refEssence){
            $.ajax({
                url: '/api/essence',
                data: {ref: refEssence},
                dataType: 'json',
                method: '',
                complete: function (jqXHR, textStatus ) {
                    var select = $$("zone-essence");
                    var data = jqXHR.responseJSON;
                    var opt;
                    select.innerHTML = '';
                    if(data.ref === undefined)
                        return;
                    if(data !== null && data.ref !== undefined){
                        opt = document.createElement("option");
                        opt.value = data.ref;
                        opt.innerText = data.libelle;
                        select.appendChild(opt);
                    }
                }
            });
        };
        
        
        var essenceList = [];
        function loadEssence(){
            $.ajax({
                url: '/api/essence/base/list',
                method: '',
                complete: function (jqXHR, textStatus ) {
                    essenceList = jqXHR.responseJSON === undefined ? [] : jqXHR.responseJSON;
                }
            });
        };

        $$("zone-bille").onchange = function(){
            var essence = this.options[this.selectedIndex].getAttribute("essence");
            for (var i = 0; i<donneesPaquet.length; i++) {
               // donneesPaquet[i][0] = this.value;
            }
            //setEssence(essence);
            
        };

        function deleteRow(tableur, name, idRow = null){
            tableur.alter("remove_row",(idRow === null ? 1 : idRow));
            //tableur.render();
            save(tableur.getData(), name);
            /*if(name === 'colis')
                colisIndex--;
            else if(name === 'paquet')
                paquetIndex--;
            */
            //setLastOperation();
        }
        
        var colisIndex = 1;
        var paquetIndex = 1;
        
        
        function addRow(tableur, name, data=null){
                console.log(data);
            if(name === 'colis'){
                // On conserve l'état des boutons radio sur les colis
                //keepColisChecked();
                var id = colisIndex;
/*                donneesColis.reverse()[tableur.getData().length] = [
                    '<input data="'+(data !== null ?data.refColis : 'ref')+'" type="radio" name="currentColis" id="colis_row_'+(id)+'">',
                    (data!== null ? data.refColis.split('-')[1] : ''),
                    (data!== null ? data.longueurColis : ''),
                    (data!== null ? data.largeurColis : ''),
                    (data!== null ? data.qualite : ''),
                    (data!== null ? data.nbrePaquet : '0'),
                    (data!== null ? parseFloat(data.surface).toFixed(2) : '0'),
                    '<input type="checkbox" id="check_colis_'+id+'" class="check_close_colis">'
                ];
*/
                    var tr, td, checkbox, colis, nombre, longueur, largeur, qualite, surface, fermer, del;
                    tr = document.createElement("tr");
                        tr.id = "row-paquet-"+(paquetIndex++);
                    checkbox = document.createElement("input");
                        checkbox.type = "radio";
                        checkbox.name = "currentColis";
                        checkbox.id = "colis_row_"+(id);
                        checkbox.setAttribute("data",(data !== null ?data.refColis : 'ref'));
                        $(checkbox).css(["width", 'height', 'border-radius'], ["100%", "100%", "0px"]);
                    td = document.createElement("td");
                        $(td).css("width", "50px");
                        td.appendChild(checkbox);
                    $(td).css({border: '1px solid black'});
                    tr.appendChild(td);
                    
                    colis = document.createElement("input");
                        colis.type = "input";
                        colis.value = (data!== null ? data.refColis.split('-')[1] : '');
                        $(colis).css({width:"100px", height:"50px", 'border':"0px", 'text-align':'center'});
                    td = document.createElement("td");
                        $(td).css(["width", 'height', "border-radius", 'border', 'text-align'], ["100px", '50px', "0px","none", 'center']);
                    $(td).css({border: '1px solid black'});
                        td.appendChild(colis);
                    tr.appendChild(td);
                    
                    longueur = document.createElement("input");
                        longueur.type = "number";
                        longueur.min = "1";
                        longueur.value = (data!== null ? data.longueurColis : '');
                        $(longueur).css({width:"100px", height:"50px", 'border':"0px", 'text-align':'center'});
                    td = document.createElement("td");
                        $(td).css(["width", 'height', 'border', 'text-align'], ["100%", "50px", "0px", 'center']);
                        $(td).css({border: '1px solid black'});
                    td.appendChild(longueur);
                    tr.appendChild(td);
                    
                    largeur = document.createElement("input");
                        largeur.type = "number";
                        largeur.min = "1";
                        largeur.value = (data!== null ? data.largeurColis : '');
                        $(largeur).css({width:"100px", height:"50px", 'border':"0px", 'text-align':'center'});
                    td = document.createElement("td");
                        $(td).css(["width", 'height', 'border', 'text-align'], ["100%", "50px", "0px", 'center']);
                        $(td).css({border: '1px solid black'});
                        td.appendChild(largeur);
                    tr.appendChild(td);
                    
                    qualite = document.createElement("input");
                        qualite.type = "text";
                        qualite.value = (data!== null ? data.qualite : '');
                        $(qualite).css({width:"100px", height:"50px", 'border':"0px", 'text-align':'center'});
                    td = document.createElement("td");
                        $(td).css({border: '1px solid black'});
                        $(td).css(["width", 'height', 'border', 'text-align'], ["100%", "50px", "0px", 'center']);
                        td.appendChild(qualite);
                    tr.appendChild(td);
                    
                    nombre = document.createElement("input");
                        nombre.type = "number";
                        nombre.min = "1";
                        nombre.setAttribute("readonly","");
                        nombre.value = (data!== null ? data.nbrePaquet : '0');
                        $(nombre).css({border: '1px solid black'});
                        $(nombre).css({width:"100px", height:"50px", 'border':"0px", 'text-align':'center'});
                    td = document.createElement("td");
                        $(td).css(["width", 'height', 'border', 'text-align'], ["100%", "50px", "0px", 'center']);
                        $(td).css({border: '1px solid black'});
                        td.appendChild(nombre);
                    tr.appendChild(td);
                    
                    surface = document.createElement("input");
                        surface.type = "text";
                        surface.min = "1";
                        surface.setAttribute("readonly","");    
                        surface.value = (data!== null ? parseFloat(data.surface).toFixed(2) : '0');
                        $(surface).css({width:"100px", height:"50px", 'border':"0px", 'text-align':'center'});
                    td = document.createElement("td");
                        $(td).css(["width", 'height', 'border', 'text-align'], ["100%", "50px", "0px", 'center']);
                        $(td).css({border: '1px solid black'});
                        td.appendChild(surface);
                    tr.appendChild(td);
                    
                fermer = document.createElement("input");
                        fermer.type = "checkbox";
                        fermer.id = "check_colis_"+(id);
                        fermer.setAttribute("class","check_close_colis");
                        $(fermer).css({border: '1px solid black'});
                        $(fermer).css(["width", 'height', 'border-radius'], ["100%", "100%", "0px"]);
                    td = document.createElement("td");
                        $(td).css("width", "50px");
                        $(td).css({border: '1px solid black'});
                        td.appendChild(fermer);
                    tr.appendChild(td);
                    
                fermer.onchange = function(){
                    tr.style.display = "none";
                };
                    del = document.createElement("btn");
                        $(del).attr("class", "btn btn-danger");
                        $(del).attr("type", "button");
                        $(del).html("<i class=\"fa fa-trash\"></i>");
                        $(del).click(function(){
                           $(tr).remove(); 
                        });
                    td = document.createElement("td");
                        td.appendChild(del);
                    tr.appendChild(td);
                    

                    document.getElementById("tableur-colis-body").appendChild(tr);

                //save(donneesColis, name);
                colisIndex++;
                // On restaure l'état des boutons radio sur les colis
//                restaureColisChecked(1, donneesColis.length);
            }
            else if(name === 'paquet'){
                    var tr, td, checkbox, billon, plot, face, paquet, feuil, longueur, largeur, qualite, surface, del;
                    tr = document.createElement("tr");
                        tr.id = "row-paquet-"+(paquetIndex++);
                    checkbox = document.createElement("input");
                        checkbox.type = "checkbox";
                        checkbox.checked = true;
                        checkbox.setAttribute("name", "selectedPaquet");
                        $(checkbox).css(["width", 'height', 'border-radius'], ["100%", "100%", "0px"]);
                    td = document.createElement("td");
                        $(td).css("width", "50px");
                        $(td).css({border: '1px solid black'});
                        td.appendChild(checkbox);
                    tr.appendChild(td);
                    
                    billon = document.createElement("input");
                        billon.type = "input";
                        billon.value = (data!== null ? data.ref.split('-')[0]+'-'+data.ref.split('-')[1]+'-'+data.ref.split('-')[2] : '');
                        $(billon).css({width:"100px", height:"50px", 'border':"0px", 'text-align':'center'});
                    td = document.createElement("td");
                        $(td).css(["width", 'height', "border-radius", 'border', 'text-align'], ["100px", '50px', "0px","none", 'center']);
                        $(td).css({border: '1px solid black'});
                    td.appendChild(billon);
                    tr.appendChild(td);
                                        
                    plot = document.createElement("input");
                        plot.type = "input";
                        plot.value = (data!==null ? data.ref.split('-')[3] : '');
                        $(plot).css({width:"100px", height:"50px", 'border':"0px", 'text-align':'center'});
                    td = document.createElement("td");
                        $(td).css(["width", 'height', 'border', 'text-align'], ["100%", "50px", "0px", 'center']);
                        $(td).css({border: '1px solid black'});
                    td.appendChild(plot);
                    tr.appendChild(td);
                    
                    face = document.createElement("input");
                        face.type = "number";
                        face.min = "1";
                        $(face).css({width:"100px", height:"50px", 'border':"0px", 'text-align':'center'});
                    td = document.createElement("td");
                        $(td).css({border: '1px solid black'});
                        $(td).css(["width", 'height', 'border', 'text-align'], ["100%", "50px", "0px", 'center']);
                        td.appendChild(face);
                    tr.appendChild(td);
                    
                    paquet = document.createElement("input");
                        paquet.type = "number";
                        paquet.min = "1";
                        $(paquet).css({width:"100%", height:"50px", 'border':"0px", 'text-align':'center'});
                    td = document.createElement("td");
                        $(td).css({"width":"100%", 'height':"50px", 'border':"0px", 'text-align':'center'});
                        $(td).css({border: '1px solid black'});
                        td.appendChild(paquet);
                    tr.appendChild(td);
                    
                    feuil = document.createElement("input");
                        feuil.type = "number";
                        feuil.min = "1";
                        $(feuil).css({width:"100px", height:"50px", 'border':"0px", 'text-align':'center'});
                    td = document.createElement("td");
                        $(td).css(["width", 'height', 'border', 'text-align'], ["100%", "50px", "0px", 'center']);
                        $(td).css({border: '1px solid black'});
                        td.appendChild(feuil);
                    tr.appendChild(td);
                    
                    longueur = document.createElement("input");
                        longueur.type = "number";
                        longueur.min = "1";
                        longueur.value = "0";
                        $(longueur).css({width:"100px", height:"50px", 'border':"0px", 'text-align':'center'});
                    td = document.createElement("td");
                        $(td).css(["width", 'height', 'border', 'text-align'], ["100%", "50px", "0px", 'center']);
                        $(td).css({border: '1px solid black'});
                        td.appendChild(longueur);
                    tr.appendChild(td);
                    
                    largeur = document.createElement("input");
                        largeur.type = "number";
                        largeur.min = "1";
                        largeur.value = "0";
                        $(largeur).css({width:"100px", height:"50px", 'border':"0px", 'text-align':'center'});
                    td = document.createElement("td");
                        $(td).css(["width", 'height', 'border', 'text-align'], ["100%", "50px", "0px", 'center']);
                        $(td).css({border: '1px solid black'});
                        td.appendChild(largeur);
                    tr.appendChild(td);
                    
                    qualite = document.createElement("input");
                        qualite.type = "text";
                        $(qualite).css({width:"100px", height:"50px", 'border':"0px", 'text-align':'center'});
                    td = document.createElement("td");
                        $(td).css(["width", 'height', 'border', 'text-align'], ["100%", "50px", "0px", 'center']);
                        $(td).css({border: '1px solid black'});
                        td.appendChild(qualite);
                    tr.appendChild(td);
                    
                    surface = document.createElement("input");
                        surface.type = "number";
                        surface.min = "1";
                        $(surface).css({width:"100px", height:"50px", 'border':"0px", 'text-align':'center'});
                    td = document.createElement("td");
                        $(td).css(["width", 'height', 'border', 'text-align'], ["100%", "50px", "0px", 'center']);
                        $(td).css({border: '1px solid black'});
                        td.appendChild(surface);
                    tr.appendChild(td);
                    
                    /**
                     * En cas de changement de la longueur ou de la largeur
                     */
                    largeur.onchange = longueur.onchange = function(){
                        surface.value = parseFloat((parseFloat(longueur.value)*parseFloat(largeur.value))+"").toFixed(2);
                    };
            
                    var colorBorder = function(){
                        $(this).css("border", "solid 1px red");
                    };
                    var unColorBorder = function(){
                        $(this).css("border", "none");
                    }
                
                    surface.readonly="readonly";
                    $(billon).focusin(colorBorder);
                    $(plot).focusin(colorBorder);
                    $(face).focusin(colorBorder);
                    $(paquet).focusin(colorBorder);
                    $(feuil).focusin(colorBorder);
                    $(longueur).focusin(colorBorder);
                    $(largeur).focusin(colorBorder);
                    $(qualite).focusin(colorBorder);
                    $(surface).focusin(colorBorder);
                    $(billon).focusout(unColorBorder);
                    $(plot).focusout(unColorBorder);
                    $(face).focusout(unColorBorder);
                    $(paquet).focusout(unColorBorder);
                    $(feuil).focusout(unColorBorder);
                    $(longueur).focusout(unColorBorder);
                    $(largeur).focusout(unColorBorder);
                    $(qualite).focusout(unColorBorder);
                    $(surface).focusout(unColorBorder);
                    del = document.createElement("btn");
                        $(del).attr("class", "btn btn-danger");
                        $(del).attr("type", "button");
                        $(del).html("<i class=\"fa fa-trash\"></i>");
                        $(del).click(function(){
                           $(tr).remove(); 
                        });
                    td = document.createElement("td");
                        td.appendChild(del);
                    tr.appendChild(td);

                    document.getElementById("tableur-paquet-body").appendChild(tr);
 //               save(donneesPaquet, name);
            }
            
            
/*            
            //setLastOperation();
            $('div[id="hot-display-license-info"]').each(function(){
                $(this).attr('class', 'hide'); 
             });
             $('#tableur').css('.height', '80%');
*/
        }
                
        var i=0;
        var clipboardCache = '';
        var sheetclip = SheetClip;
        var isRowChecked = [];
        
        function setPaquetTableur(){
            var container = document.getElementById('tableur-paquet');
            /*tableurPaquet = new Handsontable(container, {
                data: donneesPaquet,
                colWidths: [50, 90, 70, 70, 70, 70, 90, 70, 70, 90],
                rowHeaders: false,
                width: '100%',
                manualColumnResize: true,
                colHeaders: ["Selec.","Billon", "Plot", "Face", "N° Paquet", "Nbr Feuil.", "Longueur[cm]", "Largeur[cm]", "Qualité", "Surface[M²]"], 
                filters: true,
                dropdownMenu: false,
                renderer: 'html',
                afterChange: function(change, source){
                    if(change === null)
                        return;
                        for(var i=0; i<change.length; i++){
                            if(change[i][1] === 5 || change[i][1] === 6 || change[i][1] === 7){
                                if( donneesPaquet[change[i][0]][5] !== '' && 
                                    donneesPaquet[change[i][0]][6] !== '' && 
                                    donneesPaquet[change[i][0]][7] !== ''){
                                    donneesPaquet[change[i][0]][9] = donneesPaquet[change[i][0]][5]*donneesPaquet[change[i][0]][6]*donneesPaquet[change[i][0]][7];
                                    donneesPaquet[change[i][0]][9] = (donneesPaquet[change[i][0]][9]/10000).toFixed(2);
                                    tableurPaquet.render();
                                }
                            }
                        }
                        // On restaure l'état des checkbox
                        restaurePacketsChecked();
                        
                        // On sauvegarde dans la bd
                        save(donneesPaquet, "paquet");
                        
                },
                beforeChange: function(change, source){
                    // on sauvegarde l'état des paquets selectionnés
                    keepPacketsChecked();
                },
                afterCopy: function(changes) {
                    clipboardCache = sheetclip.stringify(changes);
                },
                afterCut: function(changes) {
                    clipboardCache = sheetclip.stringify(changes);
                },
                afterPaste: function(changes) {
                    // we want to be sure that our cache is up to date, even if someone pastes data from another source than our tables.
                    clipboardCache = sheetclip.stringify(changes);
                },
                contextMenu: [
                    'copy',
                    'cut',
                    {
                      key: 'paste',
                      name: 'Paste',
                      disabled: function() {
                        return clipboardCache.length === 0;
                      },
                      callback: function() {
                        var plugin = this.getPlugin('copyPaste');

                        this.listen();
                        plugin.paste(clipboardCache);
                      }
                    }
                ]
            });
            tableurPaquet.updateSettings({
                cells: function (row, col, prop) {
                  var cellProperties = {};

                  if (col === 0 || col === 9) {
                    cellProperties.editor = false;
                  }

                  return cellProperties;
                }
            });
            deleteRow(tableurPaquet, "paquet");*/
//            deleteRow(tableurPaquet, "paquet");
        }


        var activeColis = null;
        var isActiveColis = [];
        function setColisTableur(){
                container = $$('tableur-colis');
                tableurColis = new Handsontable(container, {
                  data: donneesColis,
                  rowHeaders: false,
                  width: '100%',
                  manualColumnResize: true,
                  colHeaders: ["Actif", "Colis", 'Long', 'Larg', "Qual", "Nombre", "Surf[M²]", "Fermer"],  
                  colWidths: [60, 60, 50, 50, 70, 50, 70, 50],
                  filters: false,
                  dropdownMenu: false,
                  renderer: 'html',
                afterChange: function(change, source){
                    if(change !== null)
                        //console.log(change[0][0]);
                    
                    // On restaure l'état des checkbox
                    var items = document.getElementsByName('currentColis');
                    if(items === undefined)
                        return;
                    for(var k = 0; k< items.length; k++){
                        items[k].checked = isActiveColis[k].checked;
                        items[k].setAttribute("data", isActiveColis[k].ref);
                    };
                    save(donneesColis, "colis");
                },
                afterRender: function(isForce){
                    
                    // On reinitialise la possibilité de fermer le colis
                    
                    $('.check_close_colis').click(function(){
                        var items = document.getElementsByClassName('check_close_colis');
                        for(var i=0; i<items.length; i++)
                            if(items[i] === this){
                                if(confirm('Voulez-vous vraiment fermer ce colis?')===true){
                                    deleteRow(tableurColis, 'colis', i);
                                    break;
                                }
                            }
                    });
                },
                beforeChange: function(change, source){
                    var items = document.getElementsByName('currentColis');
                    isActiveColis = new Array(items.length).fill(false);

                    // On sauvegarde l'état des checkbox
                    for(var k = 0; k< items.length; k++){
                        isActiveColis[k] = {checked: items[k].checked, ref: items[k].getAttribute("data")};
                    };
                }
                });
                deleteRow(tableurColis, "colis");
                tableurColis.updateSettings({
                    cells: function (row, col, prop) {
                      var cellProperties = {};
                      // On rend non modifiables les colonnes Actif, Nombre, Surface Fermer
                      // de telle sorte qu'on ne puisse double cliquer dessus
                      if (col === 0 || col === 5 || col === 6 || col === 7) {
                        cellProperties.editor = false;
                      }

                      return cellProperties;
                    }
                });
                //restaureLocalSave();
                //setColisOuverts();

            }


        function keepColisChecked(start = null, end=null){
            var items = document.getElementsByName('currentColis');
            isActiveColis = new Array(items.length).fill(false);

            // On sauvegarde l'état des checkbox
            var begin = (start === null ? 0 : start);
            var fin = (end === null ? items.length : end);
            for(var k = begin; k< fin; k++){
                isActiveColis[k] = {checked: items[k].checked, ref: items[k].getAttribute("data")};
            };
        }
        
        function restaureColisChecked(start = null, end=null){
            var items = document.getElementsByName('currentColis');
            var begin = (start === null ? 0 : start);
            if(end === null)
                for(var k = begin; k< items.length; k++){
                    items[k].checked = isActiveColis[k].checked;
                    items[k].setAttribute("data", isActiveColis[k].ref);
                }
            else
                for(var k = begin; k<items.length; k++){
                    items[k].checked = isActiveColis[k-1].checked;
                    items[k].setAttribute("data", isActiveColis[k-1].ref);
                }
        }

        function keepPacketsChecked(start = null, end=null){
            var items = document.getElementsByName('selectedPaquet');
            isRowChecked = new Array(items.length).fill(false);

            // On sauvegarde l'état des checkbox
            var begin = (start === null ? 0 : start);
            var fin = (end === null ? items.length : end);
            for(var k = begin; k< fin; k++){
                isRowChecked[k] = items[k].checked;
            };
        }
        
        function restaurePacketsChecked(start = null, end=null){
            var items = document.getElementsByName('selectedPaquet');
            var begin = (start === null ? 0 : start);
            if(end === null)
                for(var k = begin; k< items.length; k++){
                   items[k].checked = isRowChecked[k];
                }
            else
                for(var k = begin; k<items.length; k++){
                   items[k].checked = isRowChecked[k-1];
                }
        }
        
        
        function save(data, name){
            localStorage.setItem(name, JSON.stringify(data));
        }

        function saveMassicotage(data){
            var name = 'massicotage';
            var oldData = localStorage.getItem(name);
            //on récupère l'id du button radio associé au colis, lequel sera la clé dans le mapping en bdlocale
            var idColis = document.getElementsByName("currentColis")[data.indexColis].id;
            var ref = document.getElementsByName("currentColis")[data.indexColis].getAttribute("data");
            if(oldData === null || oldData === undefined || oldData === ''){
                localStorage.setItem(name, JSON.stringify(new Array(1).fill({idColis: idColis, ref: ref, paquets: [data.paquets]})));
                oldData = JSON.parse(localStorage.getItem(name));
            }else{
                oldData = JSON.parse(oldData);
                //console.log('Old data');
                var lastRecord = oldData.find(function(e){
                    
                    if(e.idColis == idColis)
                        return e;
                });
                if(lastRecord !== null && lastRecord !== undefined && lastRecord.idColis !== undefined){
                    lastRecord.paquets.push(data.paquets);
                }else{
                    oldData.push({idColis: idColis, ref: ref, paquets: [data.paquets]});
                    localStorage.setItem(name, JSON.stringify(oldData));
                }
                //oldData.push(data.idColis);
                localStorage.setItem(name, JSON.stringify(oldData));
            }
            console.log(data);
            /**
             * On met à jour la valeur de l'essence de ce colis
             */
            var essenceItem = document.getElementById('zone-essence');
            updateColisEssenceTravail(
                idColis, 
                essenceItem.value, 
                essenceItem.options[essenceItem.selectedIndex].innerText, 
                $('#zone-bille').val(), 
                getColisAt(activeColis),  // Colis actif donc selectionné
                $('#zone-equipe').val(), 
                $('#zone-epaisseur').val());
        
        }
        
        function restaureMassicot(){
            var name = 'massicotage';
            var data = JSON.parse(localStorage.getItem(name));
            if(data === null || data === undefined || data.length === 0)
                return [];
            return data;
        }
        
        function restaureLocalSave(){
            donneesColis = JSON.parse(localStorage.getItem("colis"));
            donneesPaquet = JSON.parse(localStorage.getItem("paquet"));
            console.log(donneesColis);
            for(var k=0; k<donneesColis.length; k++)
                addRow(tableurColis, "colis", donneesColis[k]);
            tableurColis.render();
            tableurPaquet.render();
            colisIndex = donneesColis.length;
            paquetIndex = donneesPaquet.length;
         }
        
        function showError(msg){
            $('#zone-error').html(msg).show(500);
            setTimeout(()=>$('#zone-error').hide(500), 5000);
        }
        
        function getColisAt(index){
            var table = document.getElementById("tableur-colis-body");
            var cells = table.childNodes[index].children;
            var data = [];
            for(var i=0; i<cells.length; i++)
                data[i] = cells[i].children[0].value;
            return data;
        }
        
        function transfertPaquet(){
            
            activeColis = null;
            var colisIterator = 0;
            $('input[name="currentColis"]').each(function(){if(this.checked === true) activeColis = colisIterator; colisIterator++;});
            
            if(activeColis === null){
                showError('Veuillez sélectionner le colis dans lequel transférer');
                return;
            }
            
            //tableurColis.render();

            var colis = getColisAt(activeColis);
            //donneesColis = donneesColis.splice(tableurColis.getData().length-activeColis-1, 1);
            //tableurColis.render();
            // On retire le checkbox
            colis.pop();
            // on retire le button radio
            colis.reverse().pop();
            colis.reverse();
            
            var listPaquets = [];
            
            if($('#zone-bille').val() === '' ){
                showError('Veuillez sélectionner le numéro de travail');
                return;
            }
            if($('#zone-essence').val() === '' ){
                showError('Veuillez sélectionner l\'essence');
                return;
            }
            
            if($('#zone-epaisseur').val() === '' ){
                showError('Veuillez préciser la valeur de l\'épaisseur');
                return;
            }
            if($('#zone-epaisseur').val().indexOf('/10') < 0 ){
                showError('La valeur de l\'épaisseur doit être au format X/10');
                return;
            }
            
            var iteration = 0;
            var checked = 0;
            //tableurPaquet.render();
            $('input[name="selectedPaquet"]').each(function(){
                //console.log('iteration = '+iteration+' checked = '+this.checked);
                if(this.checked === true){
                    checked++;
                    /**
                     * On récupere les données du paquet selectionné
                     */
                    var tab = [];
                    var tableRow = this.parentNode.parentNode;
                    for(var i=1; i<tableRow.children.length-1; i++){
                        tab[i-1] = tableRow.children[i].firstChild.value;
                    }
                    
                    /**
                     * On se rassure qu'on ajoute dans le colis uniquement les paquets de meme 
                     * qualité
                     */
                    
                    if(tab[7] === colis[3]){
                        listPaquets.push({id:iteration, tab: tab});
                        $(tableRow).remove();
                    }
                }
                this.checked = false;
                iteration++;
            });

            if(listPaquets.length === 0){
                showError('Veuillez sélectionner les paquets à transférer');
                return;
            }
            
            for(var i=0; i<listPaquets.length; i++){
                var paquet = listPaquets[i].tab;
                for(var j=0; j<paquet.length; j++)
                    if(paquet[j]===null || paquet[j]===undefined || paquet[j]===''){
                        showError('Veuillez compléter les informations '+(j)+' du Paquet '+listPaquets[i].id);
                        return;
                }
            }
            
            // Données correctement remplises.
            
            for(var i=0; i<listPaquets.length; i++){
                var paquet = listPaquets[i].tab;
                for(var j=0; j<paquet.length; j++)
                    if(paquet[5]===null || paquet[5]===undefined || paquet[5]===''){
                        showError('Veuillez compléter la longueur du paquet '+listPaquets[i].id);
                        return;
                    }
                    if(paquet[6]===null || paquet[6]===undefined || paquet[6]===''){
                        showError('Veuillez compléter la largeur du paquet '+listPaquets[i].id);
                        return;
                    }
                    if(paquet[7]===null || paquet[4]===undefined || paquet[7]===''){
                        showError('Veuillez compléter le nombre de feuilles du paquet '+listPaquets[i].id);
                        return;
                    }
                    if(paquet[5]< colis[1]===''){
                        showError('La longueur du paquet '+listPaquets[i].id+' doit être inférieure à la longueur du colis');
                        return;
                    }
                    if(paquet[6]< colis[1]===''){
                        showError('La largeur du paquet '+listPaquets[i].id+' doit être inférieure à la largeur du colis');
                        return;
                    }
                    if(paquet[5]< colis[1]===''){
                        showError('La longueur du paquet '+listPaquets[i].id+' doit être inférieure à la longueur du colis');
                        return;
                    }
            }
            

            
            var massicotage = {
                indexColis: activeColis,// il s'agit du numéro de la ligne sélectionnée dans les colis
                paquets: listPaquets
            };
            
            saveMassicotage(massicotage);
            
            // On supprime le colis du tableau
            //donneesColis.splice(donneesColis.length-activeColis-1, 1);
            //tableurColis.render();
            
            //tableurColis.alter('remove_row', tableurColis.getData().length-activeColis-1);
            var rows = [],
           
            nombre = parseInt(colis[4]),
            surface = parseFloat(colis[5]);
    
            listPaquets.forEach(function(e){
                rows.push([e.id, 1]);
                surface = (parseFloat(e.tab[8])+surface);//.toFixed(2);
                nombre += 1;
            });
            
            document.getElementById("tableur-colis-body").children[activeColis].children[5].children[0].value =  nombre;
            document.getElementById("tableur-colis-body").children[activeColis].children[6].children[0].value =  surface;
//            tableurColis.setDataAtCell(activeColis, 6, parseFloat(surface+'').toFixed(2));

  //          tableurPaquet.alter('remove_row', rows);
            
            showError('');
            activeColis = null;
    //        setLastOperation();
            
        }
        
        var restauration = [];
        function annuler(){
            
            var obj = restauration.pop();
            //console.log(restauration);
            if(obj !== undefined && obj !== null){
                donneesColis = (obj.colis === undefined ? [] : JSON.parse(obj.colis));
                donneesPaquet = (obj.paquet === undefined ? [] : JSON.parse(obj.paquet));
                colisIndex = obj.colisIndex;
                paquetIndex = obj.paquetIndex;
            }
            
            save(donneesColis, "colis");
            save(donneesPaquet, "paquet");
            tableurColis.render();
            tableurPaquet.render();
            //console.log(restauration);
        }
        
        function setLastOperation(){
            
            restauration.push({
                colis: (tableurColis === undefined ? "[]" : JSON.stringify(donneesColis)),
                paquet: (tableurPaquet === undefined ? "[]" : JSON.stringify(donneesPaquet)),
                colisIndex: colisIndex,
                paquetIndex: paquetIndex,
            });
            save(donneesColis, "colis");
            save(donneesPaquet, "paquet");
            
            
            //console.log(restauration);
        }

        function updateColisEssenceTravail(idColis, essenceId, essenceLibelle, travail, colisInf, equipe, epaisseur){
            var name = 'essence_colis';
            var oldData = JSON.parse(localStorage.getItem(name));
            
            if(oldData === null || oldData === undefined){
                oldData = new Array(1).fill(
                        {essence: {ref:essenceId, libelle: essenceLibelle}, 
                        colis: idColis, 
                        travail: travail, 
                        colisInf: colisInf, 
                        equipe: equipe, 
                        epaisseur: epaisseur
                    });
                localStorage.setItem(name, JSON.stringify(oldData));
            }else{
                var lastRecord = null;
                for(var i=0; i<oldData.length; i++)
                        if(oldData[i].colis === idColis){
                            lastRecord = oldData[i];
                            break;
                        }
                
                if(lastRecord === null){
                    lastRecord = {
                        essence: {ref: essenceId, libelle: essenceLibelle},
                        colis: idColis,
                        travail: travail,
                        colisInf: colisInf,
                        equipe: equipe,
                        epaisseur: epaisseur
                    };
                    oldData.push(lastRecord);
                }else{
                    lastRecord.essence = {ref: essenceId, libelle: essenceLibelle};
                    lastRecord.travail = travail;
                    lastRecord.equipe = equipe;
                    lastRecord.epaisseur = epaisseur;
                    lastRecord.colisInf= colisInf;
                }
                localStorage.setItem(name, JSON.stringify(oldData));
            }
        }

        let donneesPretes = [];
        function printVisual(){
            /*
             * On restaure les données stockées
             */
            var data = restaureMassicot();
            var surface = 0;
            // on regenere les donnees pretes pour garantir la cohérence et l'intégrité
            donneesPretes = []
            
            var table = $$('table-massicotage-detail-commande');
            table.innerHTML = '';
            for(var i=0; i<data.length; i++){

                var idColis = data[i].idColis;
                var ref = data[i].ref;
                var essenceTravail = getEssenceForColis(idColis);
                var paquets = data[i].paquets;
                
                /**
                 * Contient l'ensemble de paquets pour un colis
                 * Ici dessous on agrege les paquets par colis
                 */
                var paquetsForRow = [];
                for(var j=0; j<paquets.length; j++){
                    for(var k=0; k<paquets[j].length; k++){
                        paquetsForRow.push(paquets[j][k]);
                        surface = paquets[j][k].tab[8]+'+'+surface;
                    }
                }
                var l = surface.split('+');
                surface = 0;
                for(var m=0; m<paquetsForRow.length; m++)
                    surface = surface+parseFloat(l[m]);

                var nbrPaquets = paquetsForRow.length;
                var colisCell;
                var colisData = getColisRecordByHTMLID(idColis);
                var colisDataJSON = {
                    colis: {
                        ref: ref,
                        number: colisData[1],
                        longueur: colisData[2],
                        largeur: colisData[3],
                        essence: essenceTravail.essence.ref,
                        qualite: colisData[4],
                        surface: surface,
                        equipe: essenceTravail.equipe,
                        epaisseur: essenceTravail.epaisseur,
                        travail: essenceTravail.travail
                    },
                    paquets: {
                        epaisseur: essenceTravail.epaisseur,
                        list: paquetsForRow,
                        taille:  paquetsForRow.length
                    }
                };
                
                // on ajoute une donnée prête
                donneesPretes.push(colisDataJSON);
                
                for(var j=0; j<paquetsForRow.length; j++){
                    let ligne = document.createElement('tr');

                    paquetsForRow[j].travail = essenceTravail.travail;

                    let refPaquet = document.createElement('td');
                    refPaquet.innerHTML = paquetsForRow[j].tab[0]+'-'+paquetsForRow[j].tab[1]+'-'+paquetsForRow[j].tab[2]+'-'+paquetsForRow[j].tab[3];
                    //refPaquet.innerHTML = essenceTravail.travail+'-'+paquetsForRow[j].tab[0]+'-'+paquetsForRow[j].tab[1]+'-'+paquetsForRow[j].tab[2]+'-'+paquetsForRow[j].tab[3];
                    //la variable essenceTravail.travail contient le numéro de travail pour la bille courante
                    let nbrFeuil = document.createElement('td');
                    nbrFeuil.innerHTML = paquetsForRow[j].tab[4];

                    let longueur = document.createElement('td');
                    longueur.innerHTML = paquetsForRow[j].tab[5];

                    let largeur = document.createElement('td');
                    largeur.innerHTML = paquetsForRow[j].tab[6];

                    surface = document.createElement('td');
                    surface.innerHTML = paquetsForRow[j].tab[8];

                    let btnSup = document.createElement('button');
                    btnSup.setAttribute("class", "btn btn-danger");
                    btnSup.innerHTML = '<i class="fa fa-trash"></i>';
                    btnSup.setAttribute("data", j);
                    btnSup.setAttribute("row", i);
                    btnSup.onclick  = function(){
                        if(
                                confirm('Voulez-vous vraiment supprimer le paquet '+
                                paquetsForRow[this.getAttribute("data")].tab[0]+'-'+
                                paquetsForRow[this.getAttribute("data")].tab[1]+'-'+
                                paquetsForRow[this.getAttribute("data")].tab[2]+'-'+
                                paquetsForRow[this.getAttribute("data")].tab[3]+" ?"
                            )){
                           $(ligne).remove(); 
                           console.log('données pretes before');
                           console.log(donneesPretes);
                           console.log('suppression de paquetsForRow');
                           paquetsForRow.splice(this.getAttribute("data"), 1);
                           console.log('suppression de données pretes');
                           donneesPretes[this.getAttribute("row")].paquets.taille--;
                           console.log('paquet supprimé = '+this.getAttribute("data"));
                           console.log(paquetsForRow[this.getAttribute("data")]);
                           console.log('paquets restants = ');
                           console.log(paquetsForRow);
                           console.log('données pretes pour le colis = ');
                           console.log(donneesPretes[this.getAttribute("row")]);
                           
                        }
                    };

                    if(j===0){
                        colisCell = document.createElement('td');
                        colisCell.setAttribute("rowspan", nbrPaquets);
                        colisCell.innerHTML = 
                            'N° = '+$('#zone-atelier').val()+''+colisDataJSON.colis.number+'<br>'+
                            (colisDataJSON.colis.ref === 'ref' ? '' : 'Ref = '+colisDataJSON.colis.ref+'<br>')+
                            'Longueur = '+colisDataJSON.colis.longueur+' cm<br>'+
                            'Largeur = '+colisDataJSON.colis.largeur+' cm<br>'+
                            'Essence = '+essenceTravail.essence.libelle+'<br>'+
                            'Qualité = '+colisDataJSON.colis.qualite+'<br>'+
                            'Nbr Paquets = '+colisDataJSON.paquets.taille+'<br>'+
                            'Surface = '+colisDataJSON.colis.surface+' m²<br>'+
                            'Equipe = Équipe '+colisDataJSON.colis.equipe;
                        ligne.appendChild(colisCell);

                    }
                    ligne.appendChild(refPaquet);
                    ligne.appendChild(nbrFeuil);
                    ligne.appendChild(longueur);
                    ligne.appendChild(largeur);
                    ligne.appendChild(surface);
                    ligne.appendChild(btnSup);
                    table.appendChild(ligne);
                }
            }

        }

        function getColisRecordByHTMLID(idColis){
            var items = getEssenceForColis(idColis);
            return items.colisInf;
        }
        
        function getEssenceForColis(idColis){
            var oldData = JSON.parse(localStorage.getItem('essence_colis'));
                //console.log('Old data');
                var lastData = null;
                for(var i=0; i<oldData.length; i++)
                    if(oldData[i].colis === idColis)
                        lastData = oldData[i];
                ;
                if(lastData !== null && lastData !== undefined && lastData.idColis !== undefined){
                    return {};
                }else{
                    return lastData;
                }
        }
        //setLastOperation();
        
        loadEssence();
        
        ///setPaquetTableur();
        //setColisTableur();
        setColisOuverts();
        

        $('select').select2();

