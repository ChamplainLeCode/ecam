/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global user */

$(function(){
    
    
   
   $('#btn-addTranche').click(function(){
       
       $.ajax({
            url: "/tranche/add",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                $('#modal-default #modal-body').html(data);
           },
            error: function (jqXHR, textStatus, errorThrown) {
                 $('#modal-default #modal-body').html(jqXHR.responseText);
            }
       });
   });
    if(user.can_add !== 'O')       
        $('#btn-addTranche').remove();
   
   
    function getTableRow(dataRow){
        var row = document.createElement("tr"),
                
//        celREF = document.createElement("td"),
        celDATEDEBUT = document.createElement("date"),
        celDATEFIN = document.createElement("date"),
        celEPAISSEUR = document.createElement("td"),
        celMachine = document.createElement("td"),
        celNUMTRAVAIL = document.createElement("td"),
        celEQUIPE = document.createElement("td"),
        celPLOT = document.createElement("td"),
        celFace1 = document.createElement("td"),
        celFace2 = document.createElement("td"),
        celFace3 = document.createElement("td"),
        celFaceMN = document.createElement("td"),
        celCertif = document.createElement("td"),
        celOpe = document.createElement("td"),
        
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        icon = document.createElement("i");

//         celREF.innerHTML = dataRow.ref;
        celDATEDEBUT.innerHTML = new Date(dataRow.dateDebut).toDateString();
        celEPAISSEUR.innerHTML = dataRow.epaisseur;
        celMachine.innerHTML = dataRow.machine.libelle;
        celNUMTRAVAIL.innerHTML = dataRow.travail;
        celEQUIPE.innerHTML = dataRow.equipe === null ? '' : dataRow.equipe.libelle;
        celPLOT.innerHTML = dataRow.plot === null ? '' : dataRow.plot.ref;
        celFace1.innerHTML = dataRow.plot === null ? '' : dataRow.plot.face1;
        celFace2.innerHTML = dataRow.plot === null ? '' : dataRow.plot.face2;
        celFace3.innerHTML = dataRow.plot === null ? '' : dataRow.plot.face3;
        celFaceMN.innerHTML = dataRow.plot === null ? '' : dataRow.plot.faceMN;
        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.cni;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        $.ajax({
            url: "/api/certification/travail",
            method: "GET",
            data: {ref: dataRow.travail},
            dataType: 'json',
            complete: function (jqXHR, textStatus ) {
                if(jqXHR.responseJSON !== null)
                    celCertif.innerHTML = jqXHR.responseJSON.certification;
            }
        });
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-pencil";
        btnEdi.className = "btn btn-success btn-edit-tranche";
        btnEdi.title = 'Modifier '+dataRow.nom;
        btnEdi.setAttribute("data-toggle", "modal");
        btnEdi.setAttribute("data-target", "#modal-edit");
        btnEdi.appendChild(icon);
        btnEdi.onclick = function(){
            $.ajax({
                url: "/tranche/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    console.log(jqXHR);
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        
        

        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
        //        row.appendChild( celREF);
        row.appendChild(celMachine);
        row.appendChild(celDATEDEBUT);
        row.appendChild(celDATEFIN);
        row.appendChild(celEPAISSEUR);
        row.appendChild(celNUMTRAVAIL);
        row.appendChild(celEQUIPE);
        row.appendChild(celPLOT);
        row.appendChild(celFace1);
        row.appendChild(celFace2);
        row.appendChild(celFace3);
        row.appendChild(celFaceMN);
        row.appendChild(celCertif);
        row.appendChild(celOpe);
        return row;
    }

    window.deleteData = function(data, source){
        $.ajax({
            url: "/tranche/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('.status-del-tranche').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-tranche').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('.status-del-tranche').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-tranche').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    


window.loadData = function (){
        $("#table-body").html('');
        $.ajax({
            url: "/api/tranche/list",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                
                for(var i=0; i<data.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(data[i]));
                }
                            $('table').DataTable();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("processus erroné");
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
                console.log("Le processus est terminé");
            }
        });
    };
        function parseDate(dateStringify = (new Date().getYear()+1990)+"-"+new Date().getMonth()+"-"+new Date().getDate()){
            var date = dateStringify.split('-');
            // console.log(date);
            return new Date(parseInt(date[0]), parseInt(date[1]), parseInt(date[2]));
        }    
window.saveData = function(tranche, ref, dateDebut, epaisseur,numTravail,equipe,plot, face1, face2, face3, faceMN){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return ;
     $.ajax({
            url: "/tranche/add",
            method: "POST",
            data: {
                machine: tranche,
                ref: ref,
                dateDebut: parseDate(dateDebut).getTime(),
                epaisseur: epaisseur,
                travail: numTravail,
                equipe: equipe,
                plot: plot, 
                face1: face1,
                face2: face2,
                face3: face3,
                faceMN: faceMN,
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-addPane').click();
                loadData();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            complete: function (jqXHR, textStatus) {
                $('.close').click();
            }
        });
}

    
window.editData = function(ref, dateDebut, dateFin, epaisseur,numTravail,equipe,plot){
     $.ajax({
            url: "/tranche/edit",
            method: "POST",
            data: {
                ref: ref,
                dateDebut: dateDebut,
                dateFin: dateFin,
                epaisseur: epaisseur,
                numTravail: numTravail,
                equipe: equipe,
                plot: plot
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-editPane').click();
                loadData();
                console.log(data);
                console.log('icic');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
}

});
