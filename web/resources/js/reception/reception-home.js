
/* global detailCompteur, currentDetail, parseFloat */

$(function(){
    window.$ = $;
    window.saveData = function saveData(ref, date, fournisseur, bille, essence, gdDiam, ptDiam, moyDiam, longueur, refraction, volAttendu, volRecu, lettreRef, lettreCertificat, lettreFichier){
        
        
        $.ajax({
            data: {
                ref: ref,
                date: date,
                fournisseur: fournisseur,
                bille: bille,
                essence: essence,
                gdDiam: gdDiam,
                ptDiam: ptDiam,
                moyDiam: moyDiam,
                longueur: longueur,
                refraction: refraction,
                volumeAttendu: volAttendu,
                volumeRecu: volRecu,
                lettreRef: lettreRef,
                lettreCertificat: lettreCertificat,
                lettreFichier: lettreFichier
            },
            dataType: 'json',
            url: "/reception/add",
            method: 'POST', 
            async: false,
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('#form-add-reception .overlay').addClass('hide');
                    $('#form-add-reception #btn-save-reception').removeClass("disabled");
                    $('.status-add-reception').removeClass('bg-red .disabled').addClass('bg-green disabled').html('Enregistré');
                    $('#form-add-reception #zone-ref').val('REF_CLASS_'+(new Date()).getTime());
                    loadData();
                }else{
                    $('#form-add-reception .overlay').addClass('hide');
                    $('#form-add-reception #btn-save-reception').removeAttr("disabled");
                    $('.status-add-reception').removeClass('bg-red .disabled').addClass('bg-red disabled').html(data.reason);
                }
                // console.log(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('#form-add-reception .overlay').addClass('hide');
                    $('#form-add-reception #btn-save-reception').removeClass("disabled");
                    $('.status-add-reception').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason);
                }else{
                    //this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
        

    };

//$('#btn-addReception').click(function(){sendForm();});

window.sendForm = function(){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return false;
    
        var data = [];
        var dataLV = [];
        var j = 0;
        console.log()
//        $('.zone-mesure').each(function(){
        $('input[type="checkbox"]').each(function(){
            console.log('J = '+j);
            var index = $(this).attr('data');
            console.log('Index  = '+index);
            console.log(this);
            
            //if(document.getElementById('select'+index).checked === false){
            if(this.checked === true){
            
                var rayon = parseFloat($('#zone-moy-diam'+index).val() == '' ? $('#zone-moy-diam'+index).attr("placeholder") : $('#zone-moy-diam'+index).val())/2;
                var hauteur = parseFloat($('#zone-longueur'+index).val() == '' ? $('#zone-longueur'+index).attr("placeholder") : $('#zone-longueur'+index).val());

                var volume = ((rayon * rayon * Math.PI * hauteur) / Math.pow(10, 6))+'' ;
                //($('#zone-cubage'+index).val() == '' ? $('#zone-cubage'+index).attr("placeholder") : $('#zone-cubage'+index).val());

                volume = volume.substring(0,((volume.indexOf(',')>=0 ? volume.indexOf(',') : volume.indexOf('.'))+4));
                dataLV[j] = {
                    essence: $('#zone-essence'+index).val(),
                    bille: ($('#zone-bille'+index).val() == '' ? $('#zone-bille'+index).attr("placeholder") : $('#zone-bille'+index).val()),
                    longueur: parseFloat($('#zone-longueur'+index+'-lv').val()).toFixed(3),
                    gdDiam: parseFloat($('#zone-gd-diam'+index+'-lv').val()).toFixed(3),
                    pttDiam: parseFloat($('#zone-ptt-diam'+index+'-lv').val()).toFixed(3),
                    moyDiam: parseFloat(((parseFloat($('#zone-gd-diam'+index+'-lv').val())+parseFloat($('#zone-ptt-diam'+index+'-lv').val()))/2)+"").toFixed(3),
                    volume: parseFloat($('#zone-cubage'+index+'-lv').val()),
                    certificat: $('#zone-certificat'+index).val()
                };

                data[j++] = {
                    essence: $('#zone-essence'+index).val(),
                    bille: ($('#zone-bille'+index).val() == '' ? $('#zone-bille'+index).attr("placeholder") : $('#zone-bille'+index).val()),
                    longueur: hauteur,
                    gdDiam: ($('#zone-gd-diam'+index).val() == '' ? $('#zone-gd-diam'+index).attr("placeholder") :  $('#zone-gd-diam'+index).val()),
                    pttDiam:($('#zone-ptt-diam'+index).val() == '' ? $('#zone-ptt-diam'+index).attr("placeholder") : $('#zone-ptt-diam'+index).val()),
                    moyDiam: (rayon*2),
                    volume: parseFloat(volume),
                    prix: ($('#zone-prix'+index).val() == '' ? $('#zone-prix'+index).attr("placeholder") : $('#zone-prix'+index).val()),
                    observation: ($('#zone-observation'+index).val() == '' ? $('#zone-observation'+index).attr("placeholder") : $('#zone-observation'+index).val()),
                    statut: $('#zone-statut'+index).val(),
                    certificat: $('#zone-certificat'+index).val(),
                    parc: $('#zone-origine').attr('data')
                };
                console.log('Donnees lv');
                console.log(dataLV[j-1]);
                console.log('Donnée data');
                console.log((data[j-1]));
            }
        });
        console.log(data);
        console.log(data);
        if(data.length === 0){
            alert('Veuillez selectionner les billes à receptionner');
            return false;
        }
        var result = {
            commande: $('#zone-commande').val(),
            data: {parc: data, lv: dataLV},
            lettreFichier: $('#zone-lettre-fichier').val().split('\\')[2],
            lettreReference: $('#zone-lettre-ref').val(),
            taille: data.length
        };
        
        $.ajax({
            url: "/reception/add",
            method: "POST",
            dataType: 'json',
            data: result,
            async: false,
            complete: function (jqXHR, textStatus) {
                var data = jqXHR.responseJSON;
                if(data.resultat === true){
                    var files = document.getElementById("zone-lettre-fichier").files;
                    var formData = new FormData();
                    for (var i = 0; i < files.length; i++) {
                        var file = files[i];
                        var extend = result.lettreFichier.split('.')
                        formData.append('photos', file, result.lettreReference+'.'+extend[extend.length-1]);
                    }
                    formData.append("lettre", data.lettre);
                      $.ajax({
                          url:'/upload/lettre-camion',
                          method: 'POST',
                          data: formData, 
                          async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                          complete: function (jqXHR, textStatus) {
                                $.ajax({
                                    url: '/reception/bon',
                                    data: result,
                                    dataType: 'json',
                                    method: "POST",
                                    async: false,
                                    complete: function (jqXHR, textStatus) {
                                        if(confirm('Imprimer la fiche "RECEPTION LIVRÉE"')){
                                            window.open("/reception/livree?reception="+data.id+"&num="+result.lettreReference, "ECAM PLACAGE | BON DE RECEPTION LIVRÉE "+(new Date().getTime()), "directories=no,titlebar=no,status=no,menubar=no,scrollbars=yes,resizable=no,width="+screen.width+",height="+screen.height);
                                        }                                    
                                        if(confirm('Imprimer le "BON DE RECEPTION"')){
                                            window.open("", "ECAM PLACAGE | BON DE RECEPTION "+(new Date().getTime()), "directories=no,titlebar=no,status=no,menubar=no,scrollbars=yes,resizable=no,width="+screen.width+",height="+screen.height)
                                                  .document.write(jqXHR.responseText);
                                        }
                                        location.reload();
                                        
                                    }
                                });
                                /*$.ajax({
                                    url: '/reception/livree',
                                    data: {reception: data.id},
                                    dataType: 'json',
                                    method: "GET",
                                    async: false,
                                    complete: function (jqXHR, textStatus) {
                                        var printer = window.open("/", "ECAM PLACAGE | RECEPTION LIVRÉE", "directories=no,titlebar=no,status=no,menubar=no,scrollbars=yes,resizable=no,width="+screen.width+",height="+screen.height);
                                        printer.document.write(jqXHR.responseText);
                                    }
                                });*/
                                //location.reload();
                              //location.href = "/reception/bon?user="+user.matricule+"&page=/reception&result="+result;//
                              //location.reload();
                          }
                      });
                }
            }
        });
        return false;
    };
    // on supprimer le button chargé de l'envoi des données
    if(user.can_add !== 'O'){    
        $('#btn-addReception').remove();// btn pour terminer la reception
        $('#btn-display-form').remove();//btn pour afficher le formulaire de recption
    }
   

    function getTableRow(dataRow){
        var row = document.createElement("tr"),
        celDate = document.createElement("td"),
        celFour = document.createElement("td"),
        celCommande = document.createElement("td"),
        celCertification = document.createElement("td"),
        celLettre = document.createElement("td"),
        celVolumeRecu = document.createElement("td"),
        celVolumeTotal = document.createElement("td"),
        celComplete = document.createElement("td"),
        
        btnSee = document.createElement("button"),
        btnCom = document.createElement("button"),
        btnInComp = document.createElement("button"),
        iconSee = document.createElement("i"),
        icon = document.createElement("i");

        var volume = dataRow.volumeRecu+"";
        
        celVolumeRecu.innerHTML = volume.substring(0,((volume.indexOf(',')>=0 ? volume.indexOf(',') : volume.indexOf('.'))+4));
        celVolumeTotal.innerHTML = dataRow.volumeTotal;
        celDate.innerHTML = new Date(parseFloat(dataRow.date)).toLocaleString();
        celFour.innerHTML = dataRow.fournisseur;
        celLettre.innerHTML = dataRow.lettre;
        celCommande.innerHTML = dataRow.commande;
        celCertification.innerHTML = dataRow.certification;

        // Indique si oui ou non la commande est déjà complete
        
        if(dataRow.complete == true ^ (dataRow.complete == false && parseFloat(dataRow.volumeRecu) > parseFloat(dataRow.volumeTotal))){
            btnCom.type = "button";
            btnCom.className = "btn btn-success";
            btnCom.appendChild(icon);
            icon.className = "fa fa-check";
            btnCom.title = "Commande receptionnée";
            celComplete.appendChild(btnCom);
        }else{
            btnInComp.type = "button";
            btnInComp.className = "btn btn-danger";
            btnInComp.appendChild(icon);
            icon.className = "fa fa-remove";
            btnInComp.title = "Commande encours de reception";
            celComplete.appendChild(btnInComp);
        }
        
        btnSee.type = "button";
        btnSee.className = "btn btn-primary";
        btnSee.appendChild(iconSee);
        iconSee.className = "fa fa-eye";
        btnSee.title = "voir plus";
        btnSee.style.marginLeft = "10px";
        btnSee.onclick = function(){
            $('#vzone-ref').html(dataRow.commande);
            $('#vzone-fournisseur').html(dataRow.fournisseur);
            $('#vzone-date').html(new Date(dataRow.date));
            var tableau = document.getElementById('table-view-detail-reception');
            tableau.innerHTML = '';
            /**
             * si c'st complet on récupère tout les billes 
             * si non on récupère celle qui n'ont pas encore été receptionnées
             */
                    if(dataRow.complete === true){
                        $.ajax({
                            url: '/api/detail_reception/by_commande',
                            dataType: 'json',
                            data: {commande: dataRow.commande},
                            method: 'GET',
                            async: false,
                            complete: function (cmd, textStatus ) {

                                var commandesDetails = cmd.responseJSON;

                                var tableau = document.getElementById("table-view-detail-reception");
                                var tr = document.createElement("tr");
                                var td = document.createElement("td");
                                td.setAttribute("colspan", "11");
                                td.setAttribute("style", "text-align:center; font-weight: bold; font-style:italic;");
                                td.innerHTML = "Billes receptionnées";
                                tr.appendChild(td);
                                tableau.appendChild(tr);
                                tr = document.createElement("tr");
                                tr.appendChild(document.createElement("td"));
                                tableau.appendChild(tr);

                                for(var i=0; i< commandesDetails.length; i++){
                                    var obj = commandesDetails[i];
/*                                        $.ajax({
                                        url: '/api/detail_reception/by_bille',
                                        dataType: 'json',
                                        async: false,
                                        data: {bille: commandesDetails[i].numBille},
                                        method: 'GET',
                                        complete: function (jqXHR, textStatus ) {
                                            var donnee = jqXHR.responseJSON;
                                            /**
                                             * Si la bille a déja été receptionnée ne pas l'afficher
                                             /

                                            if(donnee == null){
                                                return;
                                            }*/

                                            getRowDetailReceptionForView(commandesDetails[i]);

/*                                            }
                                    });
*/
                                }

                               // $('#btn-view-commande_client').click();
                            }
                       });


                        $.ajax({
                            url: '/api/detail_reception/non_recu/by_commande',
                            dataType: 'json',
                            data: {commande: dataRow.commande},
                            method: 'GET',
                            async: false,
                            complete: function (cmd, textStatus ) {

                                var tableau = document.getElementById("table-view-detail-reception");
                                var tr = document.createElement("tr");
                                var td = document.createElement("td");
                                td.setAttribute("colspan", "11");
                                td.setAttribute("style", "text-align:center; font-weight: bold; font-style:italic;");
                                td.innerHTML = "Billes non receptionnées";
                                tr.appendChild(td);
                                tableau.appendChild(tr);

                                var commandesDetails = cmd.responseJSON;
                                for (var item in commandesDetails) {
                                    //console.log(commandesDetails[item]);
                                    getRowDetailCommandeForView(commandesDetails[item]);
                                }

                                $('#btn-view-commande_client').click();
                            }
                       });

/*                    $.ajax({
                    url: '/api/detail_reception/by_commande',
                    method: 'GET',
                    dataType: 'json',
                    data: {commande: dataRow.commande},
                    complete: function (jqXHR, textStatus) {
                        var donnees = jqXHR.responseJSON;
                        // console.log(donnees);
                        getRowsDetailReceptionForView(donnees);
                        $('#btn-view-commande_client').click();
                    }
                });*/
            }else{

                $.ajax({
                    url: '/api/detail_reception/by_commande',
                    dataType: 'json',
                    data: {commande: dataRow.commande},
                    method: 'GET',
                    async: false,
                    complete: function (cmd, textStatus ) {

                        var commandesDetails = cmd.responseJSON;

                        var tableau = document.getElementById("table-view-detail-reception");
                        var tr = document.createElement("tr");
                        var td = document.createElement("td");
                        td.setAttribute("colspan", "11");
                        td.setAttribute("style", "text-align:center; font-weight: bold; font-style:italic;");
                        td.innerHTML = "Billes receptionnées";
                        tr.appendChild(td);
                        tableau.appendChild(tr);
                        tr = document.createElement("tr");
                        tr.appendChild(document.createElement("td"));
                        tableau.appendChild(tr);

                        for(var i=0; i< commandesDetails.length; i++){
                            var obj = commandesDetails[i];
                            $.ajax({
                                url: '/api/detail_reception/by_bille',
                                dataType: 'json',
                                async: false,
                                data: {bille: commandesDetails[i].numBille},
                                method: 'GET',
                                complete: function (jqXHR, textStatus ) {
                                    var donnee = jqXHR.responseJSON;
                                    /**
                                     * Si la bille a déja été receptionnée ne pas l'afficher
                                     */

                                    if(donnee == null){
                                        return;
                                    }

                                    getRowDetailReceptionForView(commandesDetails[i]);

                                }
                            });

                        }

                       // $('#btn-view-commande_client').click();
                    }
               });


                $.ajax({
                    url: '/api/detail_reception/non_recu/by_commande',
                    dataType: 'json',
                    data: {commande: dataRow.commande},
                    method: 'GET',
                    async: false,
                    complete: function (cmd, textStatus ) {

                        var tableau = document.getElementById("table-view-detail-reception");
                        var tr = document.createElement("tr");
                        var td = document.createElement("td");
                        td.setAttribute("colspan", "11");
                        td.setAttribute("style", "text-align:center; font-weight: bold; font-style:italic;");
                        td.innerHTML = "Billes non receptionnées";
                        tr.appendChild(td);
                        tableau.appendChild(tr);

                        var commandesDetails = cmd.responseJSON;
                        for (var item in commandesDetails) {
                            //console.log(commandesDetails[item]);
                            getRowDetailCommandeForView(commandesDetails[item]);
                        }

                        $('#btn-view-commande_client').click();
                    }
               });

            }

        }
        celComplete.appendChild(btnSee);

        row.appendChild(celDate);
        row.appendChild(celFour);
        row.appendChild(celCommande);
        row.appendChild(celCertification);
        row.appendChild(celLettre);
        row.appendChild(celVolumeTotal);
        row.appendChild(celVolumeRecu);
        row.appendChild(celComplete);
        return row;
    }
    

    function getRowsDetailReceptionForView(detail){

        var tableau = document.getElementById("table-view-detail-reception");
        // On réinitialise le tableau à chaque fois pour afficher les détails de la commande encours
        tableau.innerHTML = '';
        var detailRow = detail;
        if(detailRow === null){
            detailRow = {
                bille: '',
                longueur: 0,
                gdDiam  :0,
                ptitDiam :0,
                moyDiam :0,
                cubage  :0,
                essence:{prixUAchat:0}
            };
        }
        for(var i=0; i<detailRow.length; i++){
            var ligne = document.createElement("tr");
            if(detailRow[i].essence === null){
                detailRow[i].essence = {libelle: '', ref:''};
            }
            ligne.className = "odd";
            ligne.innerHTML = 
                       '<td><span>'+detailRow[i].essence.libelle+'</span></td>\
                        <td><span>'+detailRow[i].numBille+'</span></td>\
                        <td><span>'+detailRow[i].longueur+'</span></td>\
                        <td><span>'+detailRow[i].gdDiam+'</span></td>\
                        <td><span>'+detailRow[i].ptitDiam+'</span></td>\
                        <td><span>'+detailRow[i].diaMoyen+'</span></td>\
                        <td><span>'+detailRow[i].volume+'</span></td>\
                        <td><span>'+detailRow[i].prix+'</span></td>\
                        <td><span>'+detailRow[i].observation+'</span></td>\
                        <td><span>'+detailRow[i].status+'</span></td>\
                        <td><span>'+new Date(parseFloat(detailRow[i].date)).toDateString()+'</span></td>\
            ';
            tableau.appendChild(ligne);
        }
    } 

    
    function getRowDetailReceptionForView(detail){

        var tableau = document.getElementById("table-view-detail-reception");
        // On réinitialise le tableau à chaque fois pour afficher les détails de la commande encours
        var detailRow = detail;
        if(detailRow === null){
            detailRow = {
                bille: '',
                longueur: 0,
                gdDiam  :0,
                pttDiam :0,
                moyDiam :0,
                diaMoyen :0,
                cubage  :0,
                essence:{prixUAchat:0}
            };
        }
        var ligne = document.createElement("tr");
        if(detailRow.essence === null){
            detailRow.essence = {libelle: '', ref:''};
        }
        ligne.className = "odd";
        // console.log(detailRow.moyDiam);
        ligne.innerHTML = 
                   '<td><span>'+detailRow.essence.libelle+'</span></td>\
                    <td><span>'+detailRow.numBille+'</span></td>\
                    <td><span>'+detailRow.certificat+'</span></td>\
                    <td><span>'+detailRow.longueur+'</span></td>\
                    <td><span>'+detailRow.gdDiam+'</span></td>\
                    <td><span>'+detailRow.ptitDiam+'</span></td>\
                    <td><span>'+detailRow.diaMoyen+'</span></td>\
                    <td><span>'+detailRow.volume+'</span></td>\
                    <td><span>'+detailRow.prix+'</span></td>\
                    <td><span>'+detailRow.observation+'</span></td>\
                    <td><span>'+detailRow.status+'</span></td>\
                    <td><span>'+new Date(parseFloat(detailRow.date)).toLocaleString()+'</span></td>\
        ';
        tableau.appendChild(ligne);
    } 


    function getRowDetailCommandeForView(detail){
            
            var tableau = document.getElementById("table-view-detail-reception");
            // On réinitialise le tableau à chaque fois pour afficher les détails de la commande encours
            var detailRow = detail;
            
            var ligne = document.createElement("tr");
            if(detailRow.essence === null){
                detailRow.essence = {libelle: '', ref:''};
            }
            ligne.className = "odd";
            // console.log(detailRow.moyDiam);
            ligne.innerHTML = 
                       '<td><span>'+detailRow.essence.libelle+'</span></td>\
                        <td><span>'+detailRow.numBille+'</span></td>\
                        <td><span></span></td>\
                        <td><span>'+detailRow.longueur+'</span></td>\
                        <td><span>'+detailRow.gdDiam+'</span></td>\
                        <td><span>'+detailRow.pttDiam+'</span></td>\
                        <td><span>'+detailRow.moyDiam+'</span></td>\
                        <td><span>'+detailRow.volume+'</span></td>\
                        <td><span>'+detailRow.prix+'</span></td>\
                        <td><span>'+detailRow.observation+'</span></td>\
                        <td style="text-align:center">-</td>\
                        <td style="text-align:center">-</td>\
            ';
            tableau.appendChild(ligne);
        } 
        

    window.loadData = function(filtre = {typeParc: 'all'}){
        $('#table-listeReception-body').html('');
        $.ajax({
            url: "/api/reception/min",
            method: "GET",
            data: filtre,
            dataType: 'json',
            success: function (donnees, textStatus, jqXHR) {
                var data = (donnees);
                
                for(var data in donnees){
                    // console.log(donnees[data]);
                    //console.log(donnees[data]);
                    document.getElementById("table-listeReception-body").appendChild(getTableRow(donnees[data]));
                }
                $('.btn-edit-reception').attr("data-toggle","modal").attr("data-target","#modal-edit");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // console.log(jqXHR.responseText);
            }
        });
    };
    
    window.deleteData = function(data, source){
        $.ajax({
            url: "/reception/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat == "true"){
                    $('.status-del-reception').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-reception').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                    // console.log(source.parentNode.parentNode);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('.status-del-reception').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-reception').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    
    window.editData = function(ref, client, typeCoupe, data){
        $.ajax({
            data: {
                ref: ref,
                client: client,
                typeCoupe: typeCoupe,
                detail: data,
                taille: data.length
            },
            dataType: 'json',
            url: "/reception/edit",
            method: 'POST',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('#form-add-reception .overlay').addClass('hide');
                    $('#form-add-reception #btn-save-reception').removeClass("disabled");
                    $('.status-add-reception').removeClass('bg-red .disabled').addClass('bg-green disabled').html('Enregistré');
                    $('#form-add-reception #zone-ref').val('REF_CLASS_'+(new Date()).getTime());
                    loadData();
                }else{
                    $('#form-add-reception .overlay').addClass('hide');
                    $('#form-add-reception #btn-save-reception').removeAttr("disabled");
                    $('.status-add-reception').removeClass('bg-red .disabled').addClass('bg-red disabled').html(data.reason);
                }
                // console.log(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('#form-add-reception .overlay').addClass('hide');
                    $('#form-add-reception #btn-save-reception').removeClass("disabled");
                    $('.status-add-reception').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason);
                }else{
                    //this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
});

