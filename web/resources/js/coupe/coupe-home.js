
/* global user */

$(function(){
    
    window.getBille = function(){
        $.ajax({
            url: '/api/coupe/bille_for_coupe/list',
            method: 'GET',
            complete: function (jqXHR, textStatus) {
                var select = document.getElementById("zone-bille");
                var opt = document.createElement("option");
                var data = jqXHR.responseJSON;
                
                select.innerHTML = '';
                opt.value = '';
                opt.innerHTML = '-- Selectionnez --';
                select.appendChild(opt);
                
                for(var i=0; i<data.length; i++){
                    opt = document.createElement('option');
                    opt.value = data[i];
                    opt.innerHTML = data[i];
                    select.appendChild(opt);
                }
            }
        });
    };
    
    window.getTypeCoupe = function(){
        $.ajax({
            url: '/api/type_coupe/list',
            method: 'GET',
            complete: function (jqXHR, textStatus) {
                var select = document.getElementById("zone-coupe");
                var opt = document.createElement("option");
                var data = jqXHR.responseJSON;
                
                select.innerHTML = '';
                opt.value = '';
                opt.innerHTML = '-- Selectionnez --';
                select.appendChild(opt);
                
                for(var i=0; i<data.length; i++){
                    opt = document.createElement('option');
                    opt.value = data[i].ref;
                    opt.innerHTML = data[i].libelle;
                    select.appendChild(opt);
                }
            }
        });
    };
    
    window.getBillonForBille = function(numBille){
        $.ajax({
            url: '/api/coupe/billon_from_bille',
            method: 'GET',
            data: {bille: numBille},
            dataType: 'json',
            complete: function (jqXHR, textStatus) {
                var select = document.getElementById("zone-billon");
                var opt = document.createElement("option");
                var data = jqXHR.responseJSON;
                select.innerHTML = '';
                opt.value = '';
                opt.innerHTML = '-- Selectionnez --';
                select.appendChild(opt);
                
                for(var i=0; i<data.length; i++){
                    opt = document.createElement('option');
                    opt.value = data[i][0];
                    opt.innerHTML = data[i][0];
                    opt.setAttribute("longueur", data[i][2]);
                    opt.setAttribute("travail", data[i][1]);
                    select.appendChild(opt);
                }
            }
        });
    };
    
    window.setNombrePlots = function(){
        $('#zone-nombrePlot').change(function(){
            var val = $(this).val();
               var tableau = document.getElementById('table-details-plots');
               tableau.innerHTML = '';
               //console.log(window.longueur);
                for(var i=0; i<val; i++){
                    var ligne = document.createElement("tr");
                    var celTravail = document.createElement("td");
                    var celVol = document.createElement("td");
                    var celRef = document.createElement("td");
                    var input = document.createElement("input");
                    input.setAttribute("readonly","");
                    input.value = $('#zone-billon').val()+String.fromCharCode('A'.charCodeAt(0)+i);
                    input.id = 'zone-plot-ref-'+i;
                    celRef.appendChild(input);


                    input = document.createElement("input");
                    input.type = "number";
                    input.min = 0;
                    input.value = window.longueur;
                    input.setAttribute("readonly", "readonly");
                    input.id = 'zone-plot-longueur-'+i;
                    celVol.appendChild(input);
                    
                    input = document.createElement("input");
                    input.type = "text";
                    input.value = window.travail+"";
                    input.id = 'zone-plot-travail-'+i;
                    input.setAttribute("readonly", "");
                    celTravail.appendChild(input);

                    ligne.appendChild(celRef);
                    ligne.appendChild(celVol);
                    ligne.appendChild(celTravail);

                    tableau.appendChild(ligne);
               }
        });
    }
    
    
   $('#btn-addCoupe').click(function(){
      
       
       $.ajax({
            url: "/coupe/add",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#modal-default #modal-body').html(data);
           },
            error: function (jqXHR, textStatus, errorThrown) {
                 $('#modal-default #modal-body').html(jqXHR.responseText);
            }
       });
   });
    if(user.can_add !== 'O')       
        $('#btn-addCoupe').remove();
   
   
   
    function getTableRow(dataRow){
        if(dataRow.billon == null)
            return null;
        var row = document.createElement("tr"),
                
        celREFERENCE = document.createElement("td"),
        celBillon = document.createElement("td"),
        celNombrePlot = document.createElement("td"),
        celTypeCoupe = document.createElement("td"),
        celCertif = document.createElement("td"),
        celOpe = document.createElement("td"),
        
        btnDel = document.createElement("button"),
        btnEdi = document.createElement("button"),
        icon = document.createElement("i");

        celREFERENCE.innerHTML = dataRow.billon.split('-')[0] +'-'+dataRow.billon.split('-')[1]; //dataRow.ref;
        celBillon.innerHTML = dataRow.billon;
        celNombrePlot.innerHTML = dataRow.nbrePlot;
        celTypeCoupe.innerHTML = dataRow.typeCoupe.libelle;
        btnDel.type = "button";
        btnDel.className = "btn btn-danger";
            icon.className = "fa fa-trash";
        btnDel.appendChild(icon);
        btnDel.onclick = function(){if(confirm('Voulez-vous vraiment supprimer?'))deleteData(dataRow, this);};
        btnDel.id = "btn-del-"+dataRow.ref;
        btnDel.style.marginRight = "5px";
        btnDel.title = 'Delete ';
        
        
        btnEdi.type = "button";
        btnEdi.value = "Edit";
        btnEdi.id = "btn-edit"+dataRow.ref;
            icon = document.createElement('i');
            icon.className = "fa fa-pencil";
        btnEdi.className = "btn btn-success btn-edit-coupe";
        btnEdi.title = 'Modifier '+dataRow.billon;
        btnEdi.setAttribute("data-toggle", "modal");
        btnEdi.setAttribute("data-target", "#modal-edit");
        btnEdi.appendChild(icon);
        btnEdi.onclick = function(){
            $.ajax({
                url: "/coupe/edit",
                method: "GET",
                data: dataRow,
                dataType: 'json',
                complete: function (jqXHR, textStatus ) {
                    console.log(jqXHR);
                    $('#modal-edit #modal-body').html(jqXHR.responseText);
                }
            });
        };
        let iref = dataRow.billon.split('-');
        $.ajax({
            url: "/api/certification/travail",
            method: "GET",
            data: {ref: iref[0]+'-'+iref[1]},
            dataType: 'json',
            complete: function (jqXHR, textStatus ) {
                celCertif.innerHTML = jqXHR.responseJSON.certification;
            }
        });
        
        if(user.can_delete === 'O')
            celOpe.appendChild(btnDel);
        if(user.can_edit === 'O')
            celOpe.appendChild(btnEdi);
        row.appendChild(celREFERENCE);
        row.appendChild(celBillon);
        row.appendChild(celNombrePlot);
        row.appendChild(celTypeCoupe);
        row.appendChild(celCertif);
        row.appendChild(celOpe);
        return row;
    }

    window.deleteData = function(data, source){
        $.ajax({
            url: "/coupe/delete",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if(data.resultat === "true"){
                    $('.status-del-coupe').removeClass('bg-red .disabled').addClass('bg-green disabled').html('  Supprimé  ').css({'margin-left':'40px','width':'30%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-coupe').hide(1000);},4000);
                    var parent = source.parentNode.parentNode;
                    parent.parentNode.removeChild(parent);
                }else{
                    this.error(data, textStatus, jqXHR);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var message = jqXHR;
                if(message.resultat === "false"){
                    $('.status-del-coupe').removeClass('bg-green disabled').addClass('bg-red disabled').html(message.reason).css({'margin-left':'40px','width':'60%','text-align':'center'}).show(1000);
                    setTimeout(function(){$('.status-del-coupe').hide(1000);},4000);
                }else{
                   this.success(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };
    


window.loadData = function (){
        $("#table-body").html('');
        $.ajax({
            url: "/api/coupe/list",
            method: "GET",
            data: {},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                
                for(var i=0; i<data.length; i++){
                    document.getElementById("table-body").appendChild(getTableRow(data[i]));
                }
                            $('table').DataTable();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("processus erroné");
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
                console.log("Le processus est terminé");
            }
        });
    };
    
window.saveData = function(ref, billon, nombrePlot, typeCoupe, data){
    if(confirm('Voulez-vous vraiment enregistrer?') === false)
        return ;
    
     $.ajax({
            url: "/coupe/add",
            method: "POST",
            data: {
                ref: ref,
                billon: billon,
                nombrePlot: nombrePlot,
                typeCoupe: typeCoupe,
                data: data
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-addPane').click();
                loadData();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            complete: function (jqXHR, textStatus) {
                console.log("terminé");
            }
        });
};

    
window.editData = function(ref, billon, nombrePlot, typeCoupe){
     $.ajax({
            url: "/coupe/edit",
            method: "POST",
            data: {
                ref: ref,
                billon: billon,
                nombrePlot: nombrePlot,
                typeCoupe: typeCoupe
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#btn-close-editPane').click();
                loadData();
                console.log(data);
                console.log('icic');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
};

});